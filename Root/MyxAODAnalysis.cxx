//!  MyxAODAnalysis
/*!
  A more elaborate class description.
  */

#include <AsgTools/MessageCheck.h>
#include <TauAnalysis/MyxAODAnalysis.h>
#include <xAODEventInfo/EventInfo.h>

// Muon
#include <xAODMuon/MuonContainer.h>

//#define SCAN_XAOD

MyxAODAnalysis :: MyxAODAnalysis (const std::string& name,
    ISvcLocator *pSvcLocator)
: EL::AnaAlgorithm (name, pSvcLocator)
  , m_selTool( "TauAnalysisTools::TauSelectionTool/TauSelectionTool", this )
  , m_smearTool( "TauAnalysisTools::TauSmearingTool/TauSmearingTool", this )
  , m_effTool( "TauAnalysisTools::TauEfficiencyCorrectionsTool/TauEfficiencyCorrectionsTool", this )
  , m_truthTau( "TauAnalysisTools::TauTruthMatchingTool/TauTruthMatchingTool", this )
  , m_tEMLHTool(0)
  , InDet_pixel_raw_ClustersEta(4)
  , InDet_pixel_raw_ClustersPhi(4)
  , InDet_pixel_raw_ClustersCharge(4)
  , InDet_pixel_raw_globalX(4)
  , InDet_pixel_raw_globalY(4)
  , InDet_pixel_raw_globalZ(4)
  , InDet_pixel_raw_IB_energyDeposit(200)
  , InDet_pixel_raw_IB_barcode(200)
  , InDet_pixel_raw_IB_pdgId(200)
  , InDet_pixel_raw_BL_energyDeposit(200)
  , InDet_pixel_raw_BL_barcode(200)
  , InDet_pixel_raw_BL_pdgId(200)
  , InDet_pixel_raw_L1_energyDeposit(200)
  , InDet_pixel_raw_L1_barcode(200)
  , InDet_pixel_raw_L1_pdgId(200)
  , InDet_pixel_raw_L2_energyDeposit(200)
  , InDet_pixel_raw_L2_barcode(200)
  , InDet_pixel_raw_L2_pdgId(200)
  , InDet_sct_raw_globalX(4)
  , InDet_sct_raw_globalY(4)
  , InDet_sct_raw_globalZ(4)
{
  // Here you put any code for the base initialization of variables,
  // e.g. initialize all pointers to 0. Note that things like resetting
  // statistics variables should rather go into the initialize() function.

  declareProperty( "TauSelectionTool", m_selTool );
  declareProperty( "TauSmearingTool", m_smearTool );
  declareProperty( "TauEfficiencyTool", m_effTool );
  declareProperty( "TauTruthMatchingTool", m_truthTau );
}



StatusCode MyxAODAnalysis :: initialize ()
{
  ATH_MSG_INFO("Enter initialize()");
  // Here you do everything that needs to be done at the very
  // beginning on each worker node, e.g. create histograms and output
  // trees.  This method gets called before any input files are
  // connected.
  m_InDetTrackParticles = new TTree("m_InDetTrackParticles","m_InDetTrackParticles");
  ANA_CHECK( histSvc()->regTree("/SKIMTREESTREAM/m_InDetTrackParticles",m_InDetTrackParticles));
  MyxAODAnalysis :: InitSkim();

  /////////////////////////////////////////////////////////////////////////////
  //
  // TTree are declared here.
  //
  /////////////////////////////////////////////////////////////////////////////
  m_trueTauTree = new TTree("m_trueTauTree","m_trueTauTree");
  ANA_CHECK( histSvc()->regTree("/ANOTHERSTREAM/m_trueTauTree",m_trueTauTree));
  m_trueTauTree->Branch("tt_truth_TruthParticleContainerSize",     &tt_truth_TruthParticleContainerSize,     "tt_truth_TruthParticleContainerSize/I");
  m_trueTauTree->Branch("tt_truth_chargedHadron",           &tt_truth_chargedHadron,           "tt_truth_chargedHadron/I");
  // General information
  m_trueTauTree->Branch("eventNumber",                       &eventNumber,    "eventNumber/I");
  //
  // TruthParticleContainer basic kinematic variables //////////////////////////////////////////////////
  m_trueTauTree->Branch("Truth_part_status",                       &Truth_part_status);
  m_trueTauTree->Branch("Truth_part_isCharged",                    &Truth_part_isCharged);
  m_trueTauTree->Branch("Truth_part_status",                       &Truth_part_status);
  m_trueTauTree->Branch("Truth_part_isCharged",                    &Truth_part_isCharged);
  m_trueTauTree->Branch("Truth_part_pdgId",                        &Truth_part_pdgId);
  m_trueTauTree->Branch("Truth_part_hasProdVtx",                   &Truth_part_hasProdVtx);
  m_trueTauTree->Branch("Truth_part_hasDecayVtx",                  &Truth_part_hasDecayVtx);
  m_trueTauTree->Branch("Truth_part_nParents",                     &Truth_part_nParents);
  m_trueTauTree->Branch("Truth_part_nChildren",                    &Truth_part_nChildren);
  m_trueTauTree->Branch("Truth_part_numChargedTracks",             &Truth_part_numChargedTracks,         "Truth_part_numChargedTracks/I");
  m_trueTauTree->Branch("Truth_part_numChargedLeptonTracks",       &Truth_part_numChargedLeptonTracks,   "Truth_part_numChargedLeptonTracks/I");
  //  m_trueTauTree->Branch("Truth_part_p4",                     &Truth_part_p4);
  // TruthParticleContainer daughter kinematic variables //////////////////////////////////////////////////
  m_trueTauTree->Branch("Truth_dau_pdgId",                         &Truth_dau_pdgId);
  //
  // Truth tracks from tau                            //////////////////////////////////////////////////
  m_trueTauTree->Branch("Truth_track_pt",                 &Truth_track_pt);
  m_trueTauTree->Branch("Truth_track_eta",                &Truth_track_eta);
  m_trueTauTree->Branch("Truth_track_phi",                &Truth_track_phi);
  m_trueTauTree->Branch("Truth_track_m",                &Truth_track_m);
  m_trueTauTree->Branch("Truth_track_vtx",                &Truth_track_vtx);
  m_trueTauTree->Branch("Truth_track_charge",                &Truth_track_charge);
  m_trueTauTree->Branch("Truth_track_pdgId",                &Truth_track_pdgId);
  //
  // Truth production vertex information 
  m_trueTauTree->Branch("Truth_prodVtx_x",                &Truth_prodVtx_x,      "Truth_prodVtx_x/F" );
  m_trueTauTree->Branch("Truth_prodVtx_y",                &Truth_prodVtx_y,      "Truth_prodVtx_y/F" );
  m_trueTauTree->Branch("Truth_prodVtx_z",                &Truth_prodVtx_z,      "Truth_prodVtx_z/F" );
  m_trueTauTree->Branch("Truth_prodVtx_perp",             &Truth_prodVtx_perp,   "Truth_prodVtx_perp/F");
  m_trueTauTree->Branch("Truth_prodVtx_eta",              &Truth_prodVtx_eta,    "Truth_prodVtx_eta/F");
  m_trueTauTree->Branch("Truth_prodVtx_phi",              &Truth_prodVtx_phi,    "Truth_prodVtx_phi/F");
  m_trueTauTree->Branch("Truth_prodVtx_t",                &Truth_prodVtx_t,      "Truth_prodVtx_t/F");
  //
  // Truth decay vertex information 
  m_trueTauTree->Branch("Truth_decVtx_x",                &Truth_decVtx_x,      "Truth_decVtx_x/F" );
  m_trueTauTree->Branch("Truth_decVtx_y",                &Truth_decVtx_y,      "Truth_decVtx_y/F" );
  m_trueTauTree->Branch("Truth_decVtx_z",                &Truth_decVtx_z,      "Truth_decVtx_z/F" );
  m_trueTauTree->Branch("Truth_decVtx_perp",             &Truth_decVtx_perp,   "Truth_decVtx_perp/F");
  m_trueTauTree->Branch("Truth_decVtx_eta",              &Truth_decVtx_eta,    "Truth_decVtx_eta/F");
  m_trueTauTree->Branch("Truth_decVtx_phi",              &Truth_decVtx_phi,    "Truth_decVtx_phi/F");
  m_trueTauTree->Branch("Truth_decVtx_t",                &Truth_decVtx_t,      "Truth_decVtx_t/F");
  //
  // Truth tau info
  m_trueTauTree->Branch("Truth_tau_pt",                       &Truth_tau_pt,                       "Truth_tau_pt/D");
  m_trueTauTree->Branch("Truth_tau_eta",                      &Truth_tau_eta,                      "Truth_tau_eta/D");
  m_trueTauTree->Branch("Truth_tau_phi",                      &Truth_tau_phi,                      "Truth_tau_phi/D");
  m_trueTauTree->Branch("Truth_tau_e",                        &Truth_tau_e,                        "Truth_tau_e/D");
  m_trueTauTree->Branch("Truth_tau_m",                        &Truth_tau_m,                        "Truth_tau_m/D");
  m_trueTauTree->Branch("Truth_tau_pdgId",                    &Truth_tau_pdgId,                    "Truth_tau_pdgId/I");
  m_trueTauTree->Branch("Truth_tau_numTracks",                &Truth_tau_numTracks,                   "Truth_tau_numTracks/I");
  m_trueTauTree->Branch("Truth_tau_decayRad",                 &Truth_tau_decayRad,             "Truth_tau_decayRad/D");
  m_trueTauTree->Branch("Truth_tau_isMatchedWithReco",                    &Truth_tau_isMatchedWithReco,                    "Truth_tau_isMatchedWithReco/O");
  m_trueTauTree->Branch("Truth_tau_isLeptonic",           &Truth_tau_isLeptonic,           "Truth_tau_isLeptonic/O");
  m_trueTauTree->Branch("Truth_tau_isHadronic",           &Truth_tau_isHadronic,           "Truth_tau_isHadronic/O");
  //
  // 
  m_trueTauTree->Branch("tt_truth_leptonic_electron",        &tt_truth_leptonic_electron,        "tt_truth_leptonic_electron/O");
  m_trueTauTree->Branch("tt_truth_leptonic_muon",            &tt_truth_leptonic_muon,            "tt_truth_leptonic_muon/O");
  m_trueTauTree->Branch("tt_truth_leptonic_tau",             &tt_truth_leptonic_tau,             "tt_truth_leptonic_tau/O");
  m_trueTauTree->Branch("tt_truth_neutrino_pt",              &tt_truth_neutrino_pt,              "tt_truth_neutrino_pt/D");
  m_trueTauTree->Branch("tt_truth_neutrino_eta",             &tt_truth_neutrino_eta,             "tt_truth_neutrino_eta/D");
  m_trueTauTree->Branch("tt_truth_neutrino_phi",             &tt_truth_neutrino_phi,             "tt_truth_neutrino_phi/D");
  m_trueTauTree->Branch("tt_truth_neutrino_e",               &tt_truth_neutrino_e,               "tt_truth_neutrino_e/D");
  //
  ////////////////////// Truth neutrino information
  m_trueTauTree->Branch("Truth_neutrino_pdgId",              &Truth_neutrino_pdgId       );
  m_trueTauTree->Branch("Truth_neutrino_pt",                 &Truth_neutrino_pt          );
  m_trueTauTree->Branch("Truth_neutrino_eta",                &Truth_neutrino_eta         );
  m_trueTauTree->Branch("Truth_neutrino_phi",                &Truth_neutrino_phi         );
  //
  ////////////////////// Reconstructed tau information 
  m_trueTauTree->Branch("Reco_tau_pt",                        &Reco_tau_pt,                        "Reco_tau_pt/D");
  m_trueTauTree->Branch("Reco_tau_eta",                       &Reco_tau_eta,                       "Reco_tau_eta/D");
  m_trueTauTree->Branch("Reco_tau_phi",                       &Reco_tau_phi,                       "Reco_tau_phi/D");
  m_trueTauTree->Branch("Reco_tau_e",                         &Reco_tau_e,                         "Reco_tau_e/D");
  m_trueTauTree->Branch("Reco_tau_numTracks",                 &Reco_tau_numTracks,          "Reco_tau_numTracks/I");
  m_trueTauTree->Branch("Reco_tau_isSelected",                &Reco_tau_isSelected,                  "Reco_tau_isSelected/O");
  m_trueTauTree->Branch("Reco_track_numberOfPixelHits",         &Reco_track_numberOfPixelHits);

  m_trueTauTree->Branch("Reco_track_pt",                       &Reco_track_pt);
  m_trueTauTree->Branch("Reco_track_eta",                      &Reco_track_eta);
  m_trueTauTree->Branch("Reco_track_phi",                      &Reco_track_phi);
  m_trueTauTree->Branch("Reco_track_e",                        &Reco_track_e);
  m_trueTauTree->Branch("Reco_track_m",                        &Reco_track_m);
  m_trueTauTree->Branch("Reco_track_d0",                       &Reco_track_d0);
  m_trueTauTree->Branch("Reco_track_z0",                       &Reco_track_z0);
  //
  // InDetParticles (raw)
  m_trueTauTree->Branch("InDet_pixel_raw_ClustersEta",        &InDet_pixel_raw_ClustersEta);
  m_trueTauTree->Branch("InDet_pixel_raw_ClustersPhi",        &InDet_pixel_raw_ClustersPhi);
  m_trueTauTree->Branch("InDet_pixel_raw_ClustersCharge",     &InDet_pixel_raw_ClustersCharge);
  m_trueTauTree->Branch("InDet_pixel_raw_globalX",            &InDet_pixel_raw_globalX);
  m_trueTauTree->Branch("InDet_pixel_raw_globalY",            &InDet_pixel_raw_globalY);
  m_trueTauTree->Branch("InDet_pixel_raw_globalZ",            &InDet_pixel_raw_globalZ);
  m_trueTauTree->Branch("InDet_pixel_raw_IB_energyDeposit",   &InDet_pixel_raw_IB_energyDeposit);
  m_trueTauTree->Branch("InDet_pixel_raw_BL_energyDeposit",   &InDet_pixel_raw_BL_energyDeposit);
  m_trueTauTree->Branch("InDet_pixel_raw_L1_energyDeposit",   &InDet_pixel_raw_L1_energyDeposit);
  m_trueTauTree->Branch("InDet_pixel_raw_L2_energyDeposit",   &InDet_pixel_raw_L2_energyDeposit);
  m_trueTauTree->Branch("InDet_pixel_raw_IB_barcode",         &InDet_pixel_raw_IB_barcode);
  m_trueTauTree->Branch("InDet_pixel_raw_BL_barcode",         &InDet_pixel_raw_BL_barcode);
  m_trueTauTree->Branch("InDet_pixel_raw_L1_barcode",         &InDet_pixel_raw_L1_barcode);
  m_trueTauTree->Branch("InDet_pixel_raw_L2_barcode",         &InDet_pixel_raw_L2_barcode);
  m_trueTauTree->Branch("InDet_pixel_raw_IB_pdgId",           &InDet_pixel_raw_IB_pdgId);
  m_trueTauTree->Branch("InDet_pixel_raw_BL_pdgId",           &InDet_pixel_raw_BL_pdgId);
  m_trueTauTree->Branch("InDet_pixel_raw_L1_pdgId",           &InDet_pixel_raw_L1_pdgId);
  m_trueTauTree->Branch("InDet_pixel_raw_L2_pdgId",           &InDet_pixel_raw_L2_pdgId);
  m_trueTauTree->Branch("InDet_sct_raw_globalX",              &InDet_sct_raw_globalX);
  m_trueTauTree->Branch("InDet_sct_raw_globalY",              &InDet_sct_raw_globalY);
  m_trueTauTree->Branch("InDet_sct_raw_globalZ",              &InDet_sct_raw_globalZ);
  //
  // InDetParticles 
  m_trueTauTree->Branch("InDet_numTracks",                   &InDet_numTracks,                  "InDet_numTracks/I");
  m_trueTauTree->Branch("InDet_track_pt",                    &InDet_track_pt);
  m_trueTauTree->Branch("InDet_track_eta",                   &InDet_track_eta);
  m_trueTauTree->Branch("InDet_track_phi",                   &InDet_track_phi);
  m_trueTauTree->Branch("InDet_track_m",                     &InDet_track_m);
  m_trueTauTree->Branch("InDet_track_charge",               &InDet_track_charge);
  m_trueTauTree->Branch("InDet_track_d0",                     &InDet_track_d0);
  m_trueTauTree->Branch("InDet_track_z0",                     &InDet_track_z0);
  m_trueTauTree->Branch("InDet_track_sctZero",               &InDet_track_sctZero);
  m_trueTauTree->Branch("InDet_track_sctHits",               &InDet_track_sctHits);
  m_trueTauTree->Branch("InDet_track_barcode",               &InDet_track_barcode);
  m_trueTauTree->Branch("InDet_track_type",                  &InDet_track_type);
  m_trueTauTree->Branch("InDet_track_numberOfPixelHits",     &InDet_track_numberOfPixelHits);
  m_trueTauTree->Branch("InDet_track_numberOfIBLHits",           &InDet_track_numberOfIBLHits);
  m_trueTauTree->Branch("InDet_track_numberOfBLayerHits",        &InDet_track_numberOfBLayerHits);
  m_trueTauTree->Branch("InDet_track_numberOfPixelL1Hits",            &InDet_track_numberOfPixelL1Hits);
  m_trueTauTree->Branch("InDet_track_numberOfPixelL2Hits",            &InDet_track_numberOfPixelL2Hits);
  m_trueTauTree->Branch("InDet_track_numberOfSCTHits",       &InDet_track_numberOfSCTHits);
  m_trueTauTree->Branch("InDet_track_vx",                    &InDet_track_vx);
  m_trueTauTree->Branch("InDet_track_vy",                    &InDet_track_vy);
  m_trueTauTree->Branch("InDet_track_vz",                    &InDet_track_vz);
  //
  m_trueTauTree->Branch("InDet_pixel_numAllOfClusters",      &InDet_pixel_numAllOfClusters,      "InDet_pixel_numAllOfClusters/I");
  m_trueTauTree->Branch("InDet_pixel_IBLClustersEta",        &InDet_pixel_IBLClustersEta);
  m_trueTauTree->Branch("InDet_pixel_BLayerClustersEta",     &InDet_pixel_BLayerClustersEta);
  m_trueTauTree->Branch("InDet_pixel_L1ClustersEta",         &InDet_pixel_L1ClustersEta);
  m_trueTauTree->Branch("InDet_pixel_L2ClustersEta",         &InDet_pixel_L2ClustersEta);
  m_trueTauTree->Branch("InDet_pixel_IBLClustersPhi",        &InDet_pixel_IBLClustersPhi);
  m_trueTauTree->Branch("InDet_pixel_BLayerClustersPhi",     &InDet_pixel_BLayerClustersPhi);
  m_trueTauTree->Branch("InDet_pixel_L1ClustersPhi",         &InDet_pixel_L1ClustersPhi);
  m_trueTauTree->Branch("InDet_pixel_L2ClustersPhi",         &InDet_pixel_L2ClustersPhi);
  m_trueTauTree->Branch("InDet_pixel_IBLClustersCharge",     &InDet_pixel_IBLClustersCharge);
  m_trueTauTree->Branch("InDet_pixel_BLayerClustersCharge",  &InDet_pixel_BLayerClustersCharge);
  m_trueTauTree->Branch("InDet_pixel_L1ClustersCharge",      &InDet_pixel_L1ClustersCharge);
  m_trueTauTree->Branch("InDet_pixel_L2ClustersCharge",      &InDet_pixel_L2ClustersCharge);
  //
  // PixelClustersInfo  (without matching to reco-tau)
  m_trueTauTree->Branch("Pix_numCluster_IBL",              &Pix_numCluster_IBL,             "Pix_numCluster_IBL/I");
  m_trueTauTree->Branch("Pix_numCluster_BLayer",           &Pix_numCluster_BLayer,          "Pix_numCluster_BLayer/I");
  m_trueTauTree->Branch("Pix_numCluster_L1",               &Pix_numCluster_L1,              "Pix_numCluster_L1/I");
  m_trueTauTree->Branch("Pix_numCluster_L2",               &Pix_numCluster_L2,              "Pix_numCluster_L2/I");
  m_trueTauTree->Branch("Pix_charge_IBL",                  &Pix_charge_IBL,                 "Pix_charge_IBL/D");
  m_trueTauTree->Branch("Pix_charge_BLayer",               &Pix_charge_BLayer,              "Pix_charge_BLayer/D");
  m_trueTauTree->Branch("Pix_charge_L1",                   &Pix_charge_L1,                  "Pix_charge_L1/D");
  m_trueTauTree->Branch("Pix_charge_L2",                   &Pix_charge_L2,                  "Pix_charge_L2/D");
  //
  // Number of tracks
  // Pixel
  m_trueTauTree->Branch("tt_numberOfContribPixelLayers",      &tt_numberOfContribPixelLayers);
  m_trueTauTree->Branch("tt_numberOfPixelHits",               &tt_numberOfPixelHits        );
  m_trueTauTree->Branch("tt_numberOfInnermostPixelLayerHits",       &tt_numberOfInnermostPixelLayerHits   );
  m_trueTauTree->Branch("tt_numberOfNextToInnermostPixelLayerHits", &tt_numberOfNextToInnermostPixelLayerHits   );
  m_trueTauTree->Branch("tt_numberOfPixelSharedHits",         &tt_numberOfPixelSharedHits        );
  m_trueTauTree->Branch("tt_numberOfPixelHoles",              &tt_numberOfPixelHoles        );
  m_trueTauTree->Branch("tt_numberOfPixelSpoiltHits",         &tt_numberOfPixelSpoiltHits        );
  m_trueTauTree->Branch("tt_numberOfPixelSplitHits",          &tt_numberOfPixelSplitHits        );
  m_trueTauTree->Branch("tt_numberOfPixelOutliers",           &tt_numberOfPixelOutliers        );
  m_trueTauTree->Branch("tt_expectInnermostPixelLayerHit",    &tt_expectInnermostPixelLayerHit              );
  m_trueTauTree->Branch("tt_numberOfInnermostPixelLayerHits", &tt_numberOfInnermostPixelLayerHits              );
  m_trueTauTree->Branch("tt_numberOfGangedPixels",            &tt_numberOfGangedPixels              );
  m_trueTauTree->Branch("tt_numberOfPixelDeadSensors",        &tt_numberOfPixelDeadSensors              );
  // Pixel B-Layer
  m_trueTauTree->Branch("tt_numberOfBLayerHits",              &tt_numberOfBLayerHits       );
  m_trueTauTree->Branch("tt_numberOfBLayerSharedHits",        &tt_numberOfBLayerSharedHits        );
  m_trueTauTree->Branch("tt_numberOfBLayerOutliers",          &tt_numberOfBLayerOutliers        );
  m_trueTauTree->Branch("tt_numberOfBLayerSplitHits ",        &tt_numberOfBLayerSplitHits         );
  m_trueTauTree->Branch("tt_expectBLayerHit",                 &tt_expectBLayerHit          );
  // Precision information
  m_trueTauTree->Branch("tt_numberOfPrecisionLayers",         &tt_numberOfPrecisionLayers      );
  m_trueTauTree->Branch("tt_numberOfPrecisionHoleLayers",     &tt_numberOfPrecisionHoleLayers  );
  m_trueTauTree->Branch("tt_numberOfPhiLayers",               &tt_numberOfPhiLayers            );
  m_trueTauTree->Branch("tt_numberOfPhiHoleLayers",           &tt_numberOfPhiHoleLayers        );
  m_trueTauTree->Branch("tt_numberOfTriggerEtaLayers",        &tt_numberOfTriggerEtaLayers     );
  m_trueTauTree->Branch("tt_numberOfTriggerEtaHoleLayers",    &tt_numberOfTriggerEtaHoleLayers );
  m_trueTauTree->Branch("tt_numberOfGoodPrecisionLayers",     &tt_numberOfGoodPrecisionLayers  );
  m_trueTauTree->Branch("tt_numberOfOutliersOnTrack",         &tt_numberOfOutliersOnTrack      );
  //
  m_trueTauTree->Branch("tt_numberOfSCTHits",                 &tt_numberOfSCTHits          );
  m_trueTauTree->Branch("tt_numberOfTRTHits",                 &tt_numberOfTRTHits          );
  m_trueTauTree->Branch("tt_numberOfSCTSharedHits",           &tt_numberOfSCTSharedHits        );
  m_trueTauTree->Branch("tt_numberOfTRTSharedHits",           &tt_numberOfTRTSharedHits        );
  m_trueTauTree->Branch("tt_numberOfSCTHoles",                &tt_numberOfSCTHoles         );
  m_trueTauTree->Branch("tt_numberOfSCTDoubleHoles",          &tt_numberOfSCTDoubleHoles        );
  m_trueTauTree->Branch("tt_numberOfTRTHoles",                &tt_numberOfTRTHoles         );
  m_trueTauTree->Branch("tt_numberOfSCTOutliers",             &tt_numberOfSCTOutliers        );
  m_trueTauTree->Branch("tt_numberOfTRTOutliers",             &tt_numberOfTRTOutliers        );
  m_trueTauTree->Branch("tt_numberOfSCTSpoiltHits",           &tt_numberOfSCTSpoiltHits        );
  m_trueTauTree->Branch("tt_numberOfGangedFlaggedFakes",      &tt_numberOfGangedFlaggedFakes              );
  m_trueTauTree->Branch("tt_numberOfSCTDeadSensors",          &tt_numberOfSCTDeadSensors              );
  m_trueTauTree->Branch("tt_numberOfTRTDeadStraws",           &tt_numberOfTRTDeadStraws              );
  m_trueTauTree->Branch("tt_pixeldEdx",                       &tt_pixeldEdx);                
  //
  // Pixel raw clusters
  m_trueTauTree->Branch( "Reco_track_numberOfInnermostPixelLayerSensors"      , &Reco_track_numberOfInnermostPixelLayerSensors      );
  m_trueTauTree->Branch( "Reco_track_numberOfNextToInnermostPixelLayerSensors", &Reco_track_numberOfNextToInnermostPixelLayerSensors);
  m_trueTauTree->Branch( "Reco_track_numberOfPixelL1Sensors"                  , &Reco_track_numberOfPixelL1Sensors                  );
  m_trueTauTree->Branch( "Reco_track_numberOfPixelL2Sensors"                  , &Reco_track_numberOfPixelL2Sensors                  );
  m_trueTauTree->Branch( "Reco_track_numberOfInnermostPixelHits"              , &Reco_track_numberOfInnermostPixelHits      );
  m_trueTauTree->Branch( "Reco_track_numberOfNextToInnermostPixelHits"        , &Reco_track_numberOfNextToInnermostPixelHits);
  m_trueTauTree->Branch( "Reco_track_numberOfPixelL1Hits"                     , &Reco_track_numberOfPixelL1Hits                  );
  m_trueTauTree->Branch( "Reco_track_numberOfPixelL2Hits"                     , &Reco_track_numberOfPixelL2Hits              );

  // Greet the user:
  ATH_MSG_INFO( "Initialising - Package version: " << PACKAGE_VERSION );
  ATH_MSG_INFO( "SGKey = " << m_sgKey );
  ATH_MSG_INFO( "TauSelectionTool  = " << m_selTool );
  ATH_MSG_INFO( "TauSmearingTool   = " << m_smearTool );
  ATH_MSG_INFO( "TauEfficiencyTool = " << m_effTool );
  ATH_MSG_INFO( "TauTruthMatchingTool = " << m_truthTau );

  // Retrieve the tools:
  ATH_CHECK( m_selTool.retrieve() );
  ATH_CHECK( m_smearTool.retrieve() );
  ATH_CHECK( m_effTool.retrieve() );
  ATH_CHECK( m_truthTau.retrieve() );

  // Selection tool
  TauSelTool = new TauAnalysisTools::TauSelectionTool( "TauSelectionTool" );
  TauSelTool->msg().setLevel( MSG::DEBUG );
  // preparation for control hisograms
  //TauSelTool->setOutFile( fOutputFile.get() );
  ATH_CHECK(TauSelTool->setProperty("MuonOLR", true ));
  ATH_CHECK(TauSelTool->setProperty("EleOLR", true ));
  ATH_CHECK(TauSelTool->setProperty("JetIDWP", int(TauAnalysisTools::JETIDBDTMEDIUM) ));
  ATH_CHECK(TauSelTool->setProperty("PtMin", 20. ));
  ATH_CHECK(TauSelTool->setProperty("ConfigPath", "" ));
  ATH_CHECK(TauSelTool->setProperty("SelectionCuts", 
        int(TauAnalysisTools::CutPt|
          TauAnalysisTools::CutMuonOLR|
          TauAnalysisTools::CutJetIDWP|
          TauAnalysisTools::CutEleOLR) ));
  ATH_CHECK(TauSelTool->initialize());

  // AsgElectronLikelihoodTool
  m_tEMLHTool = new AsgElectronLikelihoodTool(name()+"_ELHTool");

  ATH_MSG_INFO("Exist initialize()");
  return StatusCode::SUCCESS;
}



StatusCode MyxAODAnalysis :: execute ()
{
#ifdef SCAN_XAOD 
  /* This SCAN_XAOD part can be used to skim xAOD.
     In particular, this partis for InDetParticles before any reconstruction requirement.
     */
  const xAOD::TruthParticleContainer* scan_truth = 0;
  ATH_CHECK( evtStore()->retrieve( scan_truth, "TruthParticles" ));
  bool scan_xaod_hadtau = false;
  const xAOD::TruthVertex* scan_pVtx = 0 ;
  for(xAOD::TruthParticleContainer::const_iterator truth_itr = scan_truth->begin(); truth_itr != scan_truth->end(); truth_itr++ ) {
    const xAOD::TruthParticle* pTruth = *truth_itr;

    if ( !pTruth->isTau() ) continue; 
    // get vertex and check if it exists
    const xAOD::TruthVertex* xDecayVertex = pTruth->decayVtx();
    if (!xDecayVertex) continue;

    // Counting the number of outgoing particles
    for ( size_t iOutgoingParticle = 0; iOutgoingParticle < xDecayVertex->nOutgoingParticles(); ++iOutgoingParticle ) {
      const xAOD::TruthParticle* xTruthDaughter = xDecayVertex->outgoingParticle(iOutgoingParticle);
      int iAbsPdgId = xTruthDaughter->absPdgId();
      // only process stable particles
      //  status 1 = stable
      //  status 2 = un-stable
      if (xTruthDaughter->status() != 1 ) continue;

      if ( xTruthDaughter->isHadron() ) { scan_xaod_hadtau = true; scan_pVtx = pTruth->decayVtx(); break;}
      if ( xTruthDaughter->isLepton() ) break;
    }
  }
  //  if ( scan_xaod_hadtau == true && scan_pVtx->perp() > 33.25 ) {
  //  if ( scan_xaod_hadtau == true && scan_pVtx->perp() > 50.5 ) {
  //  if ( scan_xaod_hadtau == true && scan_pVtx->perp() > 88.5 ) {
  if ( scan_xaod_hadtau == true && scan_pVtx->perp() > 122.5 ) {
    // To scan the xAOD basic variables
    const xAOD::TrackParticleContainer* TrackPart = 0;
    CHECK(evtStore()->retrieve( TrackPart, "InDetTrackParticles"));
    MyxAODAnalysis :: FillInDetTrackParticle(TrackPart);
  }
  return StatusCode::SUCCESS;
#endif
  // General information 
  static int event_counter=1;
  eventNumber = event_counter;
  event_counter++;

  // retrieve the eventInfo object from the event store
  const xAOD::EventInfo *eventInfo = nullptr;
  ANA_CHECK (evtStore()->retrieve (eventInfo, "EventInfo"));

  // Loop over reco tau: 
  /// This is for caluculating fractions = reco/true;
  // Retrieve the taus:
  const xAOD::TauJetContainer* taus = 0;
  ATH_CHECK( evtStore()->retrieve( taus ) );

  // TruthParticle --> TauJet  //////////////////////////////////////////////////////////////////////////////////////////////////////////////
  //
  //   The follwoing parts can be used to calculate the "efficiency".
  //   1. Find a true tau (hadoronic decay) from TruthParticleContainer.
  //   2. And then check whether the mathced tau exists or not.

  // ==================================== Pixel information ========================================
  // ===============================================================================================

  Pix_numCluster_IBL          = 0;
  Pix_numCluster_BLayer       = 0;
  Pix_numCluster_L1           = 0;
  Pix_numCluster_L2           = 0;
  Pix_charge_IBL              = 0;   
  Pix_charge_BLayer           = 0; 
  Pix_charge_L1               = 0;   
  Pix_charge_L2               = 0;   
  //
  InDet_numTracks              = 0;
  InDet_pixel_numAllOfClusters = 0;
  //
  for ( int iLayer=0; iLayer<4; iLayer++){
    InDet_pixel_raw_ClustersEta   [iLayer]      .clear();
    InDet_pixel_raw_ClustersPhi   [iLayer]      .clear();
    InDet_pixel_raw_ClustersCharge[iLayer]      .clear();
    InDet_pixel_raw_globalX       [iLayer]      .clear();
    InDet_pixel_raw_globalY       [iLayer]      .clear();
    InDet_pixel_raw_globalZ       [iLayer]      .clear();
    InDet_sct_raw_globalX       [iLayer]      .clear();
    InDet_sct_raw_globalY       [iLayer]      .clear();
    InDet_sct_raw_globalZ       [iLayer]      .clear();
  }
  for ( int iCluster=0; iCluster<200; iCluster++) {
    InDet_pixel_raw_IB_energyDeposit[iCluster]    .clear();
    InDet_pixel_raw_IB_barcode      [iCluster]    .clear();
    InDet_pixel_raw_IB_pdgId        [iCluster]    .clear();
    InDet_pixel_raw_BL_energyDeposit[iCluster]    .clear();
    InDet_pixel_raw_BL_barcode      [iCluster]    .clear();
    InDet_pixel_raw_BL_pdgId        [iCluster]    .clear();
    InDet_pixel_raw_L1_energyDeposit[iCluster]    .clear();
    InDet_pixel_raw_L1_barcode      [iCluster]    .clear();
    InDet_pixel_raw_L1_pdgId        [iCluster]    .clear();
    InDet_pixel_raw_L2_energyDeposit[iCluster]    .clear();
    InDet_pixel_raw_L2_barcode      [iCluster]    .clear();
    InDet_pixel_raw_L2_pdgId        [iCluster]    .clear();
  }
  //
  InDet_track_pt                      .clear();
  InDet_track_eta                     .clear();
  InDet_track_phi                     .clear();
  InDet_track_m                       .clear();
  InDet_track_charge                  .clear();
  InDet_track_d0                      .clear();
  InDet_track_z0                      .clear();
  InDet_track_sctZero                 .clear();
  InDet_track_numberOfIBLHits         .clear();
  InDet_track_numberOfBLayerHits      .clear();
  InDet_track_numberOfPixelL1Hits     .clear();
  InDet_track_numberOfPixelL2Hits     .clear();
  InDet_track_sctHits                 .clear();
  InDet_track_barcode                 .clear();
  InDet_track_type                    .clear();
  InDet_track_numberOfPixelHits       .clear();
  InDet_track_numberOfSCTHits         .clear();
  InDet_track_vx                      .clear();
  InDet_track_vy                      .clear();
  InDet_track_vz                      .clear();
  InDet_pixel_IBLClustersEta          .clear();
  InDet_pixel_BLayerClustersEta       .clear();
  InDet_pixel_L1ClustersEta           .clear();
  InDet_pixel_L2ClustersEta           .clear();
  InDet_pixel_IBLClustersPhi          .clear();
  InDet_pixel_BLayerClustersPhi       .clear();
  InDet_pixel_L1ClustersPhi           .clear();
  InDet_pixel_L2ClustersPhi           .clear();
  InDet_pixel_IBLClustersCharge       .clear();
  InDet_pixel_BLayerClustersCharge    .clear();
  InDet_pixel_L1ClustersCharge        .clear();
  InDet_pixel_L2ClustersCharge        .clear();

  //
  //---------------------
  // RECONSTRUCTED TRACKS
  //---------------------
  const xAOD::TrackParticleContainer* recoTracksOrig = 0;
  CHECK(evtStore()->retrieve(recoTracksOrig,"InDetTrackParticles"));
  // make a shallow copy
  std::pair< xAOD::TrackParticleContainer*, xAOD::ShallowAuxContainer* > recoTracksShallowCopyPair = xAOD::shallowCopyContainer( *recoTracksOrig );
  xAOD::TrackParticleContainer* recoTracks = recoTracksShallowCopyPair.first;
  InDet_numTracks = recoTracks->size();

  //---------------------
  // Pixel Clusters
  //---------------------
  const xAOD::TrackMeasurementValidationContainer* pixClustersOrig = 0;
  CHECK(evtStore()->retrieve(pixClustersOrig,"PixelClusters"));
  // make a shallow copy
  std::pair< xAOD::TrackMeasurementValidationContainer*, xAOD::ShallowAuxContainer* > pixClustersPair = xAOD::shallowCopyContainer( *pixClustersOrig );
  xAOD::TrackMeasurementValidationContainer* pixClusters =  pixClustersPair.first;
  
  InDet_pixel_numAllOfClusters = pixClusters->size();
  /* 
   * Raw pixel clusters 
   * */
  int numClustersonIBL    = 0;
  int numClustersonBLayer = 0;
  int numClustersonL1     = 0;
  int numClustersonL2     = 0;

  int ibl_test_counter=0;
  int bl_test_counter=0;
  int l1_test_counter=0;
  int l2_test_counter=0;
  int other_counter=0;
  for (xAOD::TrackMeasurementValidationContainer::iterator clus_itr=(pixClusters)->begin(); clus_itr!=(pixClusters)->end(); ++clus_itr) {
    if      ((*clus_itr)->auxdata<int>("layer")==0 && (*clus_itr)->auxdata<int>("bec")==0) {  // IBL
      ibl_test_counter++;
    }
    else if      ((*clus_itr)->auxdata<int>("layer")==1 && (*clus_itr)->auxdata<int>("bec")==0) {  // IBL
      bl_test_counter++;
    }
    else if      ((*clus_itr)->auxdata<int>("layer")==2 && (*clus_itr)->auxdata<int>("bec")==0) {  // IBL
      l1_test_counter++;
    }
    else if      ((*clus_itr)->auxdata<int>("layer")==3 && (*clus_itr)->auxdata<int>("bec")==0) {  // IBL
      l2_test_counter++;
    }else {
      other_counter++;
    }
  }

  for (xAOD::TrackMeasurementValidationContainer::iterator clus_itr=(pixClusters)->begin(); clus_itr!=(pixClusters)->end(); ++clus_itr) {
    if      ((*clus_itr)->auxdata<int>("layer")==0 && (*clus_itr)->auxdata<int>("bec")==0) {  // IBL
      TLorentzVector clusterTLV;
      clusterTLV.SetXYZT( (*clus_itr)->globalX(), (*clus_itr)->globalY(), (*clus_itr)->globalZ(), 0);
      InDet_pixel_raw_ClustersEta[0]          .push_back( clusterTLV.Eta() );
      InDet_pixel_raw_ClustersPhi[0]          .push_back( clusterTLV.Phi() );
      InDet_pixel_raw_ClustersCharge[0]       .push_back( (*clus_itr)->auxdata<float>("charge") );
      InDet_pixel_raw_globalX[0]              .push_back( (*clus_itr)->globalX() );
      InDet_pixel_raw_globalY[0]              .push_back( (*clus_itr)->globalY() );
      InDet_pixel_raw_globalZ[0]              .push_back( (*clus_itr)->globalZ() );
      
      static const SG::AuxElement::Accessor<std::vector<float> > acc_sihit_energyDeposit ("sihit_energyDeposit");
      static const SG::AuxElement::Accessor<std::vector<int> > acc_sihit_barcode ("sihit_barcode");
      static const SG::AuxElement::Accessor<std::vector<int> > acc_sihit_pdgid   ("sihit_pdgid");
      static const SG::AuxElement::Accessor<std::vector<int> > acc_truth_barcode ("truth_barcode");
      static const SG::AuxElement::Accessor<std::vector<float> > acc_sihit_startPosX("sihit_startPosX");
      static const SG::AuxElement::Accessor<std::vector<float> > acc_sihit_startPosY("sihit_startPosY");
      static const SG::AuxElement::Accessor<std::vector<float> > acc_sihit_startPosZ("sihit_startPosZ");
      
      auto pixelCluster = *clus_itr;
      std::vector<float> energyDeposit = acc_sihit_energyDeposit(*pixelCluster);
      std::vector<int> barcodes        = acc_sihit_barcode(*pixelCluster);
      std::vector<int> pdgId           = acc_sihit_pdgid(*pixelCluster);
      std::vector<int> truth_barcode   = acc_truth_barcode(*pixelCluster);
      std::vector<float> startPosX       = acc_sihit_startPosX(*pixelCluster);
      std::vector<float> startPosY       = acc_sihit_startPosY(*pixelCluster);
      std::vector<float> startPosZ       = acc_sihit_startPosZ(*pixelCluster);
      // std::cout 
      // << (*clus_itr)->auxdata<uint64_t>("detectorElementID") << " " << (*clus_itr)->auxdata<int>("eta_module") << " "
      // << (*clus_itr)->auxdata<int>("phi_module") << " " << (*clus_itr)->identifier() << std::endl;
      for(unsigned int index=0; index< barcodes.size(); index++){
        InDet_pixel_raw_IB_energyDeposit[numClustersonIBL].push_back(energyDeposit.at(index));
        InDet_pixel_raw_IB_barcode      [numClustersonIBL].push_back(barcodes    .at(index));
        InDet_pixel_raw_IB_pdgId        [numClustersonIBL].push_back(pdgId       .at(index));
      }
      if (barcodes.size() == 0 ){
        InDet_pixel_raw_IB_energyDeposit[numClustersonIBL].push_back(0);
        InDet_pixel_raw_IB_barcode      [numClustersonIBL].push_back(0);
        InDet_pixel_raw_IB_pdgId        [numClustersonIBL].push_back(0);
      }
      numClustersonIBL++;
    }
    else if ((*clus_itr)->auxdata<int>("layer")==1 && (*clus_itr)->auxdata<int>("bec")==0) {  // b-layer
      TLorentzVector clusterTLV;
      clusterTLV.SetXYZT( (*clus_itr)->globalX(), (*clus_itr)->globalY(), (*clus_itr)->globalZ(), 0);
      InDet_pixel_raw_ClustersEta[1]          .push_back( clusterTLV.Eta() );
      InDet_pixel_raw_ClustersPhi[1]          .push_back( clusterTLV.Phi() );
      InDet_pixel_raw_ClustersCharge[1]       .push_back( (*clus_itr)->auxdata<float>("charge") );
      InDet_pixel_raw_globalX[1]              .push_back( (*clus_itr)->globalX() );
      InDet_pixel_raw_globalY[1]              .push_back( (*clus_itr)->globalY() );
      InDet_pixel_raw_globalZ[1]              .push_back( (*clus_itr)->globalZ() );
      
      static const SG::AuxElement::Accessor<std::vector<float> > acc_sihit_energyDeposit ("sihit_energyDeposit");
      static const SG::AuxElement::Accessor<std::vector<int> > acc_sihit_barcode ("sihit_barcode");
      static const SG::AuxElement::Accessor<std::vector<int> > acc_sihit_pdgid   ("sihit_pdgid");
      auto pixelCluster = *clus_itr;
      std::vector<float> energyDeposit = acc_sihit_energyDeposit(*pixelCluster);
      std::vector<int> barcodes        = acc_sihit_barcode(*pixelCluster);
      std::vector<int> pdgId           = acc_sihit_pdgid(*pixelCluster);
      for(unsigned int index=0; index< barcodes.size(); index++){
        InDet_pixel_raw_BL_energyDeposit[numClustersonBLayer].push_back(energyDeposit.at(index));
        InDet_pixel_raw_BL_barcode      [numClustersonBLayer].push_back(barcodes    .at(index));
        InDet_pixel_raw_BL_pdgId        [numClustersonBLayer].push_back(pdgId       .at(index));
      }
      if (barcodes.size() == 0 ){
        InDet_pixel_raw_BL_energyDeposit[numClustersonBLayer].push_back(1);
        InDet_pixel_raw_BL_barcode      [numClustersonBLayer].push_back(1);
        InDet_pixel_raw_BL_pdgId        [numClustersonBLayer].push_back(1);
      }
      numClustersonBLayer++;
    }
    else if ((*clus_itr)->auxdata<int>("layer")==2 && (*clus_itr)->auxdata<int>("bec")==0) {  // b-layer
      TLorentzVector clusterTLV;
      clusterTLV.SetXYZT( (*clus_itr)->globalX(), (*clus_itr)->globalY(), (*clus_itr)->globalZ(), 0);
      InDet_pixel_raw_ClustersEta[2]          .push_back( clusterTLV.Eta() );
      InDet_pixel_raw_ClustersPhi[2]          .push_back( clusterTLV.Phi() );
      InDet_pixel_raw_ClustersCharge[2]       .push_back( (*clus_itr)->auxdata<float>("charge") );
      InDet_pixel_raw_globalX[2]              .push_back( (*clus_itr)->globalX() );
      InDet_pixel_raw_globalY[2]              .push_back( (*clus_itr)->globalY() );
      InDet_pixel_raw_globalZ[2]              .push_back( (*clus_itr)->globalZ() );
      
      static const SG::AuxElement::Accessor<std::vector<float> > acc_sihit_energyDeposit ("sihit_energyDeposit");
      static const SG::AuxElement::Accessor<std::vector<int> > acc_sihit_barcode ("sihit_barcode");
      static const SG::AuxElement::Accessor<std::vector<int> > acc_sihit_pdgid   ("sihit_pdgid");
      auto pixelCluster = *clus_itr;
      std::vector<float> energyDeposit = acc_sihit_energyDeposit(*pixelCluster);
      std::vector<int> barcodes        = acc_sihit_barcode(*pixelCluster);
      std::vector<int> pdgId           = acc_sihit_pdgid(*pixelCluster);
      for(unsigned int index=0; index< barcodes.size(); index++){
        InDet_pixel_raw_L1_energyDeposit[numClustersonL1].push_back(energyDeposit.at(index));
        InDet_pixel_raw_L1_barcode      [numClustersonL1].push_back(barcodes    .at(index));
        InDet_pixel_raw_L1_pdgId        [numClustersonL1].push_back(pdgId       .at(index));
      }
      if (barcodes.size() == 0 ){
        InDet_pixel_raw_L1_energyDeposit[numClustersonL1].push_back(2);
        InDet_pixel_raw_L1_barcode      [numClustersonL1].push_back(2);
        InDet_pixel_raw_L1_pdgId        [numClustersonL1].push_back(2);
      }
      numClustersonL1++;
    }
    else if ((*clus_itr)->auxdata<int>("layer")==3 && (*clus_itr)->auxdata<int>("bec")==0) {  // b-layer
      TLorentzVector clusterTLV;
      clusterTLV.SetXYZT( (*clus_itr)->globalX(), (*clus_itr)->globalY(), (*clus_itr)->globalZ(), 0);
      InDet_pixel_raw_ClustersEta[3]          .push_back( clusterTLV.Eta() );
      InDet_pixel_raw_ClustersPhi[3]          .push_back( clusterTLV.Phi() );
      InDet_pixel_raw_ClustersCharge[3]       .push_back( (*clus_itr)->auxdata<float>("charge") );
      InDet_pixel_raw_globalX[3]              .push_back( (*clus_itr)->globalX() );
      InDet_pixel_raw_globalY[3]              .push_back( (*clus_itr)->globalY() );
      InDet_pixel_raw_globalZ[3]              .push_back( (*clus_itr)->globalZ() );
      
      static const SG::AuxElement::Accessor<std::vector<float> > acc_sihit_energyDeposit ("sihit_energyDeposit");
      static const SG::AuxElement::Accessor<std::vector<int> > acc_sihit_barcode ("sihit_barcode");
      static const SG::AuxElement::Accessor<std::vector<int> > acc_sihit_pdgid   ("sihit_pdgid");
      auto pixelCluster = *clus_itr;
      std::vector<float> energyDeposit = acc_sihit_energyDeposit(*pixelCluster);
      std::vector<int> barcodes        = acc_sihit_barcode(*pixelCluster);
      std::vector<int> pdgId           = acc_sihit_pdgid(*pixelCluster);
      for(unsigned int index=0; index< barcodes.size(); index++){
        InDet_pixel_raw_L2_energyDeposit[numClustersonL2].push_back(energyDeposit.at(index));
        InDet_pixel_raw_L2_barcode      [numClustersonL2].push_back(barcodes    .at(index));
        InDet_pixel_raw_L2_pdgId        [numClustersonL2].push_back(pdgId       .at(index));
      }
      if (barcodes.size() == 0 ){
        InDet_pixel_raw_L2_energyDeposit[numClustersonL2].push_back(3);
        InDet_pixel_raw_L2_barcode      [numClustersonL2].push_back(3);
        InDet_pixel_raw_L2_pdgId        [numClustersonL2].push_back(3);
      }
      numClustersonL2++;
    }
  }
  std::cout << " ***************** " << std::endl;

  static SG::AuxElement::ConstAccessor<MeasurementsOnTrack>  acc_MeasurementsOnTrack("msosLink");
  // raw InDetTracks
  for (xAOD::TrackParticleContainer::const_iterator recoTrk_itr=recoTracks->begin(); recoTrk_itr!=recoTracks->end(); recoTrk_itr++) {

    // ---------------------------------------------
    //  Pixel clusters associeated with reco tracks
    // ---------------------------------------------
    Int_t numtype         = (*recoTrk_itr)->type();
    InDet_track_type     .push_back( numtype );
    InDet_track_pt       .push_back( (*recoTrk_itr)->pt()  );
    InDet_track_eta      .push_back( (*recoTrk_itr)->eta() );
    InDet_track_phi      .push_back( (*recoTrk_itr)->phi() );
    InDet_track_m        .push_back( (*recoTrk_itr)->m()   );
    InDet_track_charge   .push_back( (*recoTrk_itr)->charge()  );
    InDet_track_d0       .push_back( (*recoTrk_itr)->d0()  );
    InDet_track_z0       .push_back( (*recoTrk_itr)->z0()  );
    InDet_track_vx       .push_back( (*recoTrk_itr)->vx()  );
    InDet_track_vy       .push_back( (*recoTrk_itr)->vy()  );
    InDet_track_vz       .push_back( (*recoTrk_itr)->vz()  );

    uint8_t numberOfSCTHits   = 245;
    uint8_t numberOfPixelHits = 245;
    if ( (*recoTrk_itr)-> summaryValue(numberOfPixelHits, xAOD::numberOfPixelHits)) InDet_track_numberOfPixelHits .push_back( numberOfPixelHits );
    if ( (*recoTrk_itr)-> summaryValue(numberOfSCTHits,   xAOD::numberOfSCTHits)  ) InDet_track_numberOfSCTHits   .push_back( numberOfSCTHits   );

    TLorentzVector recoTrack;
    recoTrack.SetPtEtaPhiM( 
        (*recoTrk_itr)->pt(),
        (*recoTrk_itr)->eta(),
        (*recoTrk_itr)->phi(),
        (*recoTrk_itr)->m() 
        );

    // Check the number of pixel hit information 
    Int_t ibl_hits      = 0;
    Int_t blayer_hits   = 0;
    Int_t l1_hits       = 0;
    Int_t l2_hits       = 0;
    Int_t total_sct_num = 0;
    std::vector<double> IBLClusterEta;
    std::vector<double> BLayerClusterEta;
    std::vector<double> L1ClusterEta;
    std::vector<double> L2ClusterEta;
    std::vector<double> IBLClusterPhi;
    std::vector<double> BLayerClusterPhi;
    std::vector<double> L1ClusterPhi;
    std::vector<double> L2ClusterPhi;
    std::vector<double> IBLClusterCharge;
    std::vector<double> BLayerClusterCharge;
    std::vector<double> L1ClusterCharge;
    std::vector<double> L2ClusterCharge;

    const MeasurementsOnTrack& measurementsOnTrack = acc_MeasurementsOnTrack(*(*recoTrk_itr));
    for (MeasurementsOnTrackIter msos_iter=measurementsOnTrack.begin(); msos_iter!=measurementsOnTrack.end(); ++msos_iter) {
      if (!(*msos_iter).isValid()) { continue; }
      const xAOD::TrackStateValidation* msos = *(*msos_iter);
      if (!msos->trackMeasurementValidationLink().isValid()) { continue; }
      if (!(*(msos->trackMeasurementValidationLink())))      { continue; }
      if (msos->detType()==1) { // its a pixel 
        if (msos->type()==6) { continue; } // not a hole
        const xAOD::TrackMeasurementValidation* msosClus =  *(msos->trackMeasurementValidationLink());
        for (xAOD::TrackMeasurementValidationContainer::iterator clus_itr=(pixClusters)->begin(); clus_itr!=(pixClusters)->end(); ++clus_itr) {
          if ((*clus_itr)->identifier()!=(msosClus)->identifier()) { continue; }
          if ((*clus_itr)->auxdata<float>("charge")!=(msosClus)->auxdata<float>("charge")) { continue; }
          if ((*clus_itr)->auxdata<int>("layer")==0 && (*clus_itr)->auxdata<int>("bec")==0) {  // IBL

            if ((*clus_itr)->isAvailable<std::vector<int>>("rdo_phi_pixel_index")) {
              int nHits = (*clus_itr)->auxdata<std::vector<int>>("rdo_phi_pixel_index").size();
              Pix_numCluster_IBL++;
              for (int i=0; i<nHits; i++) {
                int eta = (*clus_itr)->auxdata<std::vector<int>>("rdo_eta_pixel_index")[i];
                int phi = (*clus_itr)->auxdata<std::vector<int>>("rdo_phi_pixel_index")[i];
              }
            }
            Pix_charge_IBL += (*clus_itr)->auxdata<float>("charge");
            TLorentzVector clusterTLV;
            clusterTLV.SetXYZT((*clus_itr)->globalX(),
                (*clus_itr)->globalY(),
                (*clus_itr)->globalZ(),
                0
                );
            IBLClusterEta.push_back( clusterTLV.Eta() );
            IBLClusterPhi.push_back( clusterTLV.Phi() );
            IBLClusterCharge.push_back( (*clus_itr)->auxdata<float>("charge") );
            ibl_hits++;
          }
          else if ((*clus_itr)->auxdata<int>("layer")==1 && (*clus_itr)->auxdata<int>("bec")==0) {  // b-layer
            Pix_numCluster_BLayer++;
            Pix_charge_BLayer += (*clus_itr)->auxdata<float>("charge");
            TLorentzVector clusterTLV;
            clusterTLV.SetXYZT((*clus_itr)->globalX(),
                (*clus_itr)->globalY(),
                (*clus_itr)->globalZ(),
                0
                );
            BLayerClusterEta.push_back( clusterTLV.Eta() );
            BLayerClusterPhi.push_back( clusterTLV.Phi() );
            BLayerClusterCharge.push_back( (*clus_itr)->auxdata<float>("charge") );
            blayer_hits++;
          }
          else if ((*clus_itr)->auxdata<int>("layer")==2 && (*clus_itr)->auxdata<int>("bec")==0) {  // L1
            Pix_numCluster_L1++;
            Pix_charge_L1 += (*clus_itr)->auxdata<float>("charge");
            TLorentzVector clusterTLV;
            clusterTLV.SetXYZT((*clus_itr)->globalX(),
                (*clus_itr)->globalY(),
                (*clus_itr)->globalZ(),
                0
                );
            L1ClusterEta.push_back( clusterTLV.Eta() );
            L1ClusterPhi.push_back( clusterTLV.Phi() );
            L1ClusterCharge.push_back( (*clus_itr)->auxdata<float>("charge") );
            l1_hits++;
          }
          else if ((*clus_itr)->auxdata<int>("layer")==3 && (*clus_itr)->auxdata<int>("bec")==0) {  // L2
            Pix_numCluster_L2++;
            Pix_charge_L2 += (*clus_itr)->auxdata<float>("charge");
            TLorentzVector clusterTLV;
            clusterTLV.SetXYZT((*clus_itr)->globalX(),
                (*clus_itr)->globalY(),
                (*clus_itr)->globalZ(),
                0
                );
            L2ClusterEta.push_back( clusterTLV.Eta() );
            L2ClusterPhi.push_back( clusterTLV.Phi() );
            L2ClusterCharge.push_back( (*clus_itr)->auxdata<float>("charge") );
            l2_hits++;
          }
        }
      }
      else if (msos->detType() == 2 ){
          std::cout << __FILE__ << " " << __LINE__ << std::endl;
        const xAOD::TrackMeasurementValidation* msosClus =  *(msos->trackMeasurementValidationLink());
        if ( msosClus->isAvailable<float>("charge")){
          std::cout << __FILE__ << " " << __LINE__ << std::endl;
            }else {
          std::cout << __FILE__ << " " << __LINE__ << std::endl;
            }
      }
    }
    //
    InDet_track_numberOfIBLHits         .push_back( ibl_hits    );
    InDet_track_numberOfBLayerHits      .push_back( blayer_hits );
    InDet_track_numberOfPixelL1Hits     .push_back( l1_hits     );
    InDet_track_numberOfPixelL2Hits     .push_back( l2_hits     );
    //
    InDet_pixel_IBLClustersEta          .push_back( IBLClusterEta     );
    InDet_pixel_BLayerClustersEta       .push_back( BLayerClusterEta  );
    InDet_pixel_L1ClustersEta           .push_back( L1ClusterEta      );
    InDet_pixel_L2ClustersEta           .push_back( L2ClusterEta      );
    //
    InDet_pixel_IBLClustersPhi          .push_back( IBLClusterPhi     );
    InDet_pixel_BLayerClustersPhi       .push_back( BLayerClusterPhi  );
    InDet_pixel_L1ClustersPhi           .push_back( L1ClusterPhi      );
    InDet_pixel_L2ClustersPhi           .push_back( L2ClusterPhi      );
    //
    InDet_pixel_IBLClustersCharge       .push_back( IBLClusterCharge     );
    InDet_pixel_BLayerClustersCharge    .push_back( BLayerClusterCharge  );
    InDet_pixel_L1ClustersCharge        .push_back( L1ClusterCharge      );
    InDet_pixel_L2ClustersCharge        .push_back( L2ClusterCharge      );
  }

  // ============================================  TruthParticleContainer  =============================================
  // ===================================================================================================================
  // initialized before the TruthParticleContainer loop.
  tt_truth_chargedHadron     = 0;
  Truth_part_numChargedTracks       = 0;
  Truth_part_numChargedLeptonTracks = 0;
  /// ------------  Variables will be initialized.
  bool  IsHadronicTau           = false; // tau decays hadronically
  bool  IsLeptonicTau           = false; // tau decays leptonically
  int   numCharged              = 0;     // the number of charged tracks
  bool  IsElectron              = false; // tau decays leptonically
  bool  IsMuon                  = false; // tau decays leptonically
  bool  IsTauon                 = false; // tau decays leptonically
  Int_t hasNeutorino            = 0;

  /// Truth matching
  Truth_tau_isMatchedWithReco   = false;
  Truth_tau_isHadronic          = false;
  Truth_tau_isLeptonic          = false;
  tt_truth_leptonic_electron    = false;
  tt_truth_leptonic_muon        = false;
  tt_truth_leptonic_tau         = false;

  // Pass the tau selection tool requirement 
  Reco_tau_isSelected           = false;

  Truth_track_pt                .clear();
  Truth_track_eta               .clear();
  Truth_track_phi               .clear();
  Truth_track_m                 .clear();
  Truth_track_vtx               .clear();
  Truth_track_charge            .clear();
  //
  Truth_neutrino_pdgId          .clear();
  Truth_neutrino_pt             .clear();
  Truth_neutrino_eta            .clear();
  Truth_neutrino_phi            .clear();
  /// -----------------------------------------------

  std::vector<TLorentzVector> vecElectronNeutrinoP4, vecMuNeutrinoP4, vecTauNeutrinoP4;// Neutrino TLV
  // Mathcing requiremnt;
  bool ExistTau     = false;
  int TauCount      = 0;
  const xAOD::TruthParticleContainer* truth = 0;
  ATH_CHECK( evtStore()->retrieve( truth, "TruthParticles" ));
  tt_truth_TruthParticleContainerSize = truth->size();
  
//  for(xAOD::TruthParticleContainer::const_iterator test_itr = truth->begin(); test_itr != truth->end(); test_itr++ ) {
//    const xAOD::TruthParticle* pTruth = *test_itr;
//    std::cout << "Parent = "       <<  pTruth->nParents() << ": Child  = " <<  pTruth->nChildren() << ": pdg ID = " <<  pTruth->pdgId() << std::endl;
//    if ( pTruth->hasDecayVtx() ) std::cout << "Dec vertex x = " << pTruth->decayVtx()->x() << " y = " <<  pTruth->decayVtx()->y() << " z = " << pTruth->decayVtx()->z() << std::endl;
//    if ( pTruth->hasProdVtx() )  std::cout << "Production vertex x = " << pTruth->prodVtx()->x() << " y = " <<  pTruth->prodVtx()->y() << " z = " << pTruth->prodVtx()->z() << std::endl;
//  }
//  std::cout << " ----------------- "<< std::endl;

  for(xAOD::TruthParticleContainer::const_iterator truth_itr = truth->begin(); truth_itr != truth->end(); truth_itr++ ) {
    const xAOD::TruthParticle* pTruth = *truth_itr;


    TLorentzVector TruthTLV; 
    TruthTLV.SetPtEtaPhiM( pTruth->pt(), pTruth->eta(), pTruth->phi(), pTruth->m());

    // Truth information 
    //    Truth_part_status       .push_back(pTruth->status());
    //    Truth_part_isCharged    .push_back(pTruth->isCharged());
    //    Truth_part_pdgId        .push_back(pTruth->pdgId());
    //    Truth_part_hasProdVtx   .push_back(pTruth->hasProdVtx());
    //    Truth_part_hasDecayVtx  .push_back(pTruth->hasDecayVtx());
    //    Truth_part_nParents     .push_back(pTruth->nParents());
    //    Truth_part_nChildren    .push_back(pTruth->nChildren());
    //    Truth_part_p4           .push_back(TruthTLV);


    if ( pTruth->status() == 1 && pTruth->isHadron() && pTruth->isCharged() ) Truth_part_numChargedTracks       += 1;
    if ( pTruth->status() == 1 && pTruth->isLepton() && pTruth->isCharged() ) Truth_part_numChargedLeptonTracks += 1;

    if ( !pTruth->isTau() ) continue; 
    ExistTau=true;
    TauCount++;
    Truth_tau_pdgId = pTruth->pdgId();
    /// Truth tau decay mode  //////////////////////////////////////////////
    //   - IsHadronicTau
    //   - IsLeptonicTau
    ////////////////////////////////////////////////////////////////////////
    // get vertex and check if it exists
    const xAOD::TruthVertex* xDecayVertex = pTruth->decayVtx();
    if (!xDecayVertex) continue;

    // Outgoing particles (= tau decay product)
    for ( size_t iOutgoingParticle = 0; iOutgoingParticle < xDecayVertex->nOutgoingParticles(); ++iOutgoingParticle ) {
      const xAOD::TruthParticle* xTruthDaughter = xDecayVertex->outgoingParticle(iOutgoingParticle);
      int iAbsPdgId = xTruthDaughter->absPdgId();

      if ( xTruthDaughter->isNeutrino() ) {
        hasNeutorino++;
        TLorentzVector neutrino; 
        neutrino.SetPtEtaPhiM(xTruthDaughter->pt(), xTruthDaughter->eta(), xTruthDaughter->phi(), xTruthDaughter->m());

        switch( iAbsPdgId ){
          case 12:
            vecElectronNeutrinoP4.push_back( neutrino );
            Truth_neutrino_pdgId  .push_back( xTruthDaughter->pdgId() );
            Truth_neutrino_pt     .push_back( neutrino.Pt()  );
            Truth_neutrino_eta    .push_back( neutrino.Eta() );
            Truth_neutrino_phi    .push_back( neutrino.Phi() );
            break;
          case 14:
            vecMuNeutrinoP4.push_back( neutrino );
            Truth_neutrino_pdgId  .push_back( xTruthDaughter->pdgId() );
            Truth_neutrino_pt     .push_back( neutrino.Pt()  );
            Truth_neutrino_eta    .push_back( neutrino.Eta() );
            Truth_neutrino_phi    .push_back( neutrino.Phi() );
            break;
          case 16:
            vecTauNeutrinoP4.push_back( neutrino );
            Truth_neutrino_pdgId  .push_back( xTruthDaughter->pdgId() );
            Truth_neutrino_pt     .push_back( neutrino.Pt()  );
            Truth_neutrino_eta    .push_back( neutrino.Eta() );
            Truth_neutrino_phi    .push_back( neutrino.Phi() );
            break;
        }
      }
      // Judge it as a leptonic tau
      if(xTruthDaughter->status() != 3 ){
        if ((iAbsPdgId == 11 || iAbsPdgId == 13 || iAbsPdgId == 15)) {
          IsLeptonicTau = true;
          switch( iAbsPdgId ){
            case 11: 
              IsElectron = true;
              break;
            case 13:
              IsMuon = true;
              break;
            case 15:
              IsTauon = true;
              break;
          }
        }
      }
      // only process stable particles
      //  status 1 = stable
      //  status 2 = un-stable
      if (xTruthDaughter->status() != 1 ) continue;

      // if tau decays leptonically, indicated by an electron/muon neutrino then
      // it is not a hadronic decay
      if ( xTruthDaughter->isHadron() ) {
        IsHadronicTau = true;
        if ( xTruthDaughter->isCharged() ) {
          numCharged++; 
          TLorentzVector daughterTLV;
          daughterTLV.SetPtEtaPhiM( xTruthDaughter->pt(), xTruthDaughter->eta(), xTruthDaughter->phi(), xTruthDaughter->m());
          Truth_track_pt    .push_back ( xTruthDaughter->pt() );
          Truth_track_eta   .push_back ( xTruthDaughter->eta());
          Truth_track_phi   .push_back ( xTruthDaughter->phi());
          Truth_track_m     .push_back ( xTruthDaughter->m()  );
          Truth_track_vtx   .push_back ( xTruthDaughter->prodVtx()->perp() );
          Truth_track_charge   .push_back ( xTruthDaughter->charge() );
          Truth_track_pdgId    .push_back ( xTruthDaughter->pdgId() );
        }
      }
    }// iOutgoingParticle --- * --- *--- * --- *--- * --- *--- * --- *--- * --- *--- * --- *--- * --- *--- * --- *--- * --- 

    /* Record the tau decay product information */
    Truth_dau_pdgId.clear();
    if( IsHadronicTau ){
      const xAOD::TruthVertex* DecayVertex = pTruth->decayVtx();
      for ( size_t iOutgoingParticle = 0; iOutgoingParticle < DecayVertex->nOutgoingParticles(); ++iOutgoingParticle ) {
        const xAOD::TruthParticle* xTruthDaughter = xDecayVertex->outgoingParticle(iOutgoingParticle);
        Truth_dau_pdgId .push_back(xTruthDaughter->pdgId());
      }
    }

    if ( !IsLeptonicTau && !IsHadronicTau ) continue;
    if ( !pTruth->hasDecayVtx() ) continue;
    /* After the selection, we get the truth tau without considering the decay mode. 
     * We can compare the truth tau and tauJet, namely we can calculate the matching efficiency.
     * */

    tt_truth_neutrino_pt  = 0;

    const xAOD::TruthVertex* pDecayVtx = pTruth->decayVtx();
    const xAOD::TruthVertex* pProdVtx  = pTruth->prodVtx();
    if( IsHadronicTau ){
      Truth_tau_isHadronic = true; 
      // For hadronic truth tau, the associated tracks should be stored.
      Truth_tau_numTracks = numCharged;

      // IsHadronicTau
      if ( hasNeutorino == 1 ) {
        for ( unsigned int i=0; i<vecElectronNeutrinoP4.size(); i++){
          TruthTLV -= vecElectronNeutrinoP4.at(i);
          tt_truth_neutrino_pt = vecElectronNeutrinoP4.at(i).Pt();
        }
        for ( unsigned int i=0; i<vecMuNeutrinoP4.size(); i++){
          TruthTLV -= vecMuNeutrinoP4.at(i);
          tt_truth_neutrino_pt = vecMuNeutrinoP4.at(i).Pt();
        }
        for ( unsigned int i=0; i<vecTauNeutrinoP4.size(); i++){
          TruthTLV -= vecTauNeutrinoP4.at(i);
          tt_truth_neutrino_pt = vecTauNeutrinoP4.at(i).Pt();
        }
      }
    }
    // IsLeptonicTau 
    else if ( IsLeptonicTau ){
      Truth_tau_isLeptonic = true;
      if( IsElectron ) tt_truth_leptonic_electron = true;
      else if( IsMuon )     tt_truth_leptonic_muon = true;
      else if( IsTauon )    tt_truth_leptonic_tau = true;

      if ( hasNeutorino == 2 ){
        for ( unsigned int i=0; i<vecElectronNeutrinoP4.size(); i++){
          TruthTLV -= vecElectronNeutrinoP4.at(i);
          tt_truth_neutrino_pt += vecElectronNeutrinoP4.at(i).Pt();
        }
        for ( unsigned int i=0; i<vecMuNeutrinoP4.size(); i++){
          TruthTLV -= vecMuNeutrinoP4.at(i);
          tt_truth_neutrino_pt += vecMuNeutrinoP4.at(i).Pt();
        }
        for ( unsigned int i=0; i<vecTauNeutrinoP4.size(); i++){
          TruthTLV -= vecTauNeutrinoP4.at(i);
          tt_truth_neutrino_pt += vecTauNeutrinoP4.at(i).Pt();
        }
      } 
    }

    // Store the truth information 
    Truth_tau_pt           = TruthTLV.Pt();
    Truth_tau_eta          = pTruth->eta();
    Truth_tau_phi          = pTruth->phi();
    Truth_tau_e            = pTruth->e();
    Truth_tau_m            = pTruth->m();
    Truth_tau_decayRad     = pDecayVtx->perp(); // = sqrt(x*x+y*y)
    // Production vertex information 
    Truth_prodVtx_x         = pProdVtx->x();
    Truth_prodVtx_y         = pProdVtx->y();
    Truth_prodVtx_z         = pProdVtx->z();
    Truth_prodVtx_perp      = pProdVtx->perp();
    Truth_prodVtx_eta       = pProdVtx->eta();
    Truth_prodVtx_phi       = pProdVtx->phi();
    Truth_prodVtx_t         = pProdVtx->t();
    // Decay vertex information 
    Truth_decVtx_x         = pDecayVtx->x();
    Truth_decVtx_y         = pDecayVtx->y();
    Truth_decVtx_z         = pDecayVtx->z();
    Truth_decVtx_perp      = pDecayVtx->perp();
    Truth_decVtx_eta       = pDecayVtx->eta();
    Truth_decVtx_phi       = pDecayVtx->phi();
    Truth_decVtx_t         = pDecayVtx->t();

    ///////////////////////////////////////////////////////////////////////////////////
    //
    //  The above code uses only "truth" information, nemaely TruthParticles container.
    //  We'll access the reconstructed tau objects using TauJet container.
    //  This is because we have to consider if the truth tau is reconstructed as tau.
    //   --> Compare a truth tau with reconstructed jets
    //
    ///////////////////////////////////////////////////////////////////////////////////
    const xAOD::TauJetContainer* tauJetInTruthLoop = 0;
    const xAOD::TauJet* xTauJet = NULL ;   
    ATH_CHECK( evtStore()->retrieve( tauJetInTruthLoop ) );

    Double_t dPtMax=0;
    for( auto pTau: *tauJetInTruthLoop) {
      /// truth-pt, eta, phi
      /// Find reco-tau matching
      if ( pTau->p4().DeltaR(TruthTLV) <= 0.2 ){
        if ( TruthTLV.Pt() < dPtMax ) continue;
        dPtMax = TruthTLV.Pt();

        Truth_tau_isMatchedWithReco           = true;
        Reco_tau_pt               = pTau->pt();
        /// truth-pt, eta, phi
        Reco_tau_eta              = pTau->eta();
        Reco_tau_phi              = pTau->phi();
        Reco_tau_e                = pTau->e();
        Reco_tau_numTracks        = pTau->nTracks();
        xTauJet                   = pTau;

        if(TauSelTool->accept( *pTau )) Reco_tau_isSelected = true;

      }// DeltaR
    }// TauJet

    Reco_track_pt                               .clear();
    Reco_track_eta                              .clear();
    Reco_track_phi                              .clear();
    Reco_track_e                                .clear();
    Reco_track_m                                .clear();
    Reco_track_numberOfPixelHits .clear();
    // The number of innder detector hits (summaryValue)
    const xAOD::TauJetContainer* tauJetContainer = 0;
    const xAOD::TauJet* pTauJet = NULL ; 
    ATH_CHECK( evtStore()->retrieve( tauJetContainer ) );
    if ( tauJetContainer->size() > 0 ){
      for ( auto pTau : *tauJetContainer ){
        pTauJet = pTau;
        for( const xAOD::TauTrack* tauTrack : pTauJet->tracks() ){ 
          const xAOD::TrackParticle* trk = tauTrack->track();

          Reco_track_pt     .push_back( trk->pt() );
          Reco_track_eta    .push_back( trk->eta() );
          Reco_track_phi    .push_back( trk->phi() );
          Reco_track_e      .push_back( trk->e() );
          Reco_track_m      .push_back( trk->m() );

          uint8_t numberOfSCTHits   = 245;
          uint8_t numberOfPixelHits = 245;
          if (trk -> summaryValue(numberOfPixelHits, xAOD::numberOfPixelHits) ) Reco_track_numberOfPixelHits .push_back( numberOfPixelHits );
        }
      }
    }

    if ( !Truth_tau_isMatchedWithReco ) continue;  // ----* ----* ----* ----* ----* ----* ----* ----* ----* ----* ----* ----* 
    // After the selection, we will get the truth tau which matched with the recon tau.

    /////////////////////////////////////////////////////////
    //
    //  The inner detector information 
    //
    /////////////////////////////////////////////////////////
    // Clear each vector branch
    // Pixel 
    tt_numberOfContribPixelLayers             .clear();
    tt_numberOfPixelHits                      .clear();
    tt_numberOfPixelHoles                     .clear();
    tt_expectInnermostPixelLayerHit           .clear();
    tt_numberOfInnermostPixelLayerHits        .clear();
    tt_numberOfNextToInnermostPixelLayerHits  .clear();
    tt_numberOfGangedPixels                   .clear();
    tt_numberOfPixelDeadSensors               .clear();
    tt_numberOfPixelSharedHits                .clear();
    tt_numberOfPixelSplitHits                 .clear();
    tt_numberOfPixelOutliers                  .clear();
    tt_numberOfPixelSpoiltHits                .clear();
    // Pixel B-Layer
    tt_numberOfBLayerHits                     .clear(); 
    tt_numberOfBLayerSharedHits               .clear(); 
    tt_numberOfBLayerOutliers                 .clear(); 
    tt_numberOfBLayerSplitHits                .clear(); 
    tt_expectBLayerHit                        .clear(); 
    // Precision information
    tt_numberOfPrecisionLayers                .clear();
    tt_numberOfPrecisionHoleLayers            .clear();
    tt_numberOfPhiLayers                      .clear();
    tt_numberOfPhiHoleLayers                  .clear();
    tt_numberOfTriggerEtaLayers               .clear();
    tt_numberOfTriggerEtaHoleLayers           .clear();
    tt_numberOfGoodPrecisionLayers            .clear();
    tt_numberOfOutliersOnTrack                .clear();
    //
    tt_numberOfSCTHits                        .clear(); 
    tt_numberOfTRTHits                        .clear(); 
    tt_numberOfSCTSharedHits                  .clear(); 
    tt_numberOfTRTSharedHits                  .clear(); 
    tt_numberOfSCTHoles                       .clear(); 
    tt_numberOfSCTDoubleHoles                 .clear(); 
    tt_numberOfTRTHoles                       .clear(); 
    tt_numberOfSCTOutliers                    .clear(); 
    tt_numberOfTRTOutliers                    .clear(); 
    tt_numberOfSCTSpoiltHits                  .clear(); 
    tt_numberOfGangedFlaggedFakes             .clear(); 
    tt_numberOfSCTDeadSensors                 .clear(); 
    tt_numberOfTRTDeadStraws                  .clear(); 
    tt_pixeldEdx                              .clear(); 
    //
    // Reco-tau track info
    Reco_track_d0                             .clear();
    Reco_track_z0                             .clear();

    for( const xAOD::TauTrack* tauTrack : xTauJet->tracks() ){ 
      const xAOD::TrackParticle* trk = tauTrack->track();

      Reco_track_d0.push_back( trk->d0() );
      Reco_track_z0.push_back( trk->z0() );

      //xAOD::TauJetParameters::TauTrackFlag flag = xAOD::TauJetParameters::TauTrackFlag::classifiedCharged; // classified as charged track from tau decay
      //xAOD::TauTrack::TrackFlagType mask = 1 << flag;
      //if( !tauTrack->flagWithMask(mask) ) continue;

      //Track Summary
      // Pixel
      uint8_t numberOfContribPixelLayers            = 245;
      uint8_t numberOfPixelHits                     = 245;
      uint8_t numberOfPixelSharedHits               = 245;
      uint8_t numberOfPixelHoles                    = 245;
      uint8_t numberOfPixelOutliers                 = 245;
      uint8_t numberOfPixelSpoiltHits               = 245;
      uint8_t numberOfPixelSplitHits                = 245;
      uint8_t expectInnermostPixelLayerHit          = 245;
      uint8_t numberOfInnermostPixelLayerHits       = 245;
      uint8_t numberOfNextToInnermostPixelLayerHits = 245;
      uint8_t numberOfGangedPixels                  = 245;
      uint8_t numberOfPixelDeadSensors              = 245;
      // Pixel B-Layer
      uint8_t numberOfBLayerHits                    = 245;
      uint8_t numberOfBLayerSharedHits              = 245;
      uint8_t numberOfBLayerOutliers                = 245;
      uint8_t numberOfBLayerSplitHits               = 245;
      uint8_t expectBLayerHit                       = 245;
      // Precision information
      uint8_t numberOfPrecisionLayers               = 245;
      uint8_t numberOfPrecisionHoleLayers           = 245;
      uint8_t numberOfPhiLayers                     = 245;
      uint8_t numberOfPhiHoleLayers                 = 245;
      uint8_t numberOfTriggerEtaLayers              = 245;
      uint8_t numberOfTriggerEtaHoleLayers          = 245;
      uint8_t numberOfGoodPrecisionLayers           = 245;
      uint8_t numberOfOutliersOnTrack               = 245;
      //
      uint8_t numberOfSCTHits                       = 245;
      uint8_t numberOfTRTHits                       = 245;
      uint8_t numberOfSCTSharedHits                 = 245;
      uint8_t numberOfTRTSharedHits                 = 245;
      uint8_t numberOfSCTHoles                      = 245;
      uint8_t numberOfSCTDoubleHoles                = 245;
      uint8_t numberOfTRTHoles                      = 245;
      uint8_t numberOfSCTOutliers                   = 245;
      uint8_t numberOfTRTOutliers                   = 245;
      uint8_t numberOfSCTSpoiltHits                 = 245;
      uint8_t numberOfGangedFlaggedFakes            = 245;
      uint8_t numberOfSCTDeadSensors                = 245;
      uint8_t numberOfTRTDeadStraws                 = 245;
      float   pixeldEdx                             = 245;

      // Pixel
      if ( trk->summaryValue(numberOfContribPixelLayers,            xAOD::numberOfContribPixelLayers) ) tt_numberOfContribPixelLayers .push_back(static_cast<unsigned int>(numberOfContribPixelLayers));    
      if ( trk->summaryValue(numberOfPixelHits,                     xAOD::numberOfPixelHits) )          tt_numberOfPixelHits          .push_back(static_cast<unsigned int>(numberOfPixelHits)); 
      if ( trk->summaryValue(numberOfPixelSharedHits,               xAOD::numberOfPixelSharedHits))     tt_numberOfPixelSharedHits    .push_back(static_cast<unsigned int>(numberOfPixelSharedHits   ));    
      if ( trk->summaryValue(numberOfPixelHoles,                    xAOD::numberOfPixelHoles))          tt_numberOfPixelHoles         .push_back(static_cast<unsigned int>(numberOfPixelHoles        ));    
      if ( trk->summaryValue(numberOfPixelOutliers,                 xAOD::numberOfPixelOutliers))       tt_numberOfPixelOutliers      .push_back(static_cast<unsigned int>(numberOfPixelOutliers     ));    
      if ( trk->summaryValue(numberOfPixelSpoiltHits,               xAOD::numberOfPixelSpoiltHits))     tt_numberOfPixelSpoiltHits    .push_back(static_cast<unsigned int>(numberOfPixelSpoiltHits   ));    
      if ( trk->summaryValue(numberOfPixelSplitHits,                xAOD::numberOfPixelSplitHits))      tt_numberOfPixelSplitHits     .push_back(static_cast<unsigned int>(numberOfPixelSplitHits   ));    
      if ( trk->summaryValue(expectInnermostPixelLayerHit,          xAOD::expectInnermostPixelLayerHit)) tt_expectInnermostPixelLayerHit      .push_back(static_cast<unsigned int>(expectInnermostPixelLayerHit ));    
      if ( trk->summaryValue(numberOfInnermostPixelLayerHits,       xAOD::numberOfInnermostPixelLayerHits)) tt_numberOfInnermostPixelLayerHits      .push_back(static_cast<unsigned int>(numberOfInnermostPixelLayerHits ));    
      if ( trk->summaryValue(numberOfNextToInnermostPixelLayerHits, xAOD::numberOfNextToInnermostPixelLayerHits)) tt_numberOfNextToInnermostPixelLayerHits   .push_back(static_cast<unsigned int>(numberOfNextToInnermostPixelLayerHits ));    
      if ( trk->summaryValue(numberOfGangedPixels, xAOD::numberOfGangedPixels)) tt_numberOfGangedPixels   .push_back(static_cast<unsigned int>(numberOfGangedPixels ));    
      if ( trk->summaryValue(numberOfPixelDeadSensors, xAOD::numberOfPixelDeadSensors)) tt_numberOfPixelDeadSensors   .push_back(static_cast<unsigned int>(numberOfPixelDeadSensors ));    
      //
      // Pixel B-Layer
      if ( trk->summaryValue(numberOfBLayerHits,         xAOD::numberOfBLayerHits))          tt_numberOfBLayerHits         .push_back(static_cast<unsigned int>(numberOfBLayerHits        ));    
      if ( trk->summaryValue(numberOfBLayerSharedHits,   xAOD::numberOfBLayerSharedHits))    tt_numberOfBLayerSharedHits   .push_back(static_cast<unsigned int>(numberOfBLayerSharedHits  ));    
      if ( trk->summaryValue(numberOfBLayerOutliers,     xAOD::numberOfBLayerOutliers))      tt_numberOfBLayerOutliers     .push_back(static_cast<unsigned int>(numberOfBLayerOutliers    ));    
      if ( trk->summaryValue(numberOfBLayerSplitHits,    xAOD::numberOfBLayerSplitHits))     tt_numberOfBLayerSplitHits    .push_back(static_cast<unsigned int>(numberOfBLayerSplitHits    ));    
      if ( trk->summaryValue(expectBLayerHit,            xAOD::expectBLayerHit))             tt_expectBLayerHit            .push_back(static_cast<unsigned int>(expectBLayerHit    ));    
      //
      // Precision information
      if ( trk->summaryValue(numberOfPrecisionLayers                , xAOD::numberOfPrecisionLayers     ))  tt_numberOfPrecisionLayers       .push_back(static_cast<unsigned int>(numberOfPrecisionLayers     ));
      if ( trk->summaryValue(numberOfPrecisionHoleLayers            , xAOD::numberOfPrecisionHoleLayers ))  tt_numberOfPrecisionHoleLayers   .push_back(static_cast<unsigned int>(numberOfPrecisionHoleLayers ));
      if ( trk->summaryValue(numberOfPhiLayers                      , xAOD::numberOfPhiLayers           ))  tt_numberOfPhiLayers             .push_back(static_cast<unsigned int>(numberOfPhiLayers           ));
      if ( trk->summaryValue(numberOfPhiHoleLayers                  , xAOD::numberOfPhiHoleLayers       ))  tt_numberOfPhiHoleLayers         .push_back(static_cast<unsigned int>(numberOfPhiHoleLayers       ));
      if ( trk->summaryValue(numberOfTriggerEtaLayers               , xAOD::numberOfTriggerEtaLayers    ))  tt_numberOfTriggerEtaLayers      .push_back(static_cast<unsigned int>(numberOfTriggerEtaLayers    ));
      if ( trk->summaryValue(numberOfTriggerEtaHoleLayers           , xAOD::numberOfTriggerEtaHoleLayers))  tt_numberOfTriggerEtaHoleLayers  .push_back(static_cast<unsigned int>(numberOfTriggerEtaHoleLayers));
      if ( trk->summaryValue(numberOfGoodPrecisionLayers            , xAOD::numberOfGoodPrecisionLayers ))  tt_numberOfGoodPrecisionLayers   .push_back(static_cast<unsigned int>(numberOfGoodPrecisionLayers ));
      if ( trk->summaryValue(numberOfOutliersOnTrack                , xAOD::numberOfOutliersOnTrack     ))  tt_numberOfOutliersOnTrack       .push_back(static_cast<unsigned int>(numberOfOutliersOnTrack     ));
      // 
      if ( trk->summaryValue(numberOfSCTHits,            xAOD::numberOfSCTHits))             tt_numberOfSCTHits            .push_back(static_cast<unsigned int>(numberOfSCTHits           ));    
      if ( trk->summaryValue(numberOfTRTHits,            xAOD::numberOfTRTHits))             tt_numberOfTRTHits            .push_back(static_cast<unsigned int>(numberOfTRTHits           ));    
      if ( trk->summaryValue(numberOfSCTSharedHits,      xAOD::numberOfSCTSharedHits))       tt_numberOfSCTSharedHits      .push_back(static_cast<unsigned int>(numberOfSCTSharedHits     ));    
      if ( trk->summaryValue(numberOfTRTSharedHits,      xAOD::numberOfTRTSharedHits))       tt_numberOfTRTSharedHits      .push_back(static_cast<unsigned int>(numberOfTRTSharedHits     ));    
      if ( trk->summaryValue(numberOfSCTHoles,           xAOD::numberOfSCTHoles))            tt_numberOfSCTHoles           .push_back(static_cast<unsigned int>(numberOfSCTHoles          ));    
      if ( trk->summaryValue(numberOfSCTDoubleHoles,     xAOD::numberOfSCTDoubleHoles))      tt_numberOfSCTDoubleHoles     .push_back(static_cast<unsigned int>(numberOfSCTDoubleHoles    ));    
      if ( trk->summaryValue(numberOfTRTHoles,           xAOD::numberOfTRTHoles))            tt_numberOfTRTHoles           .push_back(static_cast<unsigned int>(numberOfTRTHoles          ));    
      if ( trk->summaryValue(numberOfSCTOutliers,        xAOD::numberOfSCTOutliers))         tt_numberOfSCTOutliers        .push_back(static_cast<unsigned int>(numberOfSCTOutliers       ));    
      if ( trk->summaryValue(numberOfTRTOutliers,        xAOD::numberOfTRTOutliers))         tt_numberOfTRTOutliers        .push_back(static_cast<unsigned int>(numberOfTRTOutliers       ));    
      if ( trk->summaryValue(numberOfSCTSpoiltHits,      xAOD::numberOfSCTSpoiltHits))       tt_numberOfSCTSpoiltHits      .push_back(static_cast<unsigned int>(numberOfSCTSpoiltHits     ));    
    }// TauTrack

    // ----------------------------------------------
    //
    //  Raw pixel hit information will be handled from here.
    //
    // ----------------------------------------------

    // truth matched tau jet
    Reco_track_numberOfInnermostPixelLayerSensors         .clear();
    Reco_track_numberOfNextToInnermostPixelLayerSensors   .clear();
    Reco_track_numberOfPixelL1Sensors                     .clear();
    Reco_track_numberOfPixelL2Sensors                     .clear();

    Reco_track_numberOfInnermostPixelHits          .clear(); 
    Reco_track_numberOfNextToInnermostPixelHits    .clear();
    Reco_track_numberOfPixelL1Hits                 .clear();
    Reco_track_numberOfPixelL2Hits                 .clear();

    /*
     * Removed the lines here.
     * */

  }// TruthParticleContainer

  m_trueTauTree->Fill();

  if(!ExistTau){
    ATH_MSG_INFO(" =================== truth->size() = " << truth->size());
    for(xAOD::TruthParticleContainer::const_iterator truth_itr = truth->begin(); truth_itr != truth->end(); truth_itr++ ) {
      std::cout << "PDG ID = ["  << (*truth_itr)->pdgId() << "] ";
    }
    std::cout << std::endl;
  }
  if( TauCount != 1 ){
    ATH_MSG_INFO("The number of tuth taus is = " << TauCount);
    ATH_MSG_INFO(" =================== truth->size() = " << truth->size());
    for(xAOD::TruthParticleContainer::const_iterator truth_itr = truth->begin(); truth_itr != truth->end(); truth_itr++ ) {
      std::cout << "PDG ID = ["  << (*truth_itr)->pdgId() << "] ";
    }
    std::cout << std::endl;
  }
  return StatusCode::SUCCESS;
}


StatusCode MyxAODAnalysis :: finalize ()
{
  // This method is the mirror image of initialize(), meaning it gets
  // called after the last event has been processed on the worker node
  // and allows you to finish up any objects you created in
  // initialize() before they are written to disk.  This is actually
  // fairly rare, since this happens separately for each worker node.
  // Most of the time you want to do your post-processing on the
  // submission node after all your histogram outputs have been
  // merged
  //
  ATH_MSG_INFO("Enter finalize()");
  m_trueTauTree->Write();
  MyxAODAnalysis :: Write();
  ATH_MSG_INFO("Exit  finalize()");
  return StatusCode::SUCCESS;
}

void MyxAODAnalysis :: InitSkim()
{
  InDetTrackParticles_d0                                             = 0;
  InDetTrackParticles_z0                                             = 0;
  InDetTrackParticles_phi                                            = 0;
  InDetTrackParticles_theta                                          = 0;
  InDetTrackParticles_qOverP                                         = 0;
  // InDetTrackParticles_definingParametersCovMatrix                 = 0;
  InDetTrackParticles_vx                                             = 0;
  InDetTrackParticles_vy                                             = 0;
  InDetTrackParticles_vz                                             = 0;
  InDetTrackParticles_radiusOfFirstHit                               = 0;
  InDetTrackParticles_identifierOfFirstHit                           = 0;
  InDetTrackParticles_beamlineTiltX                                  = 0;
  InDetTrackParticles_beamlineTiltY                                  = 0;
  InDetTrackParticles_hitPattern                                     = 0;
  InDetTrackParticles_chiSquared                                     = 0;
  InDetTrackParticles_numberDoF                                      = 0;
  InDetTrackParticles_trackFitter                                    = 0;
  InDetTrackParticles_particleHypothesis                             = 0;
  InDetTrackParticles_trackProperties                                = 0;
  InDetTrackParticles_patternRecoInfo                                = 0;
  InDetTrackParticles_numberOfContribPixelLayers                     = 0;
  InDetTrackParticles_numberOfInnermostPixelLayerHits                = 0;
  InDetTrackParticles_numberOfInnermostPixelLayerOutliers            = 0;
  InDetTrackParticles_numberOfInnermostPixelLayerSharedHits          = 0;
  InDetTrackParticles_numberOfInnermostPixelLayerSplitHits           = 0;
  InDetTrackParticles_expectInnermostPixelLayerHit                   = 0;
  InDetTrackParticles_numberOfNextToInnermostPixelLayerHits          = 0;
  InDetTrackParticles_numberOfNextToInnermostPixelLayerOutliers      = 0;
  InDetTrackParticles_numberOfNextToInnermostPixelLayerSharedHits    = 0;
  InDetTrackParticles_numberOfNextToInnermostPixelLayerSplitHits     = 0;
  InDetTrackParticles_expectNextToInnermostPixelLayerHit             = 0;
  InDetTrackParticles_numberOfPixelHits                              = 0;
  InDetTrackParticles_numberOfPixelOutliers                          = 0;
  InDetTrackParticles_numberOfPixelHoles                             = 0;
  InDetTrackParticles_numberOfPixelSharedHits                        = 0;
  InDetTrackParticles_numberOfPixelSplitHits                         = 0;
  InDetTrackParticles_numberOfGangedPixels                           = 0;
  InDetTrackParticles_numberOfGangedFlaggedFakes                     = 0;
  InDetTrackParticles_numberOfPixelDeadSensors                       = 0;
  InDetTrackParticles_numberOfPixelSpoiltHits                        = 0;
  InDetTrackParticles_numberOfDBMHits                                = 0;
  InDetTrackParticles_numberOfSCTHits                                = 0;
  InDetTrackParticles_numberOfSCTOutliers                            = 0;
  InDetTrackParticles_numberOfSCTHoles                               = 0;
  InDetTrackParticles_numberOfSCTDoubleHoles                         = 0;
  InDetTrackParticles_numberOfSCTSharedHits                          = 0;
  InDetTrackParticles_numberOfSCTDeadSensors                         = 0;
  InDetTrackParticles_numberOfSCTSpoiltHits                          = 0;
  InDetTrackParticles_numberOfTRTHits                                = 0;
  InDetTrackParticles_numberOfTRTOutliers                            = 0;
  InDetTrackParticles_numberOfTRTHoles                               = 0;
  InDetTrackParticles_numberOfTRTHighThresholdHits                   = 0;
  InDetTrackParticles_numberOfTRTHighThresholdHitsTotal              = 0;
  InDetTrackParticles_numberOfTRTHighThresholdOutliers               = 0;
  InDetTrackParticles_numberOfTRTDeadStraws                          = 0;
  InDetTrackParticles_numberOfTRTTubeHits                            = 0;
  InDetTrackParticles_numberOfTRTXenonHits                           = 0;
  InDetTrackParticles_numberOfTRTSharedHits                          = 0;
  InDetTrackParticles_numberOfPrecisionLayers                        = 0;
  InDetTrackParticles_numberOfPrecisionHoleLayers                    = 0;
  InDetTrackParticles_numberOfPhiLayers                              = 0;
  InDetTrackParticles_numberOfPhiHoleLayers                          = 0;
  InDetTrackParticles_numberOfTriggerEtaLayers                       = 0;
  InDetTrackParticles_numberOfTriggerEtaHoleLayers                   = 0;
  InDetTrackParticles_numberOfOutliersOnTrack                        = 0;
  InDetTrackParticles_standardDeviationOfChi2OS                      = 0;
  InDetTrackParticles_eProbabilityComb                               = 0;
  InDetTrackParticles_eProbabilityHT                                 = 0;
  InDetTrackParticles_pixeldEdx                                      = 0;
  InDetTrackParticles_numberOfUsedHitsdEdx                           = 0;
  InDetTrackParticles_numberOfIBLOverflowsdEdx                       = 0;
  //InDetTrackParticles_vertexLink                                   = 0;
  InDetTrackParticles_TRTTrackOccupancy                              = 0;
  //InDetTrackParticles_TrkIBLX                                      = 0;
  InDetTrackParticles_TRTdEdxUsedHits                                = 0;
  InDetTrackParticles_TrkIBLY                                        = 0;
  InDetTrackParticles_TrkIBLZ                                        = 0;
  InDetTrackParticles_TRTdEdx                                        = 0;
  InDetTrackParticles_TrkBLX                                         = 0;
  InDetTrackParticles_TrkBLY                                         = 0;
  InDetTrackParticles_TrkBLZ                                         = 0;
  InDetTrackParticles_nBC_meas                                       = 0;
  InDetTrackParticles_TrkL1X                                         = 0;
  InDetTrackParticles_TrkL1Y                                         = 0;
  InDetTrackParticles_TrkL2X                                         = 0;
  InDetTrackParticles_truthMatchProbability                          = 0;
  InDetTrackParticles_TrkL2Y                                         = 0;
  //InDetTrackParticles_truthType                                    = 0;
  //InDetTrackParticles_truthOrigin                                  = 0;
  InDetTrackParticles_TrkL2Z                                         = 0;
  //InDetTrackParticles_measurement_region                           = 0;
  //InDetTrackParticles_measurement_det                              = 0;
  //InDetTrackParticles_measurement_iLayer                           = 0;
  //InDetTrackParticles_hitResiduals_residualLocX                    = 0;
  //InDetTrackParticles_hitResiduals_pullLocX                        = 0;
  //InDetTrackParticles_hitResiduals_residualLocY                    = 0;
  //InDetTrackParticles_hitResiduals_pullLocY                        = 0;
  //InDetTrackParticles_hitResiduals_phiWidth                        = 0;
  //InDetTrackParticles_hitResiduals_etaWidth                        = 0;
  //InDetTrackParticles_measurement_type                             = 0;
  InDetTrackParticles_d0err                                          = 0;
  InDetTrackParticles_z0err                                          = 0;
  InDetTrackParticles_phierr                                         = 0;
  InDetTrackParticles_thetaerr                                       = 0;
  InDetTrackParticles_qopterr                                        = 0;
  //InDetTrackParticles_msosLink                                     = 0;
  //InDetTrackParticles_caloExt_Decorated                            = 0;
  //InDetTrackParticles_caloExt_eta                                  = 0;
  //InDetTrackParticles_caloExt_phi                                  = 0;
  m_InDetTrackParticles->Branch( "d0", &InDetTrackParticles_d0                                         );
  m_InDetTrackParticles->Branch( "z0", &InDetTrackParticles_z0                                         );
  m_InDetTrackParticles->Branch( "phi", &InDetTrackParticles_phi                                        );
  m_InDetTrackParticles->Branch( "theta", &InDetTrackParticles_theta                                      );
  m_InDetTrackParticles->Branch( "qOverP", &InDetTrackParticles_qOverP                                     );
  //m_InDetTrackParticles->Branch( "definingParametersCovMatrix", &InDetTrackParticles_definingParametersCovMatrix             );
  m_InDetTrackParticles->Branch( "vx", &InDetTrackParticles_vx                                         );
  m_InDetTrackParticles->Branch( "vy", &InDetTrackParticles_vy                                         );
  m_InDetTrackParticles->Branch( "vz", &InDetTrackParticles_vz                                         );
  m_InDetTrackParticles->Branch( "radiusOfFirstHit", &InDetTrackParticles_radiusOfFirstHit                           );
  m_InDetTrackParticles->Branch( "identifierOfFirstHit", &InDetTrackParticles_identifierOfFirstHit                       );
  m_InDetTrackParticles->Branch( "beamlineTiltX", &InDetTrackParticles_beamlineTiltX                              );
  m_InDetTrackParticles->Branch( "beamlineTiltY", &InDetTrackParticles_beamlineTiltY                              );
  m_InDetTrackParticles->Branch( "hitPattern", &InDetTrackParticles_hitPattern                                 );
  m_InDetTrackParticles->Branch( "chiSquared", &InDetTrackParticles_chiSquared                                 );
  m_InDetTrackParticles->Branch( "numberDoF", &InDetTrackParticles_numberDoF                                  );
  m_InDetTrackParticles->Branch( "trackFitter", &InDetTrackParticles_trackFitter                                );
  m_InDetTrackParticles->Branch( "particleHypothesis", &InDetTrackParticles_particleHypothesis                         );
  m_InDetTrackParticles->Branch( "trackProperties", &InDetTrackParticles_trackProperties                            );
  m_InDetTrackParticles->Branch( "patternRecoInfo", &InDetTrackParticles_patternRecoInfo                            );
  m_InDetTrackParticles->Branch( "numberOfContribPixelLayers", &InDetTrackParticles_numberOfContribPixelLayers                 );
  m_InDetTrackParticles->Branch( "numberOfInnermostPixelLayerHits", &InDetTrackParticles_numberOfInnermostPixelLayerHits            );
  m_InDetTrackParticles->Branch( "numberOfInnermostPixelLayerOutliers", &InDetTrackParticles_numberOfInnermostPixelLayerOutliers        );
  m_InDetTrackParticles->Branch( "numberOfInnermostPixelLayerSharedHits", &InDetTrackParticles_numberOfInnermostPixelLayerSharedHits      );
  m_InDetTrackParticles->Branch( "numberOfInnermostPixelLayerSplitHits", &InDetTrackParticles_numberOfInnermostPixelLayerSplitHits       );
  m_InDetTrackParticles->Branch( "expectInnermostPixelLayerHit", &InDetTrackParticles_expectInnermostPixelLayerHit               );
  m_InDetTrackParticles->Branch( "numberOfNextToInnermostPixelLayerHits", &InDetTrackParticles_numberOfNextToInnermostPixelLayerHits      );
  m_InDetTrackParticles->Branch( "numberOfNextToInnermostPixelLayerOutliers", &InDetTrackParticles_numberOfNextToInnermostPixelLayerOutliers  );
  m_InDetTrackParticles->Branch( "numberOfNextToInnermostPixelLayerSharedHits", &InDetTrackParticles_numberOfNextToInnermostPixelLayerSharedHits);
  m_InDetTrackParticles->Branch( "numberOfNextToInnermostPixelLayerSplitHits", &InDetTrackParticles_numberOfNextToInnermostPixelLayerSplitHits );
  m_InDetTrackParticles->Branch( "expectNextToInnermostPixelLayerHit", &InDetTrackParticles_expectNextToInnermostPixelLayerHit         );
  m_InDetTrackParticles->Branch( "numberOfPixelHits", &InDetTrackParticles_numberOfPixelHits                          );
  m_InDetTrackParticles->Branch( "numberOfPixelOutliers", &InDetTrackParticles_numberOfPixelOutliers                      );
  m_InDetTrackParticles->Branch( "numberOfPixelHoles", &InDetTrackParticles_numberOfPixelHoles                         );
  m_InDetTrackParticles->Branch( "numberOfPixelSharedHits", &InDetTrackParticles_numberOfPixelSharedHits                    );
  m_InDetTrackParticles->Branch( "numberOfPixelSplitHits", &InDetTrackParticles_numberOfPixelSplitHits                     );
  m_InDetTrackParticles->Branch( "numberOfGangedPixels", &InDetTrackParticles_numberOfGangedPixels                       );
  m_InDetTrackParticles->Branch( "numberOfGangedFlaggedFakes", &InDetTrackParticles_numberOfGangedFlaggedFakes                 );
  m_InDetTrackParticles->Branch( "numberOfPixelDeadSensors", &InDetTrackParticles_numberOfPixelDeadSensors                   );
  m_InDetTrackParticles->Branch( "numberOfPixelSpoiltHits", &InDetTrackParticles_numberOfPixelSpoiltHits                    );
  m_InDetTrackParticles->Branch( "numberOfDBMHits", &InDetTrackParticles_numberOfDBMHits                            );
  m_InDetTrackParticles->Branch( "numberOfSCTHits", &InDetTrackParticles_numberOfSCTHits                            );
  m_InDetTrackParticles->Branch( "numberOfSCTOutliers", &InDetTrackParticles_numberOfSCTOutliers                        );
  m_InDetTrackParticles->Branch( "numberOfSCTHoles", &InDetTrackParticles_numberOfSCTHoles                           );
  m_InDetTrackParticles->Branch( "numberOfSCTDoubleHoles", &InDetTrackParticles_numberOfSCTDoubleHoles                     );
  m_InDetTrackParticles->Branch( "numberOfSCTSharedHits", &InDetTrackParticles_numberOfSCTSharedHits                      );
  m_InDetTrackParticles->Branch( "numberOfSCTDeadSensors", &InDetTrackParticles_numberOfSCTDeadSensors                     );
  m_InDetTrackParticles->Branch( "numberOfSCTSpoiltHits", &InDetTrackParticles_numberOfSCTSpoiltHits                      );
  m_InDetTrackParticles->Branch( "numberOfTRTHits", &InDetTrackParticles_numberOfTRTHits                            );
  m_InDetTrackParticles->Branch( "numberOfTRTOutliers", &InDetTrackParticles_numberOfTRTOutliers                        );
  m_InDetTrackParticles->Branch( "numberOfTRTHoles", &InDetTrackParticles_numberOfTRTHoles                           );
  m_InDetTrackParticles->Branch( "numberOfTRTHighThresholdHits", &InDetTrackParticles_numberOfTRTHighThresholdHits               );
  m_InDetTrackParticles->Branch( "numberOfTRTHighThresholdHitsTotal", &InDetTrackParticles_numberOfTRTHighThresholdHitsTotal          );
  m_InDetTrackParticles->Branch( "numberOfTRTHighThresholdOutliers", &InDetTrackParticles_numberOfTRTHighThresholdOutliers           );
  m_InDetTrackParticles->Branch( "numberOfTRTDeadStraws", &InDetTrackParticles_numberOfTRTDeadStraws                      );
  m_InDetTrackParticles->Branch( "numberOfTRTTubeHits", &InDetTrackParticles_numberOfTRTTubeHits                        );
  m_InDetTrackParticles->Branch( "numberOfTRTXenonHits", &InDetTrackParticles_numberOfTRTXenonHits                       );
  m_InDetTrackParticles->Branch( "numberOfTRTSharedHits", &InDetTrackParticles_numberOfTRTSharedHits                      );
  m_InDetTrackParticles->Branch( "numberOfPrecisionLayers", &InDetTrackParticles_numberOfPrecisionLayers                    );
  m_InDetTrackParticles->Branch( "numberOfPrecisionHoleLayers", &InDetTrackParticles_numberOfPrecisionHoleLayers                );
  m_InDetTrackParticles->Branch( "numberOfPhiLayers", &InDetTrackParticles_numberOfPhiLayers                          );
  m_InDetTrackParticles->Branch( "numberOfPhiHoleLayers", &InDetTrackParticles_numberOfPhiHoleLayers                      );
  m_InDetTrackParticles->Branch( "numberOfTriggerEtaLayers", &InDetTrackParticles_numberOfTriggerEtaLayers                   );
  m_InDetTrackParticles->Branch( "numberOfTriggerEtaHoleLayers", &InDetTrackParticles_numberOfTriggerEtaHoleLayers               );
  m_InDetTrackParticles->Branch( "numberOfOutliersOnTrack", &InDetTrackParticles_numberOfOutliersOnTrack                    );
  m_InDetTrackParticles->Branch( "standardDeviationOfChi2OS", &InDetTrackParticles_standardDeviationOfChi2OS                  );
  m_InDetTrackParticles->Branch( "eProbabilityComb", &InDetTrackParticles_eProbabilityComb                           );
  m_InDetTrackParticles->Branch( "eProbabilityHT", &InDetTrackParticles_eProbabilityHT                             );
  m_InDetTrackParticles->Branch( "pixeldEdx", &InDetTrackParticles_pixeldEdx                                  );
  m_InDetTrackParticles->Branch( "numberOfUsedHitsdEdx", &InDetTrackParticles_numberOfUsedHitsdEdx                       );
  m_InDetTrackParticles->Branch( "numberOfIBLOverflowsdEdx", &InDetTrackParticles_numberOfIBLOverflowsdEdx                   );
  //  m_InDetTrackParticles->Branch( "vertexLink", &InDetTrackParticles_vertexLink                               );
  m_InDetTrackParticles->Branch( "TRTTrackOccupancy", &InDetTrackParticles_TRTTrackOccupancy                          );
  //  m_InDetTrackParticles->Branch( "TrkIBLX", &InDetTrackParticles_TrkIBLX                                  );
  m_InDetTrackParticles->Branch( "TRTdEdxUsedHits", &InDetTrackParticles_TRTdEdxUsedHits                            );
  m_InDetTrackParticles->Branch( "TrkIBLY", &InDetTrackParticles_TrkIBLY                                    );
  m_InDetTrackParticles->Branch( "TrkIBLZ", &InDetTrackParticles_TrkIBLZ                                    );
  m_InDetTrackParticles->Branch( "TRTdEdx", &InDetTrackParticles_TRTdEdx                                    );
  m_InDetTrackParticles->Branch( "TrkBLX", &InDetTrackParticles_TrkBLX                                     );
  m_InDetTrackParticles->Branch( "TrkBLY", &InDetTrackParticles_TrkBLY                                     );
  m_InDetTrackParticles->Branch( "TrkBLZ", &InDetTrackParticles_TrkBLZ                                     );
  m_InDetTrackParticles->Branch( "nBC_meas", &InDetTrackParticles_nBC_meas                                   );
  m_InDetTrackParticles->Branch( "TrkL1X", &InDetTrackParticles_TrkL1X                                     );
  m_InDetTrackParticles->Branch( "TrkL1Y", &InDetTrackParticles_TrkL1Y                                     );
  m_InDetTrackParticles->Branch( "TrkL2X", &InDetTrackParticles_TrkL2X                                     );
  m_InDetTrackParticles->Branch( "truthMatchProbability", &InDetTrackParticles_truthMatchProbability                      );
  m_InDetTrackParticles->Branch( "TrkL2Y", &InDetTrackParticles_TrkL2Y                                     );
  //  m_InDetTrackParticles->Branch( "truthType",  &InDetTrackParticles_truthType                                );
  //  m_InDetTrackParticles->Branch( "truthOrigin",  &InDetTrackParticles_truthOrigin                              );
  m_InDetTrackParticles->Branch( "TrkL2Z", &InDetTrackParticles_TrkL2Z                                     );
  // m_InDetTrackParticles->Branch( "measurement_region", &InDetTrackParticles_measurement_region                       );
  // m_InDetTrackParticles->Branch( "measurement_det", &InDetTrackParticles_measurement_det                          );
  // m_InDetTrackParticles->Branch( "measurement_iLayer", &InDetTrackParticles_measurement_iLayer                       );
  // m_InDetTrackParticles->Branch( "hitResiduals_residualLocX", &InDetTrackParticles_hitResiduals_residualLocX                );
  // m_InDetTrackParticles->Branch( "hitResiduals_pullLocX", &InDetTrackParticles_hitResiduals_pullLocX                    );
  // m_InDetTrackParticles->Branch( "hitResiduals_residualLocY", &InDetTrackParticles_hitResiduals_residualLocY                );
  // m_InDetTrackParticles->Branch( "hitResiduals_pullLocY", &InDetTrackParticles_hitResiduals_pullLocY                    );
  // m_InDetTrackParticles->Branch( "hitResiduals_phiWidth", &InDetTrackParticles_hitResiduals_phiWidth                    );
  // m_InDetTrackParticles->Branch( "hitResiduals_etaWidth", &InDetTrackParticles_hitResiduals_etaWidth                    );
  // m_InDetTrackParticles->Branch( "measurement_type", &InDetTrackParticles_measurement_type                         );
  m_InDetTrackParticles->Branch( "d0err", &InDetTrackParticles_d0err                                      );
  m_InDetTrackParticles->Branch( "z0err", &InDetTrackParticles_z0err                                      );
  m_InDetTrackParticles->Branch( "phierr", &InDetTrackParticles_phierr                                     );
  m_InDetTrackParticles->Branch( "thetaerr", &InDetTrackParticles_thetaerr                                   );
  m_InDetTrackParticles->Branch( "qopterr", &InDetTrackParticles_qopterr                                    );
  // m_InDetTrackParticles->Branch( "msosLink", &InDetTrackParticles_msosLink                                 );
  // m_InDetTrackParticles->Branch( "caloExt_Decorated", &InDetTrackParticles_caloExt_Decorated                        );
  // m_InDetTrackParticles->Branch( "caloExt_eta", &InDetTrackParticles_caloExt_eta                              );
  // m_InDetTrackParticles->Branch( "caloExt_phi", &InDetTrackParticles_caloExt_phi                              );
}

void MyxAODAnalysis::FillInDetTrackParticle( const xAOD::TrackParticleContainer* TrackPart )
{
  InDetTrackParticles_d0                                             ->clear();
  InDetTrackParticles_z0                                             ->clear();
  InDetTrackParticles_phi                                            ->clear();
  InDetTrackParticles_theta                                          ->clear();
  InDetTrackParticles_qOverP                                         ->clear();
  // InDetTrackParticles_definingParametersCovMatrix                 ->clear();
  InDetTrackParticles_vx                                             ->clear();
  InDetTrackParticles_vy                                             ->clear();
  InDetTrackParticles_vz                                             ->clear();
  InDetTrackParticles_radiusOfFirstHit                               ->clear();
  InDetTrackParticles_identifierOfFirstHit                           ->clear();
  InDetTrackParticles_beamlineTiltX                                  ->clear();
  InDetTrackParticles_beamlineTiltY                                  ->clear();
  InDetTrackParticles_hitPattern                                     ->clear();
  InDetTrackParticles_chiSquared                                     ->clear();
  InDetTrackParticles_numberDoF                                      ->clear();
  InDetTrackParticles_trackFitter                                    ->clear();
  InDetTrackParticles_particleHypothesis                             ->clear();
  InDetTrackParticles_trackProperties                                ->clear();
  InDetTrackParticles_patternRecoInfo                                ->clear();
  InDetTrackParticles_numberOfContribPixelLayers                     ->clear();
  InDetTrackParticles_numberOfInnermostPixelLayerHits                ->clear();
  InDetTrackParticles_numberOfInnermostPixelLayerOutliers            ->clear();
  InDetTrackParticles_numberOfInnermostPixelLayerSharedHits          ->clear();
  InDetTrackParticles_numberOfInnermostPixelLayerSplitHits           ->clear();
  InDetTrackParticles_expectInnermostPixelLayerHit                   ->clear();
  InDetTrackParticles_numberOfNextToInnermostPixelLayerHits          ->clear();
  InDetTrackParticles_numberOfNextToInnermostPixelLayerOutliers      ->clear();
  InDetTrackParticles_numberOfNextToInnermostPixelLayerSharedHits    ->clear();
  InDetTrackParticles_numberOfNextToInnermostPixelLayerSplitHits     ->clear();
  InDetTrackParticles_expectNextToInnermostPixelLayerHit             ->clear();
  InDetTrackParticles_numberOfPixelHits                              ->clear();
  InDetTrackParticles_numberOfPixelOutliers                          ->clear();
  InDetTrackParticles_numberOfPixelHoles                             ->clear();
  InDetTrackParticles_numberOfPixelSharedHits                        ->clear();
  InDetTrackParticles_numberOfPixelSplitHits                         ->clear();
  InDetTrackParticles_numberOfGangedPixels                           ->clear();
  InDetTrackParticles_numberOfGangedFlaggedFakes                     ->clear();
  InDetTrackParticles_numberOfPixelDeadSensors                       ->clear();
  InDetTrackParticles_numberOfPixelSpoiltHits                        ->clear();
  InDetTrackParticles_numberOfDBMHits                                ->clear();
  InDetTrackParticles_numberOfSCTHits                                ->clear();
  InDetTrackParticles_numberOfSCTOutliers                            ->clear();
  InDetTrackParticles_numberOfSCTHoles                               ->clear();
  InDetTrackParticles_numberOfSCTDoubleHoles                         ->clear();
  InDetTrackParticles_numberOfSCTSharedHits                          ->clear();
  InDetTrackParticles_numberOfSCTDeadSensors                         ->clear();
  InDetTrackParticles_numberOfSCTSpoiltHits                          ->clear();
  InDetTrackParticles_numberOfTRTHits                                ->clear();
  InDetTrackParticles_numberOfTRTOutliers                            ->clear();
  InDetTrackParticles_numberOfTRTHoles                               ->clear();
  InDetTrackParticles_numberOfTRTHighThresholdHits                   ->clear();
  InDetTrackParticles_numberOfTRTHighThresholdHitsTotal              ->clear();
  InDetTrackParticles_numberOfTRTHighThresholdOutliers               ->clear();
  InDetTrackParticles_numberOfTRTDeadStraws                          ->clear();
  InDetTrackParticles_numberOfTRTTubeHits                            ->clear();
  InDetTrackParticles_numberOfTRTXenonHits                           ->clear();
  InDetTrackParticles_numberOfTRTSharedHits                          ->clear();
  InDetTrackParticles_numberOfPrecisionLayers                        ->clear();
  InDetTrackParticles_numberOfPrecisionHoleLayers                    ->clear();
  InDetTrackParticles_numberOfPhiLayers                              ->clear();
  InDetTrackParticles_numberOfPhiHoleLayers                          ->clear();
  InDetTrackParticles_numberOfTriggerEtaLayers                       ->clear();
  InDetTrackParticles_numberOfTriggerEtaHoleLayers                   ->clear();
  InDetTrackParticles_numberOfOutliersOnTrack                        ->clear();
  InDetTrackParticles_standardDeviationOfChi2OS                      ->clear();
  InDetTrackParticles_eProbabilityComb                               ->clear();
  InDetTrackParticles_eProbabilityHT                                 ->clear();
  InDetTrackParticles_pixeldEdx                                      ->clear();
  InDetTrackParticles_numberOfUsedHitsdEdx                           ->clear();
  InDetTrackParticles_numberOfIBLOverflowsdEdx                       ->clear();
  //InDetTrackParticles_vertexLink                                   ->clear();
  InDetTrackParticles_TRTTrackOccupancy                              ->clear();
  //InDetTrackParticles_TrkIBLX                                      ->clear();
  InDetTrackParticles_TRTdEdxUsedHits                                ->clear();
  InDetTrackParticles_TrkIBLY                                        ->clear();
  InDetTrackParticles_TrkIBLZ                                        ->clear();
  InDetTrackParticles_TRTdEdx                                        ->clear();
  InDetTrackParticles_TrkBLX                                         ->clear();
  InDetTrackParticles_TrkBLY                                         ->clear();
  InDetTrackParticles_TrkBLZ                                         ->clear();
  InDetTrackParticles_nBC_meas                                       ->clear();
  InDetTrackParticles_TrkL1X                                         ->clear();
  InDetTrackParticles_TrkL1Y                                         ->clear();
  InDetTrackParticles_TrkL2X                                         ->clear();
  InDetTrackParticles_truthMatchProbability                          ->clear();
  InDetTrackParticles_TrkL2Y                                         ->clear();
  //InDetTrackParticles_truthType                                    ->clear();
  //InDetTrackParticles_truthOrigin                                  ->clear();
  InDetTrackParticles_TrkL2Z                                         ->clear();
  //InDetTrackParticles_measurement_region                           ->clear();
  //InDetTrackParticles_measurement_det                              ->clear();
  //InDetTrackParticles_measurement_iLayer                           ->clear();
  //InDetTrackParticles_hitResiduals_residualLocX                    ->clear();
  //InDetTrackParticles_hitResiduals_pullLocX                        ->clear();
  //InDetTrackParticles_hitResiduals_residualLocY                    ->clear();
  //InDetTrackParticles_hitResiduals_pullLocY                        ->clear();
  //InDetTrackParticles_hitResiduals_phiWidth                        ->clear();
  //InDetTrackParticles_hitResiduals_etaWidth                        ->clear();
  //InDetTrackParticles_measurement_type                             ->clear();
  InDetTrackParticles_d0err                                          ->clear();
  InDetTrackParticles_z0err                                          ->clear();
  InDetTrackParticles_phierr                                         ->clear();
  InDetTrackParticles_thetaerr                                       ->clear();
  InDetTrackParticles_qopterr                                        ->clear();
  //InDetTrackParticles_msosLink                                     ->clear();
  //InDetTrackParticles_caloExt_Decorated                            ->clear();
  //InDetTrackParticles_caloExt_eta                                  ->clear();
  //InDetTrackParticles_caloExt_phi                                  ->clear();
  for ( auto TrackPart : *TrackPart ){
    InDetTrackParticles_d0                                             ->push_back( TrackPart->auxdata<float>("d0"));
    InDetTrackParticles_z0                                             ->push_back( TrackPart->auxdata<float>("z0"));
    InDetTrackParticles_phi                                            ->push_back( TrackPart->auxdata<float>("phi"));
    InDetTrackParticles_theta                                          ->push_back( TrackPart->auxdata<float>("theta"));
    InDetTrackParticles_qOverP                                         ->push_back( TrackPart->auxdata<float>("qOverP"));
    // InDetTrackParticles_definingParametersCovMatrix                 ->push_back( TrackPart->auxdata<float>("definingParametersCovMatrix"));
    InDetTrackParticles_vx                                             ->push_back( TrackPart->auxdata<float>("vx"));
    InDetTrackParticles_vy                                             ->push_back( TrackPart->auxdata<float>("vy"));
    InDetTrackParticles_vz                                             ->push_back( TrackPart->auxdata<float>("vz"));
    InDetTrackParticles_radiusOfFirstHit                               ->push_back( TrackPart->auxdata<float>("radiusOfFirstHit"));
    InDetTrackParticles_identifierOfFirstHit                           ->push_back( TrackPart->auxdata<unsigned long>("identifierOfFirstHit"));
    InDetTrackParticles_beamlineTiltX                                  ->push_back( TrackPart->auxdata<float>("beamlineTiltX"));
    InDetTrackParticles_beamlineTiltY                                  ->push_back( TrackPart->auxdata<float>("beamlineTiltY"));
    InDetTrackParticles_hitPattern                                     ->push_back( TrackPart->auxdata<unsigned int>("hitPattern"));
    InDetTrackParticles_chiSquared                                     ->push_back( TrackPart->auxdata<float>("chiSquared"));
    InDetTrackParticles_numberDoF                                      ->push_back( TrackPart->auxdata<float>("numberDoF"));
    InDetTrackParticles_trackFitter                                    ->push_back( TrackPart->auxdata<unsigned char>("trackFitter"));
    InDetTrackParticles_particleHypothesis                             ->push_back( TrackPart->auxdata<unsigned char>("particleHypothesis"));
    InDetTrackParticles_trackProperties                                ->push_back( TrackPart->auxdata<unsigned char>("trackProperties"));
    InDetTrackParticles_patternRecoInfo                                ->push_back( TrackPart->auxdata<unsigned long>("patternRecoInfo"));
    InDetTrackParticles_numberOfContribPixelLayers                     ->push_back( TrackPart->auxdata<unsigned char>("numberOfContribPixelLayers"));
    InDetTrackParticles_numberOfInnermostPixelLayerHits                ->push_back( TrackPart->auxdata<unsigned char>("numberOfInnermostPixelLayerHits"));
    InDetTrackParticles_numberOfInnermostPixelLayerOutliers            ->push_back( TrackPart->auxdata<unsigned char>("numberOfInnermostPixelLayerOutliers"));
    InDetTrackParticles_numberOfInnermostPixelLayerSharedHits          ->push_back( TrackPart->auxdata<unsigned char>("numberOfInnermostPixelLayerSharedHits"));
    InDetTrackParticles_numberOfInnermostPixelLayerSplitHits           ->push_back( TrackPart->auxdata<unsigned char>("numberOfInnermostPixelLayerSplitHits"));
    InDetTrackParticles_expectInnermostPixelLayerHit                   ->push_back( TrackPart->auxdata<unsigned char>("expectInnermostPixelLayerHit"));
    InDetTrackParticles_numberOfNextToInnermostPixelLayerHits          ->push_back( TrackPart->auxdata<unsigned char>("numberOfNextToInnermostPixelLayerHits"));
    InDetTrackParticles_numberOfNextToInnermostPixelLayerOutliers      ->push_back( TrackPart->auxdata<unsigned char>("numberOfNextToInnermostPixelLayerOutliers"));
    InDetTrackParticles_numberOfNextToInnermostPixelLayerSharedHits    ->push_back( TrackPart->auxdata<unsigned char>("numberOfNextToInnermostPixelLayerSharedHits"));
    InDetTrackParticles_numberOfNextToInnermostPixelLayerSplitHits     ->push_back( TrackPart->auxdata<unsigned char>("numberOfNextToInnermostPixelLayerSplitHits"));
    InDetTrackParticles_expectNextToInnermostPixelLayerHit             ->push_back( TrackPart->auxdata<unsigned char>("expectNextToInnermostPixelLayerHit"));
    InDetTrackParticles_numberOfPixelHits                              ->push_back( TrackPart->auxdata<unsigned char>("numberOfPixelHits"));
    InDetTrackParticles_numberOfPixelOutliers                          ->push_back( TrackPart->auxdata<unsigned char>("numberOfPixelOutliers"));
    InDetTrackParticles_numberOfPixelHoles                             ->push_back( TrackPart->auxdata<unsigned char>("numberOfPixelHoles"));
    InDetTrackParticles_numberOfPixelSharedHits                        ->push_back( TrackPart->auxdata<unsigned char>("numberOfPixelSharedHits"));
    InDetTrackParticles_numberOfPixelSplitHits                         ->push_back( TrackPart->auxdata<unsigned char>("numberOfPixelSplitHits"));
    InDetTrackParticles_numberOfGangedPixels                           ->push_back( TrackPart->auxdata<unsigned char>("numberOfGangedPixels"));
    InDetTrackParticles_numberOfGangedFlaggedFakes                     ->push_back( TrackPart->auxdata<unsigned char>("numberOfGangedFlaggedFakes"));
    InDetTrackParticles_numberOfPixelDeadSensors                       ->push_back( TrackPart->auxdata<unsigned char>("numberOfPixelDeadSensors"));
    InDetTrackParticles_numberOfPixelSpoiltHits                        ->push_back( TrackPart->auxdata<unsigned char>("numberOfPixelSpoiltHits"));
    InDetTrackParticles_numberOfDBMHits                                ->push_back( TrackPart->auxdata<unsigned char>("numberOfDBMHits"));
    InDetTrackParticles_numberOfSCTHits                                ->push_back( TrackPart->auxdata<unsigned char>("numberOfSCTHits"));
    InDetTrackParticles_numberOfSCTOutliers                            ->push_back( TrackPart->auxdata<unsigned char>("numberOfSCTOutliers"));
    InDetTrackParticles_numberOfSCTHoles                               ->push_back( TrackPart->auxdata<unsigned char>("numberOfSCTHoles"));
    InDetTrackParticles_numberOfSCTDoubleHoles                         ->push_back( TrackPart->auxdata<unsigned char>("numberOfSCTDoubleHoles"));
    InDetTrackParticles_numberOfSCTSharedHits                          ->push_back( TrackPart->auxdata<unsigned char>("numberOfSCTSharedHits"));
    InDetTrackParticles_numberOfSCTDeadSensors                         ->push_back( TrackPart->auxdata<unsigned char>("numberOfSCTDeadSensors"));
    InDetTrackParticles_numberOfSCTSpoiltHits                          ->push_back( TrackPart->auxdata<unsigned char>("numberOfSCTSpoiltHits"));
    InDetTrackParticles_numberOfTRTHits                                ->push_back( TrackPart->auxdata<unsigned char>("numberOfTRTHits"));
    InDetTrackParticles_numberOfTRTOutliers                            ->push_back( TrackPart->auxdata<unsigned char>("numberOfTRTOutliers"));
    InDetTrackParticles_numberOfTRTHoles                               ->push_back( TrackPart->auxdata<unsigned char>("numberOfTRTHoles"));
    InDetTrackParticles_numberOfTRTHighThresholdHits                   ->push_back( TrackPart->auxdata<unsigned char>("numberOfTRTHighThresholdHits"));
    InDetTrackParticles_numberOfTRTHighThresholdHitsTotal              ->push_back( TrackPart->auxdata<unsigned char>("numberOfTRTHighThresholdHitsTotal"));
    InDetTrackParticles_numberOfTRTHighThresholdOutliers               ->push_back( TrackPart->auxdata<unsigned char>("numberOfTRTHighThresholdOutliers"));
    InDetTrackParticles_numberOfTRTDeadStraws                          ->push_back( TrackPart->auxdata<unsigned char>("numberOfTRTDeadStraws"));
    InDetTrackParticles_numberOfTRTTubeHits                            ->push_back( TrackPart->auxdata<unsigned char>("numberOfTRTTubeHits"));
    InDetTrackParticles_numberOfTRTXenonHits                           ->push_back( TrackPart->auxdata<unsigned char>("numberOfTRTXenonHits"));
    InDetTrackParticles_numberOfTRTSharedHits                          ->push_back( TrackPart->auxdata<unsigned char>("numberOfTRTSharedHits"));
    InDetTrackParticles_numberOfPrecisionLayers                        ->push_back( TrackPart->auxdata<unsigned char>("numberOfPrecisionLayers"));
    InDetTrackParticles_numberOfPrecisionHoleLayers                    ->push_back( TrackPart->auxdata<unsigned char>("numberOfPrecisionHoleLayers"));
    InDetTrackParticles_numberOfPhiLayers                              ->push_back( TrackPart->auxdata<unsigned char>("numberOfPhiLayers"));
    InDetTrackParticles_numberOfPhiHoleLayers                          ->push_back( TrackPart->auxdata<unsigned char>("numberOfPhiHoleLayers"));
    InDetTrackParticles_numberOfTriggerEtaLayers                       ->push_back( TrackPart->auxdata<unsigned char>("numberOfTriggerEtaLayers"));
    InDetTrackParticles_numberOfTriggerEtaHoleLayers                   ->push_back( TrackPart->auxdata<unsigned char>("numberOfTriggerEtaHoleLayers"));
    InDetTrackParticles_numberOfOutliersOnTrack                        ->push_back( TrackPart->auxdata<unsigned char>("numberOfOutliersOnTrack"));
    InDetTrackParticles_standardDeviationOfChi2OS                      ->push_back( TrackPart->auxdata<unsigned char>("standardDeviationOfChi2OS"));
    InDetTrackParticles_eProbabilityComb                               ->push_back( TrackPart->auxdata<float>("eProbabilityComb"));
    InDetTrackParticles_eProbabilityHT                                 ->push_back( TrackPart->auxdata<float>("eProbabilityHT"));
    InDetTrackParticles_pixeldEdx                                      ->push_back( TrackPart->auxdata<float>("pixeldEdx"));
    InDetTrackParticles_numberOfUsedHitsdEdx                           ->push_back( TrackPart->auxdata<unsigned char>("numberOfUsedHitsdEdx"));
    InDetTrackParticles_numberOfIBLOverflowsdEdx                       ->push_back( TrackPart->auxdata<unsigned char>("numberOfIBLOverflowsdEdx"));
    //InDetTrackParticles_vertexLink                                   ->push_back( TrackPart->auxdata<>("vertexLink"));
    InDetTrackParticles_TRTTrackOccupancy                              ->push_back( TrackPart->auxdata<float>("TRTTrackOccupancy"));
    //InDetTrackParticles_TrkIBLX                                      ->push_back( TrackPart->auxdata<>("TrkIBLX"));
    InDetTrackParticles_TRTdEdxUsedHits                                ->push_back( TrackPart->auxdata<unsigned char>("TRTdEdxUsedHits"));
    InDetTrackParticles_TrkIBLY                                        ->push_back( TrackPart->auxdata<float>("TrkIBLY"));
    InDetTrackParticles_TrkIBLZ                                        ->push_back( TrackPart->auxdata<float>("TrkIBLZ"));
    InDetTrackParticles_TRTdEdx                                        ->push_back( TrackPart->auxdata<float>("TRTdEdx"));
    InDetTrackParticles_TrkBLX                                         ->push_back( TrackPart->auxdata<float>("TrkBLX"));
    InDetTrackParticles_TrkBLY                                         ->push_back( TrackPart->auxdata<float>("TrkBLY"));
    InDetTrackParticles_TrkBLZ                                         ->push_back( TrackPart->auxdata<float>("TrkBLZ"));
    InDetTrackParticles_nBC_meas                                       ->push_back( TrackPart->auxdata<int>("nBC_meas"));
    InDetTrackParticles_TrkL1X                                         ->push_back( TrackPart->auxdata<float>("TrkL1X"));
    InDetTrackParticles_TrkL1Y                                         ->push_back( TrackPart->auxdata<float>("TrkL1Y"));
    InDetTrackParticles_TrkL2X                                         ->push_back( TrackPart->auxdata<float>("TrkL2X"));
    InDetTrackParticles_truthMatchProbability                          ->push_back( TrackPart->auxdata<float>("truthMatchProbability"));
    InDetTrackParticles_TrkL2Y                                         ->push_back( TrackPart->auxdata<float>("TrkL2Y"));
    //InDetTrackParticles_truthType                                    ->push_back( TrackPart->auxdata<int>("truthType"));
    //InDetTrackParticles_truthOrigin                                  ->push_back( TrackPart->auxdata<float>("truthOrigin"));
    InDetTrackParticles_TrkL2Z                                         ->push_back( TrackPart->auxdata<float>("TrkL2Z"));
    //InDetTrackParticles_measurement_region                           ->push_back( TrackPart->auxdata<>("measurement_region"));
    //InDetTrackParticles_measurement_det                              ->push_back( TrackPart->auxdata<>("measurement_det"));
    //InDetTrackParticles_measurement_iLayer                           ->push_back( TrackPart->auxdata<>("measurement_iLayer"));
    //InDetTrackParticles_hitResiduals_residualLocX                    ->push_back( TrackPart->auxdata<int>("hitResiduals_residualLocX"));
    //InDetTrackParticles_hitResiduals_pullLocX                        ->push_back( TrackPart->auxdata<int>("hitResiduals_pullLocX"));
    //InDetTrackParticles_hitResiduals_residualLocY                    ->push_back( TrackPart->auxdata<int>("hitResiduals_residualLocY"));
    //InDetTrackParticles_hitResiduals_pullLocY                        ->push_back( TrackPart->auxdata<int>("hitResiduals_pullLocY"));
    //InDetTrackParticles_hitResiduals_phiWidth                        ->push_back( TrackPart->auxdata<int>("hitResiduals_phiWidth"));
    //InDetTrackParticles_hitResiduals_etaWidth                        ->push_back( TrackPart->auxdata<int>("hitResiduals_etaWidth"));
    //InDetTrackParticles_measurement_type                             ->push_back( TrackPart->auxdata<>("measurement_type"));
    InDetTrackParticles_d0err                                          ->push_back( TrackPart->auxdata<float>("d0err"));
    InDetTrackParticles_z0err                                          ->push_back( TrackPart->auxdata<float>("z0err"));
    InDetTrackParticles_phierr                                         ->push_back( TrackPart->auxdata<float>("phierr"));
    InDetTrackParticles_thetaerr                                       ->push_back( TrackPart->auxdata<float>("thetaerr"));
    InDetTrackParticles_qopterr                                        ->push_back( TrackPart->auxdata<float>("qopterr"));
    //InDetTrackParticles_msosLink                                     ->push_back( TrackPart->auxdata<float>("msosLink"));
    //InDetTrackParticles_caloExt_Decorated                            ->push_back( TrackPart->auxdata<char>("caloExt_Decorated"));
    //InDetTrackParticles_caloExt_eta                                  ->push_back( TrackPart->auxdata<float>("caloExt_eta"));
    //InDetTrackParticles_caloExt_phi                                  ->push_back( TrackPart->auxdata<float>("caloExt_phi"));
  }
  m_InDetTrackParticles->Fill();
}

void MyxAODAnalysis :: Write()
{
  m_InDetTrackParticles->Write();
}
