
#include <TText.h>

void text( Double_t x, Double_t y, TString str)
{
  TText* t = new TText(x,y,str);
  t->SetNDC(1);
  t->SetTextAlign(22);
  t->SetTextColor(kBlack);
  t->SetTextFont(42);
  t->SetTextSize(0.05);
  t->SetTextAngle(0);
  t->Draw();
}
