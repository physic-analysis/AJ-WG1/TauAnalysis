
#include <TLine.h>

void PixelLayer( TH1D* hist , Double_t ymax = 0)
{
  if ( ymax == 0 ) 
    ymax = hist->GetYaxis()->GetXmax();
  TLine* pixel[4];
  for ( int iLayer=0; iLayer<4; iLayer++){
    switch (iLayer){
      case 0:
        pixel[iLayer] = new TLine(33.25, 0, 33.25, ymax);
        break;
      case 1:
        pixel[iLayer] = new TLine(50.5,  0, 50.5,  ymax);
        break;
      case 2:
        pixel[iLayer] = new TLine(88.5,  0, 88.5,  ymax);
        break;
      case 3:
        pixel[iLayer] = new TLine(122.5, 0, 122.5, ymax);
        break;
    }
    pixel[iLayer]->SetLineWidth(2);
    pixel[iLayer]->SetLineStyle(9);
    pixel[iLayer]->SetLineColor(kBlack);
    pixel[iLayer]->Draw("same");
  }
}
