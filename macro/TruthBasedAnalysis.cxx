
#include <iostream>
#include <TH1.h>
#include <TH2.h>
#include <TFile.h>

#include <TTree.h>
#include <TStyle.h>
#include <TText.h>
#include <TCanvas.h>
#include <TLegend.h>
#include <TLine.h>

#include "tools/text.h"
#include "tools/PixelLayer.h"
#include "ModeSelector.h"

int prong(int nprong) { 
  if ( nprong == 1 ) return 0;
  if ( nprong == 3 ) return 1;
}

int IBL(){
  return 0;
}

int BLayer(){
  return 1;
}

int L1(){
  return 2;
}

int L2(){
  return 3;
}

int isOusterPixel(int decayRad)
{
  if ( decayRad < 122.5 ) return 0;
  if ( decayRad > 122.5 ) return 1;
}

struct EventCounter
{
  EventCounter(){
   all=0;
   hadronic=0;
   eta_1_0=0;
   prong1=0;
   prong3=0;
   decayRadLower33_25=0;
   decayRadLower50_5=0;
   decayRadLower88_5=0;
   decayRadLower122_5=0;
   decayRadLower122_5=0;
   decayRadLarger122_5=0;
  };
  int all;
  int hadronic;
  int eta_1_0;
  int prong1;
  int prong3;
  int decayRadLower33_25;
  int decayRadLower50_5;
  int decayRadLower88_5;
  int decayRadLower122_5;
  int decayRadLarger122_5;
};

void TruthBasedAnalysis()
{
  //std::string simulator="FullG4_LongLived";
  std::string simulator="MC12G4";
  //std::string path = "/afs/cern.ch/user/k/ktakeda/workspace/public/atlas-phys/AJ/WG1/SingleParticle/AnalysisArea/run/";
  std::string path = "../Data/";
  std::string file_name = "MyTauNtuple." + simulator + "_20181107.outputs.root";

  TFile* file = new TFile( (path+file_name).c_str(), "read");
  TTree* tree = (TTree*)file->Get("m_trueTauTree");

  
  int Truth_part_numChargedTracks       = 0;
  int Truth_part_numChargedLeptonTracks = 0;

  //
  ///////////////////////////////////////////
  // Truth tau
  Double_t Truth_tau_pt; 
  Double_t Truth_tau_eta; 
  Double_t Truth_tau_phi; 
  Double_t Truth_tau_m; 
  Bool_t   Truth_tau_isLeptonic = false; 
  Bool_t   Truth_tau_isHadronic = false; 
  Int_t    Truth_tau_numTracks =0; 
  Bool_t   Truth_tau_isMatchedWithReco = false; 
  Double_t Truth_tau_decayRad = 0; 
  //
  ///////////////////////////////////////////
  // Truth daughter event
  std::vector<int>*          Truth_dau_pdgId=0;
  //
  Bool_t   tt_truth_leptonic_electron = false; 
  Bool_t   tt_truth_leptonic_muon = false; 
  Bool_t   tt_truth_leptonic_tau = false; 
  Double_t tt_truth_neutrino_pt; 
  //
  Int_t                      eventNumber;
  //
  std::vector<double>*       Truth_track_pt = 0;
  std::vector<double>*       Truth_track_eta = 0;
  std::vector<double>*       Truth_track_phi = 0;
  std::vector<double>*       Truth_track_m = 0;
  std::vector<double>*       Truth_track_charge = 0;
  //
  ///////////////////////////////////////////
  // Truth neutrino information 
  std::vector<int>*          Truth_neutrino_pdgId=0;
  std::vector<double>*       Truth_neutrino_pt=0;
  std::vector<double>*       Truth_neutrino_eta=0;
  std::vector<double>*       Truth_neutrino_phi=0;
  //
  Float_t                    Truth_prodVtx_x;
  Float_t                    Truth_prodVtx_y;
  Float_t                    Truth_prodVtx_z;
  Float_t                    Truth_prodVtx_eta;
  Float_t                    Truth_prodVtx_phi;
  Float_t                    Truth_prodVtx_perp;
  //
  //
  Float_t                    Truth_decVtx_x;
  Float_t                    Truth_decVtx_y;
  Float_t                    Truth_decVtx_z;
  Float_t                    Truth_decVtx_t;
  Float_t                    Truth_decVtx_eta;
  Float_t                    Truth_decVtx_phi;
  Float_t                    Truth_decVtx_perp;
  //
  Int_t                      InDet_numTracks;        //! the number of tracks passed pixel layer per events
  std::vector<double>*       InDet_track_pt          = 0;
  std::vector<double>*       InDet_track_eta         = 0;
  std::vector<double>*       InDet_track_phi         = 0;
  std::vector<double>*       InDet_track_m           = 0;
  std::vector<double>*       InDet_track_d0           = 0;
  std::vector<double>*       InDet_track_z0           = 0;
  std::vector<float>*        InDet_track_charge       = 0;
  std::vector<int>*          InDet_track_numberOfIBLHits = 0;
  std::vector<int>*          InDet_track_numberOfBLayerHits = 0;
  std::vector<int>*          InDet_track_numberOfPixelL1Hits = 0;
  std::vector<int>*          InDet_track_numberOfPixelL2Hits = 0;
  std::vector<int>*          InDet_track_numberOfPixelHits = 0;
  std::vector<int>*          InDet_track_numberOfSCTHits   = 0;
  //
  ///////////////////////////////////////////
  // Pixel raw cluster information 
  std::vector<std::vector<double>>* InDet_pixel_raw_ClustersEta    = 0;
  std::vector<std::vector<double>>* InDet_pixel_raw_ClustersPhi    = 0;
  std::vector<std::vector<double>>* InDet_pixel_raw_ClustersCharge = 0;
  std::vector<std::vector<double>>* InDet_pixel_raw_globalX        = 0;
  std::vector<std::vector<double>>* InDet_pixel_raw_globalY        = 0;
  std::vector<std::vector<double>>* InDet_pixel_raw_globalZ        = 0;
  std::vector<std::vector<double> >*    InDet_pixel_raw_IB_energyDeposit= 0;
  std::vector<std::vector<double> >*    InDet_pixel_raw_BL_energyDeposit= 0;
  std::vector<std::vector<double> >*    InDet_pixel_raw_L1_energyDeposit= 0;
  std::vector<std::vector<double> >*    InDet_pixel_raw_L2_energyDeposit= 0;
  std::vector<std::vector<double> >*    InDet_pixel_raw_IB_barcode= 0;
  std::vector<std::vector<double> >*    InDet_pixel_raw_BL_barcode= 0;
  std::vector<std::vector<double> >*    InDet_pixel_raw_L1_barcode= 0;
  std::vector<std::vector<double> >*    InDet_pixel_raw_L2_barcode= 0;
  std::vector<std::vector<double> >*    InDet_pixel_raw_IB_pdgId= 0;
  std::vector<std::vector<double> >*    InDet_pixel_raw_BL_pdgId= 0;
  std::vector<std::vector<double> >*    InDet_pixel_raw_L1_pdgId= 0;
  std::vector<std::vector<double> >*    InDet_pixel_raw_L2_pdgId= 0;
  //
  ///////////////////////////////////////////
  // Pixel cluster information
  Int_t                             InDet_pixel_numAllOfClusters;
  std::vector<std::vector<double>>* InDet_pixel_IBLClustersEta = 0;
  std::vector<std::vector<double>>* InDet_pixel_BLayerClustersEta = 0;
  std::vector<std::vector<double>>* InDet_pixel_L1ClustersEta = 0;
  std::vector<std::vector<double>>* InDet_pixel_L2ClustersEta = 0;
  //
  std::vector<std::vector<double>>* InDet_pixel_IBLClustersPhi = 0;
  std::vector<std::vector<double>>* InDet_pixel_BLayerClustersPhi = 0;
  std::vector<std::vector<double>>* InDet_pixel_L1ClustersPhi = 0;
  std::vector<std::vector<double>>* InDet_pixel_L2ClustersPhi = 0;
  //
  std::vector<std::vector<double>>* InDet_pixel_IBLClustersCharge = 0;
  std::vector<std::vector<double>>* InDet_pixel_BLayerClustersCharge = 0;
  std::vector<std::vector<double>>* InDet_pixel_L1ClustersCharge = 0;
  std::vector<std::vector<double>>* InDet_pixel_L2ClustersCharge = 0;
  ///////////////////////////////////////////
  // Pixel cluster information (not considered the tau)
  Int_t                      Pix_numCluster_IBL;   //! the number of clusters on the IBL
  Int_t                      Pix_numCluster_BLayer;//! the number of clusters on the B-Layer
  Int_t                      Pix_numCluster_L1;    //! the number of clusters on the pixel 2nd layer 
  Int_t                      Pix_numCluster_L2;    //! the number of clusters on the pixel 3rd layer
  //
  Int_t    Reco_tau_numTracks = 0; 
  Bool_t   Reco_tau_isSelected;
  Double_t Reco_tau_pt; 
  Double_t Reco_tau_eta; 
  Double_t Reco_tau_phi; 
  Double_t Reco_tau_e; 
    
  // Tracks from a reco-tau
  std::vector<double>*          Reco_track_pt=0;  
  std::vector<double>*          Reco_track_eta=0;  
  std::vector<double>*          Reco_track_phi=0;  
  std::vector<double>*          Reco_track_e=0;   
  std::vector<double>*          Reco_track_m=0;   
  std::vector<double>*          Reco_track_d0=0;   
  std::vector<double>*          Reco_track_z0=0;   
  std::vector<int>*             Reco_track_numberOfPixelHits=0;

  std::vector<unsigned int>* tt_numberOfContribPixelLayers      =0 ; //!
  std::vector<unsigned int>* tt_numberOfBLayerHits              =0 ; //!
  std::vector<unsigned int>* tt_numberOfPixelHits               =0 ; //!
  std::vector<unsigned int>* tt_numberOfSCTHits                 =0 ; //!
  std::vector<unsigned int>* tt_numberOfTRTHits                 =0 ; //!
  std::vector<unsigned int>* tt_numberOfBLayerSharedHits        =0 ; //!
  std::vector<unsigned int>* tt_numberOfPixelSharedHits         =0 ; //!
  std::vector<unsigned int>* tt_numberOfSCTSharedHits           =0 ; //!
  std::vector<unsigned int>* tt_numberOfTRTSharedHits           =0 ; //!
  std::vector<unsigned int>* tt_numberOfPixelHoles              =0 ; //!
  std::vector<unsigned int>* tt_numberOfSCTHoles                =0 ; //!
  std::vector<unsigned int>* tt_numberOfSCTDoubleHoles          =0 ; //!
  std::vector<unsigned int>* tt_numberOfTRTHoles                =0 ; //!
  std::vector<unsigned int>* tt_numberOfBLayerOutliers          =0 ; //!
  std::vector<unsigned int>* tt_numberOfPixelOutliers           =0 ; //!
  std::vector<unsigned int>* tt_numberOfSCTOutliers             =0 ; //!
  std::vector<unsigned int>* tt_numberOfTRTOutliers             =0 ; //!
  std::vector<unsigned int>* tt_numberOfPixelSpoiltHits         =0 ; //!
  std::vector<unsigned int>* tt_numberOfSCTSpoiltHits           =0 ; //!
  std::vector<unsigned int>* tt_expectBLayerHit                 =0 ; //!
  std::vector<unsigned int>* tt_expectInnermostPixelLayerHit    =0 ; //!
  std::vector<unsigned int>* tt_numberOfInnermostPixelLayerHits =0 ; //!
  std::vector<unsigned int>* tt_numberOfNextToInnermostPixelLayerHits =0 ; //!
  std::vector<unsigned int>* tt_numberOfGangedPixels            =0 ; //!
  std::vector<unsigned int>* tt_numberOfGangedFlaggedFakes      =0 ; //!
  std::vector<unsigned int>* tt_numberOfPixelDeadSensors        =0 ; //!
  std::vector<unsigned int>* tt_numberOfSCTDeadSensors          =0 ; //!
  std::vector<unsigned int>* tt_numberOfTRTDeadStraws           =0 ; //!
  // Pixel raw cluster
  std::vector<unsigned int>* Reco_track_numberOfInnermostPixelHits       = 0 ;//!
  std::vector<unsigned int>* Reco_track_numberOfNextToInnermostPixelHits = 0 ;//!
  std::vector<unsigned int>* Reco_track_numberOfPixelL1Hits                   = 0 ;//!
  std::vector<unsigned int>* Reco_track_numberOfPixelL2Hits                   = 0 ;//!

  tree->SetBranchAddress("eventNumber",               &eventNumber);
  //
  tree->SetBranchAddress("Truth_part_numChargedTracks",                  &Truth_part_numChargedTracks);
  tree->SetBranchAddress("Truth_part_numChargedLeptonTracks",            &Truth_part_numChargedLeptonTracks);
  //
  tree->SetBranchAddress("tt_truth_neutrino_pt",              &tt_truth_neutrino_pt);
  tree->SetBranchAddress("Truth_tau_isLeptonic",           &Truth_tau_isLeptonic);
  tree->SetBranchAddress("tt_truth_leptonic_electron",        &tt_truth_leptonic_electron);
  tree->SetBranchAddress("tt_truth_leptonic_muon",            &tt_truth_leptonic_muon);
  tree->SetBranchAddress("tt_truth_leptonic_tau",             &tt_truth_leptonic_tau);
  tree->SetBranchAddress("Truth_tau_isHadronic",           &Truth_tau_isHadronic);
  tree->SetBranchAddress("Truth_tau_isMatchedWithReco",                    &Truth_tau_isMatchedWithReco);
  tree->SetBranchAddress("Truth_tau_pt",                       &Truth_tau_pt);
  tree->SetBranchAddress("Truth_tau_eta",                      &Truth_tau_eta);
  tree->SetBranchAddress("Truth_tau_phi",                      &Truth_tau_phi);
  tree->SetBranchAddress("Truth_tau_m",                      &Truth_tau_m);
  tree->SetBranchAddress("Truth_tau_decayRad",             &Truth_tau_decayRad);
  tree->SetBranchAddress("Truth_tau_numTracks",                   &Truth_tau_numTracks);
  //
  tree->SetBranchAddress("Truth_dau_pdgId",               &Truth_dau_pdgId);
  //
  tree->SetBranchAddress("Truth_neutrino_pdgId",          &Truth_neutrino_pdgId);
  tree->SetBranchAddress("Truth_neutrino_pt",             &Truth_neutrino_pt);
  tree->SetBranchAddress("Truth_neutrino_eta",            &Truth_neutrino_eta);
  tree->SetBranchAddress("Truth_neutrino_phi",            &Truth_neutrino_phi);
  //
  tree->SetBranchAddress("Truth_track_pt",                &Truth_track_pt);
  tree->SetBranchAddress("Truth_track_eta",               &Truth_track_eta);
  tree->SetBranchAddress("Truth_track_phi",               &Truth_track_phi);
  tree->SetBranchAddress("Truth_track_m",                 &Truth_track_m);
  tree->SetBranchAddress("Truth_track_charge",            &Truth_track_charge);
  //
  tree->SetBranchAddress("Truth_prodVtx_x",               &Truth_prodVtx_x);
  tree->SetBranchAddress("Truth_prodVtx_y",               &Truth_prodVtx_y);
  tree->SetBranchAddress("Truth_prodVtx_z",               &Truth_prodVtx_z);
  tree->SetBranchAddress("Truth_prodVtx_eta",             &Truth_prodVtx_eta);
  tree->SetBranchAddress("Truth_prodVtx_phi",             &Truth_prodVtx_phi);
  tree->SetBranchAddress("Truth_prodVtx_perp",            &Truth_prodVtx_perp);
  //
  tree->SetBranchAddress("Truth_decVtx_x",               &Truth_decVtx_x);
  tree->SetBranchAddress("Truth_decVtx_y",               &Truth_decVtx_y);
  tree->SetBranchAddress("Truth_decVtx_z",               &Truth_decVtx_z);
  tree->SetBranchAddress("Truth_decVtx_t",               &Truth_decVtx_t);
  tree->SetBranchAddress("Truth_decVtx_eta",             &Truth_decVtx_eta);
  tree->SetBranchAddress("Truth_decVtx_phi",             &Truth_decVtx_phi);
  tree->SetBranchAddress("Truth_decVtx_perp",            &Truth_decVtx_perp);
  //
  tree->SetBranchAddress("InDet_numTracks",               &InDet_numTracks);
  tree->SetBranchAddress("InDet_track_pt",                &InDet_track_pt);
  tree->SetBranchAddress("InDet_track_eta",               &InDet_track_eta);
  tree->SetBranchAddress("InDet_track_phi",               &InDet_track_phi);
  tree->SetBranchAddress("InDet_track_m",                 &InDet_track_m);
  tree->SetBranchAddress("InDet_track_d0",                &InDet_track_d0);
  tree->SetBranchAddress("InDet_track_z0",                &InDet_track_z0);
  tree->SetBranchAddress("InDet_track_charge",            &InDet_track_charge);
  tree->SetBranchAddress("InDet_track_numberOfIBLHits",       &InDet_track_numberOfIBLHits);
  tree->SetBranchAddress("InDet_track_numberOfBLayerHits",    &InDet_track_numberOfBLayerHits);
  tree->SetBranchAddress("InDet_track_numberOfPixelL1Hits",        &InDet_track_numberOfPixelL1Hits);
  tree->SetBranchAddress("InDet_track_numberOfPixelL2Hits",        &InDet_track_numberOfPixelL2Hits);
  tree->SetBranchAddress("InDet_track_numberOfPixelHits", &InDet_track_numberOfPixelHits);
  tree->SetBranchAddress("InDet_track_numberOfSCTHits",   &InDet_track_numberOfSCTHits);
  //
  ///////////////////////////////////////////
  // Pixel raw cluster information 
  tree->SetBranchAddress("InDet_pixel_raw_ClustersEta",        &InDet_pixel_raw_ClustersEta);
  tree->SetBranchAddress("InDet_pixel_raw_ClustersPhi",        &InDet_pixel_raw_ClustersPhi);
  tree->SetBranchAddress("InDet_pixel_raw_ClustersCharge",     &InDet_pixel_raw_ClustersCharge);
  tree->SetBranchAddress("InDet_pixel_raw_globalX",            &InDet_pixel_raw_globalX);
  tree->SetBranchAddress("InDet_pixel_raw_globalY",            &InDet_pixel_raw_globalY);
  tree->SetBranchAddress("InDet_pixel_raw_globalZ",            &InDet_pixel_raw_globalZ);
  tree->SetBranchAddress("InDet_pixel_raw_IB_energyDeposit",   &InDet_pixel_raw_IB_energyDeposit);
  tree->SetBranchAddress("InDet_pixel_raw_BL_energyDeposit",   &InDet_pixel_raw_BL_energyDeposit);
  tree->SetBranchAddress("InDet_pixel_raw_L1_energyDeposit",   &InDet_pixel_raw_L1_energyDeposit);
  tree->SetBranchAddress("InDet_pixel_raw_L2_energyDeposit",   &InDet_pixel_raw_L2_energyDeposit);
  tree->SetBranchAddress("InDet_pixel_raw_IB_barcode",         &InDet_pixel_raw_IB_barcode);
  tree->SetBranchAddress("InDet_pixel_raw_BL_barcode",         &InDet_pixel_raw_BL_barcode);
  tree->SetBranchAddress("InDet_pixel_raw_L1_barcode",         &InDet_pixel_raw_L1_barcode);
  tree->SetBranchAddress("InDet_pixel_raw_L2_barcode",         &InDet_pixel_raw_L2_barcode);
  tree->SetBranchAddress("InDet_pixel_raw_IB_pdgId",           &InDet_pixel_raw_IB_pdgId);
  tree->SetBranchAddress("InDet_pixel_raw_BL_pdgId",           &InDet_pixel_raw_BL_pdgId);
  tree->SetBranchAddress("InDet_pixel_raw_L1_pdgId",           &InDet_pixel_raw_L1_pdgId);
  tree->SetBranchAddress("InDet_pixel_raw_L2_pdgId",           &InDet_pixel_raw_L2_pdgId);
  //
  ///////////////////////////////////////////
  // Pixel cluster information 
  tree->SetBranchAddress("InDet_pixel_numAllOfClusters",  &InDet_pixel_numAllOfClusters);
  tree->SetBranchAddress("InDet_pixel_IBLClustersEta",    &InDet_pixel_IBLClustersEta);
  tree->SetBranchAddress("InDet_pixel_BLayerClustersEta", &InDet_pixel_BLayerClustersEta);
  tree->SetBranchAddress("InDet_pixel_L1ClustersEta",     &InDet_pixel_L1ClustersEta);
  tree->SetBranchAddress("InDet_pixel_L2ClustersEta",     &InDet_pixel_L2ClustersEta);
  tree->SetBranchAddress("InDet_pixel_IBLClustersPhi",    &InDet_pixel_IBLClustersPhi);
  tree->SetBranchAddress("InDet_pixel_BLayerClustersPhi", &InDet_pixel_BLayerClustersPhi);
  tree->SetBranchAddress("InDet_pixel_L1ClustersPhi",     &InDet_pixel_L1ClustersPhi);
  tree->SetBranchAddress("InDet_pixel_L2ClustersPhi",     &InDet_pixel_L2ClustersPhi);
  tree->SetBranchAddress("InDet_pixel_IBLClustersCharge",    &InDet_pixel_IBLClustersCharge);
  tree->SetBranchAddress("InDet_pixel_BLayerClustersCharge", &InDet_pixel_BLayerClustersCharge);
  tree->SetBranchAddress("InDet_pixel_L1ClustersCharge",     &InDet_pixel_L1ClustersCharge);
  tree->SetBranchAddress("InDet_pixel_L2ClustersCharge",     &InDet_pixel_L2ClustersCharge);
  //
  tree->SetBranchAddress("Pix_numCluster_IBL",    &Pix_numCluster_IBL);
  tree->SetBranchAddress("Pix_numCluster_BLayer", &Pix_numCluster_BLayer);
  tree->SetBranchAddress("Pix_numCluster_L1",     &Pix_numCluster_L1);
  tree->SetBranchAddress("Pix_numCluster_L2",     &Pix_numCluster_L2);
  //
  tree->SetBranchAddress("Reco_tau_pt",                        &Reco_tau_pt);
  tree->SetBranchAddress("Reco_tau_eta",                       &Reco_tau_eta);
  tree->SetBranchAddress("Reco_tau_phi",                       &Reco_tau_phi);
  tree->SetBranchAddress("Reco_tau_e",                         &Reco_tau_e);
  tree->SetBranchAddress("Reco_tau_numTracks",          &Reco_tau_numTracks);
  tree->SetBranchAddress("Reco_tau_isSelected",                  &Reco_tau_isSelected);
  //
  tree->SetBranchAddress("tt_numberOfContribPixelLayers",      &tt_numberOfContribPixelLayers);
  tree->SetBranchAddress("tt_numberOfBLayerHits",              &tt_numberOfBLayerHits       );
  tree->SetBranchAddress("tt_numberOfPixelHits",               &tt_numberOfPixelHits        );
  tree->SetBranchAddress("tt_numberOfSCTHits",                 &tt_numberOfSCTHits          );
  tree->SetBranchAddress("tt_numberOfTRTHits",                 &tt_numberOfTRTHits          );
  tree->SetBranchAddress("tt_numberOfBLayerSharedHits",        &tt_numberOfBLayerSharedHits        );
  tree->SetBranchAddress("tt_numberOfPixelSharedHits",         &tt_numberOfPixelSharedHits        );
  tree->SetBranchAddress("tt_numberOfSCTSharedHits",           &tt_numberOfSCTSharedHits        );
  tree->SetBranchAddress("tt_numberOfTRTSharedHits",           &tt_numberOfTRTSharedHits        );
  tree->SetBranchAddress("tt_numberOfPixelHoles",              &tt_numberOfPixelHoles        );
  tree->SetBranchAddress("tt_numberOfSCTHoles",                &tt_numberOfSCTHoles         );
  tree->SetBranchAddress("tt_numberOfSCTDoubleHoles",          &tt_numberOfSCTDoubleHoles        );
  tree->SetBranchAddress("tt_numberOfTRTHoles",                &tt_numberOfTRTHoles         );
  tree->SetBranchAddress("tt_numberOfBLayerOutliers",          &tt_numberOfBLayerOutliers        );
  tree->SetBranchAddress("tt_numberOfPixelOutliers",           &tt_numberOfPixelOutliers        );
  tree->SetBranchAddress("tt_numberOfSCTOutliers",             &tt_numberOfSCTOutliers        );
  tree->SetBranchAddress("tt_numberOfTRTOutliers",             &tt_numberOfTRTOutliers        );
  tree->SetBranchAddress("tt_numberOfPixelSpoiltHits",         &tt_numberOfPixelSpoiltHits        );
  tree->SetBranchAddress("tt_numberOfSCTSpoiltHits",           &tt_numberOfSCTSpoiltHits        );
  tree->SetBranchAddress("tt_expectBLayerHit",                 &tt_expectBLayerHit          );
  tree->SetBranchAddress("tt_expectInnermostPixelLayerHit",    &tt_expectInnermostPixelLayerHit              );
  tree->SetBranchAddress("tt_numberOfInnermostPixelLayerHits", &tt_numberOfInnermostPixelLayerHits              );
  tree->SetBranchAddress("tt_numberOfNextToInnermostPixelLayerHits", &tt_numberOfNextToInnermostPixelLayerHits              );
  tree->SetBranchAddress("tt_numberOfGangedPixels",            &tt_numberOfGangedPixels              );
  tree->SetBranchAddress("tt_numberOfGangedFlaggedFakes",      &tt_numberOfGangedFlaggedFakes              );
  tree->SetBranchAddress("tt_numberOfPixelDeadSensors",        &tt_numberOfPixelDeadSensors              );
  tree->SetBranchAddress("tt_numberOfSCTDeadSensors",          &tt_numberOfSCTDeadSensors              );
  tree->SetBranchAddress("tt_numberOfTRTDeadStraws",           &tt_numberOfTRTDeadStraws              );
  //
  tree->SetBranchAddress("Reco_track_numberOfInnermostPixelHits",            &Reco_track_numberOfInnermostPixelHits);
  tree->SetBranchAddress("Reco_track_numberOfNextToInnermostPixelHits",      &Reco_track_numberOfNextToInnermostPixelHits);
  tree->SetBranchAddress("Reco_track_numberOfPixelL1Hits",                   &Reco_track_numberOfPixelL1Hits);
  tree->SetBranchAddress("Reco_track_numberOfPixelL2Hits",                   &Reco_track_numberOfPixelL2Hits);
  //
  ///////////////////////////////////////////
  // Reconstructed track info 
  tree->SetBranchAddress( "Reco_track_pt",                     &Reco_track_pt);
  tree->SetBranchAddress( "Reco_track_eta",                    &Reco_track_eta);
  tree->SetBranchAddress( "Reco_track_phi",                    &Reco_track_phi);
  tree->SetBranchAddress( "Reco_track_e",                      &Reco_track_e);
  tree->SetBranchAddress( "Reco_track_m",                      &Reco_track_m);
  tree->SetBranchAddress( "Reco_track_d0",                     &Reco_track_d0);
  tree->SetBranchAddress( "Reco_track_z0",                     &Reco_track_z0);
  tree->SetBranchAddress("Reco_track_numberOfPixelHits",       &Reco_track_numberOfPixelHits);
  //_//_//_//_//_//_//_//_//_//_//_//_//_//_//_//_//_//_//_//_//_//_//_//_ //_//_//_//_//_//_//_//_//_//_//_//_//_//_//_//_//_//_//_//_//_//_//_//
  //
  //                                  The decralation of histograms
  //
  //_//_//_//_//_//_//_//_//_//_//_//_//_//_//_//_//_//_//_//_//_//_//_//_ //_//_//_//_//_//_//_//_//_//_//_//_//_//_//_//_//_//_//_//_//_//_//_//
  /// truth momentum
  // (1) Tau momentum
  TH1D* h_truth_pt_all                      = new TH1D("h_truth_pt_all",";pT [GeV];Events"        ,200, 0, 1200);
  TH1D* h_TruthPtHadronic                 = new TH1D("h_TruthPtHadronic",";pT [GeV];Events"   , 50, 0, 1200);
  TH1D* h_truth_pt_had[2];
  for (int iProng=0; iProng<2; iProng++){
    h_truth_pt_had[iProng] = new TH1D( Form("h_truth_pt_had_%d", iProng ), ";pT [GeV];Events"     ,50, 0, 1400);
  }
  TH1D* h_truth_pt_leptonic                 = new TH1D("h_truth_pt_leptonic",";pT [GeV];Events"   ,200, 0, 1200);
  TH1D* h_truth_pt_electron                 = new TH1D("h_truth_pt_electron",";pT [GeV];Events"   ,200, 0, 1200);
  TH1D* h_truth_pt_muon                     = new TH1D("h_truth_pt_muon",";pT [GeV];Events"       ,200, 0, 1200);
  // (2) Neutrino momentum
  TH1D* h_neutrino_pt_all                   = new TH1D("h_neutrino_pt_all",";pT [GeV];Events"     ,200, 0, 1200);
  TH1D* h_neutrino_pt_hadronic              = new TH1D("h_neutrino_pt_hadronic",";pT [GeV];Events",200, 0, 1200);
  TH1D* h_neutrino_pt_leptonic              = new TH1D("h_neutrino_pt_leptonic",";pT [GeV];Events",200, 0, 1200);

  /// Reconstrusted momentum
  TH1D* h_Reco_tau_pt_all           = new TH1D("h_Reco_tau_pt_all",";pT [GeV];Events"             ,200, 0, 1200);
  TH1D* h_Reco_tau_pt_all_EleOLR    = new TH1D("h_Reco_tau_pt_all_EleOLR",";pT [GeV];Events"      ,200, 0, 1200);
  TH1D* h_Reco_tau_pt_all_MuonOLR   = new TH1D("h_Reco_tau_pt_all_MuonOLR",";pT [GeV];Events"     ,200, 0, 1200);
  TH1D* h_Reco_tau_pt_hadronic      = new TH1D("h_Reco_tau_pt_hadronic",";pT [GeV];Events"        , 50, 0, 1400);
  TH1D* h_Reco_tau_pt_had[2][7];
  for ( int iTruthProng=0; iTruthProng<2; iTruthProng++){
    for ( int iProng=0; iProng<7; iProng++ ){
      h_Reco_tau_pt_had[iTruthProng][iProng] = new TH1D( Form("h_Reco_tau_pt_had_truth%d_reco%d", iTruthProng, iProng), "", 50, 0, 1400);
    }
  }
  TH1D* h_Reco_tau_pt_leptonic      = new TH1D("h_Reco_tau_pt_leptonic",";pT [GeV];Events"        ,200, 0, 1200);

  /// _DecayRadius
//  Double_t xbin[] = {0,10,20,30,40,50,60,70,80,90,100,120,140,160,180,200,220,250};
  TH1D* h_truth_decayRad[5];
  std::string DecayRadCateg[5] = {"all", "1prong", "3prong", "1prong_reco1p", "3prong_reco3p"};
  for ( int iCateg=0; iCateg<5; iCateg++){
    //h_truth_decayRad[iCateg] = new TH1D( Form("h_truth_decayRad_%s", DecayRadCateg[iCateg].c_str()), ";decay radius [mm];Events", 17, xbin);
    h_truth_decayRad[iCateg] = new TH1D( Form("h_truth_decayRad_%s", DecayRadCateg[iCateg].c_str()), ";decay radius [mm];Events", 50, 0,500);
  }

  /// _DecayRadius_vs_NumberOfXXX
  TH1D* h_numberOfPixelHits[2][5];
  TH1D* h_numberOfSCTHits[2][5];
  TH1D* h_numberOfRawPixelHits[2][5];
  for(int iProng = 0; iProng<2; iProng++){
    for(int iDecayRegion =0; iDecayRegion<5; iDecayRegion++){
      h_numberOfPixelHits[iProng][iDecayRegion]    = new TH1D( Form("h_numberOfPixelHits_%d_Region%d",    iProng, iDecayRegion), "", 11,-0.5, 10.5);
      h_numberOfSCTHits[iProng][iDecayRegion]      = new TH1D( Form("h_numberOfSCTHits_%d_Region%d",      iProng, iDecayRegion), "", 19,-0.5, 18.5);
      h_numberOfRawPixelHits[iProng][iDecayRegion] = new TH1D( Form("h_numberOfRawPixelHits_%d_Region%d", iProng, iDecayRegion), "", 11,-0.5, 10.5);
    }
  }

  // _numberOfXXX
  TH2D* h_numberOfPixelHitsDecayRegion                     = new TH2D("h_numberOfPixelHitsDecayRegion","", 50, 0, 250, 11, -0.5, 10.5);
  TH2D* h_numberOfPixelHits_InnerMost_vs_DecayRegion       = new TH2D("h_numberOfPixelHits_InnerMost_vs_DecayRegion","", 50, 0, 250, 3, -0.5, 2.5);
  TH2D* h_numberOfPixelHits_NextToInnerMost_vs_DecayRegion = new TH2D("h_numberOfPixelHits_NextToInnerMost_vs_DecayRegion","", 50, 0, 250, 3, -0.5, 2.5);
  TH2D* h_numberOfSCTHits_vs_DecayRegion                   = new TH2D("h_numberOfSCTHits_vs_DecayRegion","",   50, 0, 250, 18, -0.5, 17.5);

  // _numberOfPixelHits_DecayRadius
  TH2D* h_numberOfPixelHitsDecayRadius[2][5];
  for (int iTruthProng=0; iTruthProng<2; iTruthProng++){
    for (int iRecoProng=0; iRecoProng<5; iRecoProng++){
      h_numberOfPixelHitsDecayRadius[iTruthProng][iRecoProng] = new TH2D( Form("h_numberOfPixelHitsDecayRadius_truth%d_reco%d", iTruthProng, iRecoProng), "", 50, 0, 250, 11, -0.5, 10.5);
    }
  }

  // _numberOfPixelHits_Eta
  TH2D* h_numberOfPixelHitsEta[2][6];
  for (int iTruthProng=0; iTruthProng<2; iTruthProng++){
    for (int iRegion=0; iRegion<6; iRegion++){
      h_numberOfPixelHitsEta[iTruthProng][iRegion] = new TH2D( Form("h_numberOfPixelHitsEta_truth%d_Region%d", iTruthProng, iRegion), "", 50, -2.5, 2.5, 11, -0.5, 10.5);
    }
  }

  // _Efficiency_DecayRadius
  TH1D* h_number_of_tracks[2][8];
  double xBinning [40] = {0,5,10,15,20,25,30,35,40,45,50,55,60,65,70,75,80,85,90,95,100,105,110,115,120,125,135,140,145,150,160,170,180,190,200,210,220,230,240,250};
  // [2] : 0 for 1-prong, 1 for 3-prong 
  // [8] : 0 ~ 4 for reco-prong, 
  //       5 for not truth matched events,
  //       6 for the truth matched reco-tau
  //       7 for the truth tau, 
  for ( int iProng=0; iProng<2; iProng++){
    for ( int iRecoProng=0; iRecoProng<8; iRecoProng++){
      h_number_of_tracks[iProng][iRecoProng] = new TH1D(Form("h_number_of_tracks_%d_%d", iProng, iRecoProng), "", 39, xBinning );
    }
  }
  // _Efficiency_D0
  TH1D* h_numTracksVSd0[2];
  TH1D* h_numTracksVSz0[2];
  // [2] : 0 for 1-prong, 1 for 3-prong 
  // [8] : 0 ~ 4 for reco-prong, 
  //       5 for not truth matched events,
  //       6 for the truth matched reco-tau
  //       7 for the truth tau, 
  for ( int iProng=0; iProng<2; iProng++){
    h_numTracksVSd0[iProng] = new TH1D(Form("h_numTracksVSd0_%d", iProng), ";d0;Events", 200, -1, 1);
    h_numTracksVSz0[iProng] = new TH1D(Form("h_numTracksVSz0_%d", iProng), ":z0;Events", 300, -150, 150);
  }

  // ** Basic kinematics ** //
  // _Basic_Pix_clusters
  //  index
  //   0 : IBL
  //   1 : B-Layer
  //   2 : L1
  //   3 : L2
  TH1D* h_Pix[4];
  for (int iLayer=0; iLayer<4; iLayer++){
    h_Pix[iLayer] = new TH1D( Form("h_Pix_Layer%d", iLayer), "", 20, 0,20);
  }
  // _Basic_kinematics
  TH1D* h_numTracks[2];
  for ( int iHadLep=0; iHadLep<2; iHadLep++){
    h_numTracks[iHadLep] = new TH1D( Form("h_numTracks%d", iHadLep),";# of tracks;Events", 50, 0,50 );
  }

  // _numTrack_vs_DecayRadius
  TH2D* h_numTrackDecayR = new TH2D("h_numTrackDecayR",";;", 50, 0, 200, 100, 0, 100);
  // _chargedTrack_distribution
  
  TH1D* h_truthTrackPt    = new TH1D("h_truthTrackPt" , ";pT [GeV];Events", 1000, -1, 1);
  TH1D* h_truthTrackPt_3prong    = new TH1D("h_truthTrackPt_3prong" , ";pT [GeV];Events", 50, 0, 1500);
  TH1D* h_truthTrackEta   = new TH1D("h_truthTrackEta", "", 100, -3, 3);
  TH1D* h_truthTrackPhi   = new TH1D("h_truthTrackPhi", "", 100, TMath::Pi(), TMath::Pi());

  // _numberOfTracks
  TH1D* h_InDetTrack_num_1prong  = new TH1D("h_InDetTrack_num_1prong", "", 60, 0, 60);
  TH1D* h_InDetTrack_num_1prong_outer  = new TH1D("h_InDetTrack_num_1prong_outer", "", 60, 0, 60);
  TH1D* h_InDetTrack_num_3prong  = new TH1D("h_InDetTrack_num_3prong", "", 60, 0, 60);
  TH1D* h_InDetTrack_num_3prong_outer  = new TH1D("h_InDetTrack_num_3prong_outer", "", 60, 0, 60);
  
  // _numberOfTracks_optimized
  TH1D* h_numberOfTracks[10];
  for ( unsigned int iOpt=0; iOpt<10; iOpt++){
    h_numberOfTracks[iOpt] = new TH1D( Form("h_numberOfTracks%d", iOpt), "", 100, 0, 100);
  }

  /* Track momentum 
   *  - tracks from truth tau 
   *  - tracks from truth tau matched with reco-tracks
   *  -- h_TruthTrackRecoMatched_Pt
   *      0 : reco exact 1-prong
   *      4 : reco exact 3-prong
   *  -- h_TruthTrack_PtExact
    
   *  - tracks from reco tau 
   *  -- h_InDetTrack_Pt
   *      0 : reco exact 1-prong
   *      4 : reco exact 3-prong
   *  - tracks from reco tau with optimized requirement
   *  */
  TH1D* h_TruthTrack_Pt[10];
  TH1D* h_TruthTrack_QOverPt[10];
  TH1D* h_TruthTrackRecoMatched_Pt[10];
  TH1D* h_InDetTrack_Pt[10];
  TH1D* h_InDetTrack_QOverPt[10];
  /* 0 : truth 1-prong && exact reco 1 track */
  TH1D* h_InDetTrack_Pt_Optimized[10];
  // Optimized : pixel + SCT > 9 hits
  for( unsigned int iType=0; iType<10; iType++){
    h_TruthTrack_Pt[iType]           = new TH1D( Form("h_TruthTrack_Pt%d", iType) ,            ";pT [GeV];Events", 50, 0, 1500);
    h_TruthTrack_QOverPt[iType]      = new TH1D( Form("h_TruthTrack_QOverPt%d", iType) ,       ";pT [GeV];Events", 100, -0.7, 0.7);
    h_TruthTrackRecoMatched_Pt[iType]= new TH1D( Form("h_TruthTrackRecoMatched_Pt%d", iType) , ";pT [GeV];Events", 50, 0, 1500);
    h_InDetTrack_Pt[iType]           = new TH1D( Form("h_InDetTrack_Pt%d", iType) ,            ";pT [GeV];Events", 50, 0, 1500);
    h_InDetTrack_QOverPt[iType]      = new TH1D( Form("h_InDetTrack_QOverPt%d", iType) ,       ";pT [GeV];Events", 100, -0.7, 0.7);
    h_InDetTrack_Pt_Optimized[iType] = new TH1D( Form("h_InDetTrack_Pt_Optimized%d", iType) ,  ";pT [GeV];Events", 50, 0, 1500);
  }
  TH1D* h_TruthTrack_PtExact[2][10];
  for ( unsigned int iProng = 0; iProng<2; iProng++){
    for ( unsigned int iRange = 0; iRange<10; iRange++){
      h_TruthTrack_PtExact[iProng][iRange] = new TH1D( Form("h_TruthTrack_PtExact%d%d", iProng, iRange) ,  ";pT [GeV];Events", 50, 0, 1500);
    }
  }

  /* Tau eta distributions 
   * */
  TH1D* h_TruthTau_Eta[2];
  TH2D* h_TruthTau_TruthDecVtx_Eta[2];
  TH2D* h_TruthTau_TruthTrack_Eta[2];
  TH2D* h_TruthTrack_TruthDecVtx_Eta[2];
  for ( unsigned int iProng = 0; iProng<2; iProng++){
    h_TruthTau_Eta[iProng]               =  new TH1D( Form("h_TruthTau_Eta%d", iProng),             "", 100, -3,3);
    h_TruthTau_TruthDecVtx_Eta[iProng]   =  new TH2D( Form("h_TruthTau_TruthDecVtx_Eta%d", iProng),   "", 100, -3,3, 100, -3, 3);
    h_TruthTau_TruthTrack_Eta[iProng]    =  new TH2D( Form("h_TruthTau_TruthTrack_Eta%d", iProng),    "", 100, -3,3, 100, -3, 3);
    h_TruthTrack_TruthDecVtx_Eta[iProng] =  new TH2D( Form("h_TruthTrack_TruthDecVtx_Eta%d", iProng), "", 100, -3,3, 100, -3, 3);
  }

  /* Truth decay vertex properties 
   * */
  TH1D* h_TruthDecVtx_Eta[2];
  for ( unsigned int iProng = 0; iProng<2; iProng++){
    h_TruthDecVtx_Eta[iProng] =  new TH1D( Form("h_TruthDecVtx_Eta%d", iProng), "", 100, -3,3);
  }
  
  /* Track properties
   * - q/pt
   * - eta
   * - phi
   *   */
  // Truth track
  TH1D* h_TruthTrackEta[2];
  TH1D* h_TruthTrackPhi[2];
  // Reco track
  TH1D* h_InDetTrack_qOverPt[2];
  TH1D* h_InDetTrack_RawqOverPt[2];
  TH1D* h_TruthTrackQOverPt[2];
  TH1D* h_InDetTrack_Eta[2];
  TH1D* h_InDetTrack_Phi[2];
  TH1D* h_InDetTrack_RawEta[2];
  TH1D* h_InDetTrack_RawPhi[2];
  // q/pt 
  TH2D* h_TruthTrack_InDetTrac_qOverPt[2];
  // Vertex information 
  TH1D* h_ProdVtx_X[2];
  TH1D* h_ProdVtx_Y[2];
  TH1D* h_ProdVtx_Z[2];
  TH1D* h_ProdVtx_Eta[2];
  TH1D* h_ProdVtx_Phi[2];
  TH1D* h_ProdVtx_perp[2];
  //
  TH1D* h_DecVtx_X[2];
  TH1D* h_DecVtx_Y[2];
  TH1D* h_DecVtx_Z[2];
  TH1D* h_DecVtx_Eta[2];
  TH1D* h_DecVtx_Eta_Manual[2];
  TH1D* h_DecVtx_Phi[2];
  TH1D* h_DecVtx_perp[2];
  //
  TH2D* h_ProdVtx_Eta_DecVtx[2];
  /* Pixel clusters */
  TH1D* h_numberOfPixelClusters[2];
  TH1D* h_numberOfPixelClustersMuon[2];
  TH1D* h_PixelCluIBLEta[2];
  TH1D* h_PixelCluIBLDeltaR[2];
  TH1D* h_PixelCluBLayerDeltaR[2];
  TH1D* h_PixelCluL1DeltaR[2];
  TH1D* h_PixelCluL2DeltaR[2];
  //
  /* Reco tracks */
  TH1D* h_RecoTracksNumberOfPixelHits[2][2];
  //
  /* Reco tau vs tracks from the tau */
  TH2D* h_RecoTauRecoTracksEta[2];
  TH2D* h_RecoTauRecoTracksPhi[2];
  TH1D* h_RecoTauInDetTrack_DeltaR[2];
  
  for ( unsigned int iProng = 0; iProng<2; iProng++){
    h_TruthTrackEta         [iProng]       = new TH1D( Form("h_TruthTrackEta%d",    iProng) ,   ";TruthTrack #eta;", 100, -3, 3);
    h_TruthTrackPhi         [iProng]       = new TH1D( Form("h_TruthTrackPhi%d",    iProng) ,   ";TruthTrack #phi;", 100, TMath::Pi(), TMath::Pi());
    // Reco track 
    h_InDetTrack_qOverPt    [iProng]       = new TH1D( Form("h_InDetTrack_qOverPt%d", iProng) ,   ";InDetTrack  q/p_{T} [GeV];Events", 1000, -0.1, 0.1);
    h_TruthTrackQOverPt     [iProng]       = new TH1D( Form("h_TruthTrackQOverPt%d", iProng) ,   ";truth track q/p_{T} [GeV];Events", 1000, -0.1, 0.1);
    h_InDetTrack_Eta        [iProng]       = new TH1D( Form("h_InDetTrack_Eta%d",    iProng) ,   ";InDetTrack #eta;", 100, -3, 3);
    h_InDetTrack_Phi        [iProng]       = new TH1D( Form("h_InDetTrack_Phi%d",    iProng) ,   ";InDetTrack #phi;", 100, TMath::Pi(), TMath::Pi());
    h_InDetTrack_RawEta     [iProng]       = new TH1D( Form("h_InDetTrack_RawEta%d",    iProng) ,   ";InDetTrack #eta;", 100, -3, 3);
    h_InDetTrack_RawPhi     [iProng]       = new TH1D( Form("h_InDetTrack_RawPhi%d",    iProng) ,   ";InDetTrack #phi;", 100, TMath::Pi(), TMath::Pi());
    // Reco track without any matching requirment
    h_InDetTrack_RawqOverPt [iProng]       = new TH1D( Form("h_InDetTrack_RawqOverPt%d", iProng) ,  ";InDetTrack  q/p_{T} [GeV];Events", 1000, -0.1, 0.1);
    // Comparison
    h_TruthTrack_InDetTrac_qOverPt[iProng] = new TH2D(Form("h_TruthTrack_InDetTrac_qOverPt%d", iProng),  ";TruthTrack q/p_{T};InDetTrack  q/p_{T} [GeV]",100, -0.01, 0.01, 100, -0.01, 0.01);
    // Vertex
    h_ProdVtx_X             [iProng]       = new TH1D( Form("h_ProdVtx_X%d", iProng), ";X;Events", 100, -0.8, 0);
    h_ProdVtx_Y             [iProng]       = new TH1D( Form("h_ProdVtx_Y%d", iProng), ";Y;Events", 100, -0.8, 0);
    h_ProdVtx_Z             [iProng]       = new TH1D( Form("h_ProdVtx_Z%d", iProng), ";Z;Events", 100, -200, 200);
    h_ProdVtx_Eta           [iProng]       = new TH1D( Form("h_ProdVtx_Eta%d", iProng), "", 100, -7, 7);
    h_ProdVtx_Phi           [iProng]       = new TH1D( Form("h_ProdVtx_Phi%d", iProng), "", 100, -7, 7);
    h_ProdVtx_perp          [iProng]       = new TH1D( Form("h_ProdVtx_perp%d", iProng),"", 100, 0, 2);
    // Decay vertex
    h_DecVtx_X              [iProng]       = new TH1D( Form("h_DecVtx_X%d", iProng), ";X;Events", 100, -200, 200);
    h_DecVtx_Y              [iProng]       = new TH1D( Form("h_DecVtx_Y%d", iProng), ";Y;Events", 100, -200, 200);
    h_DecVtx_Z              [iProng]       = new TH1D( Form("h_DecVtx_Z%d", iProng), ";Z;Events", 100, -2000, 2000);
    h_DecVtx_Eta            [iProng]       = new TH1D( Form("h_DecVtx_Eta%d", iProng), "", 100, -7, 7);
    h_DecVtx_Eta_Manual     [iProng]       = new TH1D( Form("h_DecVtx_Eta_Manual%d", iProng), "", 100, -7, 7);
    h_DecVtx_Phi            [iProng]       = new TH1D( Form("h_DecVtx_Phi%d", iProng), "", 100, -7, 7);
    h_DecVtx_perp           [iProng]       = new TH1D( Form("h_DecVtx_perp%d", iProng),"", 100, 0, 2);
    //
    h_ProdVtx_Eta_DecVtx    [iProng]       = new TH2D( Form("h_ProdVtx_Eta_DecVtx%d", iProng), "", 100, 0, 100, 100, 0, 100);
    //
    h_numberOfPixelClusters [iProng]       = new TH1D( Form("h_numberOfPixelClusters%d", iProng), "", 400, 0, 400 );
    h_numberOfPixelClustersMuon [iProng]   = new TH1D( Form("h_numberOfPixelClustersMuon%d", iProng), "", 400, 0, 400 );
    h_PixelCluIBLEta        [iProng]       = new TH1D( Form("h_PixelCluIBLEta%d", iProng), "", 100, -3, 3 );
    h_PixelCluIBLDeltaR     [iProng]       = new TH1D( Form("h_PixelCluIBLDeltaR%d", iProng), "", 100, 0, 5 );
    h_PixelCluBLayerDeltaR  [iProng]       = new TH1D( Form("h_PixelCluBLayerDeltaR%d", iProng), "", 100, 0, 5 );
    h_PixelCluL1DeltaR      [iProng]       = new TH1D( Form("h_PixelCluL1DeltaR%d", iProng), "", 100, 0, 5 );
    h_PixelCluL2DeltaR      [iProng]       = new TH1D( Form("h_PixelCluL2DeltaR%d", iProng), "", 100, 0, 5 );
    //
    for ( int iIdRange=0; iIdRange<2; iIdRange++ ){
      h_RecoTracksNumberOfPixelHits[iProng][iIdRange]  = new TH1D( Form("h_RecoTracksNumberOfPixelHits%d%d",iProng, iIdRange), "", 10, 0, 10);
    }
    //
    h_RecoTauRecoTracksEta  [iProng]       = new TH2D( Form("h_RecoTauRecoTracksEta%d", iProng), "",100, -3,3, 100, -3,3);
    h_RecoTauRecoTracksPhi  [iProng]       = new TH2D( Form("h_RecoTauRecoTracksPhi%d", iProng), "",100, -TMath::Pi(),TMath::Pi(), 100,TMath::Pi(),TMath::Pi());
    h_RecoTauInDetTrack_DeltaR [iProng]    = new TH1D( Form("h_RecoTauInDetTrack_DeltaR%d", iProng), "",100, 0,0.1);
  }
  
  TH1D* h_chargedTrackEta = new TH1D("h_chargedTrackEta","", 100, -3,3);
  TH1D* h_chargedTrackPhi = new TH1D("h_chargedTrackPhi","", 100, -TMath::Pi(), TMath::Pi());
  TH1D* h_chargedTrackM   = new TH1D("h_chargedTrackM","",200,0,1000);

  // _ImpactParameter
  TH1D* h_ImpactParameter_d0 = new TH1D("h_ImpactParameter_d0", "", 100, -150,150 );
  TH1D* h_ImpactParameter_z0 = new TH1D("h_ImpactParameter_z0", "", 100, -200,200 );

  // _PixelHits
  // 0 : inside  Pixel
  // 1 : outside Pixel
  TH1D* h_PixelHits[10];
  TH1D* h_PixelHits_IBL[10];
  TH1D* h_PixelHits_BLayer[10];
  TH1D* h_PixelHits_L1[10];
  TH1D* h_PixelHits_L2[10];
  for ( unsigned int iType=0; iType<10; iType++){
    h_PixelHits[iType]        = new TH1D(Form("h_PixelHits%d",        iType),  ";# hits; Events", 10, 0, 10);
    h_PixelHits_IBL[iType]    = new TH1D(Form("h_PixelHits_IBL%d",    iType),  ";# hits; Events", 10, 0, 10);
    h_PixelHits_BLayer[iType] = new TH1D(Form("h_PixelHits_BLayer%d", iType),  ";# hits; Events", 10, 0, 10);
    h_PixelHits_L1[iType]     = new TH1D(Form("h_PixelHits_L1%d",     iType),  ";# hits; Events", 10, 0, 10);
    h_PixelHits_L2[iType]     = new TH1D(Form("h_PixelHits_L2%d",     iType),  ";# hits; Events", 10, 0, 10);
  }
  TH1D* h_InDet_track_numberOfPixelHits[10];
  TH1D* h_InDet_track_numberOfSCTHits[10];
  for ( int iType=0; iType<10; iType++){
    h_InDet_track_numberOfPixelHits[iType] = new TH1D( Form("h_InDet_track_numberOfPixelHits%d", iType ), "", 10, 0, 10);
    h_InDet_track_numberOfSCTHits[iType]   = new TH1D( Form("h_InDet_track_numberOfSCTHits%d", iType ),   "", 10, 0, 10);
  }

  // _PixelHits_vs_DecayRadius
  TH2D* h_PixelHits_DecayRad = new TH2D("h_PixelHits_DecayRad", "", 50, 0,200, 10, 0,10);
  TH1D* h_PixelHits_DecayRegion[5];
  for ( int iReg=0; iReg<5; iReg++){
   h_PixelHits_DecayRegion[iReg] = new TH1D( Form("h_PixelHits_DecayRegion_%d", iReg), "", 10, 0,10);
  }

  // _TruthPt_RecoPt
  TH2D* h_truthTrackRecoTrack[2];
  h_truthTrackRecoTrack[0] = new TH2D( Form("h_truthTrackRecoTrack%dprong",1 ), "", 500, -0.1,0.1, 500,-0.1,0.1);
  h_truthTrackRecoTrack[1] = new TH2D( Form("h_truthTrackRecoTrack%dprong",3 ), "", 500, -0.1,0.1, 500,-0.1,0.1);

  /* _DeltaR 
   *
   * h_DeltaR_AllTracks : delta R between truth track and all of reco-tracks
   * h_DeltaR           :         between truth track and exact events 
   * */
  TH2D* h_DeltaR_AllTracks[2];
  TH1D* h_DeltaR_PtSlice10GeV[2][100];
  //
  TH1D* h_DeltaR_AllTracks_1D[2];
  TH1D* h_DeltaR_AllTracks_1D_3prong[3];
  //
  TH2D* h_DeltaR[2];
  TH1D* h_DeltaR_1D[2];
  TH1D* h_DeltaR_TruthTauTruthTrack = new TH1D("h_DeltaR_TruthTauTruthTrack", "", 1000, 0, 1);
  for ( int iProng=0; iProng<2; iProng++){
    h_DeltaR[iProng]              = new TH2D( Form("h_DeltaR%d",iProng),              ";p_{T} [GeV];#Delta R", 50, 0, 1000, 5000, 0, 0.5);
    h_DeltaR_AllTracks[iProng]    = new TH2D( Form("h_DeltaR_AllTracks%d",iProng),    ";truth track p_{T} [GeV];#Delta R (truth and reco)", 50, 0, 1000, 5000, 0, 0.5);
    h_DeltaR_AllTracks_1D[iProng] = new TH1D( Form("h_DeltaR_AllTracks_1D%d",iProng), ";#Delta R;Events", 100, 0, 0.5);
    h_DeltaR_1D[iProng]           = new TH1D( Form("h_DeltaR_1D%d",iProng),           ";#Delta R; Events",100, 0, 0.5);
    for ( int iPtRange=0; iPtRange<100; iPtRange++){
      h_DeltaR_PtSlice10GeV[iProng][iPtRange] = new TH1D( Form("h_DeltaR_PtSlice10GeV%d%d", iProng, iPtRange), ";#Delta R;Events", 50, 0, 0.015);
    }
  }
  for ( int iEachProng=0; iEachProng<2; iEachProng++){
    h_DeltaR_AllTracks_1D_3prong[iEachProng]  = new TH1D( Form("h_DeltaR_AllTracks_1D_3prong%d",iEachProng),  ";#Delta R;Events", 100, 0, 0.5);
  }

  /* Matching requirement */
  TH2D* h_qOverPtVsDeltaR[2];
  TH2D* h_TruthPtVsNumTracks[2];
  for ( int iProng=0; iProng<2; iProng++){
    h_qOverPtVsDeltaR   [iProng]        = new TH2D( Form("h_qOverPtVsDeltaR%d",   iProng), ";q/p_{T} [GeV];#Delta R",       50, -1,1, 5000, 0, 0.5);
    h_TruthPtVsNumTracks[iProng]        = new TH2D( Form("h_TruthPtVsNumTracks%d",iProng), ";p_{T} [GeV];Number of tracks", 50, 0, 1000, 50, 0, 50);
  }

  /* _TruthPt_MatchedTruthPt */
  TH1D* h_TruthTrackPt       = new TH1D( "h_TruthTrackPt",       ";p_{T} [GeV]; Events", 50, 0, 1000);
  TH1D* h_TruthPt3prong = new TH1D( "h_TruthPt3prong", ";p_{T} [GeV]; Events", 50, 0, 1000);
  TH1D* h_TruthTrackPt_MatchedTruthPt[2][5];
  for ( int iProng=0; iProng<2; iProng++){
    for ( int iRegion=0; iRegion<5; iRegion++){
    h_TruthTrackPt_MatchedTruthPt[iProng][iRegion]   = new TH1D( Form("h_TruthTrackPt_MatchedTruthPt%d%d",iProng,iRegion),  ";truth track p_{T};Events", 50, 0, 1000);
    }
  }
  
  /* Neutrino */
  TH2D* h_Neutrino_TruthTrack_Pt             = new TH2D("h_Neutrino_TruthTrack_Pt",";#nu_{#tau} p_{T} [GeV];track p_{T} [GeV]", 100,0,1000,100,0,1000);
  TH2D* h_TrackPt_NeutrinoTrack_OpeningAngle = new TH2D("h_TrackPt_NeutrinoTrack_OpeningAngle",";track p_{T} [GeV];Opening andle #theta [rad]", 100,0,1000, 1000,0,0.05);
  TH2D* h_TrackPt_TauTrack_OpeningAngle      = new TH2D("h_TrackPt_TauTrack_OpeningAngle",   ";track p_{T} [GeV];Opening andle #theta [rad]", 100,0,1000, 1000,0,0.05);
  TH1D* h_Neutrino_DeltaR                    = new TH1D("h_Neutrino_DeltaR",         "", 100, 0, 1);
  TH2D* h_NeutrinoPt_DeltaR                  = new TH2D("h_NeutrinoPt_DeltaR",       "", 100, 0,1000, 100, 0, 1);
  TH2D* h_NeutrinoPt_NumPixelHits            = new TH2D("h_NeutrinoPt_NumPixelHits", "", 100, 0,1000, 10, 0, 10);
  


  TH1D* h_TrackPt_NumTracks[10];
  TH1D* h_Spec_NumberOfPixelHits[10];
  for ( int iPtRange=0; iPtRange<10; iPtRange++){
    h_TrackPt_NumTracks[iPtRange] = new TH1D( Form("h_TrackPt_NumTracks%d",iPtRange), ";The number of tracks;", 7, 0, 7);
    h_Spec_NumberOfPixelHits[iPtRange]   = new TH1D( Form("h_Spec_NumberOfPixelHits%d",iPtRange), ";the number of pixel hits;", 10, 0, 10);
  }

  /* Special event 
   *   This special event is " tau -> nu + pi^+ ".
   *  */
  TH1D* h_Spec_RecoTauPt            = new TH1D("h_Spec_RecoTauPt", "",  100,0,1000);
  TH1D* h_Spec_RecoTauEta            = new TH1D("h_Spec_RecoTauEta", "", 100, -3,3);
  TH1D* h_Spec_RecoTauPhi            = new TH1D("h_Spec_RecoTauPhi", "", 100, -TMath::Pi(), TMath::Pi());
  TH1D* h_Spec_InDetTrack_NumTracks  = new TH1D("h_Spec_InDetTrack_NumTracks", "", 100, 0, 100);

  /* 
   * Pixel delta R */
  TH1D* h_PixelClusterEta       [4];
  TH1D* h_PixelClusterEtaCategoryByCategory[4][10];
  TH1D* h_TauPixelMinimumDeltaR     [4][2];
  TH1D* h_TauPixelDeltaR            [4][2];
  TH1D* h_PixelHittedLayer      [2];
  TH1D* h_PixelClustersNum   [4][2];
  TH1D* h_PixelClustersCharge[4][2];
  for ( int iInsideOutside=0; iInsideOutside<2; iInsideOutside++){
    h_PixelHittedLayer[iInsideOutside] = new TH1D( Form("h_PixelHittedLayer%d", iInsideOutside), ";The number of hitted layer;", 6, 0, 6 );
  }
  for (int iLayer=0; iLayer<4; iLayer++){
    h_PixelClusterEta  [iLayer]                   = new TH1D( Form("h_PixelClusterEta%d", iLayer), ";#eta_{Cluster};", 100, -4, 4);
    for ( int iInsideOutside=0; iInsideOutside<2; iInsideOutside++){
      h_TauPixelMinimumDeltaR   [iLayer][iInsideOutside] = new TH1D( Form("h_TauPixelMinimumDeltaR%d%d", iLayer, iInsideOutside), "", 100, 0, 0.1);
      h_TauPixelDeltaR          [iLayer][iInsideOutside] = new TH1D( Form("h_TauPixelDeltaR%d%d",        iLayer, iInsideOutside), "", 100, 0, 3);
      h_PixelClustersNum        [iLayer][iInsideOutside] = new TH1D( Form("h_PixelClustersNum%d%d", iLayer, iInsideOutside), ";the number of clusters;", 100, 0, 100);
      h_PixelClustersCharge     [iLayer][iInsideOutside] = new TH1D( Form("h_PixelClustersCharge%d%d", iLayer, iInsideOutside), ";Depoist charge;", 50, 0, 200);
    }
    for ( int iCategory=0; iCategory<10; iCategory++){
      h_PixelClusterEtaCategoryByCategory[iLayer][iCategory] = new TH1D( Form("h_PixelClusterEtaCategoryByCategory%d%d", iLayer, iCategory), "", 100, -4, 4);
    }
  }

  /*
   * Event display 
   * */
  Int_t EventDisplayTargetEvent = 389; // Truth_tau_decayRad > 122.4 
//  Int_t EventDisplayTargetEvent = 339; // Truth_tau_decayRad > 122.4 && numberOfPixelHittedLayer == 4
  TGraph* h_EventDisplay_PixelCluster           [ EventDisplayTargetEvent ];
  TGraph* h_EventDisplay_PixelCluster_Classified[ EventDisplayTargetEvent ];
  TGraph* h_EventDisplay_PixelCluster_RZ        [ EventDisplayTargetEvent ];
  TGraph* h_EventDisplay_DecayVtx               [ EventDisplayTargetEvent ];
  TGraph* h_EventDisplay_DecayVtx_RZ            [ EventDisplayTargetEvent ];
  for ( int iEvent=0; iEvent< EventDisplayTargetEvent ; iEvent++){
    h_EventDisplay_PixelCluster[iEvent]            = new TGraph();
    h_EventDisplay_PixelCluster_Classified[iEvent] = new TGraph();
    h_EventDisplay_PixelCluster_RZ[iEvent]         = new TGraph();
    h_EventDisplay_DecayVtx[iEvent]                = new TGraph();
    h_EventDisplay_DecayVtx_RZ[iEvent]             = new TGraph();
  }
  std::vector<std::vector<TLatex*>> h_TTextTrackInfo    (389);
  std::vector<std::vector<TText*>>  h_TTextClusterPdgId  (1640);
  for ( int i=0; i< EventDisplayTargetEvent; i++){
    h_TTextTrackInfo[i]    .clear();
  }
  for ( int i=0; i< 1640; i++){
    h_TTextClusterPdgId[i]    .clear();
  }

  //_//_//_//_//_//_//_//_//_//_//_//_//_//_//_//_//_//_//_//_//_//_//_//_ //_//_//_//_//_//_//_//_//_//_//_//_//_//_//_//_//_//_//_//_//_//_//_//
  //
  //                             Main Logic 
  //
  //_//_//_//_//_//_//_//_//_//_//_//_//_//_//_//_//_//_//_//_//_//_//_//_ //_//_//_//_//_//_//_//_//_//_//_//_//_//_//_//_//_//_//_//_//_//_//_//
  std::cout << "The total number of events : " << tree->GetEntries() << std::endl;
  int TargetEventCounter=0;
  EventCounter eventCounter;
  Double_t XPositionLogger[ EventDisplayTargetEvent ][4];
  Double_t YPositionLogger[ EventDisplayTargetEvent ][4];
  Double_t ZPositionLogger[ EventDisplayTargetEvent ][4];
  
  // truth tau position logger
  Double_t XTauPositionLogger   [ EventDisplayTargetEvent ][2];
  Double_t YTauPositionLogger   [ EventDisplayTargetEvent ][2];
  Double_t ZTauPositionLogger   [ EventDisplayTargetEvent ][2];
  Double_t RTauPositionLogger   [ EventDisplayTargetEvent ][2];
  int      TruthTauNumberOfPixelHitsLogger[EventDisplayTargetEvent];

  // Truth neutrino logger
  Double_t XNeutrinoPositionLogger [EventDisplayTargetEvent][2];
  Double_t YNeutrinoPositionLogger [EventDisplayTargetEvent][2];
  
  // truth track position logger
  Double_t XTrackPositionLogger[ EventDisplayTargetEvent ][2];
  Double_t YTrackPositionLogger[ EventDisplayTargetEvent ][2];
  Double_t ZTrackPositionLogger[ EventDisplayTargetEvent ][2];
  Double_t RTrackPositionLogger[ EventDisplayTargetEvent ][2];

  Double_t TrackPtLogger        [EventDisplayTargetEvent];
  Double_t TrackTauThetaLoogger [EventDisplayTargetEvent];
  
  // InDetTrack position logger
  std::vector<std::vector< double > > XInDetTrackPositionLogger(1640);
  std::vector<std::vector< double > > YInDetTrackPositionLogger(1640);
  std::vector<std::vector< double > > ZInDetTrackPositionLogger(1640);
  std::vector<std::vector< int > >    NumberOfPixelHitsLogger(1640);
  std::vector<std::vector< int > >    NumberOf_IBL_PixelHitsLogger(1640);
  std::vector<std::vector< int > >    NumberOf_BLayer_PixelHitsLogger(1640);
  std::vector<std::vector< int > >    NumberOf_L1_PixelHitsLogger(1640);
  std::vector<std::vector< int > >    NumberOf_L2_PixelHitsLogger(1640);
  
  // InDet track information logger
  std::vector<std::vector<int>>    InDetBarcodeLogger(1640);
  //
  std::vector<std::vector<int>>    InDetBarcodeLogger_BL       (1640);
  //
  std::vector<std::vector<int>>    InDetBarcodeLogger_L1       (1640);
  //
  std::vector<std::vector<int>>    InDetBarcodeLogger_L2       (1640);

  for(int iEntry=0; iEntry< tree->GetEntries(); iEntry++){
    tree->GetEntry(iEntry);

    if ( iEntry > 20000) break;
    
    /* Count the number of event in each region */
    eventCounter.all++;
    if ( Truth_tau_isHadronic ){
      eventCounter.hadronic++;
      if ( Truth_tau_numTracks == 1 ){
        eventCounter.prong1++;
        if ( TMath::Abs(Truth_tau_eta) < 1.0 ){
          eventCounter.eta_1_0++;
          if       ( Truth_tau_decayRad < 33.25 ) eventCounter.decayRadLower33_25++;
          else if  ( Truth_tau_decayRad < 50.5  ) eventCounter.decayRadLower50_5++;
          else if  ( Truth_tau_decayRad < 88.5  ) eventCounter.decayRadLower88_5++;
          else if  ( Truth_tau_decayRad < 122.5 ) eventCounter.decayRadLower122_5++;
          else if  ( Truth_tau_decayRad > 122.5 ) eventCounter.decayRadLarger122_5++;
        }
      }
      if (Truth_tau_numTracks == 3 ){
        eventCounter.prong3++;
      }
    }else if (Truth_tau_isLeptonic){
    }

    if ( iEntry % 10000 == 0 ) std::cout << iEntry << " events were processed... " << std::endl;

    h_Pix[0]->Fill( Pix_numCluster_IBL);
    h_Pix[1]->Fill( Pix_numCluster_BLayer);
    h_Pix[2]->Fill( Pix_numCluster_L1);
    h_Pix[3]->Fill( Pix_numCluster_L2);

    if ( Truth_tau_isHadronic ) h_numTracks[0]->Fill( Truth_part_numChargedTracks );
    if ( Truth_tau_isLeptonic ) h_numTracks[1]->Fill( Truth_part_numChargedLeptonTracks );


    h_truth_pt_all          ->Fill(Truth_tau_pt/1000.);
    h_neutrino_pt_all       ->Fill(tt_truth_neutrino_pt/1000.);

    if( Truth_tau_isLeptonic ) {
      h_truth_pt_leptonic   ->Fill(Truth_tau_pt/1000.);
      h_neutrino_pt_leptonic->Fill(tt_truth_neutrino_pt/1000.);

      if( tt_truth_leptonic_electron ) h_truth_pt_electron ->Fill(Truth_tau_pt/1000.);
      if( tt_truth_leptonic_muon )     h_truth_pt_muon     ->Fill(Truth_tau_pt/1000.);
          
    }
      
    for ( unsigned int iLayer=0; iLayer<InDet_pixel_raw_ClustersEta->size(); iLayer++){
        if ( TMath::Abs(Truth_tau_eta) < 1.0  ) h_PixelClustersNum[iLayer][isOusterPixel(Truth_tau_decayRad)] ->Fill(InDet_pixel_raw_ClustersEta->at(iLayer).size());
//        if (InDet_pixel_raw_ClustersEta->at(iLayer).size()<15) std::cout << Truth_tau_decayRad << std::endl;
      }

    if ( !Truth_tau_isHadronic ) continue;
      /*
       * Pixel raw cluster information 
       * */
      for ( unsigned int iLayer=0; iLayer<4; iLayer++){
        for (unsigned int iCluster=0; iCluster<InDet_pixel_raw_ClustersEta->at(iLayer).size(); iCluster++){
          h_PixelClusterEta[iLayer]                      ->Fill( InDet_pixel_raw_ClustersEta->at(iLayer).at(iCluster) );
        }
      }

    if( 1.0 < TMath::Abs(Truth_tau_eta) ) continue; // Barrel
    //if( TMath::Abs(Truth_tau_eta) < 1.03 ) continue; // Endcap
    // -------------------------------------
    //
    //  Truth hadronic tau 
    //
    // -------------------------------------
    /* 1-prong 
     * */
    if ( Truth_tau_numTracks == 1 ) { 
      if ( Truth_tau_decayRad < 122.5 ) {
        h_InDetTrack_num_1prong       -> Fill( InDet_numTracks );
        for ( unsigned int iReco=0; iReco<Reco_track_numberOfPixelHits->size(); iReco++){
          h_RecoTracksNumberOfPixelHits[prong(1)][0] -> Fill( Reco_track_numberOfPixelHits ->at(iReco) );
        }
      }
      else if ( Truth_tau_decayRad > 122.5 ) {
        h_InDetTrack_num_1prong_outer -> Fill( InDet_numTracks );
        for ( unsigned int iReco=0; iReco<Reco_track_numberOfPixelHits->size(); iReco++){
          h_RecoTracksNumberOfPixelHits[prong(1)][1] -> Fill( Reco_track_numberOfPixelHits ->at(iReco) );
        }
      }
      for ( unsigned int iReco=0; iReco<Reco_track_pt->size(); iReco++){
        h_RecoTauRecoTracksEta[prong(1)] -> Fill( Reco_tau_eta, Reco_track_eta -> at( iReco) );
        h_RecoTauRecoTracksPhi[prong(1)] -> Fill( Reco_tau_phi, Reco_track_phi -> at( iReco) );
      }

      h_numTrackDecayR->Fill( Truth_tau_decayRad, InDet_numTracks);
      h_TruthTau_Eta[prong(1)]                     -> Fill( Truth_tau_eta );
      h_TruthTau_TruthDecVtx_Eta[prong(1)]         -> Fill ( Truth_tau_eta, Truth_decVtx_eta );
      /* 
       * Production vertex 
       *  - Vertex x,y,z, eta, phi*/
      h_ProdVtx_X[prong(1)]                        -> Fill( Truth_prodVtx_x );
      h_ProdVtx_Y[prong(1)]                        -> Fill( Truth_prodVtx_y );
      h_ProdVtx_Z[prong(1)]                        -> Fill( Truth_prodVtx_z );
      h_ProdVtx_Eta[prong(1)]                      -> Fill( Truth_prodVtx_eta );
      h_ProdVtx_Phi[prong(1)]                      -> Fill( Truth_prodVtx_phi );
      h_ProdVtx_perp[prong(1)]                     -> Fill( Truth_prodVtx_perp);
      /* 
       * Decay vertex 
       *  - Vertex x,y,z, eta, phi*/
      h_DecVtx_X[prong(1)]   -> Fill( Truth_decVtx_x );
      h_DecVtx_Y[prong(1)]   -> Fill( Truth_decVtx_y );
      h_DecVtx_Z[prong(1)]   -> Fill( Truth_decVtx_z );
      h_DecVtx_Eta[prong(1)] -> Fill( Truth_decVtx_eta );
      TLorentzVector eta_manual; 
      eta_manual.SetXYZT(Truth_decVtx_x,
          Truth_decVtx_y,
          Truth_decVtx_z,
          Truth_decVtx_t
          );
      h_DecVtx_Eta_Manual[prong(1)] -> Fill( eta_manual.Eta() );
      h_DecVtx_Phi[prong(1)]        -> Fill( Truth_decVtx_phi );
      h_DecVtx_perp[prong(1)]       -> Fill( Truth_decVtx_perp);
      //
      h_numberOfPixelClusters[prong(1)] ->Fill( InDet_pixel_numAllOfClusters );
      /* 
       * Delta R between reco-tau track and InDetTracks.
       *  - reco-tau track : Some charged tracks from a tau
       *  - InDetTracks    : reconstructed tracks in the inner detector volumes
       * */
      for ( unsigned int iReco=0; iReco<Reco_track_pt->size(); iReco++){
        bool isFilled = false;
        TLorentzVector recoTrack;
        recoTrack.SetPtEtaPhiM( 
            Reco_track_pt->at(iReco),
            Reco_track_eta->at(iReco),
            Reco_track_phi->at(iReco),
            Reco_track_m->at(iReco)
            );
        for ( unsigned int iInDet = 0; iInDet<InDet_track_pt->size(); iInDet++){
          TLorentzVector InDetTrack;
          InDetTrack.SetPtEtaPhiM( 
              InDet_track_pt->at(iInDet),
              InDet_track_eta->at(iInDet),
              InDet_track_phi->at(iInDet),
              InDet_track_m->at(iInDet)
              );
          if ( Truth_tau_decayRad < 122.5 ){
            h_RecoTauInDetTrack_DeltaR[prong(1)] -> Fill( recoTrack.DeltaR(InDetTrack) );
          }else if (Truth_tau_decayRad > 122.5){
            if ( isFilled == false && InDet_track_numberOfPixelHits->at(iInDet) > 0){
              h_RecoTauInDetTrack_DeltaR[1] -> Fill( recoTrack.DeltaR(InDetTrack) );
              isFilled = true;
            } else if ( isFilled == false && InDet_track_numberOfPixelHits->at(iInDet) ==0) {
            }
          }
        }
      }

      /* 
       * Delta R between reco-tau tracks and pixel clusters 
       *  - This block can help you to understand that how far is the tau track from some pixle clusters. 
       *    Are there any clustrs associated with the track? 
       *    */
      if ( 1.4 > TMath::Abs(Truth_tau_eta) && Truth_tau_decayRad > 122.5 ) {
        for ( unsigned int iReco=0; iReco<Reco_track_pt->size(); iReco++){
          TLorentzVector recoTrack;
          recoTrack.SetPtEtaPhiM( 
              Reco_track_pt->at(iReco),
              Reco_track_eta->at(iReco),
              Reco_track_phi->at(iReco),
              Reco_track_m->at(iReco)
              );
          //double minDeltaR=999;

          for ( unsigned int index = 0; index<InDet_pixel_IBLClustersEta->size(); index++){
            for ( unsigned int j = 0; j < InDet_pixel_IBLClustersEta->at(index).size(); j++){
              h_PixelCluIBLEta[prong(1)] -> Fill( InDet_pixel_IBLClustersEta->at(index).at(j));
              TLorentzVector pixelClu;
              pixelClu.SetPtEtaPhiM(
                  0,
                  InDet_pixel_IBLClustersEta->at(index).at(j),
                  InDet_pixel_IBLClustersPhi->at(index).at(j),
                  0
                  );
              //std::cout << truthTrack.DeltaR( pixelClu ) << std::endl;
              h_PixelCluIBLDeltaR[prong(1)] -> Fill( pixelClu.DeltaR(recoTrack ) );
            }
          }
          for ( unsigned int index = 0; index<InDet_pixel_BLayerClustersEta->size(); index++){
            for ( unsigned int j = 0; j < InDet_pixel_BLayerClustersEta->at(index).size(); j++){
              TLorentzVector pixelClu;
              pixelClu.SetPtEtaPhiM(
                  0,
                  InDet_pixel_BLayerClustersEta->at(index).at(j),
                  InDet_pixel_BLayerClustersPhi->at(index).at(j),
                  0
                  );
              //std::cout << truthTrack.DeltaR( pixelClu ) << std::endl;
              h_PixelCluBLayerDeltaR[prong(1)] -> Fill( pixelClu.DeltaR(recoTrack ) );
            }
          }
          for ( unsigned int index = 0; index<InDet_pixel_L1ClustersEta->size(); index++){
            for ( unsigned int j = 0; j < InDet_pixel_L1ClustersEta->at(index).size(); j++){
              TLorentzVector pixelClu;
              pixelClu.SetPtEtaPhiM(
                  0,
                  InDet_pixel_L1ClustersEta->at(index).at(j),
                  InDet_pixel_L1ClustersPhi->at(index).at(j),
                  0
                  );
              //std::cout << truthTrack.DeltaR( pixelClu ) << std::endl;
              h_PixelCluL1DeltaR[prong(1)] -> Fill( pixelClu.DeltaR(recoTrack ) );
            }
          }
          for ( unsigned int index = 0; index<InDet_pixel_L2ClustersEta->size(); index++){
            for ( unsigned int j = 0; j < InDet_pixel_L2ClustersEta->at(index).size(); j++){
              TLorentzVector pixelClu;
              pixelClu.SetPtEtaPhiM(
                  0,
                  InDet_pixel_L2ClustersEta->at(index).at(j),
                  InDet_pixel_L2ClustersPhi->at(index).at(j),
                  0
                  );
              h_PixelCluL2DeltaR[prong(1)] -> Fill( pixelClu.DeltaR(recoTrack ) );
            }
          }
        }
      }
      

      /* 
       * Neutrino properties
       *  - If the neutrino passes away with larger momentum, will the delta R distribution be changed ?  
       * */
#define zero_PI0
//#define one_PI0
//#define two_PI0
#ifdef zero_PI0
      // tau -> pi^{-} + nu
      bool target_event = false;
      if ( Truth_dau_pdgId->size() == 2 ){
        bool pi_  = false;
        bool tauneu = false;
        for ( unsigned int iDau=0; iDau<Truth_dau_pdgId->size(); iDau++){
          if ( TMath::Abs(Truth_dau_pdgId->at(iDau)) == 211 ) pi_  = true;
          if ( TMath::Abs(Truth_dau_pdgId->at(iDau)) == 16  ) tauneu = true;
        }
        if ( pi_ ==true && tauneu == true ) target_event = true;
      }
#elif defined one_PI0 
      // tau -> pi^{-} + pi0 + nu
      bool target_event = false;
      if ( Truth_dau_pdgId->size() == 3 ){
        bool pi_    = false;
        bool pi0    = false;
        bool tauneu = false;
        for ( unsigned int iDau=0; iDau<Truth_dau_pdgId->size(); iDau++){
          if ( TMath::Abs(Truth_dau_pdgId->at(iDau)) == 211 ) pi_    = true;
          if ( TMath::Abs(Truth_dau_pdgId->at(iDau)) == 111 ) pi0    = true;
          if ( TMath::Abs(Truth_dau_pdgId->at(iDau)) == 16  ) tauneu = true;
        }
        if ( pi_ ==true && pi0==true && tauneu == true ) target_event = true;
      }
#elif defined two_PI0 
      // tau -> pi^{-} + pi0 + nu
      bool target_event = false;
      if ( Truth_dau_pdgId->size() == 4 ){
        bool pi_    = false;
        bool pi0    = false;
        bool pi02   = false;
        bool tauneu = false;
        bool onePi  = false;
        for ( unsigned int iDau=0; iDau<Truth_dau_pdgId->size(); iDau++){
          if ( TMath::Abs(Truth_dau_pdgId->at(iDau)) == 211 ) pi_    = true;
          if ( onePi == false && TMath::Abs(Truth_dau_pdgId->at(iDau)) == 111 ) { pi0    = true; onePi = true; }
          if ( onePi == true  && TMath::Abs(Truth_dau_pdgId->at(iDau)) == 111 ) { pi02   = true;}
          if ( TMath::Abs(Truth_dau_pdgId->at(iDau)) == 16  ) tauneu = true;
        }
        if ( pi_ ==true && pi0==true && pi02 == true && tauneu == true ) target_event = true;
      }
#endif
      if ( target_event ) {
        if ( Reco_tau_numTracks == 0 ) {
          h_Spec_RecoTauPt            -> Fill( Reco_tau_pt/1000.);
          h_Spec_RecoTauEta           -> Fill( Reco_tau_eta);
          h_Spec_RecoTauPhi           -> Fill( Reco_tau_phi);
          for ( int indet=0; indet< InDet_track_pt->size(); indet++){
            if ( InDet_track_numberOfPixelHits->at(indet) !=0 ){
              h_Spec_InDetTrack_NumTracks -> Fill( InDet_numTracks);
            }
          }
        }

        /* Pixel raw clusters information */
        // Truth tau
        if ( TMath::Abs(Truth_tau_eta) < 1.0 ) {
          // Truth tau info
          TLorentzVector TruthTau;
          TruthTau.SetPtEtaPhiM(
              Truth_tau_pt,
              Truth_tau_eta,
              Truth_tau_phi,
              Truth_tau_m
              );
          // Truth track (from tau) info
          TLorentzVector TruthTrack;
          TruthTrack.SetPtEtaPhiM(
              Truth_track_pt->at(0),
              Truth_track_eta->at(0),
              Truth_track_phi->at(0),
              Truth_track_m->at(0)
              );

          //std::cout << TruthTau.Eta() << " " << TruthTrack.Eta() << std::endl;

          //std::cout << TruthTau.X() << " " << TruthTau.Y() << " " << TruthTau.Z() << std::endl;
          //std::cout << TruthTrack.X() << " " << TruthTrack.Y() << " " << TruthTrack.Z() << std::endl;
          //std::cout << " --------------------------- " << std::endl;

          h_DeltaR_TruthTauTruthTrack->Fill( TruthTau.DeltaR(TruthTrack) );

          Double_t DeltaR[4] = {999,999,999,999}; // Calculate the DeltaR per each layer
          Double_t minX[4];
          Double_t minY[4];
          Double_t minZ[4] = {999};
          Double_t minR[4];
          Double_t minClusterX[4];
          Double_t minClusterY[4];
          Double_t minClusterZ[4];
          Int_t IBLHits    = 0;
          Int_t BLayerHits = 0;
          Int_t L1Hits     = 0;
          Int_t L2Hits     = 0;


          for ( unsigned int iLayer=0; iLayer<InDet_pixel_raw_ClustersEta->size(); iLayer++){
            // Check the numebr of pixel clusters
            //h_PixelClustersNum   [iLayer][isOusterPixel(Truth_tau_decayRad)] -> Fill( InDet_pixel_raw_ClustersEta   ->at(iLayer).size());
          
            // Find the closes pixel clusters  
            for ( unsigned int iCluster=0; iCluster<InDet_pixel_raw_ClustersEta->at(iLayer).size(); iCluster++){
              // Modify x,y,z
              TLorentzVector PixelCluster;
              PixelCluster.SetXYZT( 
                  InDet_pixel_raw_globalX->at(iLayer).at(iCluster) - Truth_prodVtx_x,
                  InDet_pixel_raw_globalY->at(iLayer).at(iCluster) - Truth_prodVtx_y,
                  InDet_pixel_raw_globalZ->at(iLayer).at(iCluster) - Truth_prodVtx_z,
                  0
                  );
            

              //if ( PixelCluster.DeltaR(TruthTrack) < DeltaR )
              if ( PixelCluster.DeltaR(TruthTau) < DeltaR[iLayer] ){
                //DeltaR[iLayer] = PixelCluster.DeltaR(TruthTrack);
                DeltaR[iLayer]      = PixelCluster.DeltaR(TruthTau);
                minClusterX[iLayer] = PixelCluster.X();
                minClusterY[iLayer] = PixelCluster.Y();
                minClusterZ[iLayer] = PixelCluster.Z();
                minX[iLayer]        = InDet_pixel_raw_globalX->at(iLayer).at(iCluster); 
                minY[iLayer]        = InDet_pixel_raw_globalY->at(iLayer).at(iCluster); 
                minZ[iLayer]        = InDet_pixel_raw_globalZ->at(iLayer).at(iCluster); 
                minR[iLayer]        = TMath::Sqrt(InDet_pixel_raw_globalX->at(iLayer).at(iCluster)*InDet_pixel_raw_globalX->at(iLayer).at(iCluster) + 
                    InDet_pixel_raw_globalY->at(iLayer).at(iCluster)*InDet_pixel_raw_globalY->at(iLayer).at(iCluster)); 
              }
              //if ( PixelCluster.DeltaR(TruthTau) < 0.01)
              h_PixelClustersCharge[iLayer][isOusterPixel(Truth_tau_decayRad)] -> Fill( InDet_pixel_raw_ClustersCharge->at(iLayer).at(iCluster)/1000.);

              if      ( Truth_tau_decayRad < 122.5 ) h_TauPixelDeltaR [iLayer][0] ->Fill( PixelCluster.DeltaR(TruthTau) );
              else if ( Truth_tau_decayRad > 122.5 ) {
                if      ( PixelCluster.Y() < 40  ) h_TauPixelDeltaR [0][1] ->Fill( PixelCluster.DeltaR(TruthTau) );
                else if ( PixelCluster.Y() < 70  ) h_TauPixelDeltaR [0][1] ->Fill( PixelCluster.DeltaR(TruthTau) );
                else if ( PixelCluster.Y() < 90  ) h_TauPixelDeltaR [0][1] ->Fill( PixelCluster.DeltaR(TruthTau) );
                else if ( PixelCluster.Y() < 130 ) h_TauPixelDeltaR [0][1] ->Fill( PixelCluster.DeltaR(TruthTau) );
                // Plot pixel cluster (Event display)
                h_EventDisplay_PixelCluster[TargetEventCounter]->SetPoint(
                    h_EventDisplay_PixelCluster[TargetEventCounter]->GetN(), 
                    InDet_pixel_raw_globalX->at(iLayer).at(iCluster), 
                    InDet_pixel_raw_globalY->at(iLayer).at(iCluster));

                // Plot pixel cluster (Z-R plane)
                if ( InDet_pixel_raw_globalY->at(iLayer).at(iCluster) > 0 ) 
                  h_EventDisplay_PixelCluster_RZ[TargetEventCounter]->SetPoint(
                      h_EventDisplay_PixelCluster_RZ[TargetEventCounter]->GetN(), 
                      minZ[iLayer], 
                      TMath::Sqrt(InDet_pixel_raw_globalX->at(iLayer).at(iCluster)*InDet_pixel_raw_globalX->at(iLayer).at(iCluster) +
                        InDet_pixel_raw_globalY->at(iLayer).at(iCluster)*InDet_pixel_raw_globalY->at(iLayer).at(iCluster)));
                else                    
                  h_EventDisplay_PixelCluster_RZ[TargetEventCounter]->SetPoint(
                      h_EventDisplay_PixelCluster_RZ[TargetEventCounter]->GetN(), 
                      minZ[iLayer], 
                      -1*TMath::Sqrt(InDet_pixel_raw_globalX->at(iLayer).at(iCluster)*InDet_pixel_raw_globalX->at(iLayer).at(iCluster) + 
                        InDet_pixel_raw_globalY->at(iLayer).at(iCluster)*InDet_pixel_raw_globalY->at(iLayer).at(iCluster)));

                // Plot PDG ID
                if ( iLayer == 0 ) {
                  for ( unsigned int jndex=0; jndex < InDet_pixel_raw_IB_pdgId->at(iCluster).size(); jndex++){
                    if (InDet_pixel_raw_IB_pdgId->at(iCluster).at(jndex) != 0 ) {
                      InDetBarcodeLogger       .at(TargetEventCounter).push_back(InDet_pixel_raw_IB_barcode ->at(iCluster).at(jndex));
                      // Clusters
                      h_EventDisplay_PixelCluster_Classified[TargetEventCounter]->SetPoint(
                          h_EventDisplay_PixelCluster_Classified[TargetEventCounter]->GetN(),
                          InDet_pixel_raw_globalX->at(iLayer).at(iCluster),
                          InDet_pixel_raw_globalY->at(iLayer).at(iCluster));
                      // PDG ID for Pixel cluster
                      TText* pdg_id = new TText(
                          InDet_pixel_raw_globalX->at(iLayer).at(iCluster)+1, 
                          InDet_pixel_raw_globalY->at(iLayer).at(iCluster)+1,
                          Form("%.0f",InDet_pixel_raw_IB_pdgId->at(iCluster).at(jndex)));

                      if ( TMath::Abs(InDet_pixel_raw_IB_pdgId->at(iCluster).at(jndex)) == 15){
                        h_EventDisplay_PixelCluster_Classified[TargetEventCounter]->SetMarkerSize(0.5);
                        h_EventDisplay_PixelCluster_Classified[TargetEventCounter]->SetMarkerColor(kRed);
                        pdg_id->SetTextColor(kRed);
                      } else {
                        std::cout << __FILE__ << " " << __LINE__ << " " << TMath::Abs(InDet_pixel_raw_IB_pdgId->at(iCluster).at(jndex)) << std::endl;
                        h_EventDisplay_PixelCluster_Classified[TargetEventCounter]->SetMarkerSize(0.5);
                        h_EventDisplay_PixelCluster_Classified[TargetEventCounter]->SetMarkerColor(kCyan);
                        pdg_id->SetTextColor(kCyan);
                      }

                      pdg_id->SetTextSize(0.015);
                      h_TTextClusterPdgId[TargetEventCounter].push_back( pdg_id );
                    }
                  }
                }else if ( iLayer == 1 ){
                  for ( unsigned int jndex=0; jndex < InDet_pixel_raw_BL_pdgId->at(iCluster).size(); jndex++){
                    if (InDet_pixel_raw_BL_pdgId->at(iCluster).at(jndex) != 0 ) {
                      InDetBarcodeLogger_BL       .at(TargetEventCounter).push_back(InDet_pixel_raw_BL_barcode ->at(iCluster).at(jndex));
                      h_EventDisplay_PixelCluster_Classified[TargetEventCounter]->SetPoint(
                          h_EventDisplay_PixelCluster_Classified[TargetEventCounter]->GetN(),
                          InDet_pixel_raw_globalX->at(iLayer).at(iCluster),
                          InDet_pixel_raw_globalY->at(iLayer).at(iCluster));
                      h_EventDisplay_PixelCluster_Classified[TargetEventCounter]->SetMarkerSize(0.5);
                      if ( TMath::Abs(InDet_pixel_raw_BL_pdgId->at(iCluster).at(jndex))== 15){
                        h_EventDisplay_PixelCluster_Classified[TargetEventCounter]->SetMarkerSize(0.5);
                        h_EventDisplay_PixelCluster_Classified[TargetEventCounter]->SetMarkerColor(kRed);
                      } else {
                        h_EventDisplay_PixelCluster_Classified[TargetEventCounter]->SetMarkerSize(0.5);
                        h_EventDisplay_PixelCluster_Classified[TargetEventCounter]->SetMarkerColor(kCyan);
                      }
                      // PDG ID for Pixel cluster
                      TText* pdg_id = new TText(
                          InDet_pixel_raw_globalX->at(iLayer).at(iCluster)+1, 
                          InDet_pixel_raw_globalY->at(iLayer).at(iCluster)+1,
                          Form("%.0f",InDet_pixel_raw_BL_pdgId->at(iCluster).at(jndex)));
                      pdg_id->SetTextSize(0.015);
                      if ( TMath::Abs(InDet_pixel_raw_BL_pdgId->at(iCluster).at(jndex)) == 15) 
                        pdg_id->SetTextColor(kRed);
                      else 
                        pdg_id->SetTextColor(kCyan);
                      h_TTextClusterPdgId[TargetEventCounter].push_back( pdg_id );
                    }
                  }
                } else if ( iLayer == 2 ){
                  for ( unsigned int jndex=0; jndex < InDet_pixel_raw_L1_pdgId->at(iCluster).size(); jndex++){
                    if (InDet_pixel_raw_L1_pdgId->at(iCluster).at(jndex) != 0 ) {
                      InDetBarcodeLogger_L1       .at(TargetEventCounter).push_back(InDet_pixel_raw_L1_barcode ->at(iCluster).at(jndex));
                      h_EventDisplay_PixelCluster_Classified[TargetEventCounter]->SetPoint(
                          h_EventDisplay_PixelCluster_Classified[TargetEventCounter]->GetN(),
                          InDet_pixel_raw_globalX->at(iLayer).at(iCluster),
                          InDet_pixel_raw_globalY->at(iLayer).at(iCluster));
                      h_EventDisplay_PixelCluster_Classified[TargetEventCounter]->SetMarkerSize(0.5);
                      if ( TMath::Abs(InDet_pixel_raw_L1_pdgId->at(iCluster).at(jndex))== 15){
                        h_EventDisplay_PixelCluster_Classified[TargetEventCounter]->SetMarkerSize(0.5);
                        h_EventDisplay_PixelCluster_Classified[TargetEventCounter]->SetMarkerColor(kRed);
                      } else {
                        h_EventDisplay_PixelCluster_Classified[TargetEventCounter]->SetMarkerSize(0.5);
                        h_EventDisplay_PixelCluster_Classified[TargetEventCounter]->SetMarkerColor(kCyan);
                      }
                      // PDG ID for Pixel cluster
                      TText* pdg_id = new TText(
                          InDet_pixel_raw_globalX->at(iLayer).at(iCluster)+1, 
                          InDet_pixel_raw_globalY->at(iLayer).at(iCluster)+1,
                          Form("%.0f",InDet_pixel_raw_L1_pdgId->at(iCluster).at(jndex)));
                      pdg_id->SetTextSize(0.015);
                      if ( TMath::Abs(InDet_pixel_raw_L1_pdgId->at(iCluster).at(jndex)) == 15) 
                        pdg_id->SetTextColor(kRed);
                      else 
                        pdg_id->SetTextColor(kCyan);
                      h_TTextClusterPdgId[TargetEventCounter].push_back( pdg_id );
                    }
                  }
                } else if ( iLayer == 3 ){
                  for ( unsigned int jndex=0; jndex < InDet_pixel_raw_L2_pdgId->at(iCluster).size(); jndex++){
                    if (InDet_pixel_raw_L2_pdgId->at(iCluster).at(jndex) != 0 ) {
                      InDetBarcodeLogger_L2       .at(TargetEventCounter).push_back(InDet_pixel_raw_L2_barcode ->at(iCluster).at(jndex));
                      h_EventDisplay_PixelCluster_Classified[TargetEventCounter]->SetPoint(
                          h_EventDisplay_PixelCluster_Classified[TargetEventCounter]->GetN(),
                          InDet_pixel_raw_globalX->at(iLayer).at(iCluster),
                          InDet_pixel_raw_globalY->at(iLayer).at(iCluster));
                      h_EventDisplay_PixelCluster_Classified[TargetEventCounter]->SetMarkerSize(0.5);
                      if ( TMath::Abs(InDet_pixel_raw_L2_pdgId->at(iCluster).at(jndex))== 15){
                        h_EventDisplay_PixelCluster_Classified[TargetEventCounter]->SetMarkerSize(0.5);
                        h_EventDisplay_PixelCluster_Classified[TargetEventCounter]->SetMarkerColor(kRed);
                      } else {
                        h_EventDisplay_PixelCluster_Classified[TargetEventCounter]->SetMarkerSize(0.3);
                        h_EventDisplay_PixelCluster_Classified[TargetEventCounter]->SetMarkerColor(kCyan);
                      }
                      // PDG ID for Pixel cluster
                      TText* pdg_id = new TText(
                          InDet_pixel_raw_globalX->at(iLayer).at(iCluster)+1, 
                          InDet_pixel_raw_globalY->at(iLayer).at(iCluster)+1,
                          Form("%.0f",InDet_pixel_raw_L2_pdgId->at(iCluster).at(jndex)));
                      pdg_id->SetTextSize(0.015);
                      if ( TMath::Abs(InDet_pixel_raw_L2_pdgId->at(iCluster).at(jndex)) == 15) 
                        pdg_id->SetTextColor(kRed);
                      else 
                        pdg_id->SetTextColor(kCyan);
                      h_TTextClusterPdgId[TargetEventCounter].push_back( pdg_id );
                    }
                  }
                }
              } // decayRad>122.5
            }// iCluster

            if      ( Truth_tau_decayRad < 122.5 ) h_TauPixelMinimumDeltaR [iLayer][0] ->Fill( DeltaR[iLayer] );
            else if ( Truth_tau_decayRad > 122.5 ) h_TauPixelMinimumDeltaR [iLayer][1] ->Fill( DeltaR[iLayer] );

            if ( DeltaR[iLayer] < 0.01 ) {
              if ( iLayer ==0 )  IBLHits++;  
              if ( iLayer ==1 )  BLayerHits++; 
              if ( iLayer ==2 )  L1Hits++;  
              if ( iLayer ==3 )  L2Hits++; 
            }
        }// iLayer
        if      ( Truth_tau_decayRad < 122.5 ) h_PixelHittedLayer[0]->Fill( IBLHits + BLayerHits + L1Hits + L2Hits);
        else if ( Truth_tau_decayRad > 122.5 ){
          h_PixelHittedLayer[1]->Fill( IBLHits + BLayerHits + L1Hits + L2Hits);
          
          for ( int iLayer=0; iLayer<4; iLayer++){
            XPositionLogger[TargetEventCounter][iLayer] = minX [iLayer];
            YPositionLogger[TargetEventCounter][iLayer] = minY [iLayer];
            ZPositionLogger[TargetEventCounter][iLayer] = minZ [iLayer];

            // Truth tau position
            XTauPositionLogger[TargetEventCounter][0] = Truth_prodVtx_x;
            YTauPositionLogger[TargetEventCounter][0] = Truth_prodVtx_y;
            ZTauPositionLogger[TargetEventCounter][0] = Truth_prodVtx_z;
            RTauPositionLogger[TargetEventCounter][0] = TMath::Sqrt(Truth_prodVtx_x*Truth_prodVtx_x+Truth_prodVtx_y*Truth_prodVtx_y);
//            XTauPositionLogger[TargetEventCounter][1] = TruthTau.X();
//            YTauPositionLogger[TargetEventCounter][1] = TruthTau.Y();
            XTauPositionLogger[TargetEventCounter][1] = Truth_decVtx_x;
            YTauPositionLogger[TargetEventCounter][1] = Truth_decVtx_y;
            ZTauPositionLogger[TargetEventCounter][1] = TruthTau.Z();
            RTauPositionLogger[TargetEventCounter][1] = TMath::Sqrt(TruthTau.X()*TruthTau.X()+TruthTau.Y()*TruthTau.Y());

            // Neutrino
            TLorentzVector NeuVec;
            NeuVec.SetPtEtaPhiM( Truth_neutrino_pt->at(0), Truth_neutrino_eta->at(0), Truth_neutrino_phi->at(0), 0);
            XNeutrinoPositionLogger[TargetEventCounter][0] = Truth_decVtx_x;
            XNeutrinoPositionLogger[TargetEventCounter][1] = NeuVec.X();
            YNeutrinoPositionLogger[TargetEventCounter][0] = Truth_decVtx_y;
            YNeutrinoPositionLogger[TargetEventCounter][1] = NeuVec.Y();

            // Truth track position
            XTrackPositionLogger[TargetEventCounter][0] = Truth_decVtx_x;
            YTrackPositionLogger[TargetEventCounter][0] = Truth_decVtx_y;
            ZTrackPositionLogger[TargetEventCounter][0] = Truth_decVtx_z;
            RTrackPositionLogger[TargetEventCounter][0] = TMath::Sqrt(Truth_decVtx_x*Truth_decVtx_x+Truth_decVtx_y*Truth_decVtx_y);
            XTrackPositionLogger[TargetEventCounter][1] = TruthTrack.X();
            YTrackPositionLogger[TargetEventCounter][1] = TruthTrack.Y();
            ZTrackPositionLogger[TargetEventCounter][1] = TruthTrack.Z();
            RTrackPositionLogger[TargetEventCounter][1] = TMath::Sqrt(TruthTrack.X()*TruthTrack.X()+TruthTrack.Y()*TruthTrack.Y());

            TrackPtLogger[TargetEventCounter]        = TruthTrack.Pt()/1000.;
            TrackTauThetaLoogger[TargetEventCounter] = TruthTrack.Angle(TruthTau.Vect());
          }
          // Decay vertex 
          h_EventDisplay_DecayVtx[TargetEventCounter]   ->SetPoint(
              h_EventDisplay_DecayVtx[TargetEventCounter]->GetN(),
              Truth_decVtx_x,
              Truth_decVtx_y);

          if ( Truth_decVtx_y > 0 ) 
            h_EventDisplay_DecayVtx_RZ[TargetEventCounter]->SetPoint(
                h_EventDisplay_DecayVtx_RZ[TargetEventCounter]->GetN(), 
                Truth_decVtx_z, 
                TMath::Sqrt(Truth_decVtx_x*Truth_decVtx_x + Truth_decVtx_y*Truth_decVtx_y));
          else                      
            h_EventDisplay_DecayVtx_RZ[TargetEventCounter]->SetPoint(
                h_EventDisplay_DecayVtx_RZ[TargetEventCounter]->GetN(), 
                Truth_decVtx_z, 
                -1*TMath::Sqrt(Truth_decVtx_x*Truth_decVtx_x + Truth_decVtx_y*Truth_decVtx_y));

          // InDetTrackParticle
          TLatex* track_num = new TLatex(-80, 180, Form("All the InDetTrack # = %d", InDet_track_pt->size() ) );
          track_num->SetTextAlign(22);
          track_num->SetTextColor(kBlack);
          track_num->SetTextFont(43);
          track_num->SetTextSize(15);
          h_TTextTrackInfo[TargetEventCounter].push_back( track_num );
          // InDet track 
          for ( int iInDet=0; iInDet<InDet_track_pt->size(); iInDet++){
            TLorentzVector InDetVec;
            InDetVec.SetPtEtaPhiM( 
                InDet_track_pt ->at(iInDet)/1000., 
                InDet_track_eta->at(iInDet),
                InDet_track_phi->at(iInDet),
                InDet_track_m  ->at(iInDet)
                );
            XInDetTrackPositionLogger       .at(TargetEventCounter).push_back( InDetVec.X() );
            YInDetTrackPositionLogger       .at(TargetEventCounter).push_back( InDetVec.Y() );
            ZInDetTrackPositionLogger       .at(TargetEventCounter).push_back( InDetVec.Z() );
            NumberOfPixelHitsLogger         .at(TargetEventCounter).push_back( InDet_track_numberOfPixelHits->at(iInDet) );
            NumberOf_IBL_PixelHitsLogger    .at(TargetEventCounter).push_back( InDet_track_numberOfIBLHits->at(iInDet) );
            NumberOf_BLayer_PixelHitsLogger .at(TargetEventCounter).push_back( InDet_track_numberOfBLayerHits->at(iInDet) );
            NumberOf_L1_PixelHitsLogger     .at(TargetEventCounter).push_back( InDet_track_numberOfPixelL1Hits->at(iInDet) );
            NumberOf_L2_PixelHitsLogger     .at(TargetEventCounter).push_back( InDet_track_numberOfPixelL2Hits->at(iInDet) );

            // For InDetTrack legend
            TLatex* track_info  = new TLatex(
                -80, 180 - h_TTextTrackInfo[TargetEventCounter].size()*18, 
                Form("pT = %.1f [GeV], #eta = %.3f, #phi = %.3f", InDet_track_pt->at(iInDet)/1000., InDet_track_eta->at(iInDet), InDet_track_phi->at(iInDet)) );
            track_info->SetTextAlign(22);
            track_info->SetTextColor(kBlack);
            track_info->SetTextFont(43);
            track_info->SetTextSize(15);
            h_TTextTrackInfo[TargetEventCounter].push_back( track_info );
          }
          TargetEventCounter++;
        }

        for ( unsigned int iNeutrino=0; iNeutrino<Truth_neutrino_pt->size(); iNeutrino++){
          TLorentzVector neutrino;
          neutrino.SetPtEtaPhiM( 
              Truth_neutrino_pt ->at(iNeutrino),
              Truth_neutrino_eta->at(iNeutrino),
              Truth_neutrino_phi->at(iNeutrino),
              0
              );
          TLorentzVector TruthTau;
          TruthTau.SetPtEtaPhiM( 
              Truth_tau_pt ,
              Truth_tau_eta,
              Truth_tau_phi,
              Truth_tau_m  
              );

          h_Neutrino_DeltaR        ->Fill(neutrino.DeltaR(TruthTau));

          for ( unsigned int iTruth = 0; iTruth < Truth_track_pt->size(); iTruth++){
            TLorentzVector TruthTrack;
            TruthTrack.SetPtEtaPhiM( 
                Truth_track_pt ->at(iTruth),
                Truth_track_eta->at(iTruth),
                Truth_track_phi->at(iTruth),
                Truth_track_m  ->at(iTruth)
                );
            int HistIndex = (TruthTrack.Pt()/1000.)/100;
            h_TrackPt_NumTracks[HistIndex]         -> Fill(Reco_tau_numTracks);
            h_Neutrino_TruthTrack_Pt               -> Fill( neutrino.Pt()/1000., TruthTrack.Pt()/1000. );
            h_TrackPt_NeutrinoTrack_OpeningAngle   -> Fill( TruthTrack.Pt()/1000., TruthTrack.Angle(neutrino.Vect()));
            h_TrackPt_TauTrack_OpeningAngle        -> Fill( TruthTrack.Pt()/1000., TruthTrack.Angle(TruthTau.Vect()));
          }

          for ( unsigned int iRecoTrack=0; iRecoTrack< Reco_track_pt->size(); iRecoTrack++){
            //              h_NeutrinoPt_NumPixelHits->Fill(Truth_neutrino_pt->at(iNeutrino)/1000., Reco_track_numberOfPixelHits->at(iRecoTrack));
            h_NeutrinoPt_NumPixelHits->Fill( Reco_track_pt->at(iRecoTrack)/1000., Reco_track_numberOfPixelHits->at(iRecoTrack));
            TLorentzVector RecoTrack;
            RecoTrack.SetPtEtaPhiM( 
                Reco_track_pt ->at(iRecoTrack),
                Reco_track_eta->at(iRecoTrack),
                Reco_track_phi->at(iRecoTrack),
                Reco_track_m  ->at(iRecoTrack)
                );
            //            h_NeutrinoPt_DeltaR ->Fill( Truth_neutrino_pt->at(iNeutrino)/1000., TruthTrack.DeltaR(TruthTau));
            h_NeutrinoPt_DeltaR ->Fill( Truth_neutrino_pt->at(iNeutrino)/1000., RecoTrack.DeltaR(TruthTau));
            int HistIndex;
            if(RecoTrack.Pt()/1000.< 1000){
              HistIndex = (RecoTrack.Pt()/1000.)/100;
            } else {
              HistIndex = 9;
            }
            h_Spec_NumberOfPixelHits[HistIndex]    -> Fill( Reco_track_numberOfPixelHits->at(iRecoTrack));
            //              std::cout << Reco_track_numberOfPixelHits->at(iRecoTrack)  << std::endl;
          }
        }
      }// target event

      /* Detailed studies of the number of tracks 
       *  - Delta R between truth tau tracks and InDetTracks.
       *  */
      // == truth == 
      TLorentzVector truthTrack, recoTrack;
      for ( unsigned int iTruth = 0; iTruth < Truth_track_pt->size(); iTruth++){
        int numTracks=0;
        int TruthIndex = 999;
        int InDetInDex = 999;
        bool isMatched = false;
        double minDeltaR = 999;
        truthTrack.SetPtEtaPhiM( 
            Truth_track_pt->at(iTruth),
            Truth_track_eta->at(iTruth),
            Truth_track_phi->at(iTruth),
            Truth_track_m->at(iTruth)
            );
        h_TruthTrackPt                        -> Fill( truthTrack.Pt()/1000.);
        h_TruthPtVsNumTracks[prong(1)]        -> Fill( Truth_track_pt->at(iTruth)/1000., InDet_numTracks);
        h_truthTrackEta                       -> Fill (Truth_track_eta->at(iTruth));
        h_TruthTrackEta[prong(1)]             -> Fill (Truth_track_eta->at(iTruth));
        h_TruthDecVtx_Eta[prong(1)]           -> Fill (Truth_decVtx_eta);
        h_truthTrackPhi                       -> Fill (Truth_track_phi->at(iTruth));
        h_TruthTrackPhi[prong(1)]             -> Fill (Truth_track_phi->at(iTruth));
        h_TruthTrack_Pt[0]                    -> Fill (Truth_track_pt->at(iTruth)/1000.);
        h_TruthTau_TruthTrack_Eta[prong(1)]           -> Fill ( Truth_tau_eta, Truth_track_eta->at(iTruth) );
        h_TruthTrack_TruthDecVtx_Eta[prong(1)]         -> Fill ( Truth_track_eta->at(iTruth), Truth_decVtx_eta );
        double pt = 1000 * Truth_track_charge -> at(iTruth) / Truth_track_pt->at(iTruth);
        h_TruthTrack_QOverPt[0]               -> Fill (pt);

        // == reco == 
        for ( unsigned int iID = 0; iID<InDet_track_pt->size(); iID++){
          // Raw distribution
          h_InDetTrack_RawEta   [prong(1)]  -> Fill( InDet_track_eta->at(iID) );
          h_InDetTrack_RawPhi   [prong(1)]  -> Fill( InDet_track_phi->at(iID) );
          h_InDetTrack_RawqOverPt[prong(1)]  ->Fill( 1000.*InDet_track_charge->at(iID)/InDet_track_pt->at(iID) );

          recoTrack.SetPtEtaPhiM( 
              InDet_track_pt ->at(iID),
              InDet_track_eta->at(iID),
              InDet_track_phi->at(iID),
              InDet_track_m  ->at(iID)
              );
          if ( recoTrack.DeltaR(truthTrack) < minDeltaR ) {
            minDeltaR = recoTrack.DeltaR(truthTrack);
            TruthIndex = iTruth;
            InDetInDex = iID;
          }
          if ( recoTrack.DeltaR(truthTrack) < 0.001 ) numTracks++;
          h_qOverPtVsDeltaR[0]               ->Fill( 1000.*InDet_track_charge->at(iID)/InDet_track_pt->at(iID), recoTrack.DeltaR(truthTrack) );
        } // recoTracks

        if ( minDeltaR < 0.01 ) isMatched = true; 

        // Delta R (Pt slice , per 10 GeV)
        for ( int iPtRange=0; iPtRange<100; iPtRange++ ){
          if ( iPtRange*10 <= truthTrack.Pt()/1000. && truthTrack.Pt()/1000. < (iPtRange+1)*10 ){
            h_DeltaR_PtSlice10GeV[prong(1)][iPtRange] -> Fill( minDeltaR );
          }
        }

        h_DeltaR_AllTracks   [prong(1)] ->Fill( truthTrack.Pt()/1000., minDeltaR );
        h_DeltaR_AllTracks_1D[prong(1)] ->Fill( minDeltaR );
        h_numberOfTracks     [prong(1)] ->Fill( numTracks );
        if ( isMatched ) {
          h_TruthTrackPt_MatchedTruthPt[prong(1)][1]->Fill(truthTrack.Pt()/1000.);
          h_InDetTrack_qOverPt[prong(1)]  ->Fill( 1000.*InDet_track_charge->at(InDetInDex)/InDet_track_pt->at(InDetInDex) );
          h_TruthTrackQOverPt [prong(1)]  ->Fill( 1000.*Truth_track_charge->at(TruthIndex)/Truth_track_pt->at(TruthIndex) );
          h_InDetTrack_Eta    [prong(1)]  -> Fill( InDet_track_eta->at(InDetInDex) );
          h_InDetTrack_Phi    [prong(1)]  -> Fill( InDet_track_phi->at(InDetInDex) );
          h_TruthTrack_InDetTrac_qOverPt[prong(1)] -> Fill(1000.*Truth_track_charge->at(TruthIndex)/Truth_track_pt->at(TruthIndex),1000.*InDet_track_charge->at(InDetInDex)/InDet_track_pt->at(InDetInDex));
        }
      } // truthTracks

      for ( unsigned int iTruth = 0; iTruth < Truth_track_pt->size(); iTruth++){ // truth track
        truthTrack.SetPtEtaPhiM( 
            Truth_track_pt->at(iTruth),
            Truth_track_eta->at(iTruth),
            Truth_track_phi->at(iTruth),
            Truth_track_m->at(iTruth)
            );

        if ( InDet_numTracks == 0 )      h_TruthTrack_PtExact[0][0] -> Fill( Truth_track_pt -> at(iTruth)/1000. );
        else if ( InDet_numTracks == 1 ) h_TruthTrack_PtExact[0][1] -> Fill( Truth_track_pt -> at(iTruth)/1000. );
        else if ( InDet_numTracks > 1 )  h_TruthTrack_PtExact[0][2] -> Fill( Truth_track_pt -> at(iTruth)/1000. );

        if ( InDet_numTracks == 0 ){
          //h_InDetTrack_Pt[0]  -> Fill( InDet_track_pt ->at(iID)/1000. );
        } else if( InDet_numTracks == 1 ) {

          /* truth 1-prong && exact reco 1-prong */ 
          for ( unsigned int iID = 0; iID<InDet_track_pt->size(); iID++){ // reco track
            recoTrack.SetPtEtaPhiM( 
                InDet_track_pt ->at(iID),
                InDet_track_eta->at(iID),
                InDet_track_phi->at(iID),
                InDet_track_m  ->at(iID)
                );
            h_DeltaR[0]   ->Fill( truthTrack.Pt()/1000., recoTrack.DeltaR(truthTrack));
            h_DeltaR_1D[0]->Fill( recoTrack.DeltaR(truthTrack));
            double qOverp = 1000. * InDet_track_charge->at(iID)/InDet_track_pt ->at(iID);
            h_InDetTrack_Pt[1]       -> Fill(InDet_track_pt ->at(iID)/1000.);
            h_InDetTrack_QOverPt[1]  -> Fill(qOverp);

            //if ( InDet_track_numberOfPixelHits->at(iID) + InDet_track_numberOfSCTHits->at(iID) >= 13 ){
            if ( recoTrack.DeltaR(truthTrack) < 100){
              double pt_optimzed = InDet_track_pt ->at(iID)/1000.;
              h_InDetTrack_Pt_Optimized[1]->Fill(1/pt_optimzed);
            }
            //}
            double TruthQOverPt = 1000.*Truth_track_charge->at(iTruth)/Truth_track_pt->at(iTruth);
            double RecoQOverPt  = 1000.*InDet_track_charge->at(iID)   /InDet_track_pt->at(iID);
            h_truthTrackRecoTrack[0]->Fill( TruthQOverPt, RecoQOverPt);


            if ( recoTrack.DeltaR(truthTrack) < 0.01 ){ 
              if ( Truth_tau_decayRad < 122.5 ){
                h_InDet_track_numberOfPixelHits[0] ->Fill( InDet_track_numberOfPixelHits->at(iID) );
                h_InDet_track_numberOfSCTHits[0]   ->Fill( InDet_track_numberOfSCTHits  ->at(iID) );
              } else if ( Truth_tau_decayRad > 122.5 ) {
                h_InDet_track_numberOfPixelHits[1] ->Fill( InDet_track_numberOfPixelHits->at(iID) );
                h_InDet_track_numberOfSCTHits[1]   ->Fill( InDet_track_numberOfSCTHits  ->at(iID) );
              }

              h_TruthTrackRecoMatched_Pt[0]->Fill(Truth_track_pt->at(iTruth)/1000.);
              h_chargedTrackEta -> Fill( InDet_track_eta->at(iID) );
              h_chargedTrackPhi -> Fill( InDet_track_phi->at(iID) );
            }

            /* The number of pixel hits are recorded int the following procedure. */
            int total_pixel_hits =0; 
            total_pixel_hits += InDet_track_numberOfIBLHits->at(iID);
            total_pixel_hits += InDet_track_numberOfBLayerHits->at(iID);
            total_pixel_hits += InDet_track_numberOfPixelL1Hits->at(iID);
            total_pixel_hits += InDet_track_numberOfPixelL2Hits->at(iID);
            if ( Truth_tau_decayRad < 122.5 ) {
              h_PixelHits[0]        -> Fill( total_pixel_hits );
              h_PixelHits_IBL[0]    -> Fill( InDet_track_numberOfIBLHits    ->at(iID) );
              h_PixelHits_BLayer[0] -> Fill( InDet_track_numberOfBLayerHits ->at(iID) );
              h_PixelHits_L1[0]     -> Fill( InDet_track_numberOfPixelL1Hits     ->at(iID) );
              h_PixelHits_L2[0]     -> Fill( InDet_track_numberOfPixelL2Hits     ->at(iID) );
            } else if ( Truth_tau_decayRad > 122.5 ){
              h_PixelHits[1]        -> Fill( total_pixel_hits );
              h_PixelHits_IBL[1]    -> Fill( InDet_track_numberOfIBLHits    ->at(iID) );
              h_PixelHits_BLayer[1] -> Fill( InDet_track_numberOfBLayerHits ->at(iID) );
              h_PixelHits_L1[1]     -> Fill( InDet_track_numberOfPixelL1Hits     ->at(iID) );
              h_PixelHits_L2[1]     -> Fill( InDet_track_numberOfPixelL2Hits     ->at(iID) );
            }
            h_PixelHits_DecayRad->Fill( Truth_tau_decayRad, total_pixel_hits );
            if ( Truth_tau_decayRad < 33.25 ) {
              h_PixelHits_DecayRegion[0] -> Fill( total_pixel_hits);
            } else if ( 33.25 < Truth_tau_decayRad && Truth_tau_decayRad < 50.5 ){
              h_PixelHits_DecayRegion[1] -> Fill( total_pixel_hits);
            } else if ( 50.5  < Truth_tau_decayRad && Truth_tau_decayRad < 88.5 ){
              h_PixelHits_DecayRegion[2] -> Fill( total_pixel_hits);
            } else if ( 88.5  < Truth_tau_decayRad && Truth_tau_decayRad < 122.5){
              h_PixelHits_DecayRegion[3] -> Fill( total_pixel_hits);
            } else if ( 122.5 < Truth_tau_decayRad ) {
              h_PixelHits_DecayRegion[4] -> Fill( total_pixel_hits);
            }
          }
        } else if ( InDet_numTracks > 1 ){
          for ( unsigned int iID = 0; iID<InDet_track_pt->size(); iID++){ // reco track
            h_InDetTrack_Pt[2]  -> Fill( InDet_track_pt ->at(iID)/1000. );
            if ( InDet_track_numberOfPixelHits->at(iID) + InDet_track_numberOfSCTHits->at(iID) >= 9 ){
              h_InDetTrack_Pt_Optimized[2]->Fill( InDet_track_pt ->at(iID)/1000. );
            }
          }
        }

        for ( unsigned int iImpact=0; iImpact< InDet_track_d0->size(); iImpact++){
          h_ImpactParameter_d0->Fill( InDet_track_d0->at(iImpact) );
          h_ImpactParameter_z0->Fill( InDet_track_z0->at(iImpact) );
        }
      }// out of Pixel
    }
    }
    /* 
       truth 3-prong tau 
       Truth track momentum */
    else if ( Truth_tau_numTracks == 3 ) { 

      if ( Truth_tau_decayRad < 122.5 ) {
        h_InDetTrack_num_3prong       -> Fill( InDet_numTracks );
      } else {
        h_InDetTrack_num_3prong_outer -> Fill( InDet_numTracks );
      }
      for ( unsigned int iTruth = 0; iTruth < Truth_track_pt->size(); iTruth++){ 
        h_truthTrackPt_3prong      -> Fill (Truth_track_pt->at(iTruth)/1000.);
        h_TruthPt3prong            -> Fill (Truth_track_pt->at(iTruth)/1000.);
        h_TruthTrack_QOverPt[4]    -> Fill (1000*Truth_track_charge->at(iTruth)/Truth_track_pt->at(iTruth));
        if ( InDet_numTracks < 3 )      h_TruthTrack_PtExact[1][0]  -> Fill( Truth_track_pt -> at(iTruth)/1000. );
        else if ( InDet_numTracks == 3 ) h_TruthTrack_PtExact[1][1] -> Fill( Truth_track_pt -> at(iTruth)/1000. );
        else if ( InDet_numTracks > 3 )  h_TruthTrack_PtExact[1][2] -> Fill( Truth_track_pt -> at(iTruth)/1000. );
      }
      h_numberOfPixelClusters[prong(3)] ->Fill( InDet_pixel_numAllOfClusters );

      std::vector<double> recoTrack_pt;
      std::vector<double> recoTrack_eta;
      std::vector<double> recoTrack_phi;
      std::vector<double> recoTrack_m;
      for ( unsigned int iReco = 0; iReco<InDet_track_pt->size(); iReco++){ // reco track
        recoTrack_pt .push_back( InDet_track_pt ->at(iReco) );
        recoTrack_eta.push_back( InDet_track_eta->at(iReco) );
        recoTrack_phi.push_back( InDet_track_phi->at(iReco) );
        recoTrack_m  .push_back( InDet_track_m  ->at(iReco) );
      }
      std::vector<double> truthTrack_pt;
      std::vector<double> truthTrack_eta;
      std::vector<double> truthTrack_phi;
      std::vector<double> truthTrack_m;
      for ( unsigned int iTruth= 0; iTruth< Truth_track_pt->size(); iTruth++){ // reco track
        truthTrack_pt .push_back( Truth_track_pt ->at(iTruth) );
        truthTrack_eta.push_back( Truth_track_eta->at(iTruth) );
        truthTrack_phi.push_back( Truth_track_phi->at(iTruth) );
        truthTrack_m  .push_back( Truth_track_m  ->at(iTruth) );
      }

      int isMatchedCount = 0;
      for ( int iCount=0; iCount<3; iCount++){
        if ( recoTrack_pt.size() == 0 ) break;
        int RecoIndex  = 999;
        int TruthIndex = 999;
        double minDeltaR = 999;
        for ( unsigned int iTruth = 0; iTruth < truthTrack_pt.size(); iTruth++){ 
          TLorentzVector truthTrack;
          truthTrack.SetPtEtaPhiM( 
              truthTrack_pt .at(iTruth),
              truthTrack_eta.at(iTruth),
              truthTrack_phi.at(iTruth),
              truthTrack_m  .at(iTruth)
              );
          for ( unsigned int iReco = 0; iReco< recoTrack_pt.size(); iReco++){ // reco track
            TLorentzVector recoTrack;
            recoTrack.SetPtEtaPhiM( 
                recoTrack_pt .at(iReco),
                recoTrack_eta.at(iReco),
                recoTrack_phi.at(iReco),
                recoTrack_m  .at(iReco)
                );
            if ( recoTrack.DeltaR(truthTrack) < minDeltaR ) {
              minDeltaR  = recoTrack.DeltaR(truthTrack);
              RecoIndex  = iReco;
              TruthIndex = iTruth;
            }
            h_DeltaR_AllTracks[prong(3)]   ->Fill( truthTrack.Pt()/1000., recoTrack.DeltaR(truthTrack));
          }
        }
        //if ( minDeltaR < 0.002 ) isMatchedCount++;
        if ( truthTrack_pt.at(TruthIndex)/1000. < 200 ){
          if ( minDeltaR < 0.002 ) isMatchedCount++;
        } else if ( truthTrack_pt.at(TruthIndex)/1000. < 400 ){
          if ( minDeltaR < 0.001 ) isMatchedCount++;
        } else if ( truthTrack_pt.at(TruthIndex)/1000. < 600 ){
          if ( minDeltaR < 0.001 ) isMatchedCount++;
        } else if ( truthTrack_pt.at(TruthIndex)/1000. < 800 ){
          if ( minDeltaR < 0.001 ) isMatchedCount++;
        } else if ( truthTrack_pt.at(TruthIndex)/1000. < 1000 ){
          if ( minDeltaR < 0.001 ) isMatchedCount++;
        }
        h_DeltaR_AllTracks_1D_3prong[0] -> Fill( minDeltaR );
        recoTrack_pt  .erase( recoTrack_pt  .begin() + RecoIndex);
        recoTrack_eta .erase( recoTrack_eta .begin() + RecoIndex);
        recoTrack_phi .erase( recoTrack_phi .begin() + RecoIndex);
        recoTrack_m   .erase( recoTrack_m   .begin() + RecoIndex);
        truthTrack_pt .erase( truthTrack_pt .begin() + TruthIndex);
        truthTrack_eta.erase( truthTrack_eta.begin() + TruthIndex);
        truthTrack_phi.erase( truthTrack_phi.begin() + TruthIndex);
        truthTrack_m  .erase( truthTrack_m  .begin() + TruthIndex);
      }
      if ( isMatchedCount == 3 ) {
        for ( unsigned int iTruth = 0; iTruth < Truth_track_pt->size(); iTruth++){ 
          h_TruthTrackPt_MatchedTruthPt[1][0] -> Fill( Truth_track_pt->at(iTruth)/1000.);
        }
      }

      if( InDet_numTracks == 3 ){
        /* Delta R */
        std::vector<double> RecoTrack_pt;
        std::vector<double> RecoTrack_eta;
        std::vector<double> RecoTrack_phi;
        std::vector<double> RecoTrack_m;
        for ( unsigned int iReco = 0; iReco<InDet_track_pt->size(); iReco++){ // reco track
          RecoTrack_pt .push_back( InDet_track_pt ->at(iReco) );
          RecoTrack_eta.push_back( InDet_track_eta->at(iReco) );
          RecoTrack_phi.push_back( InDet_track_phi->at(iReco) );
          RecoTrack_m  .push_back( InDet_track_m  ->at(iReco) );
        }
        for ( unsigned int iTruth = 0; iTruth < Truth_track_pt->size(); iTruth++) {
          TLorentzVector truthTrack, recoMinimum;
          int minimumDelta = 999;
          int index = -999;
          truthTrack.SetPtEtaPhiM( 
              Truth_track_pt->at(iTruth),
              Truth_track_eta->at(iTruth),
              Truth_track_phi->at(iTruth),
              Truth_track_m->at(iTruth)
              );

          for ( unsigned int iReco = 0; iReco < RecoTrack_pt.size(); iReco++){ // reco track
            TLorentzVector recoTrack;
            recoTrack.SetPtEtaPhiM( 
                RecoTrack_pt .at(iReco),
                RecoTrack_eta.at(iReco),
                RecoTrack_phi.at(iReco),
                RecoTrack_m  .at(iReco)
                );
            if ( recoTrack.DeltaR(truthTrack) < minimumDelta ){
              minimumDelta = recoTrack.DeltaR(truthTrack);
              index = iReco;
            }
          }// iReco

          recoMinimum.SetPtEtaPhiM( 
              RecoTrack_pt .at(index),
              RecoTrack_eta.at(index),
              RecoTrack_phi.at(index),
              RecoTrack_m  .at(index)
              );
          RecoTrack_pt .erase( RecoTrack_pt .begin() + index);
          RecoTrack_eta.erase( RecoTrack_eta.begin() + index);
          RecoTrack_phi.erase( RecoTrack_phi.begin() + index);
          RecoTrack_m  .erase( RecoTrack_m  .begin() + index);

          h_DeltaR[1]->Fill( truthTrack.Pt()/1000., recoMinimum.DeltaR(truthTrack));

        }// end of Delta_R

        // Detailed studies of the number of tracks
        int numMatchedTracks = 0;
        for ( unsigned int iTruth = 0; iTruth < Truth_track_pt->size(); iTruth++) {
          TLorentzVector truthTrack, recoMinimum;
          int minimumDelta = 999;
          int index = -999;
          truthTrack.SetPtEtaPhiM( 
              Truth_track_pt->at(iTruth),
              Truth_track_eta->at(iTruth),
              Truth_track_phi->at(iTruth),
              Truth_track_m->at(iTruth)
              );

          /* Find a closest reco-track */ 
          for ( unsigned int iReco = 0; iReco<InDet_track_pt->size(); iReco++){ // reco track
            TLorentzVector recoTrack;
            recoTrack.SetPtEtaPhiM( 
                InDet_track_pt ->at(iReco),
                InDet_track_eta->at(iReco),
                InDet_track_phi->at(iReco),
                InDet_track_m  ->at(iReco)
                );
            if ( recoTrack.DeltaR(truthTrack) < minimumDelta ){
              minimumDelta = recoTrack.DeltaR(truthTrack);
              index = iReco;
            }
          }// iReco

          recoMinimum.SetPtEtaPhiM( 
              InDet_track_pt ->at(index),
              InDet_track_eta->at(index),
              InDet_track_phi->at(index),
              InDet_track_m  ->at(index)
              );
          double TruthQOverPt = 1000.*Truth_track_charge->at(iTruth)/Truth_track_pt->at(iTruth);
          double RecoQOverPt  = 1000.*InDet_track_charge->at(index)/ InDet_track_pt->at(index);
          h_truthTrackRecoTrack[1]->Fill( TruthQOverPt, RecoQOverPt);

          /* Calculate the deltaR */
          if ( recoMinimum.DeltaR(truthTrack) < 0.01 ) {
            if ( Truth_tau_decayRad < 122.5 ) {
              h_InDet_track_numberOfPixelHits[2] ->Fill( InDet_track_numberOfPixelHits->at(index) );
              h_InDet_track_numberOfSCTHits[2]   ->Fill( InDet_track_numberOfSCTHits  ->at(index) );
            } else if ( Truth_tau_decayRad > 122.5 ) {
              h_InDet_track_numberOfPixelHits[3] ->Fill( InDet_track_numberOfPixelHits->at(index) );
              h_InDet_track_numberOfSCTHits[3]   ->Fill( InDet_track_numberOfSCTHits  ->at(index) );
            }
            numMatchedTracks++;
            InDet_track_pt ->erase( InDet_track_pt->begin()  + index);
            InDet_track_eta->erase( InDet_track_eta->begin() + index);
            InDet_track_phi->erase( InDet_track_phi->begin() + index);
            InDet_track_m  ->erase( InDet_track_m->begin()   + index);
          }
        }//iTruth
        if( numMatchedTracks == 3 ) {
          for ( unsigned int iTruth = 0; iTruth < Truth_track_pt->size(); iTruth++){ // truth track
            h_TruthTrackRecoMatched_Pt[4]->Fill(Truth_track_pt->at(iTruth)/1000.);
          }
        }

        for ( unsigned int iID = 0; iID<InDet_track_pt->size(); iID++) { // reco track
          h_InDetTrack_QOverPt[4]->Fill(1000.*InDet_track_charge->at(iID)/InDet_track_pt->at(iID));
        }

        //for ( unsigned int iID = 0; iID<InDet_track_pt->size(); iID++){ // reco track
        //      int total_pixel_hits =0; 
        //      total_pixel_hits += InDet_track_numberOfIBLHits->at(iID);
        //      total_pixel_hits += InDet_track_numberOfBLayerHits->at(iID);
        //      total_pixel_hits += InDet_track_numberOfPixelL1Hits->at(iID);
        //      total_pixel_hits += InDet_track_numberOfPixelL2Hits->at(iID);
        //    if ( Truth_tau_decayRad < 122.5 ) {
        //      h_PixelHits[0]        -> Fill( total_pixel_hits );
        //      h_PixelHits_IBL[0]    -> Fill( InDet_track_numberOfIBLHits    ->at(iID) );
        //      h_PixelHits_BLayer[0] -> Fill( InDet_track_numberOfBLayerHits ->at(iID) );
        //      h_PixelHits_L1[0]     -> Fill( InDet_track_numberOfPixelL1Hits     ->at(iID) );
        //      h_PixelHits_L2[0]     -> Fill( InDet_track_numberOfPixelL2Hits     ->at(iID) );
        //    } else if ( Truth_tau_decayRad > 122.5 ){
        //      h_PixelHits[1]        -> Fill( total_pixel_hits );
        //      h_PixelHits_IBL[1]    -> Fill( InDet_track_numberOfIBLHits    ->at(iID) );
        //      h_PixelHits_BLayer[1] -> Fill( InDet_track_numberOfBLayerHits ->at(iID) );
        //      h_PixelHits_L1[1]     -> Fill( InDet_track_numberOfPixelL1Hits     ->at(iID) );
        //      h_PixelHits_L2[1]     -> Fill( InDet_track_numberOfPixelL2Hits     ->at(iID) );
        //    }
        //    if ( Truth_tau_decayRad < 33.25 ) {
        //      h_PixelHits_DecayRegion[0] -> Fill( total_pixel_hits);
        //    } else if ( 33.25 < Truth_tau_decayRad && Truth_tau_decayRad < 50.5 ){
        //      h_PixelHits_DecayRegion[1] -> Fill( total_pixel_hits);
        //    } else if ( 50.5  < Truth_tau_decayRad && Truth_tau_decayRad < 88.5 ){
        //      h_PixelHits_DecayRegion[2] -> Fill( total_pixel_hits);
        //    } else if ( 88.5  < Truth_tau_decayRad && Truth_tau_decayRad < 122.5){
        //      h_PixelHits_DecayRegion[3] -> Fill( total_pixel_hits);
        //    } else if ( 122.5 < Truth_tau_decayRad ) {
        //      h_PixelHits_DecayRegion[4] -> Fill( total_pixel_hits);
        //    }
        //  }
        for ( unsigned int iID = 0; iID<InDet_track_pt->size(); iID++){ // reco track
          h_InDetTrack_Pt[4]  -> Fill( InDet_track_pt ->at(iID)/1000. );
          if ( InDet_track_numberOfPixelHits->at(iID) + InDet_track_numberOfSCTHits->at(iID) >= 9 ){
            h_InDetTrack_Pt_Optimized[4]->Fill( InDet_track_pt ->at(iID)/1000. );
          }
        }
      } else if( InDet_numTracks < 3 ){
        for ( unsigned int iID = 0; iID<InDet_track_pt->size(); iID++){ // reco track
          h_InDetTrack_Pt[3]  -> Fill( InDet_track_pt ->at(iID)/1000. );
          if ( InDet_track_numberOfPixelHits->at(iID) + InDet_track_numberOfSCTHits->at(iID) >= 9 ){
            h_InDetTrack_Pt_Optimized[3]->Fill( InDet_track_pt ->at(iID)/1000. );
          }
        }
      } else if ( InDet_numTracks > 3 ){
        for ( unsigned int iID = 0; iID<InDet_track_pt->size(); iID++){ // reco track
          h_InDetTrack_Pt[5]  -> Fill( InDet_track_pt ->at(iID)/1000. );
          if ( InDet_track_numberOfPixelHits->at(iID) + InDet_track_numberOfSCTHits->at(iID) >= 9 ){
            h_InDetTrack_Pt_Optimized[5]->Fill( InDet_track_pt ->at(iID)/1000. );
          }
        }
      }
    }

    h_TruthPtHadronic->Fill(Truth_tau_pt/1000.);
    if ( Truth_tau_isHadronic == true && Truth_tau_numTracks == 1 ) h_truth_pt_had[0]->Fill(Truth_tau_pt/1000.);
    if ( Truth_tau_isHadronic == true && Truth_tau_numTracks == 3 ) h_truth_pt_had[1]->Fill(Truth_tau_pt/1000.);

    h_neutrino_pt_hadronic->Fill(tt_truth_neutrino_pt/1000.);
    h_truth_decayRad[0] ->Fill(Truth_tau_decayRad);

    if( Truth_tau_numTracks == 1 ) {
      h_truth_decayRad[1] ->Fill(Truth_tau_decayRad);
      if( Truth_tau_isMatchedWithReco  && Reco_tau_numTracks == 1) 
        h_truth_decayRad[3] ->Fill(Truth_tau_decayRad);
    }
    if( Truth_tau_numTracks == 3 ) {
      h_truth_decayRad[2] ->Fill(Truth_tau_decayRad);
      if( Truth_tau_isMatchedWithReco  && Reco_tau_numTracks == 3) 
        h_truth_decayRad[4] ->Fill(Truth_tau_decayRad);
    }
    Int_t truth_nProng = 999;
    if ( Truth_tau_numTracks == 1 ) truth_nProng = 0; 
    if ( Truth_tau_numTracks == 3 ) truth_nProng = 1; 

    h_number_of_tracks[truth_nProng][7]->Fill( Truth_tau_decayRad );
    if( Truth_tau_isMatchedWithReco ) {
      // Reco-tau found
      h_number_of_tracks[truth_nProng][Reco_tau_numTracks]->Fill( Truth_tau_decayRad );
    }

    if ( !Truth_tau_isMatchedWithReco ) continue;

    h_Reco_tau_pt_all->Fill(Reco_tau_pt/1000.);
    if( Truth_tau_isLeptonic ) h_Reco_tau_pt_leptonic->Fill(Reco_tau_pt/1000.);
    if( Truth_tau_isHadronic ) {
      if ( Truth_tau_numTracks == 1 ) h_Reco_tau_pt_hadronic                     ->Fill(Reco_tau_pt/1000.);
      if ( Truth_tau_numTracks == 1 ) h_Reco_tau_pt_had[0][Reco_tau_numTracks] ->Fill(Reco_tau_pt/1000.); 
      if ( Truth_tau_numTracks == 3 ) h_Reco_tau_pt_had[1][Reco_tau_numTracks] ->Fill(Reco_tau_pt/1000.);
    }

    // -------------------------------------------
    //
    //       The number of pixel/SCT hits       
    //
    // -------------------------------------------
    if ( !Truth_tau_isHadronic ) continue;
    Int_t xBin=0;
    Int_t xBinAll=5;
    if(                               Truth_tau_decayRad < 33.25 ) xBin = 0;
    if( 33.25 < Truth_tau_decayRad && Truth_tau_decayRad < 50.5  ) xBin = 1;
    if( 50.5  < Truth_tau_decayRad && Truth_tau_decayRad < 88.5  ) xBin = 2;
    if( 88.5  < Truth_tau_decayRad && Truth_tau_decayRad < 122.5 ) xBin = 3;
    if( 122.5 < Truth_tau_decayRad                               ) xBin = 4;

    if ( Truth_tau_numTracks == 1 ) truth_nProng = 0;
    if ( Truth_tau_numTracks == 3 ) truth_nProng = 1;

    for ( unsigned int index=0; index< tt_numberOfPixelHits->size(); index++){
      h_numberOfPixelHits[truth_nProng][xBin]  ->Fill( tt_numberOfPixelHits->at(index));
      h_numberOfSCTHits[truth_nProng][xBin]    ->Fill( tt_numberOfSCTHits->at(index));

      if ( Truth_tau_numTracks == 3 && Reco_tau_numTracks == 3){
        h_numberOfPixelHitsDecayRegion->Fill( Truth_tau_decayRad, tt_numberOfPixelHits->at(index));
        h_numberOfPixelHits_InnerMost_vs_DecayRegion->Fill( Truth_tau_decayRad, tt_numberOfInnermostPixelLayerHits->at(index));
        h_numberOfPixelHits_NextToInnerMost_vs_DecayRegion->Fill( Truth_tau_decayRad, tt_numberOfNextToInnermostPixelLayerHits->at(index));
        h_numberOfSCTHits_vs_DecayRegion->Fill( Truth_tau_decayRad, tt_numberOfSCTHits->at(index));
      }
    }
    if ( Truth_tau_numTracks == 1 && Reco_tau_isSelected ){
      for( unsigned int iTrack=0; iTrack<tt_numberOfPixelHits->size(); iTrack++){
        h_numberOfPixelHitsDecayRadius[truth_nProng][Reco_tau_numTracks] ->Fill( Truth_tau_decayRad, tt_numberOfPixelHits->at(iTrack) );
        h_numberOfPixelHitsEta[truth_nProng][xBin]                             ->Fill( Truth_tau_eta,          tt_numberOfPixelHits->at(iTrack) );
        h_numberOfPixelHitsEta[truth_nProng][xBinAll]                          ->Fill( Truth_tau_eta,          tt_numberOfPixelHits->at(iTrack) );
      }
    }
    if ( Truth_tau_numTracks == 3 && Reco_tau_isSelected ){
      for( unsigned int iTrack=0; iTrack<tt_numberOfPixelHits->size(); iTrack++){
        h_numberOfPixelHitsDecayRadius[truth_nProng][Reco_tau_numTracks] ->Fill( Truth_tau_decayRad, tt_numberOfPixelHits->at(iTrack) );
        h_numberOfPixelHitsEta[truth_nProng][xBin]                             ->Fill( Truth_tau_eta,          tt_numberOfPixelHits->at(iTrack) );
        h_numberOfPixelHitsEta[truth_nProng][xBinAll]                          ->Fill( Truth_tau_eta,          tt_numberOfPixelHits->at(iTrack) );
      }
    }

    // ----------------------------------
    //      Reco-tau impact parameters
    // ----------------------------------
    for ( int iD0=0; iD0< Reco_track_d0->size(); iD0++){
      if ( Reco_tau_numTracks == 1 ) h_numTracksVSd0[0]->Fill(Reco_track_d0->at(iD0));
      if ( Reco_tau_numTracks == 3 ) h_numTracksVSz0[1]->Fill(Reco_track_z0->at(iD0));
    }
    // --------------------------
    //      Pixel raw data
    // --------------------------
    for ( unsigned int index=0; index< Reco_track_numberOfPixelHits->size(); index++){
      h_numberOfRawPixelHits[truth_nProng][xBin]  ->Fill( Reco_track_numberOfPixelHits->at(index));
    }
  }
  //_//_//_//_//_//_//_//_//_//_//_//_//_//_//_//_//_//_//_//_//_//_//_//_ //_//_//_//_//_//_//_//_//_//_//_//_//_//_//_//_//_//_//_//_//_//_//_//
  //
  //                             Draw histograms
  //
  //_//_//_//_//_//_//_//_//_//_//_//_//_//_//_//_//_//_//_//_//_//_//_//_ //_//_//_//_//_//_//_//_//_//_//_//_//_//_//_//_//_//_//_//_//_//_//_//
#ifdef _Momentum_All 
  // ----------------------------------------------------------------------------------------
  //  This is the basic momentum distribution. 
  //  I can compare the truth visible momentum and reco-tau momentum.
  // ----------------------------------------------------------------------------------------
  TCanvas* MomentumAll = new TCanvas("MomentumAll","MomentumAll");
  h_truth_pt_all    = (TH1D*)h_truth_pt_all->Clone();
  h_Reco_tau_pt_all  = (TH1D*)h_Reco_tau_pt_all->Clone();

  h_Reco_tau_pt_all->SetLineColor(kMagenta);

  h_truth_pt_all->Draw();
  h_Reco_tau_pt_all->Draw("same");

  //ATLASLabel(0.2,0.8,"Simulation");
  TLegend* legMomentumAll = new TLegend(0.2,0.65,0.6,0.8);
  legMomentumAll->SetFillColor(0);
  legMomentumAll->SetFillStyle(0);
  legMomentumAll->SetBorderSize(0);
  legMomentumAll->AddEntry(h_truth_pt_all,   "truth visible energy", "l");
  legMomentumAll->AddEntry(h_Reco_tau_pt_all, "reconsructed energy",  "l");
  legMomentumAll->Draw();
#endif

#ifdef _Momentum_truth 
  // ----------------------------------------------------------------------------------------
  //  I can see how the truth tau decays to each channel. 
  //  This is the visible momentum.
  // ----------------------------------------------------------------------------------------
  TCanvas* Truth_MomentumComponent = new TCanvas("Truth_MomentumComponent","Truth_MomentumComponent");
  h_truth_pt_all      = (TH1D*)h_truth_pt_all      ->Clone();
  h_TruthPtHadronic = (TH1D*)h_TruthPtHadronic ->Clone();
  h_truth_pt_electron = (TH1D*)h_truth_pt_electron ->Clone();
  h_truth_pt_muon     = (TH1D*)h_truth_pt_muon     ->Clone();

  h_TruthPtHadronic->SetLineColor(kRed);
  h_truth_pt_electron->SetLineColor(kBlue);
  h_truth_pt_muon->SetLineColor(kGreen);

  h_truth_pt_all->GetYaxis()->SetRangeUser(0,311);
  h_truth_pt_all->Draw();
  h_TruthPtHadronic->Draw("same");
  h_truth_pt_electron->Draw("same");
  h_truth_pt_muon->Draw("same");

  Truth_MomentumComponent->RedrawAxis();

  ATLASLabel(0.2,0.8,"Simulation");
  TLegend* legTruth_MomentumComponent = new TLegend(0.2,0.65,0.6,0.8);
  legTruth_MomentumComponent->SetFillColor(0);
  legTruth_MomentumComponent->SetFillStyle(0);
  legTruth_MomentumComponent->SetBorderSize(0);
  legTruth_MomentumComponent->AddEntry(h_truth_pt_all,      Form("Truth tau    : %0.f events", h_truth_pt_all->Integral()), "l");
  legTruth_MomentumComponent->AddEntry(h_TruthPtHadronic, Form("hadronic tau : %0.f events", h_TruthPtHadronic->Integral()), "l");
  legTruth_MomentumComponent->AddEntry(h_truth_pt_electron, Form("-> electron  : %0.f events", h_truth_pt_electron->Integral()), "l");
  legTruth_MomentumComponent->AddEntry(h_truth_pt_muon,     Form("-> muon      : %0.f events", h_truth_pt_muon->Integral()), "l");
  legTruth_MomentumComponent->Draw();
#endif

#ifdef _Momentum_Leptonic 
  // ----------------------------------------------------------------------------------------
  //  This is a mometum distribution of the leptonic decay.
  // ----------------------------------------------------------------------------------------
  TCanvas* MomentumLeptonic = new TCanvas("MomentumLeptonic","MomentumLeptonic");
  h_truth_pt_leptonic->GetYaxis()->SetRangeUser(0,2000);

  h_truth_pt_leptonic->SetLineColor(kRed);

  h_truth_pt_leptonic->Draw();
  h_Reco_tau_pt_leptonic->Draw("same");

  ATLASLabel(0.2,0.8,"Simulation");
  TLegend* legMomentumLeptonic = new TLegend(0.2,0.65,0.6,0.8);
  legMomentumLeptonic->SetFillColor(0);
  legMomentumLeptonic->SetFillStyle(0);
  legMomentumLeptonic->SetBorderSize(0);
  legMomentumLeptonic->AddEntry(h_truth_pt_leptonic, "truth leptonic", "l");
  legMomentumLeptonic->AddEntry(h_Reco_tau_pt_leptonic,  "reco leptonic", "l");
  legMomentumLeptonic->Draw();
#endif 

#ifdef _Momentum_Hadronic
  // ----------------------------------------------------------------------------------------
  //  I can compare the truth visible momentum and reco-tau momentum in the hadronic decay.
  // ----------------------------------------------------------------------------------------
  TCanvas* Momentum_Hadronic = new TCanvas("Momentum_Hadronic","Momentum_Hadronic");
  h_TruthPtHadronic    = (TH1D*)h_TruthPtHadronic->Clone();
  h_Reco_tau_pt_hadronic = (TH1D*)h_Reco_tau_pt_hadronic->Clone();

  h_TruthPtHadronic->GetYaxis()->SetRangeUser(0,1400);
  h_TruthPtHadronic->SetLineColor(kMagenta);
  h_TruthPtHadronic->Draw();
  h_Reco_tau_pt_hadronic->Draw("same");

  ATLASLabel(0.2,0.8,"Simulation");
  TLegend* legMomentumHadronic = new TLegend(0.2,0.68,0.7,0.8);
  legMomentumHadronic->SetFillColor(0);
  legMomentumHadronic->SetFillStyle(0);
  legMomentumHadronic->SetBorderSize(0);
  legMomentumHadronic->AddEntry(h_TruthPtHadronic,      Form("truth hadronic : %0.f events", h_TruthPtHadronic->Integral()), "l");
  legMomentumHadronic->AddEntry(h_Reco_tau_pt_hadronic, Form("reco hadronic  : %0.f events", h_Reco_tau_pt_hadronic->Integral()), "l");
  legMomentumHadronic->Draw();
#endif

#ifdef _Momentum_HaronicReco
  TCanvas* Momentum_HaronicReco = new TCanvas("Momentum_HaronicReco", "Momentum_HaronicReco");
  h_truth_pt_had[0] = (TH1D*)h_truth_pt_had[0]->Clone();
  h_truth_pt_had[1] = (TH1D*)h_truth_pt_had[1]->Clone();
  h_truth_pt_had[0]->SetLineColor(kMagenta);
  h_truth_pt_had[0]->Add( h_truth_pt_had[1] );
  h_truth_pt_had[0]->Draw();
  std::cout << " reco truth : " << h_truth_pt_had[0]->Integral() << std::endl;
  
//  h_Reco_tau_pt_hadronic->GetYaxis()->SetRangeUser(0,900);
  h_Reco_tau_pt_hadronic->Draw("same");
  std::cout << " reco : " << h_Reco_tau_pt_hadronic->Integral() << std::endl;

  TLegend* legMomentum_HaronicReco = new TLegend(0.2, 0.6, 0.4, 0.9);
  for ( int iProng=0; iProng<7; iProng++){
    h_Reco_tau_pt_had[0][iProng]->SetLineColor(iProng+2);
    h_Reco_tau_pt_had[0][iProng]->Add(h_Reco_tau_pt_had[1][iProng]);
    h_Reco_tau_pt_had[0][iProng]->Draw("same");
    legMomentum_HaronicReco -> AddEntry( h_Reco_tau_pt_had[0][iProng], Form(" recon %d-prong", iProng), "l");
    std::cout << iProng << " prong : " << h_Reco_tau_pt_had[0][iProng]->Integral() << std::endl;
  }
  legMomentum_HaronicReco->Draw("same");
  Momentum_HaronicReco->RedrawAxis();
#endif

#ifdef _Momentum_HadronicContents
  // ----------------------------------------------------------------------------------------
  //  I can compare the truth visible momentum and reco-tau momentum in the hadronic decay.
  // ----------------------------------------------------------------------------------------
  TCanvas* Momentum_HadronicContents = new TCanvas("Momentum_HadronicContents","Momentum_HadronicContents");
  h_TruthPtHadronic = (TH1D*)h_TruthPtHadronic->Clone();
  h_truth_pt_had[0]   = (TH1D*)h_truth_pt_had[0]->Clone();
  h_truth_pt_had[1]   = (TH1D*)h_truth_pt_had[1]->Clone();

//  h_TruthPtHadronic->GetYaxis()->SetRangeUser(0,1400);
  h_TruthPtHadronic->Draw();
  h_truth_pt_had[0]->SetLineColor(kRed);
  h_truth_pt_had[0]->Draw("same");
  h_truth_pt_had[1]->SetLineColor(kBlue);
  h_truth_pt_had[1]->Draw("same");

  ATLASLabel(0.2,0.8,"Simulation");
  TLegend* legMomentumHadronic = new TLegend(0.2, 0.6, 0.8, 0.75);
  legMomentumHadronic->SetFillColor(0);
  legMomentumHadronic->SetFillStyle(0);
  legMomentumHadronic->SetBorderSize(0);
  legMomentumHadronic->AddEntry(h_TruthPtHadronic,      Form("truth hadronic : %0.f events", h_TruthPtHadronic->Integral()), "l");
  legMomentumHadronic->AddEntry(h_truth_pt_had[0],        Form("truth hadronic 1-prong : %0.f events", h_truth_pt_had[0]->Integral()), "l");
  legMomentumHadronic->AddEntry(h_truth_pt_had[1],        Form("truth hadronic 3-prong : %0.f events", h_truth_pt_had[1]->Integral()), "l");
  legMomentumHadronic->Draw();
#endif

#ifdef _Momentum_HaronicReco1Prong
  TCanvas* Momentum_HaronicReco1Prong = new TCanvas("Momentum_HaronicReco1Prong", "Momentum_HaronicReco1Prong");
  TLegend* legMomentum_HaronicReco1Prong = new TLegend(0.2, 0.6, 0.4, 0.9);
  
  h_truth_pt_had[0]->SetLineStyle(2);
  h_truth_pt_had[0]->GetYaxis()->SetRangeUser(0, 3100);
  h_truth_pt_had[0]->Draw();
  legMomentum_HaronicReco1Prong -> AddEntry( h_truth_pt_had[0], Form("truth 1-prong : %0.f evt", h_truth_pt_had[0]->Integral()), "l");
  std::cout << " truth 1-prong " << h_truth_pt_had[0]->Integral() << std::endl;
  
  for ( int iProng=0; iProng<7; iProng++){
    h_Reco_tau_pt_had[0][iProng]->SetLineColor(iProng+1);
    h_Reco_tau_pt_had[0][iProng]->Draw("same");
    legMomentum_HaronicReco1Prong -> AddEntry( h_Reco_tau_pt_had[0][iProng], Form("reco %d-prong : %0.f evt", iProng, h_Reco_tau_pt_had[0][iProng]->Integral() ), "l");
    std::cout << iProng << " prong : " << h_Reco_tau_pt_had[0][iProng]->Integral() << std::endl;
  }
  legMomentum_HaronicReco1Prong->Draw("same");
  Momentum_HaronicReco1Prong->RedrawAxis();

#endif

#ifdef _Momentum_HaronicReco3Prong
  TCanvas* Momentum_HaronicReco3Prong = new TCanvas("Momentum_HaronicReco3Prong", "Momentum_HaronicReco3Prong");
  TLegend* legMomentum_HaronicReco3Prong = new TLegend(0.2, 0.6, 0.4, 0.9);
  
  h_truth_pt_had[1]->SetLineStyle(2);
  h_truth_pt_had[1]->GetYaxis()->SetRangeUser(0, 3100);
  h_truth_pt_had[1]->Draw();
  legMomentum_HaronicReco3Prong -> AddEntry( h_truth_pt_had[1], Form("truth 3-prong : %0.f evt", h_truth_pt_had[1]->Integral()), "l");
  std::cout << " truth 3-prong " << h_truth_pt_had[1]->Integral() << std::endl;
  
  for ( int iProng=0; iProng<7; iProng++){
    h_Reco_tau_pt_had[1][iProng]->SetLineColor(iProng+1);
    h_Reco_tau_pt_had[1][iProng]->Draw("same");
    legMomentum_HaronicReco3Prong -> AddEntry( h_Reco_tau_pt_had[1][iProng], Form("reco %d-prong : %0.f evt", iProng, h_Reco_tau_pt_had[1][iProng]->Integral() ), "l");
    std::cout << iProng << " prong : " << h_Reco_tau_pt_had[1][iProng]->Integral() << std::endl;
  }
  legMomentum_HaronicReco3Prong->Draw("same");
  Momentum_HaronicReco3Prong->RedrawAxis();

#endif

#ifdef _Momentum_reco
  // -----------------------------------------------------------------
  //  I can see how reco-tau decays in each channel.
  // -----------------------------------------------------------------
  TCanvas* Momentum_reco = new TCanvas("Momentum_reco","Momentum_reco");
  /// Clone
  h_Reco_tau_pt_all         = (TH1D*)h_Reco_tau_pt_all->Clone();
  h_Reco_tau_pt_leptonic    = (TH1D*)h_Reco_tau_pt_leptonic->Clone();
  h_Reco_tau_pt_hadronic    = (TH1D*)h_Reco_tau_pt_hadronic->Clone();

  h_Reco_tau_pt_all->GetYaxis()->SetRangeUser(0,311);

  /// SetLineColor 
  h_Reco_tau_pt_all->SetLineColor(kBlack);
  h_Reco_tau_pt_leptonic->SetLineColor(kBlue);
  h_Reco_tau_pt_hadronic->SetLineColor(kRed);

  /// Draw
  h_Reco_tau_pt_all->Draw();
  h_Reco_tau_pt_leptonic->Draw("same");
  h_Reco_tau_pt_hadronic->Draw("same");

  /// Integral 
  h_Reco_tau_pt_all->Integral();
  h_Reco_tau_pt_leptonic->Draw("same");
  h_Reco_tau_pt_hadronic->Draw("same");

  /// Legend
  ATLASLabel(0.2,0.85, "Simulation");

  TLegend* legMomentum_reco = new TLegend(0.2, 0.7, 0.7, 0.8);
  legMomentum_reco->SetFillColor(0);
  legMomentum_reco->SetBorderSize(0);
  legMomentum_reco->AddEntry(h_Reco_tau_pt_all,      Form("reco all : %.0f events", h_Reco_tau_pt_all->Integral() ), "lp");
  legMomentum_reco->AddEntry(h_Reco_tau_pt_hadronic, Form("reco (hadronic matched) : %.0f events", h_Reco_tau_pt_hadronic->Integral()), "l");
  legMomentum_reco->AddEntry(h_Reco_tau_pt_leptonic, Form("reco (leptonic matched) : %.0f events", h_Reco_tau_pt_leptonic->Integral()), "l");
  legMomentum_reco->Draw();
#endif

#ifdef _Momentum_reco_ElectronMuonRemoval
  // -----------------------------------------------------------------
  TCanvas* Momentum_reco_ElectronMuonRemoval = new TCanvas("Momentum_reco_ElectronMuonRemoval","Momentum_reco_ElectronMuonRemoval");
  h_Reco_tau_pt_all          = (TH1D*)h_Reco_tau_pt_all->Clone();
  h_Reco_tau_pt_all_MuonOLR  = (TH1D*)h_Reco_tau_pt_all_MuonOLR->Clone();
  h_Reco_tau_pt_all_EleOLR   = (TH1D*)h_Reco_tau_pt_all_EleOLR->Clone();

  h_Reco_tau_pt_all_MuonOLR->SetFillStyle(3444);
  h_Reco_tau_pt_all_MuonOLR->SetFillColor(kRed);

  h_Reco_tau_pt_all_EleOLR->SetFillStyle(3445);
  h_Reco_tau_pt_all_EleOLR->SetFillColor(kBlue);

  h_Reco_tau_pt_all->Draw();
  h_Reco_tau_pt_all_MuonOLR->Draw("same");
  h_Reco_tau_pt_all_EleOLR->Draw("same");
#endif

#ifdef _DecayRadius 
  // -------------------------------------------------------------------------
  //  I can see the decay radius distribution.
  // -------------------------------------------------------------------------
  TCanvas* DecayRadius = new TCanvas("DecayRadius","DecayRadius");
  DecayRadius->SetLogy();
  
  //ATLASLabel(0.6,0.8,"Simulation");
  TLegend* legDecayRadius = new TLegend(0.45,0.7,0.75,0.9);
  legDecayRadius->SetFillColor(0);
  legDecayRadius->SetFillStyle(0);
  legDecayRadius->SetBorderSize(0);
  legDecayRadius->SetTextSize(0.03);
  
  for ( int iCateg=0; iCateg<5; iCateg++){
    // Clone
    h_truth_decayRad[iCateg] = (TH1D*)h_truth_decayRad[iCateg]->Clone();
    
    // decorate
    switch (iCateg){
      case 0:
        h_truth_decayRad[iCateg] -> SetMarkerSize(0.7);
        h_truth_decayRad[iCateg] -> SetMarkerColor(kBlack);
        legDecayRadius->AddEntry(h_truth_decayRad[iCateg],  Form("truth hadronic : %0.f events", h_truth_decayRad[iCateg]->Integral()), "lp");
        break;
      case 1:
        h_truth_decayRad[iCateg] -> SetMarkerSize(0.7);
        h_truth_decayRad[iCateg] -> SetMarkerColor(kRed);
        legDecayRadius->AddEntry(h_truth_decayRad[iCateg],  Form("truth 1-prong : %0.f events", h_truth_decayRad[iCateg]->Integral()), "lp");
        break;
      case 2:
        h_truth_decayRad[iCateg] -> SetMarkerSize(0.7);
        h_truth_decayRad[iCateg] -> SetMarkerColor(kBlue);
        legDecayRadius->AddEntry(h_truth_decayRad[iCateg],  Form("truth 3-prong : %0.f events", h_truth_decayRad[iCateg]->Integral()), "lp");
        break;
      case 3: // reco-matched truth tau
        h_truth_decayRad[iCateg] -> SetMarkerStyle(32);
        h_truth_decayRad[iCateg] -> SetMarkerColor(kRed);
        legDecayRadius->AddEntry(h_truth_decayRad[iCateg],  Form("truth 1-prong (reco 1-prong): %0.f events", h_truth_decayRad[iCateg]->Integral()), "lp");
        break;
      case 4: // reco-matched truth tau
        h_truth_decayRad[iCateg] -> SetMarkerStyle(32);
        h_truth_decayRad[iCateg] -> SetMarkerColor(kBlue);
        legDecayRadius->AddEntry(h_truth_decayRad[iCateg],  Form("truth 3-prong (reco 3-prong) : %0.f events", h_truth_decayRad[iCateg]->Integral()), "lp");
        break;
    }
    h_truth_decayRad[iCateg]->Draw("same:p");
  }
  legDecayRadius->Draw();
  //  PixelLayer(h_truth_decayRad[0], 3200);
  //DecayRadius->SaveAs("DecayRadius_FullG4_LongLived.pdf");
#endif

#ifdef _Momentum_truth_neutritno
  // ----------------------------------------------------------------------------------------------
  //  
  // ----------------------------------------------------------------------------------------------
  TCanvas* Momentum_truth_neutritno = new TCanvas("Momentum_truth_neutritno","Momentum_truth_neutritno");
  h_truth_pt_all         = (TH1D*)h_truth_pt_all->Clone();
  h_neutrino_pt_all      = (TH1D*)h_neutrino_pt_all->Clone();
  h_neutrino_pt_hadronic = (TH1D*)h_neutrino_pt_hadronic->Clone();
  h_neutrino_pt_leptonic = (TH1D*)h_neutrino_pt_leptonic->Clone();

  h_neutrino_pt_all->SetLineColor(kMagenta);
  h_neutrino_pt_hadronic->SetFillStyle(3445);
  h_neutrino_pt_hadronic->SetLineColor(kOrange+1);
  h_neutrino_pt_leptonic->SetFillStyle(3454);
  h_neutrino_pt_leptonic->SetLineColor(kAzure+1);

  h_truth_pt_all->Draw();
  h_neutrino_pt_all->Draw("same");
  h_neutrino_pt_hadronic->Draw("same");
  h_neutrino_pt_leptonic->Draw("same");

  ATLASLabel(0.2,0.85, "Simulation");
  TLegend* legMomentum_truth_neutritno = new TLegend(0.2, 0.7, 0.7, 0.8);
  legMomentum_truth_neutritno->SetFillColor(0);
  legMomentum_truth_neutritno->SetBorderSize(0);
  legMomentum_truth_neutritno->AddEntry(h_neutrino_pt_all,      "neutrino all", "l");
  legMomentum_truth_neutritno->AddEntry(h_neutrino_pt_hadronic, "from hadronic","l");
  legMomentum_truth_neutritno->AddEntry(h_neutrino_pt_leptonic, "from leptonic","l");
  legMomentum_truth_neutritno->Draw();

  Momentum_truth_neutritno->RedrawAxis();
#endif  

#ifdef _numberOfXXX 
  // ----------------------------------------------------------------------------------------------
  TCanvas* numberOfXXX = new TCanvas("numberOfXXX","numberOfXXX", 800, 800);

  TStyle* style = new TStyle();
  style->SetPalette(1);
  //numberOfXXX->Divide(2,2);
  //numberOfXXX->cd(1);
  h_numberOfPixelHitsDecayRegion->SetMarkerColor(kWhite);
  h_numberOfPixelHitsDecayRegion->Draw("text45 colz");

  Double_t ymax = h_numberOfPixelHitsDecayRegion->GetYaxis()->GetXmax();
  TLine* IBL = new TLine(33.25, -0.5, 33.25, ymax);
  IBL->SetLineWidth(2);
  IBL->SetLineStyle(9);
  IBL->SetLineColor(kBlack);
  IBL->Draw("same");

  TLine* Blayer = new TLine(50.5, -0.5, 50.5, ymax);
  Blayer->SetLineWidth(2);
  Blayer->SetLineStyle(9);
  Blayer->SetLineColor(kBlack);
  Blayer->Draw("same");

  TLine* Pix2 = new TLine(88.5, -0.5, 88.5, ymax);
  Pix2->SetLineWidth(2);
  Pix2->SetLineStyle(9);
  Pix2->SetLineColor(kBlack);
  Pix2->Draw("same");

  TLine* Pix3 = new TLine(122.5, -0.5, 122.5, ymax);
  Pix3->SetLineWidth(2);
  Pix3->SetLineStyle(9);
  Pix3->SetLineColor(kBlack);
  Pix3->Draw("same");
  /*
     numberOfXXX->cd(2);
     h_numberOfSCTHits_vs_DecayRegion->Draw("colz");

     ymax = h_numberOfSCTHits_vs_DecayRegion->GetYaxis()->GetXmax();
     IBL->Draw("same");
     Blayer->Draw("same");
     Pix2->Draw("same");
     Pix3->Draw("same");

     numberOfXXX->cd(3);
     h_numberOfPixelHits_InnerMost_vs_DecayRegion->Draw("colz");
     numberOfXXX->cd(4);
     h_numberOfPixelHits_NextToInnerMost_vs_DecayRegion->Draw("colz");
     */
#endif

#ifdef _numberOfPixelHits_DecayRadius // -----------------------------------------------------------------------------------
  TCanvas* numberOfPixelHits_DecayRadius = new TCanvas("numberOfPixelHits_DecayRadius","numberOfPixelHits_DecayRadius", 1000, 400);

  gStyle->SetPalette(1);
  gStyle->SetOptStat(1);
  numberOfPixelHits_DecayRadius->Divide(5,2);

  for( int iTruthProng=0; iTruthProng<2; iTruthProng++){
    for( int iRecoProng=0; iRecoProng<5; iRecoProng++){
      numberOfPixelHits_DecayRadius->cd(iTruthProng*5 + iRecoProng+1);
      h_numberOfPixelHitsDecayRadius[iTruthProng][iRecoProng]->SetMarkerColor(kWhite);
      h_numberOfPixelHitsDecayRadius[iTruthProng][iRecoProng]->SetMarkerSize(0.9);
      h_numberOfPixelHitsDecayRadius[iTruthProng][iRecoProng]->Draw("text45 colz");

      if( h_numberOfPixelHitsDecayRadius[iTruthProng][iRecoProng]->Integral() == 0 ){
        TText* t = new TText(0.5,0.6, "Empty");
        t->SetNDC(1);
        t->SetTextAlign(22);
        t->SetTextColor(kBlack);
        t->SetTextFont(43);
        t->SetTextSize(40);
        t->SetTextAngle(45);
        t->Draw();
        continue;
      }

      TText* t = new TText(0.7,0.9, Form("reco %d-prong", iRecoProng));
      t->SetNDC(1);
      t->SetTextAlign(22);
      t->SetTextColor(kBlack);
      t->SetTextFont(43);
      t->SetTextSize(15);
      t->Draw("same");
      gPad->SetLogz();
    }
  }
  numberOfPixelHits_DecayRadius->SaveAs("numberOfPixelHits_DecayRadius.pdf");
#endif

#ifdef _numberOfPixelHits_Eta // -----------------------------------------------------------------------------------
  TCanvas* numberOfPixelHits_Eta = new TCanvas("numberOfPixelHits_Eta","numberOfPixelHits_Eta", 1000, 400);

  gStyle->SetPalette(1);
  gStyle->SetOptStat(1);
  numberOfPixelHits_Eta->Divide(6,2);

  std::string strRegion[6] ={"X<33.25mm","33.25mm<X<50.5mm","50.5mm<X<88.5mm","88.5mm<X<122.5mm","122.5mm<X","all"};
  for( int iTruthProng=0; iTruthProng<2; iTruthProng++){
    for( int iRegion=0; iRegion<6; iRegion++){
      numberOfPixelHits_Eta->cd(iTruthProng*6 + iRegion+1);
      h_numberOfPixelHitsEta[iTruthProng][iRegion]->SetMarkerColor(kWhite);
      h_numberOfPixelHitsEta[iTruthProng][iRegion]->SetMarkerSize(0.9);
      h_numberOfPixelHitsEta[iTruthProng][iRegion]->Draw("text45 colz");

      TText* t = new TText(0.7,0.9, Form("%s", strRegion[iRegion].c_str()));
      t->SetNDC(1);
      t->SetTextAlign(22);
      t->SetTextColor(kBlack);
      t->SetTextFont(43);
      t->SetTextSize(10);
      t->Draw();
    }
  }
  numberOfPixelHits_Eta->SaveAs("numberOfPixelHits_Eta.pdf");
#endif


#ifdef _DecayRadius_vs_NumberOfXXX // ---------------------------------------------------------------------------------------

  gStyle->SetOptLogy(1); 

  TCanvas* DecayRadius_vs_NumberOfXXX_Pixel = new TCanvas("DecayRadius_vs_NumberOfXXX_Pixel","DecayRadius_vs_NumberOfXXX_Pixel", 1200, 800);
  DecayRadius_vs_NumberOfXXX_Pixel->Divide(6,4);

  std::vector<Double_t> eventsPixel_[2][5];
  std::vector<Double_t> eventsSCT_[2][5];

  for( int iProng = 0; iProng<2; iProng++){
    for ( int iRegion =0; iRegion<5; iRegion++) {
      // SetLineColor
      h_numberOfPixelHits[iProng][iRegion]->SetLineColor(iRegion+1);
      h_numberOfSCTHits[iProng][iRegion]->SetLineColor(iRegion+1);
      // Integral
      eventsPixel_[iProng][iRegion] .push_back(h_numberOfPixelHits[iProng][iRegion]->Integral() );
      eventsSCT_[iProng][iRegion]   .push_back(h_numberOfSCTHits[iProng][iRegion]->Integral()   );
      // Scale
      //      h_numberOfPixelHits[iProng][iRegion]->Scale(1/h_numberOfPixelHits[iProng][iRegion]->Integral());
      //      h_numberOfSCTHits[iProng][iRegion]->Scale(1/h_numberOfSCTHits[iProng][iRegion]->Integral());
    }
  }

  for( int iProng = 0; iProng < 2; iProng++){
    /** the number of Pixel hits  **/
    for ( int iRegion =0; iRegion<5; iRegion++) {
      DecayRadius_vs_NumberOfXXX_Pixel->cd(iRegion+1 + iProng*12);
      h_numberOfPixelHits[iProng][iRegion]->Draw();

      /* TLegend */
      TLegend* legDecayRadius_vs_NumberOfXXX = new TLegend(0.3,0.65,0.9,0.8);
      legDecayRadius_vs_NumberOfXXX->SetFillColor(0);
      legDecayRadius_vs_NumberOfXXX->SetFillStyle(0);
      legDecayRadius_vs_NumberOfXXX->SetBorderSize(0);
      //      legDecayRadius_vs_NumberOfXXX->AddEntry( h_numberOfPixelHits[iProng][iRegion], Form( "%d events", eventsPixel_[iProng][iRegion], ""));
      //     legDecayRadius_vs_NumberOfXXX->Draw();
    }

    DecayRadius_vs_NumberOfXXX_Pixel->cd(6 + iProng*12);
    for ( int iRegion =0; iRegion<5; iRegion++) {
      h_numberOfPixelHits[iProng][iRegion]->Draw("same");
    }
    DecayRadius_vs_NumberOfXXX_Pixel->RedrawAxis();

    /** the number of SCT hits  **/
    for ( int iRegion =0; iRegion<5; iRegion++) {
      DecayRadius_vs_NumberOfXXX_Pixel->cd(iRegion+1+6 + iProng*12);
      h_numberOfSCTHits[iProng][iRegion]->Draw();

      /* TLegend */
      TLegend* legDecayRadius_vs_NumberOfXXX = new TLegend(0.7,0.65,0.9,0.8);
      legDecayRadius_vs_NumberOfXXX->SetFillColor(0);
      legDecayRadius_vs_NumberOfXXX->SetFillStyle(0);
      legDecayRadius_vs_NumberOfXXX->SetBorderSize(0);
      //      legDecayRadius_vs_NumberOfXXX->AddEntry( h_numberOfSCTHits[iProng][iRegion], Form( "%d events", eventsSCT[iProng][iRegion], ""));
      //      legDecayRadius_vs_NumberOfXXX->Draw();
    }

    DecayRadius_vs_NumberOfXXX_Pixel->cd(12 + iProng*12);
    for ( int iRegion =0; iRegion<5; iRegion++) {
      h_numberOfSCTHits[iProng][iRegion]->Draw("same");
    }
  }

  DecayRadius_vs_NumberOfXXX_Pixel->RedrawAxis();
#endif

#ifdef _DecayRadius_vs_RawNumberOfXXX // ------------------------------------------------------------------------------------------------------
  TCanvas* DecayRadius_vs_RawNumberOfXXX = new TCanvas("DecayRadius_vs_RawNumberOfXXX","DecayRadius_vs_RawNumberOfXXX", 1200, 400);
  DecayRadius_vs_RawNumberOfXXX->Divide(6,2);

  std::vector<Double_t> eventsPixel[2][5];
  std::vector<Double_t> eventsSCT[2][5];

  for( int iProng = 0; iProng<2; iProng++){
    for ( int iRegion =0; iRegion<5; iRegion++) {
      // SetLineColor
      h_numberOfRawPixelHits[iProng][iRegion]->SetLineColor(iRegion+1);
      if( iRegion == 4 ) h_numberOfRawPixelHits[iProng][iRegion]->SetLineColor(kMagenta);
      // Integral
      eventsPixel[iProng][iRegion] .push_back(h_numberOfRawPixelHits[iProng][iRegion]->Integral() );
      // Scale
      //      h_numberOfPixelHits[iProng][iRegion]->Scale(1/h_numberOfPixelHits[iProng][iRegion]->Integral());
      //      h_numberOfSCTHits[iProng][iRegion]->Scale(1/h_numberOfSCTHits[iProng][iRegion]->Integral());
    }
  }

  for( int iProng = 0; iProng < 2; iProng++){
    /** the number of Pixel hits  **/
    for ( int iRegion =0; iRegion<5; iRegion++) {
      DecayRadius_vs_RawNumberOfXXX->cd(iRegion+1 + iProng*6);
      h_numberOfRawPixelHits[iProng][iRegion]->Draw();

      /* TLegend */
      std::stringstream ss;
      ss << h_numberOfRawPixelHits[iProng][iRegion]->Integral() << " evn";
      TText *t = new TText(0.7,0.7, ss.str().c_str() );
      t->SetNDC(1);
      t->SetTextAlign(22);
      t->SetTextColor(kBlack);
      t->SetTextFont(43);
      t->SetTextSize(15);
      t->Draw("same");
      /*
         TLegend* legDecayRadius_vs_RawNumberOfXXX = new TLegend(0.4,0.3,0.9,0.8);
         legDecayRadius_vs_RawNumberOfXXX->SetFillColor(0);
         legDecayRadius_vs_RawNumberOfXXX->SetFillStyle(0);
         legDecayRadius_vs_RawNumberOfXXX->SetBorderSize(0);
         legDecayRadius_vs_RawNumberOfXXX->AddEntry( h_numberOfRawPixelHits[iProng][iRegion], Form( "%d events", h_numberOfRawPixelHits[iProng][iRegion]->Integral(), ""));
         legDecayRadius_vs_RawNumberOfXXX->Draw();
         */
    }
    DecayRadius_vs_RawNumberOfXXX->RedrawAxis();

    /* Add all of the histograms */
    DecayRadius_vs_RawNumberOfXXX->cd(6 + iProng*6);
    for ( int iRegion =0; iRegion<5; iRegion++) {
      h_numberOfRawPixelHits[iProng][iRegion]->Draw("same");
      DecayRadius_vs_RawNumberOfXXX->RedrawAxis();
    }
  }
#endif

#ifdef _DecayRadius_vs_RawNumberOfXXX_Normalize // ------------------------------------------------------------------------------------------------------
  TCanvas* DecayRadius_vs_RawNumberOfXXX_Normalize = new TCanvas("DecayRadius_vs_RawNumberOfXXX_Normalize","DecayRadius_vs_RawNumberOfXXX_Normalize", 1200, 400);
  DecayRadius_vs_RawNumberOfXXX_Normalize->Divide(6,2);

  for( int iProng = 0; iProng<2; iProng++){
    for ( int iRegion =0; iRegion<5; iRegion++) {
      // SetLineColor
      h_numberOfRawPixelHits[iProng][iRegion]->SetLineColor(iRegion+1);
      if( iRegion == 4 ) h_numberOfRawPixelHits[iProng][iRegion]->SetLineColor(kMagenta);
      // Scale
      //h_numberOfRawPixelHits[iProng][iRegion]->Scale(1/h_numberOfRawPixelHits[iProng][iRegion]->Integral());
    }
  }

  for( int iProng = 0; iProng < 2; iProng++){
    /** the number of Pixel hits  **/
    for ( int iRegion =0; iRegion<5; iRegion++) {
      DecayRadius_vs_RawNumberOfXXX_Normalize->cd(iRegion+1 + iProng*6);
      gPad->SetLogy();
      h_numberOfRawPixelHits[iProng][iRegion]->Draw();
    }
    DecayRadius_vs_RawNumberOfXXX_Normalize->RedrawAxis();

    /* Add all of the histograms */
    DecayRadius_vs_RawNumberOfXXX_Normalize->cd(6 + iProng*6);
    for ( int iRegion =0; iRegion<5; iRegion++) {
      gPad->SetLogy();
      h_numberOfRawPixelHits[iProng][iRegion]->Draw("same");
      DecayRadius_vs_RawNumberOfXXX_Normalize->RedrawAxis();
    }
  }
  TFile* compare_file = new TFile( Form("compare_%s.root", simulator.c_str()), "recreate");
  for( int iProng = 0; iProng < 2; iProng++){
    for ( int iRegion =0; iRegion<5; iRegion++) {
      h_numberOfRawPixelHits[iProng][iRegion]->Write();
    }
  }
  compare_file->Close();

#endif

#ifdef _Efficiency_DecayRadius 
  // --------------------------------------------------------------------------------------------
  //  I can show the tracking efficiency.
  // --------------------------------------------------------------------------------------------
  TCanvas* Efficiency_DecayRadius = new TCanvas("Efficiency_DecayRadius", "Efficiency_DecayRadius", 1000, 500);
  Efficiency_DecayRadius->Divide(2,1);

  for ( int iProng=0; iProng<2; iProng++){
    Efficiency_DecayRadius->cd(iProng+1);

    /* TLegend */
    TLegend* legEfficiency_DecayRadius;
    if ( iProng == 0 ) legEfficiency_DecayRadius = new TLegend(0.65,0.5,0.95,0.75);
    if ( iProng == 1 ) legEfficiency_DecayRadius = new TLegend(0.2,0.7,0.5,0.95);
    legEfficiency_DecayRadius->SetFillColor(0);
    legEfficiency_DecayRadius->SetFillStyle(0);
    legEfficiency_DecayRadius->SetBorderSize(0);

    /* */
    h_number_of_tracks[iProng][7]->Sumw2();
    for( int iRecoProng =0; iRecoProng < 7 ; iRecoProng++ ){
      h_number_of_tracks[iProng][iRecoProng]->SetLineColor(iRecoProng+1);
      h_number_of_tracks[iProng][iRecoProng]->SetMarkerSize(0.7);
      h_number_of_tracks[iProng][iRecoProng]->SetMarkerColor(iRecoProng+1);

      h_number_of_tracks[iProng][iRecoProng]->Sumw2();
      h_number_of_tracks[iProng][iRecoProng]->Divide( h_number_of_tracks[iProng][iRecoProng], h_number_of_tracks[iProng][7], 1,1,"B");

      h_number_of_tracks[iProng][iRecoProng]->Draw("same:pe");
      legEfficiency_DecayRadius->AddEntry( h_number_of_tracks[iProng][iRecoProng], Form( "reco %d prong", iRecoProng));
      //if ( iRecoProng == 5 ) legEfficiency_DecayRadius->AddEntry( h_number_of_tracks[iProng][iRecoProng], "Reco-tau not found");
      //if ( iRecoProng == 6 ) legEfficiency_DecayRadius->AddEntry( h_number_of_tracks[iProng][iRecoProng], "Reco-tau matched truth-tau" );
    }
    h_number_of_tracks[iProng][0]->GetYaxis()->SetRangeUser(0,1.05);
    legEfficiency_DecayRadius->Draw("");
  }
  Efficiency_DecayRadius->SaveAs("Efficiency_DecayRadius_FullG4_Longlived.pdf");
#endif

#ifdef _Efficiency_pT
  TCanvas* Efficiency_pT = new TCanvas("Efficiency_pT", "Efficiency_pT", 1000, 500);
  Efficiency_pT->Divide(2,1);
  h_truth_pt_had[0] = (TH1D*)h_truth_pt_had[0]->Clone(); // truth 1-prong tau pT 
  h_truth_pt_had[1] = (TH1D*)h_truth_pt_had[1]->Clone();
  for ( int iProng=0; iProng<7; iProng++){
    h_Reco_tau_pt_had[0][iProng] = (TH1D*)h_Reco_tau_pt_had[0][iProng]->Clone();
  }

  h_truth_pt_had[0]->Sumw2(); 
  h_truth_pt_had[1]->Sumw2(); 
  
  TLegend* legEfficiency_pT[2];
  legEfficiency_pT[0] = new TLegend(0.2, 0.6, 0.4, 0.9);
  legEfficiency_pT[1] = new TLegend(0.2, 0.6, 0.4, 0.9);
 
  for ( int iTruthProng=0; iTruthProng<2; iTruthProng++){
    Efficiency_pT->cd(iTruthProng+1);
    for ( int iProng=0; iProng<7; iProng++){
      h_Reco_tau_pt_had[iTruthProng][iProng]->SetLineColor(iProng+1);
      h_Reco_tau_pt_had[iTruthProng][iProng]->SetMarkerSize(0.7);
      h_Reco_tau_pt_had[iTruthProng][iProng]->SetMarkerColor(iProng+1);
      h_Reco_tau_pt_had[iTruthProng][iProng]->Sumw2();
      h_Reco_tau_pt_had[iTruthProng][iProng]->Divide( h_Reco_tau_pt_had[iTruthProng][iProng], h_truth_pt_had[iTruthProng], 1 ,1 ,"B");
      h_Reco_tau_pt_had[iTruthProng][iProng]->Draw("same:pe");
      h_Reco_tau_pt_had[iTruthProng][iProng]->GetYaxis()->SetRangeUser(0,1.05);
      legEfficiency_pT[iTruthProng] -> AddEntry( h_Reco_tau_pt_had[iTruthProng][iProng], Form("reco %d-prong ", iProng), "l");
      std::cout << iProng << " prong : " << h_Reco_tau_pt_had[iTruthProng][iProng]->Integral() << std::endl;
    }
    legEfficiency_pT[iTruthProng]->Draw("same");
  }

#endif

#ifdef _Efficiency_D0
  TCanvas* Efficiency_D0 = new TCanvas("Efficiency_D0", "Efficiency_D0");
  h_numTracksVSd0[0]->Draw();
  h_numTracksVSd0[1]->SetLineColor(kYellow);
  h_numTracksVSd0[1]->Draw("same");
#endif

#ifdef _TrackEfficiency
  TCanvas* TrackEfficiency = new TCanvas("TrackEfficiency", "TrackEfficiency", 900, 450);
  h_TruthTrackRecoMatched_Pt[0]     = (TH1D*)h_TruthTrackRecoMatched_Pt[0]->Clone();
  h_TruthTrackRecoMatched_Pt[4]     = (TH1D*)h_TruthTrackRecoMatched_Pt[4]->Clone();
  h_TruthTrackPt            = (TH1D*)h_TruthTrackPt    ->Clone();
  h_truthTrackPt_3prong     = (TH1D*)h_truthTrackPt_3prong    ->Clone();
  
  TrackEfficiency->Divide(2,1);
  TrackEfficiency->cd(1);

  h_TruthTrackRecoMatched_Pt[0] ->SetLineColor(kBlue);
  h_TruthTrackRecoMatched_Pt[0] ->SetMarkerSize(0.7);
  h_TruthTrackRecoMatched_Pt[0] ->SetMarkerColor(kBlue);

  h_TruthTrack_Pt[0]           ->Sumw2();
  h_TruthTrackRecoMatched_Pt[0]->Sumw2();
  h_TruthTrackRecoMatched_Pt[0]->Divide( h_TruthTrackRecoMatched_Pt[0], h_TruthTrack_Pt[0], 1,1, "B");
  h_TruthTrackRecoMatched_Pt[0]->SetMaximum(1.02);
  h_TruthTrackRecoMatched_Pt[0]->SetMinimum(0);
  h_TruthTrackRecoMatched_Pt[0]->Draw("e");
  
  TrackEfficiency->cd(2);
  h_TruthTrackRecoMatched_Pt[4] ->SetLineColor(kBlue);
  h_TruthTrackRecoMatched_Pt[4] ->SetMarkerSize(0.7);
  h_TruthTrackRecoMatched_Pt[4] ->SetMarkerColor(kBlue);

  h_TruthTrackRecoMatched_Pt[4]->Sumw2();
  h_truthTrackPt_3prong        ->Sumw2();
  h_TruthTrackRecoMatched_Pt[4]->Divide( h_TruthTrackRecoMatched_Pt[4], h_truthTrackPt_3prong, 1,1, "B");
  h_TruthTrackRecoMatched_Pt[4]->SetMaximum(1.02);
  h_TruthTrackRecoMatched_Pt[4]->SetMinimum(0);
  h_TruthTrackRecoMatched_Pt[4]->Draw("e");
  
  TCanvas* TrackEfficiency_Merged = new TCanvas("TrackEfficiency_Merged", "TrackEfficiency_Merged");
  h_TruthTrackRecoMatched_Pt[0]     = (TH1D*)h_TruthTrackRecoMatched_Pt[0]->Clone();
  h_TruthTrackRecoMatched_Pt[4]     = (TH1D*)h_TruthTrackRecoMatched_Pt[4]->Clone();
  h_TruthTrackRecoMatched_Pt[0] ->SetLineColor(  kMagenta);
  h_TruthTrackRecoMatched_Pt[0] ->SetMarkerColor(kMagenta);
  h_TruthTrackRecoMatched_Pt[4] ->SetLineColor(kBlue);
  h_TruthTrackRecoMatched_Pt[4] ->Draw("e");
  h_TruthTrackRecoMatched_Pt[0] ->Draw("same:e");
#endif

#ifdef _Basic_Pix_clusters
  // -------------------------------------------------------------------------------------------
  //  I want to see the basic kinematics.
  // -------------------------------------------------------------------------------------------
  TCanvas* Basic_Pix_clusters = new TCanvas("Basic_Pix_clusters","Basic_Pix_clusters", 1200, 300);
  Basic_Pix_clusters->Divide(4,1);
  Basic_Pix_clusters->SetLogy();
  TString Layer[4] = {"IBL","B-Layer", "L1", "L2"};
  text(0.5,0.7, "test");
  for (int iCanvas=0; iCanvas<4; iCanvas++){
    Basic_Pix_clusters->cd(iCanvas+1);
    h_Pix[iCanvas]->Draw();
  }
#endif

#ifdef _Basic_kinematics
  // -------------------------------------------------------------------------------------------
  //  I want to see the basic kinematics.
  // -------------------------------------------------------------------------------------------
  TCanvas* Basic_kinematics_number_of_tracks = new TCanvas("Basic_kinematics_number_of_tracks", "Basic_kinematics_number_of_tracks" , 1000, 500 );
  Basic_kinematics_number_of_tracks->Divide(2,1);
  gPad->SetLogy();
  for ( int iHadLep=0; iHadLep<2; iHadLep++){
    Basic_kinematics_number_of_tracks->cd(iHadLep+1);
    h_numTracks[iHadLep]->Draw();
  }
  Basic_kinematics_number_of_tracks->SaveAs("Basic_kinematics_number_of_tracks.pdf");
#endif

#ifdef _numTrack_vs_DecayRadius
  TCanvas* numTrack_vs_DecayRadius = new TCanvas("numTrack_vs_DecayRadius", "numTrack_vs_DecayRadius");
  h_numTrackDecayR->Draw("colz");
#endif

#ifdef _Track_distribution
  TCanvas* Track_distribution = new TCanvas("Track_distribution","Track_distribution");
  Track_distribution->Divide(2,2);
  
  Track_distribution->cd(1);
  h_InDetTrack_Pt[1]->Draw(); 
  h_TruthTrackPt->SetLineColor(kRed);
  h_TruthTrackPt->Draw("same"); 

  Track_distribution->cd(2);
  h_InDetTrack_Eta[prong(1)]->Draw();
  h_truthTrackEta->SetLineColor(kRed);
  h_truthTrackEta->Draw("same");
  
  Track_distribution->cd(3);
  h_InDetTrack_Phi[prong(1)]->Draw();
  h_truthTrackPhi->SetLineColor(kRed);
  h_truthTrackPhi->Draw("same");
  
  Track_distribution->cd(4);
  h_InDetTrack_num_1prong->Draw();
#endif

#ifdef _numberOfTracks
  TCanvas* numberOfTracks = new TCanvas("numberOfTracks","numberOfTracks");
  numberOfTracks->Divide(2,2);

  numberOfTracks->cd(1);
  h_InDetTrack_num_1prong->Draw();
  h_InDetTrack_num_3prong->SetLineColor(kBlue);
  h_InDetTrack_num_3prong->Draw("same");
  
  numberOfTracks->cd(2);
  h_InDetTrack_num_1prong_outer->Draw();
  h_InDetTrack_num_3prong_outer->SetLineColor(kBlue);
  h_InDetTrack_num_3prong_outer->Draw("same");
#endif

#ifdef _numberOfTracks_optimized
  TCanvas* numberOfTracks_optimized = new TCanvas("numberOfTracks_optimized","numberOfTracks_optimized");
//  numberOfTracks->Divide(2,2);
  h_numberOfTracks[0]->Draw();
  std::cout << h_numberOfTracks[0]->Integral() << std::endl;
#endif

#ifdef _InDetTrack
  TCanvas* InDetTrack = new TCanvas("InDetTrack", "InDetTrack", 1500, 1000);
  InDetTrack->Divide(3,2);
  /* 1 truth tau && reco-matched truth tau */
  InDetTrack->cd(1);
  h_TruthTrack_Pt[0]->SetLineColor(kRed);
  h_TruthTrack_Pt[0]->Draw();
  h_TruthTrackRecoMatched_Pt[0]->Draw("same");

  /* 1 truth tau && exact 1 track (Q over Pt) */
  InDetTrack->cd(2);
  h_TruthTrack_QOverPt[0] -> SetLineColor(kRed);
  h_TruthTrack_QOverPt[0] -> Draw();
  h_InDetTrack_QOverPt[1] -> Draw("same");
  //h_truthTrackPt     ->SetLineColor(kRed);
  //h_truthTrackPt     ->Draw("same");     // 1-prong track truth pT
  //h_InDetTrack_Pt[1] ->Draw("same"); // exact 1-trac reco pT
  //h_InDetTrack_Pt_Optimized[1]->SetLineColor(kBlue); 
  //h_InDetTrack_Pt_Optimized[1]->Draw("same"); 

  //InDetTrack->cd(2);
  //h_InDetTrack_Pt[2]->Draw("same");
  //h_truthTrackPt->Draw("same");     // 1-prong track truth pT
  //std::cout << "1-prong : " <<  h_InDetTrack_Pt[2]->Integral() << " "  << h_InDetTrack_Pt_Optimized[2]->Integral() << h_truthTrackPt->Integral() << std::endl;
  
  /* 3-prong truth tau && exact 3 track */
  InDetTrack->cd(4);
  h_truthTrackPt_3prong->SetLineColor(kRed);
  h_truthTrackPt_3prong->Draw("same");
  h_TruthTrackRecoMatched_Pt[4]->Draw("same");
  
  /* 3-prong truth tau && exact 3 track (Q over Pt) */
  InDetTrack->cd(5);
  h_TruthTrack_QOverPt[4] -> SetLineColor(kRed);
  h_TruthTrack_QOverPt[4] -> Draw();
  h_InDetTrack_QOverPt[4] -> Draw("same");

  //for ( int iCanvas=0; iCanvas<3; iCanvas++ ){
  //  InDetTrack->cd(4+iCanvas);
  //  //h_InDetTrack_Pt[3+iCanvas]->GetYaxis()->SetRangeUser(0,100000);
  //  gPad->SetLogy();
  //  h_truthTrackPt_3prong->SetLineColor(kRed);
  //  h_truthTrackPt_3prong->Draw("same");
  //  h_InDetTrack_Pt[3+iCanvas]->Draw("same");
  //  h_InDetTrack_Pt_Optimized[3+iCanvas]->SetLineColor(kBlue);
  //  h_InDetTrack_Pt_Optimized[3+iCanvas]->Draw("same");
  //  std::cout << "3-prong : " << h_InDetTrack_Pt[3+iCanvas]->Integral() <<  " " << h_truthTrackPt_3prong->Integral() << std::endl;
  //}
  TCanvas* InDetTrack_qOverPt = new TCanvas("InDetTrack_qOverPt","InDetTrack_qOverPt");
  h_InDetTrack_qOverPt[prong(1)]->Draw();
  h_TruthTrackQOverPt[prong(1)]->SetLineColor(kRed);
  h_TruthTrackQOverPt[prong(1)]->Draw("same");
  
  TCanvas* InDetTrackProperties = new TCanvas("InDetTrackProperties","InDetTrackProperties",1500,1500);
  gStyle->SetPalette(1);
  InDetTrackProperties->Divide(4,4);
  int iInDetTrackProperties = 0;
  // eta
  iInDetTrackProperties++; InDetTrackProperties->cd(iInDetTrackProperties);
  h_InDetTrack_Eta[prong(1)]    ->Draw("same");
  // raw eta
  iInDetTrackProperties++; InDetTrackProperties->cd(iInDetTrackProperties);
  h_InDetTrack_RawEta[prong(1)] ->Scale(1/h_InDetTrack_RawEta[prong(1)]->Integral());
  h_InDetTrack_Eta[prong(1)]    ->Scale(1/h_InDetTrack_Eta[prong(1)]->Integral());
  h_TruthTrackEta[prong(1)]    ->Scale(1/h_TruthTrackEta[prong(1)]->Integral());
  h_InDetTrack_Eta[prong(1)]    ->SetLineColor(kRed);
  h_TruthTrackEta[prong(1)]    ->SetLineColor(kBlue);
  h_InDetTrack_RawEta[prong(1)] ->Draw();
  h_InDetTrack_Eta[prong(1)]    ->Draw("same");
  h_TruthTrackEta[prong(1)]     ->Draw("same");
  /////////// phi 
  iInDetTrackProperties++; InDetTrackProperties->cd(iInDetTrackProperties);
  //
  h_InDetTrack_RawPhi[prong(1)] ->Scale(1/h_InDetTrack_RawPhi[prong(1)]->Integral());
  h_InDetTrack_Phi[prong(1)] ->Scale(1/h_InDetTrack_Phi[prong(1)]->Integral());
  h_InDetTrack_RawPhi[prong(1)] ->Draw("same");
  h_InDetTrack_Phi[prong(1)] ->SetLineColor(kRed);
  h_InDetTrack_Phi[prong(1)] ->Draw("same");
  /////////// q/pt
  iInDetTrackProperties++; InDetTrackProperties->cd(iInDetTrackProperties);
  h_InDetTrack_RawqOverPt[prong(1)] -> Scale(1/h_InDetTrack_RawqOverPt[prong(1)]->Integral());
  h_InDetTrack_RawqOverPt[prong(1)] -> Draw();
  h_InDetTrack_qOverPt   [prong(1)] -> Scale(1/h_InDetTrack_qOverPt[prong(1)]->Integral());
  h_InDetTrack_qOverPt   [prong(1)] -> SetLineColor(kRed);
  h_InDetTrack_qOverPt   [prong(1)] -> Draw("same");
  // comparison between vertex eta and track eta
  iInDetTrackProperties++; InDetTrackProperties->cd(iInDetTrackProperties);
  //TCanvas* print = new TCanvas("","");
  h_TruthTrackEta[prong(1)]       ->SetLineColor(kBlack);
  h_TruthTrackEta[prong(1)]       ->Draw("same");
  h_TruthDecVtx_Eta[prong(1)]     ->SetLineColor(kRed);
  h_TruthDecVtx_Eta[prong(1)]     ->Scale(1/h_TruthDecVtx_Eta[prong(1)]->Integral());
  h_TruthDecVtx_Eta[prong(1)]     ->Draw("same");
//  print->Print("print.pdf");
  // 
  iInDetTrackProperties++; InDetTrackProperties->cd(iInDetTrackProperties);
  h_TruthTau_Eta[prong(1)] ->SetLineColor(kBlack);
  h_TruthTau_Eta[prong(1)] ->Scale(1/h_TruthTau_Eta[prong(1)]->Integral());
  h_TruthTau_Eta[prong(1)] ->Draw();
  h_TruthTrackEta[prong(1)]     ->SetLineColor(kYellow);
  h_TruthTrackEta[prong(1)]     ->Draw("same");
  // 
  iInDetTrackProperties++; InDetTrackProperties->cd(iInDetTrackProperties);
  h_TruthTau_TruthDecVtx_Eta[prong(1)] ->Draw("colz");
  // 
  iInDetTrackProperties++; InDetTrackProperties->cd(iInDetTrackProperties);
  h_TruthTrack_TruthDecVtx_Eta[prong(1)] ->Draw("colz");
  //
  iInDetTrackProperties++; InDetTrackProperties->cd(iInDetTrackProperties);
  //TCanvas* print = new TCanvas("","");
  h_TruthTau_TruthTrack_Eta[prong(1)] ->Draw("colz");
  //print->Print("print.pdf");
  /////////// TruthTrackPt vs InDetTrackPt
  iInDetTrackProperties++; InDetTrackProperties->cd(iInDetTrackProperties);
  h_TruthTrack_InDetTrac_qOverPt[prong(1)]->Draw("colz");
  
  
  TCanvas* VertexProperties = new TCanvas("VertexProperties","VertexProperties",1500,1500);
  gStyle->SetPalette(1);
  VertexProperties->Divide(4,4);
  int iVertexProperties = 0;
  /////////// Produciton vertex 
  iVertexProperties++; VertexProperties->cd(iVertexProperties);
  h_ProdVtx_X[prong(1)]    -> Draw();
  iVertexProperties++; VertexProperties->cd(iVertexProperties);
  h_ProdVtx_Y[prong(1)]    -> Draw();
  iVertexProperties++; VertexProperties->cd(iVertexProperties);
  h_ProdVtx_Z[prong(1)]    -> Draw();
  iVertexProperties++; VertexProperties->cd(iVertexProperties);
  h_ProdVtx_Eta[prong(1)]  -> Draw();
  iVertexProperties++; VertexProperties->cd(iVertexProperties);
  h_ProdVtx_Phi[prong(1)]  -> Draw();
  iVertexProperties++; VertexProperties->cd(iVertexProperties);
  h_ProdVtx_perp[prong(1)] -> Draw();
  /////////// Decay vertex
  iVertexProperties++; VertexProperties->cd(iVertexProperties);
  h_DecVtx_X[prong(1)]    -> Draw();
  iVertexProperties++; VertexProperties->cd(iVertexProperties);
  h_DecVtx_Y[prong(1)]    -> Draw();
  iVertexProperties++; VertexProperties->cd(iVertexProperties);
  h_DecVtx_Z[prong(1)]    -> Draw();
  iVertexProperties++; VertexProperties->cd(iVertexProperties);
  //h_DecVtx_Eta[prong(1)]  -> SetLineColor(kRed);
  h_DecVtx_Eta_Manual[prong(1)]  -> Draw("same");
  //h_DecVtx_Eta[prong(1)]  -> Draw("same");
  iVertexProperties++; VertexProperties->cd(iVertexProperties);
  h_DecVtx_Phi[prong(1)]  -> Draw();
  iVertexProperties++; VertexProperties->cd(iVertexProperties);
  h_DecVtx_perp[prong(1)] -> Draw();

//  TCanvas* print = new TCanvas();
//  h_DecVtx_X[prong(1)]    -> Draw(); print->Print("x.pdf");
//  h_DecVtx_Y[prong(1)]    -> Draw(); print->Print("y.pdf");
//  h_DecVtx_Z[prong(1)]    -> Draw(); print->Print("z.pdf");
//  h_DecVtx_Eta[prong(1)]  -> Draw(); print->Print("eta.pdf");
//  h_DecVtx_Phi[prong(1)]  -> Draw(); print->Print("phi.pdf");
//  h_DecVtx_perp[prong(1)] -> Draw(); print->Print("perp.pdf");

#endif

#ifdef _TrackExact
  TCanvas* TrackExact = new TCanvas("TrackExact", "TrackExact", 600, 1200);
  TrackExact->Divide(1,2);
  for ( int iProng=0; iProng<2; iProng++){
    TrackExact->cd(iProng+1);
    h_TruthTrack_Pt[0]->SetLineColor(kRed);
    h_TruthTrack_Pt[0]->Draw();
    for ( int iRange =0; iRange<3; iRange++){
//      h_TruthTrack_PtExact[0][iRange]->SetLineColor(iRange+2);
      h_TruthTrack_PtExact[0][iRange]->Draw("same");
    }
  }
#endif

#ifdef _ImpactParameter
  TCanvas* ImpactParameter = new TCanvas("ImpactParameter", "ImpactParameter");
  ImpactParameter->Divide(2,1);
  ImpactParameter->cd(1);
  h_ImpactParameter_d0->Draw();
  ImpactParameter->cd(2);
  h_ImpactParameter_z0->Draw();
#endif

#ifdef _PixelHits
  TCanvas* PixelHitsAll_1prong = new TCanvas("PixelHitsAll_1prong","PixelHitsAll_1prong");
  //gPad->SetLogy();
  // Scale
  //h_numberOfRawPixelHits[iProng][iRegion]->Scale(1/h_numberOfRawPixelHits[iProng][iRegion]->Integral());
  h_InDet_track_numberOfPixelHits[0]->Scale(1/h_InDet_track_numberOfPixelHits[0]->Integral());
  h_InDet_track_numberOfPixelHits[0]->Draw();
  h_InDet_track_numberOfPixelHits[1]->Scale(1/h_InDet_track_numberOfPixelHits[1]->Integral());
  h_InDet_track_numberOfPixelHits[1]->SetLineColor(kMagenta);
  h_InDet_track_numberOfPixelHits[1]->Draw("same");
  
  TCanvas* PixelHitsAll_3prong = new TCanvas("PixelHitsAll_3prong","PixelHitsAll_3prong");
  //gPad->SetLogy();
  h_InDet_track_numberOfPixelHits[2]->Scale(1/h_InDet_track_numberOfPixelHits[2]->Integral());
  h_InDet_track_numberOfPixelHits[2]->Draw();
  h_InDet_track_numberOfPixelHits[3]->Scale(1/h_InDet_track_numberOfPixelHits[3]->Integral());
  h_InDet_track_numberOfPixelHits[3]->SetLineColor(kMagenta);
  h_InDet_track_numberOfPixelHits[3]->Draw("same");
  
  TCanvas* PixelHits = new TCanvas("PixelHits","PixelHits", 1200, 300);
  PixelHits->Divide(4,1);
  PixelHits->cd(1);
  gPad->SetLogy();
  h_PixelHits_IBL[0]    -> Draw();
  h_PixelHits_IBL[1]    -> SetLineColor(kMagenta);
  h_PixelHits_IBL[1]    -> Draw("same");
  
  PixelHits->cd(2);
  gPad->SetLogy();
  h_PixelHits_BLayer[0] -> Draw();
  h_PixelHits_BLayer[1] -> SetLineColor(kMagenta);
  h_PixelHits_BLayer[1] -> Draw("same");
 
  PixelHits->cd(3);
  gPad->SetLogy();
  h_PixelHits_L1[0]     -> Draw();
  h_PixelHits_L1[1]     -> SetLineColor(kMagenta);
  h_PixelHits_L1[1]     -> Draw("same");
 
  PixelHits->cd(4);
  gPad->SetLogy();
  h_PixelHits_L2[0]     -> Draw();
  h_PixelHits_L2[1]     -> SetLineColor(kMagenta);
  h_PixelHits_L2[1]     -> Draw("same");
#endif

#ifdef _PixelHits_vs_DecayRadius
  TCanvas* PixelHits_vs_DecayRadius = new TCanvas("PixelHits_vs_DecayRadius", "PixelHits_vs_DecayRadius");
  gStyle->SetPalette(1);
  h_PixelHits_DecayRad->Draw("colz");

  TCanvas* PixelHitsDecayRegion = new TCanvas("PixelHitsDecayRegion", "PixelHitsDecayRegion");
  TLegend* legPixelHitsDecayRegion = new TLegend( 0.65 , 0.65, 1.0,0.9);
  legPixelHitsDecayRegion -> SetFillColor(0);
  legPixelHitsDecayRegion -> SetFillStyle(0);
  legPixelHitsDecayRegion -> SetBorderSize(0);
  
  std::string DecayRegion[5] = {"R < 33.25", "33.25 < R < 50.5", "50.5  < R < 88.5", "88.5  < R < 122.5", "122.5 < R"};
  for ( int iReg=0; iReg<5; iReg++){
    h_PixelHits_DecayRegion[iReg]->SetLineColor(iReg+1);
    if ( iReg == 4 ) h_PixelHits_DecayRegion[iReg]->SetLineColor(kMagenta);
    h_PixelHits_DecayRegion[iReg]->Scale(1/h_PixelHits_DecayRegion[iReg]->Integral());
    h_PixelHits_DecayRegion[iReg]->Draw("same");
    legPixelHitsDecayRegion -> AddEntry( h_PixelHits_DecayRegion[iReg], Form("%s", DecayRegion[iReg].c_str()), "l");
  }
  legPixelHitsDecayRegion->Draw();
  PixelHitsDecayRegion->RedrawAxis();
#endif

#ifdef _TruthPt_RecoPt
  TCanvas* TruthPt_RecoPt_1prong = new TCanvas("TruthPt_RecoPt_1prong", "TruthPt_RecoPt_1prong");
  gStyle->SetPalette(1);
  h_truthTrackRecoTrack[0]->Draw("colz");
  
  TCanvas* TruthPt_RecoPt_3prong = new TCanvas("TruthPt_RecoPt_3prong", "TruthPt_RecoPt_3prong");
  gStyle->SetPalette(1);
  h_truthTrackRecoTrack[1]->Draw("colz");
#endif

#ifdef _DeltaR
  TCanvas* DeltaR_1prong = new TCanvas("DeltaR_1prong", "DeltaR_1prong");
  h_DeltaR[0]->GetYaxis()->SetRangeUser(0,0.015);
  h_DeltaR[0]->Draw("colz");
  
  TCanvas* DeltaR_3prong = new TCanvas("DeltaR_3prong", "DeltaR_3prong");
  h_DeltaR[1]->GetYaxis()->SetRangeUser(0,0.015);
  h_DeltaR[1]->Draw("colz");


  TCanvas* DeltaR_1D = new TCanvas("DeltaR_1D","DeltaR_1D");
  h_DeltaR_1D[0]->Draw();
#endif

#ifdef _DeltaRAllTracks
  gStyle->SetPalette(1);
  TCanvas* DeltaRAllTracks = new TCanvas("DeltaRAllTracks","DeltaRAllTracks");
  h_DeltaR_AllTracks[prong(1)]->Draw("colz");
  
  TCanvas* DeltaRAllTracks3prong = new TCanvas("DeltaRAllTracks3prong","DeltaRAllTracks3prong");
  h_DeltaR_AllTracks[prong(3)]->Draw("colz");

  TCanvas* DeltaR_PtSlice = new TCanvas("DeltaR_PtSlice", "DeltaR_PtSlice", 1400,500);
  DeltaR_PtSlice->Divide(10,2);
  for ( int iProng = 0; iProng<2; iProng++){
    for ( int iPtRange = 0; iPtRange<100; iPtRange++){
      DeltaR_PtSlice->cd( 10*iProng + iPtRange+1 );
      gPad->SetLogy();
      h_DeltaR_PtSlice10GeV[iProng][iPtRange]->SetLabelSize(0.03);
      h_DeltaR_PtSlice10GeV[iProng][iPtRange]->SetFillColor(kYellow);
      h_DeltaR_PtSlice10GeV[iProng][iPtRange]->Draw();
    }
  }
  TCanvas* c[25];
  for ( int iCanvas=0; iCanvas<25; iCanvas++) {
    c[iCanvas] = new TCanvas( Form("c%d",iCanvas ), Form("c%d", iCanvas) );
    c[iCanvas]->Divide(2,2);
    std::stringstream name;
    switch( iCanvas ){
      case 0:
        name << "./DeltaR_PtSlice.pdf(";
        break;
      case 24:
        name << "./DeltaR_PtSlice.pdf)";
        break;
      default : 
        name << "./DeltaR_PtSlice.pdf";
        break;
    }
    for ( int j=0; j<4; j++){
      c[iCanvas]->cd(j+1);
      std::stringstream pt;
      pt << (iCanvas*4 + j)*10 << " < pT < " << (iCanvas*4 + j+1)*10 ;
      TText* t = new TText(0.6,0.85, pt.str().c_str() );
      t->SetNDC(1);
      t->SetTextAlign(22);
      t->SetTextFont(42);
      t->SetTextSize(0.05);
      t->SetTextAngle(0);
      h_DeltaR_PtSlice10GeV[prong(1)][iCanvas*4 + j]->Scale(1/h_DeltaR_PtSlice10GeV[prong(1)][iCanvas*4 + j]->Integral());
      h_DeltaR_PtSlice10GeV[prong(1)][iCanvas*4 + j]->Draw();
      t->Draw("same");
    }
    c[iCanvas]->Print( name.str().c_str() );
  }
#endif

#ifdef _DeltaRAllTracks_1D
  TCanvas* DeltaRAllTracks_1D = new TCanvas("DeltaRAllTracks_1D","DeltaRAllTracks_1D");
  h_DeltaR_AllTracks_1D[0]->Draw();
  
  TCanvas* DeltaRAllTracks_1D_3prong = new TCanvas("DeltaRAllTracks_1D_3prong","DeltaRAllTracks_1D_3prong");
  h_DeltaR_AllTracks_1D_3prong[0]->Draw();
#endif

#ifdef _MatchingRequirement
  gStyle->SetPalette(1);
  TCanvas* MatchingRequirement = new TCanvas("MatchingRequirement","MatchingRequirement");
  h_qOverPtVsDeltaR[0] -> Draw("colz");

  TCanvas* NumberOfTracksVsTruthPt = new TCanvas("NumberOfTracksVsTruthPt", "NumberOfTracksVsTruthPt");
  h_TruthPtVsNumTracks[prong(1)]->Draw("colz");

#endif

#ifdef _TruthPt_MatchedTruthPt
  TCanvas* TruthPt_MatchedTruthPt = new TCanvas("TruthPt_MatchedTruthPt","TruthPt_MatchedTruthPt");
  h_TruthTrackPt->SetLineColor(kRed);
  h_TruthTrackPt->Draw("same"); 
  h_TruthTrackPt_MatchedTruthPt[0][1]->Draw("same");
  
  TCanvas* TruthPt_MatchedTruthPt_Eff = new TCanvas("TruthPt_MatchedTruthPt_Eff","TruthPt_MatchedTruthPt_Eff");
  //h_TruthTrackPt->Sumw2();
  //h_TruthTrackPt_MatchedTruthPt[0][1] -> Sumw2();
  //h_TruthTrackPt_MatchedTruthPt[0][1] -> Divide( h_TruthTrackPt_MatchedTruthPt[0][1], h_TruthTrackPt, 1,1, "B");
  //h_TruthTrackPt_MatchedTruthPt[0][1] ->SetMaximum(1.02);
  //h_TruthTrackPt_MatchedTruthPt[0][1] ->SetMinimum(0);
  //h_TruthTrackPt_MatchedTruthPt[0][1] ->SetMarkerSize(0.8);
  //h_TruthTrackPt_MatchedTruthPt[0][1] ->SetMarkerColor(kBlue);
  //h_TruthTrackPt_MatchedTruthPt[0][1] ->SetLineColor(kBlue);
  //h_TruthTrackPt_MatchedTruthPt[0][1] ->Draw("e");
  TEfficiency* pEff = 0;
  h_TruthTrackPt_MatchedTruthPt[0][1] ->SetMaximum(1.02);
  h_TruthTrackPt_MatchedTruthPt[0][1] ->SetMinimum(0);
  pEff = new TEfficiency( *h_TruthTrackPt_MatchedTruthPt[0][1], *h_TruthTrackPt );
  pEff ->SetLineColor(kBlue);
  pEff ->SetMarkerColor(kBlue);
  pEff ->SetMarkerSize(0.8);
//  pEff ->GetYaxis()->SetMaximum(1.02);
//  pEff ->GetYaxis()->SetMinimum(0);
  pEff ->Draw();
  TF1 *f1=new TF1("f1","1",0,1000);
  f1->SetLineColor(kBlack);
  f1->SetLineStyle(2);
  f1->SetLineWidth(1);
  f1->Draw("same");
  
  TCanvas* TruthPt_MatchedTruthPt_3prong = new TCanvas("TruthPt_MatchedTruthPt_3prong","TruthPt_MatchedTruthPt_3prong");
  h_TruthPt3prong->SetLineColor(kRed);
  h_TruthPt3prong->Draw("same"); 
  h_TruthTrackPt_MatchedTruthPt[1][0]->Draw("same");
  
  //TCanvas* TruthPt_MatchedTruthPt_3prong_Eff = new TCanvas("TruthPt_MatchedTruthPt_3prong_Eff","TruthPt_MatchedTruthPt_3prong_Eff");
  //h_TruthPt3prong->Sumw2();
  //h_TruthTrackPt_MatchedTruthPt[1][0] -> Sumw2();
  //h_TruthTrackPt_MatchedTruthPt[1][0] -> Divide( h_TruthTrackPt_MatchedTruthPt[1][0], h_TruthPt3prong, 1,1, "B");
  //h_TruthTrackPt_MatchedTruthPt[1][0] ->SetMaximum(1.02);
  //h_TruthTrackPt_MatchedTruthPt[1][0] ->SetMinimum(0);
  //h_TruthTrackPt_MatchedTruthPt[1][0] ->SetMarkerSize(0.8);
  //h_TruthTrackPt_MatchedTruthPt[1][0] ->SetMarkerColor(kBlue);
  //h_TruthTrackPt_MatchedTruthPt[1][0] ->SetLineColor(kBlue);
  //h_TruthTrackPt_MatchedTruthPt[1][0] ->Draw("e");
#endif

#ifdef _PixelClusters 
  TCanvas* PixelClusters = new TCanvas("PixelClusters","PixelClusters");
  h_numberOfPixelClusters[prong(1)]->Scale(1/h_numberOfPixelClusters[prong(1)]->Integral());
  h_numberOfPixelClusters[prong(3)]->Scale(1/h_numberOfPixelClusters[prong(3)]->Integral());
  h_numberOfPixelClusters[prong(1)]->Draw();
  h_numberOfPixelClusters[prong(3)]->SetLineColor(kBlue);
  h_numberOfPixelClusters[prong(3)]->Draw("same");
  
  TCanvas* PixelClustersMuon = new TCanvas("PixelClustersMuon","PixelClustersMuon");
  h_numberOfPixelClusters[prong(1)]->Scale(1/h_numberOfPixelClusters[prong(1)]->Integral());
  h_numberOfPixelClusters[prong(1)]->Draw();

  TCanvas* PixelClustersEta = new TCanvas("PixelClustersEta","PixelClustersEta");
  h_PixelCluIBLEta[prong(1)] -> Draw();
  
  TCanvas* PixelClustersDeltaR_IBL = new TCanvas("PixelClustersDeltaR_IBL","PixelClustersDeltaR_IBL");
  h_PixelCluIBLDeltaR[prong(1)] -> Draw();
  
  TCanvas* PixelClustersDeltaR_BLayer = new TCanvas("PixelClustersDeltaR_BLayer","PixelClustersDeltaR_BLayer");
  h_PixelCluBLayerDeltaR[prong(1)] -> Draw();
  
  TCanvas* PixelClustersDeltaR_L1 = new TCanvas("PixelClustersDeltaR_L1","PixelClustersDeltaR_L1");
  h_PixelCluL1DeltaR[prong(1)] -> Draw();
  
  TCanvas* PixelClustersDeltaR_L2 = new TCanvas("PixelClustersDeltaR_L2","PixelClustersDeltaR_L2");
  h_PixelCluL2DeltaR[prong(1)] -> Draw();
#endif

#ifdef _PixelNumberOfHits
  TCanvas* PixelNumberOfHits = new TCanvas("PixelNumberOfHits","PixelNumberOfHits");
  h_RecoTracksNumberOfPixelHits[prong(1)][0] ->Scale(1/h_RecoTracksNumberOfPixelHits[prong(1)][0]->Integral());
  h_RecoTracksNumberOfPixelHits[prong(1)][1] ->Scale(1/h_RecoTracksNumberOfPixelHits[prong(1)][1]->Integral());
  h_RecoTracksNumberOfPixelHits[prong(1)][0] ->Draw();
  h_RecoTracksNumberOfPixelHits[prong(1)][1] ->SetLineColor(kBlue);
  h_RecoTracksNumberOfPixelHits[prong(1)][1] ->Draw("same");
#endif

  TCanvas* test = new TCanvas("test","test");
//  gStyle->SetPalette(1);
////  h_NeutrinoPt_DeltaR->Draw("colz");
////  h_Neutrino_DeltaR->Draw();
////  h_Neutrino_TruthTrack_Pt->Draw("colz");
////  h_TrackPt_NeutrinoTrack_OpeningAngle->Draw("colz");
////    h_TrackPt_TauTrack_OpeningAngle->Draw("colz");
////  h_NeutrinoPt_NumPixelHits->Draw("colz");
//
////#ifdef ThisIsTheCategoryByCategoryAnalysis
//  int Color[10] = {kBlack, kRed, kBlue, kGreen, kMagenta, kYellow, kOrange, kCyan, kGray, kViolet};
//  TLegend* leg = new TLegend(0.6,0.65,0.85,0.9);
////  for ( int iPtRange=0; iPtRange<5; iPtRange++){
//  for ( int iPtRange=5; iPtRange<10; iPtRange++){
////    h_TrackPt_NumTracks[iPtRange]->Scale(1/h_TrackPt_NumTracks[iPtRange]->Integral());
////    h_TrackPt_NumTracks[iPtRange]->SetLineColor(Color[iPtRange]);
////    h_TrackPt_NumTracks[iPtRange]->Draw("same");
//    h_Spec_NumberOfPixelHits[iPtRange]->Scale(1/h_Spec_NumberOfPixelHits[iPtRange]->Integral());
//    h_Spec_NumberOfPixelHits[iPtRange]->SetLineColor(Color[iPtRange]);
//    h_Spec_NumberOfPixelHits[iPtRange]->Draw("same");
//    leg -> SetFillColor(0);
//    leg -> SetFillStyle(0);
//    leg -> SetBorderSize(0);
//    leg -> AddEntry(h_Spec_NumberOfPixelHits[iPtRange], Form( "%d < pT < %d", iPtRange*100, (iPtRange+1)*100), "l");
//  }
//  leg->Draw("same");
//#endif
//  test->Divide(2,2);
//  test->cd(1);
//  h_Spec_RecoTauPt->Draw();
//  test->cd(2);
//  h_Spec_RecoTauEta->Draw();
//  test->cd(3);
//  h_Spec_RecoTauPhi->Draw();
//  test->cd(4);
//  h_Spec_InDetTrack_NumTracks->Draw();

#ifdef PixelClusters
  TCanvas* PixelClusters = new TCanvas("PixelClusters","PixelClusters", 1000,2000);
  PixelClusters->Divide(2,4);
  for ( int iLayer=0; iLayer<4; iLayer++){
    PixelClusters->cd(2*iLayer+1);
    gPad->SetLogy();
    h_TauPixelDeltaR[iLayer][1] = (TH1D*)h_TauPixelDeltaR[iLayer][1]->Clone();
    h_TauPixelDeltaR[iLayer][0] = (TH1D*)h_TauPixelDeltaR[iLayer][0]->Clone();
    h_TauPixelDeltaR[iLayer][1] = (TH1D*)h_TauPixelDeltaR[iLayer][1]->Clone();

    h_TauPixelDeltaR[iLayer][1]->SetLineColor(kMagenta);
    h_TauPixelDeltaR[iLayer][0]->Draw();
    h_TauPixelDeltaR[iLayer][1]->Draw("same");
    
    PixelClusters->cd(2*iLayer+2);
    gPad->SetLogy();
    h_TauPixelDeltaR[iLayer][1] = (TH1D*)h_TauPixelDeltaR[iLayer][1]->Clone();
    h_TauPixelDeltaR[iLayer][0] = (TH1D*)h_TauPixelDeltaR[iLayer][0]->Clone();
    h_TauPixelDeltaR[iLayer][1] = (TH1D*)h_TauPixelDeltaR[iLayer][1]->Clone();
    
    h_TauPixelDeltaR[iLayer][0]->Scale(1/h_TauPixelDeltaR[iLayer][0]->Integral());
    h_TauPixelDeltaR[iLayer][1]->Scale(1/h_TauPixelDeltaR[iLayer][1]->Integral());
    h_TauPixelDeltaR[iLayer][1]->SetLineColor(kMagenta);
    h_TauPixelDeltaR[iLayer][0]->Draw();
    h_TauPixelDeltaR[iLayer][1]->Draw("same");
  }
  
  TCanvas* PixelClustersMinimum = new TCanvas("PixelClustersMinimum","PixelClustersMinimum", 1000,2000);
  PixelClustersMinimum->Divide(2,4);
  for ( int iLayer=0; iLayer<4; iLayer++){
    PixelClustersMinimum->cd(2*iLayer+1);
    gPad->SetLogy();
    h_TauPixelMinimumDeltaR[iLayer][1] = (TH1D*)h_TauPixelMinimumDeltaR[iLayer][1]->Clone();
    h_TauPixelMinimumDeltaR[iLayer][0] = (TH1D*)h_TauPixelMinimumDeltaR[iLayer][0]->Clone();
    h_TauPixelMinimumDeltaR[iLayer][1] = (TH1D*)h_TauPixelMinimumDeltaR[iLayer][1]->Clone();

    h_TauPixelMinimumDeltaR[iLayer][1]->SetLineColor(kMagenta);
    h_TauPixelMinimumDeltaR[iLayer][0]->Draw();
    h_TauPixelMinimumDeltaR[iLayer][1]->Draw("same");
    
    PixelClustersMinimum->cd(2*iLayer+2);
    gPad->SetLogy();
    h_TauPixelMinimumDeltaR[iLayer][1] = (TH1D*)h_TauPixelMinimumDeltaR[iLayer][1]->Clone();
    h_TauPixelMinimumDeltaR[iLayer][0] = (TH1D*)h_TauPixelMinimumDeltaR[iLayer][0]->Clone();
    h_TauPixelMinimumDeltaR[iLayer][1] = (TH1D*)h_TauPixelMinimumDeltaR[iLayer][1]->Clone();
    
    h_TauPixelMinimumDeltaR[iLayer][0]->Scale(1/h_TauPixelMinimumDeltaR[iLayer][0]->Integral());
    h_TauPixelMinimumDeltaR[iLayer][1]->Scale(1/h_TauPixelMinimumDeltaR[iLayer][1]->Integral());
    h_TauPixelMinimumDeltaR[iLayer][1]->SetLineColor(kMagenta);
    h_TauPixelMinimumDeltaR[iLayer][0]->Draw();
    h_TauPixelMinimumDeltaR[iLayer][1]->Draw("same");
  }

  TCanvas* c1 = new TCanvas("c1","c1");
  gPad->SetLogy();
  for ( int iLayer=0; iLayer<4; iLayer++){
    h_PixelClusterEta[iLayer]->SetLineColor(iLayer+1);
    h_PixelClusterEta[iLayer]->Draw("same");
  }
  ////////////////////////////////////////////
  // 
  TCanvas* c2 = new TCanvas("c2","c2");
  for ( int iX=1; iX<=6; iX++){
    h_PixelHittedLayer[0]->GetXaxis()->SetBinLabel(iX,Form("%d", iX-1));
    h_PixelHittedLayer[1]->GetXaxis()->SetBinLabel(iX,Form("%d", iX-1));
  }
  //h_PixelHittedLayer[0]->Scale(1/h_PixelHittedLayer[0]->Integral());
  //h_PixelHittedLayer[1]->Scale(1/h_PixelHittedLayer[1]->Integral());
  h_PixelHittedLayer[1]->SetLineColor(kMagenta);
  h_PixelHittedLayer[0]->Draw();
  h_PixelHittedLayer[1]->Draw("same");
 
  ////////////////////////////////////////////
  // 
  TCanvas* c3 = new TCanvas("c3","c3", 1600,800);
  c3->Divide(2,1);
  c3->cd(1);
  for ( int iLayer=0; iLayer<4; iLayer++){
    h_PixelClustersNum[iLayer][0]->SetLineColor(iLayer+1);
    h_PixelClustersNum[IBL()][0]->SetFillColor(kBlack);
    h_PixelClustersNum[IBL()][0]->SetFillStyle(3345);
    h_PixelClustersNum[iLayer][0]->Draw("same");
  }
  c3->cd(2);
  for ( int iLayer=0; iLayer<4; iLayer++){
    h_PixelClustersNum[iLayer][1]->SetLineColor(iLayer+1);
    h_PixelClustersNum[IBL()][1]->SetFillColor(kBlack);
    h_PixelClustersNum[IBL()][1]->SetFillStyle(3345);
    h_PixelClustersNum[iLayer][1]->Draw("same");
  }
  c3->RedrawAxis();
  
  ////////////////////////////////////////////
  // PixelClustersCharge
  TCanvas* PixelClustersCharge = new TCanvas("PixelClustersCharge","PixelClustersCharge", 1700,800);
  gPad->SetLogy();
  PixelClustersCharge->Divide(2,1);
  PixelClustersCharge->cd(1);
  for ( int iLayer=0; iLayer<4; iLayer++){
    h_PixelClustersCharge[iLayer][0]->SetLineColor(iLayer+1);
    h_PixelClustersCharge[iLayer][0]->Scale(1/h_PixelClustersCharge[iLayer][0]->Integral());
    h_PixelClustersCharge[IBL()][0]->SetFillColor(kBlack);
    h_PixelClustersCharge[IBL()][0]->SetFillStyle(3345);
    h_PixelClustersCharge[iLayer][0]->Draw("same");
  }
  PixelClustersCharge->cd(2);
  for ( int iLayer=0; iLayer<4; iLayer++){
    h_PixelClustersCharge[iLayer][1]->SetLineColor(iLayer+1);
    h_PixelClustersCharge[iLayer][1]->Scale(1/h_PixelClustersCharge[iLayer][1]->Integral());
    h_PixelClustersCharge[IBL()][1]->SetFillColor(kBlack);
    h_PixelClustersCharge[IBL()][1]->SetFillStyle(3345);
    h_PixelClustersCharge[iLayer][1]->Draw("same");
  }
  PixelClustersCharge->RedrawAxis();
  
  ////////////////////////////////////////////
  // DeltaR Truth track vs Truth tau
  TCanvas* DeltaR_TruthTauTruthTrack = new TCanvas("DeltaR_TruthTauTruthTrack", "DeltaR_TruthTauTruthTrack");
  h_DeltaR_TruthTauTruthTrack->Draw();

#endif 

 // TFile *fout = new TFile("EventDisplay.root", "recreate");
  /* 
   * Event display 
   *  X - Y plane */
  // IBL
  TEllipse *ellipse[4];
  TText *zPos[4];
  double PixelDiameter[4] = {33.25, 50.5, 88.5, 122.5};
  for ( int iEvent=0; iEvent< EventDisplayTargetEvent ; iEvent++){
    TCanvas* canvas = new TCanvas( Form("canvas%d",iEvent), Form("canvas%d", iEvent));
    // Pixel cluster
    h_EventDisplay_PixelCluster[iEvent]->GetXaxis()->SetLimits(-200,200);
    h_EventDisplay_PixelCluster[iEvent]->GetHistogram()->SetMinimum(-200);
    h_EventDisplay_PixelCluster[iEvent]->GetHistogram()->SetMaximum(200);
    h_EventDisplay_PixelCluster[iEvent]->SetMarkerSize(0.5);

    // Decay vertex
    h_EventDisplay_DecayVtx    [iEvent]->SetMarkerStyle(34);
    h_EventDisplay_DecayVtx    [iEvent]->SetMarkerSize(0.5);
    h_EventDisplay_DecayVtx    [iEvent]->SetMarkerColor(kRed);

    h_EventDisplay_PixelCluster[iEvent]->Draw("AP");
    h_EventDisplay_DecayVtx    [iEvent]->Draw("P");
    // Draw known clusters
    h_EventDisplay_PixelCluster_Classified[iEvent]->Draw("P");
    
    for ( int iLayer=0; iLayer<4; iLayer++){
      ellipse[iLayer] = new TEllipse(0,0, PixelDiameter[iLayer]);
      ellipse[iLayer] ->SetFillStyle(0);
      ellipse[iLayer] ->SetLineColor(iLayer+1);
      ellipse[iLayer] ->SetLineWidth(1);
      ellipse[iLayer] ->Draw("same");

      // turht tau
      TLine *Tauline = new TLine( 
          XTauPositionLogger[iEvent][0],
          YTauPositionLogger[iEvent][0],
          XTauPositionLogger[iEvent][1],
          YTauPositionLogger[iEvent][1]
          );
      Tauline->SetLineStyle(3);
      Tauline->SetLineWidth(1);
      Tauline->Draw("same");

      // Tau hit Z position
      //zPos[iLayer] = new TText( 
      //    XPositionLogger[iEvent][iLayer],
      //    YPositionLogger[iEvent][iLayer]+5, 
      //    Form("z = %f", ZPositionLogger[iEvent][iLayer]));
      //zPos[iLayer]->SetTextSize(0.015);
      //zPos[iLayer]->Draw("same");

      // Neutrino
      TLine *Neuline = new TLine( 
          XNeutrinoPositionLogger[iEvent][0],
          YNeutrinoPositionLogger[iEvent][0],
          XNeutrinoPositionLogger[iEvent][1],
          YNeutrinoPositionLogger[iEvent][1]
          );
      Neuline->SetLineStyle(2);
      Neuline->SetLineWidth(1);
      Neuline->SetLineColor(kCyan);
      Neuline->Draw("same");

      // Charged track ( = pi+ )
      TLine *Trackline = new TLine( 
          XTrackPositionLogger[iEvent][0],
          YTrackPositionLogger[iEvent][0],
          XTrackPositionLogger[iEvent][1],
          YTrackPositionLogger[iEvent][1]
          );
      Trackline->SetLineColor(kMagenta);
      Trackline->SetLineWidth(1);
      Trackline->Draw("same");

      // Draw all the tracks
      //for ( unsigned int iInDet=0; iInDet<XInDetTrackPositionLogger[iEvent].size(); iInDet++){
      //  TLine *InDetTrackline = new TLine( 
      //      -0.5,
      //      -0.5,
      //      XInDetTrackPositionLogger[iEvent][iInDet],
      //      YInDetTrackPositionLogger[iEvent][iInDet]
      //      );
      //  InDetTrackline->SetLineColor(iInDet+1);
      //  InDetTrackline->Draw("same");
      //  TText* numHits = new TText(50,50+iInDet*10, Form("# hits = %d (%d,%d,%d,%d)", 
      //        NumberOfPixelHitsLogger[iEvent].at(iInDet),
      //        NumberOf_IBL_PixelHitsLogger[iEvent].at(iInDet),
      //        NumberOf_BLayer_PixelHitsLogger[iEvent].at(iInDet),
      //        NumberOf_L1_PixelHitsLogger[iEvent].at(iInDet),
      //        NumberOf_L2_PixelHitsLogger[iEvent].at(iInDet))
      //      );
      //  numHits->SetTextSize(0.015);
      //  numHits->Draw("same");
      //}
    }
    TText* infoPt     = new TText( -80, -160, Form("Truth track pT = %.2f [GeV]", TrackPtLogger[iEvent]));
    TLatex* infoTheta = new TLatex(-80, -180,  Form("#Delta #theta = %f", TrackTauThetaLoogger[iEvent]));
    infoPt->SetTextAlign(22);
    infoPt->SetTextColor(kBlack);
    infoPt->SetTextFont(43);
    infoPt->SetTextSize(15);
    infoPt->Draw();
    infoTheta->SetTextAlign(22);
    infoTheta->SetTextColor(kBlack);
    infoTheta->SetTextFont(43);
    infoTheta->SetTextSize(15);
    infoTheta->Draw();
    

    for ( int i=0; i< h_TTextTrackInfo[iEvent].size(); i++){
      h_TTextTrackInfo   [iEvent].at(i)->Draw("same");
    }
    for ( int i=0; i< h_TTextClusterPdgId[iEvent].size(); i++){
      h_TTextClusterPdgId   [iEvent].at(i)->Draw("same");
    }

    if ( iEvent == 0  )                             canvas->SaveAs("EventDisplay.pdf[");
    else if ( iEvent == EventDisplayTargetEvent-1 ) canvas->SaveAs("EventDisplay.pdf]");
    else                                            canvas->SaveAs("EventDisplay.pdf");
  }
  
  /* 
   * Event display 
   *  Z - R plane */
  // IBL
  TLine *PixelLine[4][2];
  for ( int iEvent=0; iEvent< EventDisplayTargetEvent ; iEvent++){
  TCanvas* canvas_ZR = new TCanvas( Form("canvas_ZR%d",iEvent), Form("canvas_ZR%d", iEvent));
  // Pixel cluster
  h_EventDisplay_PixelCluster_RZ[iEvent]->GetXaxis()->SetLimits(-400,400);
  h_EventDisplay_PixelCluster_RZ[iEvent]->GetHistogram()->SetMinimum(-300);
  h_EventDisplay_PixelCluster_RZ[iEvent]->GetHistogram()->SetMaximum(300);
  h_EventDisplay_PixelCluster_RZ[iEvent]->SetMarkerSize(0.5);
  
  // Decay vertex
  h_EventDisplay_DecayVtx_RZ    [iEvent]->SetMarkerStyle(34);
  h_EventDisplay_DecayVtx_RZ    [iEvent]->SetMarkerSize(0.5);
  h_EventDisplay_DecayVtx_RZ    [iEvent]->SetMarkerColor(kRed);
  
  h_EventDisplay_PixelCluster_RZ[iEvent]->Draw("AP");
  h_EventDisplay_DecayVtx_RZ    [iEvent]->Draw("P");
  for ( int iLayer=0; iLayer<4; iLayer++){
    PixelLine[iLayer][0] = new TLine(-400, PixelDiameter[iLayer], 400, PixelDiameter[iLayer]);
    PixelLine[iLayer][0]->SetLineColor(iLayer+1);
    PixelLine[iLayer][0]->Draw();
    
    PixelLine[iLayer][1] = new TLine(-400, -1*PixelDiameter[iLayer], 400, -1*PixelDiameter[iLayer]);
    PixelLine[iLayer][1]->SetLineColor(iLayer+1);
    PixelLine[iLayer][1]->Draw();

    TLine *Tauline;
    if (YTauPositionLogger[iEvent][1]>0 ){
      Tauline = new TLine( 
        ZTauPositionLogger[iEvent][0],
        RTauPositionLogger[iEvent][0],
        ZTauPositionLogger[iEvent][1],
        RTauPositionLogger[iEvent][1]
        );
    } else {
      Tauline = new TLine( 
        ZTauPositionLogger[iEvent][0],
        -1*RTauPositionLogger[iEvent][0],
        ZTauPositionLogger[iEvent][1],
        -1*RTauPositionLogger[iEvent][1]
        );
    }
    Tauline->SetLineStyle(2);
    Tauline->SetLineWidth(1);
    Tauline->Draw("same");
    
    TLine *Trackline;
    if (YTauPositionLogger[iEvent][1]>0 ){
    Trackline = new TLine( 
        ZTrackPositionLogger[iEvent][0],
        RTrackPositionLogger[iEvent][0],
        ZTrackPositionLogger[iEvent][1],
        RTrackPositionLogger[iEvent][1]
        );
    }else {
      Trackline = new TLine( 
          ZTrackPositionLogger[iEvent][0],
          -1*RTrackPositionLogger[iEvent][0],
          ZTrackPositionLogger[iEvent][1],
          -1*RTrackPositionLogger[iEvent][1]
          );
    }
    Trackline->SetLineColor(kMagenta);
    Trackline->Draw("same");

    TText* infoPt    = new TText( 200, 250, Form("Truth track p_{T} = %.2f [GeV]", TrackPtLogger[iEvent]));
    TText* infoTheta = new TText( 200, 200, Form("#Delta #theta = %f", TrackTauThetaLoogger[iEvent]));
    infoPt->SetTextSize(0.02);
    infoTheta->SetTextSize(0.02);
    infoPt->Draw();
    infoTheta->Draw();
  }
  //canvas->Update();
  if ( iEvent == 0  )                             canvas_ZR->SaveAs("EventDisplay_ZR.pdf[");
  else if ( iEvent == EventDisplayTargetEvent-1 ) canvas_ZR->SaveAs("EventDisplay_ZR.pdf]");
  else                                            canvas_ZR->SaveAs("EventDisplay_ZR.pdf");
//  canvas->Write();
  }

  /********************************************************/
  /*         The number of events in each region          */
  /********************************************************/
  std::cout << "All                        " << eventCounter.all << std::endl;
  std::cout << "Hadronic                   " << eventCounter.hadronic << std::endl;
  std::cout << "1-prong                    " << eventCounter.prong1 << std::endl;
  std::cout << "|eta|<1.0                  " << eventCounter.eta_1_0 << std::endl;
  std::cout << " 0     < R_{decay} < 33.25 " << eventCounter.decayRadLower33_25 << std::endl;
  std::cout << " 33.25 < R_{decay} < 50.5  " << eventCounter.decayRadLower50_5 << std::endl;
  std::cout << " 50.5  < R_{decay} < 88.5  " << eventCounter.decayRadLower88_5 << std::endl;
  std::cout << " 88.5  < R_{decay} < 122.5 " << eventCounter.decayRadLower122_5 << std::endl;
  std::cout << "R_{decay} > 122.5          " << eventCounter.decayRadLarger122_5<< std::endl;
  std::cout << "3-prong                    " << eventCounter.prong3 << std::endl;


std::cout << TargetEventCounter << std::endl;
  return;
}
