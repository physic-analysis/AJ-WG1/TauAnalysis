/* Momentum distribution */
//#define _Momentum_All
//#define _Momentum_Leptonic
//#define _Momentum_Hadronic
//#define _Momentum_HaronicReco
//#define _Momentum_HaronicReco1Prong
//#define _Momentum_HaronicReco3Prong
//#define _Momentum_HadronicContents
//#define _Momentum_truth
//#define _Momentum_reco
//#define _Momentum_reco_ElectronMuonRemoval
//#define _Momentum_truth_neutritno 

/* Decay radius */
//#define _DecayRadius

/* Number of xxx */
//#define _numberOfXXX
//#define _numberOfPixelHits_DecayRadius
//#define _numberOfPixelHits_Eta
//#define _DecayRadius_vs_NumberOfXXX
//#define _DecayRadius_vs_RawNumberOfXXX
//#define _DecayRadius_vs_RawNumberOfXXX_Normalize
//#define _DecayRadius_vs_NumberOfXXX_Pixel
//#define _DecayRadius_vs_NumberOfXXX_1prong

/* Efficiency */
//#define _Efficiency_DecayRadius
//#define _Efficiency_pT
//#define _Efficiency_D0
//#define _Efficiency_Z0
//#define _TrackEfficiency

/* Basic kinematics */
//#define _Basic_Pix_clusters
//#define _Basic_kinematics

/* Track information */
//#define _numTrack_vs_DecayRadius
//#define _chargedTrack_distribution
//#define _Track_distribution
//#define _numberOfTracks
//#define _numberOfTracks_optimized
//#define _InDetTrack
//#define _TrackExact

/* Impact parameter */
//#define _ImpactParameter

/* Pixel hits 
 *  InDet_track_pixHits_XXX variables are used. */
//#define _PixelHits
//#define _PixelHits_vs_DecayRadius

/* Truth track vs Reco track */
//#define _TruthPt_RecoPt

/* Delta R */
//#define _DeltaR
//#define _DeltaRAllTracks
//#define _DeltaRAllTracks_1D

/* Matching requirements */
//#define _MatchingRequirement
//#define _TruthPt_MatchedTruthPt

/* Pixel clusters */
//#define _PixelClusters
//#define _PixelNumberOfHits
