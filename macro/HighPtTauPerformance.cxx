
#include <iostream>
#include <TH1.h>
#include <TH2.h>
#include <TFile.h>

#include <TTree.h>
#include <TStyle.h>
#include <TText.h>
#include <TCanvas.h>
#include <TLegend.h>
#include <TLine.h>

#include "tools/text.h"
#include "tools/PixelLayer.h"
#include "ModeSelector.h"

struct EventCounter
{
  EventCounter(){
    all=0;
    hadronic=0;
    eta_1_0=0;
    prong1=0;
    prong3=0;
    decayRadLower33_25=0;
    decayRadLower50_5=0;
    decayRadLower88_5=0;
    decayRadLower122_5=0;
    decayRadLower122_5=0;
    decayRadLarger122_5=0;
  };
  int all;
  int hadronic;
  int eta_1_0;
  int prong1;
  int prong3;
  int decayRadLower33_25;
  int decayRadLower50_5;
  int decayRadLower88_5;
  int decayRadLower122_5;
  int decayRadLarger122_5;
};

void HighPtTauPerformance()
{
  //std::string simulator="FullG4_LongLived";
  std::string simulator="MC12G4";
  //std::string path = "/afs/cern.ch/user/k/ktakeda/workspace/public/atlas-phys/AJ/WG1/SingleParticle/AnalysisArea/run/";
  std::string path = "../Data/";
  std::string file_name = "MyTauNtuple." + simulator + "_20181107.outputs.root";

  TFile* file = new TFile( (path+file_name).c_str(), "read");
  TTree* tree = (TTree*)file->Get("m_trueTauTree");


  int Truth_part_numChargedTracks       = 0;
  int Truth_part_numChargedLeptonTracks = 0;

  //
  ///////////////////////////////////////////
  // Truth tau
  Double_t Truth_tau_pt; 
  Double_t Truth_tau_eta; 
  Double_t Truth_tau_phi; 
  Double_t Truth_tau_m; 
  Bool_t   Truth_tau_isLeptonic = false; 
  Bool_t   Truth_tau_isHadronic = false; 
  Int_t    Truth_tau_numTracks =0; 
  Bool_t   Truth_tau_isMatchedWithReco = false; 
  Double_t Truth_tau_decayRad = 0; 
  //
  ///////////////////////////////////////////
  // Truth daughter event
  std::vector<int>*          Truth_dau_pdgId=0;
  //
  Bool_t   tt_truth_leptonic_electron = false; 
  Bool_t   tt_truth_leptonic_muon = false; 
  Bool_t   tt_truth_leptonic_tau = false; 
  Double_t tt_truth_neutrino_pt; 
  //
  Int_t                      eventNumber;
  //
  std::vector<double>*       Truth_track_pt = 0;
  std::vector<double>*       Truth_track_eta = 0;
  std::vector<double>*       Truth_track_phi = 0;
  std::vector<double>*       Truth_track_m = 0;
  std::vector<double>*       Truth_track_charge = 0;
  //
  ///////////////////////////////////////////
  // Truth neutrino information 
  std::vector<int>*          Truth_neutrino_pdgId=0;
  std::vector<double>*       Truth_neutrino_pt=0;
  std::vector<double>*       Truth_neutrino_eta=0;
  std::vector<double>*       Truth_neutrino_phi=0;
  //
  Float_t                    Truth_prodVtx_x;
  Float_t                    Truth_prodVtx_y;
  Float_t                    Truth_prodVtx_z;
  Float_t                    Truth_prodVtx_eta;
  Float_t                    Truth_prodVtx_phi;
  Float_t                    Truth_prodVtx_perp;
  //
  //
  Float_t                    Truth_decVtx_x;
  Float_t                    Truth_decVtx_y;
  Float_t                    Truth_decVtx_z;
  Float_t                    Truth_decVtx_t;
  Float_t                    Truth_decVtx_eta;
  Float_t                    Truth_decVtx_phi;
  Float_t                    Truth_decVtx_perp;
  //
  Int_t                      InDet_numTracks;        //! the number of tracks passed pixel layer per events
  std::vector<double>*       InDet_track_pt          = 0;
  std::vector<double>*       InDet_track_eta         = 0;
  std::vector<double>*       InDet_track_phi         = 0;
  std::vector<double>*       InDet_track_m           = 0;
  std::vector<double>*       InDet_track_d0           = 0;
  std::vector<double>*       InDet_track_z0           = 0;
  std::vector<float>*        InDet_track_charge       = 0;
  std::vector<int>*          InDet_track_numberOfIBLHits = 0;
  std::vector<int>*          InDet_track_numberOfBLayerHits = 0;
  std::vector<int>*          InDet_track_numberOfPixelL1Hits = 0;
  std::vector<int>*          InDet_track_numberOfPixelL2Hits = 0;
  std::vector<int>*          InDet_track_numberOfPixelHits = 0;
  std::vector<int>*          InDet_track_numberOfSCTHits   = 0;
  //
  ///////////////////////////////////////////
  // Pixel raw cluster information 
  std::vector<std::vector<double>>* InDet_pixel_raw_ClustersEta    = 0;
  std::vector<std::vector<double>>* InDet_pixel_raw_ClustersPhi    = 0;
  std::vector<std::vector<double>>* InDet_pixel_raw_ClustersCharge = 0;
  std::vector<std::vector<double>>* InDet_pixel_raw_globalX        = 0;
  std::vector<std::vector<double>>* InDet_pixel_raw_globalY        = 0;
  std::vector<std::vector<double>>* InDet_pixel_raw_globalZ        = 0;
  std::vector<std::vector<double> >*    InDet_pixel_raw_IB_energyDeposit= 0;
  std::vector<std::vector<double> >*    InDet_pixel_raw_BL_energyDeposit= 0;
  std::vector<std::vector<double> >*    InDet_pixel_raw_L1_energyDeposit= 0;
  std::vector<std::vector<double> >*    InDet_pixel_raw_L2_energyDeposit= 0;
  std::vector<std::vector<double> >*    InDet_pixel_raw_IB_barcode= 0;
  std::vector<std::vector<double> >*    InDet_pixel_raw_BL_barcode= 0;
  std::vector<std::vector<double> >*    InDet_pixel_raw_L1_barcode= 0;
  std::vector<std::vector<double> >*    InDet_pixel_raw_L2_barcode= 0;
  std::vector<std::vector<double> >*    InDet_pixel_raw_IB_pdgId= 0;
  std::vector<std::vector<double> >*    InDet_pixel_raw_BL_pdgId= 0;
  std::vector<std::vector<double> >*    InDet_pixel_raw_L1_pdgId= 0;
  std::vector<std::vector<double> >*    InDet_pixel_raw_L2_pdgId= 0;
  //
  ///////////////////////////////////////////
  // Pixel cluster information
  Int_t                             InDet_pixel_numAllOfClusters;
  std::vector<std::vector<double>>* InDet_pixel_IBLClustersEta = 0;
  std::vector<std::vector<double>>* InDet_pixel_BLayerClustersEta = 0;
  std::vector<std::vector<double>>* InDet_pixel_L1ClustersEta = 0;
  std::vector<std::vector<double>>* InDet_pixel_L2ClustersEta = 0;
  //
  std::vector<std::vector<double>>* InDet_pixel_IBLClustersPhi = 0;
  std::vector<std::vector<double>>* InDet_pixel_BLayerClustersPhi = 0;
  std::vector<std::vector<double>>* InDet_pixel_L1ClustersPhi = 0;
  std::vector<std::vector<double>>* InDet_pixel_L2ClustersPhi = 0;
  //
  std::vector<std::vector<double>>* InDet_pixel_IBLClustersCharge = 0;
  std::vector<std::vector<double>>* InDet_pixel_BLayerClustersCharge = 0;
  std::vector<std::vector<double>>* InDet_pixel_L1ClustersCharge = 0;
  std::vector<std::vector<double>>* InDet_pixel_L2ClustersCharge = 0;
  ///////////////////////////////////////////
  // Pixel cluster information (not considered the tau)
  Int_t                      Pix_numCluster_IBL;   //! the number of clusters on the IBL
  Int_t                      Pix_numCluster_BLayer;//! the number of clusters on the B-Layer
  Int_t                      Pix_numCluster_L1;    //! the number of clusters on the pixel 2nd layer 
  Int_t                      Pix_numCluster_L2;    //! the number of clusters on the pixel 3rd layer
  //
  Int_t    Reco_tau_numTracks = 0; 
  Bool_t   Reco_tau_isSelected;
  Double_t Reco_tau_pt; 
  Double_t Reco_tau_eta; 
  Double_t Reco_tau_phi; 
  Double_t Reco_tau_e; 

  // Tracks from a reco-tau
  std::vector<double>*          Reco_track_pt=0;  
  std::vector<double>*          Reco_track_eta=0;  
  std::vector<double>*          Reco_track_phi=0;  
  std::vector<double>*          Reco_track_e=0;   
  std::vector<double>*          Reco_track_m=0;   
  std::vector<double>*          Reco_track_d0=0;   
  std::vector<double>*          Reco_track_z0=0;   
  std::vector<int>*             Reco_track_numberOfPixelHits=0;
  // Pixel raw cluster
  std::vector<unsigned int>* Reco_track_numberOfInnermostPixelHits       = 0 ;//!
  std::vector<unsigned int>* Reco_track_numberOfNextToInnermostPixelHits = 0 ;//!
  std::vector<unsigned int>* Reco_track_numberOfPixelL1Hits                   = 0 ;//!
  std::vector<unsigned int>* Reco_track_numberOfPixelL2Hits                   = 0 ;//!

  tree->SetBranchAddress("eventNumber",               &eventNumber);
  //
  tree->SetBranchAddress("Truth_part_numChargedTracks",                  &Truth_part_numChargedTracks);
  tree->SetBranchAddress("Truth_part_numChargedLeptonTracks",            &Truth_part_numChargedLeptonTracks);
  //
  tree->SetBranchAddress("Truth_tau_isLeptonic",           &Truth_tau_isLeptonic);
  tree->SetBranchAddress("Truth_tau_isHadronic",           &Truth_tau_isHadronic);
  tree->SetBranchAddress("Truth_tau_isMatchedWithReco",                    &Truth_tau_isMatchedWithReco);
  tree->SetBranchAddress("Truth_tau_pt",                       &Truth_tau_pt);
  tree->SetBranchAddress("Truth_tau_eta",                      &Truth_tau_eta);
  tree->SetBranchAddress("Truth_tau_phi",                      &Truth_tau_phi);
  tree->SetBranchAddress("Truth_tau_m",                      &Truth_tau_m);
  tree->SetBranchAddress("Truth_tau_decayRad",             &Truth_tau_decayRad);
  tree->SetBranchAddress("Truth_tau_numTracks",                   &Truth_tau_numTracks);
  //
  tree->SetBranchAddress("Truth_dau_pdgId",               &Truth_dau_pdgId);
  //
  tree->SetBranchAddress("Truth_neutrino_pdgId",          &Truth_neutrino_pdgId);
  tree->SetBranchAddress("Truth_neutrino_pt",             &Truth_neutrino_pt);
  tree->SetBranchAddress("Truth_neutrino_eta",            &Truth_neutrino_eta);
  tree->SetBranchAddress("Truth_neutrino_phi",            &Truth_neutrino_phi);
  //
  tree->SetBranchAddress("Truth_track_pt",                &Truth_track_pt);
  tree->SetBranchAddress("Truth_track_eta",               &Truth_track_eta);
  tree->SetBranchAddress("Truth_track_phi",               &Truth_track_phi);
  tree->SetBranchAddress("Truth_track_m",                 &Truth_track_m);
  tree->SetBranchAddress("Truth_track_charge",            &Truth_track_charge);
  //
  tree->SetBranchAddress("Truth_prodVtx_x",               &Truth_prodVtx_x);
  tree->SetBranchAddress("Truth_prodVtx_y",               &Truth_prodVtx_y);
  tree->SetBranchAddress("Truth_prodVtx_z",               &Truth_prodVtx_z);
  tree->SetBranchAddress("Truth_prodVtx_eta",             &Truth_prodVtx_eta);
  tree->SetBranchAddress("Truth_prodVtx_phi",             &Truth_prodVtx_phi);
  tree->SetBranchAddress("Truth_prodVtx_perp",            &Truth_prodVtx_perp);
  //
  tree->SetBranchAddress("Truth_decVtx_x",               &Truth_decVtx_x);
  tree->SetBranchAddress("Truth_decVtx_y",               &Truth_decVtx_y);
  tree->SetBranchAddress("Truth_decVtx_z",               &Truth_decVtx_z);
  tree->SetBranchAddress("Truth_decVtx_t",               &Truth_decVtx_t);
  tree->SetBranchAddress("Truth_decVtx_eta",             &Truth_decVtx_eta);
  tree->SetBranchAddress("Truth_decVtx_phi",             &Truth_decVtx_phi);
  tree->SetBranchAddress("Truth_decVtx_perp",            &Truth_decVtx_perp);
  //
  tree->SetBranchAddress("InDet_numTracks",               &InDet_numTracks);
  tree->SetBranchAddress("InDet_track_pt",                &InDet_track_pt);
  tree->SetBranchAddress("InDet_track_eta",               &InDet_track_eta);
  tree->SetBranchAddress("InDet_track_phi",               &InDet_track_phi);
  tree->SetBranchAddress("InDet_track_m",                 &InDet_track_m);
  tree->SetBranchAddress("InDet_track_d0",                &InDet_track_d0);
  tree->SetBranchAddress("InDet_track_z0",                &InDet_track_z0);
  tree->SetBranchAddress("InDet_track_charge",            &InDet_track_charge);
  tree->SetBranchAddress("InDet_track_numberOfIBLHits",       &InDet_track_numberOfIBLHits);
  tree->SetBranchAddress("InDet_track_numberOfBLayerHits",    &InDet_track_numberOfBLayerHits);
  tree->SetBranchAddress("InDet_track_numberOfPixelL1Hits",        &InDet_track_numberOfPixelL1Hits);
  tree->SetBranchAddress("InDet_track_numberOfPixelL2Hits",        &InDet_track_numberOfPixelL2Hits);
  tree->SetBranchAddress("InDet_track_numberOfPixelHits", &InDet_track_numberOfPixelHits);
  tree->SetBranchAddress("InDet_track_numberOfSCTHits",   &InDet_track_numberOfSCTHits);
  //
  ///////////////////////////////////////////
  // Pixel raw cluster information 
  tree->SetBranchAddress("InDet_pixel_raw_ClustersEta",        &InDet_pixel_raw_ClustersEta);
  tree->SetBranchAddress("InDet_pixel_raw_ClustersPhi",        &InDet_pixel_raw_ClustersPhi);
  tree->SetBranchAddress("InDet_pixel_raw_ClustersCharge",     &InDet_pixel_raw_ClustersCharge);
  tree->SetBranchAddress("InDet_pixel_raw_globalX",            &InDet_pixel_raw_globalX);
  tree->SetBranchAddress("InDet_pixel_raw_globalY",            &InDet_pixel_raw_globalY);
  tree->SetBranchAddress("InDet_pixel_raw_globalZ",            &InDet_pixel_raw_globalZ);
  tree->SetBranchAddress("InDet_pixel_raw_IB_energyDeposit",   &InDet_pixel_raw_IB_energyDeposit);
  tree->SetBranchAddress("InDet_pixel_raw_BL_energyDeposit",   &InDet_pixel_raw_BL_energyDeposit);
  tree->SetBranchAddress("InDet_pixel_raw_L1_energyDeposit",   &InDet_pixel_raw_L1_energyDeposit);
  tree->SetBranchAddress("InDet_pixel_raw_L2_energyDeposit",   &InDet_pixel_raw_L2_energyDeposit);
  tree->SetBranchAddress("InDet_pixel_raw_IB_barcode",         &InDet_pixel_raw_IB_barcode);
  tree->SetBranchAddress("InDet_pixel_raw_BL_barcode",         &InDet_pixel_raw_BL_barcode);
  tree->SetBranchAddress("InDet_pixel_raw_L1_barcode",         &InDet_pixel_raw_L1_barcode);
  tree->SetBranchAddress("InDet_pixel_raw_L2_barcode",         &InDet_pixel_raw_L2_barcode);
  tree->SetBranchAddress("InDet_pixel_raw_IB_pdgId",           &InDet_pixel_raw_IB_pdgId);
  tree->SetBranchAddress("InDet_pixel_raw_BL_pdgId",           &InDet_pixel_raw_BL_pdgId);
  tree->SetBranchAddress("InDet_pixel_raw_L1_pdgId",           &InDet_pixel_raw_L1_pdgId);
  tree->SetBranchAddress("InDet_pixel_raw_L2_pdgId",           &InDet_pixel_raw_L2_pdgId);
  //
  ///////////////////////////////////////////
  // Pixel cluster information 
  tree->SetBranchAddress("InDet_pixel_numAllOfClusters",  &InDet_pixel_numAllOfClusters);
  tree->SetBranchAddress("InDet_pixel_IBLClustersEta",    &InDet_pixel_IBLClustersEta);
  tree->SetBranchAddress("InDet_pixel_BLayerClustersEta", &InDet_pixel_BLayerClustersEta);
  tree->SetBranchAddress("InDet_pixel_L1ClustersEta",     &InDet_pixel_L1ClustersEta);
  tree->SetBranchAddress("InDet_pixel_L2ClustersEta",     &InDet_pixel_L2ClustersEta);
  tree->SetBranchAddress("InDet_pixel_IBLClustersPhi",    &InDet_pixel_IBLClustersPhi);
  tree->SetBranchAddress("InDet_pixel_BLayerClustersPhi", &InDet_pixel_BLayerClustersPhi);
  tree->SetBranchAddress("InDet_pixel_L1ClustersPhi",     &InDet_pixel_L1ClustersPhi);
  tree->SetBranchAddress("InDet_pixel_L2ClustersPhi",     &InDet_pixel_L2ClustersPhi);
  tree->SetBranchAddress("InDet_pixel_IBLClustersCharge",    &InDet_pixel_IBLClustersCharge);
  tree->SetBranchAddress("InDet_pixel_BLayerClustersCharge", &InDet_pixel_BLayerClustersCharge);
  tree->SetBranchAddress("InDet_pixel_L1ClustersCharge",     &InDet_pixel_L1ClustersCharge);
  tree->SetBranchAddress("InDet_pixel_L2ClustersCharge",     &InDet_pixel_L2ClustersCharge);
  //
  tree->SetBranchAddress("Reco_tau_pt",                        &Reco_tau_pt);
  tree->SetBranchAddress("Reco_tau_eta",                       &Reco_tau_eta);
  tree->SetBranchAddress("Reco_tau_phi",                       &Reco_tau_phi);
  tree->SetBranchAddress("Reco_tau_e",                         &Reco_tau_e);
  tree->SetBranchAddress("Reco_tau_numTracks",          &Reco_tau_numTracks);
  tree->SetBranchAddress("Reco_tau_isSelected",                  &Reco_tau_isSelected);
  //
  tree->SetBranchAddress("Reco_track_numberOfInnermostPixelHits",            &Reco_track_numberOfInnermostPixelHits);
  tree->SetBranchAddress("Reco_track_numberOfNextToInnermostPixelHits",      &Reco_track_numberOfNextToInnermostPixelHits);
  tree->SetBranchAddress("Reco_track_numberOfPixelL1Hits",                   &Reco_track_numberOfPixelL1Hits);
  tree->SetBranchAddress("Reco_track_numberOfPixelL2Hits",                   &Reco_track_numberOfPixelL2Hits);
  //
  ///////////////////////////////////////////
  // Reconstructed track info 
  tree->SetBranchAddress( "Reco_track_pt",                     &Reco_track_pt);
  tree->SetBranchAddress( "Reco_track_eta",                    &Reco_track_eta);
  tree->SetBranchAddress( "Reco_track_phi",                    &Reco_track_phi);
  tree->SetBranchAddress( "Reco_track_e",                      &Reco_track_e);
  tree->SetBranchAddress( "Reco_track_m",                      &Reco_track_m);
  tree->SetBranchAddress( "Reco_track_d0",                     &Reco_track_d0);
  tree->SetBranchAddress( "Reco_track_z0",                     &Reco_track_z0);
  tree->SetBranchAddress("Reco_track_numberOfPixelHits",       &Reco_track_numberOfPixelHits);
  //_//_//_//_//_//_//_//_//_//_//_//_//_//_//_//_//_//_//_//_//_//_//_//_ //_//_//_//_//_//_//_//_//_//_//_//_//_//_//_//_//_//_//_//_//_//_//_//
  //
  //                                  The decralation of histograms
  //
  //_//_//_//_//_//_//_//_//_//_//_//_//_//_//_//_//_//_//_//_//_//_//_//_ //_//_//_//_//_//_//_//_//_//_//_//_//_//_//_//_//_//_//_//_//_//_//_//

  int numProng = 2;
  int Category = 3;
  int xBinDR_Binning = 39;
  int xBinPt_Binning = 34;
  double xBinDR[40] ={
    0,5,10,15,20,25,30,35,40,45,50,55,60,65,70,75,80,85,90,95,100,105,110,115,120,
    //125,130,135,140,145,150,155,160,165,170,175,180,185,190,195,200,
    //205,210,215,220,225,230,235,240,245,250,255,260,265,270,275,280,285,290,295,300,
    //371,443,514,554};
    135,150,165,180,195,210,235,250,265,280,295,
    371,443,514,554};
//300,350,400,450,500,550,600};

double xBinPt[38]={
  0,50,100,150,200,250,300,350,400,
  425,450,475,500,525,550,575,600,
//  610,620,630,640,650,660,670,680,690,700,710,720,730,740,750,760,770,780,790,800,810,820,830,840,850,860,870,880,890,900,910,920,930,940,950,960,970,980,990,1000};
  620,640,660,680,700,720,740,760,780,800,820,840,860,880,900,920,940,960,980,1000};


double xBinDetailed_1prong[77];
for ( int iBin=0; iBin<77; iBin++){
  if ( iBin <=60 )
    xBinDetailed_1prong[iBin] = 2*iBin;
  else 
    xBinDetailed_1prong[iBin] = 120 + 5*(iBin-60);
}

double xBinDetailed_3prong[33];
for ( int iBin=0; iBin<33; iBin++){
  if ( iBin <=24 )
    xBinDetailed_3prong[iBin] = 5*iBin;
  else 
    xBinDetailed_3prong[iBin] = 120 + 10*(iBin-24);
}


TH1D* h_TruthTauDecayRadius        [numProng][Category];
TH1D* h_TruthTauDecayRadius_Detaled[numProng][6];
TH1D* h_TruthTauPt                 [numProng][Category];
TH1D* h_TruthTauDetailsDecayRad    [numProng][5];
TH1D* h_TruthTauDetailsPt          [numProng][5];
for ( int iProng=0; iProng< 2; iProng++){
  for ( int iCat=0; iCat<3; iCat++){
    h_TruthTauDecayRadius[iProng][iCat] = new TH1D( Form("h_TruthTauDecayRadius%d%d", iProng, iCat),";R_{decay} [mm];Events", xBinDR_Binning, &xBinDR[0]);
    h_TruthTauPt         [iProng][iCat] = new TH1D( Form("h_TruthTauPt%d%d", iProng, iCat),";p_{T} [GeV];Events", xBinPt_Binning, &xBinPt[0]);
  }
  for ( int iCat=0; iCat<5; iCat++){
    h_TruthTauDetailsDecayRad    [iProng][iCat] = new TH1D( Form("h_TruthTauDetailsDecayRad%d%d", iProng, iCat),";R_{decay} [mm];Events", xBinDR_Binning, &xBinDR[0]);
    h_TruthTauDetailsPt          [iProng][iCat] = new TH1D( Form("h_TruthTauDetailsPt%d%d", iProng, iCat),";p_{T} [GeV];Events",xBinPt_Binning, &xBinPt[0]);
  }
}
for ( int iDetailed=0; iDetailed<6; iDetailed++){
  h_TruthTauDecayRadius_Detaled[0][iDetailed] = new TH1D( Form("h_TruthTauDecayRadius_Detaled%d%d",0, iDetailed), "", 76, &(xBinDetailed_1prong[0]));
  h_TruthTauDecayRadius_Detaled[1][iDetailed] = new TH1D( Form("h_TruthTauDecayRadius_Detaled%d%d",1, iDetailed), "", 32, &(xBinDetailed_3prong[0]));
}

//_//_//_//_//_//_//_//_//_//_//_//_//_//_//_//_//_//_//_//_//_//_//_//_ //_//_//_//_//_//_//_//_//_//_//_//_//_//_//_//_//_//_//_//_//_//_//_//
//
//                             Main Logic 
//
//_//_//_//_//_//_//_//_//_//_//_//_//_//_//_//_//_//_//_//_//_//_//_//_ //_//_//_//_//_//_//_//_//_//_//_//_//_//_//_//_//_//_//_//_//_//_//_//
std::cout << "The total number of events : " << tree->GetEntries() << std::endl;
EventCounter eventCounter;
for(int iEntry=0; iEntry< tree->GetEntries(); iEntry++){
  tree->GetEntry(iEntry);

  /////////////////////////////////////////////
  /* Count the number of event in each region */
  eventCounter.all++;
  if ( Truth_tau_isHadronic ){
    eventCounter.hadronic++;
    if ( Truth_tau_numTracks == 1 ){
      eventCounter.prong1++;
      if ( TMath::Abs(Truth_tau_eta) < 1.0 ){
        eventCounter.eta_1_0++;
        if       ( Truth_tau_decayRad < 33.25 ) eventCounter.decayRadLower33_25++;
        else if  ( Truth_tau_decayRad < 50.5  ) eventCounter.decayRadLower50_5++;
        else if  ( Truth_tau_decayRad < 88.5  ) eventCounter.decayRadLower88_5++;
        else if  ( Truth_tau_decayRad < 122.5 ) eventCounter.decayRadLower122_5++;
        else if  ( Truth_tau_decayRad > 122.5 ) eventCounter.decayRadLarger122_5++;
      }
    }
    if (Truth_tau_numTracks == 3 ){
      eventCounter.prong3++;
    }
  }
  /*******************************************/
  /////////////////////////////////////////////

  if ( iEntry % 10000 == 0 ) std::cout << iEntry << " events were processed... " << std::endl;
  if ( TMath::Abs(Truth_tau_eta) > 1.0 ) continue;
  if ( !Truth_tau_isHadronic )  continue;

//    if ( iEntry == 2000 ) break;

  // Decay radius
  int numTracks=0; if ( Truth_tau_numTracks == 3 ) numTracks = 1;
  if ( Truth_tau_numTracks == 1 ) {
    h_TruthTauDecayRadius         [0][0] ->Fill(Truth_tau_decayRad);
    h_TruthTauDecayRadius_Detaled [0][5] ->Fill(Truth_tau_decayRad);
    h_TruthTauPt                  [0][0] ->Fill(Truth_tau_pt/1000.);
    // Reco_tau_numTracks = 0,1,2,3,4~6
    if ( Reco_tau_numTracks < 4 ){
      h_TruthTauDetailsDecayRad    [0][Reco_tau_numTracks]->Fill(Truth_tau_decayRad);
      h_TruthTauDecayRadius_Detaled[0][Reco_tau_numTracks] ->Fill(Truth_tau_decayRad);
      h_TruthTauDetailsPt          [0][Reco_tau_numTracks]->Fill(Truth_tau_pt/1000.);
    } else {
      h_TruthTauDetailsDecayRad    [0][4]->Fill(Truth_tau_decayRad);
      h_TruthTauDecayRadius_Detaled[0][4] ->Fill(Truth_tau_decayRad);
      h_TruthTauDetailsPt          [0][4]->Fill(Truth_tau_pt/1000.);
    }
    if ( Truth_tau_isMatchedWithReco ){
      h_TruthTauDecayRadius[0][1]->Fill(Truth_tau_decayRad);
      h_TruthTauPt         [0][1]->Fill(Truth_tau_pt/1000.);
      if ( Reco_tau_numTracks == 1 ){
        h_TruthTauDecayRadius[0][2]->Fill(Truth_tau_decayRad);
        h_TruthTauPt         [0][2]->Fill(Truth_tau_pt/1000.);
      }
    }
  } else if ( Truth_tau_numTracks == 3 ) {
    h_TruthTauDecayRadius         [1][0]                 ->Fill(Truth_tau_decayRad);
    h_TruthTauDecayRadius_Detaled [1][5] ->Fill(Truth_tau_decayRad);
    h_TruthTauPt                  [1][0]                 ->Fill(Truth_tau_pt/1000.);
    if ( Reco_tau_numTracks < 4 ){
      h_TruthTauDetailsDecayRad    [1][Reco_tau_numTracks]->Fill(Truth_tau_decayRad);
      h_TruthTauDecayRadius_Detaled[1][Reco_tau_numTracks] ->Fill(Truth_tau_decayRad);
      h_TruthTauDetailsPt          [1][Reco_tau_numTracks]->Fill(Truth_tau_pt/1000.);
    } else {
      h_TruthTauDetailsDecayRad    [1][4]->Fill(Truth_tau_decayRad);
      h_TruthTauDecayRadius_Detaled[1][4] ->Fill(Truth_tau_decayRad);
      h_TruthTauDetailsPt          [1][4]->Fill(Truth_tau_pt/1000.);
    }
    if ( Truth_tau_isMatchedWithReco ){
      h_TruthTauDecayRadius[1][1]->Fill(Truth_tau_decayRad);
      h_TruthTauPt         [1][1]->Fill(Truth_tau_pt/1000.);
      if ( Reco_tau_numTracks == 3 ){
        h_TruthTauDecayRadius[1][2]->Fill(Truth_tau_decayRad);
        h_TruthTauPt         [1][2]->Fill(Truth_tau_pt/1000.);
      }
    }
  }
}
//_//_//_//_//_//_//_//_//_//_//_//_//_//_//_//_//_//_//_//_//_//_//_//_ //_//_//_//_//_//_//_//_//_//_//_//_//_//_//_//_//_//_//_//_//_//_//_//
//
//                             Draw histograms
//
//_//_//_//_//_//_//_//_//_//_//_//_//_//_//_//_//_//_//_//_//_//_//_//_ //_//_//_//_//_//_//_//_//_//_//_//_//_//_//_//_//_//_//_//_//_//_//_//

{
  int CanvasNum = 2;
  std::vector<TCanvas*> canvas(CanvasNum);
  for ( int iCanvas=0; iCanvas<CanvasNum; iCanvas++)
    canvas[iCanvas] = new TCanvas( Form("DecayRad%d", iCanvas), Form("DecayRad%d", iCanvas));

  int Color[3] = {kBlack, kMagenta, kBlue};
  for ( int iProng=0; iProng<2; iProng++){
    canvas[iProng]->cd();
    gPad->SetLogy();
    TLegend* leg = new TLegend(0.8, 0.5, 0.9, 0.7);

    for ( int iCategory=0; iCategory<3; iCategory++){
      // Histogram
      h_TruthTauDecayRadius[iProng][iCategory]->SetMarkerColor(Color[iCategory]);
      h_TruthTauDecayRadius[iProng][iCategory]->SetMarkerSize(0.5);
      h_TruthTauDecayRadius[iProng][iCategory]->Draw("same:p");
      // Legend
      //ATLASLabel(0.5, 0.85, "Simulation");
      leg -> SetFillColor(0);
      leg -> SetBorderSize(0);
      leg -> AddEntry( h_TruthTauDecayRadius[iProng][iCategory], "" , "p");
    }
    canvas[iProng]->RedrawAxis();
    if ( iProng == 0 ) canvas[iProng]->SaveAs("HighPtTauPerformance.pdf(");
    if ( iProng == 1 ) canvas[iProng]->SaveAs("HighPtTauPerformance.pdf");
  }
}

// Efficiency 
{
  int CanvasNum = 2;
  std::vector<TCanvas*> canvas(CanvasNum);
  for ( int iCanvas=0; iCanvas<CanvasNum; iCanvas++)
    canvas[iCanvas] = new TCanvas( Form("DecayRadEff%d", iCanvas), Form("DecayRadEff%d", iCanvas));

  int Color[3] = {kBlack, kMagenta, kBlue};
  // Histogram
  TH1D* hist[2][3];
  TEfficiency* pEff[2][2];
  for ( int iProng=0; iProng<2; iProng++){
    canvas[iProng]->cd();
    //TLegend* leg = new TLegend(0.8, 0.5, 0.9, 0.7);
    for ( int iCategory=0; iCategory<3; iCategory++){
      hist[iProng][iCategory] = (TH1D*)h_TruthTauDecayRadius[iProng][iCategory]->Clone();
      hist[iProng][iCategory]  ->SetMaximum(1.02);
      hist[iProng][iCategory]  ->SetMinimum(0);
      if ( iCategory == 0 ) continue;
      pEff[iProng][iCategory-1] = new TEfficiency( *hist[iProng][iCategory], *hist[iProng][0] );
      pEff[iProng][iCategory-1]->SetLineColor  ( Color[iCategory] );
      pEff[iProng][iCategory-1]->SetMarkerColor( Color[iCategory] );
      pEff[iProng][iCategory-1]->SetMarkerSize(0.8);
      if ( iCategory == 1 ) pEff[iProng][iCategory-1]->Draw("AP");
      if ( iCategory == 2 ) pEff[iProng][iCategory-1]->Draw("P same");

      gPad->Update();
      pEff[iProng][iCategory-1] ->GetPaintedGraph()->GetXaxis()->SetLimits(0,554);
      pEff[iProng][iCategory-1] ->GetPaintedGraph()->SetMaximum(1.05);
      pEff[iProng][iCategory-1] ->GetPaintedGraph()->SetMinimum(0);
    
      //Double_t ymax = h_TruthTauDecayRadius[iProng][0]->GetYaxis()->GetXmax();
      Double_t ymax = 1.05;
      TLine* ID[8];
      ID[0] = new TLine(33.25, 0, 33.25, ymax);
      ID[1] = new TLine(50.5 , 0, 50.5 , ymax);
      ID[2] = new TLine(88.5 , 0, 88.5 , ymax);
      ID[3] = new TLine(122.5, 0, 122.5, ymax);
      ID[4] = new TLine(299,   0, 299,   ymax);
      ID[5] = new TLine(371,   0, 371,   ymax);
      ID[6] = new TLine(443,   0, 443,   ymax);
      ID[7] = new TLine(514,   0, 514,   ymax);
      for ( int iLayer=0; iLayer<7; iLayer++){
        ID[iLayer]->SetLineWidth(1);
        ID[iLayer]->SetLineStyle(2);
        ID[iLayer]->SetLineColor(kBlack);
        ID[iLayer]->Draw("same");
      }
      gPad->Update();
    }
    //ATLASLabel(0.2,0.2, "Simulation");
    canvas[iProng]->RedrawAxis();
    canvas[iProng]->SaveAs("HighPtTauPerformance.pdf");
  }
}

{
  int CanvasNum = 2;
  std::vector<TCanvas*> canvas(CanvasNum);
  for ( int iCanvas=0; iCanvas<CanvasNum; iCanvas++)
    canvas[iCanvas] = new TCanvas( Form("Pt%d", iCanvas), Form("Pt%d", iCanvas));

  int Color[3] = {kBlack, kMagenta, kBlue};
  for ( int iProng=0; iProng<2; iProng++){
    canvas[iProng]->cd();
    TLegend* leg = new TLegend(0.8, 0.5, 0.9, 0.7);

    for ( int iCategory=0; iCategory<3; iCategory++){
      // Histogram
      h_TruthTauPt[iProng][iCategory]->SetMarkerColor(Color[iCategory]);
      h_TruthTauPt[iProng][iCategory]->SetLineColor(Color[iCategory]);
      h_TruthTauPt[iProng][iCategory]->SetMarkerSize(0.5);
      h_TruthTauPt[iProng][iCategory]->Draw("same");
      // Legend
      //ATLASLabel(0.2,0.85, "Simulation");
      leg -> SetFillColor(0);
      leg -> SetBorderSize(0);
      leg -> AddEntry( h_TruthTauPt[iProng][iCategory], "" , "p");
    }
    //leg -> Draw();
    canvas[iProng]->RedrawAxis();
    canvas[iProng]->SaveAs("HighPtTauPerformance.pdf");
  }
}

///////////////////////////////////////////////
// Truth tau efficiencies 
{
  int CanvasNum = 2;
  std::vector<TCanvas*> canvas(CanvasNum);
  for ( int iCanvas=0; iCanvas<CanvasNum; iCanvas++)
    canvas[iCanvas] = new TCanvas( Form("DecayRadEff%d", iCanvas), Form("DecayRadEff%d", iCanvas));

  int Color[3] = {kBlack, kMagenta, kBlue};
  // Histogram
  TH1D* hist[2][3];
  TEfficiency* pEff[2][2];
  for ( int iProng=0; iProng<2; iProng++){
    canvas[iProng]->cd();
    //TLegend* leg = new TLegend(0.8, 0.5, 0.9, 0.7);

    for ( int iCategory=0; iCategory<3; iCategory++){
      hist[iProng][iCategory] = (TH1D*)h_TruthTauPt[iProng][iCategory]->Clone();

      if ( iCategory == 0 ) continue;
      pEff[iProng][iCategory-1] = new TEfficiency( *hist[iProng][iCategory], *hist[iProng][0] );
      pEff[iProng][iCategory-1] ->SetLineColor  ( Color[iCategory] );
      pEff[iProng][iCategory-1] ->SetMarkerColor( Color[iCategory] );
      pEff[iProng][iCategory-1] ->SetMarkerSize(0.8);
      if ( iCategory == 1 ) pEff[iProng][iCategory-1]->Draw("AP");
      if ( iCategory == 2 ) pEff[iProng][iCategory-1]->Draw("P same");

      gPad->Update();
      pEff[iProng][iCategory-1] ->GetPaintedGraph()->GetXaxis()->SetLimits(0,1000);
      pEff[iProng][iCategory-1] ->GetPaintedGraph()->SetMaximum(1.05);
      pEff[iProng][iCategory-1] ->GetPaintedGraph()->SetMinimum(0);
      gPad->Update();
    }
    //ATLASLabel(0.2,0.2, "Simulation");
    canvas[iProng]->RedrawAxis();
    canvas[iProng]->SaveAs("HighPtTauPerformance.pdf");
  }
}

///////////////////////////////////////////////
// Truth tau detailes 
{
  int CanvasNum = 2;
  std::vector<TCanvas*> canvas(CanvasNum);
  for ( int iCanvas=0; iCanvas<CanvasNum; iCanvas++)
    canvas[iCanvas] = new TCanvas( Form("DecayRadEff%d", iCanvas), Form("DecayRadEff%d", iCanvas));

  int Color[3] = {kBlack, kMagenta, kBlue};
  for ( int iProng=0; iProng<2; iProng++){
    canvas[iProng]->cd();
    TLegend* leg = new TLegend(0.8, 0.5, 0.9, 0.7);

    TH1D* deno = (TH1D*)h_TruthTauDecayRadius[iProng][0]->Clone();
    TH1D* hist[2][5];
    TEfficiency* pEff[2][5];
    for ( int iCategory=0; iCategory<5; iCategory++){
      hist[iProng][iCategory] = (TH1D*)h_TruthTauDetailsDecayRad[iProng][iCategory]->Clone();

      pEff[iProng][iCategory] = new TEfficiency( *hist[iProng][iCategory], *deno );
      pEff[iProng][iCategory] ->SetLineColor  ( iCategory+1 );
      pEff[iProng][iCategory] ->SetMarkerColor( iCategory+1 );
      pEff[iProng][iCategory] ->SetMarkerSize(0.8);
      if ( iCategory == 0 )
        pEff[iProng][iCategory]->Draw("AP");
      else 
        pEff[iProng][iCategory]->Draw("P same");

      gPad->Update();
      pEff[iProng][iCategory] ->GetPaintedGraph()->GetXaxis()->SetLimits(0,554);
      pEff[iProng][iCategory] ->GetPaintedGraph()->SetMaximum(1.05);
      pEff[iProng][iCategory] ->GetPaintedGraph()->SetMinimum(0);
      gPad->Update();
      Double_t ymax = 1.05;
      TLine* ID[8];
      ID[0] = new TLine(33.25, 0, 33.25, ymax);
      ID[1] = new TLine(50.5 , 0, 50.5 , ymax);
      ID[2] = new TLine(88.5 , 0, 88.5 , ymax);
      ID[3] = new TLine(122.5, 0, 122.5, ymax);
      ID[4] = new TLine(299,   0, 299,   ymax);
      ID[5] = new TLine(371,   0, 371,   ymax);
      ID[6] = new TLine(443,   0, 443,   ymax);
      ID[7] = new TLine(514,   0, 514,   ymax);
      for ( int iLayer=0; iLayer<7; iLayer++){
        ID[iLayer]->SetLineWidth(1);
        ID[iLayer]->SetLineStyle(2);
        ID[iLayer]->SetLineColor(kBlack);
        ID[iLayer]->Draw("same");
      }
    }
    //leg -> Draw();
    canvas[iProng]->RedrawAxis();
    canvas[iProng]->SaveAs("HighPtTauPerformance.pdf");
  }
}

///////////////////////////////////////////////
// Truth tau detailes 
{
  int CanvasNum = 2;
  std::vector<TCanvas*> canvas(CanvasNum);
  for ( int iCanvas=0; iCanvas<CanvasNum; iCanvas++)
    canvas[iCanvas] = new TCanvas( Form("DecayRadEff%d", iCanvas), Form("DecayRadEff%d", iCanvas));

  int Color[3] = {kBlack, kMagenta, kBlue};
  for ( int iProng=0; iProng<2; iProng++){
    canvas[iProng]->cd();
    TLegend* leg = new TLegend(0.2, 0.5, 0.4, 0.7);

    TH1D* deno = (TH1D*)h_TruthTauPt[iProng][0]->Clone();
    TH1D* hist[2][5];
    TEfficiency* pEff[2][5];
    for ( int iCategory=0; iCategory<5; iCategory++){
      hist[iProng][iCategory] = (TH1D*)h_TruthTauDetailsPt[iProng][iCategory]->Clone();

      pEff[iProng][iCategory] = new TEfficiency( *hist[iProng][iCategory], *deno );
      pEff[iProng][iCategory] ->SetLineColor  ( iCategory+1 );
      pEff[iProng][iCategory] ->SetMarkerColor( iCategory+1 );
      pEff[iProng][iCategory] ->SetMarkerSize(0.8);
      if ( iCategory == 0 )
        pEff[iProng][iCategory]->Draw("AP");
      else 
        pEff[iProng][iCategory]->Draw("P same");

      gPad->Update();
      pEff[iProng][iCategory] ->GetPaintedGraph()->GetXaxis()->SetLimits(0,1000);
      pEff[iProng][iCategory] ->GetPaintedGraph()->SetMaximum(1.05);
      pEff[iProng][iCategory] ->GetPaintedGraph()->SetMinimum(0);
      gPad->Update();

      leg -> SetFillColor(0);
      leg -> SetBorderSize(0);
      if ( iCategory<4 ) leg -> AddEntry( pEff[iProng][iCategory], Form ("reco %d-prong", iCategory), "l");
      else               leg -> AddEntry( pEff[iProng][4],         "reco (4~6)-prong", "l"); 
    }
    //     leg -> Draw();
    //      ATLASLabel(0.2,0.7, "Simulation");
    canvas[iProng]->RedrawAxis();
    canvas[iProng]->SaveAs("HighPtTauPerformance.pdf");
  }
}

///////////////////////////////////////////////
// Truth tau detailes 
{
  int CanvasNum = 2;
  std::vector<TCanvas*> canvas(CanvasNum);
  for ( int iCanvas=0; iCanvas<CanvasNum; iCanvas++)
    canvas[iCanvas] = new TCanvas( Form("DecayRadEff%d", iCanvas), Form("DecayRadEff%d", iCanvas));

  int Color[3] = {kBlack, kMagenta, kBlue};
  for ( int iProng=0; iProng<2; iProng++){
    canvas[iProng]->cd();
    TLegend* leg = new TLegend(0.8, 0.5, 0.9, 0.7);

    TH1D* deno = (TH1D*)h_TruthTauDecayRadius_Detaled[iProng][5]->Clone();
    TH1D* hist[2][5];
    TEfficiency* pEff[2][5];
    for ( int iCategory=0; iCategory<5; iCategory++){
      hist[iProng][iCategory] = (TH1D*)h_TruthTauDecayRadius_Detaled[iProng][iCategory]->Clone();

      pEff[iProng][iCategory] = new TEfficiency( *hist[iProng][iCategory], *deno );
      pEff[iProng][iCategory] ->SetLineColor  ( iCategory+1 );
      pEff[iProng][iCategory] ->SetMarkerColor( iCategory+1 );
      pEff[iProng][iCategory] ->SetMarkerSize(0.8);
      if ( iCategory == 0 )
        pEff[iProng][iCategory]->Draw("AP");
      else 
        pEff[iProng][iCategory]->Draw("P same");

      gPad->Update();
      pEff[iProng][iCategory] ->GetPaintedGraph()->GetXaxis()->SetLimits(0,200);
      pEff[iProng][iCategory] ->GetPaintedGraph()->SetMaximum(1.05);
      pEff[iProng][iCategory] ->GetPaintedGraph()->SetMinimum(0);
      gPad->Update();
      Double_t ymax = 1.05;
      TLine* ID[8];
      ID[0] = new TLine(33.25, 0, 33.25, ymax);
      ID[1] = new TLine(50.5 , 0, 50.5 , ymax);
      ID[2] = new TLine(88.5 , 0, 88.5 , ymax);
      ID[3] = new TLine(122.5, 0, 122.5, ymax);
      ID[4] = new TLine(299,   0, 299,   ymax);
      ID[5] = new TLine(371,   0, 371,   ymax);
      ID[6] = new TLine(443,   0, 443,   ymax);
      ID[7] = new TLine(514,   0, 514,   ymax);
      for ( int iLayer=0; iLayer<7; iLayer++){
        ID[iLayer]->SetLineWidth(1);
        ID[iLayer]->SetLineStyle(2);
        ID[iLayer]->SetLineColor(kBlack);
        ID[iLayer]->Draw("same");
      }
    }
    //leg -> Draw();
    canvas[iProng]->RedrawAxis();
    if ( iProng == 0 ) canvas[iProng]->SaveAs("HighPtTauPerformance.pdf");
    if ( iProng == 1 ) canvas[iProng]->SaveAs("HighPtTauPerformance.pdf)");
  }
}

std::cout << "/********************************************************/" << std::endl;
std::cout << "/*         The number of events in each region          */" << std::endl;
std::cout << "/********************************************************/" << std::endl;
std::cout << "All                        " << eventCounter.all << std::endl;
std::cout << "Hadronic                   " << eventCounter.hadronic << std::endl;
std::cout << "1-prong                    " << eventCounter.prong1 << std::endl;
std::cout << "|eta|<1.0                  " << eventCounter.eta_1_0 << std::endl;
std::cout << " 0     < R_{decay} < 33.25 " << eventCounter.decayRadLower33_25 << std::endl;
std::cout << " 33.25 < R_{decay} < 50.5  " << eventCounter.decayRadLower50_5 << std::endl;
std::cout << " 50.5  < R_{decay} < 88.5  " << eventCounter.decayRadLower88_5 << std::endl;
std::cout << " 88.5  < R_{decay} < 122.5 " << eventCounter.decayRadLower122_5 << std::endl;
std::cout << "R_{decay} > 122.5          " << eventCounter.decayRadLarger122_5<< std::endl;
std::cout << "3-prong                    " << eventCounter.prong3 << std::endl;

return;
}
