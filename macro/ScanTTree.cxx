

void ScanTTree()
{
  std::string simulator[2] ={ "MC12G4", "FullG4_LongLived"};
  //std::string cut = "noCut";
  //std::string cut = "decayRad33.25";
  //std::string cut = "decayRad50.5";
  //std::string cut = "decayRad88.5";
  std::string cut = "decayRad122.5";
  TTree* tree[2];

  for ( iSimulator=0; iSimulator<2; iSimulator++){
    std::string file_path = "/Users/takeda/workspace/public/atlas-phys/AJ/WG1/Data";
    std::string file_name = file_path + "/MySkimTrees." + simulator[iSimulator] + "_" + cut + ".outputs.root";
    std::cout << file_name << std::endl;
    TFile* file = new TFile( file_name.c_str() );
    tree[iSimulator] = (TTree*)file->Get("m_InDetTrackParticles");
  }

  std::string path = "/Users/takeda/workspace/public/atlas-phys/AJ/WG1/BasicVariables/InDetTrackParticles";
  std::string obj[90] = {
    "d0",
    "z0",
    "phi",
    "theta",
    "qOverP",
    "vx",
    "vy",
    "vz",
    "radiusOfFirstHit",
    "identifierOfFirstHit",
    "beamlineTiltX",
    "beamlineTiltY",
    "hitPattern",
    "chiSquared",
    "numberDoF",
    "trackFitter",
    "particleHypothesis",
    "trackProperties",
    "patternRecoInfo",
    "numberOfContribPixelLayers",
    "numberOfInnermostPixelLayerHits",
    "numberOfInnermostPixelLayerOutliers",
    "numberOfInnermostPixelLayerSharedHits",
    "numberOfInnermostPixelLayerSplitHits",
    "expectInnermostPixelLayerHit",
    "numberOfNextToInnermostPixelLayerHits",
    "numberOfNextToInnermostPixelLayerOutliers",
    "numberOfNextToInnermostPixelLayerSharedHits",
    "numberOfNextToInnermostPixelLayerSplitHits",
    "expectNextToInnermostPixelLayerHit",
    "numberOfPixelHits",
    "numberOfPixelOutliers",
    "numberOfPixelHoles",
    "numberOfPixelSharedHits",
    "numberOfPixelSplitHits",
    "numberOfGangedPixels",
    "numberOfGangedFlaggedFakes",
    "numberOfPixelDeadSensors",
    "numberOfPixelSpoiltHits",
    "numberOfDBMHits",
    "numberOfSCTHits",
    "numberOfSCTOutliers",
    "numberOfSCTHoles",
    "numberOfSCTDoubleHoles",
    "numberOfSCTSharedHits",
    "numberOfSCTDeadSensors",
    "numberOfSCTSpoiltHits",
    "numberOfTRTHits",
    "numberOfTRTOutliers",
    "numberOfTRTHoles",
    "numberOfTRTHighThresholdHits",
    "numberOfTRTHighThresholdHitsTotal",
    "numberOfTRTHighThresholdOutliers",
    "numberOfTRTDeadStraws",
    "numberOfTRTTubeHits",
    "numberOfTRTXenonHits",
    "numberOfTRTSharedHits",
    "numberOfPrecisionLayers",
    "numberOfPrecisionHoleLayers",
    "numberOfPhiLayers",
    "numberOfPhiHoleLayers",
    "numberOfTriggerEtaLayers",
    "numberOfTriggerEtaHoleLayers",
    "numberOfOutliersOnTrack",
    "standardDeviationOfChi2OS",
    "eProbabilityComb",
    "eProbabilityHT",
    "pixeldEdx",
    "numberOfUsedHitsdEdx",
    "numberOfIBLOverflowsdEdx",
    "TRTTrackOccupancy",
    "TRTdEdxUsedHits",
    "TrkIBLY",
    "TrkIBLZ",
    "TRTdEdx",
    "TrkBLX",
    "TrkBLY",
    "TrkBLZ",
    "nBC_meas",
    "TrkL1X",
    "TrkL1Y",
    "TrkL2X",
    "truthMatchProbability",
    "TrkL2Y",
    "TrkL2Z",
    "d0err",
    "z0err",
    "phierr",
    "thetaerr",
    "qopterr"};

  
  
  TCanvas* c[23];
  for ( int iCanvas=0; iCanvas<23; iCanvas++) {
    c[iCanvas] = new TCanvas( Form("c%d",iCanvas ), Form("c%d", iCanvas) );
    c[iCanvas]->Divide(2,2);
    std::string name;
    if ( iCanvas == 0  ){
      name = path + "/InDetTrackParticles_" + cut  + ".pdf(";
    } 
    else if ( 0 < iCanvas && iCanvas < 22 ){
      name = path + "/InDetTrackParticles_" + cut  + ".pdf";
    }
    else if ( iCanvas == 22 ){
      name = path + "/InDetTrackParticles_" + cut  + ".pdf)";
    }

    for ( int iSimulator=0; iSimulator<2; iSimulator++){
      TText* t;
      if(  iSimulator == 0 ){
        t = new TText(0.75,0.9, simulator[iSimulator].c_str() );
        t->SetNDC(1);
        t->SetTextAlign(22);
        t->SetTextColor(kBlack);
        t->SetTextFont(42);
        t->SetTextSize(0.05);
        t->SetTextAngle(0);
      } else if ( iSimulator == 1 ){
        t = new TText(0.81,0.85, simulator[iSimulator].c_str() );
        t->SetNDC(1);
        t->SetTextAlign(22);
        t->SetTextColor(kMagenta);
        t->SetTextFont(42);
        t->SetTextSize(0.05);
        t->SetTextAngle(0);
      }

      for ( int j = 1; j<=4; j++){
        if( j-1+4*iCanvas > 89 ) break;
        c[iCanvas]->cd(j);
        if ( iSimulator == 0 ){
          tree[iSimulator]->SetLineColor(kBlack);
          tree[iSimulator]->Draw( obj[ j-1 + 4*iCanvas].c_str() );
          gPad->SetLogy();
        } else if( iSimulator == 1 ){
          tree[iSimulator]->SetLineColor(kMagenta);
          tree[iSimulator]->Draw( obj[ j-1 + 4*iCanvas].c_str() ,"", "same" );
          gPad->SetLogy();
        }
        t->Draw("same");
      }
    }
    c[iCanvas]->Print( name.c_str() );
  }
}
