
#include <iostream>
#include <TH1.h>
#include <TFile.h>

#define _nPt
//#define _PtLeptonic
//#define _RecoPt
//#define _Fraction
#define _DecayRadius
#define _DecayRadiusFraction
//#define _DecayRadiusFraction_TruthVsReco

void FractionPlotter()
{
  std::string path = "/afs/cern.ch/user/k/ktakeda/workspace/public/atlas-phys/AJ/WG1/SingleParticle/Data/Ntuple/MC12G4/MyTauNtupleMC12G4_JETIDWP_MEDIUM.outputs.root";
//  std::string path = "/afs/cern.ch/user/k/ktakeda/workspace/public/atlas-phys/AJ/WG1/SingleParticle/AnalysisArea/run/MyTauNtupleFullG4.outputs.root";
  TFile* file = new TFile(path.c_str(), "read");
  TTree* tree = (TTree*)file->Get("m_recoTauTree");
  
  Double_t reco_pt; 
  Double_t reco_eta; 
  Double_t reco_phi; 
  Double_t reco_e; 
  Bool_t truth_leptonic_match = false; 
  Bool_t truth_leptonic_electron = false; 
  Bool_t truth_leptonic_muon = false; 
  Bool_t truth_leptonic_tau = false; 
  Bool_t truth_hadronic_match = false; 
  Double_t truth_pt; 
  Double_t truth_eta; 
  Double_t truth_phi; 
  Double_t truth_e; 
  Int_t truth_nprong =0; 
  Double_t truth_decay_radius = 0; 
  Int_t reco_number_of_tracks = 0; 
  Int_t reco_number_of_selected_tracks =0; 

  tree->SetBranchAddress("reco_pt", &reco_pt);
  tree->SetBranchAddress("reco_eta", &reco_eta);
  //tree->SetBranchAddress("reco_phi",);
  //tree->SetBranchAddress("reco_e",);
  tree->SetBranchAddress("truth_leptonic_match", &truth_leptonic_match);
  tree->SetBranchAddress("truth_leptonic_electron", &truth_leptonic_electron);
  tree->SetBranchAddress("truth_leptonic_muon", &truth_leptonic_muon);
  tree->SetBranchAddress("truth_leptonic_tau", &truth_leptonic_tau);
  tree->SetBranchAddress("truth_hadronic_match", &truth_hadronic_match);
  tree->SetBranchAddress("truth_pt", &truth_pt);
  //tree->SetBranchAddress("truth_eta",);
  //tree->SetBranchAddress("truth_phi",);
  //tree->SetBranchAddress("truth_e",);
  tree->SetBranchAddress("truth_decay_radius", &truth_decay_radius);
  tree->SetBranchAddress("truth_nprong", &truth_nprong);
  tree->SetBranchAddress("reco_number_of_tracks", &reco_number_of_tracks);
  tree->SetBranchAddress("reco_number_of_selected_tracks", &reco_number_of_selected_tracks);


  TH1D* h_truth_1p = new TH1D("h_truth_1p", ";;", 40, 0, 250);
  TH1D* h_reco_1p_1p = new TH1D("h_reco_1p_1p", ";;", 40, 0, 250);
  TH1D* h_reco_2p = new TH1D("h_reco_2p", ";;", 40, 0, 250);
  TH1D* h_reco_3p = new TH1D("h_reco_3p", ";;", 40, 0, 250);
  
  TH1D* h_truth_3p = new TH1D("h_truth_3p", ";;", 40, 0, 250);
  TH1D* h_reco_3p_3p = new TH1D("h_reco_3p_3p", ";;", 40, 0, 250);

  /// Reconstrusted momentum
  TH1D* h_reco_pt_all = new TH1D("h_reco_pt_all",";pT [GeV];Events",100, 0, 700);
  TH1D* h_reco_pt_allHadronic = new TH1D("h_reco_pt_allHadronic",";pT [GeV];Events",100, 0, 700);
  TH1D* h_reco_pt_leptonic = new TH1D("h_reco_pt_leptonic",";pT [GeV];Events",100, 0, 700);
  TH1D* h_reco_pt_electron = new TH1D("h_reco_pt_electron",";pT [GeV];Events",100, 0, 700);
  TH1D* h_reco_pt_muon = new TH1D("h_reco_pt_muon",";pT [GeV];Events",100, 0, 700);
  TH1D* h_reco_pt_tau = new TH1D("h_reco_pt_tau",";pT [GeV];Events",100, 0, 700);
  
  TH1D* h_reco_pt_1p = new TH1D("h_reco_pt_1p", ";;", 100, 0, 700);
  TH1D* h_reco_pt_2p = new TH1D("h_reco_pt_2p", ";;", 100, 0, 700);
  TH1D* h_reco_pt_3p = new TH1D("h_reco_pt_3p", ";;", 100, 0, 700);
  
  TH1D* h_pt_reco1p_truth1p = new TH1D("h_pt_reco1p_truth1p", ";;", 100, 0, 700);
  TH1D* h_pt_reco2p_truth2p = new TH1D("h_pt_reco2p_truth2p", ";;", 100, 0, 700);
  TH1D* h_pt_reco3p_truth3p = new TH1D("h_pt_reco3p_truth3p", ";;", 100, 0, 700);
  
  /// Fraction
  TH1D* h_Hadall = new TH1D("h_Hadall",";;",40, 0, 700);
  TH1D* h_reco1prong = new TH1D("h_reco1prong",";pT[GeV];Fraction",40, 0, 700);
  TH1D* h_reco2prong = new TH1D("h_reco2prong",";pT[GeV];Fraction",40, 0, 700);
  TH1D* h_reco3prong = new TH1D("h_reco3prong",";pT[GeV];Fraction",40, 0, 700);
  
  /// 1-prong ot distribution
  TH1D* h_truth_pt_1p = new TH1D("h_truth_pt_1p", ";;", 100, 0, 1000);
  TH1D* h_truth_pt_3p = new TH1D("h_truth_pt_3p", ";;", 100, 0, 1000);
  
  /// Reconstrustion momentum vs truth matched momentum

  /// Decay radius
  // Endcap
  TH1D* h_decay_rad_true1p[2];
  TH1D* h_reco_decay_allHadronic[2];
  TH1D* h_reco_decay_1p_truth_1p[2];
  TH1D* h_reco_decay_2p_truth_2p[2];
  TH1D* h_reco_decay_3p_truth_3p[2];
  TH1D* h_reco_decay_1p[2];
  TH1D* h_reco_decay_2p[2];
  TH1D* h_reco_decay_3p[2];
  std::string region[2] = {"Endcap","Forward"};
  for ( int i=0; i<2; i++){
    h_decay_rad_true1p[i]       = new TH1D(Form("h_decay_rad_true1p_%s", region[i].c_str()),";decay position [mm];Events",50,0,200);
    h_reco_decay_allHadronic[i] = new TH1D(Form("h_reco_decay_allHadronic_%s", region[i].c_str()),"",50,0,200);
    h_reco_decay_1p_truth_1p[i] = new TH1D(Form("h_reco_decay_1p_truth_1p_%s", region[i].c_str()),"",50,0,200);
    h_reco_decay_2p_truth_2p[i] = new TH1D(Form("h_reco_decay_2p_truth_2p_%s", region[i].c_str()),"",50,0,200);
    h_reco_decay_3p_truth_3p[i] = new TH1D(Form("h_reco_decay_3p_truth_3p_%s", region[i].c_str()),"",50,0,200);
    h_reco_decay_1p[i]          = new TH1D(Form("h_reco_decay_1p_%s", region[i].c_str()),"",50,0,200);
    h_reco_decay_2p[i]          = new TH1D(Form("h_reco_decay_2p_%s", region[i].c_str()),"",50,0,200);
    h_reco_decay_3p[i]          = new TH1D(Form("h_reco_decay_3p_%s", region[i].c_str()),"",50,0,200);
  }

  /// Number of tracks
  TH1D* h_number_of_truth_track = new TH1D("h_number_of_truth_track", ";truth tau [n-prong];Events", 5, -0.5, 4.5);
  TH1D* h_number_of_selected_reco_track = new TH1D("h_number_of_selected_reco_track", ";reco tau [n-prong];Events", 5, -0.5, 4.5);
  TH1D* h_number_of_selected_reco_track_truth_mached = new TH1D("h_number_of_selected_reco_track_truth_mached", ";reco truth matched tau [n-prong];Events", 5, -0.5, 4.5);

  for(int iEntry=0; iEntry< tree->GetEntries(); iEntry++){


    tree->GetEntry(iEntry);
    
    // Barel
    if ( 1.03 < TMath::Abs(reco_eta) ) continue;

    ///------------------------ Fraction (pT dependence) -----------------------------------
    /// Reconstrustion momentum vs truth matched momentum
    if ( truth_leptonic_match || truth_hadronic_match) h_reco_pt_all->Fill(reco_pt/1000.);
    /// To divide leptonic decay mode into e, mu, tau mode.
    if ( truth_leptonic_match ){
      h_reco_pt_leptonic->Fill(reco_pt/1000.);
      if( truth_leptonic_electron)
        h_reco_pt_electron->Fill(reco_pt/1000.);
      if( truth_leptonic_muon)
        h_reco_pt_muon->Fill(reco_pt/1000.);
      if( truth_leptonic_tau)
        h_reco_pt_tau->Fill(reco_pt/1000.);
    }

    if( truth_hadronic_match){
      h_reco_pt_allHadronic->Fill(reco_pt/1000.);
      h_reco_decay_allHadronic[0]->Fill(truth_decay_radius); // decay radius
      h_Hadall->Fill(reco_pt/1000.);
    }
    if( truth_hadronic_match && reco_number_of_selected_tracks == 1 ) { 
      h_reco_pt_1p->Fill(truth_pt/1000.);
      h_reco1prong->Fill(truth_pt/1000.);
      h_reco_decay_1p[0]->Fill(truth_decay_radius); // decay radius
      if( truth_nprong == 1 ){
        h_pt_reco1p_truth1p->Fill(reco_pt/1000.);
        h_reco_decay_1p_truth_1p[0]->Fill(truth_decay_radius); // decay radius
      }
    }
    if( truth_hadronic_match && reco_number_of_selected_tracks == 2 ) { 
      h_reco_pt_2p->Fill(truth_pt/1000.);
      h_reco2prong->Fill(truth_pt/1000.);
      h_reco_decay_2p[0]->Fill(truth_decay_radius); // decay radius
      if( truth_nprong == 2 ){
        h_pt_reco2p_truth2p->Fill(reco_pt/1000.);
        h_reco_decay_2p_truth_2p[0]->Fill(truth_decay_radius); // decay radius
      }
    }
    if( truth_hadronic_match && reco_number_of_selected_tracks == 3 ) {
      h_reco_pt_3p->Fill(truth_pt/1000.);
      h_reco3prong->Fill(truth_pt/1000.);
        h_reco_decay_3p[0]->Fill(truth_decay_radius); // decay radius
      if( truth_nprong == 3 ){
        h_pt_reco3p_truth3p->Fill(reco_pt/1000.);
        h_reco_decay_3p_truth_3p[0]->Fill(truth_decay_radius); // decay radius
      }
    }
      
    ///------------------------ Fraction (decay radius dependence) -----------------------------------
    /// Decay radius 
    if( truth_hadronic_match == true && truth_nprong == 1 ) {
      h_truth_1p->Fill(truth_decay_radius);
      if(reco_number_of_selected_tracks == 1 ){
        h_reco_1p_1p->Fill(truth_decay_radius);
        h_decay_rad_true1p[0]->Fill(truth_decay_radius);
      }
    }

    /// Number of tracks
    h_number_of_selected_reco_track->Fill(reco_number_of_selected_tracks);
    if( truth_hadronic_match == true ){
      h_number_of_selected_reco_track_truth_mached->Fill(reco_number_of_selected_tracks);
      h_number_of_truth_track->Fill(truth_nprong);
    }
    
    if(reco_number_of_selected_tracks == 3 ){
      h_reco_3p_3p->Fill(truth_decay_radius);
      if(truth_hadronic_match){
        if(truth_nprong == 3 ) {
          h_truth_3p->Fill(truth_decay_radius);
        }
      }
    }

  }

  // -----------------------------------------------------------------
  TCanvas* decay_length = new TCanvas("decay_length","decay_length");
  h_truth_1p->SetMarkerColor(kAzure);
  h_truth_1p->SetMarkerSize(0.9);
  h_truth_1p->Draw("e");
  
  h_reco_1p_1p->SetMarkerColor(kRed);
  h_reco_1p_1p->SetMarkerSize(0.9);
  h_reco_1p_1p->Draw("e:same");
  ATLASLabel(0.65,0.85, "Simulation");
  
  TLegend* leg1 = new TLegend(0.65, 0.65, 0.85, 0.8);
  leg1->SetFillColor(0);
  leg1->SetBorderSize(0);
  leg1->AddEntry(h_decay_rad_true1p[0], "Truth 1P", "pl");
  leg1->Draw();
  
  TLine* IBL = new TLine(33.25, 0, 33.25, 390);
  IBL->SetLineWidth(2);
  IBL->SetLineStyle(9);
  IBL->SetLineColor(kCyan-7);
  IBL->Draw("same");
  
  TLine* Blayer = new TLine(50.5, 0, 50.5, 390);
  Blayer->SetLineWidth(2);
  Blayer->SetLineStyle(9);
  Blayer->SetLineColor(kCyan-7);
  Blayer->Draw("same");
  
  TLine* Pix2 = new TLine(88.5, 0, 88.5, 390);
  Pix2->SetLineWidth(2);
  Pix2->SetLineStyle(9);
  Pix2->SetLineColor(kCyan-7);
  Pix2->Draw("same");
  
  TLine* Pix3 = new TLine(122.5, 0, 122.5, 390);
  Pix3->SetLineWidth(2);
  Pix3->SetLineStyle(9);
  Pix3->SetLineColor(kCyan-7);
  Pix3->Draw("same");
 
  TCanvas* c3 = new TCanvas("c3","c3");
  h_truth_3p->Sumw2();
  h_reco_3p_3p->Sumw2();
  //h_reco_3p_3p->Divide(h_reco_3p_3p, h_truth_3p, 1 ,1, "B");
  h_truth_3p->Divide(h_truth_3p, h_reco_3p_3p, 1 ,1, "B");

  h_truth_3p->SetMarkerStyle(32);
  h_truth_3p->SetMarkerSize(1);
  h_truth_3p->SetMarkerColor(kBlue);
  h_truth_3p->SetFillColor(0);
  h_truth_3p->SetLineColor(kBlue);
  h_truth_3p->SetLineWidth(2);
  h_truth_3p->Draw();
  
  // -----------------------------------------------------------------
  TCanvas* nTracks = new TCanvas("nTracks","nTracks");
  nTracks->Divide(2,2);
  nTracks->cd(1);
  h_number_of_truth_track->Draw();
  h_number_of_truth_track->GetYaxis()->SetRangeUser(0,7500);
  nTracks->cd(3);
  h_number_of_selected_reco_track->Draw();
  h_number_of_selected_reco_track->GetYaxis()->SetRangeUser(0,7500);
  nTracks->cd(4);
  h_number_of_selected_reco_track_truth_mached->Draw();
  h_number_of_selected_reco_track_truth_mached->GetYaxis()->SetRangeUser(0,7500);
  

#ifdef _PtLeptonic
  // -----------------------------------------------------------------
  TCanvas* nPtLeptonic = new TCanvas("nPtLeptonic","nPtLeptonic");
  h_reco_pt_leptonic->Draw();
  h_reco_pt_electron->SetLineColor(kRed);
  h_reco_pt_electron->Draw("same");
  h_reco_pt_muon->SetLineColor(kBlue);
  h_reco_pt_muon->Draw("same");
  h_reco_pt_tau->SetLineColor(kMagenta);
  h_reco_pt_tau->Draw("same");
  
  ATLASLabel(0.2,0.85, "Simulation");
  
  TLegend* legPtLeptonic = new TLegend(0.2, 0.6, 0.55, 0.8);
  legPtLeptonic->SetFillColor(0);
  legPtLeptonic->SetBorderSize(0);
  legPtLeptonic->AddEntry(h_reco_pt_leptonic, "reco leptonic all", "l");
  legPtLeptonic->AddEntry(h_reco_pt_electron, "reco (electron matched)", "l");
  legPtLeptonic->AddEntry(h_reco_pt_muon, "reco (muon matched)", "l");
  legPtLeptonic->AddEntry(h_reco_pt_tau, "reco (tau matched)", "l");
  legPtLeptonic->Draw();
#endif 

#ifdef _RecoPt
  //------------------------------------------------------------------
  // To compare the all of reo hadronic tau with reco-tau 1p and 3p
  TCanvas* PtDistribution = new TCanvas("PtDistribution","PtDistribution");
  h_reco_pt_allHadronic = (TH1D*)h_reco_pt_allHadronic->Clone();
  h_reco_pt_allHadronic->SetLineColor(kBlack);
  h_reco_pt_allHadronic->Draw();
  h_reco_pt_1p->SetLineColor(kRed);
  h_reco_pt_1p->Draw("same");
  h_reco_pt_2p->SetLineColor(kBlue);
  h_reco_pt_2p->Draw("same");
  h_reco_pt_3p->SetLineColor(kMagenta);
  h_reco_pt_3p->Draw("same");
  
  ATLASLabel(0.2,0.85, "Simulation");
  
  TLegend* leg = new TLegend(0.2, 0.65, 0.3, 0.8);
  leg->SetFillColor(0);
  leg->SetBorderSize(0);
  leg->AddEntry(h_reco_pt_allHadronic, "reco all", "l");
  leg->AddEntry(h_reco_pt_1p, "reco 1p", "l");
  leg->AddEntry(h_reco_pt_2p, "reco 2p", "l");
  leg->AddEntry(h_reco_pt_3p, "reco 3p", "l");
  leg->Draw();

  //h_pt_reco1p_truth1p->SetLineColor(kBlue);
  //h_pt_reco1p_truth1p->Draw("same");
  //h_pt_reco3p_truth3p->SetLineColor(kYellow);
  //h_pt_reco3p_truth3p->Draw("same");

  PtDistribution->RedrawAxis();
#endif  

#ifdef _Fraction
  //------------------------------------------------------------------
  // To compare the all of reo hadronic tau with reco-tau 1p and 3p
  TCanvas* fraction = new TCanvas("fraction","fraction");
  h_reco1prong->Divide(h_reco1prong, h_Hadall);
  h_reco1prong->SetMarkerColor(kRed);
  h_reco1prong->SetMarkerSize(0.7);
  h_reco1prong->Draw("P");
  
  h_reco2prong->Divide(h_reco2prong, h_Hadall);
  h_reco2prong->SetMarkerColor(kBlue);
  h_reco2prong->SetMarkerSize(0.7);
  h_reco2prong->Draw("P:same");
  
  h_reco3prong->Divide(h_reco3prong, h_Hadall);
  h_reco3prong->SetMarkerColor(kMagenta);
  h_reco3prong->SetMarkerSize(0.7);
  h_reco3prong->Draw("P:same");

  h_reco1prong->GetXaxis()->SetTitleOffset(0.9);
  h_reco1prong->GetYaxis()->SetTitleOffset(0.8);
  
  ATLASLabel(0.2,0.55, "Simulation");
  
  TLegend* legFraction = new TLegend(0.2, 0.3, 0.7, 0.5);
  legFraction->SetFillColor(0);
  legFraction->SetBorderSize(0);
  legFraction->AddEntry(h_reco1prong, "reconstructed as reco 1-prong", "P");
  legFraction->AddEntry(h_reco2prong, "reconstructed as reco 2-prong", "P");
  legFraction->AddEntry(h_reco3prong, "reconstructed as reco 3-prong", "P");
  legFraction->Draw();
#endif  

#ifdef _DecayRadius
  //------------------------------------------------------------------
  // To compare the all of reo hadronic tau with reco-tau 1p and 3p
  TCanvas* DecayRadius = new TCanvas("DecayRadius","DecayRadius", 700, 1000);
  DecayRadius->Divide(1,2);
  
  /// Check whether a tau reconstructed as 1-prong matches truth 1-prong. 
  DecayRadius->cd(1);
  h_reco_decay_allHadronic[0]->SetMarkerSize(0.7);
  h_reco_decay_allHadronic[0]->Draw("p");
  
  h_reco_decay_1p_truth_1p[0]->SetMarkerColor(kRed);
  h_reco_decay_1p_truth_1p[0]->SetMarkerSize(0.7);
  h_reco_decay_1p_truth_1p[0]->Draw("p:same");
  
  h_reco_decay_2p_truth_2p[0]->SetMarkerColor(kBlue);
  h_reco_decay_2p_truth_2p[0]->SetMarkerSize(0.7);
  h_reco_decay_2p_truth_2p[0]->Draw("p:same");
  
  h_reco_decay_3p_truth_3p[0]->SetMarkerColor(kMagenta);
  h_reco_decay_3p_truth_3p[0]->SetMarkerSize(0.7);
  h_reco_decay_3p_truth_3p[0]->Draw("p:same");
  
  TLegend* legDecayRadius = new TLegend(0.5, 0.7, 0.8, 0.8);
  legDecayRadius->SetFillColor(0);
  legDecayRadius->SetBorderSize(0);
  legDecayRadius->AddEntry(h_reco_decay_allHadronic[0], "hadoronic reco tau", "P");
  legDecayRadius->AddEntry(h_reco_decay_1p_truth_1p[0], "matches truth 1-prong", "P");
  //legDecayRadius->AddEntry(h_reco_decay_2p_truth_2p, "matches truth 2-prong", "P");
  legDecayRadius->AddEntry(h_reco_decay_3p_truth_3p[0], "matches truth 3-prong", "P");
  legDecayRadius->Draw();
  
  /// See some distributions of a tau reconstructed as n-prong.
  DecayRadius->cd(2);
  h_reco_decay_allHadronic[0]->SetMarkerSize(0.7);
  h_reco_decay_allHadronic[0]->Draw("p");
  
  h_reco_decay_1p[0]->SetMarkerColor(kRed);
  h_reco_decay_1p[0]->SetMarkerSize(0.7);
  h_reco_decay_1p[0]->Draw("p:same");
  
  h_reco_decay_2p[0]->SetMarkerColor(kBlue);
  h_reco_decay_2p[0]->SetMarkerSize(0.7);
  h_reco_decay_2p[0]->Draw("p:same");
  
  h_reco_decay_3p[0]->SetMarkerColor(kMagenta);
  h_reco_decay_3p[0]->SetMarkerSize(0.7);
  h_reco_decay_3p[0]->Draw("p:same");
  
  TLegend* legDecayRadius2 = new TLegend(0.5, 0.6, 1.0, 0.8);
  legDecayRadius2->SetFillColor(0);
  legDecayRadius2->SetFillStyle(0);
  legDecayRadius2->SetBorderSize(0);
  legDecayRadius2->AddEntry(h_reco_decay_allHadronic[0], "hadoronic reco tau", "P");
  legDecayRadius2->AddEntry(h_reco_decay_1p[0], "reco 1-prong", "P");
  legDecayRadius2->AddEntry(h_reco_decay_2p[0], "reco 2-prong", "P");
  legDecayRadius2->AddEntry(h_reco_decay_3p[0], "reco 3-prong", "P");
  legDecayRadius2->Draw();
#endif

#ifdef _DecayRadiusFraction
  //------------------------------------------------------------------
  // To compare the all of reo hadronic tau with reco-tau 1p and 3p
  TCanvas* DecayRadiusFractionEndcap = new TCanvas("DecayRadiusFractionEndcap","DecayRadiusFractionEndcap", 700, 1000);
  DecayRadiusFractionEndcap->Divide(1,2);
  DecayRadiusFractionEndcap->cd(1);
  h_reco_decay_allHadronic[0] = (TH1D*)h_reco_decay_allHadronic[0]->Clone();
  h_reco_decay_1p_truth_1p[0] = (TH1D*)h_reco_decay_1p_truth_1p[0]->Clone();
  h_reco_decay_2p_truth_2p[0] = (TH1D*)h_reco_decay_2p_truth_2p[0]->Clone();
  h_reco_decay_3p_truth_3p[0] = (TH1D*)h_reco_decay_3p_truth_3p[0]->Clone();

  h_reco_decay_1p_truth_1p[0]->Divide(h_reco_decay_1p_truth_1p[0], h_reco_decay_allHadronic[0]);
  h_reco_decay_1p_truth_1p[0]->SetMarkerColor(kRed);
  h_reco_decay_1p_truth_1p[0]->SetMarkerSize(0.7);
  h_reco_decay_1p_truth_1p[0]->GetYaxis()->SetRangeUser(0,1);
  h_reco_decay_1p_truth_1p[0]->Draw("p:same");
  
  h_reco_decay_2p_truth_2p[0]->Divide(h_reco_decay_2p_truth_2p[0], h_reco_decay_allHadronic[0]);
  h_reco_decay_2p_truth_2p[0]->SetMarkerColor(kBlue);
  h_reco_decay_2p_truth_2p[0]->SetMarkerSize(0.7);
  h_reco_decay_2p_truth_2p[0]->Draw("p:same");

  h_reco_decay_3p_truth_3p[0]->Divide(h_reco_decay_3p_truth_3p[0], h_reco_decay_allHadronic[0]);
  h_reco_decay_3p_truth_3p[0]->SetMarkerColor(kMagenta);
  h_reco_decay_3p_truth_3p[0]->SetMarkerSize(0.7);
  h_reco_decay_3p_truth_3p[0]->GetYaxis()->SetRangeUser(0,1);
  h_reco_decay_3p_truth_3p[0]->Draw("p:same");
  
  
  TLegend* legDecayRadiusFraction = new TLegend(0.2, 0.3, 0.7, 0.5);
  legDecayRadiusFraction->SetFillColor(0);
  legDecayRadiusFraction->SetBorderSize(0);
//legDecayRadiusFraction->AddEntry((TObject*)0,"reco n-prong (matched with truth n-prong)/all reco","");
  legDecayRadiusFraction->AddEntry(h_reco_decay_1p_truth_1p[0], "reco 1-prong matches truth 1-prong", "P");
  legDecayRadiusFraction->AddEntry(h_reco_decay_3p_truth_3p[0], "reco 3-prong matches truth 3-prong", "P");
  legDecayRadiusFraction->Draw();
  
  // --- * --- * --- * --- *--- * --- *--- * --- *--- * --- *--- * --- *
  DecayRadiusFractionEndcap->cd(2);
  h_reco_decay_allHadronic[0] = (TH1D*)h_reco_decay_allHadronic[0]->Clone();
  h_reco_decay_1p[0]          = (TH1D*)h_reco_decay_1p[0]->Clone();
  h_reco_decay_2p[0]          = (TH1D*)h_reco_decay_2p[0]->Clone();
  h_reco_decay_3p[0]          = (TH1D*)h_reco_decay_3p[0]->Clone();

  h_reco_decay_1p[0]->Divide(h_reco_decay_1p[0], h_reco_decay_allHadronic[0]);
  h_reco_decay_1p[0]->SetMarkerColor(kRed);
  h_reco_decay_1p[0]->SetMarkerSize(0.7);
  h_reco_decay_1p[0]->GetYaxis()->SetRangeUser(0,1);
  h_reco_decay_1p[0]->Draw("p:same");
  
  h_reco_decay_2p[0]->Divide(h_reco_decay_2p[0], h_reco_decay_allHadronic[0]);
  h_reco_decay_2p[0]->SetMarkerColor(kBlue);
  h_reco_decay_2p[0]->SetMarkerSize(0.7);
  h_reco_decay_2p[0]->Draw("p:same");

  h_reco_decay_3p[0]->Divide(h_reco_decay_3p[0], h_reco_decay_allHadronic[0]);
  h_reco_decay_3p[0]->SetMarkerColor(kMagenta);
  h_reco_decay_3p[0]->SetMarkerSize(0.7);
  h_reco_decay_3p[0]->GetYaxis()->SetRangeUser(0,1);
  h_reco_decay_3p[0]->Draw("p:same");
  
  
  TLegend* legDecayRadiusFraction2 = new TLegend(0.2, 0.3, 0.7, 0.5);
  legDecayRadiusFraction2->SetFillColor(0);
  legDecayRadiusFraction2->SetBorderSize(0);
  legDecayRadiusFraction2->AddEntry(h_reco_decay_1p[0], "reco 1-prong / all hadronic reco", "P");
  legDecayRadiusFraction2->AddEntry(h_reco_decay_2p[0], "reco 2-prong / all hadronic reco", "P");
  legDecayRadiusFraction2->AddEntry(h_reco_decay_3p[0], "reco 3-prong / all hadronic reco", "P");
  legDecayRadiusFraction2->Draw();
#endif

#ifdef _DecayRadiusFraction_TruthVsReco
  //------------------------------------------------------------------
  // To compare the all of reo hadronic tau with reco-tau 1p and 3p
  TCanvas* DecayRadiusFraction_TruthVsReco = new TCanvas("DecayRadiusFraction_TruthVsReco","DecayRadiusFraction_TruthVsReco");
  h_reco_decay_1p_truth_1p[0] = (TH1D*)h_reco_decay_1p_truth_1p[0]->Clone();
  h_reco_decay_2p_truth_2p[0] = (TH1D*)h_reco_decay_2p_truth_2p[0]->Clone();
  h_reco_decay_3p_truth_3p[0] = (TH1D*)h_reco_decay_3p_truth_3p[0]->Clone();
  h_reco_decay_1p[0] = (TH1D*)h_reco_decay_1p[0]->Clone();
  h_reco_decay_2p[0] = (TH1D*)h_reco_decay_2p[0]->Clone();
  h_reco_decay_3p[0] = (TH1D*)h_reco_decay_3p[0]->Clone();

  h_reco_decay_1p[0]->Divide(h_reco_decay_1p_truth_1p, h_reco_decay_1p[0]);
  h_reco_decay_1p[0]->SetMarkerColor(kRed);
  h_reco_decay_1p[0]->SetMarkerSize(0.7);
  h_reco_decay_1p[0]->GetYaxis()->SetRangeUser(0,1);
  h_reco_decay_1p[0]->Draw("p:same");

//  h_reco_decay_2p->Divide(h_reco_decay_2p, h_reco_decay_1p_truth_1p);
  h_reco_decay_3p[0]->Divide(h_reco_decay_3p_truth_3p, h_reco_decay_3p);
  h_reco_decay_3p[0]->SetMarkerColor(kMagenta);
  h_reco_decay_3p[0]->SetMarkerSize(0.7);
  h_reco_decay_3p[0]->GetYaxis()->SetRangeUser(0,1);
  h_reco_decay_3p[0]->Draw("p:same");
  
  ATLASLabel(0.25,0.45, "Simulation");
  TLegend* legDecayRadiusFraction_TruthVsReco = new TLegend(0.2, 0.3, 0.8, 0.4);
  legDecayRadiusFraction_TruthVsReco->SetFillColor(0);
  legDecayRadiusFraction_TruthVsReco->SetBorderSize(0);
  legDecayRadiusFraction_TruthVsReco->AddEntry(h_reco_decay_1p[0], "reco 1-prong matced with truth 1-prong/ all reco 1-prong", "P");
  legDecayRadiusFraction_TruthVsReco->AddEntry(h_reco_decay_3p[0], "reco 3-prong matced with truth 3-prong/ all reco 3-prong", "P");
  legDecayRadiusFraction_TruthVsReco->Draw();
#endif
  return;
}
