
#include <iostream>
#include <TH1.h>
#include <TFile.h>

void EfficiencyPlotter()
{
  std::string path = "/afs/cern.ch/user/k/ktakeda/workspace/public/atlas-phys/AJ/WG1/SingleParticle/Data/Ntuple/MC12G4/MyTauNtupleMC12G4_JETIDWP_NONE.outputs.root";
  //std::string pathFullG4 = "/afs/cern.ch/user/k/ktakeda/workspace/public/atlas-phys/AJ/WG1/SingleParticle/AnalysisArea/run/MyTauNtupleFullG4.outputs.root";
  
  TFile* file = new TFile(path.c_str(), "read");
  TTree* tree = (TTree*)file->Get("m_trueTauTree");
  
  Bool_t tt_truth_match = false; 
  Bool_t truth_leptonic_match = false; 
  Bool_t truth_leptonic_electron = false; 
  Bool_t truth_leptonic_muon = false; 
  Bool_t truth_leptonic_tauon = false; 
  Bool_t truth_hadronic_match = false; 
  Double_t tt_truth_pt; 
  Double_t tt_reco_pt; 
  Double_t tt_truth_eta; 
  Double_t tt_truth_phi; 
  Double_t tt_truth_e; 
  Double_t tt_truth_decay_radius = 0; 
  Int_t reco_number_of_tracks = 0; 
  
  Int_t tt_truth_nprong =0; 
  Int_t tt_reco_number_of_selected_tracks =0; 

  tree->SetBranchAddress("tt_truth_pt", &tt_truth_pt);
  tree->SetBranchAddress("tt_truth_eta", &tt_truth_eta);
  tree->SetBranchAddress("tt_truth_match", &tt_truth_match);
  tree->SetBranchAddress("tt_truth_nprong", &tt_truth_nprong);
  tree->SetBranchAddress("tt_reco_pt", &tt_reco_pt);
  tree->SetBranchAddress("tt_reco_number_of_selected_tracks", &tt_reco_number_of_selected_tracks);
  tree->SetBranchAddress("tt_truth_decay_radius", &tt_truth_decay_radius);

  TH1D* h_truth_pt = new TH1D("h_truth_pt", ";;", 100, 0, 700);
  TH1D* h_reco_pt = new TH1D("h_reco_pt", ";;", 100, 0, 700);
  TH1D* h_truth_match = new TH1D("h_truth_match", ";;", 100, 0, 700);
  TH1D* h_pt_truth_1p = new TH1D("h_pt_truth_1p", ";;", 100, 0, 700);
  
  /// Truth is 1 prong, and reco tau is ...
  TH1D* h_pt_truth_1p_reco_1p = new TH1D("h_pt_truth_1p_reco_1p", ";;", 100, 0, 700);
  TH1D* h_pt_truth_1p_reco_2p = new TH1D("h_pt_truth_1p_reco_2p", ";;", 100, 0, 700);
  TH1D* h_pt_truth_1p_reco_3p = new TH1D("h_pt_truth_1p_reco_3p", ";;", 100, 0, 700);
  
  /// Truth is 1 prong, and reco tau is ...
  TH1D* h_truth_radius            = new TH1D("h_truth_radius", ";;", 50, 0, 200);
  TH1D* h_truth_radius_1p         = new TH1D("h_truth_radius_1p", ";;", 50, 0, 200);
  TH1D* h_truth_radius_3p         = new TH1D("h_truth_radius_3p", ";;", 50, 0, 200);
  TH1D* h_radius_truth_1p         = new TH1D("h_radius_truth_1p", ";;", 50, 0, 200);
  TH1D* h_radius_truth_1p_reco_1p = new TH1D("h_radius_truth_1p_reco_1p", ";decay radius [mm];", 50, 0, 200);
  TH1D* h_radius_truth_1p_reco_2p = new TH1D("h_radius_truth_1p_reco_2p", ";decay radius [mm];", 50, 0, 200);
  TH1D* h_radius_truth_1p_reco_3p = new TH1D("h_radius_truth_1p_reco_3p", ";decay radius [mm];", 50, 0, 200);
  
  /// Truth is 3 prong, and reco tau is ...
  TH1D* h_radius_truth_3p         = new TH1D("h_radius_truth_3p", ";;", 50, 0, 200);
  TH1D* h_radius_truth_3p_reco_1p = new TH1D("h_radius_truth_3p_reco_1p", ";decay radius [mm];", 50, 0, 200);
  TH1D* h_radius_truth_3p_reco_2p = new TH1D("h_radius_truth_3p_reco_2p", ";decay radius [mm];", 50, 0, 200);
  TH1D* h_radius_truth_3p_reco_3p = new TH1D("h_radius_truth_3p_reco_3p", ";decay radius [mm];", 50, 0, 200);

  for(int iEntry=0; iEntry< tree->GetEntries(); iEntry++){
    tree->GetEntry(iEntry);

    if(1.03 > TMath::Abs(tt_truth_eta)) continue;

    ///------------------------ Efficiency -----------------------------------
    h_truth_pt->Fill(tt_truth_pt/1000.);
    h_truth_radius->Fill(tt_truth_decay_radius);
    if ( tt_truth_nprong == 1 ) h_truth_radius_1p->Fill(tt_truth_decay_radius);
    if ( tt_truth_nprong == 3 ) h_truth_radius_3p->Fill(tt_truth_decay_radius);
   
    if( tt_truth_match ){
      h_reco_pt->Fill(tt_reco_pt/1000.);
    }
  
    if ( tt_truth_nprong == 1){
      h_pt_truth_1p->Fill(tt_truth_pt/1000.);
      h_radius_truth_1p->Fill(tt_truth_decay_radius);

      if( tt_truth_match ){
        if (tt_reco_number_of_selected_tracks == 1){
          h_pt_truth_1p_reco_1p->Fill(tt_reco_pt/1000.);
          h_radius_truth_1p_reco_1p->Fill(tt_truth_decay_radius);
        }
        if (tt_reco_number_of_selected_tracks == 2){
          h_pt_truth_1p_reco_2p->Fill(tt_reco_pt/1000.);
          h_radius_truth_1p_reco_2p->Fill(tt_truth_decay_radius);
        }
        if (tt_reco_number_of_selected_tracks == 3){
          h_pt_truth_1p_reco_3p->Fill(tt_reco_pt/1000.);
          h_radius_truth_1p_reco_3p->Fill(tt_truth_decay_radius);
        }
      }
    } 
    
    if ( tt_truth_nprong == 3){
      h_radius_truth_3p->Fill(tt_truth_decay_radius);

      if( tt_truth_match ){
        if (tt_reco_number_of_selected_tracks == 1){
          h_radius_truth_3p_reco_1p->Fill(tt_truth_decay_radius);
        }
        if (tt_reco_number_of_selected_tracks == 2){
          h_radius_truth_3p_reco_2p->Fill(tt_truth_decay_radius);
        }
        if (tt_reco_number_of_selected_tracks == 3){
          h_pt_truth_1p_reco_3p->Fill(tt_reco_pt/1000.);
          h_radius_truth_3p_reco_3p->Fill(tt_truth_decay_radius);
        }
      }
    } 

  }

  /// 
  //h_pt_truth_1p->Draw();
  h_pt_truth_1p_reco_1p->Divide(h_pt_truth_1p_reco_1p, h_pt_truth_1p);
  h_pt_truth_1p_reco_1p->Draw("same");
  
  h_pt_truth_1p_reco_2p->Divide(h_pt_truth_1p_reco_2p, h_pt_truth_1p);
  h_pt_truth_1p_reco_2p->Draw("same");
  
  h_pt_truth_1p_reco_3p->Divide(h_pt_truth_1p_reco_3p, h_pt_truth_1p);
  h_pt_truth_1p_reco_3p->Draw("same");
  
  /// -----------------------------------------------------------------
  //// Truth decay radius
  TCanvas* Truth_DecayRadius = new TCanvas("Truth_DecayRadius","Truth_DecayRadius");
  h_truth_radius            = (TH1D*)h_truth_radius->Clone();
  h_radius_truth_1p         = (TH1D*)h_radius_truth_1p->Clone();
  h_radius_truth_3p         = (TH1D*)h_radius_truth_3p->Clone();
  
  h_radius_truth_1p->SetMarkerColor(kRed);
  h_radius_truth_3p->SetMarkerColor(kTeal+2);
  
  h_truth_radius->SetMarkerSize(0.7);
  h_radius_truth_1p->SetMarkerSize(0.7);
  h_radius_truth_3p->SetMarkerSize(0.7);

  h_truth_radius->Draw("same:p");
  h_radius_truth_1p->Draw("same:p");
  h_radius_truth_3p->Draw("same:p");
  
  ATLASLabel(0.2,0.7, "Simulation");
  /*TLegend* legAll = new TLegend(0.55, 0.35, 0.95, 0.45);
  legAll->SetFillColor(0);
  legAll->SetFillStyle(0);
  legAll->SetBorderSize(0);
  legAll->AddEntry(h_radius_truth_1p_reco_1p, "reco 1-prong/truth 1-prong", "pl");
  legAll->AddEntry(h_radius_truth_3p_reco_3p, "reco 3-prong/truth 3-prong", "pl");
  legAll->Draw();
  */
  
  /// -----------------------------------------------------------------
  //// All efficiency
  TCanvas* DecayRadius_all = new TCanvas("DecayRadius_all","DecayRadius_all");
  h_radius_truth_1p_reco_1p = (TH1D*)h_radius_truth_1p_reco_1p->Clone();
  h_radius_truth_3p_reco_3p = (TH1D*)h_radius_truth_3p_reco_3p->Clone();

  h_radius_truth_1p_reco_1p->Divide(h_radius_truth_1p_reco_1p, h_radius_truth_1p);
  h_radius_truth_3p_reco_3p->Divide(h_radius_truth_3p_reco_3p, h_radius_truth_3p);
  
  h_radius_truth_1p_reco_1p->SetMarkerColor(kRed);
  h_radius_truth_3p_reco_3p->SetMarkerColor(kTeal+2);
  
  h_radius_truth_1p_reco_1p->SetMarkerSize(0.7);
  h_radius_truth_3p_reco_3p->SetMarkerSize(0.7);

  h_radius_truth_1p_reco_1p->GetYaxis()->SetRangeUser(0,1);
  h_radius_truth_1p_reco_1p->GetYaxis()->SetTitle("Efficiency");
  h_radius_truth_1p_reco_1p->Draw("same:p");
  h_radius_truth_3p_reco_3p->Draw("same:p");
  
  ATLASLabel(0.2,0.7, "Simulation");
  /*TLegend* legAll = new TLegend(0.55, 0.35, 0.95, 0.45);
  legAll->SetFillColor(0);
  legAll->SetFillStyle(0);
  legAll->SetBorderSize(0);
  legAll->AddEntry(h_radius_truth_1p_reco_1p, "reco 1-prong/truth 1-prong", "pl");
  legAll->AddEntry(h_radius_truth_3p_reco_3p, "reco 3-prong/truth 3-prong", "pl");
  legAll->Draw();
  */

  
  /// -----------------------------------------------------------------
  //// 1-prong
  TCanvas* DecayRadius_1p = new TCanvas("DecayRadius_1p","DecayRadius_1p");
  TH1D* clone_radius_truth_1p_reco_2p = (TH1D*)h_radius_truth_1p_reco_2p->Clone();
  TH1D* clone_radius_truth_1p_reco_3p = (TH1D*)h_radius_truth_1p_reco_3p->Clone();

  h_radius_truth_1p_reco_2p->Divide(h_radius_truth_1p_reco_2p, h_radius_truth_1p);
  h_radius_truth_1p_reco_3p->Divide(h_radius_truth_1p_reco_3p, h_radius_truth_1p);
  
  h_radius_truth_1p_reco_2p->SetMarkerColor(kBlue);
  h_radius_truth_1p_reco_3p->SetMarkerColor(kTeal+2);
  
  h_radius_truth_1p_reco_2p->SetMarkerSize(0.7);
  h_radius_truth_1p_reco_3p->SetMarkerSize(0.7);

  h_radius_truth_1p_reco_1p->GetYaxis()->SetRangeUser(0,1);
  h_radius_truth_1p_reco_1p->GetYaxis()->SetTitle("Efficiency");
  
  h_radius_truth_1p_reco_1p->Draw("same:p");
  h_radius_truth_1p_reco_2p->Draw("same:p");
  h_radius_truth_1p_reco_3p->Draw("same:p");
  
  ATLASLabel(0.65,0.5, "Simulation");
  TLegend* leg = new TLegend(0.55, 0.35, 0.95, 0.45);
  leg->SetFillColor(0);
  leg->SetFillStyle(0);
  leg->SetBorderSize(0);
  leg->AddEntry(h_radius_truth_1p_reco_1p, "reco 1-prong/truth 1-prong", "pl");
  leg->AddEntry(h_radius_truth_1p_reco_2p, "reco 2-prong/truth 1-prong", "pl");
  leg->AddEntry(h_radius_truth_1p_reco_3p, "reco 3-prong/truth 1-prong", "pl");
  leg->Draw();


  /// -----------------------------------------------------------------
  //// 3-prong
  TCanvas* DecayRadius_3p = new TCanvas("DecayRadius_3p","DecayRadius_3p");
  h_radius_truth_3p_reco_1p = (TH1D*)h_radius_truth_3p_reco_1p->Clone();
  h_radius_truth_3p_reco_2p = (TH1D*)h_radius_truth_3p_reco_2p->Clone();

  h_radius_truth_3p_reco_1p->Divide(h_radius_truth_3p_reco_1p, h_radius_truth_3p);
  h_radius_truth_3p_reco_2p->Divide(h_radius_truth_3p_reco_2p, h_radius_truth_3p);

  h_radius_truth_3p_reco_1p->SetMarkerColor(kRed);
  h_radius_truth_3p_reco_2p->SetMarkerColor(kBlue);
  
  h_radius_truth_3p_reco_1p->SetMarkerSize(0.7);
  h_radius_truth_3p_reco_2p->SetMarkerSize(0.7);

  h_radius_truth_3p_reco_1p->GetYaxis()->SetRangeUser(0,1);
  h_radius_truth_3p_reco_1p->Draw("same:p");
  h_radius_truth_3p_reco_2p->Draw("same:p");
  h_radius_truth_3p_reco_3p->GetYaxis()->SetTitle("Efficiency");
  h_radius_truth_3p_reco_3p->Draw("same:p");
  
  ATLASLabel(0.55,0.85, "Simulation");
  
  TLegend* leg1 = new TLegend(0.55, 0.75, 0.95, 0.85);
  leg1->SetFillColor(0);
  leg1->SetFillStyle(0);
  leg1->SetBorderSize(0);
  leg1->AddEntry(h_radius_truth_3p_reco_1p, "reco 1-prong/truth 3-prong", "pl");
  leg1->AddEntry(h_radius_truth_3p_reco_2p, "reco 2-prong/truth 3-prong", "pl");
  leg1->AddEntry(h_radius_truth_3p_reco_3p, "reco 3-prong/truth 3-prong", "pl");
  leg1->Draw();
 
  return;
}
