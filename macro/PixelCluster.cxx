
#include <iostream>
#include <TH1.h>
#include <TH2.h>
#include <TFile.h>

#include <TTree.h>
#include <TText.h>
#include <TCanvas.h>
#include <TLegend.h>
#include <TLine.h>

void PixelCluster()
{
  std::string WorkingPoint;
  std::string path = "/afs/cern.ch/user/k/ktakeda/workspace/public/atlas-phys/AJ/WG1/SingleParticle/AnalysisArea/run/";
  //std::string file_name = "MyTauNtuple.10000events.FullG4_LongLived.outputs.root";
  std::string file_name = "MyTauNtuple.10000events.MC12G4.outputs.root";

  TFile* file = new TFile( (path+file_name).c_str(), "read");
  TTree* tree = (TTree*)file->Get("m_trueTauTree");

  
  Double_t Truth_tau_pt; 
  Double_t Truth_tau_eta; 
  Bool_t   Truth_tau_isLeptonic = false; 
  Bool_t   Truth_tau_isHadronic = false; 
  Int_t    Truth_tau_numTracks =0; 
  Bool_t   Truth_tau_isMatchedWithReco; 
  Double_t Truth_tau_decayRad = 0; 
  Bool_t   tt_truth_leptonic_electron = false; 
  Bool_t   tt_truth_leptonic_muon = false; 
  Bool_t   tt_truth_leptonic_tau = false; 
  Double_t tt_truth_neutrino_pt; 
  
  Int_t    Reco_tau_numTracks = 0; 
  Bool_t   Reco_tau_isSelected;
  Double_t Reco_tau_pt; 
  Double_t Reco_tau_eta; 
  Double_t Reco_tau_phi; 
  Double_t Reco_tau_e; 

  std::vector<unsigned int>* tt_numberOfContribPixelLayers      =0 ; //!
  std::vector<unsigned int>* tt_numberOfBLayerHits              =0 ; //!
  std::vector<unsigned int>* tt_numberOfPixelHits               =0 ; //!
  std::vector<unsigned int>* tt_numberOfSCTHits                 =0 ; //!
  std::vector<unsigned int>* tt_numberOfTRTHits                 =0 ; //!
  std::vector<unsigned int>* tt_numberOfBLayerSharedHits        =0 ; //!
  std::vector<unsigned int>* tt_numberOfPixelSharedHits         =0 ; //!
  std::vector<unsigned int>* tt_numberOfSCTSharedHits           =0 ; //!
  std::vector<unsigned int>* tt_numberOfTRTSharedHits           =0 ; //!
  std::vector<unsigned int>* tt_numberOfPixelHoles              =0 ; //!
  std::vector<unsigned int>* tt_numberOfSCTHoles                =0 ; //!
  std::vector<unsigned int>* tt_numberOfSCTDoubleHoles          =0 ; //!
  std::vector<unsigned int>* tt_numberOfTRTHoles                =0 ; //!
  std::vector<unsigned int>* tt_numberOfBLayerOutliers          =0 ; //!
  std::vector<unsigned int>* tt_numberOfPixelOutliers           =0 ; //!
  std::vector<unsigned int>* tt_numberOfSCTOutliers             =0 ; //!
  std::vector<unsigned int>* tt_numberOfTRTOutliers             =0 ; //!
  std::vector<unsigned int>* tt_numberOfPixelSpoiltHits         =0 ; //!
  std::vector<unsigned int>* tt_numberOfSCTSpoiltHits           =0 ; //!
  std::vector<unsigned int>* tt_expectBLayerHit                 =0 ; //!
  std::vector<unsigned int>* tt_expectInnermostPixelLayerHit    =0 ; //!
  std::vector<unsigned int>* tt_numberOfInnermostPixelLayerHits =0 ; //!
  std::vector<unsigned int>* tt_numberOfNextToInnermostPixelLayerHits =0 ; //!
  std::vector<unsigned int>* tt_numberOfGangedPixels            =0 ; //!
  std::vector<unsigned int>* tt_numberOfGangedFlaggedFakes      =0 ; //!
  std::vector<unsigned int>* tt_numberOfPixelDeadSensors        =0 ; //!
  std::vector<unsigned int>* tt_numberOfSCTDeadSensors          =0 ; //!
  std::vector<unsigned int>* tt_numberOfTRTDeadStraws           =0 ; //!

  // Pixel raw cluster
  std::vector<unsigned int>* Reco_track_numberOfPixelHits                     = 0 ;//!
  std::vector<unsigned int>* Reco_track_numberOfInnermostPixelHits       = 0 ;//!
  std::vector<unsigned int>* Reco_track_numberOfNextToInnermostPixelHits = 0 ;//!
  std::vector<unsigned int>* Reco_track_numberOfPixelL1Hits                   = 0 ;//!
  std::vector<unsigned int>* Reco_track_numberOfPixelL2Hits                   = 0 ;//!

  tree->SetBranchAddress("Reco_tau_pt",                        &Reco_tau_pt);
  tree->SetBranchAddress("Reco_tau_eta",                       &Reco_tau_eta);
  tree->SetBranchAddress("Reco_tau_phi",                       &Reco_tau_phi);
  tree->SetBranchAddress("Reco_tau_e",                         &Reco_tau_e);
  tree->SetBranchAddress("tt_truth_neutrino_pt",              &tt_truth_neutrino_pt);
  tree->SetBranchAddress("Truth_tau_isLeptonic",           &Truth_tau_isLeptonic);
  tree->SetBranchAddress("tt_truth_leptonic_electron",        &tt_truth_leptonic_electron);
  tree->SetBranchAddress("tt_truth_leptonic_muon",            &tt_truth_leptonic_muon);
  tree->SetBranchAddress("tt_truth_leptonic_tau",             &tt_truth_leptonic_tau);
  tree->SetBranchAddress("Truth_tau_isHadronic",           &Truth_tau_isHadronic);
  tree->SetBranchAddress("Truth_tau_isMatchedWithReco",                    &Truth_tau_isMatchedWithReco);
  tree->SetBranchAddress("Truth_tau_pt",                       &Truth_tau_pt);
  tree->SetBranchAddress("Truth_tau_eta",                      &Truth_tau_eta);
  tree->SetBranchAddress("Truth_tau_decayRad",             &Truth_tau_decayRad);
  tree->SetBranchAddress("Truth_tau_numTracks",                   &Truth_tau_numTracks);
  tree->SetBranchAddress("Reco_tau_numTracks",          &Reco_tau_numTracks);
  tree->SetBranchAddress("Reco_tau_isSelected",                  &Reco_tau_isSelected);

  TBranch* b_numberOfPizxelHits = 0;
  tree->SetBranchAddress("tt_numberOfContribPixelLayers",      &tt_numberOfContribPixelLayers);
  tree->SetBranchAddress("tt_numberOfBLayerHits",              &tt_numberOfBLayerHits       );
  tree->SetBranchAddress("tt_numberOfPixelHits",               &tt_numberOfPixelHits        );
  tree->SetBranchAddress("tt_numberOfSCTHits",                 &tt_numberOfSCTHits          );
  tree->SetBranchAddress("tt_numberOfTRTHits",                 &tt_numberOfTRTHits          );
  tree->SetBranchAddress("tt_numberOfBLayerSharedHits",        &tt_numberOfBLayerSharedHits        );
  tree->SetBranchAddress("tt_numberOfPixelSharedHits",         &tt_numberOfPixelSharedHits        );
  tree->SetBranchAddress("tt_numberOfSCTSharedHits",           &tt_numberOfSCTSharedHits        );
  tree->SetBranchAddress("tt_numberOfTRTSharedHits",           &tt_numberOfTRTSharedHits        );
  tree->SetBranchAddress("tt_numberOfPixelHoles",              &tt_numberOfPixelHoles        );
  tree->SetBranchAddress("tt_numberOfSCTHoles",                &tt_numberOfSCTHoles         );
  tree->SetBranchAddress("tt_numberOfSCTDoubleHoles",          &tt_numberOfSCTDoubleHoles        );
  tree->SetBranchAddress("tt_numberOfTRTHoles",                &tt_numberOfTRTHoles         );
  tree->SetBranchAddress("tt_numberOfBLayerOutliers",          &tt_numberOfBLayerOutliers        );
  tree->SetBranchAddress("tt_numberOfPixelOutliers",           &tt_numberOfPixelOutliers        );
  tree->SetBranchAddress("tt_numberOfSCTOutliers",             &tt_numberOfSCTOutliers        );
  tree->SetBranchAddress("tt_numberOfTRTOutliers",             &tt_numberOfTRTOutliers        );
  tree->SetBranchAddress("tt_numberOfPixelSpoiltHits",         &tt_numberOfPixelSpoiltHits        );
  tree->SetBranchAddress("tt_numberOfSCTSpoiltHits",           &tt_numberOfSCTSpoiltHits        );
  tree->SetBranchAddress("tt_expectBLayerHit",                 &tt_expectBLayerHit          );
  tree->SetBranchAddress("tt_expectInnermostPixelLayerHit",    &tt_expectInnermostPixelLayerHit              );
  tree->SetBranchAddress("tt_numberOfInnermostPixelLayerHits", &tt_numberOfInnermostPixelLayerHits              );
  tree->SetBranchAddress("tt_numberOfNextToInnermostPixelLayerHits", &tt_numberOfNextToInnermostPixelLayerHits              );
  tree->SetBranchAddress("tt_numberOfGangedPixels",            &tt_numberOfGangedPixels              );
  tree->SetBranchAddress("tt_numberOfGangedFlaggedFakes",      &tt_numberOfGangedFlaggedFakes              );
  tree->SetBranchAddress("tt_numberOfPixelDeadSensors",        &tt_numberOfPixelDeadSensors              );
  tree->SetBranchAddress("tt_numberOfSCTDeadSensors",          &tt_numberOfSCTDeadSensors              );
  tree->SetBranchAddress("tt_numberOfTRTDeadStraws",           &tt_numberOfTRTDeadStraws              );

  tree->SetBranchAddress("Reco_track_numberOfPixelHits",                     &Reco_track_numberOfPixelHits);
  tree->SetBranchAddress("Reco_track_numberOfInnermostPixelHits",            &Reco_track_numberOfInnermostPixelHits);
  tree->SetBranchAddress("Reco_track_numberOfNextToInnermostPixelHits",      &Reco_track_numberOfNextToInnermostPixelHits);
  tree->SetBranchAddress("Reco_track_numberOfPixelL1Hits",                   &Reco_track_numberOfPixelL1Hits);
  tree->SetBranchAddress("Reco_track_numberOfPixelL2Hits",                   &Reco_track_numberOfPixelL2Hits);

  //_//_//_//_//_//_//_//_//_//_//_//_//_//_//_//_//_//_//_//_//_//_//_//_ //_//_//_//_//_//_//_//_//_//_//_//_//_//_//_//_//_//_//_//_//_//_//_//
  //
  //                                  The decralation of histograms
  //
  //_//_//_//_//_//_//_//_//_//_//_//_//_//_//_//_//_//_//_//_//_//_//_//_ //_//_//_//_//_//_//_//_//_//_//_//_//_//_//_//_//_//_//_//_//_//_//_//
  /// truth momentum
  // (1) Tau momentum
  TH1D* h_truth_pt_all                      = new TH1D("h_truth_pt_all",";pT [GeV];Events",100, 0, 700);
  TH1D* h_truth_pt_hadronic                 = new TH1D("h_truth_pt_hadronic",";pT [GeV];Events",100, 0, 700);
  TH1D* h_truth_pt_leptonic                 = new TH1D("h_truth_pt_leptonic",";pT [GeV];Events",100, 0, 700);
  TH1D* h_truth_pt_electron                 = new TH1D("h_truth_pt_electron",";pT [GeV];Events",100, 0, 700);
  TH1D* h_truth_pt_muon                     = new TH1D("h_truth_pt_muon",";pT [GeV];Events",100, 0, 700);
  // (2) Neutrino momentum
  TH1D* h_neutrino_pt_all                   = new TH1D("h_neutrino_pt_all",";pT [GeV];Events",100, 0, 700);
  TH1D* h_neutrino_pt_hadronic              = new TH1D("h_neutrino_pt_hadronic",";pT [GeV];Events",100, 0, 700);
  TH1D* h_neutrino_pt_leptonic              = new TH1D("h_neutrino_pt_leptonic",";pT [GeV];Events",100, 0, 700);

  /// Reconstrusted momentum
  TH1D* h_Reco_tau_pt_all           = new TH1D("h_Reco_tau_pt_all",";pT [GeV];Events",100, 0, 700);
  TH1D* h_Reco_tau_pt_all_EleOLR    = new TH1D("h_Reco_tau_pt_all_EleOLR",";pT [GeV];Events",100, 0, 700);
  TH1D* h_Reco_tau_pt_all_MuonOLR   = new TH1D("h_Reco_tau_pt_all_MuonOLR",";pT [GeV];Events",100, 0, 700);
  TH1D* h_Reco_tau_pt_hadronic      = new TH1D("h_Reco_tau_pt_hadronic",";pT [GeV];Events",100, 0, 700);
  TH1D* h_Reco_tau_pt_leptonic      = new TH1D("h_Reco_tau_pt_leptonic",";pT [GeV];Events",100, 0, 700);

  /// _CheckAcceptBeforeAfter
  TH1D*  h_Reco_tau_pt_all_afterSelection      = new TH1D("h_Reco_tau_pt_all_afterSelection","",100,0,700);
  TH1D*  h_Reco_tau_pt_hadronic_afterSelection = new TH1D("h_Reco_tau_pt_hadronic_afterSelection","",100,0,700);
  TH1D*  h_Reco_tau_pt_leptonic_afterSelection = new TH1D("h_Reco_tau_pt_leptonic_afterSelection","",100,0,700);
  TH1D*  h_Reco_tau_pt_electron_afterSelection = new TH1D("h_Reco_tau_pt_electron_afterSelection","",100,0,700);
  TH1D*  h_Reco_tau_pt_muon_afterSelection     = new TH1D("h_Reco_tau_pt_muon_afterSelection","",100,0,700);
  TH1D* h_Reco_tau_pt_muon          = new TH1D("h_Reco_tau_pt_muon",";pT [GeV];Events",100, 0, 700);
  TH1D* h_Reco_tau_pt_electron      = new TH1D("h_Reco_tau_pt_electron",";pT [GeV];Events",100, 0, 700);

  /// _DecayRadius
  Double_t xbin[] = {0,10,20,30,40,50,60,70,80,90,100,120,140,160,180,200,220,250};
  TH1D* h_truth_decayradius                      = new TH1D("h_truth_decayradius",";decay radius [mm];Events",                     17,xbin);
  TH1D* h_truth_decayradius_hadronic_all         = new TH1D("h_truth_decayradius_hadronic_all",";decay radius [mm];Events",        17,xbin);
  TH1D* h_truth_decayradius_hadronic_1p          = new TH1D("h_truth_decayradius_hadronic_1p",";decay radius [mm];Events",         17,xbin);
  TH1D* h_truth_decayradius_hadronic_3p          = new TH1D("h_truth_decayradius_hadronic_3p",";decay radius [mm];Events",         17,xbin);
  TH1D* h_truth_decayradius_hadronic_1p_reco_1p  = new TH1D("h_truth_decayradius_hadronic_1p_reco_1p",";decay radius [mm];Events", 17,xbin);
  TH1D* h_truth_decayradius_hadronic_3p_reco_3p  = new TH1D("h_truth_decayradius_hadronic_3p_reco_3p",";decay radius [mm];Events", 17,xbin);

  /// _DecayRadius_vs_NumberOfXXX
  TH1D* h_numberOfPixelHits[2][5];
  TH1D* h_numberOfSCTHits[2][5];
  TH1D* h_numberOfRawPixelHits[2][5];
  for(int iProng = 0; iProng<2; iProng++){
    for(int iDecayRegion =0; iDecayRegion<5; iDecayRegion++){
      h_numberOfPixelHits[iProng][iDecayRegion]    = new TH1D( Form("h_numberOfPixelHits_%d_Region%d",    iProng, iDecayRegion), "", 11,-0.5, 10.5);
      h_numberOfSCTHits[iProng][iDecayRegion]      = new TH1D( Form("h_numberOfSCTHits_%d_Region%d",      iProng, iDecayRegion), "", 19,-0.5, 18.5);
      h_numberOfRawPixelHits[iProng][iDecayRegion] = new TH1D( Form("h_numberOfRawPixelHits_%d_Region%d", iProng, iDecayRegion), "", 11,-0.5, 10.5);
    }
  }

  // _numberOfXXX
  TH2D* h_numberOfPixelHitsDecayRegion                     = new TH2D("h_numberOfPixelHitsDecayRegion","", 50, 0, 250, 11, -0.5, 10.5);
  TH2D* h_numberOfPixelHits_InnerMost_vs_DecayRegion       = new TH2D("h_numberOfPixelHits_InnerMost_vs_DecayRegion","", 50, 0, 250, 3, -0.5, 2.5);
  TH2D* h_numberOfPixelHits_NextToInnerMost_vs_DecayRegion = new TH2D("h_numberOfPixelHits_NextToInnerMost_vs_DecayRegion","", 50, 0, 250, 3, -0.5, 2.5);
  TH2D* h_numberOfSCTHits_vs_DecayRegion                   = new TH2D("h_numberOfSCTHits_vs_DecayRegion","",   50, 0, 250, 18, -0.5, 17.5);

  // _numberOfPixelHits_DecayRadius
  TH2D* h_numberOfPixelHitsDecayRadius[2][5];
  for (int iTruthProng=0; iTruthProng<2; iTruthProng++){
    for (int iRecoProng=0; iRecoProng<5; iRecoProng++){
      h_numberOfPixelHitsDecayRadius[iTruthProng][iRecoProng] = new TH2D( Form("h_numberOfPixelHitsDecayRadius_truth%d_reco%d", iTruthProng, iRecoProng), "", 50, 0, 250, 11, -0.5, 10.5);
    }
  }

  // _numberOfPixelHits_Eta
  TH2D* h_numberOfPixelHitsEta[2][6];
  for (int iTruthProng=0; iTruthProng<2; iTruthProng++){
    for (int iRegion=0; iRegion<6; iRegion++){
      h_numberOfPixelHitsEta[iTruthProng][iRegion] = new TH2D( Form("h_numberOfPixelHitsEta_truth%d_Region%d", iTruthProng, iRegion), "", 50, -2.5, 2.5, 11, -0.5, 10.5);
    }
  }

  // _DecayRadius_EachProng_Efficiency
  TH1D* h_number_of_tracks[2][8];
  double xBinning [14] = {0, 10, 20, 30, 40, 50, 60, 70, 80, 90, 100, 150, 200, 250};
  // [2] : 0 for 1-prong, 1 for 3-prong 
  // [8] : 0 ~ 4 for reco-prong, 
  //       5 for not truth matched events,
  //       6 for the truth matched reco-tau
  //       7 for the truth tau, 
  for ( int iProng=0; iProng<2; iProng++){
    for ( int iRecoProng=0; iRecoProng<8; iRecoProng++){
      h_number_of_tracks[iProng][iRecoProng] = new TH1D(Form("h_number_of_tracks_%d_%d", iProng, iRecoProng), "", 13, xBinning);
    }
  }

  //_//_//_//_//_//_//_//_//_//_//_//_//_//_//_//_//_//_//_//_//_//_//_//_ //_//_//_//_//_//_//_//_//_//_//_//_//_//_//_//_//_//_//_//_//_//_//_//
  //
  //                             Main Logic 
  //
  //_//_//_//_//_//_//_//_//_//_//_//_//_//_//_//_//_//_//_//_//_//_//_//_ //_//_//_//_//_//_//_//_//_//_//_//_//_//_//_//_//_//_//_//_//_//_//_//
  std::cout << "The total number of events : " << tree->GetEntries() << std::endl;

  for(int iEntry=0; iEntry< tree->GetEntries(); iEntry++){
    tree->GetEntry(iEntry);

    //if( TMath::Abs(Truth_tau_eta) > 1.03 ) continue; // Barrel
    //if( TMath::Abs(Truth_tau_eta) < 1.03 ) continue; // Endcap

    h_truth_pt_all          ->Fill(Truth_tau_pt/1000.);
    h_neutrino_pt_all       ->Fill(tt_truth_neutrino_pt/1000.);
    h_truth_decayradius     ->Fill(Truth_tau_decayRad);
    
    if( Truth_tau_isLeptonic ){
      h_truth_pt_leptonic   ->Fill(Truth_tau_pt/1000.);
      h_neutrino_pt_leptonic->Fill(tt_truth_neutrino_pt/1000.);
      if( tt_truth_leptonic_electron )
        h_truth_pt_electron->Fill(Truth_tau_pt/1000.);

      if( tt_truth_leptonic_muon )
        h_truth_pt_muon->Fill(Truth_tau_pt/1000.);
    }

    if( Truth_tau_isHadronic ){
      h_truth_pt_hadronic->Fill(Truth_tau_pt/1000.);
      h_neutrino_pt_hadronic->Fill(tt_truth_neutrino_pt/1000.);
      h_truth_decayradius_hadronic_all->Fill(Truth_tau_decayRad);
      
      if( Truth_tau_numTracks == 1 ) {
        h_truth_decayradius_hadronic_1p->Fill(Truth_tau_decayRad);
        if( Truth_tau_isMatchedWithReco  && Reco_tau_numTracks == 1) 
          h_truth_decayradius_hadronic_1p_reco_1p->Fill(Truth_tau_decayRad);
      }
      if( Truth_tau_numTracks == 3 ) {
        h_truth_decayradius_hadronic_3p->Fill(Truth_tau_decayRad);
        if( Truth_tau_isMatchedWithReco  && Reco_tau_numTracks == 3) 
          h_truth_decayradius_hadronic_3p_reco_3p->Fill(Truth_tau_decayRad);
      }
      // -------------------------------------
      //
      //           Efficiency
      //   _DecayRadius_EachProng_Efficiency
      //
      // -------------------------------------
      if ( Truth_tau_isHadronic ) {
        Int_t truth_nProng = 999;
        if ( Truth_tau_numTracks == 1 ) truth_nProng = 0; 
        if ( Truth_tau_numTracks == 3 ) truth_nProng = 1; 

        h_number_of_tracks[truth_nProng][7]->Fill( Truth_tau_decayRad );

        //if( Truth_tau_isMatchedWithReco && Reco_tau_isSelected ) 
        if( Truth_tau_isMatchedWithReco ) {
          // Reco-tau found
          if (  Reco_tau_numTracks == 0 ) h_number_of_tracks[truth_nProng][0]->Fill( Truth_tau_decayRad );
          if (  Reco_tau_numTracks == 1 ) h_number_of_tracks[truth_nProng][1]->Fill( Truth_tau_decayRad );
          if (  Reco_tau_numTracks == 2 ) h_number_of_tracks[truth_nProng][2]->Fill( Truth_tau_decayRad );
          if (  Reco_tau_numTracks == 3 ) h_number_of_tracks[truth_nProng][3]->Fill( Truth_tau_decayRad );
          if (  Reco_tau_numTracks == 4 ) h_number_of_tracks[truth_nProng][4]->Fill( Truth_tau_decayRad );
          h_number_of_tracks[truth_nProng][6]->Fill( Truth_tau_decayRad );
        } else {
          // Reco-tau not found
          h_number_of_tracks[truth_nProng][5]->Fill( Truth_tau_decayRad );
        }
      }
    }

    /// Reconstrustion momentum
    if ( !Truth_tau_isMatchedWithReco ) continue;
    // -------------------------------------------
    //
    //       The number of pixel/SCT hits       
    //
    // -------------------------------------------
    if ( Truth_tau_isHadronic ) {
      Int_t xBin=0;
      Int_t xBinAll=5;
      if(                                  Truth_tau_decayRad < 33.25 ) xBin = 0;
      if( 33.25 < Truth_tau_decayRad && Truth_tau_decayRad < 50.5  ) xBin = 1;
      if( 50.5  < Truth_tau_decayRad && Truth_tau_decayRad < 88.5  ) xBin = 2;
      if( 88.5  < Truth_tau_decayRad && Truth_tau_decayRad < 122.5 ) xBin = 3;
      if( 122.5 < Truth_tau_decayRad                                  ) xBin = 4;

      Int_t truth_nProng = 999;
      if ( Truth_tau_numTracks == 1 ) truth_nProng = 0;
      if ( Truth_tau_numTracks == 3 ) truth_nProng = 1;

      for ( unsigned int index=0; index< tt_numberOfPixelHits->size(); index++){
        h_numberOfPixelHits[truth_nProng][xBin]  ->Fill( tt_numberOfPixelHits->at(index));
        h_numberOfSCTHits[truth_nProng][xBin]    ->Fill( tt_numberOfSCTHits->at(index));

        if ( Truth_tau_numTracks == 3 && Reco_tau_numTracks == 3){
          h_numberOfPixelHitsDecayRegion->Fill( Truth_tau_decayRad, tt_numberOfPixelHits->at(index));
          h_numberOfPixelHits_InnerMost_vs_DecayRegion->Fill( Truth_tau_decayRad, tt_numberOfInnermostPixelLayerHits->at(index));
          h_numberOfPixelHits_NextToInnerMost_vs_DecayRegion->Fill( Truth_tau_decayRad, tt_numberOfNextToInnermostPixelLayerHits->at(index));
          h_numberOfSCTHits_vs_DecayRegion->Fill( Truth_tau_decayRad, tt_numberOfSCTHits->at(index));
        }
      }
      if ( Truth_tau_numTracks == 1 && Reco_tau_isSelected ){
        for( unsigned int iTrack=0; iTrack<tt_numberOfPixelHits->size(); iTrack++){
          h_numberOfPixelHitsDecayRadius[truth_nProng][Reco_tau_numTracks] ->Fill( Truth_tau_decayRad, tt_numberOfPixelHits->at(iTrack) );
          h_numberOfPixelHitsEta[truth_nProng][xBin]                             ->Fill( Truth_tau_eta,          tt_numberOfPixelHits->at(iTrack) );
          h_numberOfPixelHitsEta[truth_nProng][xBinAll]                          ->Fill( Truth_tau_eta,          tt_numberOfPixelHits->at(iTrack) );
        }
      }
      if ( Truth_tau_numTracks == 3 && Reco_tau_isSelected ){
        for( unsigned int iTrack=0; iTrack<tt_numberOfPixelHits->size(); iTrack++){
          h_numberOfPixelHitsDecayRadius[truth_nProng][Reco_tau_numTracks] ->Fill( Truth_tau_decayRad, tt_numberOfPixelHits->at(iTrack) );
          h_numberOfPixelHitsEta[truth_nProng][xBin]                             ->Fill( Truth_tau_eta,          tt_numberOfPixelHits->at(iTrack) );
          h_numberOfPixelHitsEta[truth_nProng][xBinAll]                          ->Fill( Truth_tau_eta,          tt_numberOfPixelHits->at(iTrack) );
        }
      }
      // --------------------------
      //      Pixel raw data
      // --------------------------
      for ( unsigned int index=0; index< Reco_track_numberOfPixelHits->size(); index++){
        h_numberOfRawPixelHits[truth_nProng][xBin]  ->Fill( Reco_track_numberOfPixelHits->at(index));
      }
    }

    h_Reco_tau_pt_all->Fill(Reco_tau_pt/1000.);

    if( Truth_tau_isLeptonic )
      h_Reco_tau_pt_leptonic->Fill(Reco_tau_pt/1000.);

    if( Truth_tau_isHadronic )
      h_Reco_tau_pt_hadronic->Fill(Reco_tau_pt/1000.);

    /// To divide leptonic decay mode into e, mu, tau mode.
    if ( Truth_tau_isLeptonic ) {
      if( tt_truth_leptonic_electron)
        h_Reco_tau_pt_electron->Fill(Reco_tau_pt/1000.);
      if( tt_truth_leptonic_muon)
        h_Reco_tau_pt_muon->Fill(Reco_tau_pt/1000.);
    }
  }
  //_//_//_//_//_//_//_//_//_//_//_//_//_//_//_//_//_//_//_//_//_//_//_//_ //_//_//_//_//_//_//_//_//_//_//_//_//_//_//_//_//_//_//_//_//_//_//_//
  //
  //                             Draw histograms
  //
  //_//_//_//_//_//_//_//_//_//_//_//_//_//_//_//_//_//_//_//_//_//_//_//_ //_//_//_//_//_//_//_//_//_//_//_//_//_//_//_//_//_//_//_//_//_//_//_//
#ifdef _Momentum_All 
  // ----------------------------------------------------------------------------------------
  //  This is the basic momentum distribution. 
  //  I can compare the truth visible momentum and reco-tau momentum.
  // ----------------------------------------------------------------------------------------
  TCanvas* MomentumAll = new TCanvas("MomentumAll","MomentumAll");
  h_truth_pt_all    = (TH1D*)h_truth_pt_all->Clone();
  h_Reco_tau_pt_all  = (TH1D*)h_Reco_tau_pt_all->Clone();

  h_Reco_tau_pt_all->SetLineColor(kMagenta);

  h_truth_pt_all->Draw();
  h_Reco_tau_pt_all->Draw("same");

  ATLASLabel(0.2,0.8,"Simulation");
  TLegend* legMomentumAll = new TLegend(0.2,0.65,0.6,0.8);
  legMomentumAll->SetFillColor(0);
  legMomentumAll->SetFillStyle(0);
  legMomentumAll->SetBorderSize(0);
  legMomentumAll->AddEntry(h_truth_pt_all,   "truth visible energy", "l");
  legMomentumAll->AddEntry(h_Reco_tau_pt_all, "reconsructed energy",  "l");
  legMomentumAll->Draw();
#endif

#ifdef _Momentum_truth 
  // ----------------------------------------------------------------------------------------
  //  I can see how the truth tau decays to each channel. 
  //  This is the visible momentum.
  // ----------------------------------------------------------------------------------------
  TCanvas* Truth_MomentumComponent = new TCanvas("Truth_MomentumComponent","Truth_MomentumComponent");
  h_truth_pt_all      = (TH1D*)h_truth_pt_all      ->Clone();
  h_truth_pt_hadronic = (TH1D*)h_truth_pt_hadronic ->Clone();
  h_truth_pt_electron = (TH1D*)h_truth_pt_electron ->Clone();
  h_truth_pt_muon     = (TH1D*)h_truth_pt_muon     ->Clone();

  h_truth_pt_hadronic->SetLineColor(kRed);
  h_truth_pt_electron->SetLineColor(kBlue);
  h_truth_pt_muon->SetLineColor(kGreen);

  h_truth_pt_all->GetYaxis()->SetRangeUser(0,311);
  h_truth_pt_all->Draw();
  h_truth_pt_hadronic->Draw("same");
  h_truth_pt_electron->Draw("same");
  h_truth_pt_muon->Draw("same");

  Truth_MomentumComponent->RedrawAxis();

  ATLASLabel(0.2,0.8,"Simulation");
  TLegend* legTruth_MomentumComponent = new TLegend(0.2,0.65,0.6,0.8);
  legTruth_MomentumComponent->SetFillColor(0);
  legTruth_MomentumComponent->SetFillStyle(0);
  legTruth_MomentumComponent->SetBorderSize(0);
  legTruth_MomentumComponent->AddEntry(h_truth_pt_all,      Form("Truth tau    : %0.f events", h_truth_pt_all->Integral()), "l");
  legTruth_MomentumComponent->AddEntry(h_truth_pt_hadronic, Form("hadronic tau : %0.f events", h_truth_pt_hadronic->Integral()), "l");
  legTruth_MomentumComponent->AddEntry(h_truth_pt_electron, Form("-> electron  : %0.f events", h_truth_pt_electron->Integral()), "l");
  legTruth_MomentumComponent->AddEntry(h_truth_pt_muon,     Form("-> muon      : %0.f events", h_truth_pt_muon->Integral()), "l");
  legTruth_MomentumComponent->Draw();
#endif

#ifdef _Momentum_Leptonic 
  // ----------------------------------------------------------------------------------------
  //  This is a mometum distribution of the leptonic decay.
  // ----------------------------------------------------------------------------------------
  TCanvas* MomentumLeptonic = new TCanvas("MomentumLeptonic","MomentumLeptonic");
  h_truth_pt_leptonic->GetYaxis()->SetRangeUser(0,315);

  h_truth_pt_leptonic->SetLineColor(kRed);

  h_truth_pt_leptonic->Draw();
  h_Reco_tau_pt_leptonic->Draw("same");

  ATLASLabel(0.2,0.8,"Simulation");
  TLegend* legMomentumLeptonic = new TLegend(0.2,0.65,0.6,0.8);
  legMomentumLeptonic->SetFillColor(0);
  legMomentumLeptonic->SetFillStyle(0);
  legMomentumLeptonic->SetBorderSize(0);
  legMomentumLeptonic->AddEntry(h_truth_pt_leptonic, "truth leptonic", "l");
  legMomentumLeptonic->AddEntry(h_Reco_tau_pt_leptonic,  "reco leptonic", "l");
  legMomentumLeptonic->Draw();
#endif 

#ifdef _Momentum_Hadronic
  // ----------------------------------------------------------------------------------------
  //  I can compare the truth visible momentum and reco-tau momentum in the hadronic decay.
  // ----------------------------------------------------------------------------------------
  TCanvas* Momentum_Hadronic = new TCanvas("Momentum_Hadronic","Momentum_Hadronic");
  h_truth_pt_hadronic = (TH1D*)h_truth_pt_hadronic->Clone();
  h_Reco_tau_pt_hadronic = (TH1D*)h_Reco_tau_pt_hadronic->Clone();

  h_truth_pt_hadronic->GetYaxis()->SetRangeUser(0,311);
  h_truth_pt_hadronic->Draw();
  h_Reco_tau_pt_hadronic->Draw("same");

  ATLASLabel(0.2,0.8,"Simulation");
  TLegend* legMomentumHadronic = new TLegend(0.2,0.65,0.6,0.8);
  legMomentumHadronic->SetFillColor(0);
  legMomentumHadronic->SetFillStyle(0);
  legMomentumHadronic->SetBorderSize(0);
  legMomentumHadronic->AddEntry(h_truth_pt_hadronic,      Form("truth hadronic : %0.f events", h_truth_pt_hadronic->Integral()), "l");
  legMomentumHadronic->AddEntry(h_Reco_tau_pt_hadronic, Form("reco hadronic  : %0.f events", h_Reco_tau_pt_hadronic->Integral()), "l");
  legMomentumHadronic->Draw();
#endif

#ifdef _Momentum_reco
  // -----------------------------------------------------------------
  //  I can see how reco-tau decays in each channel.
  // -----------------------------------------------------------------
  TCanvas* Momentum_reco = new TCanvas("Momentum_reco","Momentum_reco");
  /// Clone
  h_Reco_tau_pt_all         = (TH1D*)h_Reco_tau_pt_all->Clone();
  h_Reco_tau_pt_leptonic    = (TH1D*)h_Reco_tau_pt_leptonic->Clone();
  h_Reco_tau_pt_hadronic    = (TH1D*)h_Reco_tau_pt_hadronic->Clone();

  h_Reco_tau_pt_all->GetYaxis()->SetRangeUser(0,311);

  /// SetLineColor 
  h_Reco_tau_pt_all->SetLineColor(kBlack);
  h_Reco_tau_pt_leptonic->SetLineColor(kBlue);
  h_Reco_tau_pt_hadronic->SetLineColor(kRed);

  /// Draw
  h_Reco_tau_pt_all->Draw();
  h_Reco_tau_pt_leptonic->Draw("same");
  h_Reco_tau_pt_hadronic->Draw("same");

  /// Integral 
  h_Reco_tau_pt_all->Integral();
  h_Reco_tau_pt_leptonic->Draw("same");
  h_Reco_tau_pt_hadronic->Draw("same");

  /// Legend
  ATLASLabel(0.2,0.85, "Simulation");

  TLegend* legMomentum_reco = new TLegend(0.2, 0.7, 0.7, 0.8);
  legMomentum_reco->SetFillColor(0);
  legMomentum_reco->SetBorderSize(0);
  legMomentum_reco->AddEntry(h_Reco_tau_pt_all,      Form("reco all : %.0f events", h_Reco_tau_pt_all->Integral() ), "lp");
  legMomentum_reco->AddEntry(h_Reco_tau_pt_hadronic, Form("reco (hadronic matched) : %.0f events", h_Reco_tau_pt_hadronic->Integral()), "l");
  legMomentum_reco->AddEntry(h_Reco_tau_pt_leptonic, Form("reco (leptonic matched) : %.0f events", h_Reco_tau_pt_leptonic->Integral()), "l");
  legMomentum_reco->Draw();
#endif

#ifdef _Momentum_reco_ElectronMuonRemoval
  // -----------------------------------------------------------------
  TCanvas* _Momentum_reco_ElectronMuonRemoval = new TCanvas("_Momentum_reco_ElectronMuonRemoval","_Momentum_reco_ElectronMuonRemoval");
  h_Reco_tau_pt_all          = (TH1D*)h_Reco_tau_pt_all->Clone();
  h_Reco_tau_pt_all_MuonOLR  = (TH1D*)h_Reco_tau_pt_all_MuonOLR->Clone();
  h_Reco_tau_pt_all_EleOLR   = (TH1D*)h_Reco_tau_pt_all_EleOLR->Clone();

  h_Reco_tau_pt_all_MuonOLR->SetFillStyle(3444);
  h_Reco_tau_pt_all_MuonOLR->SetFillColor(kRed);

  h_Reco_tau_pt_all_EleOLR->SetFillStyle(3445);
  h_Reco_tau_pt_all_EleOLR->SetFillColor(kBlue);

  h_Reco_tau_pt_all->Draw();
  h_Reco_tau_pt_all_MuonOLR->Draw("same");
  h_Reco_tau_pt_all_EleOLR->Draw("same");
#endif

#ifdef _DecayRadius 
  // -------------------------------------------------------------------------
  //  I can see the decay radius distribution.
  // -------------------------------------------------------------------------
  TCanvas* DecayRadius = new TCanvas("DecayRadius","DecayRadius");
  DecayRadius->SetLogy();
  // Clone
  //h_truth_decayradius          = (TH1D*)h_truth_decayradius->Clone();
  h_truth_decayradius_hadronic_all        = (TH1D*)h_truth_decayradius_hadronic_all->Clone();
  h_truth_decayradius_hadronic_1p         = (TH1D*)h_truth_decayradius_hadronic_1p->Clone();
  h_truth_decayradius_hadronic_3p         = (TH1D*)h_truth_decayradius_hadronic_3p->Clone();
  h_truth_decayradius_hadronic_1p_reco_1p = (TH1D*)h_truth_decayradius_hadronic_1p_reco_1p->Clone();
  h_truth_decayradius_hadronic_3p_reco_3p = (TH1D*)h_truth_decayradius_hadronic_3p_reco_3p->Clone();

  // Decorate
  h_truth_decayradius_hadronic_all->SetMarkerSize(0.7);
  h_truth_decayradius_hadronic_1p->SetMarkerSize(0.7);
  h_truth_decayradius_hadronic_3p->SetMarkerSize(0.7);
  h_truth_decayradius_hadronic_1p_reco_1p->SetMarkerSize(0.7);
  h_truth_decayradius_hadronic_3p_reco_3p->SetMarkerSize(0.7);
  h_truth_decayradius_hadronic_1p_reco_1p->SetMarkerStyle(32);
  h_truth_decayradius_hadronic_3p_reco_3p->SetMarkerStyle(32);

  h_truth_decayradius_hadronic_all->SetMarkerColor(kBlack);
  h_truth_decayradius_hadronic_1p->SetMarkerColor(kRed);
  h_truth_decayradius_hadronic_3p->SetMarkerColor(kBlue);
  h_truth_decayradius_hadronic_1p_reco_1p->SetMarkerColor(kRed);
  h_truth_decayradius_hadronic_3p_reco_3p->SetMarkerColor(kBlue);

  // Draw
  //h_truth_decayradius->Draw("p");
  h_truth_decayradius_hadronic_all->Draw("same:p");
  h_truth_decayradius_hadronic_1p->Draw("same:p");
  h_truth_decayradius_hadronic_3p->Draw("same:p");
  h_truth_decayradius_hadronic_1p_reco_1p->Draw("same:p");
  h_truth_decayradius_hadronic_3p_reco_3p->Draw("same:p");

  //ATLASLabel(0.6,0.8,"Simulation");
  TLegend* legDecayRadius = new TLegend(0.55,0.7,0.85,0.9);
  legDecayRadius->SetFillColor(0);
  legDecayRadius->SetFillStyle(0);
  legDecayRadius->SetBorderSize(0);
  legDecayRadius->SetTextSize(0.03);
  legDecayRadius->AddEntry(h_truth_decayradius_hadronic_all,         Form("truth hadronic : %0.f events",   h_truth_decayradius_hadronic_all->Integral()),        "lp");
  legDecayRadius->AddEntry(h_truth_decayradius_hadronic_1p,          Form("1-prong : %0.f events",          h_truth_decayradius_hadronic_1p->Integral()),         "lp");
  legDecayRadius->AddEntry(h_truth_decayradius_hadronic_3p,          Form("3-prong : %0.f events",          h_truth_decayradius_hadronic_3p->Integral()),         "lp");
  legDecayRadius->AddEntry(h_truth_decayradius_hadronic_1p_reco_1p,  Form("reco 1-prong : %0.f events", h_truth_decayradius_hadronic_1p_reco_1p->Integral()), "lp");
  legDecayRadius->AddEntry(h_truth_decayradius_hadronic_3p_reco_3p,  Form("reco 3-prong : %0.f events", h_truth_decayradius_hadronic_3p_reco_3p->Integral()), "lp");
  legDecayRadius->Draw();

  //Double_t ymax = h_truth_decayradius_hadronic_all->GetYaxis()->GetXmax();
  Double_t ymax = 3500;
  std::cout << ymax << std::endl;
  TLine* IBL = new TLine(33.25, 0, 33.25, ymax);
  IBL->SetLineWidth(2);
  IBL->SetLineStyle(9);
  IBL->SetLineColor(kBlack);
  IBL->Draw("same");

  TLine* Blayer = new TLine(50.5, 0, 50.5, ymax);
  Blayer->SetLineWidth(2);
  Blayer->SetLineStyle(9);
  Blayer->SetLineColor(kBlack);
  Blayer->Draw("same");

  TLine* Pix2 = new TLine(88.5, 0, 88.5, ymax);
  Pix2->SetLineWidth(2);
  Pix2->SetLineStyle(9);
  Pix2->SetLineColor(kBlack);
  Pix2->Draw("same");

  TLine* Pix3 = new TLine(122.5, 0, 122.5, ymax);
  Pix3->SetLineWidth(2);
  Pix3->SetLineStyle(9);
  Pix3->SetLineColor(kBlack);
  Pix3->Draw("same");

  DecayRadius->SaveAs("DecayRadius.pdf");
#endif



#ifdef _CheckAcceptBeforeAfter // -----------------------------------------------------------------
  h_Reco_tau_pt_all      = (TH1D*)h_Reco_tau_pt_all->Clone();
  h_Reco_tau_pt_hadronic = (TH1D*)h_Reco_tau_pt_hadronic->Clone();
  h_Reco_tau_pt_leptonic = (TH1D*)h_Reco_tau_pt_leptonic->Clone();
  h_Reco_tau_pt_electron = (TH1D*)h_Reco_tau_pt_electron->Clone();
  h_Reco_tau_pt_muon     = (TH1D*)h_Reco_tau_pt_muon->Clone();

  h_Reco_tau_pt_all_afterSelection      = (TH1D*)h_Reco_tau_pt_all_afterSelection->Clone();
  h_Reco_tau_pt_hadronic_afterSelection = (TH1D*)h_Reco_tau_pt_hadronic_afterSelection->Clone();
  h_Reco_tau_pt_leptonic_afterSelection = (TH1D*)h_Reco_tau_pt_leptonic_afterSelection->Clone();
  h_Reco_tau_pt_electron_afterSelection = (TH1D*)h_Reco_tau_pt_electron_afterSelection->Clone();
  h_Reco_tau_pt_muon_afterSelection     = (TH1D*)h_Reco_tau_pt_muon_afterSelection->Clone();

  h_Reco_tau_pt_all->SetLineColor(kBlack);
  h_Reco_tau_pt_hadronic->SetLineColor(kBlack);
  h_Reco_tau_pt_leptonic->SetLineColor(kBlack);
  h_Reco_tau_pt_electron->SetLineColor(kBlack);
  h_Reco_tau_pt_muon->SetLineColor(kBlack);

  h_Reco_tau_pt_all_afterSelection->SetLineColor(kMagenta);
  h_Reco_tau_pt_hadronic_afterSelection->SetLineColor(kMagenta);
  h_Reco_tau_pt_leptonic_afterSelection->SetLineColor(kMagenta);
  h_Reco_tau_pt_electron_afterSelection->SetLineColor(kMagenta);
  h_Reco_tau_pt_muon_afterSelection->SetLineColor(kMagenta);

  TCanvas* Reco_All_BeforeAfterAccept = new TCanvas("Reco_All_BeforeAfterAccept","Reco_All_BeforeAfterAccept");
  h_Reco_tau_pt_all->GetYaxis()->SetRangeUser(0,311);
  h_Reco_tau_pt_all->Draw("same");
  h_Reco_tau_pt_all_afterSelection->Draw("same");
  /// Legend
  ATLASLabel(0.2,0.85, "Simulation");

  TCanvas* Reco_hadronic_BeforeAfterAccept = new TCanvas("Reco_hadronic_BeforeAfterAccept","Reco_hadronic_BeforeAfterAccept");
  h_Reco_tau_pt_hadronic->GetYaxis()->SetRangeUser(0,311);
  h_Reco_tau_pt_hadronic->Draw("same");
  h_Reco_tau_pt_hadronic_afterSelection->Draw("same");
  std::cout << "h_Reco_tau_pt_hadronic_afterSelection : " << h_Reco_tau_pt_hadronic_afterSelection->Integral() << std::endl;
  /// Legend
  ATLASLabel(0.2,0.85, "Simulation");

  TCanvas* Reco_leptonic_BeforeAfterAccept = new TCanvas("Reco_leptonic_BeforeAfterAccept","Reco_leptonic_BeforeAfterAccept");
  h_Reco_tau_pt_leptonic->GetYaxis()->SetRangeUser(0,311);
  h_Reco_tau_pt_leptonic->Draw("same");
  h_Reco_tau_pt_leptonic_afterSelection->Draw("same");
  /// Legend
  ATLASLabel(0.2,0.85, "Simulation");

  // 
  TCanvas* Reco_electron_BeforeAfterAccept = new TCanvas("Reco_electron_BeforeAfterAccept","Reco_electron_BeforeAfterAccept");
  h_Reco_tau_pt_electron->GetYaxis()->SetRangeUser(0,311);
  h_Reco_tau_pt_electron->Draw("same");
  h_Reco_tau_pt_electron_afterSelection->Draw("same");
  /// Legend
  ATLASLabel(0.2,0.85, "Simulation");

  TCanvas* Reco_muon_BeforeAfterAccept = new TCanvas("Reco_muon_BeforeAfterAccept","Reco_muon_BeforeAfterAccept");
  h_Reco_tau_pt_muon->GetYaxis()->SetRangeUser(0,311);
  h_Reco_tau_pt_muon->Draw("same");
  h_Reco_tau_pt_muon_afterSelection->Draw("same");
  /// Legend
  ATLASLabel(0.2,0.85, "Simulation");
#endif

#ifdef _Momentum_truth_neutritno
  // ----------------------------------------------------------------------------------------------
  //  
  // ----------------------------------------------------------------------------------------------
  TCanvas* _Momentum_truth_neutritno = new TCanvas("_Momentum_truth_neutritno","_Momentum_truth_neutritno");
  h_truth_pt_all         = (TH1D*)h_truth_pt_all->Clone();
  h_neutrino_pt_all      = (TH1D*)h_neutrino_pt_all->Clone();
  h_neutrino_pt_hadronic = (TH1D*)h_neutrino_pt_hadronic->Clone();
  h_neutrino_pt_leptonic = (TH1D*)h_neutrino_pt_leptonic->Clone();

  h_neutrino_pt_all->SetLineColor(kMagenta);
  h_neutrino_pt_hadronic->SetFillStyle(3445);
  h_neutrino_pt_hadronic->SetLineColor(kOrange+1);
  h_neutrino_pt_leptonic->SetFillStyle(3454);
  h_neutrino_pt_leptonic->SetLineColor(kAzure+1);

  h_truth_pt_all->Draw();
  h_neutrino_pt_all->Draw("same");
  h_neutrino_pt_hadronic->Draw("same");
  h_neutrino_pt_leptonic->Draw("same");

  ATLASLabel(0.2,0.85, "Simulation");
  TLegend* leg_Momentum_truth_neutritno = new TLegend(0.2, 0.7, 0.7, 0.8);
  leg_Momentum_truth_neutritno->SetFillColor(0);
  leg_Momentum_truth_neutritno->SetBorderSize(0);
  leg_Momentum_truth_neutritno->AddEntry(h_neutrino_pt_all,      "neutrino all", "l");
  leg_Momentum_truth_neutritno->AddEntry(h_neutrino_pt_hadronic, "from hadronic","l");
  leg_Momentum_truth_neutritno->AddEntry(h_neutrino_pt_leptonic, "from leptonic","l");
  leg_Momentum_truth_neutritno->Draw();

  _Momentum_truth_neutritno->RedrawAxis();
#endif  

#ifdef _numberOfXXX 
  // ----------------------------------------------------------------------------------------------
  TCanvas* numberOfXXX = new TCanvas("numberOfXXX","numberOfXXX", 800, 800);

  gStyle->SetPalette(1);
  //numberOfXXX->Divide(2,2);
  //numberOfXXX->cd(1);
  h_numberOfPixelHitsDecayRegion->SetMarkerColor(kWhite);
  h_numberOfPixelHitsDecayRegion->Draw("text45 colz");

  Double_t ymax = h_numberOfPixelHitsDecayRegion->GetYaxis()->GetXmax();
  TLine* IBL = new TLine(33.25, -0.5, 33.25, ymax);
  IBL->SetLineWidth(2);
  IBL->SetLineStyle(9);
  IBL->SetLineColor(kBlack);
  IBL->Draw("same");

  TLine* Blayer = new TLine(50.5, -0.5, 50.5, ymax);
  Blayer->SetLineWidth(2);
  Blayer->SetLineStyle(9);
  Blayer->SetLineColor(kBlack);
  Blayer->Draw("same");

  TLine* Pix2 = new TLine(88.5, -0.5, 88.5, ymax);
  Pix2->SetLineWidth(2);
  Pix2->SetLineStyle(9);
  Pix2->SetLineColor(kBlack);
  Pix2->Draw("same");

  TLine* Pix3 = new TLine(122.5, -0.5, 122.5, ymax);
  Pix3->SetLineWidth(2);
  Pix3->SetLineStyle(9);
  Pix3->SetLineColor(kBlack);
  Pix3->Draw("same");
  /*
     numberOfXXX->cd(2);
     h_numberOfSCTHits_vs_DecayRegion->Draw("colz");

     ymax = h_numberOfSCTHits_vs_DecayRegion->GetYaxis()->GetXmax();
     IBL->Draw("same");
     Blayer->Draw("same");
     Pix2->Draw("same");
     Pix3->Draw("same");

     numberOfXXX->cd(3);
     h_numberOfPixelHits_InnerMost_vs_DecayRegion->Draw("colz");
     numberOfXXX->cd(4);
     h_numberOfPixelHits_NextToInnerMost_vs_DecayRegion->Draw("colz");
     */
#endif

#ifdef _numberOfPixelHits_DecayRadius // -----------------------------------------------------------------------------------
  TCanvas* numberOfPixelHits_DecayRadius = new TCanvas("numberOfPixelHits_DecayRadius","numberOfPixelHits_DecayRadius", 1000, 400);

  gStyle->SetPalette(1);
  gStyle->SetOptStat(1);
  numberOfPixelHits_DecayRadius->Divide(5,2);

  for( int iTruthProng=0; iTruthProng<2; iTruthProng++){
    for( int iRecoProng=0; iRecoProng<5; iRecoProng++){
      numberOfPixelHits_DecayRadius->cd(iTruthProng*5 + iRecoProng+1);
      h_numberOfPixelHitsDecayRadius[iTruthProng][iRecoProng]->SetMarkerColor(kWhite);
      h_numberOfPixelHitsDecayRadius[iTruthProng][iRecoProng]->SetMarkerSize(0.9);
      h_numberOfPixelHitsDecayRadius[iTruthProng][iRecoProng]->Draw("text45 colz");

      if( h_numberOfPixelHitsDecayRadius[iTruthProng][iRecoProng]->Integral() == 0 ){
        TText* t = new TText(0.5,0.6, "Empty");
        t->SetNDC(1);
        t->SetTextAlign(22);
        t->SetTextColor(kBlack);
        t->SetTextFont(43);
        t->SetTextSize(40);
        t->SetTextAngle(45);
        t->Draw();
        continue;
      }

      TText* t = new TText(0.7,0.9, Form("reco %d-prong", iRecoProng));
      t->SetNDC(1);
      t->SetTextAlign(22);
      t->SetTextColor(kBlack);
      t->SetTextFont(43);
      t->SetTextSize(15);
      t->Draw("same");
    }
  }
  numberOfPixelHits_DecayRadius->SaveAs("numberOfPixelHits_DecayRadius.pdf");
#endif

#ifdef _numberOfPixelHits_Eta // -----------------------------------------------------------------------------------
  TCanvas* numberOfPixelHits_Eta = new TCanvas("numberOfPixelHits_Eta","numberOfPixelHits_Eta", 1000, 400);

  gStyle->SetPalette(1);
  gStyle->SetOptStat(1);
  numberOfPixelHits_Eta->Divide(6,2);

  std::string strRegion[6] ={"X<33.25mm","33.25mm<X<50.5mm","50.5mm<X<88.5mm","88.5mm<X<122.5mm","122.5mm<X","all"};
  for( int iTruthProng=0; iTruthProng<2; iTruthProng++){
    for( int iRegion=0; iRegion<6; iRegion++){
      numberOfPixelHits_Eta->cd(iTruthProng*6 + iRegion+1);
      h_numberOfPixelHitsEta[iTruthProng][iRegion]->SetMarkerColor(kWhite);
      h_numberOfPixelHitsEta[iTruthProng][iRegion]->SetMarkerSize(0.9);
      h_numberOfPixelHitsEta[iTruthProng][iRegion]->Draw("text45 colz");

      TText* t = new TText(0.7,0.9, Form("%s", strRegion[iRegion].c_str()));
      t->SetNDC(1);
      t->SetTextAlign(22);
      t->SetTextColor(kBlack);
      t->SetTextFont(43);
      t->SetTextSize(10);
      t->Draw();
    }
  }
  numberOfPixelHits_Eta->SaveAs("numberOfPixelHits_Eta.pdf");
#endif


#ifdef _DecayRadius_vs_NumberOfXXX // ---------------------------------------------------------------------------------------

  gStyle->SetOptLogy(1); 

  TCanvas* DecayRadius_vs_NumberOfXXX_Pixel = new TCanvas("DecayRadius_vs_NumberOfXXX_Pixel","DecayRadius_vs_NumberOfXXX_Pixel", 1200, 800);
  DecayRadius_vs_NumberOfXXX_Pixel->Divide(6,4);

  std::vector<Double_t> eventsPixel[2][5];
  std::vector<Double_t> eventsSCT[2][5];

  for( int iProng = 0; iProng<2; iProng++){
    for ( int iRegion =0; iRegion<5; iRegion++) {
      // SetLineColor
      h_numberOfPixelHits[iProng][iRegion]->SetLineColor(iRegion+1);
      h_numberOfSCTHits[iProng][iRegion]->SetLineColor(iRegion+1);
      // Integral
      eventsPixel[iProng][iRegion] .push_back(h_numberOfPixelHits[iProng][iRegion]->Integral() );
      eventsSCT[iProng][iRegion]   .push_back(h_numberOfSCTHits[iProng][iRegion]->Integral()   );
      // Scale
      //      h_numberOfPixelHits[iProng][iRegion]->Scale(1/h_numberOfPixelHits[iProng][iRegion]->Integral());
      //      h_numberOfSCTHits[iProng][iRegion]->Scale(1/h_numberOfSCTHits[iProng][iRegion]->Integral());
    }
  }

  for( int iProng = 0; iProng < 2; iProng++){
    /** the number of Pixel hits  **/
    for ( int iRegion =0; iRegion<5; iRegion++) {
      DecayRadius_vs_NumberOfXXX_Pixel->cd(iRegion+1 + iProng*12);
      h_numberOfPixelHits[iProng][iRegion]->Draw();

      /* TLegend */
      TLegend* legDecayRadius_vs_NumberOfXXX = new TLegend(0.3,0.65,0.9,0.8);
      legDecayRadius_vs_NumberOfXXX->SetFillColor(0);
      legDecayRadius_vs_NumberOfXXX->SetFillStyle(0);
      legDecayRadius_vs_NumberOfXXX->SetBorderSize(0);
      //      legDecayRadius_vs_NumberOfXXX->AddEntry( h_numberOfPixelHits[iProng][iRegion], Form( "%d events", eventsPixel[iProng][iRegion], ""));
      //     legDecayRadius_vs_NumberOfXXX->Draw();
    }

    DecayRadius_vs_NumberOfXXX_Pixel->cd(6 + iProng*12);
    for ( int iRegion =0; iRegion<5; iRegion++) {
      h_numberOfPixelHits[iProng][iRegion]->Draw("same");
    }
    DecayRadius_vs_NumberOfXXX_Pixel->RedrawAxis();

    /** the number of SCT hits  **/
    for ( int iRegion =0; iRegion<5; iRegion++) {
      DecayRadius_vs_NumberOfXXX_Pixel->cd(iRegion+1+6 + iProng*12);
      h_numberOfSCTHits[iProng][iRegion]->Draw();

      /* TLegend */
      TLegend* legDecayRadius_vs_NumberOfXXX = new TLegend(0.7,0.65,0.9,0.8);
      legDecayRadius_vs_NumberOfXXX->SetFillColor(0);
      legDecayRadius_vs_NumberOfXXX->SetFillStyle(0);
      legDecayRadius_vs_NumberOfXXX->SetBorderSize(0);
      //      legDecayRadius_vs_NumberOfXXX->AddEntry( h_numberOfSCTHits[iProng][iRegion], Form( "%d events", eventsSCT[iProng][iRegion], ""));
      //      legDecayRadius_vs_NumberOfXXX->Draw();
    }

    DecayRadius_vs_NumberOfXXX_Pixel->cd(12 + iProng*12);
    for ( int iRegion =0; iRegion<5; iRegion++) {
      h_numberOfSCTHits[iProng][iRegion]->Draw("same");
    }
  }

  DecayRadius_vs_NumberOfXXX_Pixel->RedrawAxis();
#endif

#ifdef _DecayRadius_vs_RawNumberOfXXX // ------------------------------------------------------------------------------------------------------
  TCanvas* DecayRadius_vs_RawNumberOfXXX = new TCanvas("DecayRadius_vs_RawNumberOfXXX","DecayRadius_vs_RawNumberOfXXX", 1200, 400);
  DecayRadius_vs_RawNumberOfXXX->Divide(6,2);

  std::vector<Double_t> eventsPixel[2][5];
  std::vector<Double_t> eventsSCT[2][5];

  for( int iProng = 0; iProng<2; iProng++){
    for ( int iRegion =0; iRegion<5; iRegion++) {
      // SetLineColor
      h_numberOfRawPixelHits[iProng][iRegion]->SetLineColor(iRegion+1);
      if( iRegion == 4 ) h_numberOfRawPixelHits[iProng][iRegion]->SetLineColor(kMagenta);
      // Integral
      eventsPixel[iProng][iRegion] .push_back(h_numberOfRawPixelHits[iProng][iRegion]->Integral() );
      // Scale
      //      h_numberOfPixelHits[iProng][iRegion]->Scale(1/h_numberOfPixelHits[iProng][iRegion]->Integral());
      //      h_numberOfSCTHits[iProng][iRegion]->Scale(1/h_numberOfSCTHits[iProng][iRegion]->Integral());
    }
  }

  for( int iProng = 0; iProng < 2; iProng++){
    /** the number of Pixel hits  **/
    for ( int iRegion =0; iRegion<5; iRegion++) {
      DecayRadius_vs_RawNumberOfXXX->cd(iRegion+1 + iProng*6);
      h_numberOfRawPixelHits[iProng][iRegion]->Draw();

      /* TLegend */
      std::stringstream ss;
      ss << h_numberOfRawPixelHits[iProng][iRegion]->Integral() << " evn";
      TText *t = new TText(0.7,0.7, ss.str().c_str() );
      t->SetNDC(1);
      t->SetTextAlign(22);
      t->SetTextColor(kBlack);
      t->SetTextFont(43);
      t->SetTextSize(15);
      t->Draw("same");
      /*
         TLegend* legDecayRadius_vs_RawNumberOfXXX = new TLegend(0.4,0.3,0.9,0.8);
         legDecayRadius_vs_RawNumberOfXXX->SetFillColor(0);
         legDecayRadius_vs_RawNumberOfXXX->SetFillStyle(0);
         legDecayRadius_vs_RawNumberOfXXX->SetBorderSize(0);
         legDecayRadius_vs_RawNumberOfXXX->AddEntry( h_numberOfRawPixelHits[iProng][iRegion], Form( "%d events", h_numberOfRawPixelHits[iProng][iRegion]->Integral(), ""));
         legDecayRadius_vs_RawNumberOfXXX->Draw();
         */
    }
    DecayRadius_vs_RawNumberOfXXX->RedrawAxis();

    /* Add all of the histograms */
    DecayRadius_vs_RawNumberOfXXX->cd(6 + iProng*6);
    for ( int iRegion =0; iRegion<5; iRegion++) {
      h_numberOfRawPixelHits[iProng][iRegion]->Draw("same");
      DecayRadius_vs_RawNumberOfXXX->RedrawAxis();
    }
  }
#endif

#ifdef _DecayRadius_vs_RawNumberOfXXX_Normalize // ------------------------------------------------------------------------------------------------------
  TCanvas* DecayRadius_vs_RawNumberOfXXX_Normalize = new TCanvas("DecayRadius_vs_RawNumberOfXXX_Normalize","DecayRadius_vs_RawNumberOfXXX_Normalize", 1200, 400);
  DecayRadius_vs_RawNumberOfXXX_Normalize->Divide(6,2);


  for( int iProng = 0; iProng<2; iProng++){
    for ( int iRegion =0; iRegion<5; iRegion++) {
      // SetLineColor
      h_numberOfRawPixelHits[iProng][iRegion]->SetLineColor(iRegion+1);
      if( iRegion == 4 ) h_numberOfRawPixelHits[iProng][iRegion]->SetLineColor(kMagenta);
      // Scale
      h_numberOfRawPixelHits[iProng][iRegion]->Scale(1/h_numberOfRawPixelHits[iProng][iRegion]->Integral());
    }
  }

  for( int iProng = 0; iProng < 2; iProng++){
    /** the number of Pixel hits  **/
    for ( int iRegion =0; iRegion<5; iRegion++) {
      DecayRadius_vs_RawNumberOfXXX_Normalize->cd(iRegion+1 + iProng*6);
      h_numberOfRawPixelHits[iProng][iRegion]->Draw();
    }
    DecayRadius_vs_RawNumberOfXXX_Normalize->RedrawAxis();

    /* Add all of the histograms */
    DecayRadius_vs_RawNumberOfXXX_Normalize->cd(6 + iProng*6);
    for ( int iRegion =0; iRegion<5; iRegion++) {
      h_numberOfRawPixelHits[iProng][iRegion]->Draw("same");
      DecayRadius_vs_RawNumberOfXXX_Normalize->RedrawAxis();
    }
  }
#endif

#ifdef _DecayRadius_EachProng_Efficiency // --------------------------------------------------------------------------------------------
  TCanvas* DecayRadius_EachProng = new TCanvas("DecayRadius_EachProng", "DecayRadius_EachProng", 1000, 500);
  DecayRadius_EachProng->Divide(2,1);

  for ( int iProng=0; iProng<2; iProng++){
    DecayRadius_EachProng->cd(iProng+1);

    /* TLegend */
    TLegend* legDecayRadius_EachProng = new TLegend(0.7,0.5,0.9,0.8);
    legDecayRadius_EachProng->SetFillColor(0);
    legDecayRadius_EachProng->SetFillStyle(0);
    legDecayRadius_EachProng->SetBorderSize(0);

    /* */
    h_number_of_tracks[iProng][7]->Sumw2();
    for( int iRecoProng =0; iRecoProng < 6 ; iRecoProng++ ){
      h_number_of_tracks[iProng][iRecoProng]->SetLineColor(iRecoProng+1);
      h_number_of_tracks[iProng][iRecoProng]->SetMarkerSize(0.7);
      h_number_of_tracks[iProng][iRecoProng]->SetMarkerColor(iRecoProng+1);

      h_number_of_tracks[iProng][iRecoProng]->Sumw2();
      h_number_of_tracks[iProng][iRecoProng]->Divide( h_number_of_tracks[iProng][iRecoProng], h_number_of_tracks[iProng][7], 1,1,"B");

      h_number_of_tracks[iProng][iRecoProng]->Draw("same:pe");
      if ( iRecoProng < 5 )  legDecayRadius_EachProng->AddEntry( h_number_of_tracks[iProng][iRecoProng], Form( "reco %d prong", iRecoProng));
      if ( iRecoProng == 5 ) legDecayRadius_EachProng->AddEntry( h_number_of_tracks[iProng][iRecoProng], "Reco-tau not found");
      if ( iRecoProng == 6 ) legDecayRadius_EachProng->AddEntry( h_number_of_tracks[iProng][iRecoProng], "Reco-tau matched truth-tau" );
    }
    h_number_of_tracks[iProng][0]->GetYaxis()->SetRangeUser(0,1.05);
    legDecayRadius_EachProng->Draw("");
  }
#endif

  return;
}
