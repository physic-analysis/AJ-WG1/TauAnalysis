
#include <iostream>
#include <TH1.h>
#include <TFile.h>

#define _nPt
#define _PtLeptonic
#define _RecoPt
#define _Fraction
#define _DecayRadius
#define _DecayRadiusFraction
#define _DecayRadiusFraction_TruthVsReco
#define CheckAcceptBeforeAfter

void WorkingPointChecker()
{
  std::vector<std::string> WorkingPoint;
  WorkingPoint.push_back("NONE");
  WorkingPoint.push_back("LOOSE");
  WorkingPoint.push_back("MEDIUM");
  WorkingPoint.push_back("TIGHT");

  std::string path = "/afs/cern.ch/user/k/ktakeda/workspace/public/atlas-phys/AJ/WG1/SingleParticle/Data/Ntuple/MC12G4/";
  std::vector<std::string> data_name;
  for(int i=0; i<WorkingPoint.size(); i++){
    data_name.push_back(path + "MyTauNtupleFullG4_JETIDWP_" + WorkingPoint.at(i) + ".outputs.root");
  }
  
  std::vector<TFile*> tfile;
  for(int i=0; i<WorkingPoint.size(); i++){
    tfile.push_back(new TFile(data_name.c_str(), "read"));
  }

  
  TFile* file = new TFile(path.c_str(), "read");
  TTree* tree = (TTree*)file->Get("m_recoTauTree");
  
  Double_t reco_pt; 
  Double_t reco_eta; 
  Double_t reco_phi; 
  Double_t reco_e; 
  Bool_t reco_not_a_electron; 
  Bool_t reco_goodTau; 
  Bool_t reco_MuonOLR; 
  Bool_t reco_selectedGoodTau; 
  Bool_t truth_leptonic_match = false; 
  Bool_t truth_leptonic_electron = false; 
  Bool_t truth_leptonic_muon = false; 
  Bool_t truth_leptonic_tau = false; 
  Bool_t truth_hadronic_match = false; 
  Double_t truth_pt; 
  Double_t truth_eta; 
  Double_t truth_phi; 
  Double_t truth_e; 
  Int_t truth_nprong =0; 
  Double_t truth_decay_radius = 0; 
  Int_t reco_number_of_tracks = 0; 
  Int_t reco_number_of_selected_tracks =0; 

  tree->SetBranchAddress("reco_pt",                        &reco_pt);
  tree->SetBranchAddress("reco_eta",                       &reco_eta);
  tree->SetBranchAddress("reco_phi",                       &reco_phi);
  tree->SetBranchAddress("reco_e",                         &reco_e);
  tree->SetBranchAddress("reco_not_a_electron",            &reco_not_a_electron);
  tree->SetBranchAddress("reco_goodTau",                   &reco_goodTau);
  tree->SetBranchAddress("reco_MuonOLR",                   &reco_MuonOLR);
  tree->SetBranchAddress("reco_selectedGoodTau",           &reco_selectedGoodTau);
  tree->SetBranchAddress("truth_leptonic_match",           &truth_leptonic_match);
  tree->SetBranchAddress("truth_leptonic_electron",        &truth_leptonic_electron);
  tree->SetBranchAddress("truth_leptonic_muon",            &truth_leptonic_muon);
  tree->SetBranchAddress("truth_leptonic_tau",             &truth_leptonic_tau);
  tree->SetBranchAddress("truth_hadronic_match",           &truth_hadronic_match);
  tree->SetBranchAddress("truth_pt",                       &truth_pt);
  tree->SetBranchAddress("truth_eta",                      &truth_eta);
  tree->SetBranchAddress("truth_phi",                      &truth_phi);
  tree->SetBranchAddress("truth_e",                        &truth_e);
  tree->SetBranchAddress("truth_decay_radius",             &truth_decay_radius);
  tree->SetBranchAddress("truth_nprong",                   &truth_nprong);
  tree->SetBranchAddress("reco_number_of_tracks",          &reco_number_of_tracks);
  tree->SetBranchAddress("reco_number_of_selected_tracks", &reco_number_of_selected_tracks);


  TH1D* h_truth_1p = new TH1D("h_truth_1p", ";;", 40, 0, 250);
  TH1D* h_reco_1p_1p = new TH1D("h_reco_1p_1p", ";;", 40, 0, 250);
  TH1D* h_reco_2p = new TH1D("h_reco_2p", ";;", 40, 0, 250);
  TH1D* h_reco_3p = new TH1D("h_reco_3p", ";;", 40, 0, 250);
  
  TH1D* h_truth_3p = new TH1D("h_truth_3p", ";;", 40, 0, 250);
  TH1D* h_reco_3p_3p = new TH1D("h_reco_3p_3p", ";;", 40, 0, 250);

  /// truth momentum
  TH1D* h_truth_pt_all      = new TH1D("h_truth_pt_all",";pT [GeV];Events",100, 0, 700);
  TH1D* h_truth_pt_hadronic = new TH1D("h_truth_pt_hadronic",";pT [GeV];Events",100, 0, 700);
  TH1D* h_truth_pt_leptonic = new TH1D("h_truth_pt_leptonic",";pT [GeV];Events",100, 0, 700);
  TH1D* h_truth_pt_electron = new TH1D("h_truth_pt_electron",";pT [GeV];Events",100, 0, 700);
  TH1D* h_truth_pt_muon     = new TH1D("h_truth_pt_muon",";pT [GeV];Events",100, 0, 700);
  
  /// Reconstrusted momentum
  TH1D* h_reco_pt_all           = new TH1D("h_reco_pt_all",";pT [GeV];Events",100, 0, 700);
  TH1D* h_reco_pt_all_bestTau   = new TH1D("h_reco_pt_all_bestTau",";pT [GeV];Events",100, 0, 700);
  TH1D* h_reco_pt_all_EleOLR    = new TH1D("h_reco_pt_all_EleOLR",";pT [GeV];Events",100, 0, 700);
  TH1D* h_reco_pt_all_MuonOLR   = new TH1D("h_reco_pt_all_MuonOLR",";pT [GeV];Events",100, 0, 700);
  TH1D* h_reco_pt_allHadronic   = new TH1D("h_reco_pt_allHadronic",";pT [GeV];Events",100, 0, 700);
  TH1D* h_reco_pt_hadronic      = new TH1D("h_reco_pt_hadronic",";pT [GeV];Events",100, 0, 700);
  TH1D* h_reco_pt_leptonic      = new TH1D("h_reco_pt_leptonic",";pT [GeV];Events",100, 0, 700);
  TH1D* h_reco_pt_electron      = new TH1D("h_reco_pt_electron",";pT [GeV];Events",100, 0, 700);
  TH1D* h_reco_pt_muon          = new TH1D("h_reco_pt_muon",";pT [GeV];Events",100, 0, 700);
  TH1D* h_reco_pt_tau           = new TH1D("h_reco_pt_tau",";pT [GeV];Events",100, 0, 700);
  
  TH1D* h_reco_pt_1p = new TH1D("h_reco_pt_1p", ";;", 100, 0, 700);
  TH1D* h_reco_pt_2p = new TH1D("h_reco_pt_2p", ";;", 100, 0, 700);
  TH1D* h_reco_pt_3p = new TH1D("h_reco_pt_3p", ";;", 100, 0, 700);
  
  TH1D* h_pt_reco1p_truth1p = new TH1D("h_pt_reco1p_truth1p", ";;", 100, 0, 700);
  TH1D* h_pt_reco2p_truth2p = new TH1D("h_pt_reco2p_truth2p", ";;", 100, 0, 700);
  TH1D* h_pt_reco3p_truth3p = new TH1D("h_pt_reco3p_truth3p", ";;", 100, 0, 700);
  
  /// Reconstrusted momentum after selected
  TH1D*  h_reco_pt_all_afterSelection      = new TH1D("h_reco_pt_all_afterSelection","",100,0,700);
  TH1D*  h_reco_pt_hadronic_afterSelection = new TH1D("h_reco_pt_hadronic_afterSelection","",100,0,700);
  TH1D*  h_reco_pt_leptonic_afterSelection = new TH1D("h_reco_pt_leptonic_afterSelection","",100,0,700);
  TH1D*  h_reco_pt_electron_afterSelection = new TH1D("h_reco_pt_electron_afterSelection","",100,0,700);
  TH1D*  h_reco_pt_muon_afterSelection     = new TH1D("h_reco_pt_muon_afterSelection","",100,0,700);
  
  /// Fraction
  TH1D* h_Hadall = new TH1D("h_Hadall",";;",40, 0, 700);
  TH1D* h_reco1prong = new TH1D("h_reco1prong",";pT[GeV];Fraction",40, 0, 700);
  TH1D* h_reco2prong = new TH1D("h_reco2prong",";pT[GeV];Fraction",40, 0, 700);
  TH1D* h_reco3prong = new TH1D("h_reco3prong",";pT[GeV];Fraction",40, 0, 700);
  
  /// 1-prong ot distribution
  TH1D* h_truth_pt_1p = new TH1D("h_truth_pt_1p", ";;", 100, 0, 1000);
  TH1D* h_truth_pt_3p = new TH1D("h_truth_pt_3p", ";;", 100, 0, 1000);
  
  /// Reconstrustion momentum vs truth matched momentum

  /// Decay radius
  TH1D* h_decay_rad_true1p = new TH1D("h_decay_rad_true1p",";decay position [mm];Events",50,0,200);
  TH1D* h_reco_decay_allHadronic = new TH1D("h_reco_decay_allHadronic","",50,0,200);
  TH1D* h_reco_decay_1p_truth_1p = new TH1D("h_reco_decay_1p_truth_1p","",50,0,200);
  TH1D* h_reco_decay_2p_truth_2p = new TH1D("h_reco_decay_2p_truth_2p","",50,0,200);
  TH1D* h_reco_decay_3p_truth_3p = new TH1D("h_reco_decay_3p_truth_3p","",50,0,200);
  TH1D* h_reco_decay_1p = new TH1D("h_reco_decay","",50,0,200);
  TH1D* h_reco_decay_2p = new TH1D("h_reco_decay","",50,0,200);
  TH1D* h_reco_decay_3p = new TH1D("h_reco_decay","",50,0,200);

  /// Number of tracks
  TH1D* h_number_of_truth_track = new TH1D("h_number_of_truth_track", ";truth tau [n-prong];Events", 5, -0.5, 4.5);
  TH1D* h_number_of_selected_reco_track = new TH1D("h_number_of_selected_reco_track", ";reco tau [n-prong];Events", 5, -0.5, 4.5);
  TH1D* h_number_of_selected_reco_track_truth_mached = new TH1D("h_number_of_selected_reco_track_truth_mached", ";reco truth matched tau [n-prong];Events", 5, -0.5, 4.5);

  for(int iEntry=0; iEntry< tree->GetEntries(); iEntry++){
    tree->GetEntry(iEntry);

    ///------------------------ Fraction (pT dependence) -----------------------------------
    /// Reconstrustion momentum vs truth matched momentum
    if ( truth_leptonic_match || truth_hadronic_match){
      h_reco_pt_all->Fill(reco_pt/1000.);
      h_truth_pt_all->Fill(truth_pt/1000.);
      
      if ( reco_selectedGoodTau ) h_reco_pt_all_afterSelection->Fill(reco_pt/1000.);

      if ( reco_goodTau && reco_not_a_electron )
        h_reco_pt_all_bestTau->Fill(reco_pt/1000.);
      if ( !reco_MuonOLR ) 
        h_reco_pt_all_MuonOLR->Fill(reco_pt/1000.);
      if( !reco_not_a_electron)
        h_reco_pt_all_EleOLR->Fill(reco_pt/1000.);
      
      if( truth_leptonic_match ){
        h_truth_pt_leptonic->Fill(truth_pt/1000.);
        if ( reco_selectedGoodTau ) h_reco_pt_leptonic_afterSelection->Fill(reco_pt/1000.);

        if( truth_leptonic_electron ) {
          h_truth_pt_electron->Fill(truth_pt/1000.);
          if ( reco_selectedGoodTau ) h_reco_pt_electron_afterSelection->Fill(reco_pt/1000.);
        }
        if( truth_leptonic_muon ){
          h_truth_pt_muon->Fill(truth_pt/1000.);
          if ( reco_selectedGoodTau ) h_reco_pt_muon_afterSelection->Fill(reco_pt/1000.);
        }
      }
      if( truth_hadronic_match ){
        h_truth_pt_hadronic->Fill(truth_pt/1000.);
        h_reco_pt_hadronic->Fill(reco_pt/1000.);
          if ( reco_selectedGoodTau ) h_reco_pt_hadronic_afterSelection->Fill(reco_pt/1000.);
      }
    }

    /// To divide leptonic decay mode into e, mu, tau mode.
    if ( truth_leptonic_match ){
      h_reco_pt_leptonic->Fill(reco_pt/1000.);
      if( truth_leptonic_electron)
        h_reco_pt_electron->Fill(reco_pt/1000.);
      if( truth_leptonic_muon)
        h_reco_pt_muon->Fill(reco_pt/1000.);
      if( truth_leptonic_tau)
        h_reco_pt_tau->Fill(reco_pt/1000.);
    }

    if( truth_hadronic_match){
      h_reco_pt_allHadronic->Fill(reco_pt/1000.);
      h_reco_decay_allHadronic->Fill(truth_decay_radius); // decay radius
      h_Hadall->Fill(reco_pt/1000.);
    }
    if( truth_hadronic_match && reco_number_of_selected_tracks == 1 ) { 
      h_reco_pt_1p->Fill(truth_pt/1000.);
      h_reco1prong->Fill(truth_pt/1000.);
      h_reco_decay_1p->Fill(truth_decay_radius); // decay radius
      if( truth_nprong == 1 ){
        h_pt_reco1p_truth1p->Fill(reco_pt/1000.);
        h_reco_decay_1p_truth_1p->Fill(truth_decay_radius); // decay radius
      }
    }
    if( truth_hadronic_match && reco_number_of_selected_tracks == 2 ) { 
      h_reco_pt_2p->Fill(truth_pt/1000.);
      h_reco2prong->Fill(truth_pt/1000.);
      h_reco_decay_2p->Fill(truth_decay_radius); // decay radius
      if( truth_nprong == 2 ){
        h_pt_reco2p_truth2p->Fill(reco_pt/1000.);
        h_reco_decay_2p_truth_2p->Fill(truth_decay_radius); // decay radius
      }
    }
    if( truth_hadronic_match && reco_number_of_selected_tracks == 3 ) {
      h_reco_pt_3p->Fill(truth_pt/1000.);
      h_reco3prong->Fill(truth_pt/1000.);
        h_reco_decay_3p->Fill(truth_decay_radius); // decay radius
      if( truth_nprong == 3 ){
        h_pt_reco3p_truth3p->Fill(reco_pt/1000.);
        h_reco_decay_3p_truth_3p->Fill(truth_decay_radius); // decay radius
      }
    }
      
    ///------------------------ Fraction (decay radius dependence) -----------------------------------
    /// Decay radius 
    if( truth_hadronic_match == true && truth_nprong == 1 ) {
      h_truth_1p->Fill(truth_decay_radius);
      if(reco_number_of_selected_tracks == 1 ){
        h_reco_1p_1p->Fill(truth_decay_radius);
        h_decay_rad_true1p->Fill(truth_decay_radius);
      }
    }

    /// Number of tracks
    h_number_of_selected_reco_track->Fill(reco_number_of_selected_tracks);
    if( truth_hadronic_match == true ){
      h_number_of_selected_reco_track_truth_mached->Fill(reco_number_of_selected_tracks);
      h_number_of_truth_track->Fill(truth_nprong);
    }
    
    if(reco_number_of_selected_tracks == 3 ){
      h_reco_3p_3p->Fill(truth_decay_radius);
      if(truth_hadronic_match){
        if(truth_nprong == 3 ) {
          h_truth_3p->Fill(truth_decay_radius);
        }
      }
    }
  }

  /// ----------------------------------------------------------------------------------------
  TCanvas* MomentumAll = new TCanvas("MomentumAll","MomentumAll");
  h_reco_pt_all = (TH1D*)h_reco_pt_all->Clone();
  
  h_reco_pt_all->SetLineColor(kMagenta);
  
  h_truth_pt_all->Draw();
  h_reco_pt_all->Draw("same");
  
  ATLASLabel(0.2,0.8,"Simulation");
  TLegend* legMomentumAll = new TLegend(0.2,0.65,0.6,0.8);
  legMomentumAll->SetFillColor(0);
  legMomentumAll->SetFillStyle(0);
  legMomentumAll->SetBorderSize(0);
  legMomentumAll->AddEntry(h_truth_pt_all, "truth visible energy", "l");
  legMomentumAll->AddEntry(h_reco_pt_all, "reconsructed energy", "l");
  legMomentumAll->Draw();

  /// ----------------------------------------------------------------------------------------
  TCanvas* Truth_MomentumComponent = new TCanvas("Truth_MomentumComponent","Truth_MomentumComponent");
  h_truth_pt_all      = (TH1D*)h_truth_pt_all->Clone();
  h_truth_pt_hadronic = (TH1D*)h_truth_pt_hadronic->Clone();
  h_truth_pt_electron = (TH1D*)h_truth_pt_electron->Clone();
  h_truth_pt_muon     = (TH1D*)h_truth_pt_muon->Clone();
  
  h_truth_pt_hadronic->SetLineColor(kRed);
  h_truth_pt_electron->SetLineColor(kBlue);
  h_truth_pt_muon->SetLineColor(kGreen);

  h_truth_pt_all->Draw();
  h_truth_pt_hadronic->Draw("same");
  h_truth_pt_electron->Draw("same");
  h_truth_pt_muon->Draw("same");

  Truth_MomentumComponent->RedrawAxis();

  ATLASLabel(0.2,0.8,"Simulation");
  TLegend* legTruth_MomentumComponent = new TLegend(0.2,0.65,0.6,0.8);
  legTruth_MomentumComponent->SetFillColor(0);
  legTruth_MomentumComponent->SetFillStyle(0);
  legTruth_MomentumComponent->SetBorderSize(0);
  legTruth_MomentumComponent->AddEntry(h_truth_pt_hadronic, Form("hadronic tau : %0.f events", h_truth_pt_hadronic->Integral()), "l");
  legTruth_MomentumComponent->AddEntry(h_truth_pt_electron, Form("-> electron : %0.f events",  h_truth_pt_electron->Integral()), "l");
  legTruth_MomentumComponent->AddEntry(h_truth_pt_muon,     Form("-> muon :%0.f evetns",       h_truth_pt_muon->Integral()), "l");
  legTruth_MomentumComponent->Draw();
  
  /// ----------------------------------------------------------------------------------------
  TCanvas* MomentumLeptonic = new TCanvas("MomentumLeptonic","MomentumLeptonic");
  h_truth_pt_leptonic->GetYaxis()->SetRangeUser(0,315);
  
  h_truth_pt_leptonic->SetLineColor(kRed);

  h_truth_pt_leptonic->Draw();
  h_reco_pt_leptonic->Draw("same");
  
  ATLASLabel(0.2,0.8,"Simulation");
  TLegend* legMomentumLeptonic = new TLegend(0.2,0.65,0.6,0.8);
  legMomentumLeptonic->SetFillColor(0);
  legMomentumLeptonic->SetFillStyle(0);
  legMomentumLeptonic->SetBorderSize(0);
  legMomentumLeptonic->AddEntry(h_truth_pt_leptonic, "truth leptonic", "l");
  legMomentumLeptonic->AddEntry(h_reco_pt_leptonic,  "reco leptonic", "l");
  legMomentumLeptonic->Draw();
  
  /// ----------------------------------------------------------------------------------------
  TCanvas* PtHadronicTruthReco = new TCanvas("PtHadronicTruthReco","PtHadronicTruthReco");
  h_truth_pt_hadronic->GetYaxis()->SetRangeUser(0,315);
  h_truth_pt_hadronic->Draw();
  h_reco_pt_allHadronic->Draw("same");

  ATLASLabel(0.2,0.8,"Simulation");
  TLegend* legMomentumHadronic = new TLegend(0.2,0.65,0.6,0.8);
  legMomentumHadronic->SetFillColor(0);
  legMomentumHadronic->SetFillStyle(0);
  legMomentumHadronic->SetBorderSize(0);
  legMomentumHadronic->AddEntry(h_truth_pt_hadronic, "truth hadronic", "l");
  legMomentumHadronic->AddEntry(h_reco_pt_allHadronic, "reco hadronic", "l");
  legMomentumHadronic->Draw();

  // -----------------------------------------------------------------
  TCanvas* Reco_MomentumComponent = new TCanvas("Reco_MomentumComponent","Reco_MomentumComponent");
  /// Clone
  h_reco_pt_all = (TH1D*)h_reco_pt_all->Clone();
  h_reco_pt_leptonic= (TH1D*)h_reco_pt_leptonic->Clone();
  h_reco_pt_allHadronic = (TH1D*)h_reco_pt_allHadronic->Clone();
  
  h_reco_pt_all->GetYaxis()->SetRangeUser(0,311);
  
  /// SetLineColor 
  h_reco_pt_all->SetLineColor(kBlack);
  h_reco_pt_leptonic->SetLineColor(kBlue);
  h_reco_pt_allHadronic->SetLineColor(kRed);

  /// Draw
  h_reco_pt_all->Draw();
  h_reco_pt_leptonic->Draw("same");
  h_reco_pt_allHadronic->Draw("same");
  
  /// Integral 
  h_reco_pt_all->Integral();
  h_reco_pt_leptonic->Draw("same");
  h_reco_pt_allHadronic->Draw("same");
  
  /// Legend
  ATLASLabel(0.2,0.85, "Simulation");
  
  TLegend* legReco_MomentumComponent = new TLegend(0.2, 0.7, 0.7, 0.8);
  legReco_MomentumComponent->SetFillColor(0);
  legReco_MomentumComponent->SetBorderSize(0);
  legReco_MomentumComponent->AddEntry(h_reco_pt_all, Form("reco all : %.0f events", h_reco_pt_all->Integral() ), "l");
  legReco_MomentumComponent->AddEntry(h_reco_pt_allHadronic, Form("reco (hadronic matched) : %.0f events", h_reco_pt_allHadronic->Integral()), "l");
  legReco_MomentumComponent->AddEntry(h_reco_pt_leptonic, Form("reco (leptonic matched) : %.0f events", h_reco_pt_leptonic->Integral()), "l");
  legReco_MomentumComponent->Draw();
  
  // -----------------------------------------------------------------
  TCanvas* Reco_ElectronMuonRemoval = new TCanvas("Reco_ElectronMuonRemoval","Reco_ElectronMuonRemoval");
  h_reco_pt_all          = (TH1D*)h_reco_pt_all->Clone();
  h_reco_pt_all_MuonOLR  = (TH1D*)h_reco_pt_all_MuonOLR->Clone();
  h_reco_pt_all_EleOLR   = (TH1D*)h_reco_pt_all_EleOLR->Clone();
  
  //h_reco_pt_all_bestTau->GetYaxis()->SetRangeUser(0,311);
  //h_reco_pt_all_bestTau->Draw();

  h_reco_pt_all_MuonOLR->SetFillStyle(3444);
  h_reco_pt_all_MuonOLR->SetFillColor(kRed);
  
  h_reco_pt_all_EleOLR->SetFillStyle(3445);
  h_reco_pt_all_EleOLR->SetFillColor(kBlue);

  h_reco_pt_all->Draw();
  h_reco_pt_all_MuonOLR->Draw("same");
  h_reco_pt_all_EleOLR->Draw("same");

#ifdef CheckAcceptBeforeAfter
  // -----------------------------------------------------------------
  h_reco_pt_all      = (TH1D*)h_reco_pt_all->Clone();
  h_reco_pt_hadronic = (TH1D*)h_reco_pt_hadronic->Clone();
  h_reco_pt_leptonic = (TH1D*)h_reco_pt_leptonic->Clone();
  h_reco_pt_electron = (TH1D*)h_reco_pt_electron->Clone();
  h_reco_pt_muon     = (TH1D*)h_reco_pt_muon->Clone();
  
  h_reco_pt_all_afterSelection      = (TH1D*)h_reco_pt_all_afterSelection->Clone();
  h_reco_pt_hadronic_afterSelection = (TH1D*)h_reco_pt_hadronic_afterSelection->Clone();
  h_reco_pt_leptonic_afterSelection = (TH1D*)h_reco_pt_leptonic_afterSelection->Clone();
  h_reco_pt_electron_afterSelection = (TH1D*)h_reco_pt_electron_afterSelection->Clone();
  h_reco_pt_muon_afterSelection     = (TH1D*)h_reco_pt_muon_afterSelection->Clone();
  
  h_reco_pt_all->SetLineColor(kBlack);
  h_reco_pt_hadronic->SetLineColor(kBlack);
  h_reco_pt_leptonic->SetLineColor(kBlack);
  h_reco_pt_electron->SetLineColor(kBlack);
  h_reco_pt_muon->SetLineColor(kBlack);
 
  h_reco_pt_all_afterSelection->SetLineColor(kMagenta);
  h_reco_pt_hadronic_afterSelection->SetLineColor(kMagenta);
  h_reco_pt_leptonic_afterSelection->SetLineColor(kMagenta);
  h_reco_pt_electron_afterSelection->SetLineColor(kMagenta);
  h_reco_pt_muon_afterSelection->SetLineColor(kMagenta);

  TCanvas* Reco_All_BeforeAfterAccept = new TCanvas("Reco_All_BeforeAfterAccept","Reco_All_BeforeAfterAccept");
  h_reco_pt_all->GetYaxis()->SetRangeUser(0,311);
  h_reco_pt_all->Draw("same");
  h_reco_pt_all_afterSelection->Draw("same");
  /// Legend
  ATLASLabel(0.2,0.85, "Simulation");

  TCanvas* Reco_hadronic_BeforeAfterAccept = new TCanvas("Reco_hadronic_BeforeAfterAccept","Reco_hadronic_BeforeAfterAccept");
  h_reco_pt_hadronic->GetYaxis()->SetRangeUser(0,311);
  h_reco_pt_hadronic->Draw("same");
  h_reco_pt_hadronic_afterSelection->Draw("same");
  /// Legend
  ATLASLabel(0.2,0.85, "Simulation");
  
  TCanvas* Reco_leptonic_BeforeAfterAccept = new TCanvas("Reco_leptonic_BeforeAfterAccept","Reco_leptonic_BeforeAfterAccept");
  h_reco_pt_leptonic->GetYaxis()->SetRangeUser(0,311);
  h_reco_pt_leptonic->Draw("same");
  h_reco_pt_leptonic_afterSelection->Draw("same");
  /// Legend
  ATLASLabel(0.2,0.85, "Simulation");
 
  // 
  TCanvas* Reco_electron_BeforeAfterAccept = new TCanvas("Reco_electron_BeforeAfterAccept","Reco_electron_BeforeAfterAccept");
  h_reco_pt_electron->GetYaxis()->SetRangeUser(0,311);
  h_reco_pt_electron->Draw("same");
  h_reco_pt_electron_afterSelection->Draw("same");
  /// Legend
  ATLASLabel(0.2,0.85, "Simulation");
 
  TCanvas* Reco_muon_BeforeAfterAccept = new TCanvas("Reco_muon_BeforeAfterAccept","Reco_muon_BeforeAfterAccept");
  h_reco_pt_muon->GetYaxis()->SetRangeUser(0,311);
  h_reco_pt_muon->Draw("same");
  h_reco_pt_muon_afterSelection->Draw("same");
  /// Legend
  ATLASLabel(0.2,0.85, "Simulation");
#endif


  return;
}
