//////////////////////////////////////////////////////////
// This class has been automatically generated on
// Tue Sep  4 18:33:12 2018 by ROOT version 6.06/02
// from TTree CollectionTree/CollectionTree
// found on file: ../user.ktakeda.15239423.EXT1._000001.xAOD.pool.root
//////////////////////////////////////////////////////////

#ifndef xAODScan_h
#define xAODScan_h

#include <TROOT.h>
#include <TChain.h>
#include <TFile.h>

// Header file for the classes stored in the TTree if any.
#include "vector"
#include "vector"
#include "vector"
#include "vector"
#include "vector"
#include "vector"
#include "vector"
#include "vector"


class xAODScan {
public :
   TTree          *fChain;   //!pointer to the analyzed TTree or TChain
   TTree* m_SkimTree;
   Int_t           fCurrent; //!current Tree number in a TChain

// Fixed size dimensions of array or collections stored in the TTree if any.
   const Int_t kMaxEventInfoAux = 1;
   const Int_t kMaxTruthParticlesAux = 1;
   const Int_t kMaxElectronsAux = 1;
   const Int_t kMaxTauTracksAux = 1;
   const Int_t kMaxMuonsAux = 1;
   const Int_t kMaxGSFTrackParticlesAux = 1;
   const Int_t kMaxInDetTrackParticlesAux = 1;
   const Int_t kMaxPixelMSOSsAux = 1;
   const Int_t kMaxBTagging_AntiKt4EMTopoAux = 1;
   const Int_t kMaxTruthVerticesAux = 1;
   const Int_t kMaxTruthEventsAux = 1;
   const Int_t kMaxAntiKt4EMTopoJetsAux = 1;
   const Int_t kMaxAntiKt4LCTopoJetsAux = 1;
   const Int_t kMaxTauJetsAux = 1;
   const Int_t kMaxPixelClustersAux = 1;
   const Int_t kMaxPrimaryVerticesAux = 1;
   const Int_t kMaxAntiKt4EMTopoJetsAuxDyn_btaggingLink = 1;
   const Int_t kMaxAntiKt4EMTopoJetsAuxDyn_HighestJVFVtx = 1;
   const Int_t kMaxAntiKt4EMTopoJetsAuxDyn_OriginVertex = 1;
   const Int_t kMaxAntiKt4LCTopoJetsAuxDyn_HighestJVFVtx = 1;
   const Int_t kMaxAntiKt4LCTopoJetsAuxDyn_OriginVertex = 1;
   const Int_t kMaxBTagging_AntiKt4EMTopoAuxDyn_SMT_mu_link = 1;
   const Int_t kMaxElectronsAuxDyn_truthParticleLink = 3;
   const Int_t kMaxElectronsAuxDyn_ambiguityLink = 3;
   const Int_t kMaxGSFTrackParticlesAuxDyn_trackLink = 26;
   const Int_t kMaxGSFTrackParticlesAuxDyn_truthParticleLink = 26;
   const Int_t kMaxGSFTrackParticlesAuxDyn_originalTrackParticle = 26;
   const Int_t kMaxInDetTrackParticlesAuxDyn_trackLink = 33;
   const Int_t kMaxInDetTrackParticlesAuxDyn_truthParticleLink = 33;
   const Int_t kMaxTauJetsAuxDyn_electronLink = 1;
   const Int_t kMaxMuonsAuxDyn_truthParticleLink = 6;

   // Declaration of leaf types
 //xAOD::TauTrackAuxContainer_v1 *TauTracksAux_;
 //xAOD::TauTrackAuxContainer_v1 *TauTracksAux_xAOD__AuxContainerBase;
   vector<float>   TauTracksAux_pt;
   vector<float>   TauTracksAux_eta;
   vector<float>   TauTracksAux_phi;
   vector<unsigned short> TauTracksAux_flagSet;
   vector<vector<float> > TauTracksAux_bdtScores;
//.*DataVector<xAOD::TrackParticle_v1> > > > TauTracksAux_trackLinks;
 //xAOD::MuonAuxContainer_v4 *MuonsAux_;
 //xAOD::MuonAuxContainer_v4 *MuonsAux_xAOD__AuxContainerBase;
  
//.*DataVector<xAOD::Vertex_v1> > > GSFTrackParticlesAux_vertexLink;
   vector<float>   GSFTrackParticlesAux_TRTTrackOccupancy;
 //xAOD::TrackParticleAuxContainer_v3 *InDetTrackParticlesAux_;
 //xAOD::TrackParticleAuxContainer_v3 *InDetTrackParticlesAux_xAOD__AuxContainerBase;

   vector<float>   InDetTrackParticlesAux_d0;
   vector<float>   InDetTrackParticlesAux_z0;
   vector<float>   InDetTrackParticlesAux_phi;
   vector<float>   InDetTrackParticlesAux_theta;
   vector<float>   InDetTrackParticlesAux_qOverP;
   vector<vector<float> > InDetTrackParticlesAux_definingParametersCovMatrix;
   vector<float>   InDetTrackParticlesAux_vx;
   vector<float>   InDetTrackParticlesAux_vy;
   vector<float>   InDetTrackParticlesAux_vz;
   vector<float>   InDetTrackParticlesAux_radiusOfFirstHit;
   vector<unsigned long> InDetTrackParticlesAux_identifierOfFirstHit;
   vector<float>   InDetTrackParticlesAux_beamlineTiltX;
   vector<float>   InDetTrackParticlesAux_beamlineTiltY;
   vector<unsigned int> InDetTrackParticlesAux_hitPattern;
   vector<float>   InDetTrackParticlesAux_chiSquared;
   vector<float>   InDetTrackParticlesAux_numberDoF;
   vector<unsigned char> InDetTrackParticlesAux_trackFitter;
   vector<unsigned char> InDetTrackParticlesAux_particleHypothesis;
   vector<unsigned char> InDetTrackParticlesAux_trackProperties;
   vector<unsigned long> InDetTrackParticlesAux_patternRecoInfo;
   vector<unsigned char> InDetTrackParticlesAux_numberOfContribPixelLayers;
   vector<unsigned char> InDetTrackParticlesAux_numberOfInnermostPixelLayerHits;
   vector<unsigned char> InDetTrackParticlesAux_numberOfInnermostPixelLayerOutliers;
   vector<unsigned char> InDetTrackParticlesAux_numberOfInnermostPixelLayerSharedHits;
   vector<unsigned char> InDetTrackParticlesAux_numberOfInnermostPixelLayerSplitHits;
   vector<unsigned char> InDetTrackParticlesAux_expectInnermostPixelLayerHit;
   vector<unsigned char> InDetTrackParticlesAux_numberOfNextToInnermostPixelLayerHits;
   vector<unsigned char> InDetTrackParticlesAux_numberOfNextToInnermostPixelLayerOutliers;
   vector<unsigned char> InDetTrackParticlesAux_numberOfNextToInnermostPixelLayerSharedHits;
   vector<unsigned char> InDetTrackParticlesAux_numberOfNextToInnermostPixelLayerSplitHits;
   vector<unsigned char> InDetTrackParticlesAux_expectNextToInnermostPixelLayerHit;
   vector<unsigned char> InDetTrackParticlesAux_numberOfPixelHits;
   vector<unsigned char> InDetTrackParticlesAux_numberOfPixelOutliers;
   vector<unsigned char> InDetTrackParticlesAux_numberOfPixelHoles;
   vector<unsigned char> InDetTrackParticlesAux_numberOfPixelSharedHits;
   vector<unsigned char> InDetTrackParticlesAux_numberOfPixelSplitHits;
   vector<unsigned char> InDetTrackParticlesAux_numberOfGangedPixels;
   vector<unsigned char> InDetTrackParticlesAux_numberOfGangedFlaggedFakes;
   vector<unsigned char> InDetTrackParticlesAux_numberOfPixelDeadSensors;
   vector<unsigned char> InDetTrackParticlesAux_numberOfPixelSpoiltHits;
   vector<unsigned char> InDetTrackParticlesAux_numberOfDBMHits;
   vector<unsigned char> InDetTrackParticlesAux_numberOfSCTHits;
   vector<unsigned char> InDetTrackParticlesAux_numberOfSCTOutliers;
   vector<unsigned char> InDetTrackParticlesAux_numberOfSCTHoles;
   vector<unsigned char> InDetTrackParticlesAux_numberOfSCTDoubleHoles;
   vector<unsigned char> InDetTrackParticlesAux_numberOfSCTSharedHits;
   vector<unsigned char> InDetTrackParticlesAux_numberOfSCTDeadSensors;
   vector<unsigned char> InDetTrackParticlesAux_numberOfSCTSpoiltHits;
   vector<unsigned char> InDetTrackParticlesAux_numberOfTRTHits;
   vector<unsigned char> InDetTrackParticlesAux_numberOfTRTOutliers;
   vector<unsigned char> InDetTrackParticlesAux_numberOfTRTHoles;
   vector<unsigned char> InDetTrackParticlesAux_numberOfTRTHighThresholdHits;
   vector<unsigned char> InDetTrackParticlesAux_numberOfTRTHighThresholdHitsTotal;
   vector<unsigned char> InDetTrackParticlesAux_numberOfTRTHighThresholdOutliers;
   vector<unsigned char> InDetTrackParticlesAux_numberOfTRTDeadStraws;
   vector<unsigned char> InDetTrackParticlesAux_numberOfTRTTubeHits;
   vector<unsigned char> InDetTrackParticlesAux_numberOfTRTXenonHits;
   vector<unsigned char> InDetTrackParticlesAux_numberOfTRTSharedHits;
   vector<unsigned char> InDetTrackParticlesAux_numberOfPrecisionLayers;
   vector<unsigned char> InDetTrackParticlesAux_numberOfPrecisionHoleLayers;
   vector<unsigned char> InDetTrackParticlesAux_numberOfPhiLayers;
   vector<unsigned char> InDetTrackParticlesAux_numberOfPhiHoleLayers;
   vector<unsigned char> InDetTrackParticlesAux_numberOfTriggerEtaLayers;
   vector<unsigned char> InDetTrackParticlesAux_numberOfTriggerEtaHoleLayers;
   vector<unsigned char> InDetTrackParticlesAux_numberOfOutliersOnTrack;
   vector<unsigned char> InDetTrackParticlesAux_standardDeviationOfChi2OS;
   vector<float>   InDetTrackParticlesAux_eProbabilityComb;
   vector<float>   InDetTrackParticlesAux_eProbabilityHT;
   vector<float>   InDetTrackParticlesAux_pixeldEdx;
   vector<unsigned char> InDetTrackParticlesAux_numberOfUsedHitsdEdx;
   vector<unsigned char> InDetTrackParticlesAux_numberOfIBLOverflowsdEdx;
//.*DataVector<xAOD::Vertex_v1> > > InDetTrackParticlesAux_vertexLink;
   vector<float>   InDetTrackParticlesAux_TRTTrackOccupancy;
 //xAOD::TrackStateValidationAuxContainer_v1 *PixelMSOSsAux_;
 //xAOD::TrackStateValidationAuxContainer_v1 *PixelMSOSsAux_xAOD__AuxContainerBase;
  
//.*DataVector<xAOD::TauTrack_v1> *TauTracks;
 //xAOD::TauJetAuxContainer_v3 *TauJetsAux_;
 //xAOD::TauJetAuxContainer_v3 *TauJetsAux_xAOD__AuxContainerBase;
   vector<float>   TauJetsAux_pt;
   vector<float>   TauJetsAux_eta;
   vector<float>   TauJetsAux_phi;
   vector<float>   TauJetsAux_m;
   vector<float>   TauJetsAux_ptJetSeed;
   vector<float>   TauJetsAux_etaJetSeed;
   vector<float>   TauJetsAux_phiJetSeed;
   vector<float>   TauJetsAux_mJetSeed;
   vector<float>   TauJetsAux_ptDetectorAxis;
   vector<float>   TauJetsAux_etaDetectorAxis;
   vector<float>   TauJetsAux_phiDetectorAxis;
   vector<float>   TauJetsAux_mDetectorAxis;
   vector<float>   TauJetsAux_ptIntermediateAxis;
   vector<float>   TauJetsAux_etaIntermediateAxis;
   vector<float>   TauJetsAux_phiIntermediateAxis;
   vector<float>   TauJetsAux_mIntermediateAxis;
   vector<float>   TauJetsAux_ptTauEnergyScale;
   vector<float>   TauJetsAux_etaTauEnergyScale;
   vector<float>   TauJetsAux_phiTauEnergyScale;
   vector<float>   TauJetsAux_mTauEnergyScale;
   vector<float>   TauJetsAux_ptTauEtaCalib;
   vector<float>   TauJetsAux_etaTauEtaCalib;
   vector<float>   TauJetsAux_phiTauEtaCalib;
   vector<float>   TauJetsAux_mTauEtaCalib;
   vector<float>   TauJetsAux_ptPanTauCellBasedProto;
   vector<float>   TauJetsAux_etaPanTauCellBasedProto;
   vector<float>   TauJetsAux_phiPanTauCellBasedProto;
   vector<float>   TauJetsAux_mPanTauCellBasedProto;
   vector<float>   TauJetsAux_ptPanTauCellBased;
   vector<float>   TauJetsAux_etaPanTauCellBased;
   vector<float>   TauJetsAux_phiPanTauCellBased;
   vector<float>   TauJetsAux_mPanTauCellBased;
   vector<float>   TauJetsAux_ptTrigCaloOnly;
   vector<float>   TauJetsAux_etaTrigCaloOnly;
   vector<float>   TauJetsAux_phiTrigCaloOnly;
   vector<float>   TauJetsAux_mTrigCaloOnly;
   vector<float>   TauJetsAux_ptFinalCalib;
   vector<float>   TauJetsAux_etaFinalCalib;
   vector<float>   TauJetsAux_phiFinalCalib;
   vector<float>   TauJetsAux_mFinalCalib;
   vector<unsigned int> TauJetsAux_ROIWord;
   vector<float>   TauJetsAux_charge;
   vector<unsigned int> TauJetsAux_isTauFlags;
   vector<float>   TauJetsAux_BDTJetScore;
   vector<float>   TauJetsAux_BDTEleScore;
   vector<float>   TauJetsAux_EleMatchLikelihoodScore;
   vector<float>   TauJetsAux_BDTJetScoreSigTrans;
//.*DataVector<xAOD::TauTrack_v1> > > > TauJetsAux_tauTrackLinks;
   vector<int>     TauJetsAux_nChargedTracks;
   vector<int>     TauJetsAux_nIsolatedTracks;
//.*DataVector<xAOD::IParticle> > > > TauJetsAux_clusterLinks;
//.*DataVector<xAOD::IParticle> > > > TauJetsAux_pi0Links;
   vector<int>     TauJetsAux_trackFilterProngs;
   vector<int>     TauJetsAux_trackFilterQuality;
   vector<float>   TauJetsAux_pi0ConeDR;
   vector<float>   TauJetsAux_ipZ0SinThetaSigLeadTrk;
   vector<float>   TauJetsAux_etOverPtLeadTrk;
   vector<float>   TauJetsAux_leadTrkPt;
   vector<float>   TauJetsAux_ipSigLeadTrk;
   vector<float>   TauJetsAux_massTrkSys;
   vector<float>   TauJetsAux_trkWidth2;
   vector<float>   TauJetsAux_trFlightPathSig;
   vector<int>     TauJetsAux_numCells;
   vector<int>     TauJetsAux_numTopoClusters;
   vector<float>   TauJetsAux_numEffTopoClusters;
   vector<float>   TauJetsAux_topoInvMass;
   vector<float>   TauJetsAux_effTopoInvMass;
   vector<float>   TauJetsAux_topoMeanDeltaR;
   vector<float>   TauJetsAux_effTopoMeanDeltaR;
   vector<float>   TauJetsAux_EMRadius;
   vector<float>   TauJetsAux_hadRadius;
   vector<float>   TauJetsAux_etEMAtEMScale;
   vector<float>   TauJetsAux_etHadAtEMScale;
   vector<float>   TauJetsAux_isolFrac;
   vector<float>   TauJetsAux_centFrac;
   vector<float>   TauJetsAux_stripWidth2;
   vector<int>     TauJetsAux_nStrip;
   vector<float>   TauJetsAux_trkAvgDist;
   vector<float>   TauJetsAux_trkRmsDist;
   vector<float>   TauJetsAux_lead2ClusterEOverAllClusterE;
   vector<float>   TauJetsAux_lead3ClusterEOverAllClusterE;
   vector<float>   TauJetsAux_caloIso;
   vector<float>   TauJetsAux_caloIsoCorrected;
   vector<float>   TauJetsAux_dRmax;
   vector<float>   TauJetsAux_secMaxStripEt;
   vector<float>   TauJetsAux_sumEMCellEtOverLeadTrkPt;
   vector<float>   TauJetsAux_hadLeakEt;
   vector<float>   TauJetsAux_TESOffset;
   vector<float>   TauJetsAux_TESCalibConstant;
   vector<float>   TauJetsAux_cellBasedEnergyRing1;
   vector<float>   TauJetsAux_cellBasedEnergyRing2;
   vector<float>   TauJetsAux_cellBasedEnergyRing3;
   vector<float>   TauJetsAux_cellBasedEnergyRing4;
   vector<float>   TauJetsAux_cellBasedEnergyRing5;
   vector<float>   TauJetsAux_cellBasedEnergyRing6;
   vector<float>   TauJetsAux_cellBasedEnergyRing7;
   vector<float>   TauJetsAux_TRT_NHT_OVER_NLT;
   vector<float>   TauJetsAux_TauJetVtxFraction;
   vector<int>     TauJetsAux_nCharged;
   vector<float>   TauJetsAux_mEflowApprox;
   vector<float>   TauJetsAux_ptRatioEflowApprox;
   vector<float>   TauJetsAux_innerTrkAvgDist;
   vector<float>   TauJetsAux_SumPtTrkFrac;
   vector<float>   TauJetsAux_etOverPtLeadTrkCorrected;
   vector<float>   TauJetsAux_ipSigLeadTrkCorrected;
   vector<float>   TauJetsAux_trFlightPathSigCorrected;
   vector<float>   TauJetsAux_massTrkSysCorrected;
   vector<float>   TauJetsAux_dRmaxCorrected;
   vector<float>   TauJetsAux_ChPiEMEOverCaloEMECorrected;
   vector<float>   TauJetsAux_EMPOverTrkSysPCorrected;
   vector<float>   TauJetsAux_ptRatioEflowApproxCorrected;
   vector<float>   TauJetsAux_mEflowApproxCorrected;
   vector<float>   TauJetsAux_centFracCorrected;
   vector<float>   TauJetsAux_innerTrkAvgDistCorrected;
   vector<float>   TauJetsAux_SumPtTrkFracCorrected;
   vector<float>   TauJetsAux_PSSFraction;
   vector<float>   TauJetsAux_ChPiEMEOverCaloEME;
   vector<float>   TauJetsAux_EMPOverTrkSysP;
   vector<int>     TauJetsAux_PanTau_isPanTauCandidate;
   vector<int>     TauJetsAux_PanTau_DecayModeProto;
   vector<int>     TauJetsAux_PanTau_DecayMode;
   vector<float>   TauJetsAux_PanTau_BDTValue_1p0n_vs_1p1n;
   vector<float>   TauJetsAux_PanTau_BDTValue_1p1n_vs_1pXn;
   vector<float>   TauJetsAux_PanTau_BDTValue_3p0n_vs_3pXn;
   vector<int>     TauJetsAux_PanTau_BDTVar_Basic_NNeutralConsts;
   vector<float>   TauJetsAux_PanTau_BDTVar_Charged_JetMoment_EtDRxTotalEt;
   vector<float>   TauJetsAux_PanTau_BDTVar_Charged_StdDev_Et_WrtEtAllConsts;
   vector<float>   TauJetsAux_PanTau_BDTVar_Neutral_HLV_SumM;
   vector<float>   TauJetsAux_PanTau_BDTVar_Neutral_PID_BDTValues_BDTSort_1;
   vector<float>   TauJetsAux_PanTau_BDTVar_Neutral_PID_BDTValues_BDTSort_2;
   vector<float>   TauJetsAux_PanTau_BDTVar_Neutral_Ratio_1stBDTEtOverEtAllConsts;
   vector<float>   TauJetsAux_PanTau_BDTVar_Neutral_Ratio_EtOverEtAllConsts;
   vector<float>   TauJetsAux_PanTau_BDTVar_Neutral_Shots_NPhotonsInSeed;
   vector<float>   TauJetsAux_PanTau_BDTVar_Combined_DeltaR1stNeutralTo1stCharged;
   vector<float>   TauJetsAux_PanTau_BDTVar_Charged_HLV_SumM;
   vector<float>   TauJetsAux_RNNJetScore;
   vector<float>   TauJetsAux_RNNJetScoreSigTrans;
   
   vector<float>   *InDetTrackParticlesAuxDyn_TrkIBLX;
   vector<unsigned char> *InDetTrackParticlesAuxDyn_TRTdEdxUsedHits;
   vector<float>   *InDetTrackParticlesAuxDyn_TrkIBLY;
   vector<float>   *InDetTrackParticlesAuxDyn_TrkIBLZ;
   vector<float>   *InDetTrackParticlesAuxDyn_TRTdEdx;
   vector<float>   *InDetTrackParticlesAuxDyn_TrkBLX;
   vector<float>   *InDetTrackParticlesAuxDyn_TrkBLY;
   vector<float>   *InDetTrackParticlesAuxDyn_TrkBLZ;
   vector<int>     *InDetTrackParticlesAuxDyn_nBC_meas;
   vector<float>   *InDetTrackParticlesAuxDyn_TrkL1X;
   vector<float>   *InDetTrackParticlesAuxDyn_TrkL1Y;
   Int_t           InDetTrackParticlesAuxDyn_trackLink_;
//   UInt_t          InDetTrackParticlesAuxDyn_trackLink_m_persKey[kMaxInDetTrackParticlesAuxDyn_trackLink];   //[InDetTrackParticlesAuxDyn.trackLink_]
 //  UInt_t          InDetTrackParticlesAuxDyn_trackLink_m_persIndex[kMaxInDetTrackParticlesAuxDyn_trackLink];   //[InDetTrackParticlesAuxDyn.trackLink_]
   vector<float>   *InDetTrackParticlesAuxDyn_TrkL1Z;
   Int_t           InDetTrackParticlesAuxDyn_truthParticleLink_;
   UInt_t          InDetTrackParticlesAuxDyn_truthParticleLink_m_persKey[kMaxInDetTrackParticlesAuxDyn_truthParticleLink];   //[InDetTrackParticlesAuxDyn.truthParticleLink_]
   UInt_t          InDetTrackParticlesAuxDyn_truthParticleLink_m_persIndex[kMaxInDetTrackParticlesAuxDyn_truthParticleLink];   //[InDetTrackParticlesAuxDyn.truthParticleLink_]
   vector<float>   *InDetTrackParticlesAuxDyn_TrkL2X;
   vector<float>   *InDetTrackParticlesAuxDyn_truthMatchProbability;
   vector<float>   *InDetTrackParticlesAuxDyn_TrkL2Y;
   vector<int>     *InDetTrackParticlesAuxDyn_truthType;
   vector<int>     *InDetTrackParticlesAuxDyn_truthOrigin;
   vector<float>   *InDetTrackParticlesAuxDyn_TrkL2Z;
   vector<vector<int> > *InDetTrackParticlesAuxDyn_measurement_region;
   vector<vector<int> > *InDetTrackParticlesAuxDyn_measurement_det;
   vector<vector<int> > *InDetTrackParticlesAuxDyn_measurement_iLayer;
   vector<vector<float> > *InDetTrackParticlesAuxDyn_hitResiduals_residualLocX;
   vector<vector<float> > *InDetTrackParticlesAuxDyn_hitResiduals_pullLocX;
   vector<vector<float> > *InDetTrackParticlesAuxDyn_hitResiduals_residualLocY;
   vector<vector<float> > *InDetTrackParticlesAuxDyn_hitResiduals_pullLocY;
   vector<vector<int> > *InDetTrackParticlesAuxDyn_hitResiduals_phiWidth;
   vector<vector<int> > *InDetTrackParticlesAuxDyn_hitResiduals_etaWidth;
   vector<vector<int> > *InDetTrackParticlesAuxDyn_measurement_type;
   vector<float>   *InDetTrackParticlesAuxDyn_d0err;
   vector<float>   *InDetTrackParticlesAuxDyn_z0err;
   vector<float>   *InDetTrackParticlesAuxDyn_phierr;
   vector<float>   *InDetTrackParticlesAuxDyn_thetaerr;
   vector<float>   *InDetTrackParticlesAuxDyn_qopterr;
   
   // List of branches
   TBranch        *b_McEventInfo;   //!
   TBranch        *b_BCM_RDOs;   //!
   TBranch        *b_MBTSContainer;   //!
   TBranch        *b_EventInfoAux_runNumber;   //!
   TBranch        *b_EventInfoAux_eventNumber;   //!
   TBranch        *b_EventInfoAux_lumiBlock;   //!
   TBranch        *b_EventInfoAux_timeStamp;   //!
   TBranch        *b_EventInfoAux_timeStampNSOffset;   //!
   TBranch        *b_EventInfoAux_bcid;   //!
   TBranch        *b_EventInfoAux_detectorMask0;   //!
   TBranch        *b_EventInfoAux_detectorMask1;   //!
   TBranch        *b_EventInfoAux_detectorMask2;   //!
   TBranch        *b_EventInfoAux_detectorMask3;   //!
   TBranch        *b_EventInfoAux_eventTypeBitmask;   //!
   TBranch        *b_EventInfoAux_statusElement;   //!
   TBranch        *b_EventInfoAux_extendedLevel1ID;   //!
   TBranch        *b_EventInfoAux_level1TriggerType;   //!
   TBranch        *b_EventInfoAux_streamTagNames;   //!
   TBranch        *b_EventInfoAux_streamTagTypes;   //!
   TBranch        *b_EventInfoAux_streamTagObeysLumiblock;   //!
   TBranch        *b_EventInfoAux_actualInteractionsPerCrossing;   //!
   TBranch        *b_EventInfoAux_averageInteractionsPerCrossing;   //!
   TBranch        *b_EventInfoAux_pixelFlags;   //!
   TBranch        *b_EventInfoAux_sctFlags;   //!
   TBranch        *b_EventInfoAux_trtFlags;   //!
   TBranch        *b_EventInfoAux_larFlags;   //!
   TBranch        *b_EventInfoAux_tileFlags;   //!
   TBranch        *b_EventInfoAux_muonFlags;   //!
   TBranch        *b_EventInfoAux_forwardDetFlags;   //!
   TBranch        *b_EventInfoAux_coreFlags;   //!
   TBranch        *b_EventInfoAux_backgroundFlags;   //!
   TBranch        *b_EventInfoAux_lumiFlags;   //!
   TBranch        *b_EventInfoAux_beamPosX;   //!
   TBranch        *b_EventInfoAux_beamPosY;   //!
   TBranch        *b_EventInfoAux_beamPosZ;   //!
   TBranch        *b_EventInfoAux_beamPosSigmaX;   //!
   TBranch        *b_EventInfoAux_beamPosSigmaY;   //!
   TBranch        *b_EventInfoAux_beamPosSigmaZ;   //!
   TBranch        *b_EventInfoAux_beamPosSigmaXY;   //!
   TBranch        *b_EventInfoAux_beamTiltXZ;   //!
   TBranch        *b_EventInfoAux_beamTiltYZ;   //!
   TBranch        *b_EventInfoAux_beamStatus;   //!
   TBranch        *b_EventInfo;   //!
   TBranch        *b_Electrons;   //!
   TBranch        *b_PrimaryVertices;   //!
   TBranch        *b_TruthParticlesAux_pdgId;   //!
   TBranch        *b_TruthParticlesAux_barcode;   //!
   TBranch        *b_TruthParticlesAux_status;   //!
   TBranch        *b_TruthParticlesAux_px;   //!
   TBranch        *b_TruthParticlesAux_py;   //!
   TBranch        *b_TruthParticlesAux_pz;   //!
   TBranch        *b_TruthParticlesAux_e;   //!
   TBranch        *b_TruthParticlesAux_m;   //!
   TBranch        *b_ElectronsAux_pt;   //!
   TBranch        *b_ElectronsAux_eta;   //!
   TBranch        *b_ElectronsAux_phi;   //!
   TBranch        *b_ElectronsAux_m;   //!
   TBranch        *b_ElectronsAux_author;   //!
   TBranch        *b_ElectronsAux_OQ;   //!
   TBranch        *b_ElectronsAux_f1;   //!
   TBranch        *b_ElectronsAux_f3;   //!
   TBranch        *b_ElectronsAux_f1core;   //!
   TBranch        *b_ElectronsAux_f3core;   //!
   TBranch        *b_ElectronsAux_weta1;   //!
   TBranch        *b_ElectronsAux_weta2;   //!
   TBranch        *b_ElectronsAux_fracs1;   //!
   TBranch        *b_ElectronsAux_wtots1;   //!
   TBranch        *b_ElectronsAux_e277;   //!
   TBranch        *b_ElectronsAux_Reta;   //!
   TBranch        *b_ElectronsAux_Rphi;   //!
   TBranch        *b_ElectronsAux_Eratio;   //!
   TBranch        *b_ElectronsAux_Rhad;   //!
   TBranch        *b_ElectronsAux_Rhad1;   //!
   TBranch        *b_ElectronsAux_DeltaE;   //!
   TBranch        *b_ElectronsAux_charge;   //!
   TBranch        *b_ElectronsAux_deltaEta0;   //!
   TBranch        *b_ElectronsAux_deltaEta1;   //!
   TBranch        *b_ElectronsAux_deltaEta2;   //!
   TBranch        *b_ElectronsAux_deltaEta3;   //!
   TBranch        *b_ElectronsAux_deltaPhi0;   //!
   TBranch        *b_ElectronsAux_deltaPhi1;   //!
   TBranch        *b_ElectronsAux_deltaPhi2;   //!
   TBranch        *b_ElectronsAux_deltaPhi3;   //!
   TBranch        *b_ElectronsAux_deltaPhiFromLastMeasurement;   //!
   TBranch        *b_ElectronsAux_deltaPhiRescaled0;   //!
   TBranch        *b_ElectronsAux_deltaPhiRescaled1;   //!
   TBranch        *b_ElectronsAux_deltaPhiRescaled2;   //!
   TBranch        *b_ElectronsAux_deltaPhiRescaled3;   //!
   TBranch        *b_PixelClusters;   //!
   TBranch        *b_TauTracksAux_pt;   //!
   TBranch        *b_TauTracksAux_eta;   //!
   TBranch        *b_TauTracksAux_phi;   //!
   TBranch        *b_TauTracksAux_flagSet;   //!
   TBranch        *b_TauTracksAux_bdtScores;   //!
   TBranch        *b_MuonsAux_pt;   //!
   TBranch        *b_MuonsAux_eta;   //!
   TBranch        *b_MuonsAux_phi;   //!
   TBranch        *b_MuonsAux_charge;   //!
   TBranch        *b_MuonsAux_allAuthors;   //!
   TBranch        *b_MuonsAux_author;   //!
   TBranch        *b_MuonsAux_muonType;   //!
   TBranch        *b_MuonsAux_quality;   //!
   TBranch        *b_MuonsAux_numberOfPrecisionLayers;   //!
   TBranch        *b_MuonsAux_numberOfPrecisionHoleLayers;   //!
   TBranch        *b_MuonsAux_numberOfPhiLayers;   //!
   TBranch        *b_MuonsAux_numberOfPhiHoleLayers;   //!
   TBranch        *b_MuonsAux_numberOfTriggerEtaLayers;   //!
   TBranch        *b_MuonsAux_numberOfTriggerEtaHoleLayers;   //!
   TBranch        *b_MuonsAux_primarySector;   //!
   TBranch        *b_MuonsAux_secondarySector;   //!
   TBranch        *b_MuonsAux_innerSmallHits;   //!
   TBranch        *b_MuonsAux_innerLargeHits;   //!
   TBranch        *b_MuonsAux_middleSmallHits;   //!
   TBranch        *b_MuonsAux_middleLargeHits;   //!
   TBranch        *b_MuonsAux_outerSmallHits;   //!
   TBranch        *b_MuonsAux_outerLargeHits;   //!
   TBranch        *b_MuonsAux_extendedSmallHits;   //!
   TBranch        *b_MuonsAux_extendedLargeHits;   //!
   TBranch        *b_MuonsAux_innerSmallHoles;   //!
   TBranch        *b_MuonsAux_innerLargeHoles;   //!
   TBranch        *b_MuonsAux_middleSmallHoles;   //!
   TBranch        *b_MuonsAux_middleLargeHoles;   //!
   TBranch        *b_MuonsAux_outerSmallHoles;   //!
   TBranch        *b_MuonsAux_outerLargeHoles;   //!
   TBranch        *b_MuonsAux_extendedSmallHoles;   //!
   TBranch        *b_MuonsAux_extendedLargeHoles;   //!
   TBranch        *b_MuonsAux_phiLayer1Hits;   //!
   TBranch        *b_MuonsAux_phiLayer2Hits;   //!
   TBranch        *b_MuonsAux_phiLayer3Hits;   //!
   TBranch        *b_MuonsAux_phiLayer4Hits;   //!
   TBranch        *b_MuonsAux_etaLayer1Hits;   //!
   TBranch        *b_MuonsAux_etaLayer2Hits;   //!
   TBranch        *b_MuonsAux_etaLayer3Hits;   //!
   TBranch        *b_MuonsAux_etaLayer4Hits;   //!
   TBranch        *b_MuonsAux_phiLayer1Holes;   //!
   TBranch        *b_MuonsAux_phiLayer2Holes;   //!
   TBranch        *b_MuonsAux_phiLayer3Holes;   //!
   TBranch        *b_MuonsAux_phiLayer4Holes;   //!
   TBranch        *b_MuonsAux_etaLayer1Holes;   //!
   TBranch        *b_MuonsAux_etaLayer2Holes;   //!
   TBranch        *b_MuonsAux_etaLayer3Holes;   //!
   TBranch        *b_MuonsAux_etaLayer4Holes;   //!
   TBranch        *b_MuonsAux_phiLayer1RPCHits;   //!
   TBranch        *b_MuonsAux_phiLayer2RPCHits;   //!
   TBranch        *b_MuonsAux_phiLayer3RPCHits;   //!
   TBranch        *b_MuonsAux_phiLayer4RPCHits;   //!
   TBranch        *b_MuonsAux_etaLayer1RPCHits;   //!
   TBranch        *b_MuonsAux_etaLayer2RPCHits;   //!
   TBranch        *b_MuonsAux_etaLayer3RPCHits;   //!
   TBranch        *b_MuonsAux_etaLayer4RPCHits;   //!
   TBranch        *b_MuonsAux_phiLayer1RPCHoles;   //!
   TBranch        *b_MuonsAux_phiLayer2RPCHoles;   //!
   TBranch        *b_MuonsAux_phiLayer3RPCHoles;   //!
   TBranch        *b_MuonsAux_phiLayer4RPCHoles;   //!
   TBranch        *b_MuonsAux_etaLayer1RPCHoles;   //!
   TBranch        *b_MuonsAux_etaLayer2RPCHoles;   //!
   TBranch        *b_MuonsAux_etaLayer3RPCHoles;   //!
   TBranch        *b_MuonsAux_etaLayer4RPCHoles;   //!
   TBranch        *b_MuonsAux_phiLayer1TGCHits;   //!
   TBranch        *b_MuonsAux_phiLayer2TGCHits;   //!
   TBranch        *b_MuonsAux_phiLayer3TGCHits;   //!
   TBranch        *b_MuonsAux_phiLayer4TGCHits;   //!
   TBranch        *b_MuonsAux_etaLayer1TGCHits;   //!
   TBranch        *b_MuonsAux_etaLayer2TGCHits;   //!
   TBranch        *b_MuonsAux_etaLayer3TGCHits;   //!
   TBranch        *b_MuonsAux_etaLayer4TGCHits;   //!
   TBranch        *b_MuonsAux_phiLayer1TGCHoles;   //!
   TBranch        *b_MuonsAux_phiLayer2TGCHoles;   //!
   TBranch        *b_MuonsAux_phiLayer3TGCHoles;   //!
   TBranch        *b_MuonsAux_phiLayer4TGCHoles;   //!
   TBranch        *b_MuonsAux_etaLayer1TGCHoles;   //!
   TBranch        *b_MuonsAux_etaLayer2TGCHoles;   //!
   TBranch        *b_MuonsAux_etaLayer3TGCHoles;   //!
   TBranch        *b_MuonsAux_etaLayer4TGCHoles;   //!
   TBranch        *b_MuonsAux_cscEtaHits;   //!
   TBranch        *b_MuonsAux_cscUnspoiledEtaHits;   //!
   TBranch        *b_MuonsAux_etcone20;   //!
   TBranch        *b_MuonsAux_etcone30;   //!
   TBranch        *b_MuonsAux_etcone40;   //!
   TBranch        *b_MuonsAux_ptcone20;   //!
   TBranch        *b_MuonsAux_ptcone30;   //!
   TBranch        *b_MuonsAux_ptcone40;   //!
   TBranch        *b_MuonsAux_ptvarcone20;   //!
   TBranch        *b_MuonsAux_ptvarcone30;   //!
   TBranch        *b_MuonsAux_ptvarcone40;   //!
   TBranch        *b_MuonsAux_energyLossType;   //!
   TBranch        *b_MuonsAux_spectrometerFieldIntegral;   //!
   TBranch        *b_MuonsAux_scatteringCurvatureSignificance;   //!
   TBranch        *b_MuonsAux_scatteringNeighbourSignificance;   //!
   TBranch        *b_MuonsAux_momentumBalanceSignificance;   //!
   TBranch        *b_MuonsAux_segmentDeltaEta;   //!
   TBranch        *b_MuonsAux_segmentDeltaPhi;   //!
   TBranch        *b_MuonsAux_segmentChi2OverDoF;   //!
   TBranch        *b_MuonsAux_t0;   //!
   TBranch        *b_MuonsAux_beta;   //!
   TBranch        *b_MuonsAux_annBarrel;   //!
   TBranch        *b_MuonsAux_annEndCap;   //!
   TBranch        *b_MuonsAux_innAngle;   //!
   TBranch        *b_MuonsAux_midAngle;   //!
   TBranch        *b_MuonsAux_msInnerMatchChi2;   //!
   TBranch        *b_MuonsAux_meanDeltaADCCountsMDT;   //!
   TBranch        *b_MuonsAux_CaloLRLikelihood;   //!
   TBranch        *b_MuonsAux_EnergyLoss;   //!
   TBranch        *b_MuonsAux_ParamEnergyLoss;   //!
   TBranch        *b_MuonsAux_MeasEnergyLoss;   //!
   TBranch        *b_MuonsAux_EnergyLossSigma;   //!
   TBranch        *b_MuonsAux_ParamEnergyLossSigmaPlus;   //!
   TBranch        *b_MuonsAux_ParamEnergyLossSigmaMinus;   //!
   TBranch        *b_MuonsAux_MeasEnergyLossSigma;   //!
   TBranch        *b_MuonsAux_msInnerMatchDOF;   //!
   TBranch        *b_MuonsAux_msOuterMatchDOF;   //!
   TBranch        *b_MuonsAux_CaloMuonIDTag;   //!
   TBranch        *b_TauJets;   //!
   TBranch        *b_Muons;   //!
   TBranch        *b_TruthEvents;   //!
   TBranch        *b_GSFTrackParticlesAux_d0;   //!
   TBranch        *b_GSFTrackParticlesAux_z0;   //!
   TBranch        *b_GSFTrackParticlesAux_phi;   //!
   TBranch        *b_GSFTrackParticlesAux_theta;   //!
   TBranch        *b_GSFTrackParticlesAux_qOverP;   //!
   TBranch        *b_GSFTrackParticlesAux_definingParametersCovMatrix;   //!
   TBranch        *b_GSFTrackParticlesAux_vx;   //!
   TBranch        *b_GSFTrackParticlesAux_vy;   //!
   TBranch        *b_GSFTrackParticlesAux_vz;   //!
   TBranch        *b_GSFTrackParticlesAux_radiusOfFirstHit;   //!
   TBranch        *b_GSFTrackParticlesAux_identifierOfFirstHit;   //!
   TBranch        *b_GSFTrackParticlesAux_beamlineTiltX;   //!
   TBranch        *b_GSFTrackParticlesAux_beamlineTiltY;   //!
   TBranch        *b_GSFTrackParticlesAux_hitPattern;   //!
   TBranch        *b_GSFTrackParticlesAux_chiSquared;   //!
   TBranch        *b_GSFTrackParticlesAux_numberDoF;   //!
   TBranch        *b_GSFTrackParticlesAux_trackFitter;   //!
   TBranch        *b_GSFTrackParticlesAux_particleHypothesis;   //!
   TBranch        *b_GSFTrackParticlesAux_trackProperties;   //!
   TBranch        *b_GSFTrackParticlesAux_patternRecoInfo;   //!
   TBranch        *b_GSFTrackParticlesAux_numberOfContribPixelLayers;   //!
   TBranch        *b_GSFTrackParticlesAux_numberOfInnermostPixelLayerHits;   //!
   TBranch        *b_GSFTrackParticlesAux_numberOfInnermostPixelLayerOutliers;   //!
   TBranch        *b_GSFTrackParticlesAux_numberOfInnermostPixelLayerSharedHits;   //!
   TBranch        *b_GSFTrackParticlesAux_numberOfInnermostPixelLayerSplitHits;   //!
   TBranch        *b_GSFTrackParticlesAux_expectInnermostPixelLayerHit;   //!
   TBranch        *b_GSFTrackParticlesAux_numberOfNextToInnermostPixelLayerHits;   //!
   TBranch        *b_GSFTrackParticlesAux_numberOfNextToInnermostPixelLayerOutliers;   //!
   TBranch        *b_GSFTrackParticlesAux_numberOfNextToInnermostPixelLayerSharedHits;   //!
   TBranch        *b_GSFTrackParticlesAux_numberOfNextToInnermostPixelLayerSplitHits;   //!
   TBranch        *b_GSFTrackParticlesAux_expectNextToInnermostPixelLayerHit;   //!
   TBranch        *b_GSFTrackParticlesAux_numberOfPixelHits;   //!
   TBranch        *b_GSFTrackParticlesAux_numberOfPixelOutliers;   //!
   TBranch        *b_GSFTrackParticlesAux_numberOfPixelHoles;   //!
   TBranch        *b_GSFTrackParticlesAux_numberOfPixelSharedHits;   //!
   TBranch        *b_GSFTrackParticlesAux_numberOfPixelSplitHits;   //!
   TBranch        *b_GSFTrackParticlesAux_numberOfGangedPixels;   //!
   TBranch        *b_GSFTrackParticlesAux_numberOfGangedFlaggedFakes;   //!
   TBranch        *b_GSFTrackParticlesAux_numberOfPixelDeadSensors;   //!
   TBranch        *b_GSFTrackParticlesAux_numberOfPixelSpoiltHits;   //!
   TBranch        *b_GSFTrackParticlesAux_numberOfDBMHits;   //!
   TBranch        *b_GSFTrackParticlesAux_numberOfSCTHits;   //!
   TBranch        *b_GSFTrackParticlesAux_numberOfSCTOutliers;   //!
   TBranch        *b_GSFTrackParticlesAux_numberOfSCTHoles;   //!
   TBranch        *b_GSFTrackParticlesAux_numberOfSCTDoubleHoles;   //!
   TBranch        *b_GSFTrackParticlesAux_numberOfSCTSharedHits;   //!
   TBranch        *b_GSFTrackParticlesAux_numberOfSCTDeadSensors;   //!
   TBranch        *b_GSFTrackParticlesAux_numberOfSCTSpoiltHits;   //!
   TBranch        *b_GSFTrackParticlesAux_numberOfTRTHits;   //!
   TBranch        *b_GSFTrackParticlesAux_numberOfTRTOutliers;   //!
   TBranch        *b_GSFTrackParticlesAux_numberOfTRTHoles;   //!
   TBranch        *b_GSFTrackParticlesAux_numberOfTRTHighThresholdHits;   //!
   TBranch        *b_GSFTrackParticlesAux_numberOfTRTHighThresholdHitsTotal;   //!
   TBranch        *b_GSFTrackParticlesAux_numberOfTRTHighThresholdOutliers;   //!
   TBranch        *b_GSFTrackParticlesAux_numberOfTRTDeadStraws;   //!
   TBranch        *b_GSFTrackParticlesAux_numberOfTRTTubeHits;   //!
   TBranch        *b_GSFTrackParticlesAux_numberOfTRTXenonHits;   //!
   TBranch        *b_GSFTrackParticlesAux_numberOfTRTSharedHits;   //!
   TBranch        *b_GSFTrackParticlesAux_numberOfPrecisionLayers;   //!
   TBranch        *b_GSFTrackParticlesAux_numberOfPrecisionHoleLayers;   //!
   TBranch        *b_GSFTrackParticlesAux_numberOfPhiLayers;   //!
   TBranch        *b_GSFTrackParticlesAux_numberOfPhiHoleLayers;   //!
   TBranch        *b_GSFTrackParticlesAux_numberOfTriggerEtaLayers;   //!
   TBranch        *b_GSFTrackParticlesAux_numberOfTriggerEtaHoleLayers;   //!
   TBranch        *b_GSFTrackParticlesAux_numberOfOutliersOnTrack;   //!
   TBranch        *b_GSFTrackParticlesAux_standardDeviationOfChi2OS;   //!
   TBranch        *b_GSFTrackParticlesAux_eProbabilityComb;   //!
   TBranch        *b_GSFTrackParticlesAux_eProbabilityHT;   //!
   TBranch        *b_GSFTrackParticlesAux_pixeldEdx;   //!
   TBranch        *b_GSFTrackParticlesAux_numberOfUsedHitsdEdx;   //!
   TBranch        *b_GSFTrackParticlesAux_numberOfIBLOverflowsdEdx;   //!
   TBranch        *b_GSFTrackParticlesAux_TRTTrackOccupancy;   //!
   TBranch        *b_InDetTrackParticlesAux_d0;   //!
   TBranch        *b_InDetTrackParticlesAux_z0;   //!
   TBranch        *b_InDetTrackParticlesAux_phi;   //!
   TBranch        *b_InDetTrackParticlesAux_theta;   //!
   TBranch        *b_InDetTrackParticlesAux_qOverP;   //!
   TBranch        *b_InDetTrackParticlesAux_definingParametersCovMatrix;   //!
   TBranch        *b_InDetTrackParticlesAux_vx;   //!
   TBranch        *b_InDetTrackParticlesAux_vy;   //!
   TBranch        *b_InDetTrackParticlesAux_vz;   //!
   TBranch        *b_InDetTrackParticlesAux_radiusOfFirstHit;   //!
   TBranch        *b_InDetTrackParticlesAux_identifierOfFirstHit;   //!
   TBranch        *b_InDetTrackParticlesAux_beamlineTiltX;   //!
   TBranch        *b_InDetTrackParticlesAux_beamlineTiltY;   //!
   TBranch        *b_InDetTrackParticlesAux_hitPattern;   //!
   TBranch        *b_InDetTrackParticlesAux_chiSquared;   //!
   TBranch        *b_InDetTrackParticlesAux_numberDoF;   //!
   TBranch        *b_InDetTrackParticlesAux_trackFitter;   //!
   TBranch        *b_InDetTrackParticlesAux_particleHypothesis;   //!
   TBranch        *b_InDetTrackParticlesAux_trackProperties;   //!
   TBranch        *b_InDetTrackParticlesAux_patternRecoInfo;   //!
   TBranch        *b_InDetTrackParticlesAux_numberOfContribPixelLayers;   //!
   TBranch        *b_InDetTrackParticlesAux_numberOfInnermostPixelLayerHits;   //!
   TBranch        *b_InDetTrackParticlesAux_numberOfInnermostPixelLayerOutliers;   //!
   TBranch        *b_InDetTrackParticlesAux_numberOfInnermostPixelLayerSharedHits;   //!
   TBranch        *b_InDetTrackParticlesAux_numberOfInnermostPixelLayerSplitHits;   //!
   TBranch        *b_InDetTrackParticlesAux_expectInnermostPixelLayerHit;   //!
   TBranch        *b_InDetTrackParticlesAux_numberOfNextToInnermostPixelLayerHits;   //!
   TBranch        *b_InDetTrackParticlesAux_numberOfNextToInnermostPixelLayerOutliers;   //!
   TBranch        *b_InDetTrackParticlesAux_numberOfNextToInnermostPixelLayerSharedHits;   //!
   TBranch        *b_InDetTrackParticlesAux_numberOfNextToInnermostPixelLayerSplitHits;   //!
   TBranch        *b_InDetTrackParticlesAux_expectNextToInnermostPixelLayerHit;   //!
   TBranch        *b_InDetTrackParticlesAux_numberOfPixelHits;   //!
   TBranch        *b_InDetTrackParticlesAux_numberOfPixelOutliers;   //!
   TBranch        *b_InDetTrackParticlesAux_numberOfPixelHoles;   //!
   TBranch        *b_InDetTrackParticlesAux_numberOfPixelSharedHits;   //!
   TBranch        *b_InDetTrackParticlesAux_numberOfPixelSplitHits;   //!
   TBranch        *b_InDetTrackParticlesAux_numberOfGangedPixels;   //!
   TBranch        *b_InDetTrackParticlesAux_numberOfGangedFlaggedFakes;   //!
   TBranch        *b_InDetTrackParticlesAux_numberOfPixelDeadSensors;   //!
   TBranch        *b_InDetTrackParticlesAux_numberOfPixelSpoiltHits;   //!
   TBranch        *b_InDetTrackParticlesAux_numberOfDBMHits;   //!
   TBranch        *b_InDetTrackParticlesAux_numberOfSCTHits;   //!
   TBranch        *b_InDetTrackParticlesAux_numberOfSCTOutliers;   //!
   TBranch        *b_InDetTrackParticlesAux_numberOfSCTHoles;   //!
   TBranch        *b_InDetTrackParticlesAux_numberOfSCTDoubleHoles;   //!
   TBranch        *b_InDetTrackParticlesAux_numberOfSCTSharedHits;   //!
   TBranch        *b_InDetTrackParticlesAux_numberOfSCTDeadSensors;   //!
   TBranch        *b_InDetTrackParticlesAux_numberOfSCTSpoiltHits;   //!
   TBranch        *b_InDetTrackParticlesAux_numberOfTRTHits;   //!
   TBranch        *b_InDetTrackParticlesAux_numberOfTRTOutliers;   //!
   TBranch        *b_InDetTrackParticlesAux_numberOfTRTHoles;   //!
   TBranch        *b_InDetTrackParticlesAux_numberOfTRTHighThresholdHits;   //!
   TBranch        *b_InDetTrackParticlesAux_numberOfTRTHighThresholdHitsTotal;   //!
   TBranch        *b_InDetTrackParticlesAux_numberOfTRTHighThresholdOutliers;   //!
   TBranch        *b_InDetTrackParticlesAux_numberOfTRTDeadStraws;   //!
   TBranch        *b_InDetTrackParticlesAux_numberOfTRTTubeHits;   //!
   TBranch        *b_InDetTrackParticlesAux_numberOfTRTXenonHits;   //!
   TBranch        *b_InDetTrackParticlesAux_numberOfTRTSharedHits;   //!
   TBranch        *b_InDetTrackParticlesAux_numberOfPrecisionLayers;   //!
   TBranch        *b_InDetTrackParticlesAux_numberOfPrecisionHoleLayers;   //!
   TBranch        *b_InDetTrackParticlesAux_numberOfPhiLayers;   //!
   TBranch        *b_InDetTrackParticlesAux_numberOfPhiHoleLayers;   //!
   TBranch        *b_InDetTrackParticlesAux_numberOfTriggerEtaLayers;   //!
   TBranch        *b_InDetTrackParticlesAux_numberOfTriggerEtaHoleLayers;   //!
   TBranch        *b_InDetTrackParticlesAux_numberOfOutliersOnTrack;   //!
   TBranch        *b_InDetTrackParticlesAux_standardDeviationOfChi2OS;   //!
   TBranch        *b_InDetTrackParticlesAux_eProbabilityComb;   //!
   TBranch        *b_InDetTrackParticlesAux_eProbabilityHT;   //!
   TBranch        *b_InDetTrackParticlesAux_pixeldEdx;   //!
   TBranch        *b_InDetTrackParticlesAux_numberOfUsedHitsdEdx;   //!
   TBranch        *b_InDetTrackParticlesAux_numberOfIBLOverflowsdEdx;   //!
   TBranch        *b_InDetTrackParticlesAux_TRTTrackOccupancy;   //!
   TBranch        *b_PixelMSOSsAux_type;   //!
   TBranch        *b_PixelMSOSsAux_detElementId;   //!
   TBranch        *b_PixelMSOSsAux_detType;   //!
   TBranch        *b_PixelMSOSsAux_localX;   //!
   TBranch        *b_PixelMSOSsAux_localY;   //!
   TBranch        *b_PixelMSOSsAux_localTheta;   //!
   TBranch        *b_PixelMSOSsAux_localPhi;   //!
   TBranch        *b_PixelMSOSsAux_unbiasedResidualX;   //!
   TBranch        *b_PixelMSOSsAux_unbiasedResidualY;   //!
   TBranch        *b_PixelMSOSsAux_unbiasedPullX;   //!
   TBranch        *b_PixelMSOSsAux_unbiasedPullY;   //!
   TBranch        *b_PixelMSOSsAux_biasedResidualX;   //!
   TBranch        *b_PixelMSOSsAux_biasedResidualY;   //!
   TBranch        *b_PixelMSOSsAux_biasedPullX;   //!
   TBranch        *b_PixelMSOSsAux_biasedPullY;   //!
   TBranch        *b_JetInputTruthParticles;   //!
   TBranch        *b_JetInputTruthParticlesNoWZ;   //!
   TBranch        *b_MuonTruthParticles;   //!
   TBranch        *b_TruthLabelBHadronsFinal;   //!
   TBranch        *b_TruthLabelBHadronsInitial;   //!
   TBranch        *b_TruthLabelBQuarksFinal;   //!
   TBranch        *b_TruthLabelCHadronsFinal;   //!
   TBranch        *b_TruthLabelCHadronsInitial;   //!
   TBranch        *b_TruthLabelCQuarksFinal;   //!
   TBranch        *b_TruthLabelHBosons;   //!
   TBranch        *b_TruthLabelPartons;   //!
   TBranch        *b_TruthLabelTQuarksFinal;   //!
   TBranch        *b_TruthLabelTausFinal;   //!
   TBranch        *b_TruthLabelWBosons;   //!
   TBranch        *b_TruthLabelZBosons;   //!
   TBranch        *b_TruthParticles;   //!
   TBranch        *b_egammaTruthParticles;   //!
   TBranch        *b_TruthVertices;   //!
   TBranch        *b_BTagging_AntiKt4EMTopoAux_SV0_significance3D;   //!
   TBranch        *b_BTagging_AntiKt4EMTopoAux_SV1_pb;   //!
   TBranch        *b_BTagging_AntiKt4EMTopoAux_SV1_pu;   //!
   TBranch        *b_BTagging_AntiKt4EMTopoAux_SV1_pc;   //!
   TBranch        *b_BTagging_AntiKt4EMTopoAux_IP2D_pb;   //!
   TBranch        *b_BTagging_AntiKt4EMTopoAux_IP2D_pu;   //!
   TBranch        *b_BTagging_AntiKt4EMTopoAux_IP2D_pc;   //!
   TBranch        *b_BTagging_AntiKt4EMTopoAux_IP3D_pb;   //!
   TBranch        *b_BTagging_AntiKt4EMTopoAux_IP3D_pu;   //!
   TBranch        *b_BTagging_AntiKt4EMTopoAux_IP3D_pc;   //!
   TBranch        *b_BTagging_AntiKt4EMTopoAux_JetFitter_pb;   //!
   TBranch        *b_BTagging_AntiKt4EMTopoAux_JetFitter_pu;   //!
   TBranch        *b_BTagging_AntiKt4EMTopoAux_JetFitter_pc;   //!
   TBranch        *b_BTagging_AntiKt4EMTopoAux_JetFitterCombNN_pb;   //!
   TBranch        *b_BTagging_AntiKt4EMTopoAux_JetFitterCombNN_pu;   //!
   TBranch        *b_BTagging_AntiKt4EMTopoAux_JetFitterCombNN_pc;   //!
   TBranch        *b_BTagging_AntiKt4EMTopoAux_MV1_discriminant;   //!
   TBranch        *b_AntiKt4EMTopoJets;   //!
   TBranch        *b_AntiKt4LCTopoJets;   //!
   TBranch        *b_TruthVerticesAux_id;   //!
   TBranch        *b_TruthVerticesAux_barcode;   //!
   TBranch        *b_TruthVerticesAux_x;   //!
   TBranch        *b_TruthVerticesAux_y;   //!
   TBranch        *b_TruthVerticesAux_z;   //!
   TBranch        *b_TruthVerticesAux_t;   //!
   TBranch        *b_BTagging_AntiKt4EMTopo;   //!
   TBranch        *b_TruthEventsAux_weights;   //!
   TBranch        *b_TruthEventsAux_crossSection;   //!
   TBranch        *b_TruthEventsAux_crossSectionError;   //!
   TBranch        *b_GSFTrackParticles;   //!
   TBranch        *b_InDetTrackParticles;   //!
   TBranch        *b_PixelMSOSs;   //!
   TBranch        *b_AntiKt4EMTopoJetsAux_pt;   //!
   TBranch        *b_AntiKt4EMTopoJetsAux_eta;   //!
   TBranch        *b_AntiKt4EMTopoJetsAux_phi;   //!
   TBranch        *b_AntiKt4EMTopoJetsAux_m;   //!
   TBranch        *b_AntiKt4EMTopoJetsAux_constituentWeights;   //!
   TBranch        *b_AntiKt4LCTopoJetsAux_pt;   //!
   TBranch        *b_AntiKt4LCTopoJetsAux_eta;   //!
   TBranch        *b_AntiKt4LCTopoJetsAux_phi;   //!
   TBranch        *b_AntiKt4LCTopoJetsAux_m;   //!
   TBranch        *b_AntiKt4LCTopoJetsAux_constituentWeights;   //!
   TBranch        *b_TauTracks;   //!
   TBranch        *b_TauJetsAux_pt;   //!
   TBranch        *b_TauJetsAux_eta;   //!
   TBranch        *b_TauJetsAux_phi;   //!
   TBranch        *b_TauJetsAux_m;   //!
   TBranch        *b_TauJetsAux_ptJetSeed;   //!
   TBranch        *b_TauJetsAux_etaJetSeed;   //!
   TBranch        *b_TauJetsAux_phiJetSeed;   //!
   TBranch        *b_TauJetsAux_mJetSeed;   //!
   TBranch        *b_TauJetsAux_ptDetectorAxis;   //!
   TBranch        *b_TauJetsAux_etaDetectorAxis;   //!
   TBranch        *b_TauJetsAux_phiDetectorAxis;   //!
   TBranch        *b_TauJetsAux_mDetectorAxis;   //!
   TBranch        *b_TauJetsAux_ptIntermediateAxis;   //!
   TBranch        *b_TauJetsAux_etaIntermediateAxis;   //!
   TBranch        *b_TauJetsAux_phiIntermediateAxis;   //!
   TBranch        *b_TauJetsAux_mIntermediateAxis;   //!
   TBranch        *b_TauJetsAux_ptTauEnergyScale;   //!
   TBranch        *b_TauJetsAux_etaTauEnergyScale;   //!
   TBranch        *b_TauJetsAux_phiTauEnergyScale;   //!
   TBranch        *b_TauJetsAux_mTauEnergyScale;   //!
   TBranch        *b_TauJetsAux_ptTauEtaCalib;   //!
   TBranch        *b_TauJetsAux_etaTauEtaCalib;   //!
   TBranch        *b_TauJetsAux_phiTauEtaCalib;   //!
   TBranch        *b_TauJetsAux_mTauEtaCalib;   //!
   TBranch        *b_TauJetsAux_ptPanTauCellBasedProto;   //!
   TBranch        *b_TauJetsAux_etaPanTauCellBasedProto;   //!
   TBranch        *b_TauJetsAux_phiPanTauCellBasedProto;   //!
   TBranch        *b_TauJetsAux_mPanTauCellBasedProto;   //!
   TBranch        *b_TauJetsAux_ptPanTauCellBased;   //!
   TBranch        *b_TauJetsAux_etaPanTauCellBased;   //!
   TBranch        *b_TauJetsAux_phiPanTauCellBased;   //!
   TBranch        *b_TauJetsAux_mPanTauCellBased;   //!
   TBranch        *b_TauJetsAux_ptTrigCaloOnly;   //!
   TBranch        *b_TauJetsAux_etaTrigCaloOnly;   //!
   TBranch        *b_TauJetsAux_phiTrigCaloOnly;   //!
   TBranch        *b_TauJetsAux_mTrigCaloOnly;   //!
   TBranch        *b_TauJetsAux_ptFinalCalib;   //!
   TBranch        *b_TauJetsAux_etaFinalCalib;   //!
   TBranch        *b_TauJetsAux_phiFinalCalib;   //!
   TBranch        *b_TauJetsAux_mFinalCalib;   //!
   TBranch        *b_TauJetsAux_ROIWord;   //!
   TBranch        *b_TauJetsAux_charge;   //!
   TBranch        *b_TauJetsAux_isTauFlags;   //!
   TBranch        *b_TauJetsAux_BDTJetScore;   //!
   TBranch        *b_TauJetsAux_BDTEleScore;   //!
   TBranch        *b_TauJetsAux_EleMatchLikelihoodScore;   //!
   TBranch        *b_TauJetsAux_BDTJetScoreSigTrans;   //!
   TBranch        *b_TauJetsAux_nChargedTracks;   //!
   TBranch        *b_TauJetsAux_nIsolatedTracks;   //!
   TBranch        *b_TauJetsAux_trackFilterProngs;   //!
   TBranch        *b_TauJetsAux_trackFilterQuality;   //!
   TBranch        *b_TauJetsAux_pi0ConeDR;   //!
   TBranch        *b_TauJetsAux_ipZ0SinThetaSigLeadTrk;   //!
   TBranch        *b_TauJetsAux_etOverPtLeadTrk;   //!
   TBranch        *b_TauJetsAux_leadTrkPt;   //!
   TBranch        *b_TauJetsAux_ipSigLeadTrk;   //!
   TBranch        *b_TauJetsAux_massTrkSys;   //!
   TBranch        *b_TauJetsAux_trkWidth2;   //!
   TBranch        *b_TauJetsAux_trFlightPathSig;   //!
   TBranch        *b_TauJetsAux_numCells;   //!
   TBranch        *b_TauJetsAux_numTopoClusters;   //!
   TBranch        *b_TauJetsAux_numEffTopoClusters;   //!
   TBranch        *b_TauJetsAux_topoInvMass;   //!
   TBranch        *b_TauJetsAux_effTopoInvMass;   //!
   TBranch        *b_TauJetsAux_topoMeanDeltaR;   //!
   TBranch        *b_TauJetsAux_effTopoMeanDeltaR;   //!
   TBranch        *b_TauJetsAux_EMRadius;   //!
   TBranch        *b_TauJetsAux_hadRadius;   //!
   TBranch        *b_TauJetsAux_etEMAtEMScale;   //!
   TBranch        *b_TauJetsAux_etHadAtEMScale;   //!
   TBranch        *b_TauJetsAux_isolFrac;   //!
   TBranch        *b_TauJetsAux_centFrac;   //!
   TBranch        *b_TauJetsAux_stripWidth2;   //!
   TBranch        *b_TauJetsAux_nStrip;   //!
   TBranch        *b_TauJetsAux_trkAvgDist;   //!
   TBranch        *b_TauJetsAux_trkRmsDist;   //!
   TBranch        *b_TauJetsAux_lead2ClusterEOverAllClusterE;   //!
   TBranch        *b_TauJetsAux_lead3ClusterEOverAllClusterE;   //!
   TBranch        *b_TauJetsAux_caloIso;   //!
   TBranch        *b_TauJetsAux_caloIsoCorrected;   //!
   TBranch        *b_TauJetsAux_dRmax;   //!
   TBranch        *b_TauJetsAux_secMaxStripEt;   //!
   TBranch        *b_TauJetsAux_sumEMCellEtOverLeadTrkPt;   //!
   TBranch        *b_TauJetsAux_hadLeakEt;   //!
   TBranch        *b_TauJetsAux_TESOffset;   //!
   TBranch        *b_TauJetsAux_TESCalibConstant;   //!
   TBranch        *b_TauJetsAux_cellBasedEnergyRing1;   //!
   TBranch        *b_TauJetsAux_cellBasedEnergyRing2;   //!
   TBranch        *b_TauJetsAux_cellBasedEnergyRing3;   //!
   TBranch        *b_TauJetsAux_cellBasedEnergyRing4;   //!
   TBranch        *b_TauJetsAux_cellBasedEnergyRing5;   //!
   TBranch        *b_TauJetsAux_cellBasedEnergyRing6;   //!
   TBranch        *b_TauJetsAux_cellBasedEnergyRing7;   //!
   TBranch        *b_TauJetsAux_TRT_NHT_OVER_NLT;   //!
   TBranch        *b_TauJetsAux_TauJetVtxFraction;   //!
   TBranch        *b_TauJetsAux_nCharged;   //!
   TBranch        *b_TauJetsAux_mEflowApprox;   //!
   TBranch        *b_TauJetsAux_ptRatioEflowApprox;   //!
   TBranch        *b_TauJetsAux_innerTrkAvgDist;   //!
   TBranch        *b_TauJetsAux_SumPtTrkFrac;   //!
   TBranch        *b_TauJetsAux_etOverPtLeadTrkCorrected;   //!
   TBranch        *b_TauJetsAux_ipSigLeadTrkCorrected;   //!
   TBranch        *b_TauJetsAux_trFlightPathSigCorrected;   //!
   TBranch        *b_TauJetsAux_massTrkSysCorrected;   //!
   TBranch        *b_TauJetsAux_dRmaxCorrected;   //!
   TBranch        *b_TauJetsAux_ChPiEMEOverCaloEMECorrected;   //!
   TBranch        *b_TauJetsAux_EMPOverTrkSysPCorrected;   //!
   TBranch        *b_TauJetsAux_ptRatioEflowApproxCorrected;   //!
   TBranch        *b_TauJetsAux_mEflowApproxCorrected;   //!
   TBranch        *b_TauJetsAux_centFracCorrected;   //!
   TBranch        *b_TauJetsAux_innerTrkAvgDistCorrected;   //!
   TBranch        *b_TauJetsAux_SumPtTrkFracCorrected;   //!
   TBranch        *b_TauJetsAux_PSSFraction;   //!
   TBranch        *b_TauJetsAux_ChPiEMEOverCaloEME;   //!
   TBranch        *b_TauJetsAux_EMPOverTrkSysP;   //!
   TBranch        *b_TauJetsAux_PanTau_isPanTauCandidate;   //!
   TBranch        *b_TauJetsAux_PanTau_DecayModeProto;   //!
   TBranch        *b_TauJetsAux_PanTau_DecayMode;   //!
   TBranch        *b_TauJetsAux_PanTau_BDTValue_1p0n_vs_1p1n;   //!
   TBranch        *b_TauJetsAux_PanTau_BDTValue_1p1n_vs_1pXn;   //!
   TBranch        *b_TauJetsAux_PanTau_BDTValue_3p0n_vs_3pXn;   //!
   TBranch        *b_TauJetsAux_PanTau_BDTVar_Basic_NNeutralConsts;   //!
   TBranch        *b_TauJetsAux_PanTau_BDTVar_Charged_JetMoment_EtDRxTotalEt;   //!
   TBranch        *b_TauJetsAux_PanTau_BDTVar_Charged_StdDev_Et_WrtEtAllConsts;   //!
   TBranch        *b_TauJetsAux_PanTau_BDTVar_Neutral_HLV_SumM;   //!
   TBranch        *b_TauJetsAux_PanTau_BDTVar_Neutral_PID_BDTValues_BDTSort_1;   //!
   TBranch        *b_TauJetsAux_PanTau_BDTVar_Neutral_PID_BDTValues_BDTSort_2;   //!
   TBranch        *b_TauJetsAux_PanTau_BDTVar_Neutral_Ratio_1stBDTEtOverEtAllConsts;   //!
   TBranch        *b_TauJetsAux_PanTau_BDTVar_Neutral_Ratio_EtOverEtAllConsts;   //!
   TBranch        *b_TauJetsAux_PanTau_BDTVar_Neutral_Shots_NPhotonsInSeed;   //!
   TBranch        *b_TauJetsAux_PanTau_BDTVar_Combined_DeltaR1stNeutralTo1stCharged;   //!
   TBranch        *b_TauJetsAux_PanTau_BDTVar_Charged_HLV_SumM;   //!
   TBranch        *b_TauJetsAux_RNNJetScore;   //!
   TBranch        *b_TauJetsAux_RNNJetScoreSigTrans;   //!
   TBranch        *b_PixelClustersAux_identifier;   //!
   TBranch        *b_PixelClustersAux_localX;   //!
   TBranch        *b_PixelClustersAux_localY;   //!
   TBranch        *b_PixelClustersAux_localXError;   //!
   TBranch        *b_PixelClustersAux_localYError;   //!
   TBranch        *b_PixelClustersAux_localXYCorrelation;   //!
   TBranch        *b_PixelClustersAux_globalX;   //!
   TBranch        *b_PixelClustersAux_globalY;   //!
   TBranch        *b_PixelClustersAux_globalZ;   //!
   TBranch        *b_PrimaryVerticesAux_chiSquared;   //!
   TBranch        *b_PrimaryVerticesAux_numberDoF;   //!
   TBranch        *b_PrimaryVerticesAux_x;   //!
   TBranch        *b_PrimaryVerticesAux_y;   //!
   TBranch        *b_PrimaryVerticesAux_z;   //!
   TBranch        *b_PrimaryVerticesAux_covariance;   //!
   TBranch        *b_PrimaryVerticesAux_vertexType;   //!
   TBranch        *b_PrimaryVerticesAux_trackWeights;   //!
   TBranch        *b_PrimaryVerticesAux_neutralWeights;   //!
   TBranch        *b_AntiKt4EMTopoJetsAuxDyn_LeadingClusterCenterLambda;   //!
   TBranch        *b_AntiKt4EMTopoJetsAuxDyn_LeadingClusterSecondR;   //!
   TBranch        *b_AntiKt4EMTopoJetsAuxDyn_GhostTruth;   //!
   TBranch        *b_AntiKt4EMTopoJetsAuxDyn_NumTrkPt500;   //!
   TBranch        *b_AntiKt4EMTopoJetsAuxDyn_SumPtTrkPt500;   //!
   TBranch        *b_AntiKt4EMTopoJetsAuxDyn_EnergyPerSampling;   //!
   TBranch        *b_AntiKt4EMTopoJetsAuxDyn_EMFrac;   //!
   TBranch        *b_AntiKt4EMTopoJetsAuxDyn_Width;   //!
   TBranch        *b_AntiKt4EMTopoJetsAuxDyn_ActiveArea;   //!
   TBranch        *b_AntiKt4EMTopoJetsAuxDyn_ActiveArea4vec_pt;   //!
   TBranch        *b_AntiKt4EMTopoJetsAuxDyn_ActiveArea4vec_eta;   //!
   TBranch        *b_AntiKt4EMTopoJetsAuxDyn_ActiveArea4vec_phi;   //!
   TBranch        *b_AntiKt4EMTopoJetsAuxDyn_ActiveArea4vec_m;   //!
   TBranch        *b_AntiKt4EMTopoJetsAuxDyn_GhostTrack;   //!
   TBranch        *b_AntiKt4EMTopoJetsAuxDyn_GhostTrackCount;   //!
   TBranch        *b_AntiKt4EMTopoJetsAuxDyn_GhostTrackPt;   //!
   TBranch        *b_AntiKt4EMTopoJetsAuxDyn_GhostMuonSegment;   //!
   TBranch        *b_AntiKt4EMTopoJetsAuxDyn_GhostMuonSegmentCount;   //!
   TBranch        *b_AntiKt4EMTopoJetsAuxDyn_GhostTruthCount;   //!
   TBranch        *b_AntiKt4EMTopoJetsAuxDyn_GhostTruthPt;   //!
   TBranch        *b_AntiKt4EMTopoJetsAuxDyn_GhostBHadronsInitial;   //!
   TBranch        *b_AntiKt4EMTopoJetsAuxDyn_GhostBHadronsInitialCount;   //!
   TBranch        *b_AntiKt4EMTopoJetsAuxDyn_GhostBHadronsInitialPt;   //!
   TBranch        *b_AntiKt4EMTopoJetsAuxDyn_GhostBHadronsFinal;   //!
   TBranch        *b_AntiKt4EMTopoJetsAuxDyn_GhostBHadronsFinalCount;   //!
   TBranch        *b_AntiKt4EMTopoJetsAuxDyn_GhostBHadronsFinalPt;   //!
   TBranch        *b_AntiKt4EMTopoJetsAuxDyn_GhostBQuarksFinal;   //!
   TBranch        *b_AntiKt4EMTopoJetsAuxDyn_GhostBQuarksFinalCount;   //!
   TBranch        *b_AntiKt4EMTopoJetsAuxDyn_GhostBQuarksFinalPt;   //!
   TBranch        *b_AntiKt4EMTopoJetsAuxDyn_GhostCHadronsInitial;   //!
   TBranch        *b_AntiKt4EMTopoJetsAuxDyn_GhostCHadronsInitialCount;   //!
   TBranch        *b_AntiKt4EMTopoJetsAuxDyn_GhostCHadronsInitialPt;   //!
   TBranch        *b_AntiKt4EMTopoJetsAuxDyn_GhostCHadronsFinal;   //!
   TBranch        *b_AntiKt4EMTopoJetsAuxDyn_GhostCHadronsFinalCount;   //!
   TBranch        *b_AntiKt4EMTopoJetsAuxDyn_GhostCHadronsFinalPt;   //!
   TBranch        *b_AntiKt4EMTopoJetsAuxDyn_GhostCQuarksFinal;   //!
   TBranch        *b_AntiKt4EMTopoJetsAuxDyn_GhostCQuarksFinalCount;   //!
   TBranch        *b_AntiKt4EMTopoJetsAuxDyn_GhostCQuarksFinalPt;   //!
   TBranch        *b_AntiKt4EMTopoJetsAuxDyn_GhostTausFinal;   //!
   TBranch        *b_AntiKt4EMTopoJetsAuxDyn_GhostTausFinalCount;   //!
   TBranch        *b_AntiKt4EMTopoJetsAuxDyn_GhostTausFinalPt;   //!
   TBranch        *b_AntiKt4EMTopoJetsAuxDyn_GhostWBosons;   //!
   TBranch        *b_AntiKt4EMTopoJetsAuxDyn_GhostWBosonsCount;   //!
   TBranch        *b_AntiKt4EMTopoJetsAuxDyn_GhostWBosonsPt;   //!
   TBranch        *b_AntiKt4EMTopoJetsAuxDyn_GhostZBosons;   //!
   TBranch        *b_AntiKt4EMTopoJetsAuxDyn_GhostZBosonsCount;   //!
   TBranch        *b_AntiKt4EMTopoJetsAuxDyn_GhostZBosonsPt;   //!
   TBranch        *b_AntiKt4EMTopoJetsAuxDyn_GhostHBosons;   //!
   TBranch        *b_AntiKt4EMTopoJetsAuxDyn_GhostHBosonsCount;   //!
   TBranch        *b_AntiKt4EMTopoJetsAuxDyn_GhostHBosonsPt;   //!
   TBranch        *b_AntiKt4EMTopoJetsAuxDyn_GhostTQuarksFinal;   //!
   TBranch        *b_AntiKt4EMTopoJetsAuxDyn_GhostTQuarksFinalCount;   //!
   TBranch        *b_AntiKt4EMTopoJetsAuxDyn_GhostTQuarksFinalPt;   //!
   TBranch        *b_AntiKt4EMTopoJetsAuxDyn_GhostPartons;   //!
   TBranch        *b_AntiKt4EMTopoJetsAuxDyn_GhostPartonsCount;   //!
   TBranch        *b_AntiKt4EMTopoJetsAuxDyn_GhostPartonsPt;   //!
   TBranch        *b_AntiKt4EMTopoJetsAuxDyn_JetGhostArea;   //!
   TBranch        *b_AntiKt4EMTopoJetsAuxDyn_DetectorEta;   //!
   TBranch        *b_AntiKt4EMTopoJetsAuxDyn_DetectorPhi;   //!
   TBranch        *b_AntiKt4EMTopoJetsAuxDyn_JetLCScaleMomentum_pt;   //!
   TBranch        *b_AntiKt4EMTopoJetsAuxDyn_JetLCScaleMomentum_eta;   //!
   TBranch        *b_AntiKt4EMTopoJetsAuxDyn_JetLCScaleMomentum_phi;   //!
   TBranch        *b_AntiKt4EMTopoJetsAuxDyn_JetLCScaleMomentum_m;   //!
   TBranch        *b_AntiKt4EMTopoJetsAuxDyn_HECFrac;   //!
   TBranch        *b_AntiKt4EMTopoJetsAuxDyn_OriginCorrected;   //!
   TBranch        *b_AntiKt4EMTopoJetsAuxDyn_PileupCorrected;   //!
   TBranch        *b_AntiKt4EMTopoJetsAuxDyn_JetPileupScaleMomentum_pt;   //!
   TBranch        *b_AntiKt4EMTopoJetsAuxDyn_JetPileupScaleMomentum_eta;   //!
   TBranch        *b_AntiKt4EMTopoJetsAuxDyn_JetPileupScaleMomentum_phi;   //!
   TBranch        *b_AntiKt4EMTopoJetsAuxDyn_JetPileupScaleMomentum_m;   //!
   TBranch        *b_AntiKt4EMTopoJetsAuxDyn_JetEtaJESScaleMomentum_pt;   //!
   TBranch        *b_AntiKt4EMTopoJetsAuxDyn_JetEtaJESScaleMomentum_eta;   //!
   TBranch        *b_AntiKt4EMTopoJetsAuxDyn_JetEtaJESScaleMomentum_phi;   //!
   TBranch        *b_AntiKt4EMTopoJetsAuxDyn_JetEtaJESScaleMomentum_m;   //!
   TBranch        *b_AntiKt4EMTopoJetsAuxDyn_WidthPhi;   //!
   TBranch        *b_AntiKt4EMTopoJetsAuxDyn_PartonTruthLabelID;   //!
   TBranch        *b_AntiKt4EMTopoJetsAuxDyn_TruthLabelDeltaR_B;   //!
   TBranch        *b_AntiKt4EMTopoJetsAuxDyn_TruthLabelDeltaR_C;   //!
   TBranch        *b_AntiKt4EMTopoJetsAuxDyn_TruthLabelDeltaR_T;   //!
   TBranch        *b_AntiKt4EMTopoJetsAuxDyn_ConeTruthLabelID;   //!
   TBranch        *b_AntiKt4EMTopoJetsAuxDyn_LArBadHVEnergyFrac;   //!
   TBranch        *b_AntiKt4EMTopoJetsAuxDyn_LArBadHVNCell;   //!
   TBranch        *b_AntiKt4EMTopoJetsAuxDyn_ECPSFraction;   //!
   TBranch        *b_AntiKt4EMTopoJetsAuxDyn_FracSamplingMax;   //!
   TBranch        *b_AntiKt4EMTopoJetsAuxDyn_FracSamplingMaxIndex;   //!
   TBranch        *b_AntiKt4EMTopoJetsAuxDyn_ConstituentScale;   //!
   TBranch        *b_AntiKt4EMTopoJetsAuxDyn_LArQuality;   //!
   TBranch        *b_AntiKt4EMTopoJetsAuxDyn_JetEMScaleMomentum_pt;   //!
   TBranch        *b_AntiKt4EMTopoJetsAuxDyn_N90Constituents;   //!
   TBranch        *b_AntiKt4EMTopoJetsAuxDyn_JetEMScaleMomentum_eta;   //!
   TBranch        *b_AntiKt4EMTopoJetsAuxDyn_NegativeE;   //!
   TBranch        *b_AntiKt4EMTopoJetsAuxDyn_JetEMScaleMomentum_phi;   //!
   TBranch        *b_AntiKt4EMTopoJetsAuxDyn_Timing;   //!
   TBranch        *b_AntiKt4EMTopoJetsAuxDyn_JetEMScaleMomentum_m;   //!
   TBranch        *b_AntiKt4EMTopoJetsAuxDyn_HECQuality;   //!
   TBranch        *b_AntiKt4EMTopoJetsAuxDyn_JetConstitScaleMomentum_pt;   //!
   TBranch        *b_AntiKt4EMTopoJetsAuxDyn_JetConstitScaleMomentum_eta;   //!
   TBranch        *b_AntiKt4EMTopoJetsAuxDyn_CentroidR;   //!
   TBranch        *b_AntiKt4EMTopoJetsAuxDyn_JetConstitScaleMomentum_phi;   //!
   TBranch        *b_AntiKt4EMTopoJetsAuxDyn_AverageLArQF;   //!
   TBranch        *b_AntiKt4EMTopoJetsAuxDyn_JetConstitScaleMomentum_m;   //!
   TBranch        *b_AntiKt4EMTopoJetsAuxDyn_BchCorrCell;   //!
   TBranch        *b_AntiKt4EMTopoJetsAuxDyn_InputType;   //!
   TBranch        *b_AntiKt4EMTopoJetsAuxDyn_OotFracClusters5;   //!
   TBranch        *b_AntiKt4EMTopoJetsAuxDyn_AlgorithmType;   //!
   TBranch        *b_AntiKt4EMTopoJetsAuxDyn_OotFracClusters10;   //!
   TBranch        *b_AntiKt4EMTopoJetsAuxDyn_SizeParameter;   //!
   TBranch        *b_AntiKt4EMTopoJetsAuxDyn_TrackWidthPt500;   //!
   TBranch        *b_AntiKt4EMTopoJetsAuxDyn_NumTrkPt1000;   //!
   TBranch        *b_AntiKt4EMTopoJetsAuxDyn_SumPtTrkPt1000;   //!
   TBranch        *b_AntiKt4EMTopoJetsAuxDyn_btaggingLink_;   //!
   TBranch        *b_AntiKt4EMTopoJetsAuxDyn_btaggingLink_m_persKey;   //!
   TBranch        *b_AntiKt4EMTopoJetsAuxDyn_btaggingLink_m_persIndex;   //!
   TBranch        *b_AntiKt4EMTopoJetsAuxDyn_TrackWidthPt1000;   //!
   TBranch        *b_AntiKt4EMTopoJetsAuxDyn_TrackSumPt;   //!
   TBranch        *b_AntiKt4EMTopoJetsAuxDyn_TrackSumMass;   //!
   TBranch        *b_AntiKt4EMTopoJetsAuxDyn_JVF;   //!
   TBranch        *b_AntiKt4EMTopoJetsAuxDyn_HighestJVFVtx_;   //!
   TBranch        *b_AntiKt4EMTopoJetsAuxDyn_HighestJVFVtx_m_persKey;   //!
   TBranch        *b_AntiKt4EMTopoJetsAuxDyn_HighestJVFVtx_m_persIndex;   //!
   TBranch        *b_AntiKt4EMTopoJetsAuxDyn_JVFCorr;   //!
   TBranch        *b_AntiKt4EMTopoJetsAuxDyn_JvtRpt;   //!
   TBranch        *b_AntiKt4EMTopoJetsAuxDyn_Jvt;   //!
   TBranch        *b_AntiKt4EMTopoJetsAuxDyn_Charge;   //!
   TBranch        *b_AntiKt4EMTopoJetsAuxDyn_OriginVertex_;   //!
   TBranch        *b_AntiKt4EMTopoJetsAuxDyn_OriginVertex_m_persKey;   //!
   TBranch        *b_AntiKt4EMTopoJetsAuxDyn_OriginVertex_m_persIndex;   //!
   TBranch        *b_AntiKt4EMTopoJetsAuxDyn_ConeExclBHadronsFinal;   //!
   TBranch        *b_AntiKt4EMTopoJetsAuxDyn_ConeExclCHadronsFinal;   //!
   TBranch        *b_AntiKt4EMTopoJetsAuxDyn_ConeExclTausFinal;   //!
   TBranch        *b_AntiKt4EMTopoJetsAuxDyn_HadronConeExclTruthLabelID;   //!
   TBranch        *b_AntiKt4EMTopoJetsAuxDyn_HadronConeExclExtendedTruthLabelID;   //!
   TBranch        *b_AntiKt4EMTopoJetsAuxDyn_LeadingClusterPt;   //!
   TBranch        *b_AntiKt4EMTopoJetsAuxDyn_LeadingClusterSecondLambda;   //!
   TBranch        *b_AntiKt4LCTopoJetsAuxDyn_LeadingClusterCenterLambda;   //!
   TBranch        *b_AntiKt4LCTopoJetsAuxDyn_LeadingClusterSecondR;   //!
   TBranch        *b_AntiKt4LCTopoJetsAuxDyn_GhostTruth;   //!
   TBranch        *b_AntiKt4LCTopoJetsAuxDyn_NumTrkPt500;   //!
   TBranch        *b_AntiKt4LCTopoJetsAuxDyn_SumPtTrkPt500;   //!
   TBranch        *b_AntiKt4LCTopoJetsAuxDyn_EnergyPerSampling;   //!
   TBranch        *b_AntiKt4LCTopoJetsAuxDyn_EMFrac;   //!
   TBranch        *b_AntiKt4LCTopoJetsAuxDyn_Width;   //!
   TBranch        *b_AntiKt4LCTopoJetsAuxDyn_ActiveArea;   //!
   TBranch        *b_AntiKt4LCTopoJetsAuxDyn_ActiveArea4vec_pt;   //!
   TBranch        *b_AntiKt4LCTopoJetsAuxDyn_ActiveArea4vec_eta;   //!
   TBranch        *b_AntiKt4LCTopoJetsAuxDyn_ActiveArea4vec_phi;   //!
   TBranch        *b_AntiKt4LCTopoJetsAuxDyn_ActiveArea4vec_m;   //!
   TBranch        *b_AntiKt4LCTopoJetsAuxDyn_GhostTrack;   //!
   TBranch        *b_AntiKt4LCTopoJetsAuxDyn_GhostTrackCount;   //!
   TBranch        *b_AntiKt4LCTopoJetsAuxDyn_GhostTrackPt;   //!
   TBranch        *b_AntiKt4LCTopoJetsAuxDyn_GhostMuonSegment;   //!
   TBranch        *b_AntiKt4LCTopoJetsAuxDyn_GhostMuonSegmentCount;   //!
   TBranch        *b_AntiKt4LCTopoJetsAuxDyn_GhostTruthCount;   //!
   TBranch        *b_AntiKt4LCTopoJetsAuxDyn_GhostTruthPt;   //!
   TBranch        *b_AntiKt4LCTopoJetsAuxDyn_GhostBHadronsInitial;   //!
   TBranch        *b_AntiKt4LCTopoJetsAuxDyn_GhostBHadronsInitialCount;   //!
   TBranch        *b_AntiKt4LCTopoJetsAuxDyn_GhostBHadronsInitialPt;   //!
   TBranch        *b_AntiKt4LCTopoJetsAuxDyn_GhostBHadronsFinal;   //!
   TBranch        *b_AntiKt4LCTopoJetsAuxDyn_GhostBHadronsFinalCount;   //!
   TBranch        *b_AntiKt4LCTopoJetsAuxDyn_GhostBHadronsFinalPt;   //!
   TBranch        *b_AntiKt4LCTopoJetsAuxDyn_GhostBQuarksFinal;   //!
   TBranch        *b_AntiKt4LCTopoJetsAuxDyn_GhostBQuarksFinalCount;   //!
   TBranch        *b_AntiKt4LCTopoJetsAuxDyn_GhostBQuarksFinalPt;   //!
   TBranch        *b_AntiKt4LCTopoJetsAuxDyn_GhostCHadronsInitial;   //!
   TBranch        *b_AntiKt4LCTopoJetsAuxDyn_GhostCHadronsInitialCount;   //!
   TBranch        *b_AntiKt4LCTopoJetsAuxDyn_GhostCHadronsInitialPt;   //!
   TBranch        *b_AntiKt4LCTopoJetsAuxDyn_GhostCHadronsFinal;   //!
   TBranch        *b_AntiKt4LCTopoJetsAuxDyn_GhostCHadronsFinalCount;   //!
   TBranch        *b_AntiKt4LCTopoJetsAuxDyn_GhostCHadronsFinalPt;   //!
   TBranch        *b_AntiKt4LCTopoJetsAuxDyn_GhostCQuarksFinal;   //!
   TBranch        *b_AntiKt4LCTopoJetsAuxDyn_GhostCQuarksFinalCount;   //!
   TBranch        *b_AntiKt4LCTopoJetsAuxDyn_GhostCQuarksFinalPt;   //!
   TBranch        *b_AntiKt4LCTopoJetsAuxDyn_GhostTausFinal;   //!
   TBranch        *b_AntiKt4LCTopoJetsAuxDyn_GhostTausFinalCount;   //!
   TBranch        *b_AntiKt4LCTopoJetsAuxDyn_GhostTausFinalPt;   //!
   TBranch        *b_AntiKt4LCTopoJetsAuxDyn_GhostWBosons;   //!
   TBranch        *b_AntiKt4LCTopoJetsAuxDyn_GhostWBosonsCount;   //!
   TBranch        *b_AntiKt4LCTopoJetsAuxDyn_GhostWBosonsPt;   //!
   TBranch        *b_AntiKt4LCTopoJetsAuxDyn_GhostZBosons;   //!
   TBranch        *b_AntiKt4LCTopoJetsAuxDyn_GhostZBosonsCount;   //!
   TBranch        *b_AntiKt4LCTopoJetsAuxDyn_GhostZBosonsPt;   //!
   TBranch        *b_AntiKt4LCTopoJetsAuxDyn_GhostHBosons;   //!
   TBranch        *b_AntiKt4LCTopoJetsAuxDyn_GhostHBosonsCount;   //!
   TBranch        *b_AntiKt4LCTopoJetsAuxDyn_GhostHBosonsPt;   //!
   TBranch        *b_AntiKt4LCTopoJetsAuxDyn_GhostTQuarksFinal;   //!
   TBranch        *b_AntiKt4LCTopoJetsAuxDyn_GhostTQuarksFinalCount;   //!
   TBranch        *b_AntiKt4LCTopoJetsAuxDyn_GhostTQuarksFinalPt;   //!
   TBranch        *b_AntiKt4LCTopoJetsAuxDyn_GhostPartons;   //!
   TBranch        *b_AntiKt4LCTopoJetsAuxDyn_GhostPartonsCount;   //!
   TBranch        *b_AntiKt4LCTopoJetsAuxDyn_GhostPartonsPt;   //!
   TBranch        *b_AntiKt4LCTopoJetsAuxDyn_JetGhostArea;   //!
   TBranch        *b_AntiKt4LCTopoJetsAuxDyn_DetectorEta;   //!
   TBranch        *b_AntiKt4LCTopoJetsAuxDyn_DetectorPhi;   //!
   TBranch        *b_AntiKt4LCTopoJetsAuxDyn_HECFrac;   //!
   TBranch        *b_AntiKt4LCTopoJetsAuxDyn_OriginCorrected;   //!
   TBranch        *b_AntiKt4LCTopoJetsAuxDyn_PileupCorrected;   //!
   TBranch        *b_AntiKt4LCTopoJetsAuxDyn_JetPileupScaleMomentum_pt;   //!
   TBranch        *b_AntiKt4LCTopoJetsAuxDyn_JetPileupScaleMomentum_eta;   //!
   TBranch        *b_AntiKt4LCTopoJetsAuxDyn_JetPileupScaleMomentum_phi;   //!
   TBranch        *b_AntiKt4LCTopoJetsAuxDyn_JetPileupScaleMomentum_m;   //!
   TBranch        *b_AntiKt4LCTopoJetsAuxDyn_JetEtaJESScaleMomentum_pt;   //!
   TBranch        *b_AntiKt4LCTopoJetsAuxDyn_JetEtaJESScaleMomentum_eta;   //!
   TBranch        *b_AntiKt4LCTopoJetsAuxDyn_JetEtaJESScaleMomentum_phi;   //!
   TBranch        *b_AntiKt4LCTopoJetsAuxDyn_JetEtaJESScaleMomentum_m;   //!
   TBranch        *b_AntiKt4LCTopoJetsAuxDyn_WidthPhi;   //!
   TBranch        *b_AntiKt4LCTopoJetsAuxDyn_PartonTruthLabelID;   //!
   TBranch        *b_AntiKt4LCTopoJetsAuxDyn_TruthLabelDeltaR_B;   //!
   TBranch        *b_AntiKt4LCTopoJetsAuxDyn_TruthLabelDeltaR_C;   //!
   TBranch        *b_AntiKt4LCTopoJetsAuxDyn_TruthLabelDeltaR_T;   //!
   TBranch        *b_AntiKt4LCTopoJetsAuxDyn_ConeTruthLabelID;   //!
   TBranch        *b_AntiKt4LCTopoJetsAuxDyn_LArBadHVEnergyFrac;   //!
   TBranch        *b_AntiKt4LCTopoJetsAuxDyn_LArBadHVNCell;   //!
   TBranch        *b_AntiKt4LCTopoJetsAuxDyn_ECPSFraction;   //!
   TBranch        *b_AntiKt4LCTopoJetsAuxDyn_FracSamplingMax;   //!
   TBranch        *b_AntiKt4LCTopoJetsAuxDyn_FracSamplingMaxIndex;   //!
   TBranch        *b_AntiKt4LCTopoJetsAuxDyn_ConstituentScale;   //!
   TBranch        *b_AntiKt4LCTopoJetsAuxDyn_LArQuality;   //!
   TBranch        *b_AntiKt4LCTopoJetsAuxDyn_JetEMScaleMomentum_pt;   //!
   TBranch        *b_AntiKt4LCTopoJetsAuxDyn_N90Constituents;   //!
   TBranch        *b_AntiKt4LCTopoJetsAuxDyn_JetEMScaleMomentum_eta;   //!
   TBranch        *b_AntiKt4LCTopoJetsAuxDyn_NegativeE;   //!
   TBranch        *b_AntiKt4LCTopoJetsAuxDyn_JetEMScaleMomentum_phi;   //!
   TBranch        *b_AntiKt4LCTopoJetsAuxDyn_Timing;   //!
   TBranch        *b_AntiKt4LCTopoJetsAuxDyn_JetEMScaleMomentum_m;   //!
   TBranch        *b_AntiKt4LCTopoJetsAuxDyn_HECQuality;   //!
   TBranch        *b_AntiKt4LCTopoJetsAuxDyn_JetConstitScaleMomentum_pt;   //!
   TBranch        *b_AntiKt4LCTopoJetsAuxDyn_JetConstitScaleMomentum_eta;   //!
   TBranch        *b_AntiKt4LCTopoJetsAuxDyn_CentroidR;   //!
   TBranch        *b_AntiKt4LCTopoJetsAuxDyn_JetConstitScaleMomentum_phi;   //!
   TBranch        *b_AntiKt4LCTopoJetsAuxDyn_AverageLArQF;   //!
   TBranch        *b_AntiKt4LCTopoJetsAuxDyn_JetConstitScaleMomentum_m;   //!
   TBranch        *b_AntiKt4LCTopoJetsAuxDyn_BchCorrCell;   //!
   TBranch        *b_AntiKt4LCTopoJetsAuxDyn_InputType;   //!
   TBranch        *b_AntiKt4LCTopoJetsAuxDyn_OotFracClusters5;   //!
   TBranch        *b_AntiKt4LCTopoJetsAuxDyn_AlgorithmType;   //!
   TBranch        *b_AntiKt4LCTopoJetsAuxDyn_OotFracClusters10;   //!
   TBranch        *b_AntiKt4LCTopoJetsAuxDyn_SizeParameter;   //!
   TBranch        *b_AntiKt4LCTopoJetsAuxDyn_TrackWidthPt500;   //!
   TBranch        *b_AntiKt4LCTopoJetsAuxDyn_NumTrkPt1000;   //!
   TBranch        *b_AntiKt4LCTopoJetsAuxDyn_SumPtTrkPt1000;   //!
   TBranch        *b_AntiKt4LCTopoJetsAuxDyn_TrackWidthPt1000;   //!
   TBranch        *b_AntiKt4LCTopoJetsAuxDyn_TrackSumPt;   //!
   TBranch        *b_AntiKt4LCTopoJetsAuxDyn_TrackSumMass;   //!
   TBranch        *b_AntiKt4LCTopoJetsAuxDyn_JVF;   //!
   TBranch        *b_AntiKt4LCTopoJetsAuxDyn_HighestJVFVtx_;   //!
   TBranch        *b_AntiKt4LCTopoJetsAuxDyn_HighestJVFVtx_m_persKey;   //!
   TBranch        *b_AntiKt4LCTopoJetsAuxDyn_HighestJVFVtx_m_persIndex;   //!
   TBranch        *b_AntiKt4LCTopoJetsAuxDyn_JVFCorr;   //!
   TBranch        *b_AntiKt4LCTopoJetsAuxDyn_JvtRpt;   //!
   TBranch        *b_AntiKt4LCTopoJetsAuxDyn_Jvt;   //!
   TBranch        *b_AntiKt4LCTopoJetsAuxDyn_Charge;   //!
   TBranch        *b_AntiKt4LCTopoJetsAuxDyn_OriginVertex_;   //!
   TBranch        *b_AntiKt4LCTopoJetsAuxDyn_OriginVertex_m_persKey;   //!
   TBranch        *b_AntiKt4LCTopoJetsAuxDyn_OriginVertex_m_persIndex;   //!
   TBranch        *b_AntiKt4LCTopoJetsAuxDyn_ConeExclBHadronsFinal;   //!
   TBranch        *b_AntiKt4LCTopoJetsAuxDyn_ConeExclCHadronsFinal;   //!
   TBranch        *b_AntiKt4LCTopoJetsAuxDyn_ConeExclTausFinal;   //!
   TBranch        *b_AntiKt4LCTopoJetsAuxDyn_HadronConeExclTruthLabelID;   //!
   TBranch        *b_AntiKt4LCTopoJetsAuxDyn_HadronConeExclExtendedTruthLabelID;   //!
   TBranch        *b_AntiKt4LCTopoJetsAuxDyn_LeadingClusterPt;   //!
   TBranch        *b_AntiKt4LCTopoJetsAuxDyn_LeadingClusterSecondLambda;   //!
   TBranch        *b_BTagging_AntiKt4EMTopoAuxDyn_BTagTrackToJetAssociator;   //!
   TBranch        *b_BTagging_AntiKt4EMTopoAuxDyn_BTagTrackToJetAssociatorBB;   //!
   TBranch        *b_BTagging_AntiKt4EMTopoAuxDyn_Muons;   //!
   TBranch        *b_BTagging_AntiKt4EMTopoAuxDyn_SV1_vertices;   //!
   TBranch        *b_BTagging_AntiKt4EMTopoAuxDyn_SV1_energyTrkInJet;   //!
   TBranch        *b_BTagging_AntiKt4EMTopoAuxDyn_SV1_dstToMatLay;   //!
   TBranch        *b_BTagging_AntiKt4EMTopoAuxDyn_SV1_masssvx;   //!
   TBranch        *b_BTagging_AntiKt4EMTopoAuxDyn_SV1_efracsvx;   //!
   TBranch        *b_BTagging_AntiKt4EMTopoAuxDyn_SV1_N2Tpair;   //!
   TBranch        *b_BTagging_AntiKt4EMTopoAuxDyn_SV1_NGTinSvx;   //!
   TBranch        *b_BTagging_AntiKt4EMTopoAuxDyn_SV1_badTracksIP;   //!
   TBranch        *b_BTagging_AntiKt4EMTopoAuxDyn_SV1_TrackParticleLinks;   //!
   TBranch        *b_BTagging_AntiKt4EMTopoAuxDyn_MSV_N2Tpair;   //!
   TBranch        *b_BTagging_AntiKt4EMTopoAuxDyn_MSV_energyTrkInJet;   //!
   TBranch        *b_BTagging_AntiKt4EMTopoAuxDyn_IP2D_TrackParticleLinks;   //!
   TBranch        *b_BTagging_AntiKt4EMTopoAuxDyn_MSV_nvsec;   //!
   TBranch        *b_BTagging_AntiKt4EMTopoAuxDyn_MSV_normdist;   //!
   TBranch        *b_BTagging_AntiKt4EMTopoAuxDyn_IP3D_TrackParticleLinks;   //!
   TBranch        *b_BTagging_AntiKt4EMTopoAuxDyn_MSV_vertices;   //!
   TBranch        *b_BTagging_AntiKt4EMTopoAuxDyn_MSV_badTracksIP;   //!
   TBranch        *b_BTagging_AntiKt4EMTopoAuxDyn_JetFitter_N2Tpair;   //!
   TBranch        *b_BTagging_AntiKt4EMTopoAuxDyn_JetFitter_JFvertices;   //!
   TBranch        *b_BTagging_AntiKt4EMTopoAuxDyn_JetFitter_fittedPosition;   //!
   TBranch        *b_BTagging_AntiKt4EMTopoAuxDyn_JetFitter_fittedCov;   //!
   TBranch        *b_BTagging_AntiKt4EMTopoAuxDyn_JetFitter_tracksAtPVchi2;   //!
   TBranch        *b_BTagging_AntiKt4EMTopoAuxDyn_JetFitter_tracksAtPVndf;   //!
   TBranch        *b_BTagging_AntiKt4EMTopoAuxDyn_JetFitter_tracksAtPVlinks;   //!
   TBranch        *b_BTagging_AntiKt4EMTopoAuxDyn_JetFitter_massUncorr;   //!
   TBranch        *b_BTagging_AntiKt4EMTopoAuxDyn_JetFitter_chi2;   //!
   TBranch        *b_BTagging_AntiKt4EMTopoAuxDyn_JetFitter_ndof;   //!
   TBranch        *b_BTagging_AntiKt4EMTopoAuxDyn_JetFitter_dRFlightDir;   //!
   TBranch        *b_BTagging_AntiKt4EMTopoAuxDyn_JetFitter_nVTX;   //!
   TBranch        *b_BTagging_AntiKt4EMTopoAuxDyn_JetFitter_nSingleTracks;   //!
   TBranch        *b_BTagging_AntiKt4EMTopoAuxDyn_JetFitter_nTracksAtVtx;   //!
   TBranch        *b_BTagging_AntiKt4EMTopoAuxDyn_JetFitter_mass;   //!
   TBranch        *b_BTagging_AntiKt4EMTopoAuxDyn_JetFitter_energyFraction;   //!
   TBranch        *b_BTagging_AntiKt4EMTopoAuxDyn_JetFitter_significance3d;   //!
   TBranch        *b_BTagging_AntiKt4EMTopoAuxDyn_JetFitter_deltaeta;   //!
   TBranch        *b_BTagging_AntiKt4EMTopoAuxDyn_JetFitter_deltaphi;   //!
   TBranch        *b_BTagging_AntiKt4EMTopoAuxDyn_IP2D_sigD0wrtPVofTracks;   //!
   TBranch        *b_BTagging_AntiKt4EMTopoAuxDyn_IP2D_weightBofTracks;   //!
   TBranch        *b_BTagging_AntiKt4EMTopoAuxDyn_IP2D_weightUofTracks;   //!
   TBranch        *b_BTagging_AntiKt4EMTopoAuxDyn_IP2D_weightCofTracks;   //!
   TBranch        *b_BTagging_AntiKt4EMTopoAuxDyn_IP2D_flagFromV0ofTracks;   //!
   TBranch        *b_BTagging_AntiKt4EMTopoAuxDyn_IP2D_gradeOfTracks;   //!
   TBranch        *b_BTagging_AntiKt4EMTopoAuxDyn_trkSum_ntrk;   //!
   TBranch        *b_BTagging_AntiKt4EMTopoAuxDyn_trkSum_SPt;   //!
   TBranch        *b_BTagging_AntiKt4EMTopoAuxDyn_trkSum_VPt;   //!
   TBranch        *b_BTagging_AntiKt4EMTopoAuxDyn_trkSum_VEta;   //!
   TBranch        *b_BTagging_AntiKt4EMTopoAuxDyn_IP3D_valD0wrtPVofTracks;   //!
   TBranch        *b_BTagging_AntiKt4EMTopoAuxDyn_IP3D_valZ0wrtPVofTracks;   //!
   TBranch        *b_BTagging_AntiKt4EMTopoAuxDyn_IP3D_sigD0wrtPVofTracks;   //!
   TBranch        *b_BTagging_AntiKt4EMTopoAuxDyn_IP3D_sigZ0wrtPVofTracks;   //!
   TBranch        *b_BTagging_AntiKt4EMTopoAuxDyn_IP3D_weightBofTracks;   //!
   TBranch        *b_BTagging_AntiKt4EMTopoAuxDyn_IP3D_weightUofTracks;   //!
   TBranch        *b_BTagging_AntiKt4EMTopoAuxDyn_IP3D_weightCofTracks;   //!
   TBranch        *b_BTagging_AntiKt4EMTopoAuxDyn_IP3D_flagFromV0ofTracks;   //!
   TBranch        *b_BTagging_AntiKt4EMTopoAuxDyn_IP3D_gradeOfTracks;   //!
   TBranch        *b_BTagging_AntiKt4EMTopoAuxDyn_rnnip_pb;   //!
   TBranch        *b_BTagging_AntiKt4EMTopoAuxDyn_rnnip_pbIsValid;   //!
   TBranch        *b_BTagging_AntiKt4EMTopoAuxDyn_rnnip_pc;   //!
   TBranch        *b_BTagging_AntiKt4EMTopoAuxDyn_rnnip_pcIsValid;   //!
   TBranch        *b_BTagging_AntiKt4EMTopoAuxDyn_rnnip_pu;   //!
   TBranch        *b_BTagging_AntiKt4EMTopoAuxDyn_rnnip_puIsValid;   //!
   TBranch        *b_BTagging_AntiKt4EMTopoAuxDyn_rnnip_ptau;   //!
   TBranch        *b_BTagging_AntiKt4EMTopoAuxDyn_rnnip_ptauIsValid;   //!
   TBranch        *b_BTagging_AntiKt4EMTopoAuxDyn_SV1_normdist;   //!
   TBranch        *b_BTagging_AntiKt4EMTopoAuxDyn_SV1_significance3d;   //!
   TBranch        *b_BTagging_AntiKt4EMTopoAuxDyn_SV1_deltaR;   //!
   TBranch        *b_BTagging_AntiKt4EMTopoAuxDyn_SV1_Lxy;   //!
   TBranch        *b_BTagging_AntiKt4EMTopoAuxDyn_SV1_L3d;   //!
   TBranch        *b_BTagging_AntiKt4EMTopoAuxDyn_MultiSVbb1_discriminant;   //!
   TBranch        *b_BTagging_AntiKt4EMTopoAuxDyn_MultiSVbb2_discriminant;   //!
   TBranch        *b_BTagging_AntiKt4EMTopoAuxDyn_SMT_mu_pt;   //!
   TBranch        *b_BTagging_AntiKt4EMTopoAuxDyn_SMT_dR;   //!
   TBranch        *b_BTagging_AntiKt4EMTopoAuxDyn_SMT_qOverPratio;   //!
   TBranch        *b_BTagging_AntiKt4EMTopoAuxDyn_SMT_mombalsignif;   //!
   TBranch        *b_BTagging_AntiKt4EMTopoAuxDyn_SMT_scatneighsignif;   //!
   TBranch        *b_BTagging_AntiKt4EMTopoAuxDyn_SMT_pTrel;   //!
   TBranch        *b_BTagging_AntiKt4EMTopoAuxDyn_SMT_mu_d0;   //!
   TBranch        *b_BTagging_AntiKt4EMTopoAuxDyn_SMT_mu_z0;   //!
   TBranch        *b_BTagging_AntiKt4EMTopoAuxDyn_SMT_ID_qOverP;   //!
   TBranch        *b_BTagging_AntiKt4EMTopoAuxDyn_SMT_mu_link_;   //!
   TBranch        *b_BTagging_AntiKt4EMTopoAuxDyn_SMT_mu_link_m_persKey;   //!
   TBranch        *b_BTagging_AntiKt4EMTopoAuxDyn_SMT_mu_link_m_persIndex;   //!
   TBranch        *b_BTagging_AntiKt4EMTopoAuxDyn_SMT_discriminant;   //!
   TBranch        *b_BTagging_AntiKt4EMTopoAuxDyn_SMT_discriminantIsValid;   //!
   TBranch        *b_BTagging_AntiKt4EMTopoAuxDyn_JetVertexCharge_discriminant;   //!
   TBranch        *b_BTagging_AntiKt4EMTopoAuxDyn_MV2c10_discriminant;   //!
   TBranch        *b_BTagging_AntiKt4EMTopoAuxDyn_DL1mu_pc;   //!
   TBranch        *b_BTagging_AntiKt4EMTopoAuxDyn_DL1mu_pu;   //!
   TBranch        *b_BTagging_AntiKt4EMTopoAuxDyn_DL1mu_pb;   //!
   TBranch        *b_BTagging_AntiKt4EMTopoAuxDyn_DL1rnn_pc;   //!
   TBranch        *b_BTagging_AntiKt4EMTopoAuxDyn_DL1rnn_pu;   //!
   TBranch        *b_BTagging_AntiKt4EMTopoAuxDyn_DL1rnn_pb;   //!
   TBranch        *b_BTagging_AntiKt4EMTopoAuxDyn_MV2c10mu_discriminant;   //!
   TBranch        *b_BTagging_AntiKt4EMTopoAuxDyn_DL1_pc;   //!
   TBranch        *b_BTagging_AntiKt4EMTopoAuxDyn_DL1_pu;   //!
   TBranch        *b_BTagging_AntiKt4EMTopoAuxDyn_DL1_pb;   //!
   TBranch        *b_BTagging_AntiKt4EMTopoAuxDyn_MV2cl100_discriminant;   //!
   TBranch        *b_BTagging_AntiKt4EMTopoAuxDyn_MV2c10rnn_discriminant;   //!
   TBranch        *b_BTagging_AntiKt4EMTopoAuxDyn_MV2c100_discriminant;   //!
   TBranch        *b_ElectronsAuxDyn_neflowisol40;   //!
   TBranch        *b_ElectronsAuxDyn_topoetconeCorrBitset;   //!
   TBranch        *b_ElectronsAuxDyn_topoetconecoreConeEnergyCorrection;   //!
   TBranch        *b_ElectronsAuxDyn_topoetconecoreConeSCEnergyCorrection;   //!
   TBranch        *b_ElectronsAuxDyn_topoetcone20ptCorrection;   //!
   TBranch        *b_ElectronsAuxDyn_topoetcone30ptCorrection;   //!
   TBranch        *b_ElectronsAuxDyn_topoetcone40ptCorrection;   //!
   TBranch        *b_ElectronsAuxDyn_neflowisolCorrBitset;   //!
   TBranch        *b_ElectronsAuxDyn_neflowisolcoreConeEnergyCorrection;   //!
   TBranch        *b_ElectronsAuxDyn_neflowisol20ptCorrection;   //!
   TBranch        *b_ElectronsAuxDyn_neflowisol30ptCorrection;   //!
   TBranch        *b_ElectronsAuxDyn_neflowisol40ptCorrection;   //!
   TBranch        *b_ElectronsAuxDyn_ptconeCorrBitset;   //!
   TBranch        *b_ElectronsAuxDyn_ptconecoreTrackPtrCorrection;   //!
   TBranch        *b_ElectronsAuxDyn_e011;   //!
   TBranch        *b_ElectronsAuxDyn_e033;   //!
   TBranch        *b_ElectronsAuxDyn_e132;   //!
   TBranch        *b_ElectronsAuxDyn_e1152;   //!
   TBranch        *b_ElectronsAuxDyn_ethad1;   //!
   TBranch        *b_ElectronsAuxDyn_ethad;   //!
   TBranch        *b_ElectronsAuxDyn_e233;   //!
   TBranch        *b_ElectronsAuxDyn_e235;   //!
   TBranch        *b_ElectronsAuxDyn_e255;   //!
   TBranch        *b_ElectronsAuxDyn_e237;   //!
   TBranch        *b_ElectronsAuxDyn_e333;   //!
   TBranch        *b_ElectronsAuxDyn_e335;   //!
   TBranch        *b_ElectronsAuxDyn_e337;   //!
   TBranch        *b_ElectronsAuxDyn_e377;   //!
   TBranch        *b_ElectronsAuxDyn_e2ts1;   //!
   TBranch        *b_ElectronsAuxDyn_e2tsts1;   //!
   TBranch        *b_ElectronsAuxDyn_etcone20;   //!
   TBranch        *b_ElectronsAuxDyn_widths1;   //!
   TBranch        *b_ElectronsAuxDyn_etcone30;   //!
   TBranch        *b_ElectronsAuxDyn_widths2;   //!
   TBranch        *b_ElectronsAuxDyn_etcone40;   //!
   TBranch        *b_ElectronsAuxDyn_poscs1;   //!
   TBranch        *b_ElectronsAuxDyn_ptcone20;   //!
   TBranch        *b_ElectronsAuxDyn_poscs2;   //!
   TBranch        *b_ElectronsAuxDyn_ptcone30;   //!
   TBranch        *b_ElectronsAuxDyn_asy1;   //!
   TBranch        *b_ElectronsAuxDyn_ptcone40;   //!
   TBranch        *b_ElectronsAuxDyn_pos;   //!
   TBranch        *b_ElectronsAuxDyn_ambiguityType;   //!
   TBranch        *b_ElectronsAuxDyn_ptvarcone20;   //!
   TBranch        *b_ElectronsAuxDyn_pos7;   //!
   TBranch        *b_ElectronsAuxDyn_EgammaCovarianceMatrix;   //!
   TBranch        *b_ElectronsAuxDyn_ptvarcone30;   //!
   TBranch        *b_ElectronsAuxDyn_barys1;   //!
   TBranch        *b_ElectronsAuxDyn_ehad1;   //!
   TBranch        *b_ElectronsAuxDyn_ptvarcone40;   //!
   TBranch        *b_ElectronsAuxDyn_caloRingsLinks;   //!
   TBranch        *b_ElectronsAuxDyn_Loose;   //!
   TBranch        *b_ElectronsAuxDyn_emins1;   //!
   TBranch        *b_ElectronsAuxDyn_isEMLoose;   //!
   TBranch        *b_ElectronsAuxDyn_emaxs1;   //!
   TBranch        *b_ElectronsAuxDyn_Medium;   //!
   TBranch        *b_ElectronsAuxDyn_r33over37allcalo;   //!
   TBranch        *b_ElectronsAuxDyn_isEMMedium;   //!
   TBranch        *b_ElectronsAuxDyn_ecore;   //!
   TBranch        *b_ElectronsAuxDyn_Tight;   //!
   TBranch        *b_ElectronsAuxDyn_isEMTight;   //!
   TBranch        *b_ElectronsAuxDyn_LHLoose;   //!
   TBranch        *b_ElectronsAuxDyn_core57cellsEnergyCorrection;   //!
   TBranch        *b_ElectronsAuxDyn_isEMLHLoose;   //!
   TBranch        *b_ElectronsAuxDyn_etconeCorrBitset;   //!
   TBranch        *b_ElectronsAuxDyn_LHValue;   //!
   TBranch        *b_ElectronsAuxDyn_etcone40ptCorrection;   //!
   TBranch        *b_ElectronsAuxDyn_LHMedium;   //!
   TBranch        *b_ElectronsAuxDyn_etcone30ptCorrection;   //!
   TBranch        *b_ElectronsAuxDyn_isEMLHMedium;   //!
   TBranch        *b_ElectronsAuxDyn_truthParticleLink_;   //!
   TBranch        *b_ElectronsAuxDyn_truthParticleLink_m_persKey;   //!
   TBranch        *b_ElectronsAuxDyn_truthParticleLink_m_persIndex;   //!
   TBranch        *b_ElectronsAuxDyn_LHTight;   //!
   TBranch        *b_ElectronsAuxDyn_etcone20ptCorrection;   //!
   TBranch        *b_ElectronsAuxDyn_isEMLHTight;   //!
   TBranch        *b_ElectronsAuxDyn_topoetcone20;   //!
   TBranch        *b_ElectronsAuxDyn_truthType;   //!
   TBranch        *b_ElectronsAuxDyn_MultiLepton;   //!
   TBranch        *b_ElectronsAuxDyn_topoetcone30;   //!
   TBranch        *b_ElectronsAuxDyn_truthOrigin;   //!
   TBranch        *b_ElectronsAuxDyn_isEMMultiLepton;   //!
   TBranch        *b_ElectronsAuxDyn_topoetcone40;   //!
   TBranch        *b_ElectronsAuxDyn_ambiguityLink_;   //!
   TBranch        *b_ElectronsAuxDyn_ambiguityLink_m_persKey;   //!
   TBranch        *b_ElectronsAuxDyn_ambiguityLink_m_persIndex;   //!
   TBranch        *b_ElectronsAuxDyn_neflowisol20;   //!
   TBranch        *b_ElectronsAuxDyn_neflowisol30;   //!
   TBranch        *b_mcChannelNumber;   //!
   TBranch        *b_EventInfoAuxDyn_streamTagRobs;   //!
   TBranch        *b_EventInfoAuxDyn_streamTagDets;   //!
   TBranch        *b_EventInfoAuxDyn__SCT_BSErr_Ntot;   //!
   TBranch        *b_EventInfoAuxDyn__SCT_BSErr_bec;   //!
   TBranch        *b_EventInfoAuxDyn__SCT_BSErr_layer;   //!
   TBranch        *b_EventInfoAuxDyn__SCT_BSErr_eta;   //!
   TBranch        *b_EventInfoAuxDyn__SCT_BSErr_phi;   //!
   TBranch        *b_EventInfoAuxDyn__SCT_BSErr_side;   //!
   TBranch        *b_EventInfoAuxDyn__SCT_BSErr_rodid;   //!
   TBranch        *b_EventInfoAuxDyn__SCT_BSErr_channel;   //!
   TBranch        *b_EventInfoAuxDyn__SCT_BSErr_type;   //!
   TBranch        *b_nPixelUA;   //!
   TBranch        *b_nBlayerUA;   //!
   TBranch        *b_nPixelBarrelUA;   //!
   TBranch        *b_nPixelEndCapAUA;   //!
   TBranch        *b_nPixelEndCapCUA;   //!
   TBranch        *b_nSCTUA;   //!
   TBranch        *b_nSCTBarrelUA;   //!
   TBranch        *b_mcEventNumber;   //!
   TBranch        *b_nSCTEndCapAUA;   //!
   TBranch        *b_nSCTEndCapCUA;   //!
   TBranch        *b_EventInfoAuxDyn_mcEventWeights;   //!
   TBranch        *b_nTRTUA;   //!
   TBranch        *b_nTRTBarrelUA;   //!
   TBranch        *b_nTRTEndCapAUA;   //!
   TBranch        *b_TrtPhaseTime;   //!
   TBranch        *b_nTRTEndCapCUA;   //!
   TBranch        *b_GSFTrackParticlesAuxDyn_trackLink_;   //!
   TBranch        *b_GSFTrackParticlesAuxDyn_trackLink_m_persKey;   //!
   TBranch        *b_GSFTrackParticlesAuxDyn_trackLink_m_persIndex;   //!
   TBranch        *b_GSFTrackParticlesAuxDyn_truthParticleLink_;   //!
   TBranch        *b_GSFTrackParticlesAuxDyn_truthParticleLink_m_persKey;   //!
   TBranch        *b_GSFTrackParticlesAuxDyn_truthParticleLink_m_persIndex;   //!
   TBranch        *b_GSFTrackParticlesAuxDyn_truthMatchProbability;   //!
   TBranch        *b_GSFTrackParticlesAuxDyn_truthType;   //!
   TBranch        *b_GSFTrackParticlesAuxDyn_perigeeExtrapEta;   //!
   TBranch        *b_GSFTrackParticlesAuxDyn_truthOrigin;   //!
   TBranch        *b_GSFTrackParticlesAuxDyn_perigeeExtrapPhi;   //!
   TBranch        *b_GSFTrackParticlesAuxDyn_QoverPLM;   //!
   TBranch        *b_GSFTrackParticlesAuxDyn_TRTdEdxUsedHits;   //!
   TBranch        *b_GSFTrackParticlesAuxDyn_originalTrackParticle_;   //!
   TBranch        *b_GSFTrackParticlesAuxDyn_originalTrackParticle_m_persKey;   //!
   TBranch        *b_GSFTrackParticlesAuxDyn_originalTrackParticle_m_persIndex;   //!
   TBranch        *b_GSFTrackParticlesAuxDyn_TRTdEdx;   //!
   TBranch        *b_InDetTrackParticlesAuxDyn_TrkIBLX;   //!
   TBranch        *b_InDetTrackParticlesAuxDyn_TRTdEdxUsedHits;   //!
   TBranch        *b_InDetTrackParticlesAuxDyn_TrkIBLY;   //!
   TBranch        *b_InDetTrackParticlesAuxDyn_TrkIBLZ;   //!
   TBranch        *b_InDetTrackParticlesAuxDyn_TRTdEdx;   //!
   TBranch        *b_InDetTrackParticlesAuxDyn_TrkBLX;   //!
   TBranch        *b_InDetTrackParticlesAuxDyn_TrkBLY;   //!
   TBranch        *b_InDetTrackParticlesAuxDyn_TrkBLZ;   //!
   TBranch        *b_InDetTrackParticlesAuxDyn_nBC_meas;   //!
   TBranch        *b_InDetTrackParticlesAuxDyn_TrkL1X;   //!
   TBranch        *b_InDetTrackParticlesAuxDyn_TrkL1Y;   //!
   TBranch        *b_InDetTrackParticlesAuxDyn_trackLink_;   //!
   TBranch        *b_InDetTrackParticlesAuxDyn_trackLink_m_persKey;   //!
   TBranch        *b_InDetTrackParticlesAuxDyn_trackLink_m_persIndex;   //!
   TBranch        *b_InDetTrackParticlesAuxDyn_TrkL1Z;   //!
   TBranch        *b_InDetTrackParticlesAuxDyn_truthParticleLink_;   //!
   TBranch        *b_InDetTrackParticlesAuxDyn_truthParticleLink_m_persKey;   //!
   TBranch        *b_InDetTrackParticlesAuxDyn_truthParticleLink_m_persIndex;   //!
   TBranch        *b_InDetTrackParticlesAuxDyn_TrkL2X;   //!
   TBranch        *b_InDetTrackParticlesAuxDyn_truthMatchProbability;   //!
   TBranch        *b_InDetTrackParticlesAuxDyn_TrkL2Y;   //!
   TBranch        *b_InDetTrackParticlesAuxDyn_truthType;   //!
   TBranch        *b_InDetTrackParticlesAuxDyn_truthOrigin;   //!
   TBranch        *b_InDetTrackParticlesAuxDyn_TrkL2Z;   //!
   TBranch        *b_InDetTrackParticlesAuxDyn_measurement_region;   //!
   TBranch        *b_InDetTrackParticlesAuxDyn_measurement_det;   //!
   TBranch        *b_InDetTrackParticlesAuxDyn_measurement_iLayer;   //!
   TBranch        *b_InDetTrackParticlesAuxDyn_hitResiduals_residualLocX;   //!
   TBranch        *b_InDetTrackParticlesAuxDyn_hitResiduals_pullLocX;   //!
   TBranch        *b_InDetTrackParticlesAuxDyn_hitResiduals_residualLocY;   //!
   TBranch        *b_InDetTrackParticlesAuxDyn_hitResiduals_pullLocY;   //!
   TBranch        *b_InDetTrackParticlesAuxDyn_hitResiduals_phiWidth;   //!
   TBranch        *b_InDetTrackParticlesAuxDyn_hitResiduals_etaWidth;   //!
   TBranch        *b_InDetTrackParticlesAuxDyn_measurement_type;   //!
   TBranch        *b_InDetTrackParticlesAuxDyn_d0err;   //!
   TBranch        *b_InDetTrackParticlesAuxDyn_z0err;   //!
   TBranch        *b_InDetTrackParticlesAuxDyn_phierr;   //!
   TBranch        *b_InDetTrackParticlesAuxDyn_thetaerr;   //!
   TBranch        *b_InDetTrackParticlesAuxDyn_qopterr;   //!
   TBranch        *b_InDetTrackParticlesAuxDyn_msosLink;   //!
   TBranch        *b_PixelClustersAuxDyn_eta_pixel_index;   //!
   TBranch        *b_PixelClustersAuxDyn_charge;   //!
   TBranch        *b_PixelClustersAuxDyn_phi_pixel_index;   //!
   TBranch        *b_PixelClustersAuxDyn_sizePhi;   //!
   TBranch        *b_PixelClustersAuxDyn_sizeZ;   //!
   TBranch        *b_PixelClustersAuxDyn_nRDO;   //!
   TBranch        *b_PixelClustersAuxDyn_ToT;   //!
   TBranch        *b_PixelClustersAuxDyn_LVL1A;   //!
   TBranch        *b_PixelClustersAuxDyn_isFake;   //!
   TBranch        *b_PixelClustersAuxDyn_gangedPixel;   //!
   TBranch        *b_PixelClustersAuxDyn_isSplit;   //!
   TBranch        *b_PixelClustersAuxDyn_splitProbability1;   //!
   TBranch        *b_PixelClustersAuxDyn_splitProbability2;   //!
   TBranch        *b_PixelClustersAuxDyn_NN_sizeX;   //!
   TBranch        *b_PixelClustersAuxDyn_NN_sizeY;   //!
   TBranch        *b_PixelClustersAuxDyn_NN_phiBS;   //!
   TBranch        *b_PixelClustersAuxDyn_NN_thetaBS;   //!
   TBranch        *b_PixelClustersAuxDyn_NN_matrixOfToT;   //!
   TBranch        *b_PixelClustersAuxDyn_NN_matrixOfCharge;   //!
   TBranch        *b_PixelClustersAuxDyn_NN_vectorOfPitchesY;   //!
   TBranch        *b_PixelClustersAuxDyn_NN_etaPixelIndexWeightedPosition;   //!
   TBranch        *b_PixelClustersAuxDyn_NN_phiPixelIndexWeightedPosition;   //!
   TBranch        *b_PixelClustersAuxDyn_NN_localEtaPixelIndexWeightedPosition;   //!
   TBranch        *b_PixelClustersAuxDyn_NN_localPhiPixelIndexWeightedPosition;   //!
   TBranch        *b_PixelClustersAuxDyn_isBSError;   //!
   TBranch        *b_PixelClustersAuxDyn_DCSState;   //!
   TBranch        *b_PixelClustersAuxDyn_BiasVoltage;   //!
   TBranch        *b_PixelClustersAuxDyn_Temperature;   //!
   TBranch        *b_PixelClustersAuxDyn_DepletionVoltage;   //!
   TBranch        *b_PixelClustersAuxDyn_LorentzShift;   //!
   TBranch        *b_PixelClustersAuxDyn_rdo_phi_pixel_index;   //!
   TBranch        *b_PixelClustersAuxDyn_rdo_eta_pixel_index;   //!
   TBranch        *b_PixelClustersAuxDyn_rdo_charge;   //!
   TBranch        *b_PixelClustersAuxDyn_rdo_tot;   //!
   TBranch        *b_PixelClustersAuxDyn_rdo_Cterm;   //!
   TBranch        *b_PixelClustersAuxDyn_rdo_Aterm;   //!
   TBranch        *b_PixelClustersAuxDyn_rdo_Eterm;   //!
   TBranch        *b_PixelClustersAuxDyn_detectorElementID;   //!
   TBranch        *b_PixelClustersAuxDyn_truth_barcode;   //!
   TBranch        *b_PixelClustersAuxDyn_sdo_words;   //!
   TBranch        *b_PixelClustersAuxDyn_sdo_depositsBarcode;   //!
   TBranch        *b_PixelClustersAuxDyn_sdo_depositsEnergy;   //!
   TBranch        *b_PixelClustersAuxDyn_sihit_energyDeposit;   //!
   TBranch        *b_PixelClustersAuxDyn_sihit_meanTime;   //!
   TBranch        *b_PixelClustersAuxDyn_sihit_barcode;   //!
   TBranch        *b_PixelClustersAuxDyn_sihit_pdgid;   //!
   TBranch        *b_PixelClustersAuxDyn_sihit_startPosX;   //!
   TBranch        *b_PixelClustersAuxDyn_sihit_startPosY;   //!
   TBranch        *b_PixelClustersAuxDyn_bec;   //!
   TBranch        *b_PixelClustersAuxDyn_sihit_startPosZ;   //!
   TBranch        *b_PixelClustersAuxDyn_layer;   //!
   TBranch        *b_PixelClustersAuxDyn_sihit_endPosX;   //!
   TBranch        *b_PixelClustersAuxDyn_phi_module;   //!
   TBranch        *b_PixelClustersAuxDyn_sihit_endPosY;   //!
   TBranch        *b_PixelClustersAuxDyn_eta_module;   //!
   TBranch        *b_PixelClustersAuxDyn_sihit_endPosZ;   //!
   TBranch        *b_PixelClustersAuxDyn_NN_positionsX;   //!
   TBranch        *b_PixelClustersAuxDyn_NN_positionsY;   //!
   TBranch        *b_PixelClustersAuxDyn_NN_positions_indexX;   //!
   TBranch        *b_PixelClustersAuxDyn_NN_positions_indexY;   //!
   TBranch        *b_PixelClustersAuxDyn_NN_theta;   //!
   TBranch        *b_PixelClustersAuxDyn_NN_phi;   //!
   TBranch        *b_PixelClustersAuxDyn_NN_barcode;   //!
   TBranch        *b_PixelClustersAuxDyn_NN_pdgid;   //!
   TBranch        *b_PixelClustersAuxDyn_NN_energyDep;   //!
   TBranch        *b_PixelClustersAuxDyn_NN_trueP;   //!
   TBranch        *b_PixelClustersAuxDyn_NN_motherBarcode;   //!
   TBranch        *b_PixelClustersAuxDyn_NN_motherPdgid;   //!
   TBranch        *b_PixelClustersAuxDyn_NN_pathlengthX;   //!
   TBranch        *b_PixelClustersAuxDyn_NN_pathlengthY;   //!
   TBranch        *b_PixelClustersAuxDyn_NN_pathlengthZ;   //!
   TBranch        *b_PixelClustersAuxDyn_broken;   //!
   TBranch        *b_TauJetsAuxDyn_TAU_ABSDELTAETA;   //!
   TBranch        *b_TauJetsAuxDyn_GhostMuonSegmentCount;   //!
   TBranch        *b_TauJetsAuxDyn_PanTau_DecayModeExtended;   //!
   TBranch        *b_TauJetsAuxDyn_TAU_ABSDELTAPHI;   //!
   TBranch        *b_TauJetsAuxDyn_pt_combined;   //!
   TBranch        *b_TauJetsAuxDyn_EMFracFixed;   //!
   TBranch        *b_TauJetsAuxDyn_eta_combined;   //!
   TBranch        *b_TauJetsAuxDyn_etHotShotWinOverPtLeadTrk;   //!
   TBranch        *b_TauJetsAuxDyn_phi_combined;   //!
   TBranch        *b_TauJetsAuxDyn_hadLeakFracFixed;   //!
   TBranch        *b_TauJetsAuxDyn_m_combined;   //!
   TBranch        *b_TauJetsAuxDyn_mu;   //!
   TBranch        *b_TauJetsAuxDyn_leadTrackProbHT;   //!
   TBranch        *b_TauJetsAuxDyn_nVtxPU;   //!
   TBranch        *b_TauJetsAuxDyn_BDTEleScoreSigTrans;   //!
   TBranch        *b_TauJetsAuxDyn_ClustersMeanCenterLambda;   //!
   TBranch        *b_TauJetsAuxDyn_ClustersMeanFirstEngDens;   //!
   TBranch        *b_TauJetsAuxDyn_ClustersMeanEMProbability;   //!
   TBranch        *b_TauJetsAuxDyn_ClustersMeanSecondLambda;   //!
   TBranch        *b_TauJetsAuxDyn_ClustersMeanPresamplerFrac;   //!
   TBranch        *b_TauJetsAuxDyn_LeadClusterFrac;   //!
   TBranch        *b_TauJetsAuxDyn_UpsilonCluster;   //!
   TBranch        *b_TauJetsAuxDyn_PFOEngRelDiff;   //!
   TBranch        *b_TauJetsAuxDyn_LC_pantau_interpolPt;   //!
   TBranch        *b_TauJetsAuxDyn_nModifiedIsolationTracks;   //!
   TBranch        *b_TauJetsAuxDyn_NUMTRACK;   //!
   TBranch        *b_TauJetsAuxDyn_MU;   //!
   TBranch        *b_TauJetsAuxDyn_NUMVERTICES;   //!
   TBranch        *b_TauJetsAuxDyn_leadTrackEta;   //!
   TBranch        *b_TauJetsAuxDyn_EMFRACTIONATEMSCALE_MOVEE3;   //!
   TBranch        *b_TauJetsAuxDyn_TAU_SEEDTRK_SECMAXSTRIPETOVERPT;   //!
   TBranch        *b_TauJetsAuxDyn_ABS_ETA_LEAD_TRACK;   //!
   TBranch        *b_TauJetsAuxDyn_CORRFTRK;   //!
   TBranch        *b_TauJetsAuxDyn_HADLEAKET;   //!
   TBranch        *b_TauJetsAuxDyn_TAU_TRT_NHT_OVER_NLT;   //!
   TBranch        *b_TauJetsAuxDyn_CORRCENTFRAC;   //!
   TBranch        *b_TauJetsAuxDyn_etHotShotDR1;   //!
   TBranch        *b_TauJetsAuxDyn_etHotShotWin;   //!
   TBranch        *b_TauJetsAuxDyn_etHotShotDR1OverPtLeadTrk;   //!
   TBranch        *b_TauJetsAuxDyn_electronLink_;   //!
   TBranch        *b_TauJetsAuxDyn_electronLink_m_persKey;   //!
   TBranch        *b_TauJetsAuxDyn_electronLink_m_persIndex;   //!
   TBranch        *b_TauJetsAuxDyn_absipSigLeadTrk;   //!
   TBranch        *b_TauTracksAuxDyn_CaloSamplingEtaEM;   //!
   TBranch        *b_TauTracksAuxDyn_CaloSamplingPhiEM;   //!
   TBranch        *b_TauTracksAuxDyn_CaloSamplingEtaHad;   //!
   TBranch        *b_TauTracksAuxDyn_CaloSamplingPhiHad;   //!
   TBranch        *b_TruthEventsAuxDyn_PDGID1;   //!
   TBranch        *b_TruthEventsAuxDyn_PDGID2;   //!
   TBranch        *b_TruthEventsAuxDyn_PDFID1;   //!
   TBranch        *b_TruthEventsAuxDyn_PDFID2;   //!
   TBranch        *b_TruthEventsAuxDyn_X1;   //!
   TBranch        *b_TruthEventsAuxDyn_X2;   //!
   TBranch        *b_TruthEventsAuxDyn_Q;   //!
   TBranch        *b_TruthEventsAuxDyn_XF1;   //!
   TBranch        *b_TruthEventsAuxDyn_XF2;   //!
   TBranch        *b_TruthParticlesAuxDyn_polarizationTheta;   //!
   TBranch        *b_TruthParticlesAuxDyn_polarizationPhi;   //!
   TBranch        *b_TruthParticlesAuxDyn_d0;   //!
   TBranch        *b_TruthParticlesAuxDyn_z0;   //!
   TBranch        *b_TruthParticlesAuxDyn_z0st;   //!
   TBranch        *b_TruthParticlesAuxDyn_theta;   //!
   TBranch        *b_TruthParticlesAuxDyn_qOverP;   //!
   TBranch        *b_TruthParticlesAuxDyn_prodR;   //!
   TBranch        *b_TruthParticlesAuxDyn_prodZ;   //!
   TBranch        *b_TruthParticlesAuxDyn_phi;   //!
   TBranch        *b_InDetTrackParticlesAuxDyn_caloExt_Decorated;   //!
   TBranch        *b_InDetTrackParticlesAuxDyn_caloExt_eta;   //!
   TBranch        *b_InDetTrackParticlesAuxDyn_caloExt_phi;   //!
   TBranch        *b_MuonsAuxDyn_neflowisol40;   //!
   TBranch        *b_MuonsAuxDyn_CT_EL_Type;   //!
   TBranch        *b_MuonsAuxDyn_topoetconeCorrBitset;   //!
   TBranch        *b_MuonsAuxDyn_CT_ET_LRLikelihood;   //!
   TBranch        *b_MuonsAuxDyn_topoetconecoreConeEnergyCorrection;   //!
   TBranch        *b_MuonsAuxDyn_CT_ET_FSRCandidateEnergy;   //!
   TBranch        *b_MuonsAuxDyn_d0_staco;   //!
   TBranch        *b_MuonsAuxDyn_z0_staco;   //!
   TBranch        *b_MuonsAuxDyn_phi0_staco;   //!
   TBranch        *b_MuonsAuxDyn_theta_staco;   //!
   TBranch        *b_MuonsAuxDyn_neflowisolCorrBitset;   //!
   TBranch        *b_MuonsAuxDyn_qOverP_staco;   //!
   TBranch        *b_MuonsAuxDyn_neflowisolcoreConeEnergyCorrection;   //!
   TBranch        *b_MuonsAuxDyn_qOverPErr_staco;   //!
   TBranch        *b_MuonsAuxDyn_numberOfGoodPrecisionLayers;   //!
   TBranch        *b_MuonsAuxDyn_innerClosePrecisionHits;   //!
   TBranch        *b_MuonsAuxDyn_middleClosePrecisionHits;   //!
   TBranch        *b_MuonsAuxDyn_ptconeCorrBitset;   //!
   TBranch        *b_MuonsAuxDyn_outerClosePrecisionHits;   //!
   TBranch        *b_MuonsAuxDyn_ptconecoreTrackPtrCorrection;   //!
   TBranch        *b_MuonsAuxDyn_extendedClosePrecisionHits;   //!
   TBranch        *b_MuonsAuxDyn_innerOutBoundsPrecisionHits;   //!
   TBranch        *b_MuonsAuxDyn_middleOutBoundsPrecisionHits;   //!
   TBranch        *b_MuonsAuxDyn_outerOutBoundsPrecisionHits;   //!
   TBranch        *b_MuonsAuxDyn_extendedOutBoundsPrecisionHits;   //!
   TBranch        *b_MuonsAuxDyn_combinedTrackOutBoundsPrecisionHits;   //!
   TBranch        *b_MuonsAuxDyn_isEndcapGoodLayers;   //!
   TBranch        *b_MuonsAuxDyn_isSmallGoodSectors;   //!
   TBranch        *b_MuonsAuxDyn_MuonSpectrometerPt;   //!
   TBranch        *b_MuonsAuxDyn_InnerDetectorPt;   //!
   TBranch        *b_MuonsAuxDyn_FSR_CandidateEnergy;   //!
   TBranch        *b_MuonsAuxDyn_numEnergyLossPerTrack;   //!
   TBranch        *b_MuonsAuxDyn_segmentsOnTrack;   //!
   TBranch        *b_MuonsAuxDyn_deltaphi_0;   //!
   TBranch        *b_MuonsAuxDyn_deltatheta_0;   //!
   TBranch        *b_MuonsAuxDyn_sigmadeltaphi_0;   //!
   TBranch        *b_MuonsAuxDyn_sigmadeltatheta_0;   //!
   TBranch        *b_MuonsAuxDyn_deltaphi_1;   //!
   TBranch        *b_MuonsAuxDyn_deltatheta_1;   //!
   TBranch        *b_MuonsAuxDyn_sigmadeltaphi_1;   //!
   TBranch        *b_MuonsAuxDyn_sigmadeltatheta_1;   //!
   TBranch        *b_MuonsAuxDyn_ET_Core;   //!
   TBranch        *b_MuonsAuxDyn_ET_EMCore;   //!
   TBranch        *b_MuonsAuxDyn_ET_TileCore;   //!
   TBranch        *b_MuonsAuxDyn_ET_HECCore;   //!
   TBranch        *b_MuonsAuxDyn_nprecMatchedHitsPerChamberLayer;   //!
   TBranch        *b_MuonsAuxDyn_nphiMatchedHitsPerChamberLayer;   //!
   TBranch        *b_MuonsAuxDyn_ntrigEtaMatchedHitsPerChamberLayer;   //!
   TBranch        *b_MuonsAuxDyn_coreMuonEnergyCorrection;   //!
   TBranch        *b_MuonsAuxDyn_etconecoreConeEnergyCorrection;   //!
   TBranch        *b_MuonsAuxDyn_etconeCorrBitset;   //!
   TBranch        *b_MuonsAuxDyn_truthParticleLink_;   //!
   TBranch        *b_MuonsAuxDyn_truthParticleLink_m_persKey;   //!
   TBranch        *b_MuonsAuxDyn_truthParticleLink_m_persIndex;   //!
   TBranch        *b_MuonsAuxDyn_topoetcone20;   //!
   TBranch        *b_MuonsAuxDyn_topoetcone30;   //!
   TBranch        *b_MuonsAuxDyn_truthType;   //!
   TBranch        *b_MuonsAuxDyn_topoetcone40;   //!
   TBranch        *b_MuonsAuxDyn_truthOrigin;   //!
   TBranch        *b_MuonsAuxDyn_neflowisol20;   //!
   TBranch        *b_MuonsAuxDyn_neflowisol30;   //!
   TBranch        *b_MuonsAuxDyn_CT_ET_Core;   //!
   TBranch        *b_PrimaryVerticesAuxDyn_sumPt2;   //!

   xAODScan(TTree *tree=0);
   virtual ~xAODScan();
   virtual Int_t    Cut(Long64_t entry);
   virtual Int_t    GetEntry(Long64_t entry);
   virtual Long64_t LoadTree(Long64_t entry);
   virtual void     Init(TTree *tree);
   virtual void     Loop();
   virtual Bool_t   Notify();
   virtual void     Show(Long64_t entry = -1);
};

#endif

#ifdef xAODScan_cxx
xAODScan::xAODScan(TTree *tree) : fChain(0) 
{
// if parameter tree is not specified (or zero), connect the file
// used to generate this class and read the Tree.
   if (tree == 0) {
      TFile *f = (TFile*)gROOT->GetListOfFiles()->FindObject("../user.ktakeda.15239423.EXT1._000001.xAOD.pool.root");
      if (!f || !f->IsOpen()) {
         f = new TFile("../user.ktakeda.15239423.EXT1._000001.xAOD.pool.root");
      }
      f->GetObject("CollectionTree",tree);

   }
   Init(tree);
}

xAODScan::~xAODScan()
{
   if (!fChain) return;
   delete fChain->GetCurrentFile();
}

Int_t xAODScan::GetEntry(Long64_t entry)
{
// Read contents of entry.
   if (!fChain) return 0;
   return fChain->GetEntry(entry);
}
Long64_t xAODScan::LoadTree(Long64_t entry)
{
// Set the environment to read one entry
   if (!fChain) return -5;
   Long64_t centry = fChain->LoadTree(entry);
   if (centry < 0) return centry;
   if (fChain->GetTreeNumber() != fCurrent) {
      fCurrent = fChain->GetTreeNumber();
      Notify();
   }
   return centry;
}

void xAODScan::Init(TTree *tree)
{
   // The Init() function is called when the selector needs to initialize
   // a new tree or chain. Typically here the branch addresses and branch
   // pointers of the tree will be set.
   // It is normally not necessary to make changes to the generated
   // code, but the routine can be extended by the user if needed.
   // Init() will be called many times when running on PROOF
   // (once per file to be processed).

   // Set object pointer
   InDetTrackParticlesAuxDyn_TrkIBLX = 0;
   InDetTrackParticlesAuxDyn_TRTdEdxUsedHits = 0;
   InDetTrackParticlesAuxDyn_TrkIBLY = 0;
   InDetTrackParticlesAuxDyn_TrkIBLZ = 0;
   InDetTrackParticlesAuxDyn_TRTdEdx = 0;
   InDetTrackParticlesAuxDyn_TrkBLX = 0;
   InDetTrackParticlesAuxDyn_TrkBLY = 0;
   InDetTrackParticlesAuxDyn_TrkBLZ = 0;
   InDetTrackParticlesAuxDyn_nBC_meas = 0;
   InDetTrackParticlesAuxDyn_TrkL1X = 0;
   InDetTrackParticlesAuxDyn_TrkL1Y = 0;
   InDetTrackParticlesAuxDyn_TrkL1Z = 0;
   InDetTrackParticlesAuxDyn_TrkL2X = 0;
   InDetTrackParticlesAuxDyn_truthMatchProbability = 0;
   InDetTrackParticlesAuxDyn_TrkL2Y = 0;
   InDetTrackParticlesAuxDyn_truthType = 0;
   InDetTrackParticlesAuxDyn_truthOrigin = 0;
   InDetTrackParticlesAuxDyn_TrkL2Z = 0;
   InDetTrackParticlesAuxDyn_measurement_region = 0;
   InDetTrackParticlesAuxDyn_measurement_det = 0;
   InDetTrackParticlesAuxDyn_measurement_iLayer = 0;
   InDetTrackParticlesAuxDyn_hitResiduals_residualLocX = 0;
   InDetTrackParticlesAuxDyn_hitResiduals_pullLocX = 0;
   InDetTrackParticlesAuxDyn_hitResiduals_residualLocY = 0;
   InDetTrackParticlesAuxDyn_hitResiduals_pullLocY = 0;
   InDetTrackParticlesAuxDyn_hitResiduals_phiWidth = 0;
   InDetTrackParticlesAuxDyn_hitResiduals_etaWidth = 0;
   InDetTrackParticlesAuxDyn_measurement_type = 0;
   InDetTrackParticlesAuxDyn_d0err = 0;
   InDetTrackParticlesAuxDyn_z0err = 0;
   InDetTrackParticlesAuxDyn_phierr = 0;
   InDetTrackParticlesAuxDyn_thetaerr = 0;
   InDetTrackParticlesAuxDyn_qopterr = 0;
   InDetTrackParticlesAuxDyn_msosLink = 0;
   PixelClustersAuxDyn_eta_pixel_index = 0;
   PixelClustersAuxDyn_charge = 0;
   PixelClustersAuxDyn_phi_pixel_index = 0;
   PixelClustersAuxDyn_sizePhi = 0;
   PixelClustersAuxDyn_sizeZ = 0;
   PixelClustersAuxDyn_nRDO = 0;
   PixelClustersAuxDyn_ToT = 0;
   PixelClustersAuxDyn_LVL1A = 0;
   PixelClustersAuxDyn_isFake = 0;
   PixelClustersAuxDyn_gangedPixel = 0;
   PixelClustersAuxDyn_isSplit = 0;
   PixelClustersAuxDyn_splitProbability1 = 0;
   PixelClustersAuxDyn_splitProbability2 = 0;
   PixelClustersAuxDyn_NN_sizeX = 0;
   PixelClustersAuxDyn_NN_sizeY = 0;
   PixelClustersAuxDyn_NN_phiBS = 0;
   PixelClustersAuxDyn_NN_thetaBS = 0;
   PixelClustersAuxDyn_NN_matrixOfToT = 0;
   PixelClustersAuxDyn_NN_matrixOfCharge = 0;
   PixelClustersAuxDyn_NN_vectorOfPitchesY = 0;
   PixelClustersAuxDyn_NN_etaPixelIndexWeightedPosition = 0;
   PixelClustersAuxDyn_NN_phiPixelIndexWeightedPosition = 0;
   PixelClustersAuxDyn_NN_localEtaPixelIndexWeightedPosition = 0;
   PixelClustersAuxDyn_NN_localPhiPixelIndexWeightedPosition = 0;
   PixelClustersAuxDyn_isBSError = 0;
   PixelClustersAuxDyn_DCSState = 0;
   PixelClustersAuxDyn_BiasVoltage = 0;
   PixelClustersAuxDyn_Temperature = 0;
   PixelClustersAuxDyn_DepletionVoltage = 0;
   PixelClustersAuxDyn_LorentzShift = 0;
   PixelClustersAuxDyn_rdo_phi_pixel_index = 0;
   PixelClustersAuxDyn_rdo_eta_pixel_index = 0;
   PixelClustersAuxDyn_rdo_charge = 0;
   PixelClustersAuxDyn_rdo_tot = 0;
   PixelClustersAuxDyn_rdo_Cterm = 0;
   PixelClustersAuxDyn_rdo_Aterm = 0;
   PixelClustersAuxDyn_rdo_Eterm = 0;
   PixelClustersAuxDyn_detectorElementID = 0;
   PixelClustersAuxDyn_truth_barcode = 0;
   PixelClustersAuxDyn_sdo_words = 0;
   PixelClustersAuxDyn_sdo_depositsBarcode = 0;
   PixelClustersAuxDyn_sdo_depositsEnergy = 0;
   PixelClustersAuxDyn_sihit_energyDeposit = 0;
   PixelClustersAuxDyn_sihit_meanTime = 0;
   PixelClustersAuxDyn_sihit_barcode = 0;
   PixelClustersAuxDyn_sihit_pdgid = 0;
   PixelClustersAuxDyn_sihit_startPosX = 0;
   PixelClustersAuxDyn_sihit_startPosY = 0;
   PixelClustersAuxDyn_bec = 0;
   PixelClustersAuxDyn_sihit_startPosZ = 0;
   PixelClustersAuxDyn_layer = 0;
   PixelClustersAuxDyn_sihit_endPosX = 0;
   PixelClustersAuxDyn_phi_module = 0;
   PixelClustersAuxDyn_sihit_endPosY = 0;
   PixelClustersAuxDyn_eta_module = 0;
   PixelClustersAuxDyn_sihit_endPosZ = 0;
   PixelClustersAuxDyn_NN_positionsX = 0;
   PixelClustersAuxDyn_NN_positionsY = 0;
   PixelClustersAuxDyn_NN_positions_indexX = 0;
   PixelClustersAuxDyn_NN_positions_indexY = 0;
   PixelClustersAuxDyn_NN_theta = 0;
   PixelClustersAuxDyn_NN_phi = 0;
   PixelClustersAuxDyn_NN_barcode = 0;
   PixelClustersAuxDyn_NN_pdgid = 0;
   PixelClustersAuxDyn_NN_energyDep = 0;
   PixelClustersAuxDyn_NN_trueP = 0;
   PixelClustersAuxDyn_NN_motherBarcode = 0;
   PixelClustersAuxDyn_NN_motherPdgid = 0;
   PixelClustersAuxDyn_NN_pathlengthX = 0;
   PixelClustersAuxDyn_NN_pathlengthY = 0;
   PixelClustersAuxDyn_NN_pathlengthZ = 0;
   PixelClustersAuxDyn_broken = 0;
   TauJetsAuxDyn_TAU_ABSDELTAETA = 0;
   TauJetsAuxDyn_GhostMuonSegmentCount = 0;
   TauJetsAuxDyn_PanTau_DecayModeExtended = 0;
   TauJetsAuxDyn_TAU_ABSDELTAPHI = 0;
   TauJetsAuxDyn_pt_combined = 0;
   TauJetsAuxDyn_EMFracFixed = 0;
   TauJetsAuxDyn_eta_combined = 0;
   TauJetsAuxDyn_etHotShotWinOverPtLeadTrk = 0;
   TauJetsAuxDyn_phi_combined = 0;
   TauJetsAuxDyn_hadLeakFracFixed = 0;
   TauJetsAuxDyn_m_combined = 0;
   TauJetsAuxDyn_mu = 0;
   TauJetsAuxDyn_leadTrackProbHT = 0;
   TauJetsAuxDyn_nVtxPU = 0;
   TauJetsAuxDyn_BDTEleScoreSigTrans = 0;
   TauJetsAuxDyn_ClustersMeanCenterLambda = 0;
   TauJetsAuxDyn_ClustersMeanFirstEngDens = 0;
   TauJetsAuxDyn_ClustersMeanEMProbability = 0;
   TauJetsAuxDyn_ClustersMeanSecondLambda = 0;
   TauJetsAuxDyn_ClustersMeanPresamplerFrac = 0;
   TauJetsAuxDyn_LeadClusterFrac = 0;
   TauJetsAuxDyn_UpsilonCluster = 0;
   TauJetsAuxDyn_PFOEngRelDiff = 0;
   TauJetsAuxDyn_LC_pantau_interpolPt = 0;
   TauJetsAuxDyn_nModifiedIsolationTracks = 0;
   TauJetsAuxDyn_NUMTRACK = 0;
   TauJetsAuxDyn_MU = 0;
   TauJetsAuxDyn_NUMVERTICES = 0;
   TauJetsAuxDyn_leadTrackEta = 0;
   TauJetsAuxDyn_EMFRACTIONATEMSCALE_MOVEE3 = 0;
   TauJetsAuxDyn_TAU_SEEDTRK_SECMAXSTRIPETOVERPT = 0;
   TauJetsAuxDyn_ABS_ETA_LEAD_TRACK = 0;
   TauJetsAuxDyn_CORRFTRK = 0;
   TauJetsAuxDyn_HADLEAKET = 0;
   TauJetsAuxDyn_TAU_TRT_NHT_OVER_NLT = 0;
   TauJetsAuxDyn_CORRCENTFRAC = 0;
   TauJetsAuxDyn_etHotShotDR1 = 0;
   TauJetsAuxDyn_etHotShotWin = 0;
   TauJetsAuxDyn_etHotShotDR1OverPtLeadTrk = 0;
   TauJetsAuxDyn_absipSigLeadTrk = 0;
   TauTracksAuxDyn_CaloSamplingEtaEM = 0;
   TauTracksAuxDyn_CaloSamplingPhiEM = 0;
   TauTracksAuxDyn_CaloSamplingEtaHad = 0;
   TauTracksAuxDyn_CaloSamplingPhiHad = 0;
   
   InDetTrackParticlesAuxDyn_caloExt_Decorated = 0;
   InDetTrackParticlesAuxDyn_caloExt_eta = 0;
   InDetTrackParticlesAuxDyn_caloExt_phi = 0;
   
     // Set branch addresses and branch pointers
   if (!tree) return;
   fChain = tree;
   fCurrent = -1;
   fChain->SetMakeClass(1);

   fChain->SetBranchAddress("EventInfoAux.runNumber", &EventInfoAux_runNumber, &b_EventInfoAux_runNumber);
   fChain->SetBranchAddress("EventInfoAux.eventNumber", &EventInfoAux_eventNumber, &b_EventInfoAux_eventNumber);
   fChain->SetBranchAddress("EventInfoAux.lumiBlock", &EventInfoAux_lumiBlock, &b_EventInfoAux_lumiBlock);
   fChain->SetBranchAddress("EventInfoAux.timeStamp", &EventInfoAux_timeStamp, &b_EventInfoAux_timeStamp);
   fChain->SetBranchAddress("EventInfoAux.timeStampNSOffset", &EventInfoAux_timeStampNSOffset, &b_EventInfoAux_timeStampNSOffset);
   fChain->SetBranchAddress("EventInfoAux.bcid", &EventInfoAux_bcid, &b_EventInfoAux_bcid);
   fChain->SetBranchAddress("EventInfoAux.detectorMask0", &EventInfoAux_detectorMask0, &b_EventInfoAux_detectorMask0);
   fChain->SetBranchAddress("EventInfoAux.detectorMask1", &EventInfoAux_detectorMask1, &b_EventInfoAux_detectorMask1);
   fChain->SetBranchAddress("EventInfoAux.detectorMask2", &EventInfoAux_detectorMask2, &b_EventInfoAux_detectorMask2);
   fChain->SetBranchAddress("EventInfoAux.detectorMask3", &EventInfoAux_detectorMask3, &b_EventInfoAux_detectorMask3);
   fChain->SetBranchAddress("EventInfoAux.eventTypeBitmask", &EventInfoAux_eventTypeBitmask, &b_EventInfoAux_eventTypeBitmask);
   fChain->SetBranchAddress("EventInfoAux.statusElement", &EventInfoAux_statusElement, &b_EventInfoAux_statusElement);
   fChain->SetBranchAddress("EventInfoAux.extendedLevel1ID", &EventInfoAux_extendedLevel1ID, &b_EventInfoAux_extendedLevel1ID);
   fChain->SetBranchAddress("EventInfoAux.level1TriggerType", &EventInfoAux_level1TriggerType, &b_EventInfoAux_level1TriggerType);
   fChain->SetBranchAddress("EventInfoAux.streamTagNames", &EventInfoAux_streamTagNames, &b_EventInfoAux_streamTagNames);
   fChain->SetBranchAddress("EventInfoAux.streamTagTypes", &EventInfoAux_streamTagTypes, &b_EventInfoAux_streamTagTypes);
   fChain->SetBranchAddress("EventInfoAux.streamTagObeysLumiblock", &EventInfoAux_streamTagObeysLumiblock, &b_EventInfoAux_streamTagObeysLumiblock);
   fChain->SetBranchAddress("EventInfoAux.actualInteractionsPerCrossing", &EventInfoAux_actualInteractionsPerCrossing, &b_EventInfoAux_actualInteractionsPerCrossing);
   fChain->SetBranchAddress("EventInfoAux.averageInteractionsPerCrossing", &EventInfoAux_averageInteractionsPerCrossing, &b_EventInfoAux_averageInteractionsPerCrossing);
   fChain->SetBranchAddress("EventInfoAux.pixelFlags", &EventInfoAux_pixelFlags, &b_EventInfoAux_pixelFlags);
   fChain->SetBranchAddress("EventInfoAux.sctFlags", &EventInfoAux_sctFlags, &b_EventInfoAux_sctFlags);
   fChain->SetBranchAddress("EventInfoAux.trtFlags", &EventInfoAux_trtFlags, &b_EventInfoAux_trtFlags);
   fChain->SetBranchAddress("EventInfoAux.larFlags", &EventInfoAux_larFlags, &b_EventInfoAux_larFlags);
   fChain->SetBranchAddress("EventInfoAux.tileFlags", &EventInfoAux_tileFlags, &b_EventInfoAux_tileFlags);
   fChain->SetBranchAddress("EventInfoAux.muonFlags", &EventInfoAux_muonFlags, &b_EventInfoAux_muonFlags);
   fChain->SetBranchAddress("EventInfoAux.forwardDetFlags", &EventInfoAux_forwardDetFlags, &b_EventInfoAux_forwardDetFlags);
   fChain->SetBranchAddress("EventInfoAux.coreFlags", &EventInfoAux_coreFlags, &b_EventInfoAux_coreFlags);
   fChain->SetBranchAddress("EventInfoAux.backgroundFlags", &EventInfoAux_backgroundFlags, &b_EventInfoAux_backgroundFlags);
   fChain->SetBranchAddress("EventInfoAux.lumiFlags", &EventInfoAux_lumiFlags, &b_EventInfoAux_lumiFlags);
   fChain->SetBranchAddress("EventInfoAux.beamPosX", &EventInfoAux_beamPosX, &b_EventInfoAux_beamPosX);
   fChain->SetBranchAddress("EventInfoAux.beamPosY", &EventInfoAux_beamPosY, &b_EventInfoAux_beamPosY);
   fChain->SetBranchAddress("EventInfoAux.beamPosZ", &EventInfoAux_beamPosZ, &b_EventInfoAux_beamPosZ);
   fChain->SetBranchAddress("EventInfoAux.beamPosSigmaX", &EventInfoAux_beamPosSigmaX, &b_EventInfoAux_beamPosSigmaX);
   fChain->SetBranchAddress("EventInfoAux.beamPosSigmaY", &EventInfoAux_beamPosSigmaY, &b_EventInfoAux_beamPosSigmaY);
   fChain->SetBranchAddress("EventInfoAux.beamPosSigmaZ", &EventInfoAux_beamPosSigmaZ, &b_EventInfoAux_beamPosSigmaZ);
   fChain->SetBranchAddress("EventInfoAux.beamPosSigmaXY", &EventInfoAux_beamPosSigmaXY, &b_EventInfoAux_beamPosSigmaXY);
   fChain->SetBranchAddress("EventInfoAux.beamTiltXZ", &EventInfoAux_beamTiltXZ, &b_EventInfoAux_beamTiltXZ);
   fChain->SetBranchAddress("EventInfoAux.beamTiltYZ", &EventInfoAux_beamTiltYZ, &b_EventInfoAux_beamTiltYZ);
   fChain->SetBranchAddress("EventInfoAux.beamStatus", &EventInfoAux_beamStatus, &b_EventInfoAux_beamStatus);
   fChain->SetBranchAddress("EventInfo", &EventInfo, &b_EventInfo);
   fChain->SetBranchAddress("Electrons", &Electrons, &b_Electrons);
   fChain->SetBranchAddress("PrimaryVertices", &PrimaryVertices, &b_PrimaryVertices);
   fChain->SetBranchAddress("TruthParticlesAux.pdgId", &TruthParticlesAux_pdgId, &b_TruthParticlesAux_pdgId);
   fChain->SetBranchAddress("TruthParticlesAux.barcode", &TruthParticlesAux_barcode, &b_TruthParticlesAux_barcode);
   fChain->SetBranchAddress("TruthParticlesAux.status", &TruthParticlesAux_status, &b_TruthParticlesAux_status);
   fChain->SetBranchAddress("TruthParticlesAux.px", &TruthParticlesAux_px, &b_TruthParticlesAux_px);
   fChain->SetBranchAddress("TruthParticlesAux.py", &TruthParticlesAux_py, &b_TruthParticlesAux_py);
   fChain->SetBranchAddress("TruthParticlesAux.pz", &TruthParticlesAux_pz, &b_TruthParticlesAux_pz);
   fChain->SetBranchAddress("TruthParticlesAux.e", &TruthParticlesAux_e, &b_TruthParticlesAux_e);
   fChain->SetBranchAddress("TruthParticlesAux.m", &TruthParticlesAux_m, &b_TruthParticlesAux_m);
   fChain->SetBranchAddress("ElectronsAux.pt", &ElectronsAux_pt, &b_ElectronsAux_pt);
   fChain->SetBranchAddress("ElectronsAux.eta", &ElectronsAux_eta, &b_ElectronsAux_eta);
   fChain->SetBranchAddress("ElectronsAux.phi", &ElectronsAux_phi, &b_ElectronsAux_phi);
   fChain->SetBranchAddress("ElectronsAux.m", &ElectronsAux_m, &b_ElectronsAux_m);
   fChain->SetBranchAddress("ElectronsAux.author", &ElectronsAux_author, &b_ElectronsAux_author);
   fChain->SetBranchAddress("ElectronsAux.OQ", &ElectronsAux_OQ, &b_ElectronsAux_OQ);
   fChain->SetBranchAddress("ElectronsAux.f1", &ElectronsAux_f1, &b_ElectronsAux_f1);
   fChain->SetBranchAddress("ElectronsAux.f3", &ElectronsAux_f3, &b_ElectronsAux_f3);
   fChain->SetBranchAddress("ElectronsAux.f1core", &ElectronsAux_f1core, &b_ElectronsAux_f1core);
   fChain->SetBranchAddress("ElectronsAux.f3core", &ElectronsAux_f3core, &b_ElectronsAux_f3core);
   fChain->SetBranchAddress("ElectronsAux.weta1", &ElectronsAux_weta1, &b_ElectronsAux_weta1);
   fChain->SetBranchAddress("ElectronsAux.weta2", &ElectronsAux_weta2, &b_ElectronsAux_weta2);
   fChain->SetBranchAddress("ElectronsAux.fracs1", &ElectronsAux_fracs1, &b_ElectronsAux_fracs1);
   fChain->SetBranchAddress("ElectronsAux.wtots1", &ElectronsAux_wtots1, &b_ElectronsAux_wtots1);
   fChain->SetBranchAddress("ElectronsAux.e277", &ElectronsAux_e277, &b_ElectronsAux_e277);
   fChain->SetBranchAddress("ElectronsAux.Reta", &ElectronsAux_Reta, &b_ElectronsAux_Reta);
   fChain->SetBranchAddress("ElectronsAux.Rphi", &ElectronsAux_Rphi, &b_ElectronsAux_Rphi);
   fChain->SetBranchAddress("ElectronsAux.Eratio", &ElectronsAux_Eratio, &b_ElectronsAux_Eratio);
   fChain->SetBranchAddress("ElectronsAux.Rhad", &ElectronsAux_Rhad, &b_ElectronsAux_Rhad);
   fChain->SetBranchAddress("ElectronsAux.Rhad1", &ElectronsAux_Rhad1, &b_ElectronsAux_Rhad1);
   fChain->SetBranchAddress("ElectronsAux.DeltaE", &ElectronsAux_DeltaE, &b_ElectronsAux_DeltaE);
   fChain->SetBranchAddress("ElectronsAux.charge", &ElectronsAux_charge, &b_ElectronsAux_charge);
   fChain->SetBranchAddress("ElectronsAux.deltaEta0", &ElectronsAux_deltaEta0, &b_ElectronsAux_deltaEta0);
   fChain->SetBranchAddress("ElectronsAux.deltaEta1", &ElectronsAux_deltaEta1, &b_ElectronsAux_deltaEta1);
   fChain->SetBranchAddress("ElectronsAux.deltaEta2", &ElectronsAux_deltaEta2, &b_ElectronsAux_deltaEta2);
   fChain->SetBranchAddress("ElectronsAux.deltaEta3", &ElectronsAux_deltaEta3, &b_ElectronsAux_deltaEta3);
   fChain->SetBranchAddress("ElectronsAux.deltaPhi0", &ElectronsAux_deltaPhi0, &b_ElectronsAux_deltaPhi0);
   fChain->SetBranchAddress("ElectronsAux.deltaPhi1", &ElectronsAux_deltaPhi1, &b_ElectronsAux_deltaPhi1);
   fChain->SetBranchAddress("ElectronsAux.deltaPhi2", &ElectronsAux_deltaPhi2, &b_ElectronsAux_deltaPhi2);
   fChain->SetBranchAddress("ElectronsAux.deltaPhi3", &ElectronsAux_deltaPhi3, &b_ElectronsAux_deltaPhi3);
   fChain->SetBranchAddress("ElectronsAux.deltaPhiFromLastMeasurement", &ElectronsAux_deltaPhiFromLastMeasurement, &b_ElectronsAux_deltaPhiFromLastMeasurement);
   fChain->SetBranchAddress("ElectronsAux.deltaPhiRescaled0", &ElectronsAux_deltaPhiRescaled0, &b_ElectronsAux_deltaPhiRescaled0);
   fChain->SetBranchAddress("ElectronsAux.deltaPhiRescaled1", &ElectronsAux_deltaPhiRescaled1, &b_ElectronsAux_deltaPhiRescaled1);
   fChain->SetBranchAddress("ElectronsAux.deltaPhiRescaled2", &ElectronsAux_deltaPhiRescaled2, &b_ElectronsAux_deltaPhiRescaled2);
   fChain->SetBranchAddress("ElectronsAux.deltaPhiRescaled3", &ElectronsAux_deltaPhiRescaled3, &b_ElectronsAux_deltaPhiRescaled3);
   fChain->SetBranchAddress("PixelClusters", &PixelClusters, &b_PixelClusters);
   fChain->SetBranchAddress("TauTracksAux.pt", &TauTracksAux_pt, &b_TauTracksAux_pt);
   fChain->SetBranchAddress("TauTracksAux.eta", &TauTracksAux_eta, &b_TauTracksAux_eta);
   fChain->SetBranchAddress("TauTracksAux.phi", &TauTracksAux_phi, &b_TauTracksAux_phi);
   fChain->SetBranchAddress("TauTracksAux.flagSet", &TauTracksAux_flagSet, &b_TauTracksAux_flagSet);
   fChain->SetBranchAddress("TauTracksAux.bdtScores", &TauTracksAux_bdtScores, &b_TauTracksAux_bdtScores);
   fChain->SetBranchAddress("MuonsAux.pt", &MuonsAux_pt, &b_MuonsAux_pt);
   fChain->SetBranchAddress("MuonsAux.eta", &MuonsAux_eta, &b_MuonsAux_eta);
   fChain->SetBranchAddress("MuonsAux.phi", &MuonsAux_phi, &b_MuonsAux_phi);
   fChain->SetBranchAddress("MuonsAux.charge", &MuonsAux_charge, &b_MuonsAux_charge);
   fChain->SetBranchAddress("MuonsAux.allAuthors", &MuonsAux_allAuthors, &b_MuonsAux_allAuthors);
   fChain->SetBranchAddress("MuonsAux.author", &MuonsAux_author, &b_MuonsAux_author);
   fChain->SetBranchAddress("MuonsAux.muonType", &MuonsAux_muonType, &b_MuonsAux_muonType);
   fChain->SetBranchAddress("MuonsAux.quality", &MuonsAux_quality, &b_MuonsAux_quality);
   fChain->SetBranchAddress("MuonsAux.numberOfPrecisionLayers", &MuonsAux_numberOfPrecisionLayers, &b_MuonsAux_numberOfPrecisionLayers);
   fChain->SetBranchAddress("MuonsAux.numberOfPrecisionHoleLayers", &MuonsAux_numberOfPrecisionHoleLayers, &b_MuonsAux_numberOfPrecisionHoleLayers);
   fChain->SetBranchAddress("MuonsAux.numberOfPhiLayers", &MuonsAux_numberOfPhiLayers, &b_MuonsAux_numberOfPhiLayers);
   fChain->SetBranchAddress("MuonsAux.numberOfPhiHoleLayers", &MuonsAux_numberOfPhiHoleLayers, &b_MuonsAux_numberOfPhiHoleLayers);
   fChain->SetBranchAddress("MuonsAux.numberOfTriggerEtaLayers", &MuonsAux_numberOfTriggerEtaLayers, &b_MuonsAux_numberOfTriggerEtaLayers);
   fChain->SetBranchAddress("MuonsAux.numberOfTriggerEtaHoleLayers", &MuonsAux_numberOfTriggerEtaHoleLayers, &b_MuonsAux_numberOfTriggerEtaHoleLayers);
   fChain->SetBranchAddress("MuonsAux.primarySector", &MuonsAux_primarySector, &b_MuonsAux_primarySector);
   fChain->SetBranchAddress("MuonsAux.secondarySector", &MuonsAux_secondarySector, &b_MuonsAux_secondarySector);
   fChain->SetBranchAddress("MuonsAux.innerSmallHits", &MuonsAux_innerSmallHits, &b_MuonsAux_innerSmallHits);
   fChain->SetBranchAddress("MuonsAux.innerLargeHits", &MuonsAux_innerLargeHits, &b_MuonsAux_innerLargeHits);
   fChain->SetBranchAddress("MuonsAux.middleSmallHits", &MuonsAux_middleSmallHits, &b_MuonsAux_middleSmallHits);
   fChain->SetBranchAddress("MuonsAux.middleLargeHits", &MuonsAux_middleLargeHits, &b_MuonsAux_middleLargeHits);
   fChain->SetBranchAddress("MuonsAux.outerSmallHits", &MuonsAux_outerSmallHits, &b_MuonsAux_outerSmallHits);
   fChain->SetBranchAddress("MuonsAux.outerLargeHits", &MuonsAux_outerLargeHits, &b_MuonsAux_outerLargeHits);
   fChain->SetBranchAddress("MuonsAux.extendedSmallHits", &MuonsAux_extendedSmallHits, &b_MuonsAux_extendedSmallHits);
   fChain->SetBranchAddress("MuonsAux.extendedLargeHits", &MuonsAux_extendedLargeHits, &b_MuonsAux_extendedLargeHits);
   fChain->SetBranchAddress("MuonsAux.innerSmallHoles", &MuonsAux_innerSmallHoles, &b_MuonsAux_innerSmallHoles);
   fChain->SetBranchAddress("MuonsAux.innerLargeHoles", &MuonsAux_innerLargeHoles, &b_MuonsAux_innerLargeHoles);
   fChain->SetBranchAddress("MuonsAux.middleSmallHoles", &MuonsAux_middleSmallHoles, &b_MuonsAux_middleSmallHoles);
   fChain->SetBranchAddress("MuonsAux.middleLargeHoles", &MuonsAux_middleLargeHoles, &b_MuonsAux_middleLargeHoles);
   fChain->SetBranchAddress("MuonsAux.outerSmallHoles", &MuonsAux_outerSmallHoles, &b_MuonsAux_outerSmallHoles);
   fChain->SetBranchAddress("MuonsAux.outerLargeHoles", &MuonsAux_outerLargeHoles, &b_MuonsAux_outerLargeHoles);
   fChain->SetBranchAddress("MuonsAux.extendedSmallHoles", &MuonsAux_extendedSmallHoles, &b_MuonsAux_extendedSmallHoles);
   fChain->SetBranchAddress("MuonsAux.extendedLargeHoles", &MuonsAux_extendedLargeHoles, &b_MuonsAux_extendedLargeHoles);
   fChain->SetBranchAddress("MuonsAux.phiLayer1Hits", &MuonsAux_phiLayer1Hits, &b_MuonsAux_phiLayer1Hits);
   fChain->SetBranchAddress("MuonsAux.phiLayer2Hits", &MuonsAux_phiLayer2Hits, &b_MuonsAux_phiLayer2Hits);
   fChain->SetBranchAddress("MuonsAux.phiLayer3Hits", &MuonsAux_phiLayer3Hits, &b_MuonsAux_phiLayer3Hits);
   fChain->SetBranchAddress("MuonsAux.phiLayer4Hits", &MuonsAux_phiLayer4Hits, &b_MuonsAux_phiLayer4Hits);
   fChain->SetBranchAddress("MuonsAux.etaLayer1Hits", &MuonsAux_etaLayer1Hits, &b_MuonsAux_etaLayer1Hits);
   fChain->SetBranchAddress("MuonsAux.etaLayer2Hits", &MuonsAux_etaLayer2Hits, &b_MuonsAux_etaLayer2Hits);
   fChain->SetBranchAddress("MuonsAux.etaLayer3Hits", &MuonsAux_etaLayer3Hits, &b_MuonsAux_etaLayer3Hits);
   fChain->SetBranchAddress("MuonsAux.etaLayer4Hits", &MuonsAux_etaLayer4Hits, &b_MuonsAux_etaLayer4Hits);
   fChain->SetBranchAddress("MuonsAux.phiLayer1Holes", &MuonsAux_phiLayer1Holes, &b_MuonsAux_phiLayer1Holes);
   fChain->SetBranchAddress("MuonsAux.phiLayer2Holes", &MuonsAux_phiLayer2Holes, &b_MuonsAux_phiLayer2Holes);
   fChain->SetBranchAddress("MuonsAux.phiLayer3Holes", &MuonsAux_phiLayer3Holes, &b_MuonsAux_phiLayer3Holes);
   fChain->SetBranchAddress("MuonsAux.phiLayer4Holes", &MuonsAux_phiLayer4Holes, &b_MuonsAux_phiLayer4Holes);
   fChain->SetBranchAddress("MuonsAux.etaLayer1Holes", &MuonsAux_etaLayer1Holes, &b_MuonsAux_etaLayer1Holes);
   fChain->SetBranchAddress("MuonsAux.etaLayer2Holes", &MuonsAux_etaLayer2Holes, &b_MuonsAux_etaLayer2Holes);
   fChain->SetBranchAddress("MuonsAux.etaLayer3Holes", &MuonsAux_etaLayer3Holes, &b_MuonsAux_etaLayer3Holes);
   fChain->SetBranchAddress("MuonsAux.etaLayer4Holes", &MuonsAux_etaLayer4Holes, &b_MuonsAux_etaLayer4Holes);
   fChain->SetBranchAddress("MuonsAux.phiLayer1RPCHits", &MuonsAux_phiLayer1RPCHits, &b_MuonsAux_phiLayer1RPCHits);
   fChain->SetBranchAddress("MuonsAux.phiLayer2RPCHits", &MuonsAux_phiLayer2RPCHits, &b_MuonsAux_phiLayer2RPCHits);
   fChain->SetBranchAddress("MuonsAux.phiLayer3RPCHits", &MuonsAux_phiLayer3RPCHits, &b_MuonsAux_phiLayer3RPCHits);
   fChain->SetBranchAddress("MuonsAux.phiLayer4RPCHits", &MuonsAux_phiLayer4RPCHits, &b_MuonsAux_phiLayer4RPCHits);
   fChain->SetBranchAddress("MuonsAux.etaLayer1RPCHits", &MuonsAux_etaLayer1RPCHits, &b_MuonsAux_etaLayer1RPCHits);
   fChain->SetBranchAddress("MuonsAux.etaLayer2RPCHits", &MuonsAux_etaLayer2RPCHits, &b_MuonsAux_etaLayer2RPCHits);
   fChain->SetBranchAddress("MuonsAux.etaLayer3RPCHits", &MuonsAux_etaLayer3RPCHits, &b_MuonsAux_etaLayer3RPCHits);
   fChain->SetBranchAddress("MuonsAux.etaLayer4RPCHits", &MuonsAux_etaLayer4RPCHits, &b_MuonsAux_etaLayer4RPCHits);
   fChain->SetBranchAddress("MuonsAux.phiLayer1RPCHoles", &MuonsAux_phiLayer1RPCHoles, &b_MuonsAux_phiLayer1RPCHoles);
   fChain->SetBranchAddress("MuonsAux.phiLayer2RPCHoles", &MuonsAux_phiLayer2RPCHoles, &b_MuonsAux_phiLayer2RPCHoles);
   fChain->SetBranchAddress("MuonsAux.phiLayer3RPCHoles", &MuonsAux_phiLayer3RPCHoles, &b_MuonsAux_phiLayer3RPCHoles);
   fChain->SetBranchAddress("MuonsAux.phiLayer4RPCHoles", &MuonsAux_phiLayer4RPCHoles, &b_MuonsAux_phiLayer4RPCHoles);
   fChain->SetBranchAddress("MuonsAux.etaLayer1RPCHoles", &MuonsAux_etaLayer1RPCHoles, &b_MuonsAux_etaLayer1RPCHoles);
   fChain->SetBranchAddress("MuonsAux.etaLayer2RPCHoles", &MuonsAux_etaLayer2RPCHoles, &b_MuonsAux_etaLayer2RPCHoles);
   fChain->SetBranchAddress("MuonsAux.etaLayer3RPCHoles", &MuonsAux_etaLayer3RPCHoles, &b_MuonsAux_etaLayer3RPCHoles);
   fChain->SetBranchAddress("MuonsAux.etaLayer4RPCHoles", &MuonsAux_etaLayer4RPCHoles, &b_MuonsAux_etaLayer4RPCHoles);
   fChain->SetBranchAddress("MuonsAux.phiLayer1TGCHits", &MuonsAux_phiLayer1TGCHits, &b_MuonsAux_phiLayer1TGCHits);
   fChain->SetBranchAddress("MuonsAux.phiLayer2TGCHits", &MuonsAux_phiLayer2TGCHits, &b_MuonsAux_phiLayer2TGCHits);
   fChain->SetBranchAddress("MuonsAux.phiLayer3TGCHits", &MuonsAux_phiLayer3TGCHits, &b_MuonsAux_phiLayer3TGCHits);
   fChain->SetBranchAddress("MuonsAux.phiLayer4TGCHits", &MuonsAux_phiLayer4TGCHits, &b_MuonsAux_phiLayer4TGCHits);
   fChain->SetBranchAddress("MuonsAux.etaLayer1TGCHits", &MuonsAux_etaLayer1TGCHits, &b_MuonsAux_etaLayer1TGCHits);
   fChain->SetBranchAddress("MuonsAux.etaLayer2TGCHits", &MuonsAux_etaLayer2TGCHits, &b_MuonsAux_etaLayer2TGCHits);
   fChain->SetBranchAddress("MuonsAux.etaLayer3TGCHits", &MuonsAux_etaLayer3TGCHits, &b_MuonsAux_etaLayer3TGCHits);
   fChain->SetBranchAddress("MuonsAux.etaLayer4TGCHits", &MuonsAux_etaLayer4TGCHits, &b_MuonsAux_etaLayer4TGCHits);
   fChain->SetBranchAddress("MuonsAux.phiLayer1TGCHoles", &MuonsAux_phiLayer1TGCHoles, &b_MuonsAux_phiLayer1TGCHoles);
   fChain->SetBranchAddress("MuonsAux.phiLayer2TGCHoles", &MuonsAux_phiLayer2TGCHoles, &b_MuonsAux_phiLayer2TGCHoles);
   fChain->SetBranchAddress("MuonsAux.phiLayer3TGCHoles", &MuonsAux_phiLayer3TGCHoles, &b_MuonsAux_phiLayer3TGCHoles);
   fChain->SetBranchAddress("MuonsAux.phiLayer4TGCHoles", &MuonsAux_phiLayer4TGCHoles, &b_MuonsAux_phiLayer4TGCHoles);
   fChain->SetBranchAddress("MuonsAux.etaLayer1TGCHoles", &MuonsAux_etaLayer1TGCHoles, &b_MuonsAux_etaLayer1TGCHoles);
   fChain->SetBranchAddress("MuonsAux.etaLayer2TGCHoles", &MuonsAux_etaLayer2TGCHoles, &b_MuonsAux_etaLayer2TGCHoles);
   fChain->SetBranchAddress("MuonsAux.etaLayer3TGCHoles", &MuonsAux_etaLayer3TGCHoles, &b_MuonsAux_etaLayer3TGCHoles);
   fChain->SetBranchAddress("MuonsAux.etaLayer4TGCHoles", &MuonsAux_etaLayer4TGCHoles, &b_MuonsAux_etaLayer4TGCHoles);
   fChain->SetBranchAddress("MuonsAux.cscEtaHits", &MuonsAux_cscEtaHits, &b_MuonsAux_cscEtaHits);
   fChain->SetBranchAddress("MuonsAux.cscUnspoiledEtaHits", &MuonsAux_cscUnspoiledEtaHits, &b_MuonsAux_cscUnspoiledEtaHits);
   fChain->SetBranchAddress("MuonsAux.etcone20", &MuonsAux_etcone20, &b_MuonsAux_etcone20);
   fChain->SetBranchAddress("MuonsAux.etcone30", &MuonsAux_etcone30, &b_MuonsAux_etcone30);
   fChain->SetBranchAddress("MuonsAux.etcone40", &MuonsAux_etcone40, &b_MuonsAux_etcone40);
   fChain->SetBranchAddress("MuonsAux.ptcone20", &MuonsAux_ptcone20, &b_MuonsAux_ptcone20);
   fChain->SetBranchAddress("MuonsAux.ptcone30", &MuonsAux_ptcone30, &b_MuonsAux_ptcone30);
   fChain->SetBranchAddress("MuonsAux.ptcone40", &MuonsAux_ptcone40, &b_MuonsAux_ptcone40);
   fChain->SetBranchAddress("MuonsAux.ptvarcone20", &MuonsAux_ptvarcone20, &b_MuonsAux_ptvarcone20);
   fChain->SetBranchAddress("MuonsAux.ptvarcone30", &MuonsAux_ptvarcone30, &b_MuonsAux_ptvarcone30);
   fChain->SetBranchAddress("MuonsAux.ptvarcone40", &MuonsAux_ptvarcone40, &b_MuonsAux_ptvarcone40);
   fChain->SetBranchAddress("MuonsAux.energyLossType", &MuonsAux_energyLossType, &b_MuonsAux_energyLossType);
   fChain->SetBranchAddress("MuonsAux.spectrometerFieldIntegral", &MuonsAux_spectrometerFieldIntegral, &b_MuonsAux_spectrometerFieldIntegral);
   fChain->SetBranchAddress("MuonsAux.scatteringCurvatureSignificance", &MuonsAux_scatteringCurvatureSignificance, &b_MuonsAux_scatteringCurvatureSignificance);
   fChain->SetBranchAddress("MuonsAux.scatteringNeighbourSignificance", &MuonsAux_scatteringNeighbourSignificance, &b_MuonsAux_scatteringNeighbourSignificance);
   fChain->SetBranchAddress("MuonsAux.momentumBalanceSignificance", &MuonsAux_momentumBalanceSignificance, &b_MuonsAux_momentumBalanceSignificance);
   fChain->SetBranchAddress("MuonsAux.segmentDeltaEta", &MuonsAux_segmentDeltaEta, &b_MuonsAux_segmentDeltaEta);
   fChain->SetBranchAddress("MuonsAux.segmentDeltaPhi", &MuonsAux_segmentDeltaPhi, &b_MuonsAux_segmentDeltaPhi);
   fChain->SetBranchAddress("MuonsAux.segmentChi2OverDoF", &MuonsAux_segmentChi2OverDoF, &b_MuonsAux_segmentChi2OverDoF);
   fChain->SetBranchAddress("MuonsAux.t0", &MuonsAux_t0, &b_MuonsAux_t0);
   fChain->SetBranchAddress("MuonsAux.beta", &MuonsAux_beta, &b_MuonsAux_beta);
   fChain->SetBranchAddress("MuonsAux.annBarrel", &MuonsAux_annBarrel, &b_MuonsAux_annBarrel);
   fChain->SetBranchAddress("MuonsAux.annEndCap", &MuonsAux_annEndCap, &b_MuonsAux_annEndCap);
   fChain->SetBranchAddress("MuonsAux.innAngle", &MuonsAux_innAngle, &b_MuonsAux_innAngle);
   fChain->SetBranchAddress("MuonsAux.midAngle", &MuonsAux_midAngle, &b_MuonsAux_midAngle);
   fChain->SetBranchAddress("MuonsAux.msInnerMatchChi2", &MuonsAux_msInnerMatchChi2, &b_MuonsAux_msInnerMatchChi2);
   fChain->SetBranchAddress("MuonsAux.meanDeltaADCCountsMDT", &MuonsAux_meanDeltaADCCountsMDT, &b_MuonsAux_meanDeltaADCCountsMDT);
   fChain->SetBranchAddress("MuonsAux.CaloLRLikelihood", &MuonsAux_CaloLRLikelihood, &b_MuonsAux_CaloLRLikelihood);
   fChain->SetBranchAddress("MuonsAux.EnergyLoss", &MuonsAux_EnergyLoss, &b_MuonsAux_EnergyLoss);
   fChain->SetBranchAddress("MuonsAux.ParamEnergyLoss", &MuonsAux_ParamEnergyLoss, &b_MuonsAux_ParamEnergyLoss);
   fChain->SetBranchAddress("MuonsAux.MeasEnergyLoss", &MuonsAux_MeasEnergyLoss, &b_MuonsAux_MeasEnergyLoss);
   fChain->SetBranchAddress("MuonsAux.EnergyLossSigma", &MuonsAux_EnergyLossSigma, &b_MuonsAux_EnergyLossSigma);
   fChain->SetBranchAddress("MuonsAux.ParamEnergyLossSigmaPlus", &MuonsAux_ParamEnergyLossSigmaPlus, &b_MuonsAux_ParamEnergyLossSigmaPlus);
   fChain->SetBranchAddress("MuonsAux.ParamEnergyLossSigmaMinus", &MuonsAux_ParamEnergyLossSigmaMinus, &b_MuonsAux_ParamEnergyLossSigmaMinus);
   fChain->SetBranchAddress("MuonsAux.MeasEnergyLossSigma", &MuonsAux_MeasEnergyLossSigma, &b_MuonsAux_MeasEnergyLossSigma);
   fChain->SetBranchAddress("MuonsAux.msInnerMatchDOF", &MuonsAux_msInnerMatchDOF, &b_MuonsAux_msInnerMatchDOF);
   fChain->SetBranchAddress("MuonsAux.msOuterMatchDOF", &MuonsAux_msOuterMatchDOF, &b_MuonsAux_msOuterMatchDOF);
   fChain->SetBranchAddress("MuonsAux.CaloMuonIDTag", &MuonsAux_CaloMuonIDTag, &b_MuonsAux_CaloMuonIDTag);
   fChain->SetBranchAddress("TauJets", &TauJets, &b_TauJets);
   fChain->SetBranchAddress("Muons", &Muons, &b_Muons);
   fChain->SetBranchAddress("TruthEvents", &TruthEvents, &b_TruthEvents);
   fChain->SetBranchAddress("GSFTrackParticlesAux.d0", &GSFTrackParticlesAux_d0, &b_GSFTrackParticlesAux_d0);
   fChain->SetBranchAddress("GSFTrackParticlesAux.z0", &GSFTrackParticlesAux_z0, &b_GSFTrackParticlesAux_z0);
   fChain->SetBranchAddress("GSFTrackParticlesAux.phi", &GSFTrackParticlesAux_phi, &b_GSFTrackParticlesAux_phi);
   fChain->SetBranchAddress("GSFTrackParticlesAux.theta", &GSFTrackParticlesAux_theta, &b_GSFTrackParticlesAux_theta);
   fChain->SetBranchAddress("GSFTrackParticlesAux.qOverP", &GSFTrackParticlesAux_qOverP, &b_GSFTrackParticlesAux_qOverP);
   fChain->SetBranchAddress("GSFTrackParticlesAux.definingParametersCovMatrix", &GSFTrackParticlesAux_definingParametersCovMatrix, &b_GSFTrackParticlesAux_definingParametersCovMatrix);
   fChain->SetBranchAddress("GSFTrackParticlesAux.vx", &GSFTrackParticlesAux_vx, &b_GSFTrackParticlesAux_vx);
   fChain->SetBranchAddress("GSFTrackParticlesAux.vy", &GSFTrackParticlesAux_vy, &b_GSFTrackParticlesAux_vy);
   fChain->SetBranchAddress("GSFTrackParticlesAux.vz", &GSFTrackParticlesAux_vz, &b_GSFTrackParticlesAux_vz);
   fChain->SetBranchAddress("GSFTrackParticlesAux.radiusOfFirstHit", &GSFTrackParticlesAux_radiusOfFirstHit, &b_GSFTrackParticlesAux_radiusOfFirstHit);
   fChain->SetBranchAddress("GSFTrackParticlesAux.identifierOfFirstHit", &GSFTrackParticlesAux_identifierOfFirstHit, &b_GSFTrackParticlesAux_identifierOfFirstHit);
   fChain->SetBranchAddress("GSFTrackParticlesAux.beamlineTiltX", &GSFTrackParticlesAux_beamlineTiltX, &b_GSFTrackParticlesAux_beamlineTiltX);
   fChain->SetBranchAddress("GSFTrackParticlesAux.beamlineTiltY", &GSFTrackParticlesAux_beamlineTiltY, &b_GSFTrackParticlesAux_beamlineTiltY);
   fChain->SetBranchAddress("GSFTrackParticlesAux.hitPattern", &GSFTrackParticlesAux_hitPattern, &b_GSFTrackParticlesAux_hitPattern);
   fChain->SetBranchAddress("GSFTrackParticlesAux.chiSquared", &GSFTrackParticlesAux_chiSquared, &b_GSFTrackParticlesAux_chiSquared);
   fChain->SetBranchAddress("GSFTrackParticlesAux.numberDoF", &GSFTrackParticlesAux_numberDoF, &b_GSFTrackParticlesAux_numberDoF);
   fChain->SetBranchAddress("GSFTrackParticlesAux.trackFitter", &GSFTrackParticlesAux_trackFitter, &b_GSFTrackParticlesAux_trackFitter);
   fChain->SetBranchAddress("GSFTrackParticlesAux.particleHypothesis", &GSFTrackParticlesAux_particleHypothesis, &b_GSFTrackParticlesAux_particleHypothesis);
   fChain->SetBranchAddress("GSFTrackParticlesAux.trackProperties", &GSFTrackParticlesAux_trackProperties, &b_GSFTrackParticlesAux_trackProperties);
   fChain->SetBranchAddress("GSFTrackParticlesAux.patternRecoInfo", &GSFTrackParticlesAux_patternRecoInfo, &b_GSFTrackParticlesAux_patternRecoInfo);
   fChain->SetBranchAddress("GSFTrackParticlesAux.numberOfContribPixelLayers", &GSFTrackParticlesAux_numberOfContribPixelLayers, &b_GSFTrackParticlesAux_numberOfContribPixelLayers);
   fChain->SetBranchAddress("GSFTrackParticlesAux.numberOfInnermostPixelLayerHits", &GSFTrackParticlesAux_numberOfInnermostPixelLayerHits, &b_GSFTrackParticlesAux_numberOfInnermostPixelLayerHits);
   fChain->SetBranchAddress("GSFTrackParticlesAux.numberOfInnermostPixelLayerOutliers", &GSFTrackParticlesAux_numberOfInnermostPixelLayerOutliers, &b_GSFTrackParticlesAux_numberOfInnermostPixelLayerOutliers);
   fChain->SetBranchAddress("GSFTrackParticlesAux.numberOfInnermostPixelLayerSharedHits", &GSFTrackParticlesAux_numberOfInnermostPixelLayerSharedHits, &b_GSFTrackParticlesAux_numberOfInnermostPixelLayerSharedHits);
   fChain->SetBranchAddress("GSFTrackParticlesAux.numberOfInnermostPixelLayerSplitHits", &GSFTrackParticlesAux_numberOfInnermostPixelLayerSplitHits, &b_GSFTrackParticlesAux_numberOfInnermostPixelLayerSplitHits);
   fChain->SetBranchAddress("GSFTrackParticlesAux.expectInnermostPixelLayerHit", &GSFTrackParticlesAux_expectInnermostPixelLayerHit, &b_GSFTrackParticlesAux_expectInnermostPixelLayerHit);
   fChain->SetBranchAddress("GSFTrackParticlesAux.numberOfNextToInnermostPixelLayerHits", &GSFTrackParticlesAux_numberOfNextToInnermostPixelLayerHits, &b_GSFTrackParticlesAux_numberOfNextToInnermostPixelLayerHits);
   fChain->SetBranchAddress("GSFTrackParticlesAux.numberOfNextToInnermostPixelLayerOutliers", &GSFTrackParticlesAux_numberOfNextToInnermostPixelLayerOutliers, &b_GSFTrackParticlesAux_numberOfNextToInnermostPixelLayerOutliers);
   fChain->SetBranchAddress("GSFTrackParticlesAux.numberOfNextToInnermostPixelLayerSharedHits", &GSFTrackParticlesAux_numberOfNextToInnermostPixelLayerSharedHits, &b_GSFTrackParticlesAux_numberOfNextToInnermostPixelLayerSharedHits);
   fChain->SetBranchAddress("GSFTrackParticlesAux.numberOfNextToInnermostPixelLayerSplitHits", &GSFTrackParticlesAux_numberOfNextToInnermostPixelLayerSplitHits, &b_GSFTrackParticlesAux_numberOfNextToInnermostPixelLayerSplitHits);
   fChain->SetBranchAddress("GSFTrackParticlesAux.expectNextToInnermostPixelLayerHit", &GSFTrackParticlesAux_expectNextToInnermostPixelLayerHit, &b_GSFTrackParticlesAux_expectNextToInnermostPixelLayerHit);
   fChain->SetBranchAddress("GSFTrackParticlesAux.numberOfPixelHits", &GSFTrackParticlesAux_numberOfPixelHits, &b_GSFTrackParticlesAux_numberOfPixelHits);
   fChain->SetBranchAddress("GSFTrackParticlesAux.numberOfPixelOutliers", &GSFTrackParticlesAux_numberOfPixelOutliers, &b_GSFTrackParticlesAux_numberOfPixelOutliers);
   fChain->SetBranchAddress("GSFTrackParticlesAux.numberOfPixelHoles", &GSFTrackParticlesAux_numberOfPixelHoles, &b_GSFTrackParticlesAux_numberOfPixelHoles);
   fChain->SetBranchAddress("GSFTrackParticlesAux.numberOfPixelSharedHits", &GSFTrackParticlesAux_numberOfPixelSharedHits, &b_GSFTrackParticlesAux_numberOfPixelSharedHits);
   fChain->SetBranchAddress("GSFTrackParticlesAux.numberOfPixelSplitHits", &GSFTrackParticlesAux_numberOfPixelSplitHits, &b_GSFTrackParticlesAux_numberOfPixelSplitHits);
   fChain->SetBranchAddress("GSFTrackParticlesAux.numberOfGangedPixels", &GSFTrackParticlesAux_numberOfGangedPixels, &b_GSFTrackParticlesAux_numberOfGangedPixels);
   fChain->SetBranchAddress("GSFTrackParticlesAux.numberOfGangedFlaggedFakes", &GSFTrackParticlesAux_numberOfGangedFlaggedFakes, &b_GSFTrackParticlesAux_numberOfGangedFlaggedFakes);
   fChain->SetBranchAddress("GSFTrackParticlesAux.numberOfPixelDeadSensors", &GSFTrackParticlesAux_numberOfPixelDeadSensors, &b_GSFTrackParticlesAux_numberOfPixelDeadSensors);
   fChain->SetBranchAddress("GSFTrackParticlesAux.numberOfPixelSpoiltHits", &GSFTrackParticlesAux_numberOfPixelSpoiltHits, &b_GSFTrackParticlesAux_numberOfPixelSpoiltHits);
   fChain->SetBranchAddress("GSFTrackParticlesAux.numberOfDBMHits", &GSFTrackParticlesAux_numberOfDBMHits, &b_GSFTrackParticlesAux_numberOfDBMHits);
   fChain->SetBranchAddress("GSFTrackParticlesAux.numberOfSCTHits", &GSFTrackParticlesAux_numberOfSCTHits, &b_GSFTrackParticlesAux_numberOfSCTHits);
   fChain->SetBranchAddress("GSFTrackParticlesAux.numberOfSCTOutliers", &GSFTrackParticlesAux_numberOfSCTOutliers, &b_GSFTrackParticlesAux_numberOfSCTOutliers);
   fChain->SetBranchAddress("GSFTrackParticlesAux.numberOfSCTHoles", &GSFTrackParticlesAux_numberOfSCTHoles, &b_GSFTrackParticlesAux_numberOfSCTHoles);
   fChain->SetBranchAddress("GSFTrackParticlesAux.numberOfSCTDoubleHoles", &GSFTrackParticlesAux_numberOfSCTDoubleHoles, &b_GSFTrackParticlesAux_numberOfSCTDoubleHoles);
   fChain->SetBranchAddress("GSFTrackParticlesAux.numberOfSCTSharedHits", &GSFTrackParticlesAux_numberOfSCTSharedHits, &b_GSFTrackParticlesAux_numberOfSCTSharedHits);
   fChain->SetBranchAddress("GSFTrackParticlesAux.numberOfSCTDeadSensors", &GSFTrackParticlesAux_numberOfSCTDeadSensors, &b_GSFTrackParticlesAux_numberOfSCTDeadSensors);
   fChain->SetBranchAddress("GSFTrackParticlesAux.numberOfSCTSpoiltHits", &GSFTrackParticlesAux_numberOfSCTSpoiltHits, &b_GSFTrackParticlesAux_numberOfSCTSpoiltHits);
   fChain->SetBranchAddress("GSFTrackParticlesAux.numberOfTRTHits", &GSFTrackParticlesAux_numberOfTRTHits, &b_GSFTrackParticlesAux_numberOfTRTHits);
   fChain->SetBranchAddress("GSFTrackParticlesAux.numberOfTRTOutliers", &GSFTrackParticlesAux_numberOfTRTOutliers, &b_GSFTrackParticlesAux_numberOfTRTOutliers);
   fChain->SetBranchAddress("GSFTrackParticlesAux.numberOfTRTHoles", &GSFTrackParticlesAux_numberOfTRTHoles, &b_GSFTrackParticlesAux_numberOfTRTHoles);
   fChain->SetBranchAddress("GSFTrackParticlesAux.numberOfTRTHighThresholdHits", &GSFTrackParticlesAux_numberOfTRTHighThresholdHits, &b_GSFTrackParticlesAux_numberOfTRTHighThresholdHits);
   fChain->SetBranchAddress("GSFTrackParticlesAux.numberOfTRTHighThresholdHitsTotal", &GSFTrackParticlesAux_numberOfTRTHighThresholdHitsTotal, &b_GSFTrackParticlesAux_numberOfTRTHighThresholdHitsTotal);
   fChain->SetBranchAddress("GSFTrackParticlesAux.numberOfTRTHighThresholdOutliers", &GSFTrackParticlesAux_numberOfTRTHighThresholdOutliers, &b_GSFTrackParticlesAux_numberOfTRTHighThresholdOutliers);
   fChain->SetBranchAddress("GSFTrackParticlesAux.numberOfTRTDeadStraws", &GSFTrackParticlesAux_numberOfTRTDeadStraws, &b_GSFTrackParticlesAux_numberOfTRTDeadStraws);
   fChain->SetBranchAddress("GSFTrackParticlesAux.numberOfTRTTubeHits", &GSFTrackParticlesAux_numberOfTRTTubeHits, &b_GSFTrackParticlesAux_numberOfTRTTubeHits);
   fChain->SetBranchAddress("GSFTrackParticlesAux.numberOfTRTXenonHits", &GSFTrackParticlesAux_numberOfTRTXenonHits, &b_GSFTrackParticlesAux_numberOfTRTXenonHits);
   fChain->SetBranchAddress("GSFTrackParticlesAux.numberOfTRTSharedHits", &GSFTrackParticlesAux_numberOfTRTSharedHits, &b_GSFTrackParticlesAux_numberOfTRTSharedHits);
   fChain->SetBranchAddress("GSFTrackParticlesAux.numberOfPrecisionLayers", &GSFTrackParticlesAux_numberOfPrecisionLayers, &b_GSFTrackParticlesAux_numberOfPrecisionLayers);
   fChain->SetBranchAddress("GSFTrackParticlesAux.numberOfPrecisionHoleLayers", &GSFTrackParticlesAux_numberOfPrecisionHoleLayers, &b_GSFTrackParticlesAux_numberOfPrecisionHoleLayers);
   fChain->SetBranchAddress("GSFTrackParticlesAux.numberOfPhiLayers", &GSFTrackParticlesAux_numberOfPhiLayers, &b_GSFTrackParticlesAux_numberOfPhiLayers);
   fChain->SetBranchAddress("GSFTrackParticlesAux.numberOfPhiHoleLayers", &GSFTrackParticlesAux_numberOfPhiHoleLayers, &b_GSFTrackParticlesAux_numberOfPhiHoleLayers);
   fChain->SetBranchAddress("GSFTrackParticlesAux.numberOfTriggerEtaLayers", &GSFTrackParticlesAux_numberOfTriggerEtaLayers, &b_GSFTrackParticlesAux_numberOfTriggerEtaLayers);
   fChain->SetBranchAddress("GSFTrackParticlesAux.numberOfTriggerEtaHoleLayers", &GSFTrackParticlesAux_numberOfTriggerEtaHoleLayers, &b_GSFTrackParticlesAux_numberOfTriggerEtaHoleLayers);
   fChain->SetBranchAddress("GSFTrackParticlesAux.numberOfOutliersOnTrack", &GSFTrackParticlesAux_numberOfOutliersOnTrack, &b_GSFTrackParticlesAux_numberOfOutliersOnTrack);
   fChain->SetBranchAddress("GSFTrackParticlesAux.standardDeviationOfChi2OS", &GSFTrackParticlesAux_standardDeviationOfChi2OS, &b_GSFTrackParticlesAux_standardDeviationOfChi2OS);
   fChain->SetBranchAddress("GSFTrackParticlesAux.eProbabilityComb", &GSFTrackParticlesAux_eProbabilityComb, &b_GSFTrackParticlesAux_eProbabilityComb);
   fChain->SetBranchAddress("GSFTrackParticlesAux.eProbabilityHT", &GSFTrackParticlesAux_eProbabilityHT, &b_GSFTrackParticlesAux_eProbabilityHT);
   fChain->SetBranchAddress("GSFTrackParticlesAux.pixeldEdx", &GSFTrackParticlesAux_pixeldEdx, &b_GSFTrackParticlesAux_pixeldEdx);
   fChain->SetBranchAddress("GSFTrackParticlesAux.numberOfUsedHitsdEdx", &GSFTrackParticlesAux_numberOfUsedHitsdEdx, &b_GSFTrackParticlesAux_numberOfUsedHitsdEdx);
   fChain->SetBranchAddress("GSFTrackParticlesAux.numberOfIBLOverflowsdEdx", &GSFTrackParticlesAux_numberOfIBLOverflowsdEdx, &b_GSFTrackParticlesAux_numberOfIBLOverflowsdEdx);
   fChain->SetBranchAddress("GSFTrackParticlesAux.TRTTrackOccupancy", &GSFTrackParticlesAux_TRTTrackOccupancy, &b_GSFTrackParticlesAux_TRTTrackOccupancy);
   fChain->SetBranchAddress("InDetTrackParticlesAux.d0", &InDetTrackParticlesAux_d0, &b_InDetTrackParticlesAux_d0);
   fChain->SetBranchAddress("InDetTrackParticlesAux.z0", &InDetTrackParticlesAux_z0, &b_InDetTrackParticlesAux_z0);
   fChain->SetBranchAddress("InDetTrackParticlesAux.phi", &InDetTrackParticlesAux_phi, &b_InDetTrackParticlesAux_phi);
   fChain->SetBranchAddress("InDetTrackParticlesAux.theta", &InDetTrackParticlesAux_theta, &b_InDetTrackParticlesAux_theta);
   fChain->SetBranchAddress("InDetTrackParticlesAux.qOverP", &InDetTrackParticlesAux_qOverP, &b_InDetTrackParticlesAux_qOverP);
   fChain->SetBranchAddress("InDetTrackParticlesAux.definingParametersCovMatrix", &InDetTrackParticlesAux_definingParametersCovMatrix, &b_InDetTrackParticlesAux_definingParametersCovMatrix);
   fChain->SetBranchAddress("InDetTrackParticlesAux.vx", &InDetTrackParticlesAux_vx, &b_InDetTrackParticlesAux_vx);
   fChain->SetBranchAddress("InDetTrackParticlesAux.vy", &InDetTrackParticlesAux_vy, &b_InDetTrackParticlesAux_vy);
   fChain->SetBranchAddress("InDetTrackParticlesAux.vz", &InDetTrackParticlesAux_vz, &b_InDetTrackParticlesAux_vz);
   fChain->SetBranchAddress("InDetTrackParticlesAux.radiusOfFirstHit", &InDetTrackParticlesAux_radiusOfFirstHit, &b_InDetTrackParticlesAux_radiusOfFirstHit);
   fChain->SetBranchAddress("InDetTrackParticlesAux.identifierOfFirstHit", &InDetTrackParticlesAux_identifierOfFirstHit, &b_InDetTrackParticlesAux_identifierOfFirstHit);
   fChain->SetBranchAddress("InDetTrackParticlesAux.beamlineTiltX", &InDetTrackParticlesAux_beamlineTiltX, &b_InDetTrackParticlesAux_beamlineTiltX);
   fChain->SetBranchAddress("InDetTrackParticlesAux.beamlineTiltY", &InDetTrackParticlesAux_beamlineTiltY, &b_InDetTrackParticlesAux_beamlineTiltY);
   fChain->SetBranchAddress("InDetTrackParticlesAux.hitPattern", &InDetTrackParticlesAux_hitPattern, &b_InDetTrackParticlesAux_hitPattern);
   fChain->SetBranchAddress("InDetTrackParticlesAux.chiSquared", &InDetTrackParticlesAux_chiSquared, &b_InDetTrackParticlesAux_chiSquared);
   fChain->SetBranchAddress("InDetTrackParticlesAux.numberDoF", &InDetTrackParticlesAux_numberDoF, &b_InDetTrackParticlesAux_numberDoF);
   fChain->SetBranchAddress("InDetTrackParticlesAux.trackFitter", &InDetTrackParticlesAux_trackFitter, &b_InDetTrackParticlesAux_trackFitter);
   fChain->SetBranchAddress("InDetTrackParticlesAux.particleHypothesis", &InDetTrackParticlesAux_particleHypothesis, &b_InDetTrackParticlesAux_particleHypothesis);
   fChain->SetBranchAddress("InDetTrackParticlesAux.trackProperties", &InDetTrackParticlesAux_trackProperties, &b_InDetTrackParticlesAux_trackProperties);
   fChain->SetBranchAddress("InDetTrackParticlesAux.patternRecoInfo", &InDetTrackParticlesAux_patternRecoInfo, &b_InDetTrackParticlesAux_patternRecoInfo);
   fChain->SetBranchAddress("InDetTrackParticlesAux.numberOfContribPixelLayers", &InDetTrackParticlesAux_numberOfContribPixelLayers, &b_InDetTrackParticlesAux_numberOfContribPixelLayers);
   fChain->SetBranchAddress("InDetTrackParticlesAux.numberOfInnermostPixelLayerHits", &InDetTrackParticlesAux_numberOfInnermostPixelLayerHits, &b_InDetTrackParticlesAux_numberOfInnermostPixelLayerHits);
   fChain->SetBranchAddress("InDetTrackParticlesAux.numberOfInnermostPixelLayerOutliers", &InDetTrackParticlesAux_numberOfInnermostPixelLayerOutliers, &b_InDetTrackParticlesAux_numberOfInnermostPixelLayerOutliers);
   fChain->SetBranchAddress("InDetTrackParticlesAux.numberOfInnermostPixelLayerSharedHits", &InDetTrackParticlesAux_numberOfInnermostPixelLayerSharedHits, &b_InDetTrackParticlesAux_numberOfInnermostPixelLayerSharedHits);
   fChain->SetBranchAddress("InDetTrackParticlesAux.numberOfInnermostPixelLayerSplitHits", &InDetTrackParticlesAux_numberOfInnermostPixelLayerSplitHits, &b_InDetTrackParticlesAux_numberOfInnermostPixelLayerSplitHits);
   fChain->SetBranchAddress("InDetTrackParticlesAux.expectInnermostPixelLayerHit", &InDetTrackParticlesAux_expectInnermostPixelLayerHit, &b_InDetTrackParticlesAux_expectInnermostPixelLayerHit);
   fChain->SetBranchAddress("InDetTrackParticlesAux.numberOfNextToInnermostPixelLayerHits", &InDetTrackParticlesAux_numberOfNextToInnermostPixelLayerHits, &b_InDetTrackParticlesAux_numberOfNextToInnermostPixelLayerHits);
   fChain->SetBranchAddress("InDetTrackParticlesAux.numberOfNextToInnermostPixelLayerOutliers", &InDetTrackParticlesAux_numberOfNextToInnermostPixelLayerOutliers, &b_InDetTrackParticlesAux_numberOfNextToInnermostPixelLayerOutliers);
   fChain->SetBranchAddress("InDetTrackParticlesAux.numberOfNextToInnermostPixelLayerSharedHits", &InDetTrackParticlesAux_numberOfNextToInnermostPixelLayerSharedHits, &b_InDetTrackParticlesAux_numberOfNextToInnermostPixelLayerSharedHits);
   fChain->SetBranchAddress("InDetTrackParticlesAux.numberOfNextToInnermostPixelLayerSplitHits", &InDetTrackParticlesAux_numberOfNextToInnermostPixelLayerSplitHits, &b_InDetTrackParticlesAux_numberOfNextToInnermostPixelLayerSplitHits);
   fChain->SetBranchAddress("InDetTrackParticlesAux.expectNextToInnermostPixelLayerHit", &InDetTrackParticlesAux_expectNextToInnermostPixelLayerHit, &b_InDetTrackParticlesAux_expectNextToInnermostPixelLayerHit);
   fChain->SetBranchAddress("InDetTrackParticlesAux.numberOfPixelHits", &InDetTrackParticlesAux_numberOfPixelHits, &b_InDetTrackParticlesAux_numberOfPixelHits);
   fChain->SetBranchAddress("InDetTrackParticlesAux.numberOfPixelOutliers", &InDetTrackParticlesAux_numberOfPixelOutliers, &b_InDetTrackParticlesAux_numberOfPixelOutliers);
   fChain->SetBranchAddress("InDetTrackParticlesAux.numberOfPixelHoles", &InDetTrackParticlesAux_numberOfPixelHoles, &b_InDetTrackParticlesAux_numberOfPixelHoles);
   fChain->SetBranchAddress("InDetTrackParticlesAux.numberOfPixelSharedHits", &InDetTrackParticlesAux_numberOfPixelSharedHits, &b_InDetTrackParticlesAux_numberOfPixelSharedHits);
   fChain->SetBranchAddress("InDetTrackParticlesAux.numberOfPixelSplitHits", &InDetTrackParticlesAux_numberOfPixelSplitHits, &b_InDetTrackParticlesAux_numberOfPixelSplitHits);
   fChain->SetBranchAddress("InDetTrackParticlesAux.numberOfGangedPixels", &InDetTrackParticlesAux_numberOfGangedPixels, &b_InDetTrackParticlesAux_numberOfGangedPixels);
   fChain->SetBranchAddress("InDetTrackParticlesAux.numberOfGangedFlaggedFakes", &InDetTrackParticlesAux_numberOfGangedFlaggedFakes, &b_InDetTrackParticlesAux_numberOfGangedFlaggedFakes);
   fChain->SetBranchAddress("InDetTrackParticlesAux.numberOfPixelDeadSensors", &InDetTrackParticlesAux_numberOfPixelDeadSensors, &b_InDetTrackParticlesAux_numberOfPixelDeadSensors);
   fChain->SetBranchAddress("InDetTrackParticlesAux.numberOfPixelSpoiltHits", &InDetTrackParticlesAux_numberOfPixelSpoiltHits, &b_InDetTrackParticlesAux_numberOfPixelSpoiltHits);
   fChain->SetBranchAddress("InDetTrackParticlesAux.numberOfDBMHits", &InDetTrackParticlesAux_numberOfDBMHits, &b_InDetTrackParticlesAux_numberOfDBMHits);
   fChain->SetBranchAddress("InDetTrackParticlesAux.numberOfSCTHits", &InDetTrackParticlesAux_numberOfSCTHits, &b_InDetTrackParticlesAux_numberOfSCTHits);
   fChain->SetBranchAddress("InDetTrackParticlesAux.numberOfSCTOutliers", &InDetTrackParticlesAux_numberOfSCTOutliers, &b_InDetTrackParticlesAux_numberOfSCTOutliers);
   fChain->SetBranchAddress("InDetTrackParticlesAux.numberOfSCTHoles", &InDetTrackParticlesAux_numberOfSCTHoles, &b_InDetTrackParticlesAux_numberOfSCTHoles);
   fChain->SetBranchAddress("InDetTrackParticlesAux.numberOfSCTDoubleHoles", &InDetTrackParticlesAux_numberOfSCTDoubleHoles, &b_InDetTrackParticlesAux_numberOfSCTDoubleHoles);
   fChain->SetBranchAddress("InDetTrackParticlesAux.numberOfSCTSharedHits", &InDetTrackParticlesAux_numberOfSCTSharedHits, &b_InDetTrackParticlesAux_numberOfSCTSharedHits);
   fChain->SetBranchAddress("InDetTrackParticlesAux.numberOfSCTDeadSensors", &InDetTrackParticlesAux_numberOfSCTDeadSensors, &b_InDetTrackParticlesAux_numberOfSCTDeadSensors);
   fChain->SetBranchAddress("InDetTrackParticlesAux.numberOfSCTSpoiltHits", &InDetTrackParticlesAux_numberOfSCTSpoiltHits, &b_InDetTrackParticlesAux_numberOfSCTSpoiltHits);
   fChain->SetBranchAddress("InDetTrackParticlesAux.numberOfTRTHits", &InDetTrackParticlesAux_numberOfTRTHits, &b_InDetTrackParticlesAux_numberOfTRTHits);
   fChain->SetBranchAddress("InDetTrackParticlesAux.numberOfTRTOutliers", &InDetTrackParticlesAux_numberOfTRTOutliers, &b_InDetTrackParticlesAux_numberOfTRTOutliers);
   fChain->SetBranchAddress("InDetTrackParticlesAux.numberOfTRTHoles", &InDetTrackParticlesAux_numberOfTRTHoles, &b_InDetTrackParticlesAux_numberOfTRTHoles);
   fChain->SetBranchAddress("InDetTrackParticlesAux.numberOfTRTHighThresholdHits", &InDetTrackParticlesAux_numberOfTRTHighThresholdHits, &b_InDetTrackParticlesAux_numberOfTRTHighThresholdHits);
   fChain->SetBranchAddress("InDetTrackParticlesAux.numberOfTRTHighThresholdHitsTotal", &InDetTrackParticlesAux_numberOfTRTHighThresholdHitsTotal, &b_InDetTrackParticlesAux_numberOfTRTHighThresholdHitsTotal);
   fChain->SetBranchAddress("InDetTrackParticlesAux.numberOfTRTHighThresholdOutliers", &InDetTrackParticlesAux_numberOfTRTHighThresholdOutliers, &b_InDetTrackParticlesAux_numberOfTRTHighThresholdOutliers);
   fChain->SetBranchAddress("InDetTrackParticlesAux.numberOfTRTDeadStraws", &InDetTrackParticlesAux_numberOfTRTDeadStraws, &b_InDetTrackParticlesAux_numberOfTRTDeadStraws);
   fChain->SetBranchAddress("InDetTrackParticlesAux.numberOfTRTTubeHits", &InDetTrackParticlesAux_numberOfTRTTubeHits, &b_InDetTrackParticlesAux_numberOfTRTTubeHits);
   fChain->SetBranchAddress("InDetTrackParticlesAux.numberOfTRTXenonHits", &InDetTrackParticlesAux_numberOfTRTXenonHits, &b_InDetTrackParticlesAux_numberOfTRTXenonHits);
   fChain->SetBranchAddress("InDetTrackParticlesAux.numberOfTRTSharedHits", &InDetTrackParticlesAux_numberOfTRTSharedHits, &b_InDetTrackParticlesAux_numberOfTRTSharedHits);
   fChain->SetBranchAddress("InDetTrackParticlesAux.numberOfPrecisionLayers", &InDetTrackParticlesAux_numberOfPrecisionLayers, &b_InDetTrackParticlesAux_numberOfPrecisionLayers);
   fChain->SetBranchAddress("InDetTrackParticlesAux.numberOfPrecisionHoleLayers", &InDetTrackParticlesAux_numberOfPrecisionHoleLayers, &b_InDetTrackParticlesAux_numberOfPrecisionHoleLayers);
   fChain->SetBranchAddress("InDetTrackParticlesAux.numberOfPhiLayers", &InDetTrackParticlesAux_numberOfPhiLayers, &b_InDetTrackParticlesAux_numberOfPhiLayers);
   fChain->SetBranchAddress("InDetTrackParticlesAux.numberOfPhiHoleLayers", &InDetTrackParticlesAux_numberOfPhiHoleLayers, &b_InDetTrackParticlesAux_numberOfPhiHoleLayers);
   fChain->SetBranchAddress("InDetTrackParticlesAux.numberOfTriggerEtaLayers", &InDetTrackParticlesAux_numberOfTriggerEtaLayers, &b_InDetTrackParticlesAux_numberOfTriggerEtaLayers);
   fChain->SetBranchAddress("InDetTrackParticlesAux.numberOfTriggerEtaHoleLayers", &InDetTrackParticlesAux_numberOfTriggerEtaHoleLayers, &b_InDetTrackParticlesAux_numberOfTriggerEtaHoleLayers);
   fChain->SetBranchAddress("InDetTrackParticlesAux.numberOfOutliersOnTrack", &InDetTrackParticlesAux_numberOfOutliersOnTrack, &b_InDetTrackParticlesAux_numberOfOutliersOnTrack);
   fChain->SetBranchAddress("InDetTrackParticlesAux.standardDeviationOfChi2OS", &InDetTrackParticlesAux_standardDeviationOfChi2OS, &b_InDetTrackParticlesAux_standardDeviationOfChi2OS);
   fChain->SetBranchAddress("InDetTrackParticlesAux.eProbabilityComb", &InDetTrackParticlesAux_eProbabilityComb, &b_InDetTrackParticlesAux_eProbabilityComb);
   fChain->SetBranchAddress("InDetTrackParticlesAux.eProbabilityHT", &InDetTrackParticlesAux_eProbabilityHT, &b_InDetTrackParticlesAux_eProbabilityHT);
   fChain->SetBranchAddress("InDetTrackParticlesAux.pixeldEdx", &InDetTrackParticlesAux_pixeldEdx, &b_InDetTrackParticlesAux_pixeldEdx);
   fChain->SetBranchAddress("InDetTrackParticlesAux.numberOfUsedHitsdEdx", &InDetTrackParticlesAux_numberOfUsedHitsdEdx, &b_InDetTrackParticlesAux_numberOfUsedHitsdEdx);
   fChain->SetBranchAddress("InDetTrackParticlesAux.numberOfIBLOverflowsdEdx", &InDetTrackParticlesAux_numberOfIBLOverflowsdEdx, &b_InDetTrackParticlesAux_numberOfIBLOverflowsdEdx);
   fChain->SetBranchAddress("InDetTrackParticlesAux.TRTTrackOccupancy", &InDetTrackParticlesAux_TRTTrackOccupancy, &b_InDetTrackParticlesAux_TRTTrackOccupancy);
   fChain->SetBranchAddress("PixelMSOSsAux.type", &PixelMSOSsAux_type, &b_PixelMSOSsAux_type);
   fChain->SetBranchAddress("PixelMSOSsAux.detElementId", &PixelMSOSsAux_detElementId, &b_PixelMSOSsAux_detElementId);
   fChain->SetBranchAddress("PixelMSOSsAux.detType", &PixelMSOSsAux_detType, &b_PixelMSOSsAux_detType);
   fChain->SetBranchAddress("PixelMSOSsAux.localX", &PixelMSOSsAux_localX, &b_PixelMSOSsAux_localX);
   fChain->SetBranchAddress("PixelMSOSsAux.localY", &PixelMSOSsAux_localY, &b_PixelMSOSsAux_localY);
   fChain->SetBranchAddress("PixelMSOSsAux.localTheta", &PixelMSOSsAux_localTheta, &b_PixelMSOSsAux_localTheta);
   fChain->SetBranchAddress("PixelMSOSsAux.localPhi", &PixelMSOSsAux_localPhi, &b_PixelMSOSsAux_localPhi);
   fChain->SetBranchAddress("PixelMSOSsAux.unbiasedResidualX", &PixelMSOSsAux_unbiasedResidualX, &b_PixelMSOSsAux_unbiasedResidualX);
   fChain->SetBranchAddress("PixelMSOSsAux.unbiasedResidualY", &PixelMSOSsAux_unbiasedResidualY, &b_PixelMSOSsAux_unbiasedResidualY);
   fChain->SetBranchAddress("PixelMSOSsAux.unbiasedPullX", &PixelMSOSsAux_unbiasedPullX, &b_PixelMSOSsAux_unbiasedPullX);
   fChain->SetBranchAddress("PixelMSOSsAux.unbiasedPullY", &PixelMSOSsAux_unbiasedPullY, &b_PixelMSOSsAux_unbiasedPullY);
   fChain->SetBranchAddress("PixelMSOSsAux.biasedResidualX", &PixelMSOSsAux_biasedResidualX, &b_PixelMSOSsAux_biasedResidualX);
   fChain->SetBranchAddress("PixelMSOSsAux.biasedResidualY", &PixelMSOSsAux_biasedResidualY, &b_PixelMSOSsAux_biasedResidualY);
   fChain->SetBranchAddress("PixelMSOSsAux.biasedPullX", &PixelMSOSsAux_biasedPullX, &b_PixelMSOSsAux_biasedPullX);
   fChain->SetBranchAddress("PixelMSOSsAux.biasedPullY", &PixelMSOSsAux_biasedPullY, &b_PixelMSOSsAux_biasedPullY);
   fChain->SetBranchAddress("JetInputTruthParticles", &JetInputTruthParticles, &b_JetInputTruthParticles);
   fChain->SetBranchAddress("JetInputTruthParticlesNoWZ", &JetInputTruthParticlesNoWZ, &b_JetInputTruthParticlesNoWZ);
   fChain->SetBranchAddress("MuonTruthParticles", &MuonTruthParticles, &b_MuonTruthParticles);
   fChain->SetBranchAddress("TruthLabelBHadronsFinal", &TruthLabelBHadronsFinal, &b_TruthLabelBHadronsFinal);
   fChain->SetBranchAddress("TruthLabelBHadronsInitial", &TruthLabelBHadronsInitial, &b_TruthLabelBHadronsInitial);
   fChain->SetBranchAddress("TruthLabelBQuarksFinal", &TruthLabelBQuarksFinal, &b_TruthLabelBQuarksFinal);
   fChain->SetBranchAddress("TruthLabelCHadronsFinal", &TruthLabelCHadronsFinal, &b_TruthLabelCHadronsFinal);
   fChain->SetBranchAddress("TruthLabelCHadronsInitial", &TruthLabelCHadronsInitial, &b_TruthLabelCHadronsInitial);
   fChain->SetBranchAddress("TruthLabelCQuarksFinal", &TruthLabelCQuarksFinal, &b_TruthLabelCQuarksFinal);
   fChain->SetBranchAddress("TruthLabelHBosons", &TruthLabelHBosons, &b_TruthLabelHBosons);
   fChain->SetBranchAddress("TruthLabelPartons", &TruthLabelPartons, &b_TruthLabelPartons);
   fChain->SetBranchAddress("TruthLabelTQuarksFinal", &TruthLabelTQuarksFinal, &b_TruthLabelTQuarksFinal);
   fChain->SetBranchAddress("TruthLabelTausFinal", &TruthLabelTausFinal, &b_TruthLabelTausFinal);
   fChain->SetBranchAddress("TruthLabelWBosons", &TruthLabelWBosons, &b_TruthLabelWBosons);
   fChain->SetBranchAddress("TruthLabelZBosons", &TruthLabelZBosons, &b_TruthLabelZBosons);
   fChain->SetBranchAddress("TruthParticles", &TruthParticles, &b_TruthParticles);
   fChain->SetBranchAddress("egammaTruthParticles", &egammaTruthParticles, &b_egammaTruthParticles);
   fChain->SetBranchAddress("TruthVertices", &TruthVertices, &b_TruthVertices);
   fChain->SetBranchAddress("BTagging_AntiKt4EMTopoAux.SV0_significance3D", &BTagging_AntiKt4EMTopoAux_SV0_significance3D, &b_BTagging_AntiKt4EMTopoAux_SV0_significance3D);
   fChain->SetBranchAddress("BTagging_AntiKt4EMTopoAux.SV1_pb", &BTagging_AntiKt4EMTopoAux_SV1_pb, &b_BTagging_AntiKt4EMTopoAux_SV1_pb);
   fChain->SetBranchAddress("BTagging_AntiKt4EMTopoAux.SV1_pu", &BTagging_AntiKt4EMTopoAux_SV1_pu, &b_BTagging_AntiKt4EMTopoAux_SV1_pu);
   fChain->SetBranchAddress("BTagging_AntiKt4EMTopoAux.SV1_pc", &BTagging_AntiKt4EMTopoAux_SV1_pc, &b_BTagging_AntiKt4EMTopoAux_SV1_pc);
   fChain->SetBranchAddress("BTagging_AntiKt4EMTopoAux.IP2D_pb", &BTagging_AntiKt4EMTopoAux_IP2D_pb, &b_BTagging_AntiKt4EMTopoAux_IP2D_pb);
   fChain->SetBranchAddress("BTagging_AntiKt4EMTopoAux.IP2D_pu", &BTagging_AntiKt4EMTopoAux_IP2D_pu, &b_BTagging_AntiKt4EMTopoAux_IP2D_pu);
   fChain->SetBranchAddress("BTagging_AntiKt4EMTopoAux.IP2D_pc", &BTagging_AntiKt4EMTopoAux_IP2D_pc, &b_BTagging_AntiKt4EMTopoAux_IP2D_pc);
   fChain->SetBranchAddress("BTagging_AntiKt4EMTopoAux.IP3D_pb", &BTagging_AntiKt4EMTopoAux_IP3D_pb, &b_BTagging_AntiKt4EMTopoAux_IP3D_pb);
   fChain->SetBranchAddress("BTagging_AntiKt4EMTopoAux.IP3D_pu", &BTagging_AntiKt4EMTopoAux_IP3D_pu, &b_BTagging_AntiKt4EMTopoAux_IP3D_pu);
   fChain->SetBranchAddress("BTagging_AntiKt4EMTopoAux.IP3D_pc", &BTagging_AntiKt4EMTopoAux_IP3D_pc, &b_BTagging_AntiKt4EMTopoAux_IP3D_pc);
   fChain->SetBranchAddress("BTagging_AntiKt4EMTopoAux.JetFitter_pb", &BTagging_AntiKt4EMTopoAux_JetFitter_pb, &b_BTagging_AntiKt4EMTopoAux_JetFitter_pb);
   fChain->SetBranchAddress("BTagging_AntiKt4EMTopoAux.JetFitter_pu", &BTagging_AntiKt4EMTopoAux_JetFitter_pu, &b_BTagging_AntiKt4EMTopoAux_JetFitter_pu);
   fChain->SetBranchAddress("BTagging_AntiKt4EMTopoAux.JetFitter_pc", &BTagging_AntiKt4EMTopoAux_JetFitter_pc, &b_BTagging_AntiKt4EMTopoAux_JetFitter_pc);
   fChain->SetBranchAddress("BTagging_AntiKt4EMTopoAux.JetFitterCombNN_pb", &BTagging_AntiKt4EMTopoAux_JetFitterCombNN_pb, &b_BTagging_AntiKt4EMTopoAux_JetFitterCombNN_pb);
   fChain->SetBranchAddress("BTagging_AntiKt4EMTopoAux.JetFitterCombNN_pu", &BTagging_AntiKt4EMTopoAux_JetFitterCombNN_pu, &b_BTagging_AntiKt4EMTopoAux_JetFitterCombNN_pu);
   fChain->SetBranchAddress("BTagging_AntiKt4EMTopoAux.JetFitterCombNN_pc", &BTagging_AntiKt4EMTopoAux_JetFitterCombNN_pc, &b_BTagging_AntiKt4EMTopoAux_JetFitterCombNN_pc);
   fChain->SetBranchAddress("BTagging_AntiKt4EMTopoAux.MV1_discriminant", &BTagging_AntiKt4EMTopoAux_MV1_discriminant, &b_BTagging_AntiKt4EMTopoAux_MV1_discriminant);
   fChain->SetBranchAddress("AntiKt4EMTopoJets", &AntiKt4EMTopoJets, &b_AntiKt4EMTopoJets);
   fChain->SetBranchAddress("AntiKt4LCTopoJets", &AntiKt4LCTopoJets, &b_AntiKt4LCTopoJets);
   fChain->SetBranchAddress("TruthVerticesAux.id", &TruthVerticesAux_id, &b_TruthVerticesAux_id);
   fChain->SetBranchAddress("TruthVerticesAux.barcode", &TruthVerticesAux_barcode, &b_TruthVerticesAux_barcode);
   fChain->SetBranchAddress("TruthVerticesAux.x", &TruthVerticesAux_x, &b_TruthVerticesAux_x);
   fChain->SetBranchAddress("TruthVerticesAux.y", &TruthVerticesAux_y, &b_TruthVerticesAux_y);
   fChain->SetBranchAddress("TruthVerticesAux.z", &TruthVerticesAux_z, &b_TruthVerticesAux_z);
   fChain->SetBranchAddress("TruthVerticesAux.t", &TruthVerticesAux_t, &b_TruthVerticesAux_t);
   fChain->SetBranchAddress("BTagging_AntiKt4EMTopo", &BTagging_AntiKt4EMTopo, &b_BTagging_AntiKt4EMTopo);
   fChain->SetBranchAddress("TruthEventsAux.weights", &TruthEventsAux_weights, &b_TruthEventsAux_weights);
   fChain->SetBranchAddress("TruthEventsAux.crossSection", &TruthEventsAux_crossSection, &b_TruthEventsAux_crossSection);
   fChain->SetBranchAddress("TruthEventsAux.crossSectionError", &TruthEventsAux_crossSectionError, &b_TruthEventsAux_crossSectionError);
   fChain->SetBranchAddress("GSFTrackParticles", &GSFTrackParticles, &b_GSFTrackParticles);
   fChain->SetBranchAddress("InDetTrackParticles", &InDetTrackParticles, &b_InDetTrackParticles);
   fChain->SetBranchAddress("PixelMSOSs", &PixelMSOSs, &b_PixelMSOSs);
   fChain->SetBranchAddress("AntiKt4EMTopoJetsAux.pt", &AntiKt4EMTopoJetsAux_pt, &b_AntiKt4EMTopoJetsAux_pt);
   fChain->SetBranchAddress("AntiKt4EMTopoJetsAux.eta", &AntiKt4EMTopoJetsAux_eta, &b_AntiKt4EMTopoJetsAux_eta);
   fChain->SetBranchAddress("AntiKt4EMTopoJetsAux.phi", &AntiKt4EMTopoJetsAux_phi, &b_AntiKt4EMTopoJetsAux_phi);
   fChain->SetBranchAddress("AntiKt4EMTopoJetsAux.m", &AntiKt4EMTopoJetsAux_m, &b_AntiKt4EMTopoJetsAux_m);
   fChain->SetBranchAddress("AntiKt4EMTopoJetsAux.constituentWeights", &AntiKt4EMTopoJetsAux_constituentWeights, &b_AntiKt4EMTopoJetsAux_constituentWeights);
   fChain->SetBranchAddress("AntiKt4LCTopoJetsAux.pt", &AntiKt4LCTopoJetsAux_pt, &b_AntiKt4LCTopoJetsAux_pt);
   fChain->SetBranchAddress("AntiKt4LCTopoJetsAux.eta", &AntiKt4LCTopoJetsAux_eta, &b_AntiKt4LCTopoJetsAux_eta);
   fChain->SetBranchAddress("AntiKt4LCTopoJetsAux.phi", &AntiKt4LCTopoJetsAux_phi, &b_AntiKt4LCTopoJetsAux_phi);
   fChain->SetBranchAddress("AntiKt4LCTopoJetsAux.m", &AntiKt4LCTopoJetsAux_m, &b_AntiKt4LCTopoJetsAux_m);
   fChain->SetBranchAddress("AntiKt4LCTopoJetsAux.constituentWeights", &AntiKt4LCTopoJetsAux_constituentWeights, &b_AntiKt4LCTopoJetsAux_constituentWeights);
   fChain->SetBranchAddress("TauTracks", &TauTracks, &b_TauTracks);
   fChain->SetBranchAddress("TauJetsAux.pt", &TauJetsAux_pt, &b_TauJetsAux_pt);
   fChain->SetBranchAddress("TauJetsAux.eta", &TauJetsAux_eta, &b_TauJetsAux_eta);
   fChain->SetBranchAddress("TauJetsAux.phi", &TauJetsAux_phi, &b_TauJetsAux_phi);
   fChain->SetBranchAddress("TauJetsAux.m", &TauJetsAux_m, &b_TauJetsAux_m);
   fChain->SetBranchAddress("TauJetsAux.ptJetSeed", &TauJetsAux_ptJetSeed, &b_TauJetsAux_ptJetSeed);
   fChain->SetBranchAddress("TauJetsAux.etaJetSeed", &TauJetsAux_etaJetSeed, &b_TauJetsAux_etaJetSeed);
   fChain->SetBranchAddress("TauJetsAux.phiJetSeed", &TauJetsAux_phiJetSeed, &b_TauJetsAux_phiJetSeed);
   fChain->SetBranchAddress("TauJetsAux.mJetSeed", &TauJetsAux_mJetSeed, &b_TauJetsAux_mJetSeed);
   fChain->SetBranchAddress("TauJetsAux.ptDetectorAxis", &TauJetsAux_ptDetectorAxis, &b_TauJetsAux_ptDetectorAxis);
   fChain->SetBranchAddress("TauJetsAux.etaDetectorAxis", &TauJetsAux_etaDetectorAxis, &b_TauJetsAux_etaDetectorAxis);
   fChain->SetBranchAddress("TauJetsAux.phiDetectorAxis", &TauJetsAux_phiDetectorAxis, &b_TauJetsAux_phiDetectorAxis);
   fChain->SetBranchAddress("TauJetsAux.mDetectorAxis", &TauJetsAux_mDetectorAxis, &b_TauJetsAux_mDetectorAxis);
   fChain->SetBranchAddress("TauJetsAux.ptIntermediateAxis", &TauJetsAux_ptIntermediateAxis, &b_TauJetsAux_ptIntermediateAxis);
   fChain->SetBranchAddress("TauJetsAux.etaIntermediateAxis", &TauJetsAux_etaIntermediateAxis, &b_TauJetsAux_etaIntermediateAxis);
   fChain->SetBranchAddress("TauJetsAux.phiIntermediateAxis", &TauJetsAux_phiIntermediateAxis, &b_TauJetsAux_phiIntermediateAxis);
   fChain->SetBranchAddress("TauJetsAux.mIntermediateAxis", &TauJetsAux_mIntermediateAxis, &b_TauJetsAux_mIntermediateAxis);
   fChain->SetBranchAddress("TauJetsAux.ptTauEnergyScale", &TauJetsAux_ptTauEnergyScale, &b_TauJetsAux_ptTauEnergyScale);
   fChain->SetBranchAddress("TauJetsAux.etaTauEnergyScale", &TauJetsAux_etaTauEnergyScale, &b_TauJetsAux_etaTauEnergyScale);
   fChain->SetBranchAddress("TauJetsAux.phiTauEnergyScale", &TauJetsAux_phiTauEnergyScale, &b_TauJetsAux_phiTauEnergyScale);
   fChain->SetBranchAddress("TauJetsAux.mTauEnergyScale", &TauJetsAux_mTauEnergyScale, &b_TauJetsAux_mTauEnergyScale);
   fChain->SetBranchAddress("TauJetsAux.ptTauEtaCalib", &TauJetsAux_ptTauEtaCalib, &b_TauJetsAux_ptTauEtaCalib);
   fChain->SetBranchAddress("TauJetsAux.etaTauEtaCalib", &TauJetsAux_etaTauEtaCalib, &b_TauJetsAux_etaTauEtaCalib);
   fChain->SetBranchAddress("TauJetsAux.phiTauEtaCalib", &TauJetsAux_phiTauEtaCalib, &b_TauJetsAux_phiTauEtaCalib);
   fChain->SetBranchAddress("TauJetsAux.mTauEtaCalib", &TauJetsAux_mTauEtaCalib, &b_TauJetsAux_mTauEtaCalib);
   fChain->SetBranchAddress("TauJetsAux.ptPanTauCellBasedProto", &TauJetsAux_ptPanTauCellBasedProto, &b_TauJetsAux_ptPanTauCellBasedProto);
   fChain->SetBranchAddress("TauJetsAux.etaPanTauCellBasedProto", &TauJetsAux_etaPanTauCellBasedProto, &b_TauJetsAux_etaPanTauCellBasedProto);
   fChain->SetBranchAddress("TauJetsAux.phiPanTauCellBasedProto", &TauJetsAux_phiPanTauCellBasedProto, &b_TauJetsAux_phiPanTauCellBasedProto);
   fChain->SetBranchAddress("TauJetsAux.mPanTauCellBasedProto", &TauJetsAux_mPanTauCellBasedProto, &b_TauJetsAux_mPanTauCellBasedProto);
   fChain->SetBranchAddress("TauJetsAux.ptPanTauCellBased", &TauJetsAux_ptPanTauCellBased, &b_TauJetsAux_ptPanTauCellBased);
   fChain->SetBranchAddress("TauJetsAux.etaPanTauCellBased", &TauJetsAux_etaPanTauCellBased, &b_TauJetsAux_etaPanTauCellBased);
   fChain->SetBranchAddress("TauJetsAux.phiPanTauCellBased", &TauJetsAux_phiPanTauCellBased, &b_TauJetsAux_phiPanTauCellBased);
   fChain->SetBranchAddress("TauJetsAux.mPanTauCellBased", &TauJetsAux_mPanTauCellBased, &b_TauJetsAux_mPanTauCellBased);
   fChain->SetBranchAddress("TauJetsAux.ptTrigCaloOnly", &TauJetsAux_ptTrigCaloOnly, &b_TauJetsAux_ptTrigCaloOnly);
   fChain->SetBranchAddress("TauJetsAux.etaTrigCaloOnly", &TauJetsAux_etaTrigCaloOnly, &b_TauJetsAux_etaTrigCaloOnly);
   fChain->SetBranchAddress("TauJetsAux.phiTrigCaloOnly", &TauJetsAux_phiTrigCaloOnly, &b_TauJetsAux_phiTrigCaloOnly);
   fChain->SetBranchAddress("TauJetsAux.mTrigCaloOnly", &TauJetsAux_mTrigCaloOnly, &b_TauJetsAux_mTrigCaloOnly);
   fChain->SetBranchAddress("TauJetsAux.ptFinalCalib", &TauJetsAux_ptFinalCalib, &b_TauJetsAux_ptFinalCalib);
   fChain->SetBranchAddress("TauJetsAux.etaFinalCalib", &TauJetsAux_etaFinalCalib, &b_TauJetsAux_etaFinalCalib);
   fChain->SetBranchAddress("TauJetsAux.phiFinalCalib", &TauJetsAux_phiFinalCalib, &b_TauJetsAux_phiFinalCalib);
   fChain->SetBranchAddress("TauJetsAux.mFinalCalib", &TauJetsAux_mFinalCalib, &b_TauJetsAux_mFinalCalib);
   fChain->SetBranchAddress("TauJetsAux.ROIWord", &TauJetsAux_ROIWord, &b_TauJetsAux_ROIWord);
   fChain->SetBranchAddress("TauJetsAux.charge", &TauJetsAux_charge, &b_TauJetsAux_charge);
   fChain->SetBranchAddress("TauJetsAux.isTauFlags", &TauJetsAux_isTauFlags, &b_TauJetsAux_isTauFlags);
   fChain->SetBranchAddress("TauJetsAux.BDTJetScore", &TauJetsAux_BDTJetScore, &b_TauJetsAux_BDTJetScore);
   fChain->SetBranchAddress("TauJetsAux.BDTEleScore", &TauJetsAux_BDTEleScore, &b_TauJetsAux_BDTEleScore);
   fChain->SetBranchAddress("TauJetsAux.EleMatchLikelihoodScore", &TauJetsAux_EleMatchLikelihoodScore, &b_TauJetsAux_EleMatchLikelihoodScore);
   fChain->SetBranchAddress("TauJetsAux.BDTJetScoreSigTrans", &TauJetsAux_BDTJetScoreSigTrans, &b_TauJetsAux_BDTJetScoreSigTrans);
   fChain->SetBranchAddress("TauJetsAux.nChargedTracks", &TauJetsAux_nChargedTracks, &b_TauJetsAux_nChargedTracks);
   fChain->SetBranchAddress("TauJetsAux.nIsolatedTracks", &TauJetsAux_nIsolatedTracks, &b_TauJetsAux_nIsolatedTracks);
   fChain->SetBranchAddress("TauJetsAux.trackFilterProngs", &TauJetsAux_trackFilterProngs, &b_TauJetsAux_trackFilterProngs);
   fChain->SetBranchAddress("TauJetsAux.trackFilterQuality", &TauJetsAux_trackFilterQuality, &b_TauJetsAux_trackFilterQuality);
   fChain->SetBranchAddress("TauJetsAux.pi0ConeDR", &TauJetsAux_pi0ConeDR, &b_TauJetsAux_pi0ConeDR);
   fChain->SetBranchAddress("TauJetsAux.ipZ0SinThetaSigLeadTrk", &TauJetsAux_ipZ0SinThetaSigLeadTrk, &b_TauJetsAux_ipZ0SinThetaSigLeadTrk);
   fChain->SetBranchAddress("TauJetsAux.etOverPtLeadTrk", &TauJetsAux_etOverPtLeadTrk, &b_TauJetsAux_etOverPtLeadTrk);
   fChain->SetBranchAddress("TauJetsAux.leadTrkPt", &TauJetsAux_leadTrkPt, &b_TauJetsAux_leadTrkPt);
   fChain->SetBranchAddress("TauJetsAux.ipSigLeadTrk", &TauJetsAux_ipSigLeadTrk, &b_TauJetsAux_ipSigLeadTrk);
   fChain->SetBranchAddress("TauJetsAux.massTrkSys", &TauJetsAux_massTrkSys, &b_TauJetsAux_massTrkSys);
   fChain->SetBranchAddress("TauJetsAux.trkWidth2", &TauJetsAux_trkWidth2, &b_TauJetsAux_trkWidth2);
   fChain->SetBranchAddress("TauJetsAux.trFlightPathSig", &TauJetsAux_trFlightPathSig, &b_TauJetsAux_trFlightPathSig);
   fChain->SetBranchAddress("TauJetsAux.numCells", &TauJetsAux_numCells, &b_TauJetsAux_numCells);
   fChain->SetBranchAddress("TauJetsAux.numTopoClusters", &TauJetsAux_numTopoClusters, &b_TauJetsAux_numTopoClusters);
   fChain->SetBranchAddress("TauJetsAux.numEffTopoClusters", &TauJetsAux_numEffTopoClusters, &b_TauJetsAux_numEffTopoClusters);
   fChain->SetBranchAddress("TauJetsAux.topoInvMass", &TauJetsAux_topoInvMass, &b_TauJetsAux_topoInvMass);
   fChain->SetBranchAddress("TauJetsAux.effTopoInvMass", &TauJetsAux_effTopoInvMass, &b_TauJetsAux_effTopoInvMass);
   fChain->SetBranchAddress("TauJetsAux.topoMeanDeltaR", &TauJetsAux_topoMeanDeltaR, &b_TauJetsAux_topoMeanDeltaR);
   fChain->SetBranchAddress("TauJetsAux.effTopoMeanDeltaR", &TauJetsAux_effTopoMeanDeltaR, &b_TauJetsAux_effTopoMeanDeltaR);
   fChain->SetBranchAddress("TauJetsAux.EMRadius", &TauJetsAux_EMRadius, &b_TauJetsAux_EMRadius);
   fChain->SetBranchAddress("TauJetsAux.hadRadius", &TauJetsAux_hadRadius, &b_TauJetsAux_hadRadius);
   fChain->SetBranchAddress("TauJetsAux.etEMAtEMScale", &TauJetsAux_etEMAtEMScale, &b_TauJetsAux_etEMAtEMScale);
   fChain->SetBranchAddress("TauJetsAux.etHadAtEMScale", &TauJetsAux_etHadAtEMScale, &b_TauJetsAux_etHadAtEMScale);
   fChain->SetBranchAddress("TauJetsAux.isolFrac", &TauJetsAux_isolFrac, &b_TauJetsAux_isolFrac);
   fChain->SetBranchAddress("TauJetsAux.centFrac", &TauJetsAux_centFrac, &b_TauJetsAux_centFrac);
   fChain->SetBranchAddress("TauJetsAux.stripWidth2", &TauJetsAux_stripWidth2, &b_TauJetsAux_stripWidth2);
   fChain->SetBranchAddress("TauJetsAux.nStrip", &TauJetsAux_nStrip, &b_TauJetsAux_nStrip);
   fChain->SetBranchAddress("TauJetsAux.trkAvgDist", &TauJetsAux_trkAvgDist, &b_TauJetsAux_trkAvgDist);
   fChain->SetBranchAddress("TauJetsAux.trkRmsDist", &TauJetsAux_trkRmsDist, &b_TauJetsAux_trkRmsDist);
   fChain->SetBranchAddress("TauJetsAux.lead2ClusterEOverAllClusterE", &TauJetsAux_lead2ClusterEOverAllClusterE, &b_TauJetsAux_lead2ClusterEOverAllClusterE);
   fChain->SetBranchAddress("TauJetsAux.lead3ClusterEOverAllClusterE", &TauJetsAux_lead3ClusterEOverAllClusterE, &b_TauJetsAux_lead3ClusterEOverAllClusterE);
   fChain->SetBranchAddress("TauJetsAux.caloIso", &TauJetsAux_caloIso, &b_TauJetsAux_caloIso);
   fChain->SetBranchAddress("TauJetsAux.caloIsoCorrected", &TauJetsAux_caloIsoCorrected, &b_TauJetsAux_caloIsoCorrected);
   fChain->SetBranchAddress("TauJetsAux.dRmax", &TauJetsAux_dRmax, &b_TauJetsAux_dRmax);
   fChain->SetBranchAddress("TauJetsAux.secMaxStripEt", &TauJetsAux_secMaxStripEt, &b_TauJetsAux_secMaxStripEt);
   fChain->SetBranchAddress("TauJetsAux.sumEMCellEtOverLeadTrkPt", &TauJetsAux_sumEMCellEtOverLeadTrkPt, &b_TauJetsAux_sumEMCellEtOverLeadTrkPt);
   fChain->SetBranchAddress("TauJetsAux.hadLeakEt", &TauJetsAux_hadLeakEt, &b_TauJetsAux_hadLeakEt);
   fChain->SetBranchAddress("TauJetsAux.TESOffset", &TauJetsAux_TESOffset, &b_TauJetsAux_TESOffset);
   fChain->SetBranchAddress("TauJetsAux.TESCalibConstant", &TauJetsAux_TESCalibConstant, &b_TauJetsAux_TESCalibConstant);
   fChain->SetBranchAddress("TauJetsAux.cellBasedEnergyRing1", &TauJetsAux_cellBasedEnergyRing1, &b_TauJetsAux_cellBasedEnergyRing1);
   fChain->SetBranchAddress("TauJetsAux.cellBasedEnergyRing2", &TauJetsAux_cellBasedEnergyRing2, &b_TauJetsAux_cellBasedEnergyRing2);
   fChain->SetBranchAddress("TauJetsAux.cellBasedEnergyRing3", &TauJetsAux_cellBasedEnergyRing3, &b_TauJetsAux_cellBasedEnergyRing3);
   fChain->SetBranchAddress("TauJetsAux.cellBasedEnergyRing4", &TauJetsAux_cellBasedEnergyRing4, &b_TauJetsAux_cellBasedEnergyRing4);
   fChain->SetBranchAddress("TauJetsAux.cellBasedEnergyRing5", &TauJetsAux_cellBasedEnergyRing5, &b_TauJetsAux_cellBasedEnergyRing5);
   fChain->SetBranchAddress("TauJetsAux.cellBasedEnergyRing6", &TauJetsAux_cellBasedEnergyRing6, &b_TauJetsAux_cellBasedEnergyRing6);
   fChain->SetBranchAddress("TauJetsAux.cellBasedEnergyRing7", &TauJetsAux_cellBasedEnergyRing7, &b_TauJetsAux_cellBasedEnergyRing7);
   fChain->SetBranchAddress("TauJetsAux.TRT_NHT_OVER_NLT", &TauJetsAux_TRT_NHT_OVER_NLT, &b_TauJetsAux_TRT_NHT_OVER_NLT);
   fChain->SetBranchAddress("TauJetsAux.TauJetVtxFraction", &TauJetsAux_TauJetVtxFraction, &b_TauJetsAux_TauJetVtxFraction);
   fChain->SetBranchAddress("TauJetsAux.nCharged", &TauJetsAux_nCharged, &b_TauJetsAux_nCharged);
   fChain->SetBranchAddress("TauJetsAux.mEflowApprox", &TauJetsAux_mEflowApprox, &b_TauJetsAux_mEflowApprox);
   fChain->SetBranchAddress("TauJetsAux.ptRatioEflowApprox", &TauJetsAux_ptRatioEflowApprox, &b_TauJetsAux_ptRatioEflowApprox);
   fChain->SetBranchAddress("TauJetsAux.innerTrkAvgDist", &TauJetsAux_innerTrkAvgDist, &b_TauJetsAux_innerTrkAvgDist);
   fChain->SetBranchAddress("TauJetsAux.SumPtTrkFrac", &TauJetsAux_SumPtTrkFrac, &b_TauJetsAux_SumPtTrkFrac);
   fChain->SetBranchAddress("TauJetsAux.etOverPtLeadTrkCorrected", &TauJetsAux_etOverPtLeadTrkCorrected, &b_TauJetsAux_etOverPtLeadTrkCorrected);
   fChain->SetBranchAddress("TauJetsAux.ipSigLeadTrkCorrected", &TauJetsAux_ipSigLeadTrkCorrected, &b_TauJetsAux_ipSigLeadTrkCorrected);
   fChain->SetBranchAddress("TauJetsAux.trFlightPathSigCorrected", &TauJetsAux_trFlightPathSigCorrected, &b_TauJetsAux_trFlightPathSigCorrected);
   fChain->SetBranchAddress("TauJetsAux.massTrkSysCorrected", &TauJetsAux_massTrkSysCorrected, &b_TauJetsAux_massTrkSysCorrected);
   fChain->SetBranchAddress("TauJetsAux.dRmaxCorrected", &TauJetsAux_dRmaxCorrected, &b_TauJetsAux_dRmaxCorrected);
   fChain->SetBranchAddress("TauJetsAux.ChPiEMEOverCaloEMECorrected", &TauJetsAux_ChPiEMEOverCaloEMECorrected, &b_TauJetsAux_ChPiEMEOverCaloEMECorrected);
   fChain->SetBranchAddress("TauJetsAux.EMPOverTrkSysPCorrected", &TauJetsAux_EMPOverTrkSysPCorrected, &b_TauJetsAux_EMPOverTrkSysPCorrected);
   fChain->SetBranchAddress("TauJetsAux.ptRatioEflowApproxCorrected", &TauJetsAux_ptRatioEflowApproxCorrected, &b_TauJetsAux_ptRatioEflowApproxCorrected);
   fChain->SetBranchAddress("TauJetsAux.mEflowApproxCorrected", &TauJetsAux_mEflowApproxCorrected, &b_TauJetsAux_mEflowApproxCorrected);
   fChain->SetBranchAddress("TauJetsAux.centFracCorrected", &TauJetsAux_centFracCorrected, &b_TauJetsAux_centFracCorrected);
   fChain->SetBranchAddress("TauJetsAux.innerTrkAvgDistCorrected", &TauJetsAux_innerTrkAvgDistCorrected, &b_TauJetsAux_innerTrkAvgDistCorrected);
   fChain->SetBranchAddress("TauJetsAux.SumPtTrkFracCorrected", &TauJetsAux_SumPtTrkFracCorrected, &b_TauJetsAux_SumPtTrkFracCorrected);
   fChain->SetBranchAddress("TauJetsAux.PSSFraction", &TauJetsAux_PSSFraction, &b_TauJetsAux_PSSFraction);
   fChain->SetBranchAddress("TauJetsAux.ChPiEMEOverCaloEME", &TauJetsAux_ChPiEMEOverCaloEME, &b_TauJetsAux_ChPiEMEOverCaloEME);
   fChain->SetBranchAddress("TauJetsAux.EMPOverTrkSysP", &TauJetsAux_EMPOverTrkSysP, &b_TauJetsAux_EMPOverTrkSysP);
   fChain->SetBranchAddress("TauJetsAux.PanTau_isPanTauCandidate", &TauJetsAux_PanTau_isPanTauCandidate, &b_TauJetsAux_PanTau_isPanTauCandidate);
   fChain->SetBranchAddress("TauJetsAux.PanTau_DecayModeProto", &TauJetsAux_PanTau_DecayModeProto, &b_TauJetsAux_PanTau_DecayModeProto);
   fChain->SetBranchAddress("TauJetsAux.PanTau_DecayMode", &TauJetsAux_PanTau_DecayMode, &b_TauJetsAux_PanTau_DecayMode);
   fChain->SetBranchAddress("TauJetsAux.PanTau_BDTValue_1p0n_vs_1p1n", &TauJetsAux_PanTau_BDTValue_1p0n_vs_1p1n, &b_TauJetsAux_PanTau_BDTValue_1p0n_vs_1p1n);
   fChain->SetBranchAddress("TauJetsAux.PanTau_BDTValue_1p1n_vs_1pXn", &TauJetsAux_PanTau_BDTValue_1p1n_vs_1pXn, &b_TauJetsAux_PanTau_BDTValue_1p1n_vs_1pXn);
   fChain->SetBranchAddress("TauJetsAux.PanTau_BDTValue_3p0n_vs_3pXn", &TauJetsAux_PanTau_BDTValue_3p0n_vs_3pXn, &b_TauJetsAux_PanTau_BDTValue_3p0n_vs_3pXn);
   fChain->SetBranchAddress("TauJetsAux.PanTau_BDTVar_Basic_NNeutralConsts", &TauJetsAux_PanTau_BDTVar_Basic_NNeutralConsts, &b_TauJetsAux_PanTau_BDTVar_Basic_NNeutralConsts);
   fChain->SetBranchAddress("TauJetsAux.PanTau_BDTVar_Charged_JetMoment_EtDRxTotalEt", &TauJetsAux_PanTau_BDTVar_Charged_JetMoment_EtDRxTotalEt, &b_TauJetsAux_PanTau_BDTVar_Charged_JetMoment_EtDRxTotalEt);
   fChain->SetBranchAddress("TauJetsAux.PanTau_BDTVar_Charged_StdDev_Et_WrtEtAllConsts", &TauJetsAux_PanTau_BDTVar_Charged_StdDev_Et_WrtEtAllConsts, &b_TauJetsAux_PanTau_BDTVar_Charged_StdDev_Et_WrtEtAllConsts);
   fChain->SetBranchAddress("TauJetsAux.PanTau_BDTVar_Neutral_HLV_SumM", &TauJetsAux_PanTau_BDTVar_Neutral_HLV_SumM, &b_TauJetsAux_PanTau_BDTVar_Neutral_HLV_SumM);
   fChain->SetBranchAddress("TauJetsAux.PanTau_BDTVar_Neutral_PID_BDTValues_BDTSort_1", &TauJetsAux_PanTau_BDTVar_Neutral_PID_BDTValues_BDTSort_1, &b_TauJetsAux_PanTau_BDTVar_Neutral_PID_BDTValues_BDTSort_1);
   fChain->SetBranchAddress("TauJetsAux.PanTau_BDTVar_Neutral_PID_BDTValues_BDTSort_2", &TauJetsAux_PanTau_BDTVar_Neutral_PID_BDTValues_BDTSort_2, &b_TauJetsAux_PanTau_BDTVar_Neutral_PID_BDTValues_BDTSort_2);
   fChain->SetBranchAddress("TauJetsAux.PanTau_BDTVar_Neutral_Ratio_1stBDTEtOverEtAllConsts", &TauJetsAux_PanTau_BDTVar_Neutral_Ratio_1stBDTEtOverEtAllConsts, &b_TauJetsAux_PanTau_BDTVar_Neutral_Ratio_1stBDTEtOverEtAllConsts);
   fChain->SetBranchAddress("TauJetsAux.PanTau_BDTVar_Neutral_Ratio_EtOverEtAllConsts", &TauJetsAux_PanTau_BDTVar_Neutral_Ratio_EtOverEtAllConsts, &b_TauJetsAux_PanTau_BDTVar_Neutral_Ratio_EtOverEtAllConsts);
   fChain->SetBranchAddress("TauJetsAux.PanTau_BDTVar_Neutral_Shots_NPhotonsInSeed", &TauJetsAux_PanTau_BDTVar_Neutral_Shots_NPhotonsInSeed, &b_TauJetsAux_PanTau_BDTVar_Neutral_Shots_NPhotonsInSeed);
   fChain->SetBranchAddress("TauJetsAux.PanTau_BDTVar_Combined_DeltaR1stNeutralTo1stCharged", &TauJetsAux_PanTau_BDTVar_Combined_DeltaR1stNeutralTo1stCharged, &b_TauJetsAux_PanTau_BDTVar_Combined_DeltaR1stNeutralTo1stCharged);
   fChain->SetBranchAddress("TauJetsAux.PanTau_BDTVar_Charged_HLV_SumM", &TauJetsAux_PanTau_BDTVar_Charged_HLV_SumM, &b_TauJetsAux_PanTau_BDTVar_Charged_HLV_SumM);
   fChain->SetBranchAddress("TauJetsAux.RNNJetScore", &TauJetsAux_RNNJetScore, &b_TauJetsAux_RNNJetScore);
   fChain->SetBranchAddress("TauJetsAux.RNNJetScoreSigTrans", &TauJetsAux_RNNJetScoreSigTrans, &b_TauJetsAux_RNNJetScoreSigTrans);
   fChain->SetBranchAddress("PixelClustersAux.identifier", &PixelClustersAux_identifier, &b_PixelClustersAux_identifier);
   fChain->SetBranchAddress("PixelClustersAux.localX", &PixelClustersAux_localX, &b_PixelClustersAux_localX);
   fChain->SetBranchAddress("PixelClustersAux.localY", &PixelClustersAux_localY, &b_PixelClustersAux_localY);
   fChain->SetBranchAddress("PixelClustersAux.localXError", &PixelClustersAux_localXError, &b_PixelClustersAux_localXError);
   fChain->SetBranchAddress("PixelClustersAux.localYError", &PixelClustersAux_localYError, &b_PixelClustersAux_localYError);
   fChain->SetBranchAddress("PixelClustersAux.localXYCorrelation", &PixelClustersAux_localXYCorrelation, &b_PixelClustersAux_localXYCorrelation);
   fChain->SetBranchAddress("PixelClustersAux.globalX", &PixelClustersAux_globalX, &b_PixelClustersAux_globalX);
   fChain->SetBranchAddress("PixelClustersAux.globalY", &PixelClustersAux_globalY, &b_PixelClustersAux_globalY);
   fChain->SetBranchAddress("PixelClustersAux.globalZ", &PixelClustersAux_globalZ, &b_PixelClustersAux_globalZ);
   fChain->SetBranchAddress("PrimaryVerticesAux.chiSquared", &PrimaryVerticesAux_chiSquared, &b_PrimaryVerticesAux_chiSquared);
   fChain->SetBranchAddress("PrimaryVerticesAux.numberDoF", &PrimaryVerticesAux_numberDoF, &b_PrimaryVerticesAux_numberDoF);
   fChain->SetBranchAddress("PrimaryVerticesAux.x", &PrimaryVerticesAux_x, &b_PrimaryVerticesAux_x);
   fChain->SetBranchAddress("PrimaryVerticesAux.y", &PrimaryVerticesAux_y, &b_PrimaryVerticesAux_y);
   fChain->SetBranchAddress("PrimaryVerticesAux.z", &PrimaryVerticesAux_z, &b_PrimaryVerticesAux_z);
   fChain->SetBranchAddress("PrimaryVerticesAux.covariance", &PrimaryVerticesAux_covariance, &b_PrimaryVerticesAux_covariance);
   fChain->SetBranchAddress("PrimaryVerticesAux.vertexType", &PrimaryVerticesAux_vertexType, &b_PrimaryVerticesAux_vertexType);
   fChain->SetBranchAddress("PrimaryVerticesAux.trackWeights", &PrimaryVerticesAux_trackWeights, &b_PrimaryVerticesAux_trackWeights);
   fChain->SetBranchAddress("PrimaryVerticesAux.neutralWeights", &PrimaryVerticesAux_neutralWeights, &b_PrimaryVerticesAux_neutralWeights);
   fChain->SetBranchAddress("AntiKt4EMTopoJetsAuxDyn.LeadingClusterCenterLambda", &AntiKt4EMTopoJetsAuxDyn_LeadingClusterCenterLambda, &b_AntiKt4EMTopoJetsAuxDyn_LeadingClusterCenterLambda);
   fChain->SetBranchAddress("AntiKt4EMTopoJetsAuxDyn.LeadingClusterSecondR", &AntiKt4EMTopoJetsAuxDyn_LeadingClusterSecondR, &b_AntiKt4EMTopoJetsAuxDyn_LeadingClusterSecondR);
   fChain->SetBranchAddress("AntiKt4EMTopoJetsAuxDyn.GhostTruth", &AntiKt4EMTopoJetsAuxDyn_GhostTruth, &b_AntiKt4EMTopoJetsAuxDyn_GhostTruth);
   fChain->SetBranchAddress("AntiKt4EMTopoJetsAuxDyn.NumTrkPt500", &AntiKt4EMTopoJetsAuxDyn_NumTrkPt500, &b_AntiKt4EMTopoJetsAuxDyn_NumTrkPt500);
   fChain->SetBranchAddress("AntiKt4EMTopoJetsAuxDyn.SumPtTrkPt500", &AntiKt4EMTopoJetsAuxDyn_SumPtTrkPt500, &b_AntiKt4EMTopoJetsAuxDyn_SumPtTrkPt500);
   fChain->SetBranchAddress("AntiKt4EMTopoJetsAuxDyn.EnergyPerSampling", &AntiKt4EMTopoJetsAuxDyn_EnergyPerSampling, &b_AntiKt4EMTopoJetsAuxDyn_EnergyPerSampling);
   fChain->SetBranchAddress("AntiKt4EMTopoJetsAuxDyn.EMFrac", &AntiKt4EMTopoJetsAuxDyn_EMFrac, &b_AntiKt4EMTopoJetsAuxDyn_EMFrac);
   fChain->SetBranchAddress("AntiKt4EMTopoJetsAuxDyn.Width", &AntiKt4EMTopoJetsAuxDyn_Width, &b_AntiKt4EMTopoJetsAuxDyn_Width);
   fChain->SetBranchAddress("AntiKt4EMTopoJetsAuxDyn.ActiveArea", &AntiKt4EMTopoJetsAuxDyn_ActiveArea, &b_AntiKt4EMTopoJetsAuxDyn_ActiveArea);
   fChain->SetBranchAddress("AntiKt4EMTopoJetsAuxDyn.ActiveArea4vec_pt", &AntiKt4EMTopoJetsAuxDyn_ActiveArea4vec_pt, &b_AntiKt4EMTopoJetsAuxDyn_ActiveArea4vec_pt);
   fChain->SetBranchAddress("AntiKt4EMTopoJetsAuxDyn.ActiveArea4vec_eta", &AntiKt4EMTopoJetsAuxDyn_ActiveArea4vec_eta, &b_AntiKt4EMTopoJetsAuxDyn_ActiveArea4vec_eta);
   fChain->SetBranchAddress("AntiKt4EMTopoJetsAuxDyn.ActiveArea4vec_phi", &AntiKt4EMTopoJetsAuxDyn_ActiveArea4vec_phi, &b_AntiKt4EMTopoJetsAuxDyn_ActiveArea4vec_phi);
   fChain->SetBranchAddress("AntiKt4EMTopoJetsAuxDyn.ActiveArea4vec_m", &AntiKt4EMTopoJetsAuxDyn_ActiveArea4vec_m, &b_AntiKt4EMTopoJetsAuxDyn_ActiveArea4vec_m);
   fChain->SetBranchAddress("AntiKt4EMTopoJetsAuxDyn.GhostTrack", &AntiKt4EMTopoJetsAuxDyn_GhostTrack, &b_AntiKt4EMTopoJetsAuxDyn_GhostTrack);
   fChain->SetBranchAddress("AntiKt4EMTopoJetsAuxDyn.GhostTrackCount", &AntiKt4EMTopoJetsAuxDyn_GhostTrackCount, &b_AntiKt4EMTopoJetsAuxDyn_GhostTrackCount);
   fChain->SetBranchAddress("AntiKt4EMTopoJetsAuxDyn.GhostTrackPt", &AntiKt4EMTopoJetsAuxDyn_GhostTrackPt, &b_AntiKt4EMTopoJetsAuxDyn_GhostTrackPt);
   fChain->SetBranchAddress("AntiKt4EMTopoJetsAuxDyn.GhostMuonSegment", &AntiKt4EMTopoJetsAuxDyn_GhostMuonSegment, &b_AntiKt4EMTopoJetsAuxDyn_GhostMuonSegment);
   fChain->SetBranchAddress("AntiKt4EMTopoJetsAuxDyn.GhostMuonSegmentCount", &AntiKt4EMTopoJetsAuxDyn_GhostMuonSegmentCount, &b_AntiKt4EMTopoJetsAuxDyn_GhostMuonSegmentCount);
   fChain->SetBranchAddress("AntiKt4EMTopoJetsAuxDyn.GhostTruthCount", &AntiKt4EMTopoJetsAuxDyn_GhostTruthCount, &b_AntiKt4EMTopoJetsAuxDyn_GhostTruthCount);
   fChain->SetBranchAddress("AntiKt4EMTopoJetsAuxDyn.GhostTruthPt", &AntiKt4EMTopoJetsAuxDyn_GhostTruthPt, &b_AntiKt4EMTopoJetsAuxDyn_GhostTruthPt);
   fChain->SetBranchAddress("AntiKt4EMTopoJetsAuxDyn.GhostBHadronsInitial", &AntiKt4EMTopoJetsAuxDyn_GhostBHadronsInitial, &b_AntiKt4EMTopoJetsAuxDyn_GhostBHadronsInitial);
   fChain->SetBranchAddress("AntiKt4EMTopoJetsAuxDyn.GhostBHadronsInitialCount", &AntiKt4EMTopoJetsAuxDyn_GhostBHadronsInitialCount, &b_AntiKt4EMTopoJetsAuxDyn_GhostBHadronsInitialCount);
   fChain->SetBranchAddress("AntiKt4EMTopoJetsAuxDyn.GhostBHadronsInitialPt", &AntiKt4EMTopoJetsAuxDyn_GhostBHadronsInitialPt, &b_AntiKt4EMTopoJetsAuxDyn_GhostBHadronsInitialPt);
   fChain->SetBranchAddress("AntiKt4EMTopoJetsAuxDyn.GhostBHadronsFinal", &AntiKt4EMTopoJetsAuxDyn_GhostBHadronsFinal, &b_AntiKt4EMTopoJetsAuxDyn_GhostBHadronsFinal);
   fChain->SetBranchAddress("AntiKt4EMTopoJetsAuxDyn.GhostBHadronsFinalCount", &AntiKt4EMTopoJetsAuxDyn_GhostBHadronsFinalCount, &b_AntiKt4EMTopoJetsAuxDyn_GhostBHadronsFinalCount);
   fChain->SetBranchAddress("AntiKt4EMTopoJetsAuxDyn.GhostBHadronsFinalPt", &AntiKt4EMTopoJetsAuxDyn_GhostBHadronsFinalPt, &b_AntiKt4EMTopoJetsAuxDyn_GhostBHadronsFinalPt);
   fChain->SetBranchAddress("AntiKt4EMTopoJetsAuxDyn.GhostBQuarksFinal", &AntiKt4EMTopoJetsAuxDyn_GhostBQuarksFinal, &b_AntiKt4EMTopoJetsAuxDyn_GhostBQuarksFinal);
   fChain->SetBranchAddress("AntiKt4EMTopoJetsAuxDyn.GhostBQuarksFinalCount", &AntiKt4EMTopoJetsAuxDyn_GhostBQuarksFinalCount, &b_AntiKt4EMTopoJetsAuxDyn_GhostBQuarksFinalCount);
   fChain->SetBranchAddress("AntiKt4EMTopoJetsAuxDyn.GhostBQuarksFinalPt", &AntiKt4EMTopoJetsAuxDyn_GhostBQuarksFinalPt, &b_AntiKt4EMTopoJetsAuxDyn_GhostBQuarksFinalPt);
   fChain->SetBranchAddress("AntiKt4EMTopoJetsAuxDyn.GhostCHadronsInitial", &AntiKt4EMTopoJetsAuxDyn_GhostCHadronsInitial, &b_AntiKt4EMTopoJetsAuxDyn_GhostCHadronsInitial);
   fChain->SetBranchAddress("AntiKt4EMTopoJetsAuxDyn.GhostCHadronsInitialCount", &AntiKt4EMTopoJetsAuxDyn_GhostCHadronsInitialCount, &b_AntiKt4EMTopoJetsAuxDyn_GhostCHadronsInitialCount);
   fChain->SetBranchAddress("AntiKt4EMTopoJetsAuxDyn.GhostCHadronsInitialPt", &AntiKt4EMTopoJetsAuxDyn_GhostCHadronsInitialPt, &b_AntiKt4EMTopoJetsAuxDyn_GhostCHadronsInitialPt);
   fChain->SetBranchAddress("AntiKt4EMTopoJetsAuxDyn.GhostCHadronsFinal", &AntiKt4EMTopoJetsAuxDyn_GhostCHadronsFinal, &b_AntiKt4EMTopoJetsAuxDyn_GhostCHadronsFinal);
   fChain->SetBranchAddress("AntiKt4EMTopoJetsAuxDyn.GhostCHadronsFinalCount", &AntiKt4EMTopoJetsAuxDyn_GhostCHadronsFinalCount, &b_AntiKt4EMTopoJetsAuxDyn_GhostCHadronsFinalCount);
   fChain->SetBranchAddress("AntiKt4EMTopoJetsAuxDyn.GhostCHadronsFinalPt", &AntiKt4EMTopoJetsAuxDyn_GhostCHadronsFinalPt, &b_AntiKt4EMTopoJetsAuxDyn_GhostCHadronsFinalPt);
   fChain->SetBranchAddress("AntiKt4EMTopoJetsAuxDyn.GhostCQuarksFinal", &AntiKt4EMTopoJetsAuxDyn_GhostCQuarksFinal, &b_AntiKt4EMTopoJetsAuxDyn_GhostCQuarksFinal);
   fChain->SetBranchAddress("AntiKt4EMTopoJetsAuxDyn.GhostCQuarksFinalCount", &AntiKt4EMTopoJetsAuxDyn_GhostCQuarksFinalCount, &b_AntiKt4EMTopoJetsAuxDyn_GhostCQuarksFinalCount);
   fChain->SetBranchAddress("AntiKt4EMTopoJetsAuxDyn.GhostCQuarksFinalPt", &AntiKt4EMTopoJetsAuxDyn_GhostCQuarksFinalPt, &b_AntiKt4EMTopoJetsAuxDyn_GhostCQuarksFinalPt);
   fChain->SetBranchAddress("AntiKt4EMTopoJetsAuxDyn.GhostTausFinal", &AntiKt4EMTopoJetsAuxDyn_GhostTausFinal, &b_AntiKt4EMTopoJetsAuxDyn_GhostTausFinal);
   fChain->SetBranchAddress("AntiKt4EMTopoJetsAuxDyn.GhostTausFinalCount", &AntiKt4EMTopoJetsAuxDyn_GhostTausFinalCount, &b_AntiKt4EMTopoJetsAuxDyn_GhostTausFinalCount);
   fChain->SetBranchAddress("AntiKt4EMTopoJetsAuxDyn.GhostTausFinalPt", &AntiKt4EMTopoJetsAuxDyn_GhostTausFinalPt, &b_AntiKt4EMTopoJetsAuxDyn_GhostTausFinalPt);
   fChain->SetBranchAddress("AntiKt4EMTopoJetsAuxDyn.GhostWBosons", &AntiKt4EMTopoJetsAuxDyn_GhostWBosons, &b_AntiKt4EMTopoJetsAuxDyn_GhostWBosons);
   fChain->SetBranchAddress("AntiKt4EMTopoJetsAuxDyn.GhostWBosonsCount", &AntiKt4EMTopoJetsAuxDyn_GhostWBosonsCount, &b_AntiKt4EMTopoJetsAuxDyn_GhostWBosonsCount);
   fChain->SetBranchAddress("AntiKt4EMTopoJetsAuxDyn.GhostWBosonsPt", &AntiKt4EMTopoJetsAuxDyn_GhostWBosonsPt, &b_AntiKt4EMTopoJetsAuxDyn_GhostWBosonsPt);
   fChain->SetBranchAddress("AntiKt4EMTopoJetsAuxDyn.GhostZBosons", &AntiKt4EMTopoJetsAuxDyn_GhostZBosons, &b_AntiKt4EMTopoJetsAuxDyn_GhostZBosons);
   fChain->SetBranchAddress("AntiKt4EMTopoJetsAuxDyn.GhostZBosonsCount", &AntiKt4EMTopoJetsAuxDyn_GhostZBosonsCount, &b_AntiKt4EMTopoJetsAuxDyn_GhostZBosonsCount);
   fChain->SetBranchAddress("AntiKt4EMTopoJetsAuxDyn.GhostZBosonsPt", &AntiKt4EMTopoJetsAuxDyn_GhostZBosonsPt, &b_AntiKt4EMTopoJetsAuxDyn_GhostZBosonsPt);
   fChain->SetBranchAddress("AntiKt4EMTopoJetsAuxDyn.GhostHBosons", &AntiKt4EMTopoJetsAuxDyn_GhostHBosons, &b_AntiKt4EMTopoJetsAuxDyn_GhostHBosons);
   fChain->SetBranchAddress("AntiKt4EMTopoJetsAuxDyn.GhostHBosonsCount", &AntiKt4EMTopoJetsAuxDyn_GhostHBosonsCount, &b_AntiKt4EMTopoJetsAuxDyn_GhostHBosonsCount);
   fChain->SetBranchAddress("AntiKt4EMTopoJetsAuxDyn.GhostHBosonsPt", &AntiKt4EMTopoJetsAuxDyn_GhostHBosonsPt, &b_AntiKt4EMTopoJetsAuxDyn_GhostHBosonsPt);
   fChain->SetBranchAddress("AntiKt4EMTopoJetsAuxDyn.GhostTQuarksFinal", &AntiKt4EMTopoJetsAuxDyn_GhostTQuarksFinal, &b_AntiKt4EMTopoJetsAuxDyn_GhostTQuarksFinal);
   fChain->SetBranchAddress("AntiKt4EMTopoJetsAuxDyn.GhostTQuarksFinalCount", &AntiKt4EMTopoJetsAuxDyn_GhostTQuarksFinalCount, &b_AntiKt4EMTopoJetsAuxDyn_GhostTQuarksFinalCount);
   fChain->SetBranchAddress("AntiKt4EMTopoJetsAuxDyn.GhostTQuarksFinalPt", &AntiKt4EMTopoJetsAuxDyn_GhostTQuarksFinalPt, &b_AntiKt4EMTopoJetsAuxDyn_GhostTQuarksFinalPt);
   fChain->SetBranchAddress("AntiKt4EMTopoJetsAuxDyn.GhostPartons", &AntiKt4EMTopoJetsAuxDyn_GhostPartons, &b_AntiKt4EMTopoJetsAuxDyn_GhostPartons);
   fChain->SetBranchAddress("AntiKt4EMTopoJetsAuxDyn.GhostPartonsCount", &AntiKt4EMTopoJetsAuxDyn_GhostPartonsCount, &b_AntiKt4EMTopoJetsAuxDyn_GhostPartonsCount);
   fChain->SetBranchAddress("AntiKt4EMTopoJetsAuxDyn.GhostPartonsPt", &AntiKt4EMTopoJetsAuxDyn_GhostPartonsPt, &b_AntiKt4EMTopoJetsAuxDyn_GhostPartonsPt);
   fChain->SetBranchAddress("AntiKt4EMTopoJetsAuxDyn.JetGhostArea", &AntiKt4EMTopoJetsAuxDyn_JetGhostArea, &b_AntiKt4EMTopoJetsAuxDyn_JetGhostArea);
   fChain->SetBranchAddress("AntiKt4EMTopoJetsAuxDyn.DetectorEta", &AntiKt4EMTopoJetsAuxDyn_DetectorEta, &b_AntiKt4EMTopoJetsAuxDyn_DetectorEta);
   fChain->SetBranchAddress("AntiKt4EMTopoJetsAuxDyn.DetectorPhi", &AntiKt4EMTopoJetsAuxDyn_DetectorPhi, &b_AntiKt4EMTopoJetsAuxDyn_DetectorPhi);
   fChain->SetBranchAddress("AntiKt4EMTopoJetsAuxDyn.JetLCScaleMomentum_pt", &AntiKt4EMTopoJetsAuxDyn_JetLCScaleMomentum_pt, &b_AntiKt4EMTopoJetsAuxDyn_JetLCScaleMomentum_pt);
   fChain->SetBranchAddress("AntiKt4EMTopoJetsAuxDyn.JetLCScaleMomentum_eta", &AntiKt4EMTopoJetsAuxDyn_JetLCScaleMomentum_eta, &b_AntiKt4EMTopoJetsAuxDyn_JetLCScaleMomentum_eta);
   fChain->SetBranchAddress("AntiKt4EMTopoJetsAuxDyn.JetLCScaleMomentum_phi", &AntiKt4EMTopoJetsAuxDyn_JetLCScaleMomentum_phi, &b_AntiKt4EMTopoJetsAuxDyn_JetLCScaleMomentum_phi);
   fChain->SetBranchAddress("AntiKt4EMTopoJetsAuxDyn.JetLCScaleMomentum_m", &AntiKt4EMTopoJetsAuxDyn_JetLCScaleMomentum_m, &b_AntiKt4EMTopoJetsAuxDyn_JetLCScaleMomentum_m);
   fChain->SetBranchAddress("AntiKt4EMTopoJetsAuxDyn.HECFrac", &AntiKt4EMTopoJetsAuxDyn_HECFrac, &b_AntiKt4EMTopoJetsAuxDyn_HECFrac);
   fChain->SetBranchAddress("AntiKt4EMTopoJetsAuxDyn.OriginCorrected", &AntiKt4EMTopoJetsAuxDyn_OriginCorrected, &b_AntiKt4EMTopoJetsAuxDyn_OriginCorrected);
   fChain->SetBranchAddress("AntiKt4EMTopoJetsAuxDyn.PileupCorrected", &AntiKt4EMTopoJetsAuxDyn_PileupCorrected, &b_AntiKt4EMTopoJetsAuxDyn_PileupCorrected);
   fChain->SetBranchAddress("AntiKt4EMTopoJetsAuxDyn.JetPileupScaleMomentum_pt", &AntiKt4EMTopoJetsAuxDyn_JetPileupScaleMomentum_pt, &b_AntiKt4EMTopoJetsAuxDyn_JetPileupScaleMomentum_pt);
   fChain->SetBranchAddress("AntiKt4EMTopoJetsAuxDyn.JetPileupScaleMomentum_eta", &AntiKt4EMTopoJetsAuxDyn_JetPileupScaleMomentum_eta, &b_AntiKt4EMTopoJetsAuxDyn_JetPileupScaleMomentum_eta);
   fChain->SetBranchAddress("AntiKt4EMTopoJetsAuxDyn.JetPileupScaleMomentum_phi", &AntiKt4EMTopoJetsAuxDyn_JetPileupScaleMomentum_phi, &b_AntiKt4EMTopoJetsAuxDyn_JetPileupScaleMomentum_phi);
   fChain->SetBranchAddress("AntiKt4EMTopoJetsAuxDyn.JetPileupScaleMomentum_m", &AntiKt4EMTopoJetsAuxDyn_JetPileupScaleMomentum_m, &b_AntiKt4EMTopoJetsAuxDyn_JetPileupScaleMomentum_m);
   fChain->SetBranchAddress("AntiKt4EMTopoJetsAuxDyn.JetEtaJESScaleMomentum_pt", &AntiKt4EMTopoJetsAuxDyn_JetEtaJESScaleMomentum_pt, &b_AntiKt4EMTopoJetsAuxDyn_JetEtaJESScaleMomentum_pt);
   fChain->SetBranchAddress("AntiKt4EMTopoJetsAuxDyn.JetEtaJESScaleMomentum_eta", &AntiKt4EMTopoJetsAuxDyn_JetEtaJESScaleMomentum_eta, &b_AntiKt4EMTopoJetsAuxDyn_JetEtaJESScaleMomentum_eta);
   fChain->SetBranchAddress("AntiKt4EMTopoJetsAuxDyn.JetEtaJESScaleMomentum_phi", &AntiKt4EMTopoJetsAuxDyn_JetEtaJESScaleMomentum_phi, &b_AntiKt4EMTopoJetsAuxDyn_JetEtaJESScaleMomentum_phi);
   fChain->SetBranchAddress("AntiKt4EMTopoJetsAuxDyn.JetEtaJESScaleMomentum_m", &AntiKt4EMTopoJetsAuxDyn_JetEtaJESScaleMomentum_m, &b_AntiKt4EMTopoJetsAuxDyn_JetEtaJESScaleMomentum_m);
   fChain->SetBranchAddress("AntiKt4EMTopoJetsAuxDyn.WidthPhi", &AntiKt4EMTopoJetsAuxDyn_WidthPhi, &b_AntiKt4EMTopoJetsAuxDyn_WidthPhi);
   fChain->SetBranchAddress("AntiKt4EMTopoJetsAuxDyn.PartonTruthLabelID", &AntiKt4EMTopoJetsAuxDyn_PartonTruthLabelID, &b_AntiKt4EMTopoJetsAuxDyn_PartonTruthLabelID);
   fChain->SetBranchAddress("AntiKt4EMTopoJetsAuxDyn.TruthLabelDeltaR_B", &AntiKt4EMTopoJetsAuxDyn_TruthLabelDeltaR_B, &b_AntiKt4EMTopoJetsAuxDyn_TruthLabelDeltaR_B);
   fChain->SetBranchAddress("AntiKt4EMTopoJetsAuxDyn.TruthLabelDeltaR_C", &AntiKt4EMTopoJetsAuxDyn_TruthLabelDeltaR_C, &b_AntiKt4EMTopoJetsAuxDyn_TruthLabelDeltaR_C);
   fChain->SetBranchAddress("AntiKt4EMTopoJetsAuxDyn.TruthLabelDeltaR_T", &AntiKt4EMTopoJetsAuxDyn_TruthLabelDeltaR_T, &b_AntiKt4EMTopoJetsAuxDyn_TruthLabelDeltaR_T);
   fChain->SetBranchAddress("AntiKt4EMTopoJetsAuxDyn.ConeTruthLabelID", &AntiKt4EMTopoJetsAuxDyn_ConeTruthLabelID, &b_AntiKt4EMTopoJetsAuxDyn_ConeTruthLabelID);
   fChain->SetBranchAddress("AntiKt4EMTopoJetsAuxDyn.LArBadHVEnergyFrac", &AntiKt4EMTopoJetsAuxDyn_LArBadHVEnergyFrac, &b_AntiKt4EMTopoJetsAuxDyn_LArBadHVEnergyFrac);
   fChain->SetBranchAddress("AntiKt4EMTopoJetsAuxDyn.LArBadHVNCell", &AntiKt4EMTopoJetsAuxDyn_LArBadHVNCell, &b_AntiKt4EMTopoJetsAuxDyn_LArBadHVNCell);
   fChain->SetBranchAddress("AntiKt4EMTopoJetsAuxDyn.ECPSFraction", &AntiKt4EMTopoJetsAuxDyn_ECPSFraction, &b_AntiKt4EMTopoJetsAuxDyn_ECPSFraction);
   fChain->SetBranchAddress("AntiKt4EMTopoJetsAuxDyn.FracSamplingMax", &AntiKt4EMTopoJetsAuxDyn_FracSamplingMax, &b_AntiKt4EMTopoJetsAuxDyn_FracSamplingMax);
   fChain->SetBranchAddress("AntiKt4EMTopoJetsAuxDyn.FracSamplingMaxIndex", &AntiKt4EMTopoJetsAuxDyn_FracSamplingMaxIndex, &b_AntiKt4EMTopoJetsAuxDyn_FracSamplingMaxIndex);
   fChain->SetBranchAddress("AntiKt4EMTopoJetsAuxDyn.ConstituentScale", &AntiKt4EMTopoJetsAuxDyn_ConstituentScale, &b_AntiKt4EMTopoJetsAuxDyn_ConstituentScale);
   fChain->SetBranchAddress("AntiKt4EMTopoJetsAuxDyn.LArQuality", &AntiKt4EMTopoJetsAuxDyn_LArQuality, &b_AntiKt4EMTopoJetsAuxDyn_LArQuality);
   fChain->SetBranchAddress("AntiKt4EMTopoJetsAuxDyn.JetEMScaleMomentum_pt", &AntiKt4EMTopoJetsAuxDyn_JetEMScaleMomentum_pt, &b_AntiKt4EMTopoJetsAuxDyn_JetEMScaleMomentum_pt);
   fChain->SetBranchAddress("AntiKt4EMTopoJetsAuxDyn.N90Constituents", &AntiKt4EMTopoJetsAuxDyn_N90Constituents, &b_AntiKt4EMTopoJetsAuxDyn_N90Constituents);
   fChain->SetBranchAddress("AntiKt4EMTopoJetsAuxDyn.JetEMScaleMomentum_eta", &AntiKt4EMTopoJetsAuxDyn_JetEMScaleMomentum_eta, &b_AntiKt4EMTopoJetsAuxDyn_JetEMScaleMomentum_eta);
   fChain->SetBranchAddress("AntiKt4EMTopoJetsAuxDyn.NegativeE", &AntiKt4EMTopoJetsAuxDyn_NegativeE, &b_AntiKt4EMTopoJetsAuxDyn_NegativeE);
   fChain->SetBranchAddress("AntiKt4EMTopoJetsAuxDyn.JetEMScaleMomentum_phi", &AntiKt4EMTopoJetsAuxDyn_JetEMScaleMomentum_phi, &b_AntiKt4EMTopoJetsAuxDyn_JetEMScaleMomentum_phi);
   fChain->SetBranchAddress("AntiKt4EMTopoJetsAuxDyn.Timing", &AntiKt4EMTopoJetsAuxDyn_Timing, &b_AntiKt4EMTopoJetsAuxDyn_Timing);
   fChain->SetBranchAddress("AntiKt4EMTopoJetsAuxDyn.JetEMScaleMomentum_m", &AntiKt4EMTopoJetsAuxDyn_JetEMScaleMomentum_m, &b_AntiKt4EMTopoJetsAuxDyn_JetEMScaleMomentum_m);
   fChain->SetBranchAddress("AntiKt4EMTopoJetsAuxDyn.HECQuality", &AntiKt4EMTopoJetsAuxDyn_HECQuality, &b_AntiKt4EMTopoJetsAuxDyn_HECQuality);
   fChain->SetBranchAddress("AntiKt4EMTopoJetsAuxDyn.JetConstitScaleMomentum_pt", &AntiKt4EMTopoJetsAuxDyn_JetConstitScaleMomentum_pt, &b_AntiKt4EMTopoJetsAuxDyn_JetConstitScaleMomentum_pt);
   fChain->SetBranchAddress("AntiKt4EMTopoJetsAuxDyn.JetConstitScaleMomentum_eta", &AntiKt4EMTopoJetsAuxDyn_JetConstitScaleMomentum_eta, &b_AntiKt4EMTopoJetsAuxDyn_JetConstitScaleMomentum_eta);
   fChain->SetBranchAddress("AntiKt4EMTopoJetsAuxDyn.CentroidR", &AntiKt4EMTopoJetsAuxDyn_CentroidR, &b_AntiKt4EMTopoJetsAuxDyn_CentroidR);
   fChain->SetBranchAddress("AntiKt4EMTopoJetsAuxDyn.JetConstitScaleMomentum_phi", &AntiKt4EMTopoJetsAuxDyn_JetConstitScaleMomentum_phi, &b_AntiKt4EMTopoJetsAuxDyn_JetConstitScaleMomentum_phi);
   fChain->SetBranchAddress("AntiKt4EMTopoJetsAuxDyn.AverageLArQF", &AntiKt4EMTopoJetsAuxDyn_AverageLArQF, &b_AntiKt4EMTopoJetsAuxDyn_AverageLArQF);
   fChain->SetBranchAddress("AntiKt4EMTopoJetsAuxDyn.JetConstitScaleMomentum_m", &AntiKt4EMTopoJetsAuxDyn_JetConstitScaleMomentum_m, &b_AntiKt4EMTopoJetsAuxDyn_JetConstitScaleMomentum_m);
   fChain->SetBranchAddress("AntiKt4EMTopoJetsAuxDyn.BchCorrCell", &AntiKt4EMTopoJetsAuxDyn_BchCorrCell, &b_AntiKt4EMTopoJetsAuxDyn_BchCorrCell);
   fChain->SetBranchAddress("AntiKt4EMTopoJetsAuxDyn.InputType", &AntiKt4EMTopoJetsAuxDyn_InputType, &b_AntiKt4EMTopoJetsAuxDyn_InputType);
   fChain->SetBranchAddress("AntiKt4EMTopoJetsAuxDyn.OotFracClusters5", &AntiKt4EMTopoJetsAuxDyn_OotFracClusters5, &b_AntiKt4EMTopoJetsAuxDyn_OotFracClusters5);
   fChain->SetBranchAddress("AntiKt4EMTopoJetsAuxDyn.AlgorithmType", &AntiKt4EMTopoJetsAuxDyn_AlgorithmType, &b_AntiKt4EMTopoJetsAuxDyn_AlgorithmType);
   fChain->SetBranchAddress("AntiKt4EMTopoJetsAuxDyn.OotFracClusters10", &AntiKt4EMTopoJetsAuxDyn_OotFracClusters10, &b_AntiKt4EMTopoJetsAuxDyn_OotFracClusters10);
   fChain->SetBranchAddress("AntiKt4EMTopoJetsAuxDyn.SizeParameter", &AntiKt4EMTopoJetsAuxDyn_SizeParameter, &b_AntiKt4EMTopoJetsAuxDyn_SizeParameter);
   fChain->SetBranchAddress("AntiKt4EMTopoJetsAuxDyn.TrackWidthPt500", &AntiKt4EMTopoJetsAuxDyn_TrackWidthPt500, &b_AntiKt4EMTopoJetsAuxDyn_TrackWidthPt500);
   fChain->SetBranchAddress("AntiKt4EMTopoJetsAuxDyn.NumTrkPt1000", &AntiKt4EMTopoJetsAuxDyn_NumTrkPt1000, &b_AntiKt4EMTopoJetsAuxDyn_NumTrkPt1000);
   fChain->SetBranchAddress("AntiKt4EMTopoJetsAuxDyn.SumPtTrkPt1000", &AntiKt4EMTopoJetsAuxDyn_SumPtTrkPt1000, &b_AntiKt4EMTopoJetsAuxDyn_SumPtTrkPt1000);
   fChain->SetBranchAddress("AntiKt4EMTopoJetsAuxDyn.btaggingLink", &AntiKt4EMTopoJetsAuxDyn_btaggingLink_, &b_AntiKt4EMTopoJetsAuxDyn_btaggingLink_);
   fChain->SetBranchAddress("AntiKt4EMTopoJetsAuxDyn.btaggingLink.m_persKey", AntiKt4EMTopoJetsAuxDyn_btaggingLink_m_persKey, &b_AntiKt4EMTopoJetsAuxDyn_btaggingLink_m_persKey);
   fChain->SetBranchAddress("AntiKt4EMTopoJetsAuxDyn.btaggingLink.m_persIndex", AntiKt4EMTopoJetsAuxDyn_btaggingLink_m_persIndex, &b_AntiKt4EMTopoJetsAuxDyn_btaggingLink_m_persIndex);
   fChain->SetBranchAddress("AntiKt4EMTopoJetsAuxDyn.TrackWidthPt1000", &AntiKt4EMTopoJetsAuxDyn_TrackWidthPt1000, &b_AntiKt4EMTopoJetsAuxDyn_TrackWidthPt1000);
   fChain->SetBranchAddress("AntiKt4EMTopoJetsAuxDyn.TrackSumPt", &AntiKt4EMTopoJetsAuxDyn_TrackSumPt, &b_AntiKt4EMTopoJetsAuxDyn_TrackSumPt);
   fChain->SetBranchAddress("AntiKt4EMTopoJetsAuxDyn.TrackSumMass", &AntiKt4EMTopoJetsAuxDyn_TrackSumMass, &b_AntiKt4EMTopoJetsAuxDyn_TrackSumMass);
   fChain->SetBranchAddress("AntiKt4EMTopoJetsAuxDyn.JVF", &AntiKt4EMTopoJetsAuxDyn_JVF, &b_AntiKt4EMTopoJetsAuxDyn_JVF);
   fChain->SetBranchAddress("AntiKt4EMTopoJetsAuxDyn.HighestJVFVtx", &AntiKt4EMTopoJetsAuxDyn_HighestJVFVtx_, &b_AntiKt4EMTopoJetsAuxDyn_HighestJVFVtx_);
   fChain->SetBranchAddress("AntiKt4EMTopoJetsAuxDyn.HighestJVFVtx.m_persKey", AntiKt4EMTopoJetsAuxDyn_HighestJVFVtx_m_persKey, &b_AntiKt4EMTopoJetsAuxDyn_HighestJVFVtx_m_persKey);
   fChain->SetBranchAddress("AntiKt4EMTopoJetsAuxDyn.HighestJVFVtx.m_persIndex", AntiKt4EMTopoJetsAuxDyn_HighestJVFVtx_m_persIndex, &b_AntiKt4EMTopoJetsAuxDyn_HighestJVFVtx_m_persIndex);
   fChain->SetBranchAddress("AntiKt4EMTopoJetsAuxDyn.JVFCorr", &AntiKt4EMTopoJetsAuxDyn_JVFCorr, &b_AntiKt4EMTopoJetsAuxDyn_JVFCorr);
   fChain->SetBranchAddress("AntiKt4EMTopoJetsAuxDyn.JvtRpt", &AntiKt4EMTopoJetsAuxDyn_JvtRpt, &b_AntiKt4EMTopoJetsAuxDyn_JvtRpt);
   fChain->SetBranchAddress("AntiKt4EMTopoJetsAuxDyn.Jvt", &AntiKt4EMTopoJetsAuxDyn_Jvt, &b_AntiKt4EMTopoJetsAuxDyn_Jvt);
   fChain->SetBranchAddress("AntiKt4EMTopoJetsAuxDyn.Charge", &AntiKt4EMTopoJetsAuxDyn_Charge, &b_AntiKt4EMTopoJetsAuxDyn_Charge);
   fChain->SetBranchAddress("AntiKt4EMTopoJetsAuxDyn.OriginVertex", &AntiKt4EMTopoJetsAuxDyn_OriginVertex_, &b_AntiKt4EMTopoJetsAuxDyn_OriginVertex_);
   fChain->SetBranchAddress("AntiKt4EMTopoJetsAuxDyn.OriginVertex.m_persKey", AntiKt4EMTopoJetsAuxDyn_OriginVertex_m_persKey, &b_AntiKt4EMTopoJetsAuxDyn_OriginVertex_m_persKey);
   fChain->SetBranchAddress("AntiKt4EMTopoJetsAuxDyn.OriginVertex.m_persIndex", AntiKt4EMTopoJetsAuxDyn_OriginVertex_m_persIndex, &b_AntiKt4EMTopoJetsAuxDyn_OriginVertex_m_persIndex);
   fChain->SetBranchAddress("AntiKt4EMTopoJetsAuxDyn.ConeExclBHadronsFinal", &AntiKt4EMTopoJetsAuxDyn_ConeExclBHadronsFinal, &b_AntiKt4EMTopoJetsAuxDyn_ConeExclBHadronsFinal);
   fChain->SetBranchAddress("AntiKt4EMTopoJetsAuxDyn.ConeExclCHadronsFinal", &AntiKt4EMTopoJetsAuxDyn_ConeExclCHadronsFinal, &b_AntiKt4EMTopoJetsAuxDyn_ConeExclCHadronsFinal);
   fChain->SetBranchAddress("AntiKt4EMTopoJetsAuxDyn.ConeExclTausFinal", &AntiKt4EMTopoJetsAuxDyn_ConeExclTausFinal, &b_AntiKt4EMTopoJetsAuxDyn_ConeExclTausFinal);
   fChain->SetBranchAddress("AntiKt4EMTopoJetsAuxDyn.HadronConeExclTruthLabelID", &AntiKt4EMTopoJetsAuxDyn_HadronConeExclTruthLabelID, &b_AntiKt4EMTopoJetsAuxDyn_HadronConeExclTruthLabelID);
   fChain->SetBranchAddress("AntiKt4EMTopoJetsAuxDyn.HadronConeExclExtendedTruthLabelID", &AntiKt4EMTopoJetsAuxDyn_HadronConeExclExtendedTruthLabelID, &b_AntiKt4EMTopoJetsAuxDyn_HadronConeExclExtendedTruthLabelID);
   fChain->SetBranchAddress("AntiKt4EMTopoJetsAuxDyn.LeadingClusterPt", &AntiKt4EMTopoJetsAuxDyn_LeadingClusterPt, &b_AntiKt4EMTopoJetsAuxDyn_LeadingClusterPt);
   fChain->SetBranchAddress("AntiKt4EMTopoJetsAuxDyn.LeadingClusterSecondLambda", &AntiKt4EMTopoJetsAuxDyn_LeadingClusterSecondLambda, &b_AntiKt4EMTopoJetsAuxDyn_LeadingClusterSecondLambda);
   fChain->SetBranchAddress("AntiKt4LCTopoJetsAuxDyn.LeadingClusterCenterLambda", &AntiKt4LCTopoJetsAuxDyn_LeadingClusterCenterLambda, &b_AntiKt4LCTopoJetsAuxDyn_LeadingClusterCenterLambda);
   fChain->SetBranchAddress("AntiKt4LCTopoJetsAuxDyn.LeadingClusterSecondR", &AntiKt4LCTopoJetsAuxDyn_LeadingClusterSecondR, &b_AntiKt4LCTopoJetsAuxDyn_LeadingClusterSecondR);
   fChain->SetBranchAddress("AntiKt4LCTopoJetsAuxDyn.GhostTruth", &AntiKt4LCTopoJetsAuxDyn_GhostTruth, &b_AntiKt4LCTopoJetsAuxDyn_GhostTruth);
   fChain->SetBranchAddress("AntiKt4LCTopoJetsAuxDyn.NumTrkPt500", &AntiKt4LCTopoJetsAuxDyn_NumTrkPt500, &b_AntiKt4LCTopoJetsAuxDyn_NumTrkPt500);
   fChain->SetBranchAddress("AntiKt4LCTopoJetsAuxDyn.SumPtTrkPt500", &AntiKt4LCTopoJetsAuxDyn_SumPtTrkPt500, &b_AntiKt4LCTopoJetsAuxDyn_SumPtTrkPt500);
   fChain->SetBranchAddress("AntiKt4LCTopoJetsAuxDyn.EnergyPerSampling", &AntiKt4LCTopoJetsAuxDyn_EnergyPerSampling, &b_AntiKt4LCTopoJetsAuxDyn_EnergyPerSampling);
   fChain->SetBranchAddress("AntiKt4LCTopoJetsAuxDyn.EMFrac", &AntiKt4LCTopoJetsAuxDyn_EMFrac, &b_AntiKt4LCTopoJetsAuxDyn_EMFrac);
   fChain->SetBranchAddress("AntiKt4LCTopoJetsAuxDyn.Width", &AntiKt4LCTopoJetsAuxDyn_Width, &b_AntiKt4LCTopoJetsAuxDyn_Width);
   fChain->SetBranchAddress("AntiKt4LCTopoJetsAuxDyn.ActiveArea", &AntiKt4LCTopoJetsAuxDyn_ActiveArea, &b_AntiKt4LCTopoJetsAuxDyn_ActiveArea);
   fChain->SetBranchAddress("AntiKt4LCTopoJetsAuxDyn.ActiveArea4vec_pt", &AntiKt4LCTopoJetsAuxDyn_ActiveArea4vec_pt, &b_AntiKt4LCTopoJetsAuxDyn_ActiveArea4vec_pt);
   fChain->SetBranchAddress("AntiKt4LCTopoJetsAuxDyn.ActiveArea4vec_eta", &AntiKt4LCTopoJetsAuxDyn_ActiveArea4vec_eta, &b_AntiKt4LCTopoJetsAuxDyn_ActiveArea4vec_eta);
   fChain->SetBranchAddress("AntiKt4LCTopoJetsAuxDyn.ActiveArea4vec_phi", &AntiKt4LCTopoJetsAuxDyn_ActiveArea4vec_phi, &b_AntiKt4LCTopoJetsAuxDyn_ActiveArea4vec_phi);
   fChain->SetBranchAddress("AntiKt4LCTopoJetsAuxDyn.ActiveArea4vec_m", &AntiKt4LCTopoJetsAuxDyn_ActiveArea4vec_m, &b_AntiKt4LCTopoJetsAuxDyn_ActiveArea4vec_m);
   fChain->SetBranchAddress("AntiKt4LCTopoJetsAuxDyn.GhostTrack", &AntiKt4LCTopoJetsAuxDyn_GhostTrack, &b_AntiKt4LCTopoJetsAuxDyn_GhostTrack);
   fChain->SetBranchAddress("AntiKt4LCTopoJetsAuxDyn.GhostTrackCount", &AntiKt4LCTopoJetsAuxDyn_GhostTrackCount, &b_AntiKt4LCTopoJetsAuxDyn_GhostTrackCount);
   fChain->SetBranchAddress("AntiKt4LCTopoJetsAuxDyn.GhostTrackPt", &AntiKt4LCTopoJetsAuxDyn_GhostTrackPt, &b_AntiKt4LCTopoJetsAuxDyn_GhostTrackPt);
   fChain->SetBranchAddress("AntiKt4LCTopoJetsAuxDyn.GhostMuonSegment", &AntiKt4LCTopoJetsAuxDyn_GhostMuonSegment, &b_AntiKt4LCTopoJetsAuxDyn_GhostMuonSegment);
   fChain->SetBranchAddress("AntiKt4LCTopoJetsAuxDyn.GhostMuonSegmentCount", &AntiKt4LCTopoJetsAuxDyn_GhostMuonSegmentCount, &b_AntiKt4LCTopoJetsAuxDyn_GhostMuonSegmentCount);
   fChain->SetBranchAddress("AntiKt4LCTopoJetsAuxDyn.GhostTruthCount", &AntiKt4LCTopoJetsAuxDyn_GhostTruthCount, &b_AntiKt4LCTopoJetsAuxDyn_GhostTruthCount);
   fChain->SetBranchAddress("AntiKt4LCTopoJetsAuxDyn.GhostTruthPt", &AntiKt4LCTopoJetsAuxDyn_GhostTruthPt, &b_AntiKt4LCTopoJetsAuxDyn_GhostTruthPt);
   fChain->SetBranchAddress("AntiKt4LCTopoJetsAuxDyn.GhostBHadronsInitial", &AntiKt4LCTopoJetsAuxDyn_GhostBHadronsInitial, &b_AntiKt4LCTopoJetsAuxDyn_GhostBHadronsInitial);
   fChain->SetBranchAddress("AntiKt4LCTopoJetsAuxDyn.GhostBHadronsInitialCount", &AntiKt4LCTopoJetsAuxDyn_GhostBHadronsInitialCount, &b_AntiKt4LCTopoJetsAuxDyn_GhostBHadronsInitialCount);
   fChain->SetBranchAddress("AntiKt4LCTopoJetsAuxDyn.GhostBHadronsInitialPt", &AntiKt4LCTopoJetsAuxDyn_GhostBHadronsInitialPt, &b_AntiKt4LCTopoJetsAuxDyn_GhostBHadronsInitialPt);
   fChain->SetBranchAddress("AntiKt4LCTopoJetsAuxDyn.GhostBHadronsFinal", &AntiKt4LCTopoJetsAuxDyn_GhostBHadronsFinal, &b_AntiKt4LCTopoJetsAuxDyn_GhostBHadronsFinal);
   fChain->SetBranchAddress("AntiKt4LCTopoJetsAuxDyn.GhostBHadronsFinalCount", &AntiKt4LCTopoJetsAuxDyn_GhostBHadronsFinalCount, &b_AntiKt4LCTopoJetsAuxDyn_GhostBHadronsFinalCount);
   fChain->SetBranchAddress("AntiKt4LCTopoJetsAuxDyn.GhostBHadronsFinalPt", &AntiKt4LCTopoJetsAuxDyn_GhostBHadronsFinalPt, &b_AntiKt4LCTopoJetsAuxDyn_GhostBHadronsFinalPt);
   fChain->SetBranchAddress("AntiKt4LCTopoJetsAuxDyn.GhostBQuarksFinal", &AntiKt4LCTopoJetsAuxDyn_GhostBQuarksFinal, &b_AntiKt4LCTopoJetsAuxDyn_GhostBQuarksFinal);
   fChain->SetBranchAddress("AntiKt4LCTopoJetsAuxDyn.GhostBQuarksFinalCount", &AntiKt4LCTopoJetsAuxDyn_GhostBQuarksFinalCount, &b_AntiKt4LCTopoJetsAuxDyn_GhostBQuarksFinalCount);
   fChain->SetBranchAddress("AntiKt4LCTopoJetsAuxDyn.GhostBQuarksFinalPt", &AntiKt4LCTopoJetsAuxDyn_GhostBQuarksFinalPt, &b_AntiKt4LCTopoJetsAuxDyn_GhostBQuarksFinalPt);
   fChain->SetBranchAddress("AntiKt4LCTopoJetsAuxDyn.GhostCHadronsInitial", &AntiKt4LCTopoJetsAuxDyn_GhostCHadronsInitial, &b_AntiKt4LCTopoJetsAuxDyn_GhostCHadronsInitial);
   fChain->SetBranchAddress("AntiKt4LCTopoJetsAuxDyn.GhostCHadronsInitialCount", &AntiKt4LCTopoJetsAuxDyn_GhostCHadronsInitialCount, &b_AntiKt4LCTopoJetsAuxDyn_GhostCHadronsInitialCount);
   fChain->SetBranchAddress("AntiKt4LCTopoJetsAuxDyn.GhostCHadronsInitialPt", &AntiKt4LCTopoJetsAuxDyn_GhostCHadronsInitialPt, &b_AntiKt4LCTopoJetsAuxDyn_GhostCHadronsInitialPt);
   fChain->SetBranchAddress("AntiKt4LCTopoJetsAuxDyn.GhostCHadronsFinal", &AntiKt4LCTopoJetsAuxDyn_GhostCHadronsFinal, &b_AntiKt4LCTopoJetsAuxDyn_GhostCHadronsFinal);
   fChain->SetBranchAddress("AntiKt4LCTopoJetsAuxDyn.GhostCHadronsFinalCount", &AntiKt4LCTopoJetsAuxDyn_GhostCHadronsFinalCount, &b_AntiKt4LCTopoJetsAuxDyn_GhostCHadronsFinalCount);
   fChain->SetBranchAddress("AntiKt4LCTopoJetsAuxDyn.GhostCHadronsFinalPt", &AntiKt4LCTopoJetsAuxDyn_GhostCHadronsFinalPt, &b_AntiKt4LCTopoJetsAuxDyn_GhostCHadronsFinalPt);
   fChain->SetBranchAddress("AntiKt4LCTopoJetsAuxDyn.GhostCQuarksFinal", &AntiKt4LCTopoJetsAuxDyn_GhostCQuarksFinal, &b_AntiKt4LCTopoJetsAuxDyn_GhostCQuarksFinal);
   fChain->SetBranchAddress("AntiKt4LCTopoJetsAuxDyn.GhostCQuarksFinalCount", &AntiKt4LCTopoJetsAuxDyn_GhostCQuarksFinalCount, &b_AntiKt4LCTopoJetsAuxDyn_GhostCQuarksFinalCount);
   fChain->SetBranchAddress("AntiKt4LCTopoJetsAuxDyn.GhostCQuarksFinalPt", &AntiKt4LCTopoJetsAuxDyn_GhostCQuarksFinalPt, &b_AntiKt4LCTopoJetsAuxDyn_GhostCQuarksFinalPt);
   fChain->SetBranchAddress("AntiKt4LCTopoJetsAuxDyn.GhostTausFinal", &AntiKt4LCTopoJetsAuxDyn_GhostTausFinal, &b_AntiKt4LCTopoJetsAuxDyn_GhostTausFinal);
   fChain->SetBranchAddress("AntiKt4LCTopoJetsAuxDyn.GhostTausFinalCount", &AntiKt4LCTopoJetsAuxDyn_GhostTausFinalCount, &b_AntiKt4LCTopoJetsAuxDyn_GhostTausFinalCount);
   fChain->SetBranchAddress("AntiKt4LCTopoJetsAuxDyn.GhostTausFinalPt", &AntiKt4LCTopoJetsAuxDyn_GhostTausFinalPt, &b_AntiKt4LCTopoJetsAuxDyn_GhostTausFinalPt);
   fChain->SetBranchAddress("AntiKt4LCTopoJetsAuxDyn.GhostWBosons", &AntiKt4LCTopoJetsAuxDyn_GhostWBosons, &b_AntiKt4LCTopoJetsAuxDyn_GhostWBosons);
   fChain->SetBranchAddress("AntiKt4LCTopoJetsAuxDyn.GhostWBosonsCount", &AntiKt4LCTopoJetsAuxDyn_GhostWBosonsCount, &b_AntiKt4LCTopoJetsAuxDyn_GhostWBosonsCount);
   fChain->SetBranchAddress("AntiKt4LCTopoJetsAuxDyn.GhostWBosonsPt", &AntiKt4LCTopoJetsAuxDyn_GhostWBosonsPt, &b_AntiKt4LCTopoJetsAuxDyn_GhostWBosonsPt);
   fChain->SetBranchAddress("AntiKt4LCTopoJetsAuxDyn.GhostZBosons", &AntiKt4LCTopoJetsAuxDyn_GhostZBosons, &b_AntiKt4LCTopoJetsAuxDyn_GhostZBosons);
   fChain->SetBranchAddress("AntiKt4LCTopoJetsAuxDyn.GhostZBosonsCount", &AntiKt4LCTopoJetsAuxDyn_GhostZBosonsCount, &b_AntiKt4LCTopoJetsAuxDyn_GhostZBosonsCount);
   fChain->SetBranchAddress("AntiKt4LCTopoJetsAuxDyn.GhostZBosonsPt", &AntiKt4LCTopoJetsAuxDyn_GhostZBosonsPt, &b_AntiKt4LCTopoJetsAuxDyn_GhostZBosonsPt);
   fChain->SetBranchAddress("AntiKt4LCTopoJetsAuxDyn.GhostHBosons", &AntiKt4LCTopoJetsAuxDyn_GhostHBosons, &b_AntiKt4LCTopoJetsAuxDyn_GhostHBosons);
   fChain->SetBranchAddress("AntiKt4LCTopoJetsAuxDyn.GhostHBosonsCount", &AntiKt4LCTopoJetsAuxDyn_GhostHBosonsCount, &b_AntiKt4LCTopoJetsAuxDyn_GhostHBosonsCount);
   fChain->SetBranchAddress("AntiKt4LCTopoJetsAuxDyn.GhostHBosonsPt", &AntiKt4LCTopoJetsAuxDyn_GhostHBosonsPt, &b_AntiKt4LCTopoJetsAuxDyn_GhostHBosonsPt);
   fChain->SetBranchAddress("AntiKt4LCTopoJetsAuxDyn.GhostTQuarksFinal", &AntiKt4LCTopoJetsAuxDyn_GhostTQuarksFinal, &b_AntiKt4LCTopoJetsAuxDyn_GhostTQuarksFinal);
   fChain->SetBranchAddress("AntiKt4LCTopoJetsAuxDyn.GhostTQuarksFinalCount", &AntiKt4LCTopoJetsAuxDyn_GhostTQuarksFinalCount, &b_AntiKt4LCTopoJetsAuxDyn_GhostTQuarksFinalCount);
   fChain->SetBranchAddress("AntiKt4LCTopoJetsAuxDyn.GhostTQuarksFinalPt", &AntiKt4LCTopoJetsAuxDyn_GhostTQuarksFinalPt, &b_AntiKt4LCTopoJetsAuxDyn_GhostTQuarksFinalPt);
   fChain->SetBranchAddress("AntiKt4LCTopoJetsAuxDyn.GhostPartons", &AntiKt4LCTopoJetsAuxDyn_GhostPartons, &b_AntiKt4LCTopoJetsAuxDyn_GhostPartons);
   fChain->SetBranchAddress("AntiKt4LCTopoJetsAuxDyn.GhostPartonsCount", &AntiKt4LCTopoJetsAuxDyn_GhostPartonsCount, &b_AntiKt4LCTopoJetsAuxDyn_GhostPartonsCount);
   fChain->SetBranchAddress("AntiKt4LCTopoJetsAuxDyn.GhostPartonsPt", &AntiKt4LCTopoJetsAuxDyn_GhostPartonsPt, &b_AntiKt4LCTopoJetsAuxDyn_GhostPartonsPt);
   fChain->SetBranchAddress("AntiKt4LCTopoJetsAuxDyn.JetGhostArea", &AntiKt4LCTopoJetsAuxDyn_JetGhostArea, &b_AntiKt4LCTopoJetsAuxDyn_JetGhostArea);
   fChain->SetBranchAddress("AntiKt4LCTopoJetsAuxDyn.DetectorEta", &AntiKt4LCTopoJetsAuxDyn_DetectorEta, &b_AntiKt4LCTopoJetsAuxDyn_DetectorEta);
   fChain->SetBranchAddress("AntiKt4LCTopoJetsAuxDyn.DetectorPhi", &AntiKt4LCTopoJetsAuxDyn_DetectorPhi, &b_AntiKt4LCTopoJetsAuxDyn_DetectorPhi);
   fChain->SetBranchAddress("AntiKt4LCTopoJetsAuxDyn.HECFrac", &AntiKt4LCTopoJetsAuxDyn_HECFrac, &b_AntiKt4LCTopoJetsAuxDyn_HECFrac);
   fChain->SetBranchAddress("AntiKt4LCTopoJetsAuxDyn.OriginCorrected", &AntiKt4LCTopoJetsAuxDyn_OriginCorrected, &b_AntiKt4LCTopoJetsAuxDyn_OriginCorrected);
   fChain->SetBranchAddress("AntiKt4LCTopoJetsAuxDyn.PileupCorrected", &AntiKt4LCTopoJetsAuxDyn_PileupCorrected, &b_AntiKt4LCTopoJetsAuxDyn_PileupCorrected);
   fChain->SetBranchAddress("AntiKt4LCTopoJetsAuxDyn.JetPileupScaleMomentum_pt", &AntiKt4LCTopoJetsAuxDyn_JetPileupScaleMomentum_pt, &b_AntiKt4LCTopoJetsAuxDyn_JetPileupScaleMomentum_pt);
   fChain->SetBranchAddress("AntiKt4LCTopoJetsAuxDyn.JetPileupScaleMomentum_eta", &AntiKt4LCTopoJetsAuxDyn_JetPileupScaleMomentum_eta, &b_AntiKt4LCTopoJetsAuxDyn_JetPileupScaleMomentum_eta);
   fChain->SetBranchAddress("AntiKt4LCTopoJetsAuxDyn.JetPileupScaleMomentum_phi", &AntiKt4LCTopoJetsAuxDyn_JetPileupScaleMomentum_phi, &b_AntiKt4LCTopoJetsAuxDyn_JetPileupScaleMomentum_phi);
   fChain->SetBranchAddress("AntiKt4LCTopoJetsAuxDyn.JetPileupScaleMomentum_m", &AntiKt4LCTopoJetsAuxDyn_JetPileupScaleMomentum_m, &b_AntiKt4LCTopoJetsAuxDyn_JetPileupScaleMomentum_m);
   fChain->SetBranchAddress("AntiKt4LCTopoJetsAuxDyn.JetEtaJESScaleMomentum_pt", &AntiKt4LCTopoJetsAuxDyn_JetEtaJESScaleMomentum_pt, &b_AntiKt4LCTopoJetsAuxDyn_JetEtaJESScaleMomentum_pt);
   fChain->SetBranchAddress("AntiKt4LCTopoJetsAuxDyn.JetEtaJESScaleMomentum_eta", &AntiKt4LCTopoJetsAuxDyn_JetEtaJESScaleMomentum_eta, &b_AntiKt4LCTopoJetsAuxDyn_JetEtaJESScaleMomentum_eta);
   fChain->SetBranchAddress("AntiKt4LCTopoJetsAuxDyn.JetEtaJESScaleMomentum_phi", &AntiKt4LCTopoJetsAuxDyn_JetEtaJESScaleMomentum_phi, &b_AntiKt4LCTopoJetsAuxDyn_JetEtaJESScaleMomentum_phi);
   fChain->SetBranchAddress("AntiKt4LCTopoJetsAuxDyn.JetEtaJESScaleMomentum_m", &AntiKt4LCTopoJetsAuxDyn_JetEtaJESScaleMomentum_m, &b_AntiKt4LCTopoJetsAuxDyn_JetEtaJESScaleMomentum_m);
   fChain->SetBranchAddress("AntiKt4LCTopoJetsAuxDyn.WidthPhi", &AntiKt4LCTopoJetsAuxDyn_WidthPhi, &b_AntiKt4LCTopoJetsAuxDyn_WidthPhi);
   fChain->SetBranchAddress("AntiKt4LCTopoJetsAuxDyn.PartonTruthLabelID", &AntiKt4LCTopoJetsAuxDyn_PartonTruthLabelID, &b_AntiKt4LCTopoJetsAuxDyn_PartonTruthLabelID);
   fChain->SetBranchAddress("AntiKt4LCTopoJetsAuxDyn.TruthLabelDeltaR_B", &AntiKt4LCTopoJetsAuxDyn_TruthLabelDeltaR_B, &b_AntiKt4LCTopoJetsAuxDyn_TruthLabelDeltaR_B);
   fChain->SetBranchAddress("AntiKt4LCTopoJetsAuxDyn.TruthLabelDeltaR_C", &AntiKt4LCTopoJetsAuxDyn_TruthLabelDeltaR_C, &b_AntiKt4LCTopoJetsAuxDyn_TruthLabelDeltaR_C);
   fChain->SetBranchAddress("AntiKt4LCTopoJetsAuxDyn.TruthLabelDeltaR_T", &AntiKt4LCTopoJetsAuxDyn_TruthLabelDeltaR_T, &b_AntiKt4LCTopoJetsAuxDyn_TruthLabelDeltaR_T);
   fChain->SetBranchAddress("AntiKt4LCTopoJetsAuxDyn.ConeTruthLabelID", &AntiKt4LCTopoJetsAuxDyn_ConeTruthLabelID, &b_AntiKt4LCTopoJetsAuxDyn_ConeTruthLabelID);
   fChain->SetBranchAddress("AntiKt4LCTopoJetsAuxDyn.LArBadHVEnergyFrac", &AntiKt4LCTopoJetsAuxDyn_LArBadHVEnergyFrac, &b_AntiKt4LCTopoJetsAuxDyn_LArBadHVEnergyFrac);
   fChain->SetBranchAddress("AntiKt4LCTopoJetsAuxDyn.LArBadHVNCell", &AntiKt4LCTopoJetsAuxDyn_LArBadHVNCell, &b_AntiKt4LCTopoJetsAuxDyn_LArBadHVNCell);
   fChain->SetBranchAddress("AntiKt4LCTopoJetsAuxDyn.ECPSFraction", &AntiKt4LCTopoJetsAuxDyn_ECPSFraction, &b_AntiKt4LCTopoJetsAuxDyn_ECPSFraction);
   fChain->SetBranchAddress("AntiKt4LCTopoJetsAuxDyn.FracSamplingMax", &AntiKt4LCTopoJetsAuxDyn_FracSamplingMax, &b_AntiKt4LCTopoJetsAuxDyn_FracSamplingMax);
   fChain->SetBranchAddress("AntiKt4LCTopoJetsAuxDyn.FracSamplingMaxIndex", &AntiKt4LCTopoJetsAuxDyn_FracSamplingMaxIndex, &b_AntiKt4LCTopoJetsAuxDyn_FracSamplingMaxIndex);
   fChain->SetBranchAddress("AntiKt4LCTopoJetsAuxDyn.ConstituentScale", &AntiKt4LCTopoJetsAuxDyn_ConstituentScale, &b_AntiKt4LCTopoJetsAuxDyn_ConstituentScale);
   fChain->SetBranchAddress("AntiKt4LCTopoJetsAuxDyn.LArQuality", &AntiKt4LCTopoJetsAuxDyn_LArQuality, &b_AntiKt4LCTopoJetsAuxDyn_LArQuality);
   fChain->SetBranchAddress("AntiKt4LCTopoJetsAuxDyn.JetEMScaleMomentum_pt", &AntiKt4LCTopoJetsAuxDyn_JetEMScaleMomentum_pt, &b_AntiKt4LCTopoJetsAuxDyn_JetEMScaleMomentum_pt);
   fChain->SetBranchAddress("AntiKt4LCTopoJetsAuxDyn.N90Constituents", &AntiKt4LCTopoJetsAuxDyn_N90Constituents, &b_AntiKt4LCTopoJetsAuxDyn_N90Constituents);
   fChain->SetBranchAddress("AntiKt4LCTopoJetsAuxDyn.JetEMScaleMomentum_eta", &AntiKt4LCTopoJetsAuxDyn_JetEMScaleMomentum_eta, &b_AntiKt4LCTopoJetsAuxDyn_JetEMScaleMomentum_eta);
   fChain->SetBranchAddress("AntiKt4LCTopoJetsAuxDyn.NegativeE", &AntiKt4LCTopoJetsAuxDyn_NegativeE, &b_AntiKt4LCTopoJetsAuxDyn_NegativeE);
   fChain->SetBranchAddress("AntiKt4LCTopoJetsAuxDyn.JetEMScaleMomentum_phi", &AntiKt4LCTopoJetsAuxDyn_JetEMScaleMomentum_phi, &b_AntiKt4LCTopoJetsAuxDyn_JetEMScaleMomentum_phi);
   fChain->SetBranchAddress("AntiKt4LCTopoJetsAuxDyn.Timing", &AntiKt4LCTopoJetsAuxDyn_Timing, &b_AntiKt4LCTopoJetsAuxDyn_Timing);
   fChain->SetBranchAddress("AntiKt4LCTopoJetsAuxDyn.JetEMScaleMomentum_m", &AntiKt4LCTopoJetsAuxDyn_JetEMScaleMomentum_m, &b_AntiKt4LCTopoJetsAuxDyn_JetEMScaleMomentum_m);
   fChain->SetBranchAddress("AntiKt4LCTopoJetsAuxDyn.HECQuality", &AntiKt4LCTopoJetsAuxDyn_HECQuality, &b_AntiKt4LCTopoJetsAuxDyn_HECQuality);
   fChain->SetBranchAddress("AntiKt4LCTopoJetsAuxDyn.JetConstitScaleMomentum_pt", &AntiKt4LCTopoJetsAuxDyn_JetConstitScaleMomentum_pt, &b_AntiKt4LCTopoJetsAuxDyn_JetConstitScaleMomentum_pt);
   fChain->SetBranchAddress("AntiKt4LCTopoJetsAuxDyn.JetConstitScaleMomentum_eta", &AntiKt4LCTopoJetsAuxDyn_JetConstitScaleMomentum_eta, &b_AntiKt4LCTopoJetsAuxDyn_JetConstitScaleMomentum_eta);
   fChain->SetBranchAddress("AntiKt4LCTopoJetsAuxDyn.CentroidR", &AntiKt4LCTopoJetsAuxDyn_CentroidR, &b_AntiKt4LCTopoJetsAuxDyn_CentroidR);
   fChain->SetBranchAddress("AntiKt4LCTopoJetsAuxDyn.JetConstitScaleMomentum_phi", &AntiKt4LCTopoJetsAuxDyn_JetConstitScaleMomentum_phi, &b_AntiKt4LCTopoJetsAuxDyn_JetConstitScaleMomentum_phi);
   fChain->SetBranchAddress("AntiKt4LCTopoJetsAuxDyn.AverageLArQF", &AntiKt4LCTopoJetsAuxDyn_AverageLArQF, &b_AntiKt4LCTopoJetsAuxDyn_AverageLArQF);
   fChain->SetBranchAddress("AntiKt4LCTopoJetsAuxDyn.JetConstitScaleMomentum_m", &AntiKt4LCTopoJetsAuxDyn_JetConstitScaleMomentum_m, &b_AntiKt4LCTopoJetsAuxDyn_JetConstitScaleMomentum_m);
   fChain->SetBranchAddress("AntiKt4LCTopoJetsAuxDyn.BchCorrCell", &AntiKt4LCTopoJetsAuxDyn_BchCorrCell, &b_AntiKt4LCTopoJetsAuxDyn_BchCorrCell);
   fChain->SetBranchAddress("AntiKt4LCTopoJetsAuxDyn.InputType", &AntiKt4LCTopoJetsAuxDyn_InputType, &b_AntiKt4LCTopoJetsAuxDyn_InputType);
   fChain->SetBranchAddress("AntiKt4LCTopoJetsAuxDyn.OotFracClusters5", &AntiKt4LCTopoJetsAuxDyn_OotFracClusters5, &b_AntiKt4LCTopoJetsAuxDyn_OotFracClusters5);
   fChain->SetBranchAddress("AntiKt4LCTopoJetsAuxDyn.AlgorithmType", &AntiKt4LCTopoJetsAuxDyn_AlgorithmType, &b_AntiKt4LCTopoJetsAuxDyn_AlgorithmType);
   fChain->SetBranchAddress("AntiKt4LCTopoJetsAuxDyn.OotFracClusters10", &AntiKt4LCTopoJetsAuxDyn_OotFracClusters10, &b_AntiKt4LCTopoJetsAuxDyn_OotFracClusters10);
   fChain->SetBranchAddress("AntiKt4LCTopoJetsAuxDyn.SizeParameter", &AntiKt4LCTopoJetsAuxDyn_SizeParameter, &b_AntiKt4LCTopoJetsAuxDyn_SizeParameter);
   fChain->SetBranchAddress("AntiKt4LCTopoJetsAuxDyn.TrackWidthPt500", &AntiKt4LCTopoJetsAuxDyn_TrackWidthPt500, &b_AntiKt4LCTopoJetsAuxDyn_TrackWidthPt500);
   fChain->SetBranchAddress("AntiKt4LCTopoJetsAuxDyn.NumTrkPt1000", &AntiKt4LCTopoJetsAuxDyn_NumTrkPt1000, &b_AntiKt4LCTopoJetsAuxDyn_NumTrkPt1000);
   fChain->SetBranchAddress("AntiKt4LCTopoJetsAuxDyn.SumPtTrkPt1000", &AntiKt4LCTopoJetsAuxDyn_SumPtTrkPt1000, &b_AntiKt4LCTopoJetsAuxDyn_SumPtTrkPt1000);
   fChain->SetBranchAddress("AntiKt4LCTopoJetsAuxDyn.TrackWidthPt1000", &AntiKt4LCTopoJetsAuxDyn_TrackWidthPt1000, &b_AntiKt4LCTopoJetsAuxDyn_TrackWidthPt1000);
   fChain->SetBranchAddress("AntiKt4LCTopoJetsAuxDyn.TrackSumPt", &AntiKt4LCTopoJetsAuxDyn_TrackSumPt, &b_AntiKt4LCTopoJetsAuxDyn_TrackSumPt);
   fChain->SetBranchAddress("AntiKt4LCTopoJetsAuxDyn.TrackSumMass", &AntiKt4LCTopoJetsAuxDyn_TrackSumMass, &b_AntiKt4LCTopoJetsAuxDyn_TrackSumMass);
   fChain->SetBranchAddress("AntiKt4LCTopoJetsAuxDyn.JVF", &AntiKt4LCTopoJetsAuxDyn_JVF, &b_AntiKt4LCTopoJetsAuxDyn_JVF);
   fChain->SetBranchAddress("AntiKt4LCTopoJetsAuxDyn.HighestJVFVtx", &AntiKt4LCTopoJetsAuxDyn_HighestJVFVtx_, &b_AntiKt4LCTopoJetsAuxDyn_HighestJVFVtx_);
   fChain->SetBranchAddress("AntiKt4LCTopoJetsAuxDyn.HighestJVFVtx.m_persKey", AntiKt4LCTopoJetsAuxDyn_HighestJVFVtx_m_persKey, &b_AntiKt4LCTopoJetsAuxDyn_HighestJVFVtx_m_persKey);
   fChain->SetBranchAddress("AntiKt4LCTopoJetsAuxDyn.HighestJVFVtx.m_persIndex", AntiKt4LCTopoJetsAuxDyn_HighestJVFVtx_m_persIndex, &b_AntiKt4LCTopoJetsAuxDyn_HighestJVFVtx_m_persIndex);
   fChain->SetBranchAddress("AntiKt4LCTopoJetsAuxDyn.JVFCorr", &AntiKt4LCTopoJetsAuxDyn_JVFCorr, &b_AntiKt4LCTopoJetsAuxDyn_JVFCorr);
   fChain->SetBranchAddress("AntiKt4LCTopoJetsAuxDyn.JvtRpt", &AntiKt4LCTopoJetsAuxDyn_JvtRpt, &b_AntiKt4LCTopoJetsAuxDyn_JvtRpt);
   fChain->SetBranchAddress("AntiKt4LCTopoJetsAuxDyn.Jvt", &AntiKt4LCTopoJetsAuxDyn_Jvt, &b_AntiKt4LCTopoJetsAuxDyn_Jvt);
   fChain->SetBranchAddress("AntiKt4LCTopoJetsAuxDyn.Charge", &AntiKt4LCTopoJetsAuxDyn_Charge, &b_AntiKt4LCTopoJetsAuxDyn_Charge);
   fChain->SetBranchAddress("AntiKt4LCTopoJetsAuxDyn.OriginVertex", &AntiKt4LCTopoJetsAuxDyn_OriginVertex_, &b_AntiKt4LCTopoJetsAuxDyn_OriginVertex_);
   fChain->SetBranchAddress("AntiKt4LCTopoJetsAuxDyn.OriginVertex.m_persKey", AntiKt4LCTopoJetsAuxDyn_OriginVertex_m_persKey, &b_AntiKt4LCTopoJetsAuxDyn_OriginVertex_m_persKey);
   fChain->SetBranchAddress("AntiKt4LCTopoJetsAuxDyn.OriginVertex.m_persIndex", AntiKt4LCTopoJetsAuxDyn_OriginVertex_m_persIndex, &b_AntiKt4LCTopoJetsAuxDyn_OriginVertex_m_persIndex);
   fChain->SetBranchAddress("AntiKt4LCTopoJetsAuxDyn.ConeExclBHadronsFinal", &AntiKt4LCTopoJetsAuxDyn_ConeExclBHadronsFinal, &b_AntiKt4LCTopoJetsAuxDyn_ConeExclBHadronsFinal);
   fChain->SetBranchAddress("AntiKt4LCTopoJetsAuxDyn.ConeExclCHadronsFinal", &AntiKt4LCTopoJetsAuxDyn_ConeExclCHadronsFinal, &b_AntiKt4LCTopoJetsAuxDyn_ConeExclCHadronsFinal);
   fChain->SetBranchAddress("AntiKt4LCTopoJetsAuxDyn.ConeExclTausFinal", &AntiKt4LCTopoJetsAuxDyn_ConeExclTausFinal, &b_AntiKt4LCTopoJetsAuxDyn_ConeExclTausFinal);
   fChain->SetBranchAddress("AntiKt4LCTopoJetsAuxDyn.HadronConeExclTruthLabelID", &AntiKt4LCTopoJetsAuxDyn_HadronConeExclTruthLabelID, &b_AntiKt4LCTopoJetsAuxDyn_HadronConeExclTruthLabelID);
   fChain->SetBranchAddress("AntiKt4LCTopoJetsAuxDyn.HadronConeExclExtendedTruthLabelID", &AntiKt4LCTopoJetsAuxDyn_HadronConeExclExtendedTruthLabelID, &b_AntiKt4LCTopoJetsAuxDyn_HadronConeExclExtendedTruthLabelID);
   fChain->SetBranchAddress("AntiKt4LCTopoJetsAuxDyn.LeadingClusterPt", &AntiKt4LCTopoJetsAuxDyn_LeadingClusterPt, &b_AntiKt4LCTopoJetsAuxDyn_LeadingClusterPt);
   fChain->SetBranchAddress("AntiKt4LCTopoJetsAuxDyn.LeadingClusterSecondLambda", &AntiKt4LCTopoJetsAuxDyn_LeadingClusterSecondLambda, &b_AntiKt4LCTopoJetsAuxDyn_LeadingClusterSecondLambda);
   fChain->SetBranchAddress("BTagging_AntiKt4EMTopoAuxDyn.BTagTrackToJetAssociator", &BTagging_AntiKt4EMTopoAuxDyn_BTagTrackToJetAssociator, &b_BTagging_AntiKt4EMTopoAuxDyn_BTagTrackToJetAssociator);
   fChain->SetBranchAddress("BTagging_AntiKt4EMTopoAuxDyn.BTagTrackToJetAssociatorBB", &BTagging_AntiKt4EMTopoAuxDyn_BTagTrackToJetAssociatorBB, &b_BTagging_AntiKt4EMTopoAuxDyn_BTagTrackToJetAssociatorBB);
   fChain->SetBranchAddress("BTagging_AntiKt4EMTopoAuxDyn.Muons", &BTagging_AntiKt4EMTopoAuxDyn_Muons, &b_BTagging_AntiKt4EMTopoAuxDyn_Muons);
   fChain->SetBranchAddress("BTagging_AntiKt4EMTopoAuxDyn.SV1_vertices", &BTagging_AntiKt4EMTopoAuxDyn_SV1_vertices, &b_BTagging_AntiKt4EMTopoAuxDyn_SV1_vertices);
   fChain->SetBranchAddress("BTagging_AntiKt4EMTopoAuxDyn.SV1_energyTrkInJet", &BTagging_AntiKt4EMTopoAuxDyn_SV1_energyTrkInJet, &b_BTagging_AntiKt4EMTopoAuxDyn_SV1_energyTrkInJet);
   fChain->SetBranchAddress("BTagging_AntiKt4EMTopoAuxDyn.SV1_dstToMatLay", &BTagging_AntiKt4EMTopoAuxDyn_SV1_dstToMatLay, &b_BTagging_AntiKt4EMTopoAuxDyn_SV1_dstToMatLay);
   fChain->SetBranchAddress("BTagging_AntiKt4EMTopoAuxDyn.SV1_masssvx", &BTagging_AntiKt4EMTopoAuxDyn_SV1_masssvx, &b_BTagging_AntiKt4EMTopoAuxDyn_SV1_masssvx);
   fChain->SetBranchAddress("BTagging_AntiKt4EMTopoAuxDyn.SV1_efracsvx", &BTagging_AntiKt4EMTopoAuxDyn_SV1_efracsvx, &b_BTagging_AntiKt4EMTopoAuxDyn_SV1_efracsvx);
   fChain->SetBranchAddress("BTagging_AntiKt4EMTopoAuxDyn.SV1_N2Tpair", &BTagging_AntiKt4EMTopoAuxDyn_SV1_N2Tpair, &b_BTagging_AntiKt4EMTopoAuxDyn_SV1_N2Tpair);
   fChain->SetBranchAddress("BTagging_AntiKt4EMTopoAuxDyn.SV1_NGTinSvx", &BTagging_AntiKt4EMTopoAuxDyn_SV1_NGTinSvx, &b_BTagging_AntiKt4EMTopoAuxDyn_SV1_NGTinSvx);
   fChain->SetBranchAddress("BTagging_AntiKt4EMTopoAuxDyn.SV1_badTracksIP", &BTagging_AntiKt4EMTopoAuxDyn_SV1_badTracksIP, &b_BTagging_AntiKt4EMTopoAuxDyn_SV1_badTracksIP);
   fChain->SetBranchAddress("BTagging_AntiKt4EMTopoAuxDyn.SV1_TrackParticleLinks", &BTagging_AntiKt4EMTopoAuxDyn_SV1_TrackParticleLinks, &b_BTagging_AntiKt4EMTopoAuxDyn_SV1_TrackParticleLinks);
   fChain->SetBranchAddress("BTagging_AntiKt4EMTopoAuxDyn.MSV_N2Tpair", &BTagging_AntiKt4EMTopoAuxDyn_MSV_N2Tpair, &b_BTagging_AntiKt4EMTopoAuxDyn_MSV_N2Tpair);
   fChain->SetBranchAddress("BTagging_AntiKt4EMTopoAuxDyn.MSV_energyTrkInJet", &BTagging_AntiKt4EMTopoAuxDyn_MSV_energyTrkInJet, &b_BTagging_AntiKt4EMTopoAuxDyn_MSV_energyTrkInJet);
   fChain->SetBranchAddress("BTagging_AntiKt4EMTopoAuxDyn.IP2D_TrackParticleLinks", &BTagging_AntiKt4EMTopoAuxDyn_IP2D_TrackParticleLinks, &b_BTagging_AntiKt4EMTopoAuxDyn_IP2D_TrackParticleLinks);
   fChain->SetBranchAddress("BTagging_AntiKt4EMTopoAuxDyn.MSV_nvsec", &BTagging_AntiKt4EMTopoAuxDyn_MSV_nvsec, &b_BTagging_AntiKt4EMTopoAuxDyn_MSV_nvsec);
   fChain->SetBranchAddress("BTagging_AntiKt4EMTopoAuxDyn.MSV_normdist", &BTagging_AntiKt4EMTopoAuxDyn_MSV_normdist, &b_BTagging_AntiKt4EMTopoAuxDyn_MSV_normdist);
   fChain->SetBranchAddress("BTagging_AntiKt4EMTopoAuxDyn.IP3D_TrackParticleLinks", &BTagging_AntiKt4EMTopoAuxDyn_IP3D_TrackParticleLinks, &b_BTagging_AntiKt4EMTopoAuxDyn_IP3D_TrackParticleLinks);
   fChain->SetBranchAddress("BTagging_AntiKt4EMTopoAuxDyn.MSV_vertices", &BTagging_AntiKt4EMTopoAuxDyn_MSV_vertices, &b_BTagging_AntiKt4EMTopoAuxDyn_MSV_vertices);
   fChain->SetBranchAddress("BTagging_AntiKt4EMTopoAuxDyn.MSV_badTracksIP", &BTagging_AntiKt4EMTopoAuxDyn_MSV_badTracksIP, &b_BTagging_AntiKt4EMTopoAuxDyn_MSV_badTracksIP);
   fChain->SetBranchAddress("BTagging_AntiKt4EMTopoAuxDyn.JetFitter_N2Tpair", &BTagging_AntiKt4EMTopoAuxDyn_JetFitter_N2Tpair, &b_BTagging_AntiKt4EMTopoAuxDyn_JetFitter_N2Tpair);
   fChain->SetBranchAddress("BTagging_AntiKt4EMTopoAuxDyn.JetFitter_JFvertices", &BTagging_AntiKt4EMTopoAuxDyn_JetFitter_JFvertices, &b_BTagging_AntiKt4EMTopoAuxDyn_JetFitter_JFvertices);
   fChain->SetBranchAddress("BTagging_AntiKt4EMTopoAuxDyn.JetFitter_fittedPosition", &BTagging_AntiKt4EMTopoAuxDyn_JetFitter_fittedPosition, &b_BTagging_AntiKt4EMTopoAuxDyn_JetFitter_fittedPosition);
   fChain->SetBranchAddress("BTagging_AntiKt4EMTopoAuxDyn.JetFitter_fittedCov", &BTagging_AntiKt4EMTopoAuxDyn_JetFitter_fittedCov, &b_BTagging_AntiKt4EMTopoAuxDyn_JetFitter_fittedCov);
   fChain->SetBranchAddress("BTagging_AntiKt4EMTopoAuxDyn.JetFitter_tracksAtPVchi2", &BTagging_AntiKt4EMTopoAuxDyn_JetFitter_tracksAtPVchi2, &b_BTagging_AntiKt4EMTopoAuxDyn_JetFitter_tracksAtPVchi2);
   fChain->SetBranchAddress("BTagging_AntiKt4EMTopoAuxDyn.JetFitter_tracksAtPVndf", &BTagging_AntiKt4EMTopoAuxDyn_JetFitter_tracksAtPVndf, &b_BTagging_AntiKt4EMTopoAuxDyn_JetFitter_tracksAtPVndf);
   fChain->SetBranchAddress("BTagging_AntiKt4EMTopoAuxDyn.JetFitter_tracksAtPVlinks", &BTagging_AntiKt4EMTopoAuxDyn_JetFitter_tracksAtPVlinks, &b_BTagging_AntiKt4EMTopoAuxDyn_JetFitter_tracksAtPVlinks);
   fChain->SetBranchAddress("BTagging_AntiKt4EMTopoAuxDyn.JetFitter_massUncorr", &BTagging_AntiKt4EMTopoAuxDyn_JetFitter_massUncorr, &b_BTagging_AntiKt4EMTopoAuxDyn_JetFitter_massUncorr);
   fChain->SetBranchAddress("BTagging_AntiKt4EMTopoAuxDyn.JetFitter_chi2", &BTagging_AntiKt4EMTopoAuxDyn_JetFitter_chi2, &b_BTagging_AntiKt4EMTopoAuxDyn_JetFitter_chi2);
   fChain->SetBranchAddress("BTagging_AntiKt4EMTopoAuxDyn.JetFitter_ndof", &BTagging_AntiKt4EMTopoAuxDyn_JetFitter_ndof, &b_BTagging_AntiKt4EMTopoAuxDyn_JetFitter_ndof);
   fChain->SetBranchAddress("BTagging_AntiKt4EMTopoAuxDyn.JetFitter_dRFlightDir", &BTagging_AntiKt4EMTopoAuxDyn_JetFitter_dRFlightDir, &b_BTagging_AntiKt4EMTopoAuxDyn_JetFitter_dRFlightDir);
   fChain->SetBranchAddress("BTagging_AntiKt4EMTopoAuxDyn.JetFitter_nVTX", &BTagging_AntiKt4EMTopoAuxDyn_JetFitter_nVTX, &b_BTagging_AntiKt4EMTopoAuxDyn_JetFitter_nVTX);
   fChain->SetBranchAddress("BTagging_AntiKt4EMTopoAuxDyn.JetFitter_nSingleTracks", &BTagging_AntiKt4EMTopoAuxDyn_JetFitter_nSingleTracks, &b_BTagging_AntiKt4EMTopoAuxDyn_JetFitter_nSingleTracks);
   fChain->SetBranchAddress("BTagging_AntiKt4EMTopoAuxDyn.JetFitter_nTracksAtVtx", &BTagging_AntiKt4EMTopoAuxDyn_JetFitter_nTracksAtVtx, &b_BTagging_AntiKt4EMTopoAuxDyn_JetFitter_nTracksAtVtx);
   fChain->SetBranchAddress("BTagging_AntiKt4EMTopoAuxDyn.JetFitter_mass", &BTagging_AntiKt4EMTopoAuxDyn_JetFitter_mass, &b_BTagging_AntiKt4EMTopoAuxDyn_JetFitter_mass);
   fChain->SetBranchAddress("BTagging_AntiKt4EMTopoAuxDyn.JetFitter_energyFraction", &BTagging_AntiKt4EMTopoAuxDyn_JetFitter_energyFraction, &b_BTagging_AntiKt4EMTopoAuxDyn_JetFitter_energyFraction);
   fChain->SetBranchAddress("BTagging_AntiKt4EMTopoAuxDyn.JetFitter_significance3d", &BTagging_AntiKt4EMTopoAuxDyn_JetFitter_significance3d, &b_BTagging_AntiKt4EMTopoAuxDyn_JetFitter_significance3d);
   fChain->SetBranchAddress("BTagging_AntiKt4EMTopoAuxDyn.JetFitter_deltaeta", &BTagging_AntiKt4EMTopoAuxDyn_JetFitter_deltaeta, &b_BTagging_AntiKt4EMTopoAuxDyn_JetFitter_deltaeta);
   fChain->SetBranchAddress("BTagging_AntiKt4EMTopoAuxDyn.JetFitter_deltaphi", &BTagging_AntiKt4EMTopoAuxDyn_JetFitter_deltaphi, &b_BTagging_AntiKt4EMTopoAuxDyn_JetFitter_deltaphi);
   fChain->SetBranchAddress("BTagging_AntiKt4EMTopoAuxDyn.IP2D_sigD0wrtPVofTracks", &BTagging_AntiKt4EMTopoAuxDyn_IP2D_sigD0wrtPVofTracks, &b_BTagging_AntiKt4EMTopoAuxDyn_IP2D_sigD0wrtPVofTracks);
   fChain->SetBranchAddress("BTagging_AntiKt4EMTopoAuxDyn.IP2D_weightBofTracks", &BTagging_AntiKt4EMTopoAuxDyn_IP2D_weightBofTracks, &b_BTagging_AntiKt4EMTopoAuxDyn_IP2D_weightBofTracks);
   fChain->SetBranchAddress("BTagging_AntiKt4EMTopoAuxDyn.IP2D_weightUofTracks", &BTagging_AntiKt4EMTopoAuxDyn_IP2D_weightUofTracks, &b_BTagging_AntiKt4EMTopoAuxDyn_IP2D_weightUofTracks);
   fChain->SetBranchAddress("BTagging_AntiKt4EMTopoAuxDyn.IP2D_weightCofTracks", &BTagging_AntiKt4EMTopoAuxDyn_IP2D_weightCofTracks, &b_BTagging_AntiKt4EMTopoAuxDyn_IP2D_weightCofTracks);
   fChain->SetBranchAddress("BTagging_AntiKt4EMTopoAuxDyn.IP2D_flagFromV0ofTracks", &BTagging_AntiKt4EMTopoAuxDyn_IP2D_flagFromV0ofTracks, &b_BTagging_AntiKt4EMTopoAuxDyn_IP2D_flagFromV0ofTracks);
   fChain->SetBranchAddress("BTagging_AntiKt4EMTopoAuxDyn.IP2D_gradeOfTracks", &BTagging_AntiKt4EMTopoAuxDyn_IP2D_gradeOfTracks, &b_BTagging_AntiKt4EMTopoAuxDyn_IP2D_gradeOfTracks);
   fChain->SetBranchAddress("BTagging_AntiKt4EMTopoAuxDyn.trkSum_ntrk", &BTagging_AntiKt4EMTopoAuxDyn_trkSum_ntrk, &b_BTagging_AntiKt4EMTopoAuxDyn_trkSum_ntrk);
   fChain->SetBranchAddress("BTagging_AntiKt4EMTopoAuxDyn.trkSum_SPt", &BTagging_AntiKt4EMTopoAuxDyn_trkSum_SPt, &b_BTagging_AntiKt4EMTopoAuxDyn_trkSum_SPt);
   fChain->SetBranchAddress("BTagging_AntiKt4EMTopoAuxDyn.trkSum_VPt", &BTagging_AntiKt4EMTopoAuxDyn_trkSum_VPt, &b_BTagging_AntiKt4EMTopoAuxDyn_trkSum_VPt);
   fChain->SetBranchAddress("BTagging_AntiKt4EMTopoAuxDyn.trkSum_VEta", &BTagging_AntiKt4EMTopoAuxDyn_trkSum_VEta, &b_BTagging_AntiKt4EMTopoAuxDyn_trkSum_VEta);
   fChain->SetBranchAddress("BTagging_AntiKt4EMTopoAuxDyn.IP3D_valD0wrtPVofTracks", &BTagging_AntiKt4EMTopoAuxDyn_IP3D_valD0wrtPVofTracks, &b_BTagging_AntiKt4EMTopoAuxDyn_IP3D_valD0wrtPVofTracks);
   fChain->SetBranchAddress("BTagging_AntiKt4EMTopoAuxDyn.IP3D_valZ0wrtPVofTracks", &BTagging_AntiKt4EMTopoAuxDyn_IP3D_valZ0wrtPVofTracks, &b_BTagging_AntiKt4EMTopoAuxDyn_IP3D_valZ0wrtPVofTracks);
   fChain->SetBranchAddress("BTagging_AntiKt4EMTopoAuxDyn.IP3D_sigD0wrtPVofTracks", &BTagging_AntiKt4EMTopoAuxDyn_IP3D_sigD0wrtPVofTracks, &b_BTagging_AntiKt4EMTopoAuxDyn_IP3D_sigD0wrtPVofTracks);
   fChain->SetBranchAddress("BTagging_AntiKt4EMTopoAuxDyn.IP3D_sigZ0wrtPVofTracks", &BTagging_AntiKt4EMTopoAuxDyn_IP3D_sigZ0wrtPVofTracks, &b_BTagging_AntiKt4EMTopoAuxDyn_IP3D_sigZ0wrtPVofTracks);
   fChain->SetBranchAddress("BTagging_AntiKt4EMTopoAuxDyn.IP3D_weightBofTracks", &BTagging_AntiKt4EMTopoAuxDyn_IP3D_weightBofTracks, &b_BTagging_AntiKt4EMTopoAuxDyn_IP3D_weightBofTracks);
   fChain->SetBranchAddress("BTagging_AntiKt4EMTopoAuxDyn.IP3D_weightUofTracks", &BTagging_AntiKt4EMTopoAuxDyn_IP3D_weightUofTracks, &b_BTagging_AntiKt4EMTopoAuxDyn_IP3D_weightUofTracks);
   fChain->SetBranchAddress("BTagging_AntiKt4EMTopoAuxDyn.IP3D_weightCofTracks", &BTagging_AntiKt4EMTopoAuxDyn_IP3D_weightCofTracks, &b_BTagging_AntiKt4EMTopoAuxDyn_IP3D_weightCofTracks);
   fChain->SetBranchAddress("BTagging_AntiKt4EMTopoAuxDyn.IP3D_flagFromV0ofTracks", &BTagging_AntiKt4EMTopoAuxDyn_IP3D_flagFromV0ofTracks, &b_BTagging_AntiKt4EMTopoAuxDyn_IP3D_flagFromV0ofTracks);
   fChain->SetBranchAddress("BTagging_AntiKt4EMTopoAuxDyn.IP3D_gradeOfTracks", &BTagging_AntiKt4EMTopoAuxDyn_IP3D_gradeOfTracks, &b_BTagging_AntiKt4EMTopoAuxDyn_IP3D_gradeOfTracks);
   fChain->SetBranchAddress("BTagging_AntiKt4EMTopoAuxDyn.rnnip_pb", &BTagging_AntiKt4EMTopoAuxDyn_rnnip_pb, &b_BTagging_AntiKt4EMTopoAuxDyn_rnnip_pb);
   fChain->SetBranchAddress("BTagging_AntiKt4EMTopoAuxDyn.rnnip_pbIsValid", &BTagging_AntiKt4EMTopoAuxDyn_rnnip_pbIsValid, &b_BTagging_AntiKt4EMTopoAuxDyn_rnnip_pbIsValid);
   fChain->SetBranchAddress("BTagging_AntiKt4EMTopoAuxDyn.rnnip_pc", &BTagging_AntiKt4EMTopoAuxDyn_rnnip_pc, &b_BTagging_AntiKt4EMTopoAuxDyn_rnnip_pc);
   fChain->SetBranchAddress("BTagging_AntiKt4EMTopoAuxDyn.rnnip_pcIsValid", &BTagging_AntiKt4EMTopoAuxDyn_rnnip_pcIsValid, &b_BTagging_AntiKt4EMTopoAuxDyn_rnnip_pcIsValid);
   fChain->SetBranchAddress("BTagging_AntiKt4EMTopoAuxDyn.rnnip_pu", &BTagging_AntiKt4EMTopoAuxDyn_rnnip_pu, &b_BTagging_AntiKt4EMTopoAuxDyn_rnnip_pu);
   fChain->SetBranchAddress("BTagging_AntiKt4EMTopoAuxDyn.rnnip_puIsValid", &BTagging_AntiKt4EMTopoAuxDyn_rnnip_puIsValid, &b_BTagging_AntiKt4EMTopoAuxDyn_rnnip_puIsValid);
   fChain->SetBranchAddress("BTagging_AntiKt4EMTopoAuxDyn.rnnip_ptau", &BTagging_AntiKt4EMTopoAuxDyn_rnnip_ptau, &b_BTagging_AntiKt4EMTopoAuxDyn_rnnip_ptau);
   fChain->SetBranchAddress("BTagging_AntiKt4EMTopoAuxDyn.rnnip_ptauIsValid", &BTagging_AntiKt4EMTopoAuxDyn_rnnip_ptauIsValid, &b_BTagging_AntiKt4EMTopoAuxDyn_rnnip_ptauIsValid);
   fChain->SetBranchAddress("BTagging_AntiKt4EMTopoAuxDyn.SV1_normdist", &BTagging_AntiKt4EMTopoAuxDyn_SV1_normdist, &b_BTagging_AntiKt4EMTopoAuxDyn_SV1_normdist);
   fChain->SetBranchAddress("BTagging_AntiKt4EMTopoAuxDyn.SV1_significance3d", &BTagging_AntiKt4EMTopoAuxDyn_SV1_significance3d, &b_BTagging_AntiKt4EMTopoAuxDyn_SV1_significance3d);
   fChain->SetBranchAddress("BTagging_AntiKt4EMTopoAuxDyn.SV1_deltaR", &BTagging_AntiKt4EMTopoAuxDyn_SV1_deltaR, &b_BTagging_AntiKt4EMTopoAuxDyn_SV1_deltaR);
   fChain->SetBranchAddress("BTagging_AntiKt4EMTopoAuxDyn.SV1_Lxy", &BTagging_AntiKt4EMTopoAuxDyn_SV1_Lxy, &b_BTagging_AntiKt4EMTopoAuxDyn_SV1_Lxy);
   fChain->SetBranchAddress("BTagging_AntiKt4EMTopoAuxDyn.SV1_L3d", &BTagging_AntiKt4EMTopoAuxDyn_SV1_L3d, &b_BTagging_AntiKt4EMTopoAuxDyn_SV1_L3d);
   fChain->SetBranchAddress("BTagging_AntiKt4EMTopoAuxDyn.MultiSVbb1_discriminant", &BTagging_AntiKt4EMTopoAuxDyn_MultiSVbb1_discriminant, &b_BTagging_AntiKt4EMTopoAuxDyn_MultiSVbb1_discriminant);
   fChain->SetBranchAddress("BTagging_AntiKt4EMTopoAuxDyn.MultiSVbb2_discriminant", &BTagging_AntiKt4EMTopoAuxDyn_MultiSVbb2_discriminant, &b_BTagging_AntiKt4EMTopoAuxDyn_MultiSVbb2_discriminant);
   fChain->SetBranchAddress("BTagging_AntiKt4EMTopoAuxDyn.SMT_mu_pt", &BTagging_AntiKt4EMTopoAuxDyn_SMT_mu_pt, &b_BTagging_AntiKt4EMTopoAuxDyn_SMT_mu_pt);
   fChain->SetBranchAddress("BTagging_AntiKt4EMTopoAuxDyn.SMT_dR", &BTagging_AntiKt4EMTopoAuxDyn_SMT_dR, &b_BTagging_AntiKt4EMTopoAuxDyn_SMT_dR);
   fChain->SetBranchAddress("BTagging_AntiKt4EMTopoAuxDyn.SMT_qOverPratio", &BTagging_AntiKt4EMTopoAuxDyn_SMT_qOverPratio, &b_BTagging_AntiKt4EMTopoAuxDyn_SMT_qOverPratio);
   fChain->SetBranchAddress("BTagging_AntiKt4EMTopoAuxDyn.SMT_mombalsignif", &BTagging_AntiKt4EMTopoAuxDyn_SMT_mombalsignif, &b_BTagging_AntiKt4EMTopoAuxDyn_SMT_mombalsignif);
   fChain->SetBranchAddress("BTagging_AntiKt4EMTopoAuxDyn.SMT_scatneighsignif", &BTagging_AntiKt4EMTopoAuxDyn_SMT_scatneighsignif, &b_BTagging_AntiKt4EMTopoAuxDyn_SMT_scatneighsignif);
   fChain->SetBranchAddress("BTagging_AntiKt4EMTopoAuxDyn.SMT_pTrel", &BTagging_AntiKt4EMTopoAuxDyn_SMT_pTrel, &b_BTagging_AntiKt4EMTopoAuxDyn_SMT_pTrel);
   fChain->SetBranchAddress("BTagging_AntiKt4EMTopoAuxDyn.SMT_mu_d0", &BTagging_AntiKt4EMTopoAuxDyn_SMT_mu_d0, &b_BTagging_AntiKt4EMTopoAuxDyn_SMT_mu_d0);
   fChain->SetBranchAddress("BTagging_AntiKt4EMTopoAuxDyn.SMT_mu_z0", &BTagging_AntiKt4EMTopoAuxDyn_SMT_mu_z0, &b_BTagging_AntiKt4EMTopoAuxDyn_SMT_mu_z0);
   fChain->SetBranchAddress("BTagging_AntiKt4EMTopoAuxDyn.SMT_ID_qOverP", &BTagging_AntiKt4EMTopoAuxDyn_SMT_ID_qOverP, &b_BTagging_AntiKt4EMTopoAuxDyn_SMT_ID_qOverP);
   fChain->SetBranchAddress("BTagging_AntiKt4EMTopoAuxDyn.SMT_mu_link", &BTagging_AntiKt4EMTopoAuxDyn_SMT_mu_link_, &b_BTagging_AntiKt4EMTopoAuxDyn_SMT_mu_link_);
   fChain->SetBranchAddress("BTagging_AntiKt4EMTopoAuxDyn.SMT_mu_link.m_persKey", BTagging_AntiKt4EMTopoAuxDyn_SMT_mu_link_m_persKey, &b_BTagging_AntiKt4EMTopoAuxDyn_SMT_mu_link_m_persKey);
   fChain->SetBranchAddress("BTagging_AntiKt4EMTopoAuxDyn.SMT_mu_link.m_persIndex", BTagging_AntiKt4EMTopoAuxDyn_SMT_mu_link_m_persIndex, &b_BTagging_AntiKt4EMTopoAuxDyn_SMT_mu_link_m_persIndex);
   fChain->SetBranchAddress("BTagging_AntiKt4EMTopoAuxDyn.SMT_discriminant", &BTagging_AntiKt4EMTopoAuxDyn_SMT_discriminant, &b_BTagging_AntiKt4EMTopoAuxDyn_SMT_discriminant);
   fChain->SetBranchAddress("BTagging_AntiKt4EMTopoAuxDyn.SMT_discriminantIsValid", &BTagging_AntiKt4EMTopoAuxDyn_SMT_discriminantIsValid, &b_BTagging_AntiKt4EMTopoAuxDyn_SMT_discriminantIsValid);
   fChain->SetBranchAddress("BTagging_AntiKt4EMTopoAuxDyn.JetVertexCharge_discriminant", &BTagging_AntiKt4EMTopoAuxDyn_JetVertexCharge_discriminant, &b_BTagging_AntiKt4EMTopoAuxDyn_JetVertexCharge_discriminant);
   fChain->SetBranchAddress("BTagging_AntiKt4EMTopoAuxDyn.MV2c10_discriminant", &BTagging_AntiKt4EMTopoAuxDyn_MV2c10_discriminant, &b_BTagging_AntiKt4EMTopoAuxDyn_MV2c10_discriminant);
   fChain->SetBranchAddress("BTagging_AntiKt4EMTopoAuxDyn.DL1mu_pc", &BTagging_AntiKt4EMTopoAuxDyn_DL1mu_pc, &b_BTagging_AntiKt4EMTopoAuxDyn_DL1mu_pc);
   fChain->SetBranchAddress("BTagging_AntiKt4EMTopoAuxDyn.DL1mu_pu", &BTagging_AntiKt4EMTopoAuxDyn_DL1mu_pu, &b_BTagging_AntiKt4EMTopoAuxDyn_DL1mu_pu);
   fChain->SetBranchAddress("BTagging_AntiKt4EMTopoAuxDyn.DL1mu_pb", &BTagging_AntiKt4EMTopoAuxDyn_DL1mu_pb, &b_BTagging_AntiKt4EMTopoAuxDyn_DL1mu_pb);
   fChain->SetBranchAddress("BTagging_AntiKt4EMTopoAuxDyn.DL1rnn_pc", &BTagging_AntiKt4EMTopoAuxDyn_DL1rnn_pc, &b_BTagging_AntiKt4EMTopoAuxDyn_DL1rnn_pc);
   fChain->SetBranchAddress("BTagging_AntiKt4EMTopoAuxDyn.DL1rnn_pu", &BTagging_AntiKt4EMTopoAuxDyn_DL1rnn_pu, &b_BTagging_AntiKt4EMTopoAuxDyn_DL1rnn_pu);
   fChain->SetBranchAddress("BTagging_AntiKt4EMTopoAuxDyn.DL1rnn_pb", &BTagging_AntiKt4EMTopoAuxDyn_DL1rnn_pb, &b_BTagging_AntiKt4EMTopoAuxDyn_DL1rnn_pb);
   fChain->SetBranchAddress("BTagging_AntiKt4EMTopoAuxDyn.MV2c10mu_discriminant", &BTagging_AntiKt4EMTopoAuxDyn_MV2c10mu_discriminant, &b_BTagging_AntiKt4EMTopoAuxDyn_MV2c10mu_discriminant);
   fChain->SetBranchAddress("BTagging_AntiKt4EMTopoAuxDyn.DL1_pc", &BTagging_AntiKt4EMTopoAuxDyn_DL1_pc, &b_BTagging_AntiKt4EMTopoAuxDyn_DL1_pc);
   fChain->SetBranchAddress("BTagging_AntiKt4EMTopoAuxDyn.DL1_pu", &BTagging_AntiKt4EMTopoAuxDyn_DL1_pu, &b_BTagging_AntiKt4EMTopoAuxDyn_DL1_pu);
   fChain->SetBranchAddress("BTagging_AntiKt4EMTopoAuxDyn.DL1_pb", &BTagging_AntiKt4EMTopoAuxDyn_DL1_pb, &b_BTagging_AntiKt4EMTopoAuxDyn_DL1_pb);
   fChain->SetBranchAddress("BTagging_AntiKt4EMTopoAuxDyn.MV2cl100_discriminant", &BTagging_AntiKt4EMTopoAuxDyn_MV2cl100_discriminant, &b_BTagging_AntiKt4EMTopoAuxDyn_MV2cl100_discriminant);
   fChain->SetBranchAddress("BTagging_AntiKt4EMTopoAuxDyn.MV2c10rnn_discriminant", &BTagging_AntiKt4EMTopoAuxDyn_MV2c10rnn_discriminant, &b_BTagging_AntiKt4EMTopoAuxDyn_MV2c10rnn_discriminant);
   fChain->SetBranchAddress("BTagging_AntiKt4EMTopoAuxDyn.MV2c100_discriminant", &BTagging_AntiKt4EMTopoAuxDyn_MV2c100_discriminant, &b_BTagging_AntiKt4EMTopoAuxDyn_MV2c100_discriminant);
   fChain->SetBranchAddress("ElectronsAuxDyn.neflowisol40", &ElectronsAuxDyn_neflowisol40, &b_ElectronsAuxDyn_neflowisol40);
   fChain->SetBranchAddress("ElectronsAuxDyn.topoetconeCorrBitset", &ElectronsAuxDyn_topoetconeCorrBitset, &b_ElectronsAuxDyn_topoetconeCorrBitset);
   fChain->SetBranchAddress("ElectronsAuxDyn.topoetconecoreConeEnergyCorrection", &ElectronsAuxDyn_topoetconecoreConeEnergyCorrection, &b_ElectronsAuxDyn_topoetconecoreConeEnergyCorrection);
   fChain->SetBranchAddress("ElectronsAuxDyn.topoetconecoreConeSCEnergyCorrection", &ElectronsAuxDyn_topoetconecoreConeSCEnergyCorrection, &b_ElectronsAuxDyn_topoetconecoreConeSCEnergyCorrection);
   fChain->SetBranchAddress("ElectronsAuxDyn.topoetcone20ptCorrection", &ElectronsAuxDyn_topoetcone20ptCorrection, &b_ElectronsAuxDyn_topoetcone20ptCorrection);
   fChain->SetBranchAddress("ElectronsAuxDyn.topoetcone30ptCorrection", &ElectronsAuxDyn_topoetcone30ptCorrection, &b_ElectronsAuxDyn_topoetcone30ptCorrection);
   fChain->SetBranchAddress("ElectronsAuxDyn.topoetcone40ptCorrection", &ElectronsAuxDyn_topoetcone40ptCorrection, &b_ElectronsAuxDyn_topoetcone40ptCorrection);
   fChain->SetBranchAddress("ElectronsAuxDyn.neflowisolCorrBitset", &ElectronsAuxDyn_neflowisolCorrBitset, &b_ElectronsAuxDyn_neflowisolCorrBitset);
   fChain->SetBranchAddress("ElectronsAuxDyn.neflowisolcoreConeEnergyCorrection", &ElectronsAuxDyn_neflowisolcoreConeEnergyCorrection, &b_ElectronsAuxDyn_neflowisolcoreConeEnergyCorrection);
   fChain->SetBranchAddress("ElectronsAuxDyn.neflowisol20ptCorrection", &ElectronsAuxDyn_neflowisol20ptCorrection, &b_ElectronsAuxDyn_neflowisol20ptCorrection);
   fChain->SetBranchAddress("ElectronsAuxDyn.neflowisol30ptCorrection", &ElectronsAuxDyn_neflowisol30ptCorrection, &b_ElectronsAuxDyn_neflowisol30ptCorrection);
   fChain->SetBranchAddress("ElectronsAuxDyn.neflowisol40ptCorrection", &ElectronsAuxDyn_neflowisol40ptCorrection, &b_ElectronsAuxDyn_neflowisol40ptCorrection);
   fChain->SetBranchAddress("ElectronsAuxDyn.ptconeCorrBitset", &ElectronsAuxDyn_ptconeCorrBitset, &b_ElectronsAuxDyn_ptconeCorrBitset);
   fChain->SetBranchAddress("ElectronsAuxDyn.ptconecoreTrackPtrCorrection", &ElectronsAuxDyn_ptconecoreTrackPtrCorrection, &b_ElectronsAuxDyn_ptconecoreTrackPtrCorrection);
   fChain->SetBranchAddress("ElectronsAuxDyn.e011", &ElectronsAuxDyn_e011, &b_ElectronsAuxDyn_e011);
   fChain->SetBranchAddress("ElectronsAuxDyn.e033", &ElectronsAuxDyn_e033, &b_ElectronsAuxDyn_e033);
   fChain->SetBranchAddress("ElectronsAuxDyn.e132", &ElectronsAuxDyn_e132, &b_ElectronsAuxDyn_e132);
   fChain->SetBranchAddress("ElectronsAuxDyn.e1152", &ElectronsAuxDyn_e1152, &b_ElectronsAuxDyn_e1152);
   fChain->SetBranchAddress("ElectronsAuxDyn.ethad1", &ElectronsAuxDyn_ethad1, &b_ElectronsAuxDyn_ethad1);
   fChain->SetBranchAddress("ElectronsAuxDyn.ethad", &ElectronsAuxDyn_ethad, &b_ElectronsAuxDyn_ethad);
   fChain->SetBranchAddress("ElectronsAuxDyn.e233", &ElectronsAuxDyn_e233, &b_ElectronsAuxDyn_e233);
   fChain->SetBranchAddress("ElectronsAuxDyn.e235", &ElectronsAuxDyn_e235, &b_ElectronsAuxDyn_e235);
   fChain->SetBranchAddress("ElectronsAuxDyn.e255", &ElectronsAuxDyn_e255, &b_ElectronsAuxDyn_e255);
   fChain->SetBranchAddress("ElectronsAuxDyn.e237", &ElectronsAuxDyn_e237, &b_ElectronsAuxDyn_e237);
   fChain->SetBranchAddress("ElectronsAuxDyn.e333", &ElectronsAuxDyn_e333, &b_ElectronsAuxDyn_e333);
   fChain->SetBranchAddress("ElectronsAuxDyn.e335", &ElectronsAuxDyn_e335, &b_ElectronsAuxDyn_e335);
   fChain->SetBranchAddress("ElectronsAuxDyn.e337", &ElectronsAuxDyn_e337, &b_ElectronsAuxDyn_e337);
   fChain->SetBranchAddress("ElectronsAuxDyn.e377", &ElectronsAuxDyn_e377, &b_ElectronsAuxDyn_e377);
   fChain->SetBranchAddress("ElectronsAuxDyn.e2ts1", &ElectronsAuxDyn_e2ts1, &b_ElectronsAuxDyn_e2ts1);
   fChain->SetBranchAddress("ElectronsAuxDyn.e2tsts1", &ElectronsAuxDyn_e2tsts1, &b_ElectronsAuxDyn_e2tsts1);
   fChain->SetBranchAddress("ElectronsAuxDyn.etcone20", &ElectronsAuxDyn_etcone20, &b_ElectronsAuxDyn_etcone20);
   fChain->SetBranchAddress("ElectronsAuxDyn.widths1", &ElectronsAuxDyn_widths1, &b_ElectronsAuxDyn_widths1);
   fChain->SetBranchAddress("ElectronsAuxDyn.etcone30", &ElectronsAuxDyn_etcone30, &b_ElectronsAuxDyn_etcone30);
   fChain->SetBranchAddress("ElectronsAuxDyn.widths2", &ElectronsAuxDyn_widths2, &b_ElectronsAuxDyn_widths2);
   fChain->SetBranchAddress("ElectronsAuxDyn.etcone40", &ElectronsAuxDyn_etcone40, &b_ElectronsAuxDyn_etcone40);
   fChain->SetBranchAddress("ElectronsAuxDyn.poscs1", &ElectronsAuxDyn_poscs1, &b_ElectronsAuxDyn_poscs1);
   fChain->SetBranchAddress("ElectronsAuxDyn.ptcone20", &ElectronsAuxDyn_ptcone20, &b_ElectronsAuxDyn_ptcone20);
   fChain->SetBranchAddress("ElectronsAuxDyn.poscs2", &ElectronsAuxDyn_poscs2, &b_ElectronsAuxDyn_poscs2);
   fChain->SetBranchAddress("ElectronsAuxDyn.ptcone30", &ElectronsAuxDyn_ptcone30, &b_ElectronsAuxDyn_ptcone30);
   fChain->SetBranchAddress("ElectronsAuxDyn.asy1", &ElectronsAuxDyn_asy1, &b_ElectronsAuxDyn_asy1);
   fChain->SetBranchAddress("ElectronsAuxDyn.ptcone40", &ElectronsAuxDyn_ptcone40, &b_ElectronsAuxDyn_ptcone40);
   fChain->SetBranchAddress("ElectronsAuxDyn.pos", &ElectronsAuxDyn_pos, &b_ElectronsAuxDyn_pos);
   fChain->SetBranchAddress("ElectronsAuxDyn.ambiguityType", &ElectronsAuxDyn_ambiguityType, &b_ElectronsAuxDyn_ambiguityType);
   fChain->SetBranchAddress("ElectronsAuxDyn.ptvarcone20", &ElectronsAuxDyn_ptvarcone20, &b_ElectronsAuxDyn_ptvarcone20);
   fChain->SetBranchAddress("ElectronsAuxDyn.pos7", &ElectronsAuxDyn_pos7, &b_ElectronsAuxDyn_pos7);
   fChain->SetBranchAddress("ElectronsAuxDyn.EgammaCovarianceMatrix", &ElectronsAuxDyn_EgammaCovarianceMatrix, &b_ElectronsAuxDyn_EgammaCovarianceMatrix);
   fChain->SetBranchAddress("ElectronsAuxDyn.ptvarcone30", &ElectronsAuxDyn_ptvarcone30, &b_ElectronsAuxDyn_ptvarcone30);
   fChain->SetBranchAddress("ElectronsAuxDyn.barys1", &ElectronsAuxDyn_barys1, &b_ElectronsAuxDyn_barys1);
   fChain->SetBranchAddress("ElectronsAuxDyn.ehad1", &ElectronsAuxDyn_ehad1, &b_ElectronsAuxDyn_ehad1);
   fChain->SetBranchAddress("ElectronsAuxDyn.ptvarcone40", &ElectronsAuxDyn_ptvarcone40, &b_ElectronsAuxDyn_ptvarcone40);
   fChain->SetBranchAddress("ElectronsAuxDyn.caloRingsLinks", &ElectronsAuxDyn_caloRingsLinks, &b_ElectronsAuxDyn_caloRingsLinks);
   fChain->SetBranchAddress("ElectronsAuxDyn.Loose", &ElectronsAuxDyn_Loose, &b_ElectronsAuxDyn_Loose);
   fChain->SetBranchAddress("ElectronsAuxDyn.emins1", &ElectronsAuxDyn_emins1, &b_ElectronsAuxDyn_emins1);
   fChain->SetBranchAddress("ElectronsAuxDyn.isEMLoose", &ElectronsAuxDyn_isEMLoose, &b_ElectronsAuxDyn_isEMLoose);
   fChain->SetBranchAddress("ElectronsAuxDyn.emaxs1", &ElectronsAuxDyn_emaxs1, &b_ElectronsAuxDyn_emaxs1);
   fChain->SetBranchAddress("ElectronsAuxDyn.Medium", &ElectronsAuxDyn_Medium, &b_ElectronsAuxDyn_Medium);
   fChain->SetBranchAddress("ElectronsAuxDyn.r33over37allcalo", &ElectronsAuxDyn_r33over37allcalo, &b_ElectronsAuxDyn_r33over37allcalo);
   fChain->SetBranchAddress("ElectronsAuxDyn.isEMMedium", &ElectronsAuxDyn_isEMMedium, &b_ElectronsAuxDyn_isEMMedium);
   fChain->SetBranchAddress("ElectronsAuxDyn.ecore", &ElectronsAuxDyn_ecore, &b_ElectronsAuxDyn_ecore);
   fChain->SetBranchAddress("ElectronsAuxDyn.Tight", &ElectronsAuxDyn_Tight, &b_ElectronsAuxDyn_Tight);
   fChain->SetBranchAddress("ElectronsAuxDyn.isEMTight", &ElectronsAuxDyn_isEMTight, &b_ElectronsAuxDyn_isEMTight);
   fChain->SetBranchAddress("ElectronsAuxDyn.LHLoose", &ElectronsAuxDyn_LHLoose, &b_ElectronsAuxDyn_LHLoose);
   fChain->SetBranchAddress("ElectronsAuxDyn.core57cellsEnergyCorrection", &ElectronsAuxDyn_core57cellsEnergyCorrection, &b_ElectronsAuxDyn_core57cellsEnergyCorrection);
   fChain->SetBranchAddress("ElectronsAuxDyn.isEMLHLoose", &ElectronsAuxDyn_isEMLHLoose, &b_ElectronsAuxDyn_isEMLHLoose);
   fChain->SetBranchAddress("ElectronsAuxDyn.etconeCorrBitset", &ElectronsAuxDyn_etconeCorrBitset, &b_ElectronsAuxDyn_etconeCorrBitset);
   fChain->SetBranchAddress("ElectronsAuxDyn.LHValue", &ElectronsAuxDyn_LHValue, &b_ElectronsAuxDyn_LHValue);
   fChain->SetBranchAddress("ElectronsAuxDyn.etcone40ptCorrection", &ElectronsAuxDyn_etcone40ptCorrection, &b_ElectronsAuxDyn_etcone40ptCorrection);
   fChain->SetBranchAddress("ElectronsAuxDyn.LHMedium", &ElectronsAuxDyn_LHMedium, &b_ElectronsAuxDyn_LHMedium);
   fChain->SetBranchAddress("ElectronsAuxDyn.etcone30ptCorrection", &ElectronsAuxDyn_etcone30ptCorrection, &b_ElectronsAuxDyn_etcone30ptCorrection);
   fChain->SetBranchAddress("ElectronsAuxDyn.isEMLHMedium", &ElectronsAuxDyn_isEMLHMedium, &b_ElectronsAuxDyn_isEMLHMedium);
   fChain->SetBranchAddress("ElectronsAuxDyn.truthParticleLink", &ElectronsAuxDyn_truthParticleLink_, &b_ElectronsAuxDyn_truthParticleLink_);
   fChain->SetBranchAddress("ElectronsAuxDyn.truthParticleLink.m_persKey", ElectronsAuxDyn_truthParticleLink_m_persKey, &b_ElectronsAuxDyn_truthParticleLink_m_persKey);
   fChain->SetBranchAddress("ElectronsAuxDyn.truthParticleLink.m_persIndex", ElectronsAuxDyn_truthParticleLink_m_persIndex, &b_ElectronsAuxDyn_truthParticleLink_m_persIndex);
   fChain->SetBranchAddress("ElectronsAuxDyn.LHTight", &ElectronsAuxDyn_LHTight, &b_ElectronsAuxDyn_LHTight);
   fChain->SetBranchAddress("ElectronsAuxDyn.etcone20ptCorrection", &ElectronsAuxDyn_etcone20ptCorrection, &b_ElectronsAuxDyn_etcone20ptCorrection);
   fChain->SetBranchAddress("ElectronsAuxDyn.isEMLHTight", &ElectronsAuxDyn_isEMLHTight, &b_ElectronsAuxDyn_isEMLHTight);
   fChain->SetBranchAddress("ElectronsAuxDyn.topoetcone20", &ElectronsAuxDyn_topoetcone20, &b_ElectronsAuxDyn_topoetcone20);
   fChain->SetBranchAddress("ElectronsAuxDyn.truthType", &ElectronsAuxDyn_truthType, &b_ElectronsAuxDyn_truthType);
   fChain->SetBranchAddress("ElectronsAuxDyn.MultiLepton", &ElectronsAuxDyn_MultiLepton, &b_ElectronsAuxDyn_MultiLepton);
   fChain->SetBranchAddress("ElectronsAuxDyn.topoetcone30", &ElectronsAuxDyn_topoetcone30, &b_ElectronsAuxDyn_topoetcone30);
   fChain->SetBranchAddress("ElectronsAuxDyn.truthOrigin", &ElectronsAuxDyn_truthOrigin, &b_ElectronsAuxDyn_truthOrigin);
   fChain->SetBranchAddress("ElectronsAuxDyn.isEMMultiLepton", &ElectronsAuxDyn_isEMMultiLepton, &b_ElectronsAuxDyn_isEMMultiLepton);
   fChain->SetBranchAddress("ElectronsAuxDyn.topoetcone40", &ElectronsAuxDyn_topoetcone40, &b_ElectronsAuxDyn_topoetcone40);
   fChain->SetBranchAddress("ElectronsAuxDyn.ambiguityLink", &ElectronsAuxDyn_ambiguityLink_, &b_ElectronsAuxDyn_ambiguityLink_);
   fChain->SetBranchAddress("ElectronsAuxDyn.ambiguityLink.m_persKey", ElectronsAuxDyn_ambiguityLink_m_persKey, &b_ElectronsAuxDyn_ambiguityLink_m_persKey);
   fChain->SetBranchAddress("ElectronsAuxDyn.ambiguityLink.m_persIndex", ElectronsAuxDyn_ambiguityLink_m_persIndex, &b_ElectronsAuxDyn_ambiguityLink_m_persIndex);
   fChain->SetBranchAddress("ElectronsAuxDyn.neflowisol20", &ElectronsAuxDyn_neflowisol20, &b_ElectronsAuxDyn_neflowisol20);
   fChain->SetBranchAddress("ElectronsAuxDyn.neflowisol30", &ElectronsAuxDyn_neflowisol30, &b_ElectronsAuxDyn_neflowisol30);
   fChain->SetBranchAddress("EventInfoAuxDyn.mcChannelNumber", &EventInfoAuxDyn_mcChannelNumber, &b_mcChannelNumber);
   fChain->SetBranchAddress("EventInfoAuxDyn.streamTagRobs", &EventInfoAuxDyn_streamTagRobs, &b_EventInfoAuxDyn_streamTagRobs);
   fChain->SetBranchAddress("EventInfoAuxDyn.streamTagDets", &EventInfoAuxDyn_streamTagDets, &b_EventInfoAuxDyn_streamTagDets);
   fChain->SetBranchAddress("EventInfoAuxDyn._SCT_BSErr_Ntot", &EventInfoAuxDyn__SCT_BSErr_Ntot, &b_EventInfoAuxDyn__SCT_BSErr_Ntot);
   fChain->SetBranchAddress("EventInfoAuxDyn._SCT_BSErr_bec", &EventInfoAuxDyn__SCT_BSErr_bec, &b_EventInfoAuxDyn__SCT_BSErr_bec);
   fChain->SetBranchAddress("EventInfoAuxDyn._SCT_BSErr_layer", &EventInfoAuxDyn__SCT_BSErr_layer, &b_EventInfoAuxDyn__SCT_BSErr_layer);
   fChain->SetBranchAddress("EventInfoAuxDyn._SCT_BSErr_eta", &EventInfoAuxDyn__SCT_BSErr_eta, &b_EventInfoAuxDyn__SCT_BSErr_eta);
   fChain->SetBranchAddress("EventInfoAuxDyn._SCT_BSErr_phi", &EventInfoAuxDyn__SCT_BSErr_phi, &b_EventInfoAuxDyn__SCT_BSErr_phi);
   fChain->SetBranchAddress("EventInfoAuxDyn._SCT_BSErr_side", &EventInfoAuxDyn__SCT_BSErr_side, &b_EventInfoAuxDyn__SCT_BSErr_side);
   fChain->SetBranchAddress("EventInfoAuxDyn._SCT_BSErr_rodid", &EventInfoAuxDyn__SCT_BSErr_rodid, &b_EventInfoAuxDyn__SCT_BSErr_rodid);
   fChain->SetBranchAddress("EventInfoAuxDyn._SCT_BSErr_channel", &EventInfoAuxDyn__SCT_BSErr_channel, &b_EventInfoAuxDyn__SCT_BSErr_channel);
   fChain->SetBranchAddress("EventInfoAuxDyn._SCT_BSErr_type", &EventInfoAuxDyn__SCT_BSErr_type, &b_EventInfoAuxDyn__SCT_BSErr_type);
   fChain->SetBranchAddress("EventInfoAuxDyn.nPixelUA", &EventInfoAuxDyn_nPixelUA, &b_nPixelUA);
   fChain->SetBranchAddress("EventInfoAuxDyn.nBlayerUA", &EventInfoAuxDyn_nBlayerUA, &b_nBlayerUA);
   fChain->SetBranchAddress("EventInfoAuxDyn.nPixelBarrelUA", &EventInfoAuxDyn_nPixelBarrelUA, &b_nPixelBarrelUA);
   fChain->SetBranchAddress("EventInfoAuxDyn.nPixelEndCapAUA", &EventInfoAuxDyn_nPixelEndCapAUA, &b_nPixelEndCapAUA);
   fChain->SetBranchAddress("EventInfoAuxDyn.nPixelEndCapCUA", &EventInfoAuxDyn_nPixelEndCapCUA, &b_nPixelEndCapCUA);
   fChain->SetBranchAddress("EventInfoAuxDyn.nSCTUA", &EventInfoAuxDyn_nSCTUA, &b_nSCTUA);
   fChain->SetBranchAddress("EventInfoAuxDyn.nSCTBarrelUA", &EventInfoAuxDyn_nSCTBarrelUA, &b_nSCTBarrelUA);
   fChain->SetBranchAddress("EventInfoAuxDyn.mcEventNumber", &EventInfoAuxDyn_mcEventNumber, &b_mcEventNumber);
   fChain->SetBranchAddress("EventInfoAuxDyn.nSCTEndCapAUA", &EventInfoAuxDyn_nSCTEndCapAUA, &b_nSCTEndCapAUA);
   fChain->SetBranchAddress("EventInfoAuxDyn.nSCTEndCapCUA", &EventInfoAuxDyn_nSCTEndCapCUA, &b_nSCTEndCapCUA);
   fChain->SetBranchAddress("EventInfoAuxDyn.mcEventWeights", &EventInfoAuxDyn_mcEventWeights, &b_EventInfoAuxDyn_mcEventWeights);
   fChain->SetBranchAddress("EventInfoAuxDyn.nTRTUA", &EventInfoAuxDyn_nTRTUA, &b_nTRTUA);
   fChain->SetBranchAddress("EventInfoAuxDyn.nTRTBarrelUA", &EventInfoAuxDyn_nTRTBarrelUA, &b_nTRTBarrelUA);
   fChain->SetBranchAddress("EventInfoAuxDyn.nTRTEndCapAUA", &EventInfoAuxDyn_nTRTEndCapAUA, &b_nTRTEndCapAUA);
   fChain->SetBranchAddress("EventInfoAuxDyn.TrtPhaseTime", &EventInfoAuxDyn_TrtPhaseTime, &b_TrtPhaseTime);
   fChain->SetBranchAddress("EventInfoAuxDyn.nTRTEndCapCUA", &EventInfoAuxDyn_nTRTEndCapCUA, &b_nTRTEndCapCUA);
   fChain->SetBranchAddress("GSFTrackParticlesAuxDyn.trackLink", &GSFTrackParticlesAuxDyn_trackLink_, &b_GSFTrackParticlesAuxDyn_trackLink_);
   fChain->SetBranchAddress("GSFTrackParticlesAuxDyn.trackLink.m_persKey", GSFTrackParticlesAuxDyn_trackLink_m_persKey, &b_GSFTrackParticlesAuxDyn_trackLink_m_persKey);
   fChain->SetBranchAddress("GSFTrackParticlesAuxDyn.trackLink.m_persIndex", GSFTrackParticlesAuxDyn_trackLink_m_persIndex, &b_GSFTrackParticlesAuxDyn_trackLink_m_persIndex);
   fChain->SetBranchAddress("GSFTrackParticlesAuxDyn.truthParticleLink", &GSFTrackParticlesAuxDyn_truthParticleLink_, &b_GSFTrackParticlesAuxDyn_truthParticleLink_);
   fChain->SetBranchAddress("GSFTrackParticlesAuxDyn.truthParticleLink.m_persKey", GSFTrackParticlesAuxDyn_truthParticleLink_m_persKey, &b_GSFTrackParticlesAuxDyn_truthParticleLink_m_persKey);
   fChain->SetBranchAddress("GSFTrackParticlesAuxDyn.truthParticleLink.m_persIndex", GSFTrackParticlesAuxDyn_truthParticleLink_m_persIndex, &b_GSFTrackParticlesAuxDyn_truthParticleLink_m_persIndex);
   fChain->SetBranchAddress("GSFTrackParticlesAuxDyn.truthMatchProbability", &GSFTrackParticlesAuxDyn_truthMatchProbability, &b_GSFTrackParticlesAuxDyn_truthMatchProbability);
   fChain->SetBranchAddress("GSFTrackParticlesAuxDyn.truthType", &GSFTrackParticlesAuxDyn_truthType, &b_GSFTrackParticlesAuxDyn_truthType);
   fChain->SetBranchAddress("GSFTrackParticlesAuxDyn.perigeeExtrapEta", &GSFTrackParticlesAuxDyn_perigeeExtrapEta, &b_GSFTrackParticlesAuxDyn_perigeeExtrapEta);
   fChain->SetBranchAddress("GSFTrackParticlesAuxDyn.truthOrigin", &GSFTrackParticlesAuxDyn_truthOrigin, &b_GSFTrackParticlesAuxDyn_truthOrigin);
   fChain->SetBranchAddress("GSFTrackParticlesAuxDyn.perigeeExtrapPhi", &GSFTrackParticlesAuxDyn_perigeeExtrapPhi, &b_GSFTrackParticlesAuxDyn_perigeeExtrapPhi);
   fChain->SetBranchAddress("GSFTrackParticlesAuxDyn.QoverPLM", &GSFTrackParticlesAuxDyn_QoverPLM, &b_GSFTrackParticlesAuxDyn_QoverPLM);
   fChain->SetBranchAddress("GSFTrackParticlesAuxDyn.TRTdEdxUsedHits", &GSFTrackParticlesAuxDyn_TRTdEdxUsedHits, &b_GSFTrackParticlesAuxDyn_TRTdEdxUsedHits);
   fChain->SetBranchAddress("GSFTrackParticlesAuxDyn.originalTrackParticle", &GSFTrackParticlesAuxDyn_originalTrackParticle_, &b_GSFTrackParticlesAuxDyn_originalTrackParticle_);
   fChain->SetBranchAddress("GSFTrackParticlesAuxDyn.originalTrackParticle.m_persKey", GSFTrackParticlesAuxDyn_originalTrackParticle_m_persKey, &b_GSFTrackParticlesAuxDyn_originalTrackParticle_m_persKey);
   fChain->SetBranchAddress("GSFTrackParticlesAuxDyn.originalTrackParticle.m_persIndex", GSFTrackParticlesAuxDyn_originalTrackParticle_m_persIndex, &b_GSFTrackParticlesAuxDyn_originalTrackParticle_m_persIndex);
   fChain->SetBranchAddress("GSFTrackParticlesAuxDyn.TRTdEdx", &GSFTrackParticlesAuxDyn_TRTdEdx, &b_GSFTrackParticlesAuxDyn_TRTdEdx);
   fChain->SetBranchAddress("InDetTrackParticlesAuxDyn.TrkIBLX", &InDetTrackParticlesAuxDyn_TrkIBLX, &b_InDetTrackParticlesAuxDyn_TrkIBLX);
   fChain->SetBranchAddress("InDetTrackParticlesAuxDyn.TRTdEdxUsedHits", &InDetTrackParticlesAuxDyn_TRTdEdxUsedHits, &b_InDetTrackParticlesAuxDyn_TRTdEdxUsedHits);
   fChain->SetBranchAddress("InDetTrackParticlesAuxDyn.TrkIBLY", &InDetTrackParticlesAuxDyn_TrkIBLY, &b_InDetTrackParticlesAuxDyn_TrkIBLY);
   fChain->SetBranchAddress("InDetTrackParticlesAuxDyn.TrkIBLZ", &InDetTrackParticlesAuxDyn_TrkIBLZ, &b_InDetTrackParticlesAuxDyn_TrkIBLZ);
   fChain->SetBranchAddress("InDetTrackParticlesAuxDyn.TRTdEdx", &InDetTrackParticlesAuxDyn_TRTdEdx, &b_InDetTrackParticlesAuxDyn_TRTdEdx);
   fChain->SetBranchAddress("InDetTrackParticlesAuxDyn.TrkBLX", &InDetTrackParticlesAuxDyn_TrkBLX, &b_InDetTrackParticlesAuxDyn_TrkBLX);
   fChain->SetBranchAddress("InDetTrackParticlesAuxDyn.TrkBLY", &InDetTrackParticlesAuxDyn_TrkBLY, &b_InDetTrackParticlesAuxDyn_TrkBLY);
   fChain->SetBranchAddress("InDetTrackParticlesAuxDyn.TrkBLZ", &InDetTrackParticlesAuxDyn_TrkBLZ, &b_InDetTrackParticlesAuxDyn_TrkBLZ);
   fChain->SetBranchAddress("InDetTrackParticlesAuxDyn.nBC_meas", &InDetTrackParticlesAuxDyn_nBC_meas, &b_InDetTrackParticlesAuxDyn_nBC_meas);
   fChain->SetBranchAddress("InDetTrackParticlesAuxDyn.TrkL1X", &InDetTrackParticlesAuxDyn_TrkL1X, &b_InDetTrackParticlesAuxDyn_TrkL1X);
   fChain->SetBranchAddress("InDetTrackParticlesAuxDyn.TrkL1Y", &InDetTrackParticlesAuxDyn_TrkL1Y, &b_InDetTrackParticlesAuxDyn_TrkL1Y);
   fChain->SetBranchAddress("InDetTrackParticlesAuxDyn.trackLink", &InDetTrackParticlesAuxDyn_trackLink_, &b_InDetTrackParticlesAuxDyn_trackLink_);
   fChain->SetBranchAddress("InDetTrackParticlesAuxDyn.trackLink.m_persKey", InDetTrackParticlesAuxDyn_trackLink_m_persKey, &b_InDetTrackParticlesAuxDyn_trackLink_m_persKey);
   fChain->SetBranchAddress("InDetTrackParticlesAuxDyn.trackLink.m_persIndex", InDetTrackParticlesAuxDyn_trackLink_m_persIndex, &b_InDetTrackParticlesAuxDyn_trackLink_m_persIndex);
   fChain->SetBranchAddress("InDetTrackParticlesAuxDyn.TrkL1Z", &InDetTrackParticlesAuxDyn_TrkL1Z, &b_InDetTrackParticlesAuxDyn_TrkL1Z);
   fChain->SetBranchAddress("InDetTrackParticlesAuxDyn.truthParticleLink", &InDetTrackParticlesAuxDyn_truthParticleLink_, &b_InDetTrackParticlesAuxDyn_truthParticleLink_);
   fChain->SetBranchAddress("InDetTrackParticlesAuxDyn.truthParticleLink.m_persKey", InDetTrackParticlesAuxDyn_truthParticleLink_m_persKey, &b_InDetTrackParticlesAuxDyn_truthParticleLink_m_persKey);
   fChain->SetBranchAddress("InDetTrackParticlesAuxDyn.truthParticleLink.m_persIndex", InDetTrackParticlesAuxDyn_truthParticleLink_m_persIndex, &b_InDetTrackParticlesAuxDyn_truthParticleLink_m_persIndex);
   fChain->SetBranchAddress("InDetTrackParticlesAuxDyn.TrkL2X", &InDetTrackParticlesAuxDyn_TrkL2X, &b_InDetTrackParticlesAuxDyn_TrkL2X);
   fChain->SetBranchAddress("InDetTrackParticlesAuxDyn.truthMatchProbability", &InDetTrackParticlesAuxDyn_truthMatchProbability, &b_InDetTrackParticlesAuxDyn_truthMatchProbability);
   fChain->SetBranchAddress("InDetTrackParticlesAuxDyn.TrkL2Y", &InDetTrackParticlesAuxDyn_TrkL2Y, &b_InDetTrackParticlesAuxDyn_TrkL2Y);
   fChain->SetBranchAddress("InDetTrackParticlesAuxDyn.truthType", &InDetTrackParticlesAuxDyn_truthType, &b_InDetTrackParticlesAuxDyn_truthType);
   fChain->SetBranchAddress("InDetTrackParticlesAuxDyn.truthOrigin", &InDetTrackParticlesAuxDyn_truthOrigin, &b_InDetTrackParticlesAuxDyn_truthOrigin);
   fChain->SetBranchAddress("InDetTrackParticlesAuxDyn.TrkL2Z", &InDetTrackParticlesAuxDyn_TrkL2Z, &b_InDetTrackParticlesAuxDyn_TrkL2Z);
   fChain->SetBranchAddress("InDetTrackParticlesAuxDyn.measurement_region", &InDetTrackParticlesAuxDyn_measurement_region, &b_InDetTrackParticlesAuxDyn_measurement_region);
   fChain->SetBranchAddress("InDetTrackParticlesAuxDyn.measurement_det", &InDetTrackParticlesAuxDyn_measurement_det, &b_InDetTrackParticlesAuxDyn_measurement_det);
   fChain->SetBranchAddress("InDetTrackParticlesAuxDyn.measurement_iLayer", &InDetTrackParticlesAuxDyn_measurement_iLayer, &b_InDetTrackParticlesAuxDyn_measurement_iLayer);
   fChain->SetBranchAddress("InDetTrackParticlesAuxDyn.hitResiduals_residualLocX", &InDetTrackParticlesAuxDyn_hitResiduals_residualLocX, &b_InDetTrackParticlesAuxDyn_hitResiduals_residualLocX);
   fChain->SetBranchAddress("InDetTrackParticlesAuxDyn.hitResiduals_pullLocX", &InDetTrackParticlesAuxDyn_hitResiduals_pullLocX, &b_InDetTrackParticlesAuxDyn_hitResiduals_pullLocX);
   fChain->SetBranchAddress("InDetTrackParticlesAuxDyn.hitResiduals_residualLocY", &InDetTrackParticlesAuxDyn_hitResiduals_residualLocY, &b_InDetTrackParticlesAuxDyn_hitResiduals_residualLocY);
   fChain->SetBranchAddress("InDetTrackParticlesAuxDyn.hitResiduals_pullLocY", &InDetTrackParticlesAuxDyn_hitResiduals_pullLocY, &b_InDetTrackParticlesAuxDyn_hitResiduals_pullLocY);
   fChain->SetBranchAddress("InDetTrackParticlesAuxDyn.hitResiduals_phiWidth", &InDetTrackParticlesAuxDyn_hitResiduals_phiWidth, &b_InDetTrackParticlesAuxDyn_hitResiduals_phiWidth);
   fChain->SetBranchAddress("InDetTrackParticlesAuxDyn.hitResiduals_etaWidth", &InDetTrackParticlesAuxDyn_hitResiduals_etaWidth, &b_InDetTrackParticlesAuxDyn_hitResiduals_etaWidth);
   fChain->SetBranchAddress("InDetTrackParticlesAuxDyn.measurement_type", &InDetTrackParticlesAuxDyn_measurement_type, &b_InDetTrackParticlesAuxDyn_measurement_type);
   fChain->SetBranchAddress("InDetTrackParticlesAuxDyn.d0err", &InDetTrackParticlesAuxDyn_d0err, &b_InDetTrackParticlesAuxDyn_d0err);
   fChain->SetBranchAddress("InDetTrackParticlesAuxDyn.z0err", &InDetTrackParticlesAuxDyn_z0err, &b_InDetTrackParticlesAuxDyn_z0err);
   fChain->SetBranchAddress("InDetTrackParticlesAuxDyn.phierr", &InDetTrackParticlesAuxDyn_phierr, &b_InDetTrackParticlesAuxDyn_phierr);
   fChain->SetBranchAddress("InDetTrackParticlesAuxDyn.thetaerr", &InDetTrackParticlesAuxDyn_thetaerr, &b_InDetTrackParticlesAuxDyn_thetaerr);
   fChain->SetBranchAddress("InDetTrackParticlesAuxDyn.qopterr", &InDetTrackParticlesAuxDyn_qopterr, &b_InDetTrackParticlesAuxDyn_qopterr);
   fChain->SetBranchAddress("InDetTrackParticlesAuxDyn.msosLink", &InDetTrackParticlesAuxDyn_msosLink, &b_InDetTrackParticlesAuxDyn_msosLink);
   fChain->SetBranchAddress("PixelClustersAuxDyn.eta_pixel_index", &PixelClustersAuxDyn_eta_pixel_index, &b_PixelClustersAuxDyn_eta_pixel_index);
   fChain->SetBranchAddress("PixelClustersAuxDyn.charge", &PixelClustersAuxDyn_charge, &b_PixelClustersAuxDyn_charge);
   fChain->SetBranchAddress("PixelClustersAuxDyn.phi_pixel_index", &PixelClustersAuxDyn_phi_pixel_index, &b_PixelClustersAuxDyn_phi_pixel_index);
   fChain->SetBranchAddress("PixelClustersAuxDyn.sizePhi", &PixelClustersAuxDyn_sizePhi, &b_PixelClustersAuxDyn_sizePhi);
   fChain->SetBranchAddress("PixelClustersAuxDyn.sizeZ", &PixelClustersAuxDyn_sizeZ, &b_PixelClustersAuxDyn_sizeZ);
   fChain->SetBranchAddress("PixelClustersAuxDyn.nRDO", &PixelClustersAuxDyn_nRDO, &b_PixelClustersAuxDyn_nRDO);
   fChain->SetBranchAddress("PixelClustersAuxDyn.ToT", &PixelClustersAuxDyn_ToT, &b_PixelClustersAuxDyn_ToT);
   fChain->SetBranchAddress("PixelClustersAuxDyn.LVL1A", &PixelClustersAuxDyn_LVL1A, &b_PixelClustersAuxDyn_LVL1A);
   fChain->SetBranchAddress("PixelClustersAuxDyn.isFake", &PixelClustersAuxDyn_isFake, &b_PixelClustersAuxDyn_isFake);
   fChain->SetBranchAddress("PixelClustersAuxDyn.gangedPixel", &PixelClustersAuxDyn_gangedPixel, &b_PixelClustersAuxDyn_gangedPixel);
   fChain->SetBranchAddress("PixelClustersAuxDyn.isSplit", &PixelClustersAuxDyn_isSplit, &b_PixelClustersAuxDyn_isSplit);
   fChain->SetBranchAddress("PixelClustersAuxDyn.splitProbability1", &PixelClustersAuxDyn_splitProbability1, &b_PixelClustersAuxDyn_splitProbability1);
   fChain->SetBranchAddress("PixelClustersAuxDyn.splitProbability2", &PixelClustersAuxDyn_splitProbability2, &b_PixelClustersAuxDyn_splitProbability2);
   fChain->SetBranchAddress("PixelClustersAuxDyn.NN_sizeX", &PixelClustersAuxDyn_NN_sizeX, &b_PixelClustersAuxDyn_NN_sizeX);
   fChain->SetBranchAddress("PixelClustersAuxDyn.NN_sizeY", &PixelClustersAuxDyn_NN_sizeY, &b_PixelClustersAuxDyn_NN_sizeY);
   fChain->SetBranchAddress("PixelClustersAuxDyn.NN_phiBS", &PixelClustersAuxDyn_NN_phiBS, &b_PixelClustersAuxDyn_NN_phiBS);
   fChain->SetBranchAddress("PixelClustersAuxDyn.NN_thetaBS", &PixelClustersAuxDyn_NN_thetaBS, &b_PixelClustersAuxDyn_NN_thetaBS);
   fChain->SetBranchAddress("PixelClustersAuxDyn.NN_matrixOfToT", &PixelClustersAuxDyn_NN_matrixOfToT, &b_PixelClustersAuxDyn_NN_matrixOfToT);
   fChain->SetBranchAddress("PixelClustersAuxDyn.NN_matrixOfCharge", &PixelClustersAuxDyn_NN_matrixOfCharge, &b_PixelClustersAuxDyn_NN_matrixOfCharge);
   fChain->SetBranchAddress("PixelClustersAuxDyn.NN_vectorOfPitchesY", &PixelClustersAuxDyn_NN_vectorOfPitchesY, &b_PixelClustersAuxDyn_NN_vectorOfPitchesY);
   fChain->SetBranchAddress("PixelClustersAuxDyn.NN_etaPixelIndexWeightedPosition", &PixelClustersAuxDyn_NN_etaPixelIndexWeightedPosition, &b_PixelClustersAuxDyn_NN_etaPixelIndexWeightedPosition);
   fChain->SetBranchAddress("PixelClustersAuxDyn.NN_phiPixelIndexWeightedPosition", &PixelClustersAuxDyn_NN_phiPixelIndexWeightedPosition, &b_PixelClustersAuxDyn_NN_phiPixelIndexWeightedPosition);
   fChain->SetBranchAddress("PixelClustersAuxDyn.NN_localEtaPixelIndexWeightedPosition", &PixelClustersAuxDyn_NN_localEtaPixelIndexWeightedPosition, &b_PixelClustersAuxDyn_NN_localEtaPixelIndexWeightedPosition);
   fChain->SetBranchAddress("PixelClustersAuxDyn.NN_localPhiPixelIndexWeightedPosition", &PixelClustersAuxDyn_NN_localPhiPixelIndexWeightedPosition, &b_PixelClustersAuxDyn_NN_localPhiPixelIndexWeightedPosition);
   fChain->SetBranchAddress("PixelClustersAuxDyn.isBSError", &PixelClustersAuxDyn_isBSError, &b_PixelClustersAuxDyn_isBSError);
   fChain->SetBranchAddress("PixelClustersAuxDyn.DCSState", &PixelClustersAuxDyn_DCSState, &b_PixelClustersAuxDyn_DCSState);
   fChain->SetBranchAddress("PixelClustersAuxDyn.BiasVoltage", &PixelClustersAuxDyn_BiasVoltage, &b_PixelClustersAuxDyn_BiasVoltage);
   fChain->SetBranchAddress("PixelClustersAuxDyn.Temperature", &PixelClustersAuxDyn_Temperature, &b_PixelClustersAuxDyn_Temperature);
   fChain->SetBranchAddress("PixelClustersAuxDyn.DepletionVoltage", &PixelClustersAuxDyn_DepletionVoltage, &b_PixelClustersAuxDyn_DepletionVoltage);
   fChain->SetBranchAddress("PixelClustersAuxDyn.LorentzShift", &PixelClustersAuxDyn_LorentzShift, &b_PixelClustersAuxDyn_LorentzShift);
   fChain->SetBranchAddress("PixelClustersAuxDyn.rdo_phi_pixel_index", &PixelClustersAuxDyn_rdo_phi_pixel_index, &b_PixelClustersAuxDyn_rdo_phi_pixel_index);
   fChain->SetBranchAddress("PixelClustersAuxDyn.rdo_eta_pixel_index", &PixelClustersAuxDyn_rdo_eta_pixel_index, &b_PixelClustersAuxDyn_rdo_eta_pixel_index);
   fChain->SetBranchAddress("PixelClustersAuxDyn.rdo_charge", &PixelClustersAuxDyn_rdo_charge, &b_PixelClustersAuxDyn_rdo_charge);
   fChain->SetBranchAddress("PixelClustersAuxDyn.rdo_tot", &PixelClustersAuxDyn_rdo_tot, &b_PixelClustersAuxDyn_rdo_tot);
   fChain->SetBranchAddress("PixelClustersAuxDyn.rdo_Cterm", &PixelClustersAuxDyn_rdo_Cterm, &b_PixelClustersAuxDyn_rdo_Cterm);
   fChain->SetBranchAddress("PixelClustersAuxDyn.rdo_Aterm", &PixelClustersAuxDyn_rdo_Aterm, &b_PixelClustersAuxDyn_rdo_Aterm);
   fChain->SetBranchAddress("PixelClustersAuxDyn.rdo_Eterm", &PixelClustersAuxDyn_rdo_Eterm, &b_PixelClustersAuxDyn_rdo_Eterm);
   fChain->SetBranchAddress("PixelClustersAuxDyn.detectorElementID", &PixelClustersAuxDyn_detectorElementID, &b_PixelClustersAuxDyn_detectorElementID);
   fChain->SetBranchAddress("PixelClustersAuxDyn.truth_barcode", &PixelClustersAuxDyn_truth_barcode, &b_PixelClustersAuxDyn_truth_barcode);
   fChain->SetBranchAddress("PixelClustersAuxDyn.sdo_words", &PixelClustersAuxDyn_sdo_words, &b_PixelClustersAuxDyn_sdo_words);
   fChain->SetBranchAddress("PixelClustersAuxDyn.sdo_depositsBarcode", &PixelClustersAuxDyn_sdo_depositsBarcode, &b_PixelClustersAuxDyn_sdo_depositsBarcode);
   fChain->SetBranchAddress("PixelClustersAuxDyn.sdo_depositsEnergy", &PixelClustersAuxDyn_sdo_depositsEnergy, &b_PixelClustersAuxDyn_sdo_depositsEnergy);
   fChain->SetBranchAddress("PixelClustersAuxDyn.sihit_energyDeposit", &PixelClustersAuxDyn_sihit_energyDeposit, &b_PixelClustersAuxDyn_sihit_energyDeposit);
   fChain->SetBranchAddress("PixelClustersAuxDyn.sihit_meanTime", &PixelClustersAuxDyn_sihit_meanTime, &b_PixelClustersAuxDyn_sihit_meanTime);
   fChain->SetBranchAddress("PixelClustersAuxDyn.sihit_barcode", &PixelClustersAuxDyn_sihit_barcode, &b_PixelClustersAuxDyn_sihit_barcode);
   fChain->SetBranchAddress("PixelClustersAuxDyn.sihit_pdgid", &PixelClustersAuxDyn_sihit_pdgid, &b_PixelClustersAuxDyn_sihit_pdgid);
   fChain->SetBranchAddress("PixelClustersAuxDyn.sihit_startPosX", &PixelClustersAuxDyn_sihit_startPosX, &b_PixelClustersAuxDyn_sihit_startPosX);
   fChain->SetBranchAddress("PixelClustersAuxDyn.sihit_startPosY", &PixelClustersAuxDyn_sihit_startPosY, &b_PixelClustersAuxDyn_sihit_startPosY);
   fChain->SetBranchAddress("PixelClustersAuxDyn.bec", &PixelClustersAuxDyn_bec, &b_PixelClustersAuxDyn_bec);
   fChain->SetBranchAddress("PixelClustersAuxDyn.sihit_startPosZ", &PixelClustersAuxDyn_sihit_startPosZ, &b_PixelClustersAuxDyn_sihit_startPosZ);
   fChain->SetBranchAddress("PixelClustersAuxDyn.layer", &PixelClustersAuxDyn_layer, &b_PixelClustersAuxDyn_layer);
   fChain->SetBranchAddress("PixelClustersAuxDyn.sihit_endPosX", &PixelClustersAuxDyn_sihit_endPosX, &b_PixelClustersAuxDyn_sihit_endPosX);
   fChain->SetBranchAddress("PixelClustersAuxDyn.phi_module", &PixelClustersAuxDyn_phi_module, &b_PixelClustersAuxDyn_phi_module);
   fChain->SetBranchAddress("PixelClustersAuxDyn.sihit_endPosY", &PixelClustersAuxDyn_sihit_endPosY, &b_PixelClustersAuxDyn_sihit_endPosY);
   fChain->SetBranchAddress("PixelClustersAuxDyn.eta_module", &PixelClustersAuxDyn_eta_module, &b_PixelClustersAuxDyn_eta_module);
   fChain->SetBranchAddress("PixelClustersAuxDyn.sihit_endPosZ", &PixelClustersAuxDyn_sihit_endPosZ, &b_PixelClustersAuxDyn_sihit_endPosZ);
   fChain->SetBranchAddress("PixelClustersAuxDyn.NN_positionsX", &PixelClustersAuxDyn_NN_positionsX, &b_PixelClustersAuxDyn_NN_positionsX);
   fChain->SetBranchAddress("PixelClustersAuxDyn.NN_positionsY", &PixelClustersAuxDyn_NN_positionsY, &b_PixelClustersAuxDyn_NN_positionsY);
   fChain->SetBranchAddress("PixelClustersAuxDyn.NN_positions_indexX", &PixelClustersAuxDyn_NN_positions_indexX, &b_PixelClustersAuxDyn_NN_positions_indexX);
   fChain->SetBranchAddress("PixelClustersAuxDyn.NN_positions_indexY", &PixelClustersAuxDyn_NN_positions_indexY, &b_PixelClustersAuxDyn_NN_positions_indexY);
   fChain->SetBranchAddress("PixelClustersAuxDyn.NN_theta", &PixelClustersAuxDyn_NN_theta, &b_PixelClustersAuxDyn_NN_theta);
   fChain->SetBranchAddress("PixelClustersAuxDyn.NN_phi", &PixelClustersAuxDyn_NN_phi, &b_PixelClustersAuxDyn_NN_phi);
   fChain->SetBranchAddress("PixelClustersAuxDyn.NN_barcode", &PixelClustersAuxDyn_NN_barcode, &b_PixelClustersAuxDyn_NN_barcode);
   fChain->SetBranchAddress("PixelClustersAuxDyn.NN_pdgid", &PixelClustersAuxDyn_NN_pdgid, &b_PixelClustersAuxDyn_NN_pdgid);
   fChain->SetBranchAddress("PixelClustersAuxDyn.NN_energyDep", &PixelClustersAuxDyn_NN_energyDep, &b_PixelClustersAuxDyn_NN_energyDep);
   fChain->SetBranchAddress("PixelClustersAuxDyn.NN_trueP", &PixelClustersAuxDyn_NN_trueP, &b_PixelClustersAuxDyn_NN_trueP);
   fChain->SetBranchAddress("PixelClustersAuxDyn.NN_motherBarcode", &PixelClustersAuxDyn_NN_motherBarcode, &b_PixelClustersAuxDyn_NN_motherBarcode);
   fChain->SetBranchAddress("PixelClustersAuxDyn.NN_motherPdgid", &PixelClustersAuxDyn_NN_motherPdgid, &b_PixelClustersAuxDyn_NN_motherPdgid);
   fChain->SetBranchAddress("PixelClustersAuxDyn.NN_pathlengthX", &PixelClustersAuxDyn_NN_pathlengthX, &b_PixelClustersAuxDyn_NN_pathlengthX);
   fChain->SetBranchAddress("PixelClustersAuxDyn.NN_pathlengthY", &PixelClustersAuxDyn_NN_pathlengthY, &b_PixelClustersAuxDyn_NN_pathlengthY);
   fChain->SetBranchAddress("PixelClustersAuxDyn.NN_pathlengthZ", &PixelClustersAuxDyn_NN_pathlengthZ, &b_PixelClustersAuxDyn_NN_pathlengthZ);
   fChain->SetBranchAddress("PixelClustersAuxDyn.broken", &PixelClustersAuxDyn_broken, &b_PixelClustersAuxDyn_broken);
   fChain->SetBranchAddress("TauJetsAuxDyn.TAU_ABSDELTAETA", &TauJetsAuxDyn_TAU_ABSDELTAETA, &b_TauJetsAuxDyn_TAU_ABSDELTAETA);
   fChain->SetBranchAddress("TauJetsAuxDyn.GhostMuonSegmentCount", &TauJetsAuxDyn_GhostMuonSegmentCount, &b_TauJetsAuxDyn_GhostMuonSegmentCount);
   fChain->SetBranchAddress("TauJetsAuxDyn.PanTau_DecayModeExtended", &TauJetsAuxDyn_PanTau_DecayModeExtended, &b_TauJetsAuxDyn_PanTau_DecayModeExtended);
   fChain->SetBranchAddress("TauJetsAuxDyn.TAU_ABSDELTAPHI", &TauJetsAuxDyn_TAU_ABSDELTAPHI, &b_TauJetsAuxDyn_TAU_ABSDELTAPHI);
   fChain->SetBranchAddress("TauJetsAuxDyn.pt_combined", &TauJetsAuxDyn_pt_combined, &b_TauJetsAuxDyn_pt_combined);
   fChain->SetBranchAddress("TauJetsAuxDyn.EMFracFixed", &TauJetsAuxDyn_EMFracFixed, &b_TauJetsAuxDyn_EMFracFixed);
   fChain->SetBranchAddress("TauJetsAuxDyn.eta_combined", &TauJetsAuxDyn_eta_combined, &b_TauJetsAuxDyn_eta_combined);
   fChain->SetBranchAddress("TauJetsAuxDyn.etHotShotWinOverPtLeadTrk", &TauJetsAuxDyn_etHotShotWinOverPtLeadTrk, &b_TauJetsAuxDyn_etHotShotWinOverPtLeadTrk);
   fChain->SetBranchAddress("TauJetsAuxDyn.phi_combined", &TauJetsAuxDyn_phi_combined, &b_TauJetsAuxDyn_phi_combined);
   fChain->SetBranchAddress("TauJetsAuxDyn.hadLeakFracFixed", &TauJetsAuxDyn_hadLeakFracFixed, &b_TauJetsAuxDyn_hadLeakFracFixed);
   fChain->SetBranchAddress("TauJetsAuxDyn.m_combined", &TauJetsAuxDyn_m_combined, &b_TauJetsAuxDyn_m_combined);
   fChain->SetBranchAddress("TauJetsAuxDyn.mu", &TauJetsAuxDyn_mu, &b_TauJetsAuxDyn_mu);
   fChain->SetBranchAddress("TauJetsAuxDyn.leadTrackProbHT", &TauJetsAuxDyn_leadTrackProbHT, &b_TauJetsAuxDyn_leadTrackProbHT);
   fChain->SetBranchAddress("TauJetsAuxDyn.nVtxPU", &TauJetsAuxDyn_nVtxPU, &b_TauJetsAuxDyn_nVtxPU);
   fChain->SetBranchAddress("TauJetsAuxDyn.BDTEleScoreSigTrans", &TauJetsAuxDyn_BDTEleScoreSigTrans, &b_TauJetsAuxDyn_BDTEleScoreSigTrans);
   fChain->SetBranchAddress("TauJetsAuxDyn.ClustersMeanCenterLambda", &TauJetsAuxDyn_ClustersMeanCenterLambda, &b_TauJetsAuxDyn_ClustersMeanCenterLambda);
   fChain->SetBranchAddress("TauJetsAuxDyn.ClustersMeanFirstEngDens", &TauJetsAuxDyn_ClustersMeanFirstEngDens, &b_TauJetsAuxDyn_ClustersMeanFirstEngDens);
   fChain->SetBranchAddress("TauJetsAuxDyn.ClustersMeanEMProbability", &TauJetsAuxDyn_ClustersMeanEMProbability, &b_TauJetsAuxDyn_ClustersMeanEMProbability);
   fChain->SetBranchAddress("TauJetsAuxDyn.ClustersMeanSecondLambda", &TauJetsAuxDyn_ClustersMeanSecondLambda, &b_TauJetsAuxDyn_ClustersMeanSecondLambda);
   fChain->SetBranchAddress("TauJetsAuxDyn.ClustersMeanPresamplerFrac", &TauJetsAuxDyn_ClustersMeanPresamplerFrac, &b_TauJetsAuxDyn_ClustersMeanPresamplerFrac);
   fChain->SetBranchAddress("TauJetsAuxDyn.LeadClusterFrac", &TauJetsAuxDyn_LeadClusterFrac, &b_TauJetsAuxDyn_LeadClusterFrac);
   fChain->SetBranchAddress("TauJetsAuxDyn.UpsilonCluster", &TauJetsAuxDyn_UpsilonCluster, &b_TauJetsAuxDyn_UpsilonCluster);
   fChain->SetBranchAddress("TauJetsAuxDyn.PFOEngRelDiff", &TauJetsAuxDyn_PFOEngRelDiff, &b_TauJetsAuxDyn_PFOEngRelDiff);
   fChain->SetBranchAddress("TauJetsAuxDyn.LC_pantau_interpolPt", &TauJetsAuxDyn_LC_pantau_interpolPt, &b_TauJetsAuxDyn_LC_pantau_interpolPt);
   fChain->SetBranchAddress("TauJetsAuxDyn.nModifiedIsolationTracks", &TauJetsAuxDyn_nModifiedIsolationTracks, &b_TauJetsAuxDyn_nModifiedIsolationTracks);
   fChain->SetBranchAddress("TauJetsAuxDyn.NUMTRACK", &TauJetsAuxDyn_NUMTRACK, &b_TauJetsAuxDyn_NUMTRACK);
   fChain->SetBranchAddress("TauJetsAuxDyn.MU", &TauJetsAuxDyn_MU, &b_TauJetsAuxDyn_MU);
   fChain->SetBranchAddress("TauJetsAuxDyn.NUMVERTICES", &TauJetsAuxDyn_NUMVERTICES, &b_TauJetsAuxDyn_NUMVERTICES);
   fChain->SetBranchAddress("TauJetsAuxDyn.leadTrackEta", &TauJetsAuxDyn_leadTrackEta, &b_TauJetsAuxDyn_leadTrackEta);
   fChain->SetBranchAddress("TauJetsAuxDyn.EMFRACTIONATEMSCALE_MOVEE3", &TauJetsAuxDyn_EMFRACTIONATEMSCALE_MOVEE3, &b_TauJetsAuxDyn_EMFRACTIONATEMSCALE_MOVEE3);
   fChain->SetBranchAddress("TauJetsAuxDyn.TAU_SEEDTRK_SECMAXSTRIPETOVERPT", &TauJetsAuxDyn_TAU_SEEDTRK_SECMAXSTRIPETOVERPT, &b_TauJetsAuxDyn_TAU_SEEDTRK_SECMAXSTRIPETOVERPT);
   fChain->SetBranchAddress("TauJetsAuxDyn.ABS_ETA_LEAD_TRACK", &TauJetsAuxDyn_ABS_ETA_LEAD_TRACK, &b_TauJetsAuxDyn_ABS_ETA_LEAD_TRACK);
   fChain->SetBranchAddress("TauJetsAuxDyn.CORRFTRK", &TauJetsAuxDyn_CORRFTRK, &b_TauJetsAuxDyn_CORRFTRK);
   fChain->SetBranchAddress("TauJetsAuxDyn.HADLEAKET", &TauJetsAuxDyn_HADLEAKET, &b_TauJetsAuxDyn_HADLEAKET);
   fChain->SetBranchAddress("TauJetsAuxDyn.TAU_TRT_NHT_OVER_NLT", &TauJetsAuxDyn_TAU_TRT_NHT_OVER_NLT, &b_TauJetsAuxDyn_TAU_TRT_NHT_OVER_NLT);
   fChain->SetBranchAddress("TauJetsAuxDyn.CORRCENTFRAC", &TauJetsAuxDyn_CORRCENTFRAC, &b_TauJetsAuxDyn_CORRCENTFRAC);
   fChain->SetBranchAddress("TauJetsAuxDyn.etHotShotDR1", &TauJetsAuxDyn_etHotShotDR1, &b_TauJetsAuxDyn_etHotShotDR1);
   fChain->SetBranchAddress("TauJetsAuxDyn.etHotShotWin", &TauJetsAuxDyn_etHotShotWin, &b_TauJetsAuxDyn_etHotShotWin);
   fChain->SetBranchAddress("TauJetsAuxDyn.etHotShotDR1OverPtLeadTrk", &TauJetsAuxDyn_etHotShotDR1OverPtLeadTrk, &b_TauJetsAuxDyn_etHotShotDR1OverPtLeadTrk);
   fChain->SetBranchAddress("TauJetsAuxDyn.electronLink", &TauJetsAuxDyn_electronLink_, &b_TauJetsAuxDyn_electronLink_);
   fChain->SetBranchAddress("TauJetsAuxDyn.electronLink.m_persKey", TauJetsAuxDyn_electronLink_m_persKey, &b_TauJetsAuxDyn_electronLink_m_persKey);
   fChain->SetBranchAddress("TauJetsAuxDyn.electronLink.m_persIndex", TauJetsAuxDyn_electronLink_m_persIndex, &b_TauJetsAuxDyn_electronLink_m_persIndex);
   fChain->SetBranchAddress("TauJetsAuxDyn.absipSigLeadTrk", &TauJetsAuxDyn_absipSigLeadTrk, &b_TauJetsAuxDyn_absipSigLeadTrk);
   fChain->SetBranchAddress("TauTracksAuxDyn.CaloSamplingEtaEM", &TauTracksAuxDyn_CaloSamplingEtaEM, &b_TauTracksAuxDyn_CaloSamplingEtaEM);
   fChain->SetBranchAddress("TauTracksAuxDyn.CaloSamplingPhiEM", &TauTracksAuxDyn_CaloSamplingPhiEM, &b_TauTracksAuxDyn_CaloSamplingPhiEM);
   fChain->SetBranchAddress("TauTracksAuxDyn.CaloSamplingEtaHad", &TauTracksAuxDyn_CaloSamplingEtaHad, &b_TauTracksAuxDyn_CaloSamplingEtaHad);
   fChain->SetBranchAddress("TauTracksAuxDyn.CaloSamplingPhiHad", &TauTracksAuxDyn_CaloSamplingPhiHad, &b_TauTracksAuxDyn_CaloSamplingPhiHad);
   fChain->SetBranchAddress("TruthEventsAuxDyn.PDGID1", &TruthEventsAuxDyn_PDGID1, &b_TruthEventsAuxDyn_PDGID1);
   fChain->SetBranchAddress("TruthEventsAuxDyn.PDGID2", &TruthEventsAuxDyn_PDGID2, &b_TruthEventsAuxDyn_PDGID2);
   fChain->SetBranchAddress("TruthEventsAuxDyn.PDFID1", &TruthEventsAuxDyn_PDFID1, &b_TruthEventsAuxDyn_PDFID1);
   fChain->SetBranchAddress("TruthEventsAuxDyn.PDFID2", &TruthEventsAuxDyn_PDFID2, &b_TruthEventsAuxDyn_PDFID2);
   fChain->SetBranchAddress("TruthEventsAuxDyn.X1", &TruthEventsAuxDyn_X1, &b_TruthEventsAuxDyn_X1);
   fChain->SetBranchAddress("TruthEventsAuxDyn.X2", &TruthEventsAuxDyn_X2, &b_TruthEventsAuxDyn_X2);
   fChain->SetBranchAddress("TruthEventsAuxDyn.Q", &TruthEventsAuxDyn_Q, &b_TruthEventsAuxDyn_Q);
   fChain->SetBranchAddress("TruthEventsAuxDyn.XF1", &TruthEventsAuxDyn_XF1, &b_TruthEventsAuxDyn_XF1);
   fChain->SetBranchAddress("TruthEventsAuxDyn.XF2", &TruthEventsAuxDyn_XF2, &b_TruthEventsAuxDyn_XF2);
   fChain->SetBranchAddress("TruthParticlesAuxDyn.polarizationTheta", &TruthParticlesAuxDyn_polarizationTheta, &b_TruthParticlesAuxDyn_polarizationTheta);
   fChain->SetBranchAddress("TruthParticlesAuxDyn.polarizationPhi", &TruthParticlesAuxDyn_polarizationPhi, &b_TruthParticlesAuxDyn_polarizationPhi);
   fChain->SetBranchAddress("TruthParticlesAuxDyn.d0", &TruthParticlesAuxDyn_d0, &b_TruthParticlesAuxDyn_d0);
   fChain->SetBranchAddress("TruthParticlesAuxDyn.z0", &TruthParticlesAuxDyn_z0, &b_TruthParticlesAuxDyn_z0);
   fChain->SetBranchAddress("TruthParticlesAuxDyn.z0st", &TruthParticlesAuxDyn_z0st, &b_TruthParticlesAuxDyn_z0st);
   fChain->SetBranchAddress("TruthParticlesAuxDyn.theta", &TruthParticlesAuxDyn_theta, &b_TruthParticlesAuxDyn_theta);
   fChain->SetBranchAddress("TruthParticlesAuxDyn.qOverP", &TruthParticlesAuxDyn_qOverP, &b_TruthParticlesAuxDyn_qOverP);
   fChain->SetBranchAddress("TruthParticlesAuxDyn.prodR", &TruthParticlesAuxDyn_prodR, &b_TruthParticlesAuxDyn_prodR);
   fChain->SetBranchAddress("TruthParticlesAuxDyn.prodZ", &TruthParticlesAuxDyn_prodZ, &b_TruthParticlesAuxDyn_prodZ);
   fChain->SetBranchAddress("TruthParticlesAuxDyn.phi", &TruthParticlesAuxDyn_phi, &b_TruthParticlesAuxDyn_phi);
   fChain->SetBranchAddress("InDetTrackParticlesAuxDyn.caloExt_Decorated", &InDetTrackParticlesAuxDyn_caloExt_Decorated, &b_InDetTrackParticlesAuxDyn_caloExt_Decorated);
   fChain->SetBranchAddress("InDetTrackParticlesAuxDyn.caloExt_eta", &InDetTrackParticlesAuxDyn_caloExt_eta, &b_InDetTrackParticlesAuxDyn_caloExt_eta);
   fChain->SetBranchAddress("InDetTrackParticlesAuxDyn.caloExt_phi", &InDetTrackParticlesAuxDyn_caloExt_phi, &b_InDetTrackParticlesAuxDyn_caloExt_phi);
   fChain->SetBranchAddress("MuonsAuxDyn.neflowisol40", &MuonsAuxDyn_neflowisol40, &b_MuonsAuxDyn_neflowisol40);
   fChain->SetBranchAddress("MuonsAuxDyn.CT_EL_Type", &MuonsAuxDyn_CT_EL_Type, &b_MuonsAuxDyn_CT_EL_Type);
   fChain->SetBranchAddress("MuonsAuxDyn.topoetconeCorrBitset", &MuonsAuxDyn_topoetconeCorrBitset, &b_MuonsAuxDyn_topoetconeCorrBitset);
   fChain->SetBranchAddress("MuonsAuxDyn.CT_ET_LRLikelihood", &MuonsAuxDyn_CT_ET_LRLikelihood, &b_MuonsAuxDyn_CT_ET_LRLikelihood);
   fChain->SetBranchAddress("MuonsAuxDyn.topoetconecoreConeEnergyCorrection", &MuonsAuxDyn_topoetconecoreConeEnergyCorrection, &b_MuonsAuxDyn_topoetconecoreConeEnergyCorrection);
   fChain->SetBranchAddress("MuonsAuxDyn.CT_ET_FSRCandidateEnergy", &MuonsAuxDyn_CT_ET_FSRCandidateEnergy, &b_MuonsAuxDyn_CT_ET_FSRCandidateEnergy);
   fChain->SetBranchAddress("MuonsAuxDyn.d0_staco", &MuonsAuxDyn_d0_staco, &b_MuonsAuxDyn_d0_staco);
   fChain->SetBranchAddress("MuonsAuxDyn.z0_staco", &MuonsAuxDyn_z0_staco, &b_MuonsAuxDyn_z0_staco);
   fChain->SetBranchAddress("MuonsAuxDyn.phi0_staco", &MuonsAuxDyn_phi0_staco, &b_MuonsAuxDyn_phi0_staco);
   fChain->SetBranchAddress("MuonsAuxDyn.theta_staco", &MuonsAuxDyn_theta_staco, &b_MuonsAuxDyn_theta_staco);
   fChain->SetBranchAddress("MuonsAuxDyn.neflowisolCorrBitset", &MuonsAuxDyn_neflowisolCorrBitset, &b_MuonsAuxDyn_neflowisolCorrBitset);
   fChain->SetBranchAddress("MuonsAuxDyn.qOverP_staco", &MuonsAuxDyn_qOverP_staco, &b_MuonsAuxDyn_qOverP_staco);
   fChain->SetBranchAddress("MuonsAuxDyn.neflowisolcoreConeEnergyCorrection", &MuonsAuxDyn_neflowisolcoreConeEnergyCorrection, &b_MuonsAuxDyn_neflowisolcoreConeEnergyCorrection);
   fChain->SetBranchAddress("MuonsAuxDyn.qOverPErr_staco", &MuonsAuxDyn_qOverPErr_staco, &b_MuonsAuxDyn_qOverPErr_staco);
   fChain->SetBranchAddress("MuonsAuxDyn.numberOfGoodPrecisionLayers", &MuonsAuxDyn_numberOfGoodPrecisionLayers, &b_MuonsAuxDyn_numberOfGoodPrecisionLayers);
   fChain->SetBranchAddress("MuonsAuxDyn.innerClosePrecisionHits", &MuonsAuxDyn_innerClosePrecisionHits, &b_MuonsAuxDyn_innerClosePrecisionHits);
   fChain->SetBranchAddress("MuonsAuxDyn.middleClosePrecisionHits", &MuonsAuxDyn_middleClosePrecisionHits, &b_MuonsAuxDyn_middleClosePrecisionHits);
   fChain->SetBranchAddress("MuonsAuxDyn.ptconeCorrBitset", &MuonsAuxDyn_ptconeCorrBitset, &b_MuonsAuxDyn_ptconeCorrBitset);
   fChain->SetBranchAddress("MuonsAuxDyn.outerClosePrecisionHits", &MuonsAuxDyn_outerClosePrecisionHits, &b_MuonsAuxDyn_outerClosePrecisionHits);
   fChain->SetBranchAddress("MuonsAuxDyn.ptconecoreTrackPtrCorrection", &MuonsAuxDyn_ptconecoreTrackPtrCorrection, &b_MuonsAuxDyn_ptconecoreTrackPtrCorrection);
   fChain->SetBranchAddress("MuonsAuxDyn.extendedClosePrecisionHits", &MuonsAuxDyn_extendedClosePrecisionHits, &b_MuonsAuxDyn_extendedClosePrecisionHits);
   fChain->SetBranchAddress("MuonsAuxDyn.innerOutBoundsPrecisionHits", &MuonsAuxDyn_innerOutBoundsPrecisionHits, &b_MuonsAuxDyn_innerOutBoundsPrecisionHits);
   fChain->SetBranchAddress("MuonsAuxDyn.middleOutBoundsPrecisionHits", &MuonsAuxDyn_middleOutBoundsPrecisionHits, &b_MuonsAuxDyn_middleOutBoundsPrecisionHits);
   fChain->SetBranchAddress("MuonsAuxDyn.outerOutBoundsPrecisionHits", &MuonsAuxDyn_outerOutBoundsPrecisionHits, &b_MuonsAuxDyn_outerOutBoundsPrecisionHits);
   fChain->SetBranchAddress("MuonsAuxDyn.extendedOutBoundsPrecisionHits", &MuonsAuxDyn_extendedOutBoundsPrecisionHits, &b_MuonsAuxDyn_extendedOutBoundsPrecisionHits);
   fChain->SetBranchAddress("MuonsAuxDyn.combinedTrackOutBoundsPrecisionHits", &MuonsAuxDyn_combinedTrackOutBoundsPrecisionHits, &b_MuonsAuxDyn_combinedTrackOutBoundsPrecisionHits);
   fChain->SetBranchAddress("MuonsAuxDyn.isEndcapGoodLayers", &MuonsAuxDyn_isEndcapGoodLayers, &b_MuonsAuxDyn_isEndcapGoodLayers);
   fChain->SetBranchAddress("MuonsAuxDyn.isSmallGoodSectors", &MuonsAuxDyn_isSmallGoodSectors, &b_MuonsAuxDyn_isSmallGoodSectors);
   fChain->SetBranchAddress("MuonsAuxDyn.MuonSpectrometerPt", &MuonsAuxDyn_MuonSpectrometerPt, &b_MuonsAuxDyn_MuonSpectrometerPt);
   fChain->SetBranchAddress("MuonsAuxDyn.InnerDetectorPt", &MuonsAuxDyn_InnerDetectorPt, &b_MuonsAuxDyn_InnerDetectorPt);
   fChain->SetBranchAddress("MuonsAuxDyn.FSR_CandidateEnergy", &MuonsAuxDyn_FSR_CandidateEnergy, &b_MuonsAuxDyn_FSR_CandidateEnergy);
   fChain->SetBranchAddress("MuonsAuxDyn.numEnergyLossPerTrack", &MuonsAuxDyn_numEnergyLossPerTrack, &b_MuonsAuxDyn_numEnergyLossPerTrack);
   fChain->SetBranchAddress("MuonsAuxDyn.segmentsOnTrack", &MuonsAuxDyn_segmentsOnTrack, &b_MuonsAuxDyn_segmentsOnTrack);
   fChain->SetBranchAddress("MuonsAuxDyn.deltaphi_0", &MuonsAuxDyn_deltaphi_0, &b_MuonsAuxDyn_deltaphi_0);
   fChain->SetBranchAddress("MuonsAuxDyn.deltatheta_0", &MuonsAuxDyn_deltatheta_0, &b_MuonsAuxDyn_deltatheta_0);
   fChain->SetBranchAddress("MuonsAuxDyn.sigmadeltaphi_0", &MuonsAuxDyn_sigmadeltaphi_0, &b_MuonsAuxDyn_sigmadeltaphi_0);
   fChain->SetBranchAddress("MuonsAuxDyn.sigmadeltatheta_0", &MuonsAuxDyn_sigmadeltatheta_0, &b_MuonsAuxDyn_sigmadeltatheta_0);
   fChain->SetBranchAddress("MuonsAuxDyn.deltaphi_1", &MuonsAuxDyn_deltaphi_1, &b_MuonsAuxDyn_deltaphi_1);
   fChain->SetBranchAddress("MuonsAuxDyn.deltatheta_1", &MuonsAuxDyn_deltatheta_1, &b_MuonsAuxDyn_deltatheta_1);
   fChain->SetBranchAddress("MuonsAuxDyn.sigmadeltaphi_1", &MuonsAuxDyn_sigmadeltaphi_1, &b_MuonsAuxDyn_sigmadeltaphi_1);
   fChain->SetBranchAddress("MuonsAuxDyn.sigmadeltatheta_1", &MuonsAuxDyn_sigmadeltatheta_1, &b_MuonsAuxDyn_sigmadeltatheta_1);
   fChain->SetBranchAddress("MuonsAuxDyn.ET_Core", &MuonsAuxDyn_ET_Core, &b_MuonsAuxDyn_ET_Core);
   fChain->SetBranchAddress("MuonsAuxDyn.ET_EMCore", &MuonsAuxDyn_ET_EMCore, &b_MuonsAuxDyn_ET_EMCore);
   fChain->SetBranchAddress("MuonsAuxDyn.ET_TileCore", &MuonsAuxDyn_ET_TileCore, &b_MuonsAuxDyn_ET_TileCore);
   fChain->SetBranchAddress("MuonsAuxDyn.ET_HECCore", &MuonsAuxDyn_ET_HECCore, &b_MuonsAuxDyn_ET_HECCore);
   fChain->SetBranchAddress("MuonsAuxDyn.nprecMatchedHitsPerChamberLayer", &MuonsAuxDyn_nprecMatchedHitsPerChamberLayer, &b_MuonsAuxDyn_nprecMatchedHitsPerChamberLayer);
   fChain->SetBranchAddress("MuonsAuxDyn.nphiMatchedHitsPerChamberLayer", &MuonsAuxDyn_nphiMatchedHitsPerChamberLayer, &b_MuonsAuxDyn_nphiMatchedHitsPerChamberLayer);
   fChain->SetBranchAddress("MuonsAuxDyn.ntrigEtaMatchedHitsPerChamberLayer", &MuonsAuxDyn_ntrigEtaMatchedHitsPerChamberLayer, &b_MuonsAuxDyn_ntrigEtaMatchedHitsPerChamberLayer);
   fChain->SetBranchAddress("MuonsAuxDyn.coreMuonEnergyCorrection", &MuonsAuxDyn_coreMuonEnergyCorrection, &b_MuonsAuxDyn_coreMuonEnergyCorrection);
   fChain->SetBranchAddress("MuonsAuxDyn.etconecoreConeEnergyCorrection", &MuonsAuxDyn_etconecoreConeEnergyCorrection, &b_MuonsAuxDyn_etconecoreConeEnergyCorrection);
   fChain->SetBranchAddress("MuonsAuxDyn.etconeCorrBitset", &MuonsAuxDyn_etconeCorrBitset, &b_MuonsAuxDyn_etconeCorrBitset);
   fChain->SetBranchAddress("MuonsAuxDyn.truthParticleLink", &MuonsAuxDyn_truthParticleLink_, &b_MuonsAuxDyn_truthParticleLink_);
   fChain->SetBranchAddress("MuonsAuxDyn.truthParticleLink.m_persKey", MuonsAuxDyn_truthParticleLink_m_persKey, &b_MuonsAuxDyn_truthParticleLink_m_persKey);
   fChain->SetBranchAddress("MuonsAuxDyn.truthParticleLink.m_persIndex", MuonsAuxDyn_truthParticleLink_m_persIndex, &b_MuonsAuxDyn_truthParticleLink_m_persIndex);
   fChain->SetBranchAddress("MuonsAuxDyn.topoetcone20", &MuonsAuxDyn_topoetcone20, &b_MuonsAuxDyn_topoetcone20);
   fChain->SetBranchAddress("MuonsAuxDyn.topoetcone30", &MuonsAuxDyn_topoetcone30, &b_MuonsAuxDyn_topoetcone30);
   fChain->SetBranchAddress("MuonsAuxDyn.truthType", &MuonsAuxDyn_truthType, &b_MuonsAuxDyn_truthType);
   fChain->SetBranchAddress("MuonsAuxDyn.topoetcone40", &MuonsAuxDyn_topoetcone40, &b_MuonsAuxDyn_topoetcone40);
   fChain->SetBranchAddress("MuonsAuxDyn.truthOrigin", &MuonsAuxDyn_truthOrigin, &b_MuonsAuxDyn_truthOrigin);
   fChain->SetBranchAddress("MuonsAuxDyn.neflowisol20", &MuonsAuxDyn_neflowisol20, &b_MuonsAuxDyn_neflowisol20);
   fChain->SetBranchAddress("MuonsAuxDyn.neflowisol30", &MuonsAuxDyn_neflowisol30, &b_MuonsAuxDyn_neflowisol30);
   fChain->SetBranchAddress("MuonsAuxDyn.CT_ET_Core", &MuonsAuxDyn_CT_ET_Core, &b_MuonsAuxDyn_CT_ET_Core);
   fChain->SetBranchAddress("PrimaryVerticesAuxDyn.sumPt2", &PrimaryVerticesAuxDyn_sumPt2, &b_PrimaryVerticesAuxDyn_sumPt2);
   Notify();
   InitSkim();
}

Bool_t xAODScan::Notify()
{
   // The Notify() function is called when a new file is opened. This
   // can be either for a new TTree in a TChain or when when a new TTree
   // is started when using PROOF. It is normally not necessary to make changes
   // to the generated code, but the routine can be extended by the
   // user if needed. The return value is currently not used.

   return kTRUE;
}

void xAODScan::InitSkim()
{
   m_SkimTree->SetBranch("InDetTrackParticlesAux.d0", &InDetTrackParticlesAux_d0);
   m_SkimTree->SetBranch("InDetTrackParticlesAux.z0", &InDetTrackParticlesAux_z0);
   m_SkimTree->SetBranch("InDetTrackParticlesAux.phi", &InDetTrackParticlesAux_phi);
   m_SkimTree->SetBranch("InDetTrackParticlesAux.theta", &InDetTrackParticlesAux_theta);
   m_SkimTree->SetBranch("InDetTrackParticlesAux.qOverP", &InDetTrackParticlesAux_qOverP);
   m_SkimTree->SetBranch("InDetTrackParticlesAux.definingParametersCovMatrix", &InDetTrackParticlesAux_definingParametersCovMatrix);
   m_SkimTree->SetBranch("InDetTrackParticlesAux.vx", &InDetTrackParticlesAux_vx);
   m_SkimTree->SetBranch("InDetTrackParticlesAux.vy", &InDetTrackParticlesAux_vy);
   m_SkimTree->SetBranch("InDetTrackParticlesAux.vz", &InDetTrackParticlesAux_vz);
   m_SkimTree->SetBranch("InDetTrackParticlesAux.radiusOfFirstHit", &InDetTrackParticlesAux_radiusOfFirstHit);
   m_SkimTree->SetBranch("InDetTrackParticlesAux.identifierOfFirstHit", &InDetTrackParticlesAux_identifierOfFirstHit);
   m_SkimTree->SetBranch("InDetTrackParticlesAux.beamlineTiltX", &InDetTrackParticlesAux_beamlineTiltX);
   m_SkimTree->SetBranch("InDetTrackParticlesAux.beamlineTiltY", &InDetTrackParticlesAux_beamlineTiltY);
   m_SkimTree->SetBranch("InDetTrackParticlesAux.hitPattern", &InDetTrackParticlesAux_hitPattern);
   m_SkimTree->SetBranch("InDetTrackParticlesAux.chiSquared", &InDetTrackParticlesAux_chiSquared);
   m_SkimTree->SetBranch("InDetTrackParticlesAux.numberDoF", &InDetTrackParticlesAux_numberDoF);
   m_SkimTree->SetBranch("InDetTrackParticlesAux.trackFitter", &InDetTrackParticlesAux_trackFitter);
   m_SkimTree->SetBranch("InDetTrackParticlesAux.particleHypothesis", &InDetTrackParticlesAux_particleHypothesis);
   m_SkimTree->SetBranch("InDetTrackParticlesAux.trackProperties", &InDetTrackParticlesAux_trackProperties);
   m_SkimTree->SetBranch("InDetTrackParticlesAux.patternRecoInfo", &InDetTrackParticlesAux_patternRecoInfo);
   m_SkimTree->SetBranch("InDetTrackParticlesAux.numberOfContribPixelLayers", &InDetTrackParticlesAux_numberOfContribPixelLayers);
   m_SkimTree->SetBranch("InDetTrackParticlesAux.numberOfInnermostPixelLayerHits", &InDetTrackParticlesAux_numberOfInnermostPixelLayerHits);
   m_SkimTree->SetBranch("InDetTrackParticlesAux.numberOfInnermostPixelLayerOutliers", &InDetTrackParticlesAux_numberOfInnermostPixelLayerOutliers);
   m_SkimTree->SetBranch("InDetTrackParticlesAux.numberOfInnermostPixelLayerSharedHits", &InDetTrackParticlesAux_numberOfInnermostPixelLayerSharedHits);
   m_SkimTree->SetBranch("InDetTrackParticlesAux.numberOfInnermostPixelLayerSplitHits", &InDetTrackParticlesAux_numberOfInnermostPixelLayerSplitHits);
   m_SkimTree->SetBranch("InDetTrackParticlesAux.expectInnermostPixelLayerHit", &InDetTrackParticlesAux_expectInnermostPixelLayerHit);
   m_SkimTree->SetBranch("InDetTrackParticlesAux.numberOfNextToInnermostPixelLayerHits", &InDetTrackParticlesAux_numberOfNextToInnermostPixelLayerHits);
   m_SkimTree->SetBranch("InDetTrackParticlesAux.numberOfNextToInnermostPixelLayerOutliers", &InDetTrackParticlesAux_numberOfNextToInnermostPixelLayerOutliers);
   m_SkimTree->SetBranch("InDetTrackParticlesAux.numberOfNextToInnermostPixelLayerSharedHits", &InDetTrackParticlesAux_numberOfNextToInnermostPixelLayerSharedHits);
   m_SkimTree->SetBranch("InDetTrackParticlesAux.numberOfNextToInnermostPixelLayerSplitHits", &InDetTrackParticlesAux_numberOfNextToInnermostPixelLayerSplitHits);
   m_SkimTree->SetBranch("InDetTrackParticlesAux.expectNextToInnermostPixelLayerHit", &InDetTrackParticlesAux_expectNextToInnermostPixelLayerHit);
   m_SkimTree->SetBranch("InDetTrackParticlesAux.numberOfPixelHits", &InDetTrackParticlesAux_numberOfPixelHits);
   m_SkimTree->SetBranch("InDetTrackParticlesAux.numberOfPixelOutliers", &InDetTrackParticlesAux_numberOfPixelOutliers);
   m_SkimTree->SetBranch("InDetTrackParticlesAux.numberOfPixelHoles", &InDetTrackParticlesAux_numberOfPixelHoles);
   m_SkimTree->SetBranch("InDetTrackParticlesAux.numberOfPixelSharedHits", &InDetTrackParticlesAux_numberOfPixelSharedHits);
   m_SkimTree->SetBranch("InDetTrackParticlesAux.numberOfPixelSplitHits", &InDetTrackParticlesAux_numberOfPixelSplitHits);
   m_SkimTree->SetBranch("InDetTrackParticlesAux.numberOfGangedPixels", &InDetTrackParticlesAux_numberOfGangedPixels);
   m_SkimTree->SetBranch("InDetTrackParticlesAux.numberOfGangedFlaggedFakes", &InDetTrackParticlesAux_numberOfGangedFlaggedFakes);
   m_SkimTree->SetBranch("InDetTrackParticlesAux.numberOfPixelDeadSensors", &InDetTrackParticlesAux_numberOfPixelDeadSensors);
   m_SkimTree->SetBranch("InDetTrackParticlesAux.numberOfPixelSpoiltHits", &InDetTrackParticlesAux_numberOfPixelSpoiltHits);
   m_SkimTree->SetBranch("InDetTrackParticlesAux.numberOfDBMHits", &InDetTrackParticlesAux_numberOfDBMHits);
   m_SkimTree->SetBranch("InDetTrackParticlesAux.numberOfSCTHits", &InDetTrackParticlesAux_numberOfSCTHits);
   m_SkimTree->SetBranch("InDetTrackParticlesAux.numberOfSCTOutliers", &InDetTrackParticlesAux_numberOfSCTOutliers);
   m_SkimTree->SetBranch("InDetTrackParticlesAux.numberOfSCTHoles", &InDetTrackParticlesAux_numberOfSCTHoles);
   m_SkimTree->SetBranch("InDetTrackParticlesAux.numberOfSCTDoubleHoles", &InDetTrackParticlesAux_numberOfSCTDoubleHoles);
   m_SkimTree->SetBranch("InDetTrackParticlesAux.numberOfSCTSharedHits", &InDetTrackParticlesAux_numberOfSCTSharedHits);
   m_SkimTree->SetBranch("InDetTrackParticlesAux.numberOfSCTDeadSensors", &InDetTrackParticlesAux_numberOfSCTDeadSensors);
   m_SkimTree->SetBranch("InDetTrackParticlesAux.numberOfSCTSpoiltHits", &InDetTrackParticlesAux_numberOfSCTSpoiltHits);
   m_SkimTree->SetBranch("InDetTrackParticlesAux.numberOfTRTHits", &InDetTrackParticlesAux_numberOfTRTHits);
   m_SkimTree->SetBranch("InDetTrackParticlesAux.numberOfTRTOutliers", &InDetTrackParticlesAux_numberOfTRTOutliers);
   m_SkimTree->SetBranch("InDetTrackParticlesAux.numberOfTRTHoles", &InDetTrackParticlesAux_numberOfTRTHoles);
   m_SkimTree->SetBranch("InDetTrackParticlesAux.numberOfTRTHighThresholdHits", &InDetTrackParticlesAux_numberOfTRTHighThresholdHits);
   m_SkimTree->SetBranch("InDetTrackParticlesAux.numberOfTRTHighThresholdHitsTotal", &InDetTrackParticlesAux_numberOfTRTHighThresholdHitsTotal);
   m_SkimTree->SetBranch("InDetTrackParticlesAux.numberOfTRTHighThresholdOutliers", &InDetTrackParticlesAux_numberOfTRTHighThresholdOutliers);
   m_SkimTree->SetBranch("InDetTrackParticlesAux.numberOfTRTDeadStraws", &InDetTrackParticlesAux_numberOfTRTDeadStraws);
   m_SkimTree->SetBranch("InDetTrackParticlesAux.numberOfTRTTubeHits", &InDetTrackParticlesAux_numberOfTRTTubeHits);
   m_SkimTree->SetBranch("InDetTrackParticlesAux.numberOfTRTXenonHits", &InDetTrackParticlesAux_numberOfTRTXenonHits);
   m_SkimTree->SetBranch("InDetTrackParticlesAux.numberOfTRTSharedHits", &InDetTrackParticlesAux_numberOfTRTSharedHits);
   m_SkimTree->SetBranch("InDetTrackParticlesAux.numberOfPrecisionLayers", &InDetTrackParticlesAux_numberOfPrecisionLayers);
   m_SkimTree->SetBranch("InDetTrackParticlesAux.numberOfPrecisionHoleLayers", &InDetTrackParticlesAux_numberOfPrecisionHoleLayers);
   m_SkimTree->SetBranch("InDetTrackParticlesAux.numberOfPhiLayers", &InDetTrackParticlesAux_numberOfPhiLayers);
   m_SkimTree->SetBranch("InDetTrackParticlesAux.numberOfPhiHoleLayers", &InDetTrackParticlesAux_numberOfPhiHoleLayers);
   m_SkimTree->SetBranch("InDetTrackParticlesAux.numberOfTriggerEtaLayers", &InDetTrackParticlesAux_numberOfTriggerEtaLayers);
   m_SkimTree->SetBranch("InDetTrackParticlesAux.numberOfTriggerEtaHoleLayers", &InDetTrackParticlesAux_numberOfTriggerEtaHoleLayers);
   m_SkimTree->SetBranch("InDetTrackParticlesAux.numberOfOutliersOnTrack", &InDetTrackParticlesAux_numberOfOutliersOnTrack);
   m_SkimTree->SetBranch("InDetTrackParticlesAux.standardDeviationOfChi2OS", &InDetTrackParticlesAux_standardDeviationOfChi2OS);
   m_SkimTree->SetBranch("InDetTrackParticlesAux.eProbabilityComb", &InDetTrackParticlesAux_eProbabilityComb);
   m_SkimTree->SetBranch("InDetTrackParticlesAux.eProbabilityHT", &InDetTrackParticlesAux_eProbabilityHT);
   m_SkimTree->SetBranch("InDetTrackParticlesAux.pixeldEdx", &InDetTrackParticlesAux_pixeldEdx);
   m_SkimTree->SetBranch("InDetTrackParticlesAux.numberOfUsedHitsdEdx", &InDetTrackParticlesAux_numberOfUsedHitsdEdx);
   m_SkimTree->SetBranch("InDetTrackParticlesAux.numberOfIBLOverflowsdEdx", &InDetTrackParticlesAux_numberOfIBLOverflowsdEdx);
   m_SkimTree->SetBranch("InDetTrackParticlesAux.TRTTrackOccupancy", &InDetTrackParticlesAux_TRTTrackOccupancy);
}

void xAODScan::Show(Long64_t entry)
{
// Print contents of entry.
// If entry is not specified, print current entry
   if (!fChain) return;
   fChain->Show(entry);
}
Int_t xAODScan::Cut(Long64_t entry)
{
// This function may be called from Loop.
// returns  1 if entry is accepted.
// returns -1 otherwise.
   return 1;
}
#endif // #ifdef xAODScan_cxx
