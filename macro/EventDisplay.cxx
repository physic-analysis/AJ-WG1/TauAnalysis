
#include <iostream>
#include <TH1.h>
#include <TH2.h>
#include <TFile.h>

#include <TTree.h>
#include <TStyle.h>
#include <TText.h>
#include <TCanvas.h>
#include <TLegend.h>
#include <TLine.h>

#include "tools/text.h"
#include "tools/PixelLayer.h"
#include "ModeSelector.h"

struct EventCounter
{
  EventCounter(){
    all=0;
    hadronic=0;
    eta_1_0=0;
    prong1=0;
    prong3=0;
    decayRadLower33_25=0;
    decayRadLower50_5=0;
    decayRadLower88_5=0;
    decayRadLower122_5=0;
    decayRadLower122_5=0;
    decayRadLarger122_5=0;
    TauHadOneProngPiNuInner=0;
    TauHadOneProngPiNuOuter=0;
  };
  int all;
  int hadronic;
  int eta_1_0;
  int prong1;
  int prong3;
  int decayRadLower33_25;
  int decayRadLower50_5;
  int decayRadLower88_5;
  int decayRadLower122_5;
  int decayRadLarger122_5;
  int TauHadOneProngPiNuInner;
  int TauHadOneProngPiNuOuter;
};

void EventDisplay()
{
  //std::string simulator="FullG4_LongLived";
  std::string simulator="MC12G4";
  //std::string path = "/afs/cern.ch/user/k/ktakeda/workspace/public/atlas-phys/AJ/WG1/SingleParticle/AnalysisArea/run/";
  std::string path = "../Data/";
  std::string file_name = "MyTauNtuple." + simulator + "_20181107.outputs.root";

  TFile* file = new TFile( (path+file_name).c_str(), "read");
  TTree* tree = (TTree*)file->Get("m_trueTauTree");


  int Truth_part_numChargedTracks       = 0;
  int Truth_part_numChargedLeptonTracks = 0;

  //
  ///////////////////////////////////////////
  // Truth tau
  Double_t Truth_tau_pt; 
  Double_t Truth_tau_eta; 
  Double_t Truth_tau_phi; 
  Double_t Truth_tau_m; 
  Bool_t   Truth_tau_isLeptonic = false; 
  Bool_t   Truth_tau_isHadronic = false; 
  Int_t    Truth_tau_numTracks =0; 
  Bool_t   Truth_tau_isMatchedWithReco = false; 
  Double_t Truth_tau_decayRad = 0; 
  //
  ///////////////////////////////////////////
  // Truth daughter event
  std::vector<int>*          Truth_dau_pdgId=0;
  //
  Bool_t   tt_truth_leptonic_electron = false; 
  Bool_t   tt_truth_leptonic_muon = false; 
  Bool_t   tt_truth_leptonic_tau = false; 
  Double_t tt_truth_neutrino_pt; 
  //
  Int_t                      eventNumber;
  //
  std::vector<double>*       Truth_track_pt = 0;
  std::vector<double>*       Truth_track_eta = 0;
  std::vector<double>*       Truth_track_phi = 0;
  std::vector<double>*       Truth_track_m = 0;
  std::vector<double>*       Truth_track_charge = 0;
  //
  ///////////////////////////////////////////
  // Truth neutrino information 
  std::vector<int>*          Truth_neutrino_pdgId=0;
  std::vector<double>*       Truth_neutrino_pt=0;
  std::vector<double>*       Truth_neutrino_eta=0;
  std::vector<double>*       Truth_neutrino_phi=0;
  //
  Float_t                    Truth_prodVtx_x;
  Float_t                    Truth_prodVtx_y;
  Float_t                    Truth_prodVtx_z;
  Float_t                    Truth_prodVtx_eta;
  Float_t                    Truth_prodVtx_phi;
  Float_t                    Truth_prodVtx_perp;
  //
  //
  Float_t                    Truth_decVtx_x;
  Float_t                    Truth_decVtx_y;
  Float_t                    Truth_decVtx_z;
  Float_t                    Truth_decVtx_t;
  Float_t                    Truth_decVtx_eta;
  Float_t                    Truth_decVtx_phi;
  Float_t                    Truth_decVtx_perp;
  //
  Int_t                      InDet_numTracks;        //! the number of tracks passed pixel layer per events
  std::vector<double>*       InDet_track_pt          = 0;
  std::vector<double>*       InDet_track_eta         = 0;
  std::vector<double>*       InDet_track_phi         = 0;
  std::vector<double>*       InDet_track_m           = 0;
  std::vector<double>*       InDet_track_d0           = 0;
  std::vector<double>*       InDet_track_z0           = 0;
  std::vector<float>*        InDet_track_charge       = 0;
  std::vector<int>*          InDet_track_numberOfIBLHits = 0;
  std::vector<int>*          InDet_track_numberOfBLayerHits = 0;
  std::vector<int>*          InDet_track_numberOfPixelL1Hits = 0;
  std::vector<int>*          InDet_track_numberOfPixelL2Hits = 0;
  std::vector<int>*          InDet_track_numberOfPixelHits = 0;
  std::vector<int>*          InDet_track_numberOfSCTHits   = 0;
  //
  ///////////////////////////////////////////
  // Pixel raw cluster information 
  std::vector<std::vector<double>>* InDet_pixel_raw_ClustersEta    = 0;
  std::vector<std::vector<double>>* InDet_pixel_raw_ClustersPhi    = 0;
  std::vector<std::vector<double>>* InDet_pixel_raw_ClustersCharge = 0;
  std::vector<std::vector<double>>* InDet_pixel_raw_globalX        = 0;
  std::vector<std::vector<double>>* InDet_pixel_raw_globalY        = 0;
  std::vector<std::vector<double>>* InDet_pixel_raw_globalZ        = 0;
  std::vector<std::vector<double> >*    InDet_pixel_raw_IB_energyDeposit= 0;
  std::vector<std::vector<double> >*    InDet_pixel_raw_BL_energyDeposit= 0;
  std::vector<std::vector<double> >*    InDet_pixel_raw_L1_energyDeposit= 0;
  std::vector<std::vector<double> >*    InDet_pixel_raw_L2_energyDeposit= 0;
  std::vector<std::vector<double> >*    InDet_pixel_raw_IB_barcode= 0;
  std::vector<std::vector<double> >*    InDet_pixel_raw_BL_barcode= 0;
  std::vector<std::vector<double> >*    InDet_pixel_raw_L1_barcode= 0;
  std::vector<std::vector<double> >*    InDet_pixel_raw_L2_barcode= 0;
  std::vector<std::vector<double> >*    InDet_pixel_raw_IB_pdgId= 0;
  std::vector<std::vector<double> >*    InDet_pixel_raw_BL_pdgId= 0;
  std::vector<std::vector<double> >*    InDet_pixel_raw_L1_pdgId= 0;
  std::vector<std::vector<double> >*    InDet_pixel_raw_L2_pdgId= 0;
  //
  ///////////////////////////////////////////
  // Pixel cluster information
  Int_t                             InDet_pixel_numAllOfClusters;
  std::vector<std::vector<double>>* InDet_pixel_IBLClustersEta = 0;
  std::vector<std::vector<double>>* InDet_pixel_BLayerClustersEta = 0;
  std::vector<std::vector<double>>* InDet_pixel_L1ClustersEta = 0;
  std::vector<std::vector<double>>* InDet_pixel_L2ClustersEta = 0;
  //
  std::vector<std::vector<double>>* InDet_pixel_IBLClustersPhi = 0;
  std::vector<std::vector<double>>* InDet_pixel_BLayerClustersPhi = 0;
  std::vector<std::vector<double>>* InDet_pixel_L1ClustersPhi = 0;
  std::vector<std::vector<double>>* InDet_pixel_L2ClustersPhi = 0;
  //
  std::vector<std::vector<double>>* InDet_pixel_IBLClustersCharge = 0;
  std::vector<std::vector<double>>* InDet_pixel_BLayerClustersCharge = 0;
  std::vector<std::vector<double>>* InDet_pixel_L1ClustersCharge = 0;
  std::vector<std::vector<double>>* InDet_pixel_L2ClustersCharge = 0;
  ///////////////////////////////////////////
  // Pixel cluster information (not considered the tau)
  Int_t                      Pix_numCluster_IBL;   //! the number of clusters on the IBL
  Int_t                      Pix_numCluster_BLayer;//! the number of clusters on the B-Layer
  Int_t                      Pix_numCluster_L1;    //! the number of clusters on the pixel 2nd layer 
  Int_t                      Pix_numCluster_L2;    //! the number of clusters on the pixel 3rd layer
  //
  Int_t    Reco_tau_numTracks = 0; 
  Bool_t   Reco_tau_isSelected;
  Double_t Reco_tau_pt; 
  Double_t Reco_tau_eta; 
  Double_t Reco_tau_phi; 
  Double_t Reco_tau_e; 

  // Tracks from a reco-tau
  std::vector<double>*          Reco_track_pt=0;  
  std::vector<double>*          Reco_track_eta=0;  
  std::vector<double>*          Reco_track_phi=0;  
  std::vector<double>*          Reco_track_e=0;   
  std::vector<double>*          Reco_track_m=0;   
  std::vector<double>*          Reco_track_d0=0;   
  std::vector<double>*          Reco_track_z0=0;   
  std::vector<int>*             Reco_track_numberOfPixelHits=0;

  std::vector<unsigned int>* tt_numberOfContribPixelLayers      =0 ; //!
  std::vector<unsigned int>* tt_numberOfBLayerHits              =0 ; //!
  std::vector<unsigned int>* tt_numberOfPixelHits               =0 ; //!
  std::vector<unsigned int>* tt_numberOfSCTHits                 =0 ; //!
  std::vector<unsigned int>* tt_numberOfTRTHits                 =0 ; //!
  std::vector<unsigned int>* tt_numberOfBLayerSharedHits        =0 ; //!
  std::vector<unsigned int>* tt_numberOfPixelSharedHits         =0 ; //!
  std::vector<unsigned int>* tt_numberOfSCTSharedHits           =0 ; //!
  std::vector<unsigned int>* tt_numberOfTRTSharedHits           =0 ; //!
  std::vector<unsigned int>* tt_numberOfPixelHoles              =0 ; //!
  std::vector<unsigned int>* tt_numberOfSCTHoles                =0 ; //!
  std::vector<unsigned int>* tt_numberOfSCTDoubleHoles          =0 ; //!
  std::vector<unsigned int>* tt_numberOfTRTHoles                =0 ; //!
  std::vector<unsigned int>* tt_numberOfBLayerOutliers          =0 ; //!
  std::vector<unsigned int>* tt_numberOfPixelOutliers           =0 ; //!
  std::vector<unsigned int>* tt_numberOfSCTOutliers             =0 ; //!
  std::vector<unsigned int>* tt_numberOfTRTOutliers             =0 ; //!
  std::vector<unsigned int>* tt_numberOfPixelSpoiltHits         =0 ; //!
  std::vector<unsigned int>* tt_numberOfSCTSpoiltHits           =0 ; //!
  std::vector<unsigned int>* tt_expectBLayerHit                 =0 ; //!
  std::vector<unsigned int>* tt_expectInnermostPixelLayerHit    =0 ; //!
  std::vector<unsigned int>* tt_numberOfInnermostPixelLayerHits =0 ; //!
  std::vector<unsigned int>* tt_numberOfNextToInnermostPixelLayerHits =0 ; //!
  std::vector<unsigned int>* tt_numberOfGangedPixels            =0 ; //!
  std::vector<unsigned int>* tt_numberOfGangedFlaggedFakes      =0 ; //!
  std::vector<unsigned int>* tt_numberOfPixelDeadSensors        =0 ; //!
  std::vector<unsigned int>* tt_numberOfSCTDeadSensors          =0 ; //!
  std::vector<unsigned int>* tt_numberOfTRTDeadStraws           =0 ; //!
  // Pixel raw cluster
  std::vector<unsigned int>* Reco_track_numberOfInnermostPixelHits       = 0 ;//!
  std::vector<unsigned int>* Reco_track_numberOfNextToInnermostPixelHits = 0 ;//!
  std::vector<unsigned int>* Reco_track_numberOfPixelL1Hits                   = 0 ;//!
  std::vector<unsigned int>* Reco_track_numberOfPixelL2Hits                   = 0 ;//!

  tree->SetBranchAddress("eventNumber",               &eventNumber);
  //
  tree->SetBranchAddress("Truth_part_numChargedTracks",                  &Truth_part_numChargedTracks);
  tree->SetBranchAddress("Truth_part_numChargedLeptonTracks",            &Truth_part_numChargedLeptonTracks);
  //
  tree->SetBranchAddress("tt_truth_neutrino_pt",              &tt_truth_neutrino_pt);
  tree->SetBranchAddress("Truth_tau_isLeptonic",           &Truth_tau_isLeptonic);
  tree->SetBranchAddress("tt_truth_leptonic_electron",        &tt_truth_leptonic_electron);
  tree->SetBranchAddress("tt_truth_leptonic_muon",            &tt_truth_leptonic_muon);
  tree->SetBranchAddress("tt_truth_leptonic_tau",             &tt_truth_leptonic_tau);
  tree->SetBranchAddress("Truth_tau_isHadronic",           &Truth_tau_isHadronic);
  tree->SetBranchAddress("Truth_tau_isMatchedWithReco",                    &Truth_tau_isMatchedWithReco);
  tree->SetBranchAddress("Truth_tau_pt",                       &Truth_tau_pt);
  tree->SetBranchAddress("Truth_tau_eta",                      &Truth_tau_eta);
  tree->SetBranchAddress("Truth_tau_phi",                      &Truth_tau_phi);
  tree->SetBranchAddress("Truth_tau_m",                      &Truth_tau_m);
  tree->SetBranchAddress("Truth_tau_decayRad",             &Truth_tau_decayRad);
  tree->SetBranchAddress("Truth_tau_numTracks",                   &Truth_tau_numTracks);
  //
  tree->SetBranchAddress("Truth_dau_pdgId",               &Truth_dau_pdgId);
  //
  tree->SetBranchAddress("Truth_neutrino_pdgId",          &Truth_neutrino_pdgId);
  tree->SetBranchAddress("Truth_neutrino_pt",             &Truth_neutrino_pt);
  tree->SetBranchAddress("Truth_neutrino_eta",            &Truth_neutrino_eta);
  tree->SetBranchAddress("Truth_neutrino_phi",            &Truth_neutrino_phi);
  //
  tree->SetBranchAddress("Truth_track_pt",                &Truth_track_pt);
  tree->SetBranchAddress("Truth_track_eta",               &Truth_track_eta);
  tree->SetBranchAddress("Truth_track_phi",               &Truth_track_phi);
  tree->SetBranchAddress("Truth_track_m",                 &Truth_track_m);
  tree->SetBranchAddress("Truth_track_charge",            &Truth_track_charge);
  //
  tree->SetBranchAddress("Truth_prodVtx_x",               &Truth_prodVtx_x);
  tree->SetBranchAddress("Truth_prodVtx_y",               &Truth_prodVtx_y);
  tree->SetBranchAddress("Truth_prodVtx_z",               &Truth_prodVtx_z);
  tree->SetBranchAddress("Truth_prodVtx_eta",             &Truth_prodVtx_eta);
  tree->SetBranchAddress("Truth_prodVtx_phi",             &Truth_prodVtx_phi);
  tree->SetBranchAddress("Truth_prodVtx_perp",            &Truth_prodVtx_perp);
  //
  tree->SetBranchAddress("Truth_decVtx_x",               &Truth_decVtx_x);
  tree->SetBranchAddress("Truth_decVtx_y",               &Truth_decVtx_y);
  tree->SetBranchAddress("Truth_decVtx_z",               &Truth_decVtx_z);
  tree->SetBranchAddress("Truth_decVtx_t",               &Truth_decVtx_t);
  tree->SetBranchAddress("Truth_decVtx_eta",             &Truth_decVtx_eta);
  tree->SetBranchAddress("Truth_decVtx_phi",             &Truth_decVtx_phi);
  tree->SetBranchAddress("Truth_decVtx_perp",            &Truth_decVtx_perp);
  //
  tree->SetBranchAddress("InDet_numTracks",               &InDet_numTracks);
  tree->SetBranchAddress("InDet_track_pt",                &InDet_track_pt);
  tree->SetBranchAddress("InDet_track_eta",               &InDet_track_eta);
  tree->SetBranchAddress("InDet_track_phi",               &InDet_track_phi);
  tree->SetBranchAddress("InDet_track_m",                 &InDet_track_m);
  tree->SetBranchAddress("InDet_track_d0",                &InDet_track_d0);
  tree->SetBranchAddress("InDet_track_z0",                &InDet_track_z0);
  tree->SetBranchAddress("InDet_track_charge",            &InDet_track_charge);
  tree->SetBranchAddress("InDet_track_numberOfIBLHits",       &InDet_track_numberOfIBLHits);
  tree->SetBranchAddress("InDet_track_numberOfBLayerHits",    &InDet_track_numberOfBLayerHits);
  tree->SetBranchAddress("InDet_track_numberOfPixelL1Hits",        &InDet_track_numberOfPixelL1Hits);
  tree->SetBranchAddress("InDet_track_numberOfPixelL2Hits",        &InDet_track_numberOfPixelL2Hits);
  tree->SetBranchAddress("InDet_track_numberOfPixelHits", &InDet_track_numberOfPixelHits);
  tree->SetBranchAddress("InDet_track_numberOfSCTHits",   &InDet_track_numberOfSCTHits);
  //
  ///////////////////////////////////////////
  // Pixel raw cluster information 
  tree->SetBranchAddress("InDet_pixel_raw_ClustersEta",        &InDet_pixel_raw_ClustersEta);
  tree->SetBranchAddress("InDet_pixel_raw_ClustersPhi",        &InDet_pixel_raw_ClustersPhi);
  tree->SetBranchAddress("InDet_pixel_raw_ClustersCharge",     &InDet_pixel_raw_ClustersCharge);
  tree->SetBranchAddress("InDet_pixel_raw_globalX",            &InDet_pixel_raw_globalX);
  tree->SetBranchAddress("InDet_pixel_raw_globalY",            &InDet_pixel_raw_globalY);
  tree->SetBranchAddress("InDet_pixel_raw_globalZ",            &InDet_pixel_raw_globalZ);
  tree->SetBranchAddress("InDet_pixel_raw_IB_energyDeposit",   &InDet_pixel_raw_IB_energyDeposit);
  tree->SetBranchAddress("InDet_pixel_raw_BL_energyDeposit",   &InDet_pixel_raw_BL_energyDeposit);
  tree->SetBranchAddress("InDet_pixel_raw_L1_energyDeposit",   &InDet_pixel_raw_L1_energyDeposit);
  tree->SetBranchAddress("InDet_pixel_raw_L2_energyDeposit",   &InDet_pixel_raw_L2_energyDeposit);
  tree->SetBranchAddress("InDet_pixel_raw_IB_barcode",         &InDet_pixel_raw_IB_barcode);
  tree->SetBranchAddress("InDet_pixel_raw_BL_barcode",         &InDet_pixel_raw_BL_barcode);
  tree->SetBranchAddress("InDet_pixel_raw_L1_barcode",         &InDet_pixel_raw_L1_barcode);
  tree->SetBranchAddress("InDet_pixel_raw_L2_barcode",         &InDet_pixel_raw_L2_barcode);
  tree->SetBranchAddress("InDet_pixel_raw_IB_pdgId",           &InDet_pixel_raw_IB_pdgId);
  tree->SetBranchAddress("InDet_pixel_raw_BL_pdgId",           &InDet_pixel_raw_BL_pdgId);
  tree->SetBranchAddress("InDet_pixel_raw_L1_pdgId",           &InDet_pixel_raw_L1_pdgId);
  tree->SetBranchAddress("InDet_pixel_raw_L2_pdgId",           &InDet_pixel_raw_L2_pdgId);
  //
  ///////////////////////////////////////////
  // Pixel cluster information 
  tree->SetBranchAddress("InDet_pixel_numAllOfClusters",  &InDet_pixel_numAllOfClusters);
  tree->SetBranchAddress("InDet_pixel_IBLClustersEta",    &InDet_pixel_IBLClustersEta);
  tree->SetBranchAddress("InDet_pixel_BLayerClustersEta", &InDet_pixel_BLayerClustersEta);
  tree->SetBranchAddress("InDet_pixel_L1ClustersEta",     &InDet_pixel_L1ClustersEta);
  tree->SetBranchAddress("InDet_pixel_L2ClustersEta",     &InDet_pixel_L2ClustersEta);
  tree->SetBranchAddress("InDet_pixel_IBLClustersPhi",    &InDet_pixel_IBLClustersPhi);
  tree->SetBranchAddress("InDet_pixel_BLayerClustersPhi", &InDet_pixel_BLayerClustersPhi);
  tree->SetBranchAddress("InDet_pixel_L1ClustersPhi",     &InDet_pixel_L1ClustersPhi);
  tree->SetBranchAddress("InDet_pixel_L2ClustersPhi",     &InDet_pixel_L2ClustersPhi);
  tree->SetBranchAddress("InDet_pixel_IBLClustersCharge",    &InDet_pixel_IBLClustersCharge);
  tree->SetBranchAddress("InDet_pixel_BLayerClustersCharge", &InDet_pixel_BLayerClustersCharge);
  tree->SetBranchAddress("InDet_pixel_L1ClustersCharge",     &InDet_pixel_L1ClustersCharge);
  tree->SetBranchAddress("InDet_pixel_L2ClustersCharge",     &InDet_pixel_L2ClustersCharge);
  //
  tree->SetBranchAddress("Pix_numCluster_IBL",    &Pix_numCluster_IBL);
  tree->SetBranchAddress("Pix_numCluster_BLayer", &Pix_numCluster_BLayer);
  tree->SetBranchAddress("Pix_numCluster_L1",     &Pix_numCluster_L1);
  tree->SetBranchAddress("Pix_numCluster_L2",     &Pix_numCluster_L2);
  //
  // Reconstructed tau tracks
  tree->SetBranchAddress("Reco_tau_pt",                        &Reco_tau_pt);
  tree->SetBranchAddress("Reco_tau_eta",                       &Reco_tau_eta);
  tree->SetBranchAddress("Reco_tau_phi",                       &Reco_tau_phi);
  tree->SetBranchAddress("Reco_tau_e",                         &Reco_tau_e);
  tree->SetBranchAddress("Reco_tau_numTracks",                 &Reco_tau_numTracks);
  tree->SetBranchAddress("Reco_tau_isSelected",                &Reco_tau_isSelected);
  //
  tree->SetBranchAddress("tt_numberOfContribPixelLayers",      &tt_numberOfContribPixelLayers);
  tree->SetBranchAddress("tt_numberOfBLayerHits",              &tt_numberOfBLayerHits       );
  tree->SetBranchAddress("tt_numberOfPixelHits",               &tt_numberOfPixelHits        );
  tree->SetBranchAddress("tt_numberOfSCTHits",                 &tt_numberOfSCTHits          );
  tree->SetBranchAddress("tt_numberOfTRTHits",                 &tt_numberOfTRTHits          );
  tree->SetBranchAddress("tt_numberOfBLayerSharedHits",        &tt_numberOfBLayerSharedHits        );
  tree->SetBranchAddress("tt_numberOfPixelSharedHits",         &tt_numberOfPixelSharedHits        );
  tree->SetBranchAddress("tt_numberOfSCTSharedHits",           &tt_numberOfSCTSharedHits        );
  tree->SetBranchAddress("tt_numberOfTRTSharedHits",           &tt_numberOfTRTSharedHits        );
  tree->SetBranchAddress("tt_numberOfPixelHoles",              &tt_numberOfPixelHoles        );
  tree->SetBranchAddress("tt_numberOfSCTHoles",                &tt_numberOfSCTHoles         );
  tree->SetBranchAddress("tt_numberOfSCTDoubleHoles",          &tt_numberOfSCTDoubleHoles        );
  tree->SetBranchAddress("tt_numberOfTRTHoles",                &tt_numberOfTRTHoles         );
  tree->SetBranchAddress("tt_numberOfBLayerOutliers",          &tt_numberOfBLayerOutliers        );
  tree->SetBranchAddress("tt_numberOfPixelOutliers",           &tt_numberOfPixelOutliers        );
  tree->SetBranchAddress("tt_numberOfSCTOutliers",             &tt_numberOfSCTOutliers        );
  tree->SetBranchAddress("tt_numberOfTRTOutliers",             &tt_numberOfTRTOutliers        );
  tree->SetBranchAddress("tt_numberOfPixelSpoiltHits",         &tt_numberOfPixelSpoiltHits        );
  tree->SetBranchAddress("tt_numberOfSCTSpoiltHits",           &tt_numberOfSCTSpoiltHits        );
  tree->SetBranchAddress("tt_expectBLayerHit",                 &tt_expectBLayerHit          );
  tree->SetBranchAddress("tt_expectInnermostPixelLayerHit",    &tt_expectInnermostPixelLayerHit              );
  tree->SetBranchAddress("tt_numberOfInnermostPixelLayerHits", &tt_numberOfInnermostPixelLayerHits              );
  tree->SetBranchAddress("tt_numberOfNextToInnermostPixelLayerHits", &tt_numberOfNextToInnermostPixelLayerHits              );
  tree->SetBranchAddress("tt_numberOfGangedPixels",            &tt_numberOfGangedPixels              );
  tree->SetBranchAddress("tt_numberOfGangedFlaggedFakes",      &tt_numberOfGangedFlaggedFakes              );
  tree->SetBranchAddress("tt_numberOfPixelDeadSensors",        &tt_numberOfPixelDeadSensors              );
  tree->SetBranchAddress("tt_numberOfSCTDeadSensors",          &tt_numberOfSCTDeadSensors              );
  tree->SetBranchAddress("tt_numberOfTRTDeadStraws",           &tt_numberOfTRTDeadStraws              );
  //
  tree->SetBranchAddress("Reco_track_numberOfInnermostPixelHits",            &Reco_track_numberOfInnermostPixelHits);
  tree->SetBranchAddress("Reco_track_numberOfNextToInnermostPixelHits",      &Reco_track_numberOfNextToInnermostPixelHits);
  tree->SetBranchAddress("Reco_track_numberOfPixelL1Hits",                   &Reco_track_numberOfPixelL1Hits);
  tree->SetBranchAddress("Reco_track_numberOfPixelL2Hits",                   &Reco_track_numberOfPixelL2Hits);
  //
  ///////////////////////////////////////////
  // Reconstructed track info 
  tree->SetBranchAddress( "Reco_track_pt",                     &Reco_track_pt);
  tree->SetBranchAddress( "Reco_track_eta",                    &Reco_track_eta);
  tree->SetBranchAddress( "Reco_track_phi",                    &Reco_track_phi);
  tree->SetBranchAddress( "Reco_track_e",                      &Reco_track_e);
  tree->SetBranchAddress( "Reco_track_m",                      &Reco_track_m);
  tree->SetBranchAddress( "Reco_track_d0",                     &Reco_track_d0);
  tree->SetBranchAddress( "Reco_track_z0",                     &Reco_track_z0);
  tree->SetBranchAddress( "Reco_track_numberOfPixelHits",       &Reco_track_numberOfPixelHits);
  //_//_//_//_//_//_//_//_//_//_//_//_//_//_//_//_//_//_//_//_//_//_//_//_ //_//_//_//_//_//_//_//_//_//_//_//_//_//_//_//_//_//_//_//_//_//_//_//
  //
  //                                  The decralation of histograms
  //
  //_//_//_//_//_//_//_//_//_//_//_//_//_//_//_//_//_//_//_//_//_//_//_//_ //_//_//_//_//_//_//_//_//_//_//_//_//_//_//_//_//_//_//_//_//_//_//_//
  /*
   * Event display 
   * */
  Int_t EventDisplayTargetEvent = 9950; // 1-prong && pi+nu && Truth_tau_decayRad > 122.5
  //  Int_t EventDisplayTargetEvent = 4438; // 1-prong && pi+nu && Truth_tau_decayRad > 122.5
  //  Int_t EventDisplayTargetEvent = 339; // Truth_tau_decayRad > 122.4 && numberOfPixelHittedLayer == 4
  TGraph* h_PixelCluster                        [ EventDisplayTargetEvent ];
  TGraph* h_PixelCluster_RZ                     [ EventDisplayTargetEvent ];
  TGraph* h_DecayVertex                         [ EventDisplayTargetEvent ];
  TGraph* h_EventDisplay_DecayVtx_RZ            [ EventDisplayTargetEvent ];
  TLine*  h_Tau                                 [ EventDisplayTargetEvent ];
  for ( int iEvent=0; iEvent< EventDisplayTargetEvent ; iEvent++){
    h_Tau[iEvent]                      = new TLine();
    h_PixelCluster[iEvent]             = new TGraph();
    h_PixelCluster_RZ[iEvent]          = new TGraph();
    h_DecayVertex[iEvent]              = new TGraph();
    h_EventDisplay_DecayVtx_RZ[iEvent] = new TGraph();
  }
  std::vector<std::vector<TGraph*>> h_PixClu_Classified    (EventDisplayTargetEvent);
  std::vector<std::vector<TLatex*>> h_TTextTauInfo         (EventDisplayTargetEvent);
  std::vector<std::vector<TLatex*>> h_RecoTrackInfo        (EventDisplayTargetEvent);
  std::vector<TLatex*>              h_TTextRecoTauInfo     (EventDisplayTargetEvent);
  std::vector<TLatex*>              h_TTextDecayVertexInfo (EventDisplayTargetEvent);
  std::vector<std::vector<TLatex*>> h_TTextTrackInfo       (EventDisplayTargetEvent);
  std::vector<std::vector<TText*>>  h_TTextClusterPdgId    (EventDisplayTargetEvent);
  std::vector<std::vector<TLine*>>  h_TruthChargedTracks   (EventDisplayTargetEvent);
  h_TTextRecoTauInfo    .clear();
  h_TTextDecayVertexInfo.clear();
  for ( int i=0; i< EventDisplayTargetEvent; i++){
    h_PixClu_Classified[i] .clear();
    h_TTextTrackInfo[i]    .clear();
    h_RecoTrackInfo[i]     .clear();
    h_TTextTauInfo[i]      .clear();
    h_TTextClusterPdgId[i] .clear();
    h_TruthChargedTracks[i].clear();
  }
  int TargetEventCounter=0;
  EventCounter eventCounter;

  // truth tau position logger
  Double_t XTauPositionLogger   [ EventDisplayTargetEvent ][2];
  Double_t YTauPositionLogger   [ EventDisplayTargetEvent ][2];
  Double_t ZTauPositionLogger   [ EventDisplayTargetEvent ][2];
  Double_t RTauPositionLogger   [ EventDisplayTargetEvent ][2];
  int      TruthTauNumberOfPixelHitsLogger[EventDisplayTargetEvent];

  // Truth neutrino logger
  Double_t XNeutrinoPositionLogger [EventDisplayTargetEvent][2];
  Double_t YNeutrinoPositionLogger [EventDisplayTargetEvent][2];

  // truth track position logger
  Double_t ZTrackPositionLogger[ EventDisplayTargetEvent ][2];
  Double_t RTrackPositionLogger[ EventDisplayTargetEvent ][2];

  Double_t TrackPtLogger        [EventDisplayTargetEvent];
  Double_t TrackTauThetaLoogger [EventDisplayTargetEvent];

  // InDetTrack position logger
  std::vector<std::vector< double > > XInDetTrackPositionLogger      (EventDisplayTargetEvent);
  std::vector<std::vector< double > > YInDetTrackPositionLogger      (EventDisplayTargetEvent);
  std::vector<std::vector< double > > ZInDetTrackPositionLogger      (EventDisplayTargetEvent);
  std::vector<std::vector< int > >    NumberOfPixelHitsLogger        (EventDisplayTargetEvent);
  std::vector<std::vector< int > >    NumberOf_IBL_PixelHitsLogger   (EventDisplayTargetEvent);
  std::vector<std::vector< int > >    NumberOf_BLayer_PixelHitsLogger(EventDisplayTargetEvent);
  std::vector<std::vector< int > >    NumberOf_L1_PixelHitsLogger    (EventDisplayTargetEvent);
  std::vector<std::vector< int > >    NumberOf_L2_PixelHitsLogger    (EventDisplayTargetEvent);
  //_//_//_//_//_//_//_//_//_//_//_//_//_//_//_//_//_//_//_//_//_//_//_//_ //_//_//_//_//_//_//_//_//_//_//_//_//_//_//_//_//_//_//_//_//_//_//_//
  //
  //                             Main Logic 
  //
  //_//_//_//_//_//_//_//_//_//_//_//_//_//_//_//_//_//_//_//_//_//_//_//_ //_//_//_//_//_//_//_//_//_//_//_//_//_//_//_//_//_//_//_//_//_//_//_//
  std::cout << "The total number of events : " << tree->GetEntries() << std::endl;
  for(int iEntry=0; iEntry< tree->GetEntries(); iEntry++){
    tree->GetEntry(iEntry);

      if ( iEntry > 10000) break;

    /* Count the number of event in each region */
    eventCounter.all++;
    if ( Truth_tau_isHadronic ){
      eventCounter.hadronic++;
      if ( Truth_tau_numTracks == 1 ){
        eventCounter.prong1++;
        if ( TMath::Abs(Truth_tau_eta) < 1.0 ){
          eventCounter.eta_1_0++;
          if       ( Truth_tau_decayRad < 33.25 ) eventCounter.decayRadLower33_25++;
          else if  ( Truth_tau_decayRad < 50.5  ) eventCounter.decayRadLower50_5++;
          else if  ( Truth_tau_decayRad < 88.5  ) eventCounter.decayRadLower88_5++;
          else if  ( Truth_tau_decayRad < 122.5 ) eventCounter.decayRadLower122_5++;
          else if  ( Truth_tau_decayRad > 122.5 ) eventCounter.decayRadLarger122_5++;
        }
        if ( TMath::Abs(Truth_tau_eta) < 1.0 && Truth_dau_pdgId->size() == 2 ){
          bool pi_  = false;
          bool tauneu = false;
          for ( unsigned int iDau=0; iDau<Truth_dau_pdgId->size(); iDau++){
            if ( TMath::Abs(Truth_dau_pdgId->at(iDau)) == 211 ) pi_  = true;
            if ( TMath::Abs(Truth_dau_pdgId->at(iDau)) == 16  ) tauneu = true;
          }
          if ( pi_ ==true && tauneu == true && Truth_tau_decayRad > 122.5) eventCounter.TauHadOneProngPiNuOuter++;
          if ( pi_ ==true && tauneu == true && Truth_tau_decayRad < 122.5) eventCounter.TauHadOneProngPiNuInner++;
        }
      }
      else if (Truth_tau_numTracks == 3 ){
        eventCounter.prong3++;
      }
    }

    if ( iEntry % 10000 == 0 ) std::cout << iEntry << " events were processed... " << std::endl;
    if ( !Truth_tau_isHadronic )           continue;
    if ( TMath::Abs(Truth_tau_eta) > 1.0 ) continue; // Barrel

    if ( Truth_tau_decayRad < 50.5 || Truth_tau_decayRad < 88.5) continue;
    if ( Truth_tau_numTracks != 1 ) continue;
    if ( Truth_neutrino_pt->size() == 0 ) continue;

    TLorentzVector TruthTau;
    TruthTau.SetPtEtaPhiM(
        Truth_tau_pt,
        Truth_tau_eta,
        Truth_tau_phi,
        Truth_tau_m
        );
    // Truth track (from tau) info
    TLorentzVector TruthTrack;
    TruthTrack.SetPtEtaPhiM(
        Truth_track_pt->at(0),
        Truth_track_eta->at(0),
        Truth_track_phi->at(0),
        Truth_track_m->at(0)
        );
    
    // Charged tracks
    for ( unsigned int iTracks=0; iTracks<Truth_track_pt->size(); iTracks++){
      TLorentzVector TruthTrack;
      TruthTrack.SetPtEtaPhiM(
          Truth_track_pt ->at(iTracks),
          Truth_track_eta->at(iTracks),
          Truth_track_phi->at(iTracks),
          Truth_track_m  ->at(iTracks)
          );
      // Truth track position
      TLine* truth_charged_tracks = new TLine(
          Truth_decVtx_x,
          Truth_decVtx_y,
          TruthTrack.X(),
          TruthTrack.Y()
          );
      truth_charged_tracks->SetLineColor(6-iTracks);
      truth_charged_tracks->SetLineWidth(1);
      h_TruthChargedTracks[TargetEventCounter].push_back( truth_charged_tracks );
      
      double deltay = 10*iTracks;
      TLatex* tau_track = new TLatex(-180, -150+deltay,
          Form("Truth #tau track p_{T}=%.2f [GeV], #eta=%.3f, #phi=%.3f", TruthTrack.Pt()/1000., TruthTrack.Eta(), TruthTrack.Phi()));
      tau_track->SetTextAlign(11);
      tau_track->SetTextColor(kBlack);
      tau_track->SetTextFont(43);
      tau_track->SetTextSize(10);
      h_TTextTauInfo[TargetEventCounter].push_back(tau_track);
    }

    // Reco tracks
    for ( unsigned int iTracks=0; iTracks< Reco_track_pt->size(); iTracks++){
      TLorentzVector RecoTrack;
      RecoTrack.SetPtEtaPhiM(
          Reco_track_pt ->at(iTracks),
          Reco_track_eta->at(iTracks),
          Reco_track_phi->at(iTracks),
          Reco_track_m  ->at(iTracks)
          );
      
      double deltay = 10*iTracks;
      TLatex* reco_track = new TLatex(20, -150+deltay,
          Form("Reco track p_{T}=%.2f [GeV], #eta=%.3f, #phi=%.3f", RecoTrack.Pt()/1000., RecoTrack.Eta(), RecoTrack.Phi()));
      reco_track->SetTextAlign(11);
      reco_track->SetTextColor(kBlack);
      reco_track->SetTextFont(43);
      reco_track->SetTextSize(10);
      h_RecoTrackInfo[TargetEventCounter].push_back(reco_track);
    }
    TLatex* reco_tau = new TLatex(20, -165, 
        Form("Reco #tau pt_{T} = %.2f [GeV], #eta =%.3f, #phi=%.3f, \#tracks = %d", Reco_tau_pt/1000., Reco_tau_eta, Reco_tau_phi, Reco_tau_numTracks));
    reco_tau->SetTextAlign(11);
    reco_tau->SetTextColor(kBlack);
    reco_tau->SetTextFont(43);
    reco_tau->SetTextSize(10);
    h_TTextRecoTauInfo.push_back(reco_tau);

    for ( unsigned int iLayer=0; iLayer<InDet_pixel_raw_ClustersEta->size(); iLayer++){
      for ( unsigned int iCluster=0; iCluster<InDet_pixel_raw_ClustersEta->at(iLayer).size(); iCluster++){
        // Modify x,y,z
        TLorentzVector PixelCluster;
        PixelCluster.SetXYZT( 
            InDet_pixel_raw_globalX->at(iLayer).at(iCluster) - Truth_prodVtx_x,
            InDet_pixel_raw_globalY->at(iLayer).at(iCluster) - Truth_prodVtx_y,
            InDet_pixel_raw_globalZ->at(iLayer).at(iCluster) - Truth_prodVtx_z,
            0
            );

        // Plot pixel cluster (X-Y plane)
        h_PixelCluster[TargetEventCounter]->SetPoint(
            h_PixelCluster[TargetEventCounter]->GetN(), 
            InDet_pixel_raw_globalX->at(iLayer).at(iCluster), 
            InDet_pixel_raw_globalY->at(iLayer).at(iCluster));

        // Plot pixel cluster (Z-R plane)
        if ( InDet_pixel_raw_globalY->at(iLayer).at(iCluster) > 0 ) 
          h_PixelCluster_RZ[TargetEventCounter]->SetPoint(
              h_PixelCluster_RZ[TargetEventCounter]->GetN(), 
              InDet_pixel_raw_globalZ->at(iLayer).at(iCluster), 
              TMath::Sqrt(InDet_pixel_raw_globalX->at(iLayer).at(iCluster)*InDet_pixel_raw_globalX->at(iLayer).at(iCluster) +
                InDet_pixel_raw_globalY->at(iLayer).at(iCluster)*InDet_pixel_raw_globalY->at(iLayer).at(iCluster)));
        else                    
          h_PixelCluster_RZ[TargetEventCounter]->SetPoint(
              h_PixelCluster_RZ[TargetEventCounter]->GetN(), 
              InDet_pixel_raw_globalZ->at(iLayer).at(iCluster), 
              -1*TMath::Sqrt(InDet_pixel_raw_globalX->at(iLayer).at(iCluster)*InDet_pixel_raw_globalX->at(iLayer).at(iCluster) + 
                InDet_pixel_raw_globalY->at(iLayer).at(iCluster)*InDet_pixel_raw_globalY->at(iLayer).at(iCluster)));

        // Plot PDG ID
        if ( iLayer == 0 ) {
          for ( unsigned int jndex=0; jndex < InDet_pixel_raw_IB_pdgId->at(iCluster).size(); jndex++){
            if (InDet_pixel_raw_IB_pdgId->at(iCluster).at(jndex) != 0 ) {
              TGraph* cluster = new TGraph();
              cluster->SetPoint(
                  cluster->GetN(),
                  InDet_pixel_raw_globalX->at(iLayer).at(iCluster),
                  InDet_pixel_raw_globalY->at(iLayer).at(iCluster));

              // PDG ID for Pixel cluster
              TText* pdg_id = new TText(
                  InDet_pixel_raw_globalX->at(iLayer).at(iCluster)+1, 
                  InDet_pixel_raw_globalY->at(iLayer).at(iCluster)+1,
                  Form("%.0f",InDet_pixel_raw_IB_pdgId->at(iCluster).at(jndex)));

              if ( TMath::Abs(InDet_pixel_raw_IB_pdgId->at(iCluster).at(jndex)) == 15){
                cluster->SetMarkerSize(0.3);
                cluster->SetMarkerColor(kRed);
                pdg_id->SetTextColor(kRed);
              } else {
                cluster->SetMarkerSize(0.3);
                cluster->SetMarkerColor(kCyan);
                pdg_id->SetTextColor(kCyan);
              }

              pdg_id->SetTextSize(0.015);
              h_PixClu_Classified[TargetEventCounter].push_back( cluster );
              h_TTextClusterPdgId[TargetEventCounter].push_back( pdg_id  );
            }
          }
        }else if ( iLayer == 1 ){
          for ( unsigned int jndex=0; jndex < InDet_pixel_raw_BL_pdgId->at(iCluster).size(); jndex++){
            if (InDet_pixel_raw_BL_pdgId->at(iCluster).at(jndex) != 0 ) {
              TGraph* cluster = new TGraph();
              cluster->SetPoint(
                  cluster->GetN(),
                  InDet_pixel_raw_globalX->at(iLayer).at(iCluster),
                  InDet_pixel_raw_globalY->at(iLayer).at(iCluster));

              // PDG ID for Pixel cluster
              TText* pdg_id = new TText(
                  InDet_pixel_raw_globalX->at(iLayer).at(iCluster)+1, 
                  InDet_pixel_raw_globalY->at(iLayer).at(iCluster)+1,
                  Form("%.0f",InDet_pixel_raw_BL_pdgId->at(iCluster).at(jndex)));

              if ( TMath::Abs(InDet_pixel_raw_BL_pdgId->at(iCluster).at(jndex))== 15){
                cluster->SetMarkerSize(0.3);
                cluster->SetMarkerColor(kRed);
                pdg_id->SetTextColor(kRed);
              } else {
                cluster->SetMarkerSize(0.3);
                cluster->SetMarkerColor(kCyan);
                pdg_id->SetTextColor(kCyan);
              }
              pdg_id->SetTextSize(0.015);

              h_TTextClusterPdgId[TargetEventCounter].push_back( pdg_id );
              h_PixClu_Classified[TargetEventCounter].push_back( cluster );
            }
          }
        } else if ( iLayer == 2 ){
          for ( unsigned int jndex=0; jndex < InDet_pixel_raw_L1_pdgId->at(iCluster).size(); jndex++){
            if (InDet_pixel_raw_L1_pdgId->at(iCluster).at(jndex) != 0 ) {
              TGraph* cluster = new TGraph();
              cluster->SetPoint(
                  cluster->GetN(),
                  InDet_pixel_raw_globalX->at(iLayer).at(iCluster),
                  InDet_pixel_raw_globalY->at(iLayer).at(iCluster));

              // PDG ID for Pixel cluster
              TText* pdg_id = new TText(
                  InDet_pixel_raw_globalX->at(iLayer).at(iCluster)+1, 
                  InDet_pixel_raw_globalY->at(iLayer).at(iCluster)+1,
                  Form("%.0f",InDet_pixel_raw_L1_pdgId->at(iCluster).at(jndex)));

              if ( TMath::Abs(InDet_pixel_raw_L1_pdgId->at(iCluster).at(jndex))== 15){
                cluster->SetMarkerSize(0.3);
                cluster->SetMarkerColor(kRed);
                pdg_id->SetTextColor(kRed);
              } else {
                cluster->SetMarkerSize(0.3);
                cluster->SetMarkerColor(kCyan);
                pdg_id->SetTextColor(kCyan);
              }
              pdg_id->SetTextSize(0.015);

              h_TTextClusterPdgId[TargetEventCounter].push_back( pdg_id );
              h_PixClu_Classified[TargetEventCounter].push_back( cluster );
            }
          }
        } else if ( iLayer == 3 ){
          for ( unsigned int jndex=0; jndex < InDet_pixel_raw_L2_pdgId->at(iCluster).size(); jndex++){
            if (InDet_pixel_raw_L2_pdgId->at(iCluster).at(jndex) != 0 ) {
              TGraph* cluster = new TGraph();
              cluster->SetPoint(
                  cluster->GetN(),
                  InDet_pixel_raw_globalX->at(iLayer).at(iCluster),
                  InDet_pixel_raw_globalY->at(iLayer).at(iCluster));

              // PDG ID for Pixel cluster
              TText* pdg_id = new TText(
                  InDet_pixel_raw_globalX->at(iLayer).at(iCluster)+1, 
                  InDet_pixel_raw_globalY->at(iLayer).at(iCluster)+1,
                  Form("%.0f",InDet_pixel_raw_L2_pdgId->at(iCluster).at(jndex)));

              if ( TMath::Abs(InDet_pixel_raw_L2_pdgId->at(iCluster).at(jndex))== 15){
                cluster->SetMarkerSize(0.3);
                cluster->SetMarkerColor(kRed);
                pdg_id->SetTextColor(kRed);
              } else {
                cluster->SetMarkerSize(0.3);
                cluster->SetMarkerColor(kCyan);
                pdg_id->SetTextColor(kCyan);
              }
              pdg_id->SetTextSize(0.015);

              h_TTextClusterPdgId[TargetEventCounter].push_back( pdg_id );
              h_PixClu_Classified[TargetEventCounter].push_back( cluster );
            }
          }
        }
      }// iCluster
    }// iLayer

    TLine* tau = new TLine(
        Truth_prodVtx_x,
        Truth_prodVtx_y,
        Truth_decVtx_x,
        Truth_decVtx_y
        );
      tau ->SetLineStyle(3);
      tau ->SetLineWidth(1);
      tau ->Draw("same");
    h_Tau[TargetEventCounter] = tau;
      // Truth tau position
      XTauPositionLogger[TargetEventCounter][0] = Truth_prodVtx_x;
      YTauPositionLogger[TargetEventCounter][0] = Truth_prodVtx_y;
      ZTauPositionLogger[TargetEventCounter][0] = Truth_prodVtx_z;
      RTauPositionLogger[TargetEventCounter][0] = TMath::Sqrt(Truth_prodVtx_x*Truth_prodVtx_x+Truth_prodVtx_y*Truth_prodVtx_y);
      //            XTauPositionLogger[TargetEventCounter][1] = TruthTau.X();
      //            YTauPositionLogger[TargetEventCounter][1] = TruthTau.Y();
      XTauPositionLogger[TargetEventCounter][1] = Truth_decVtx_x;
      YTauPositionLogger[TargetEventCounter][1] = Truth_decVtx_y;
      ZTauPositionLogger[TargetEventCounter][1] = TruthTau.Z();
      RTauPositionLogger[TargetEventCounter][1] = TMath::Sqrt(TruthTau.X()*TruthTau.X()+TruthTau.Y()*TruthTau.Y());

      // Neutrino
      TLorentzVector NeuVec;
      NeuVec.SetPtEtaPhiM( Truth_neutrino_pt->at(0), Truth_neutrino_eta->at(0), Truth_neutrino_phi->at(0), 0);
      XNeutrinoPositionLogger[TargetEventCounter][0] = Truth_decVtx_x;
      XNeutrinoPositionLogger[TargetEventCounter][1] = NeuVec.X();
      YNeutrinoPositionLogger[TargetEventCounter][0] = Truth_decVtx_y;
      YNeutrinoPositionLogger[TargetEventCounter][1] = NeuVec.Y();

      ZTrackPositionLogger[TargetEventCounter][0] = Truth_decVtx_z;
      RTrackPositionLogger[TargetEventCounter][0] = TMath::Sqrt(Truth_decVtx_x*Truth_decVtx_x+Truth_decVtx_y*Truth_decVtx_y);
      ZTrackPositionLogger[TargetEventCounter][1] = TruthTrack.Z();
      RTrackPositionLogger[TargetEventCounter][1] = TMath::Sqrt(TruthTrack.X()*TruthTrack.X()+TruthTrack.Y()*TruthTrack.Y());

      TrackPtLogger[TargetEventCounter]        = TruthTrack.Pt()/1000.;
      TrackTauThetaLoogger[TargetEventCounter] = TruthTrack.Angle(TruthTau.Vect());


      // Decay vertex 
    h_DecayVertex[TargetEventCounter]   ->SetPoint(
        h_DecayVertex[TargetEventCounter]->GetN(),
        Truth_decVtx_x,
        Truth_decVtx_y);
    TLatex* decayVtx = new TLatex( Truth_decVtx_x+5, Truth_decVtx_y, 
        Form("R_{decay} = %.2f", Truth_tau_decayRad));
    decayVtx->SetTextAlign(22);
    decayVtx->SetTextSize(0.015);
    decayVtx->SetTextColor(kBlack);
    decayVtx->SetTextFont(43);
    h_TTextDecayVertexInfo.push_back( decayVtx );

    if ( Truth_decVtx_y > 0 ) 
      h_EventDisplay_DecayVtx_RZ[TargetEventCounter]->SetPoint(
          h_EventDisplay_DecayVtx_RZ[TargetEventCounter]->GetN(), 
          Truth_decVtx_z, 
          TMath::Sqrt(Truth_decVtx_x*Truth_decVtx_x + Truth_decVtx_y*Truth_decVtx_y));
    else                      
      h_EventDisplay_DecayVtx_RZ[TargetEventCounter]->SetPoint(
          h_EventDisplay_DecayVtx_RZ[TargetEventCounter]->GetN(), 
          Truth_decVtx_z, 
          -1*TMath::Sqrt(Truth_decVtx_x*Truth_decVtx_x + Truth_decVtx_y*Truth_decVtx_y));

    // InDetTrackParticle
    TLatex* track_num = new TLatex(-180, 180, Form("# InDetTrack = %d", InDet_track_pt->size() ) );
    track_num->SetTextAlign(11);
    track_num->SetTextColor(kBlack);
    track_num->SetTextFont(43);
    track_num->SetTextSize(10);
    h_TTextTrackInfo[TargetEventCounter].push_back( track_num );

    // InDet track 
    for ( int iInDet=0; iInDet<InDet_track_pt->size(); iInDet++){
      TLorentzVector InDetVec;
      InDetVec.SetPtEtaPhiM( 
          InDet_track_pt ->at(iInDet)/1000., 
          InDet_track_eta->at(iInDet),
          InDet_track_phi->at(iInDet),
          InDet_track_m  ->at(iInDet)
          );
      XInDetTrackPositionLogger       .at(TargetEventCounter).push_back( InDetVec.X() );
      YInDetTrackPositionLogger       .at(TargetEventCounter).push_back( InDetVec.Y() );
      ZInDetTrackPositionLogger       .at(TargetEventCounter).push_back( InDetVec.Z() );
      NumberOfPixelHitsLogger         .at(TargetEventCounter).push_back( InDet_track_numberOfPixelHits->at(iInDet) );
      NumberOf_IBL_PixelHitsLogger    .at(TargetEventCounter).push_back( InDet_track_numberOfIBLHits->at(iInDet) );
      NumberOf_BLayer_PixelHitsLogger .at(TargetEventCounter).push_back( InDet_track_numberOfBLayerHits->at(iInDet) );
      NumberOf_L1_PixelHitsLogger     .at(TargetEventCounter).push_back( InDet_track_numberOfPixelL1Hits->at(iInDet) );
      NumberOf_L2_PixelHitsLogger     .at(TargetEventCounter).push_back( InDet_track_numberOfPixelL2Hits->at(iInDet) );

      // For InDetTrack legend
      TLatex* track_info  = new TLatex(
          -180, 180 - h_TTextTrackInfo[TargetEventCounter].size()*10, 
          Form("pT = %.1f [GeV], #eta = %.3f, #phi = %.3f", InDet_track_pt->at(iInDet)/1000., InDet_track_eta->at(iInDet), InDet_track_phi->at(iInDet)) );
      track_info->SetTextAlign(11);
      track_info->SetTextColor(kBlack);
      track_info->SetTextFont(43);
      track_info->SetTextSize(10);
      h_TTextTrackInfo[TargetEventCounter].push_back( track_info );
    }
    TargetEventCounter++;
  }

  // TFile *fout = new TFile("EventDisplay.root", "recreate");
  /* 
   * Event display 
   *  X - Y plane */
  // IBL
  TEllipse *ellipse[4];
  TText *zPos[4];
  double PixelDiameter[4] = {33.25, 50.5, 88.5, 122.5};
  EventDisplayTargetEvent = 300;
  for ( int iEvent=0; iEvent< EventDisplayTargetEvent ; iEvent++){
    TCanvas* canvas = new TCanvas( Form("canvas%d",iEvent), Form("canvas%d", iEvent));
    // Pixel cluster
    h_PixelCluster[iEvent]->GetXaxis()->SetLimits(-200,200);
    h_PixelCluster[iEvent]->GetHistogram()->SetMinimum(-200);
    h_PixelCluster[iEvent]->GetHistogram()->SetMaximum(200);
    h_PixelCluster[iEvent]->SetMarkerSize(0.5);

    // Decay vertex
    h_DecayVertex    [iEvent]->SetMarkerStyle(34);
    h_DecayVertex    [iEvent]->SetMarkerSize(0.5);
    h_DecayVertex    [iEvent]->SetMarkerColor(kRed);

    h_PixelCluster[iEvent]->Draw("AP");
    h_DecayVertex [iEvent]->Draw("P");
    // Draw known clusters
    for (int iCluster=0; iCluster< h_PixClu_Classified[iEvent].size(); iCluster++){
      h_PixClu_Classified[iEvent].at(iCluster)->Draw("P");
    }

    for ( int iLayer=0; iLayer<4; iLayer++){
      ellipse[iLayer] = new TEllipse(0,0, PixelDiameter[iLayer]);
      ellipse[iLayer] ->SetFillStyle(0);
      ellipse[iLayer] ->SetLineColor(iLayer+1);
      ellipse[iLayer] ->SetLineWidth(1);
      ellipse[iLayer] ->Draw("same");

      // turht tau
      //TLine *Tauline = new TLine( 
      //    XTauPositionLogger[iEvent][0],
      //    YTauPositionLogger[iEvent][0],
      //    XTauPositionLogger[iEvent][1],
      //    YTauPositionLogger[iEvent][1]
      //    );
      //Tauline->SetLineStyle(3);
      //Tauline->SetLineWidth(1);
      //Tauline->Draw("same");
      h_Tau[iEvent]->Draw("same");

      // Tau hit Z position
      //zPos[iLayer] = new TText( 
      //zPos[iLayer]->SetTextSize(0.015);
      //zPos[iLayer]->Draw("same");

      // Neutrino
      TLine *Neuline = new TLine( 
          XNeutrinoPositionLogger[iEvent][0],
          YNeutrinoPositionLogger[iEvent][0],
          XNeutrinoPositionLogger[iEvent][1],
          YNeutrinoPositionLogger[iEvent][1]
          );
      Neuline->SetLineStyle(2);
      Neuline->SetLineWidth(1);
      Neuline->SetLineColor(kCyan);
      Neuline->Draw("same");

      // Charged track 
      for ( int iTracks=0; iTracks<h_TruthChargedTracks[iEvent].size(); iTracks++ ){
        h_TruthChargedTracks[iEvent].at(iTracks)->Draw("same");
      }

      // Draw all the tracks
      //for ( unsigned int iInDet=0; iInDet<XInDetTrackPositionLogger[iEvent].size(); iInDet++){
      //  TLine *InDetTrackline = new TLine( 
      //      -0.5,
      //      -0.5,
      //      XInDetTrackPositionLogger[iEvent][iInDet],
      //      YInDetTrackPositionLogger[iEvent][iInDet]
      //      );
      //  InDetTrackline->SetLineColor(iInDet+1);
      //  InDetTrackline->Draw("same");
      //  TText* numHits = new TText(50,50+iInDet*10, Form("# hits = %d (%d,%d,%d,%d)", 
      //        NumberOfPixelHitsLogger[iEvent].at(iInDet),
      //        NumberOf_IBL_PixelHitsLogger[iEvent].at(iInDet),
      //        NumberOf_BLayer_PixelHitsLogger[iEvent].at(iInDet),
      //        NumberOf_L1_PixelHitsLogger[iEvent].at(iInDet),
      //        NumberOf_L2_PixelHitsLogger[iEvent].at(iInDet))
      //      );
      //  numHits->SetTextSize(0.015);
      //  numHits->Draw("same");
      //}
    }

    TLatex* infoTheta = new TLatex(-180, -180,  Form("#Delta #theta = %f", TrackTauThetaLoogger[iEvent]));
    infoTheta->SetTextAlign(11);
    infoTheta->SetTextColor(kBlack);
    infoTheta->SetTextFont(43);
    infoTheta->SetTextSize(10);
    infoTheta->Draw();

    for ( int i=0; i< h_TTextTrackInfo[iEvent].size(); i++){
      h_TTextTrackInfo    [iEvent].at(i)->Draw("same");
    }
    for ( int i=0; i< h_TTextClusterPdgId[iEvent].size(); i++){
      h_TTextClusterPdgId [iEvent].at(i)->Draw("same");
    }
    for ( int i=0; i< h_TTextTauInfo[iEvent].size(); i++){
      h_TTextTauInfo      [iEvent].at(i)->Draw("same");
    }
    for ( int i=0; i< h_RecoTrackInfo[iEvent].size(); i++){
      h_RecoTrackInfo     [iEvent].at(i)->Draw("same");
    }
    h_TTextRecoTauInfo    [iEvent]->Draw();
    //if ( h_TTextTauInfo[iEvent].size() > iEvent ) {
    //  h_TTextDecayVertexInfo[iEvent]->Draw();
    //}

    if      ( iEvent == 0  )                        canvas->SaveAs("EventDisplay.pdf(");
    else if ( iEvent == EventDisplayTargetEvent-1 ) canvas->SaveAs("EventDisplay.pdf)");
    else                                            canvas->SaveAs("EventDisplay.pdf");
  }

  /* 
   * Event display 
   *  Z - R plane */
  // IBL
  TLine *PixelLine[4][2];
  for ( int iEvent=0; iEvent< EventDisplayTargetEvent ; iEvent++){
    TCanvas* canvas_ZR = new TCanvas( Form("canvas_ZR%d",iEvent), Form("canvas_ZR%d", iEvent));
    // Pixel cluster
    h_PixelCluster_RZ[iEvent]->GetXaxis()->SetLimits(-400,400);
    h_PixelCluster_RZ[iEvent]->GetHistogram()->SetMinimum(-300);
    h_PixelCluster_RZ[iEvent]->GetHistogram()->SetMaximum(300);
    h_PixelCluster_RZ[iEvent]->SetMarkerSize(0.5);

    // Decay vertex
    h_EventDisplay_DecayVtx_RZ    [iEvent]->SetMarkerStyle(34);
    h_EventDisplay_DecayVtx_RZ    [iEvent]->SetMarkerSize(0.5);
    h_EventDisplay_DecayVtx_RZ    [iEvent]->SetMarkerColor(kRed);

    h_PixelCluster_RZ[iEvent]->Draw("AP");
    h_EventDisplay_DecayVtx_RZ    [iEvent]->Draw("P");
    for ( int iLayer=0; iLayer<4; iLayer++){
      PixelLine[iLayer][0] = new TLine(-400, PixelDiameter[iLayer], 400, PixelDiameter[iLayer]);
      PixelLine[iLayer][0]->SetLineColor(iLayer+1);
      PixelLine[iLayer][0]->Draw();

      PixelLine[iLayer][1] = new TLine(-400, -1*PixelDiameter[iLayer], 400, -1*PixelDiameter[iLayer]);
      PixelLine[iLayer][1]->SetLineColor(iLayer+1);
      PixelLine[iLayer][1]->Draw();

      TLine *Tauline;
      if (YTauPositionLogger[iEvent][1]>0 ){
        Tauline = new TLine( 
            ZTauPositionLogger[iEvent][0],
            RTauPositionLogger[iEvent][0],
            ZTauPositionLogger[iEvent][1],
            RTauPositionLogger[iEvent][1]
            );
      } else {
        Tauline = new TLine( 
            ZTauPositionLogger[iEvent][0],
            -1*RTauPositionLogger[iEvent][0],
            ZTauPositionLogger[iEvent][1],
            -1*RTauPositionLogger[iEvent][1]
            );
      }
      Tauline->SetLineStyle(2);
      Tauline->SetLineWidth(1);
      Tauline->Draw("same");

      TLine *Trackline;
      if (YTauPositionLogger[iEvent][1]>0 ){
        Trackline = new TLine( 
            ZTrackPositionLogger[iEvent][0],
            RTrackPositionLogger[iEvent][0],
            ZTrackPositionLogger[iEvent][1],
            RTrackPositionLogger[iEvent][1]
            );
      }else {
        Trackline = new TLine( 
            ZTrackPositionLogger[iEvent][0],
            -1*RTrackPositionLogger[iEvent][0],
            ZTrackPositionLogger[iEvent][1],
            -1*RTrackPositionLogger[iEvent][1]
            );
      }
      Trackline->SetLineColor(kMagenta);
      Trackline->Draw("same");

      //TText* infoPt    = new TText( 200, 250, Form("Truth track p_{T} = %.2f [GeV]", TrackPtLogger[iEvent]));
      //TText* infoTheta = new TText( 200, 200, Form("#Delta #theta = %f", TrackTauThetaLoogger[iEvent]));
      //infoPt->SetTextSize(0.02);
      //infoTheta->SetTextSize(0.02);
      //infoPt->Draw();
      //infoTheta->Draw();
    }
    //canvas->Update();
    if ( iEvent == 0  )                             canvas_ZR->SaveAs("EventDisplay_ZR.pdf(");
    else if ( iEvent == EventDisplayTargetEvent-1 ) canvas_ZR->SaveAs("EventDisplay_ZR.pdf)");
    else                                            canvas_ZR->SaveAs("EventDisplay_ZR.pdf");
    //  canvas->Write();
  }

  /********************************************************/
  /*         The number of events in each region          */
  /********************************************************/
  std::cout << "All                                       " << eventCounter.all << std::endl;
  std::cout << "Hadronic                                  " << eventCounter.hadronic << std::endl;
  std::cout << "1-prong                                   " << eventCounter.prong1 << std::endl;
  std::cout << "|eta|<1.0                                 " << eventCounter.eta_1_0 << std::endl;
  std::cout << " 0     < R_{decay} < 33.25                " << eventCounter.decayRadLower33_25 << std::endl;
  std::cout << " 33.25 < R_{decay} < 50.5                 " << eventCounter.decayRadLower50_5 << std::endl;
  std::cout << " 50.5  < R_{decay} < 88.5                 " << eventCounter.decayRadLower88_5 << std::endl;
  std::cout << " 88.5  < R_{decay} < 122.5                " << eventCounter.decayRadLower122_5 << std::endl;
  std::cout << "R_{decay} > 122.5                         " << eventCounter.decayRadLarger122_5<< std::endl;
  std::cout << "R_{decay} < 122.5 && pi+nu) && |eta| <1.0 " << eventCounter.TauHadOneProngPiNuInner << std::endl;
  std::cout << "R_{decay} > 122.5 && pi+nu) && |eta| <1.0 " << eventCounter.TauHadOneProngPiNuOuter << std::endl;
  std::cout << " "                           << std::endl;
  std::cout << "3-prong                                   " << eventCounter.prong3 << std::endl;

  std::cout << TargetEventCounter << std::endl;
  return;
}
