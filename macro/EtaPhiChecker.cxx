
#include <iostream>
#include <TH1.h>
#include <TFile.h>

#define _nPt
#define _PtLeptonic
#define _RecoPt
#define _Fraction
#define _DecayRadius
#define _DecayRadiusFraction
#define _DecayRadiusFraction_TruthVsReco

void EtaPhiChecker()
{
  std::string path = "/afs/cern.ch/user/k/ktakeda/workspace/public/atlas-phys/AJ/WG1/SingleParticle/AnalysisArea/run/MyTauNtupleMC12G4.outputs.root";
  //std::string path = "/afs/cern.ch/user/k/ktakeda/workspace/public/atlas-phys/AJ/WG1/SingleParticle/AnalysisArea/run/MyTauNtupleFullG4.outputs.root";
  TFile* file = new TFile(path.c_str(), "read");
  TTree* tree = (TTree*)file->Get("m_recoTauTree");
  
  Double_t reco_pt; 
  Double_t reco_eta; 
  Double_t reco_phi; 
  Double_t reco_e; 
  Bool_t truth_leptonic_match = false; 
  Bool_t truth_leptonic_electron = false; 
  Bool_t truth_leptonic_muon = false; 
  Bool_t truth_leptonic_tau = false; 
  Bool_t truth_hadronic_match = false; 
  Double_t truth_pt; 
  Double_t truth_eta; 
  Double_t truth_phi; 
  Double_t truth_e; 
  Int_t truth_nprong =0; 
  Double_t truth_decay_radius = 0; 
  Int_t reco_number_of_tracks = 0; 
  Int_t reco_number_of_selected_tracks =0; 

  tree->SetBranchAddress("reco_pt", &reco_pt);
  tree->SetBranchAddress("reco_eta", &reco_eta);
  tree->SetBranchAddress("reco_phi", &reco_phi);
  tree->SetBranchAddress("reco_e", &reco_e);
  tree->SetBranchAddress("truth_leptonic_match", &truth_leptonic_match);
  tree->SetBranchAddress("truth_leptonic_electron", &truth_leptonic_electron);
  tree->SetBranchAddress("truth_leptonic_muon", &truth_leptonic_muon);
  tree->SetBranchAddress("truth_leptonic_tau", &truth_leptonic_tau);
  tree->SetBranchAddress("truth_hadronic_match", &truth_hadronic_match);
  tree->SetBranchAddress("truth_pt", &truth_pt);
  tree->SetBranchAddress("truth_eta", &truth_eta);
  tree->SetBranchAddress("truth_phi", &truth_phi);
  tree->SetBranchAddress("truth_e", &truth_e);
  tree->SetBranchAddress("truth_decay_radius", &truth_decay_radius);
  tree->SetBranchAddress("truth_nprong", &truth_nprong);
  tree->SetBranchAddress("reco_number_of_tracks", &reco_number_of_tracks);
  tree->SetBranchAddress("reco_number_of_selected_tracks", &reco_number_of_selected_tracks);


  TH1D* h_truth_1p = new TH1D("h_truth_1p", ";;", 40, 0, 250);
  TH1D* h_reco_1p_1p = new TH1D("h_reco_1p_1p", ";;", 40, 0, 250);
  TH1D* h_reco_2p = new TH1D("h_reco_2p", ";;", 40, 0, 250);
  TH1D* h_reco_3p = new TH1D("h_reco_3p", ";;", 40, 0, 250);
  
  TH1D* h_truth_3p = new TH1D("h_truth_3p", ";;", 40, 0, 250);
  TH1D* h_reco_3p_3p = new TH1D("h_reco_3p_3p", ";;", 40, 0, 250);

  /// truth momentum
  TH1D* h_truth_pt_all      = new TH1D("h_truth_pt_all",";pT [GeV];Events",100, 0, 700);
  TH1D* h_truth_pt_hadronic = new TH1D("h_truth_pt_hadronic",";pT [GeV];Events",100, 0, 700);
  TH1D* h_truth_pt_leptonic = new TH1D("h_truth_pt_leptonic",";pT [GeV];Events",100, 0, 700);
  TH1D* h_truth_pt_electron = new TH1D("h_truth_pt_electron",";pT [GeV];Events",100, 0, 700);
  TH1D* h_truth_pt_muon     = new TH1D("h_truth_pt_muon",";pT [GeV];Events",100, 0, 700);
  
  /// Reconstrusted momentum
  TH1D* h_reco_pt_all = new TH1D("h_reco_pt_all",";pT [GeV];Events",100, 0, 700);
  TH1D* h_reco_pt_allHadronic = new TH1D("h_reco_pt_allHadronic",";pT [GeV];Events",100, 0, 700);
  TH1D* h_reco_pt_leptonic = new TH1D("h_reco_pt_leptonic",";pT [GeV];Events",100, 0, 700);
  TH1D* h_reco_pt_electron = new TH1D("h_reco_pt_electron",";pT [GeV];Events",100, 0, 700);
  TH1D* h_reco_pt_muon = new TH1D("h_reco_pt_muon",";pT [GeV];Events",100, 0, 700);
  TH1D* h_reco_pt_tau = new TH1D("h_reco_pt_tau",";pT [GeV];Events",100, 0, 700);
  
  TH1D* h_reco_pt_1p = new TH1D("h_reco_pt_1p", ";;", 100, 0, 700);
  TH1D* h_reco_pt_2p = new TH1D("h_reco_pt_2p", ";;", 100, 0, 700);
  TH1D* h_reco_pt_3p = new TH1D("h_reco_pt_3p", ";;", 100, 0, 700);
  
  TH1D* h_pt_reco1p_truth1p = new TH1D("h_pt_reco1p_truth1p", ";;", 100, 0, 700);
  TH1D* h_pt_reco2p_truth2p = new TH1D("h_pt_reco2p_truth2p", ";;", 100, 0, 700);
  TH1D* h_pt_reco3p_truth3p = new TH1D("h_pt_reco3p_truth3p", ";;", 100, 0, 700);


  /// Eta - phi
  const int nEta = 76;
  double bineta[nEta+1] = {-2.600, -2.455, -2.425, -2.358, -2.290, // Forward
    -2.220, -2.156, -2.095, -2.033, -1.970, // Forward
    -1.916, -1.864, -1.809,                 // Endcap
    -1.753, -1.700, -1.650, -1.604, -1.563, // Endcap
    -1.524, -1.486,                         // no FI
    -1.445, -1.398,                         // Endcap
    -1.345, -1.285, -1.225, -1.171, -1.123, // Endcap
    -1.055, -1.020,                         // Endcap
    -0.950, -0.900, -0.840, -0.770,         // RPC
    -0.660, -0.570, -0.430, -0.320, -0.150, // RPC
    0.000,                                 // RPC
    0.150,  0.320,  0.430,  0.570,  0.660, // RPC
    0.770,  0.840,  0.900,  0.950,         // RPC
    1.020,  1.055,  1.123,  1.171,  1.225, // Endcap
    1.285,  1.345,  1.398,  1.445,         // Endcap
    1.486,  1.524,                         // no FI
    1.563,  1.604,  1.650,  1.700,  1.753, // Endcap
    1.809,  1.864,  1.916,                 // Endcap
    1.970,  2.033,  2.095,  2.156,  2.220, // Forward
    2.290,  2.358,  2.425,  2.455,  2.600};

//  TH1D* h_truth_eta = new TH1D("h_truth_eta",";;", nEta, bineta);
//  TH1D* h_reco_eta = new TH1D("h_reco_eta",";;", nEta, bineta);
  TH1D* h_truth_eta = new TH1D("h_truth_eta",";;", 60, -3, 3);
  TH1D* h_truth_eta_1p = new TH1D("h_truth_eta_1p",";;", 60, -3, 3);
  TH1D* h_truth_eta_3p = new TH1D("h_truth_eta_3p",";;", 60, -3, 3);
  
  TH1D* h_reco_eta = new TH1D("h_reco_eta",";;", 60, -3, 3);
  TH1D* h_reco_eta_1p = new TH1D("h_reco_eta_1p",";;", 60, -3, 3);
  TH1D* h_reco_eta_2p = new TH1D("h_reco_eta_2p",";;", 60, -3, 3);
  TH1D* h_reco_eta_3p = new TH1D("h_reco_eta_3p",";;", 60, -3, 3);

  /// Fraction
  TH1D* h_Hadall = new TH1D("h_Hadall",";;",40, 0, 700);
  TH1D* h_reco1prong = new TH1D("h_reco1prong",";pT[GeV];Fraction",40, 0, 700);
  TH1D* h_reco2prong = new TH1D("h_reco2prong",";pT[GeV];Fraction",40, 0, 700);
  TH1D* h_reco3prong = new TH1D("h_reco3prong",";pT[GeV];Fraction",40, 0, 700);
  
  /// 1-prong ot distribution
  TH1D* h_truth_pt_1p = new TH1D("h_truth_pt_1p", ";;", 100, 0, 1000);
  TH1D* h_truth_pt_3p = new TH1D("h_truth_pt_3p", ";;", 100, 0, 1000);
  
  /// Reconstrustion momentum vs truth matched momentum

  /// Decay radius
  TH1D* h_decay_rad_true1p = new TH1D("h_decay_rad_true1p",";decay position [mm];Events",50,0,200);
  TH1D* h_reco_decay_allHadronic = new TH1D("h_reco_decay_allHadronic","",50,0,200);
  TH1D* h_reco_decay_1p_truth_1p = new TH1D("h_reco_decay_1p_truth_1p","",50,0,200);
  TH1D* h_reco_decay_2p_truth_2p = new TH1D("h_reco_decay_2p_truth_2p","",50,0,200);
  TH1D* h_reco_decay_3p_truth_3p = new TH1D("h_reco_decay_3p_truth_3p","",50,0,200);
  TH1D* h_reco_decay_1p = new TH1D("h_reco_decay","",50,0,200);
  TH1D* h_reco_decay_2p = new TH1D("h_reco_decay","",50,0,200);
  TH1D* h_reco_decay_3p = new TH1D("h_reco_decay","",50,0,200);

  /// Number of tracks
  TH1D* h_number_of_truth_track = new TH1D("h_number_of_truth_track", ";truth tau [n-prong];Events", 5, -0.5, 4.5);
  TH1D* h_number_of_selected_reco_track = new TH1D("h_number_of_selected_reco_track", ";reco tau [n-prong];Events", 5, -0.5, 4.5);
  TH1D* h_number_of_selected_reco_track_truth_mached = new TH1D("h_number_of_selected_reco_track_truth_mached", ";reco truth matched tau [n-prong];Events", 5, -0.5, 4.5);

  for(int iEntry=0; iEntry< tree->GetEntries(); iEntry++){
    tree->GetEntry(iEntry);

    ///------------------------ Fraction (pT dependence) -----------------------------------
    /// Reconstrustion eta 
    h_reco_eta->Fill(reco_eta);
    if ( reco_number_of_selected_tracks == 1 ) h_reco_eta_1p->Fill(reco_eta);
    if ( reco_number_of_selected_tracks == 2 ) h_reco_eta_2p->Fill(reco_eta);
    if ( reco_number_of_selected_tracks == 3 ) h_reco_eta_3p->Fill(reco_eta);
    
    if ( truth_leptonic_match || truth_hadronic_match ){
      h_truth_eta->Fill(truth_eta);
      if ( truth_nprong == 1 ) h_truth_eta_1p->Fill(truth_eta);
      if ( truth_nprong == 3 ) h_truth_eta_3p->Fill(truth_eta);
    }

    /// Reconstrustion momentum vs truth matched momentum
    if ( truth_leptonic_match || truth_hadronic_match){
      h_reco_pt_all->Fill(reco_pt/1000.);
      h_truth_pt_all->Fill(truth_pt/1000.);
      if( truth_leptonic_match ){
        h_truth_pt_leptonic->Fill(truth_pt/1000.);
        if( truth_leptonic_electron ) h_truth_pt_electron->Fill(truth_pt/1000.);
        if( truth_leptonic_muon )     h_truth_pt_muon->Fill(truth_pt/1000.);
      }
      if( truth_hadronic_match ){
        h_truth_pt_hadronic->Fill(truth_pt/1000.);
      }
    }

    /// To divide leptonic decay mode into e, mu, tau mode.
    if ( truth_leptonic_match ){
      h_reco_pt_leptonic->Fill(reco_pt/1000.);
      if( truth_leptonic_electron)
        h_reco_pt_electron->Fill(reco_pt/1000.);
      if( truth_leptonic_muon)
        h_reco_pt_muon->Fill(reco_pt/1000.);
      if( truth_leptonic_tau)
        h_reco_pt_tau->Fill(reco_pt/1000.);
    }

    if( truth_hadronic_match){
      h_reco_pt_allHadronic->Fill(reco_pt/1000.);
      h_reco_decay_allHadronic->Fill(truth_decay_radius); // decay radius
      h_Hadall->Fill(reco_pt/1000.);
    }
    if( truth_hadronic_match && reco_number_of_selected_tracks == 1 ) { 
      h_reco_pt_1p->Fill(truth_pt/1000.);
      h_reco1prong->Fill(truth_pt/1000.);
      h_reco_decay_1p->Fill(truth_decay_radius); // decay radius
      if( truth_nprong == 1 ){
        h_pt_reco1p_truth1p->Fill(reco_pt/1000.);
        h_reco_decay_1p_truth_1p->Fill(truth_decay_radius); // decay radius
      }
    }
    if( truth_hadronic_match && reco_number_of_selected_tracks == 2 ) { 
      h_reco_pt_2p->Fill(truth_pt/1000.);
      h_reco2prong->Fill(truth_pt/1000.);
      h_reco_decay_2p->Fill(truth_decay_radius); // decay radius
      if( truth_nprong == 2 ){
        h_pt_reco2p_truth2p->Fill(reco_pt/1000.);
        h_reco_decay_2p_truth_2p->Fill(truth_decay_radius); // decay radius
      }
    }
    if( truth_hadronic_match && reco_number_of_selected_tracks == 3 ) {
      h_reco_pt_3p->Fill(truth_pt/1000.);
      h_reco3prong->Fill(truth_pt/1000.);
        h_reco_decay_3p->Fill(truth_decay_radius); // decay radius
      if( truth_nprong == 3 ){
        h_pt_reco3p_truth3p->Fill(reco_pt/1000.);
        h_reco_decay_3p_truth_3p->Fill(truth_decay_radius); // decay radius
      }
    }
      
    ///------------------------ Fraction (decay radius dependence) -----------------------------------
    /// Decay radius 
    if( truth_hadronic_match == true && truth_nprong == 1 ) {
      h_truth_1p->Fill(truth_decay_radius);
      if(reco_number_of_selected_tracks == 1 ){
        h_reco_1p_1p->Fill(truth_decay_radius);
        h_decay_rad_true1p->Fill(truth_decay_radius);
      }
    }

    /// Number of tracks
    h_number_of_selected_reco_track->Fill(reco_number_of_selected_tracks);
    if( truth_hadronic_match == true ){
      h_number_of_selected_reco_track_truth_mached->Fill(reco_number_of_selected_tracks);
      h_number_of_truth_track->Fill(truth_nprong);
    }
    
    if(reco_number_of_selected_tracks == 3 ){
      h_reco_3p_3p->Fill(truth_decay_radius);
      if(truth_hadronic_match){
        if(truth_nprong == 3 ) {
          h_truth_3p->Fill(truth_decay_radius);
        }
      }
    }
  }
  
  /// ----------------------------------------------------------------------------------------
  // This distribution can be checked how is the construction.
  // 
  TCanvas* EtaTruth = new TCanvas("EtaTruth","EtaTruth");
  TH1D* clone_h_truth_eta = (TH1D*)h_truth_eta->Clone();
  clone_h_truth_eta->SetLineColor(kRed);
 
  clone_h_truth_eta->GetYaxis()->SetRangeUser(0,300);
  clone_h_truth_eta->GetXaxis()->SetTitle("#eta");

  clone_h_truth_eta->Draw("same");
  h_reco_eta->Draw("same");
  
  ATLASLabel(0.2,0.85,"Simulation");
  TLegend* legEtaAll = new TLegend(0.2,0.75,0.6,0.83);
  legEtaAll->SetFillColor(0);
  legEtaAll->SetFillStyle(0);
  legEtaAll->SetBorderSize(0);
  legEtaAll->AddEntry(h_reco_eta, "truth eta", "l");
  legEtaAll->AddEntry(h_truth_eta, "reconsructed eta", "l");
  legEtaAll->Draw();
  
  /// ----------------------------------------------------------------------------------------
  //   Eta distribution will be drawn by using the number of tracks.
  // 
  TCanvas* EtaTruthPerProng = new TCanvas("EtaTruthPerProng","EtaTruthPerProng");
  h_truth_eta = (TH1D*)h_truth_eta->Clone();
 
  h_truth_eta->GetYaxis()->SetRangeUser(0,300);
  h_truth_eta_1p->SetLineColor(kRed);
  h_truth_eta_3p->SetLineColor(kBlue);
  
  h_truth_eta->Draw();
  h_truth_eta_1p->Draw("same");
  h_truth_eta_3p->Draw("same");
  
  ATLASLabel(0.2,0.85,"Simulation");
  TLegend* legEtaTruthPerProng = new TLegend(0.2,0.75,0.6,0.83);
  legEtaTruthPerProng->SetFillColor(0);
  legEtaTruthPerProng->SetFillStyle(0);
  legEtaTruthPerProng->SetBorderSize(0);
  legEtaTruthPerProng->AddEntry(h_truth_eta,    "truth_eta", "l");
  legEtaTruthPerProng->AddEntry(h_truth_eta_1p, "truth eta (1-prong)", "l");
  legEtaTruthPerProng->AddEntry(h_truth_eta_3p, "truth eta (3-prong)", "l");
  legEtaTruthPerProng->Draw();
  
  /// ----------------------------------------------------------------------------------------
  //
  //
  TCanvas* EtaTruthVsRecoPerProng = new TCanvas("EtaTruthVsRecoPerProng","EtaTruthVsRecoPerProng");
  h_truth_eta = (TH1D*)h_truth_eta->Clone();
  h_reco_eta_1p = (TH1D*)h_reco_eta_1p->Clone();
  h_reco_eta_2p = (TH1D*)h_reco_eta_2p->Clone();
  h_reco_eta_3p = (TH1D*)h_reco_eta_3p->Clone();
 
  h_reco_eta_1p->SetLineColor(kMagenta);
  h_reco_eta_2p->SetLineColor(kRed);
  h_reco_eta_3p->SetLineColor(kBlue);
  
  h_reco_eta_1p->SetLineStyle(2);
  h_reco_eta_2p->SetLineStyle(2);
  h_reco_eta_3p->SetLineStyle(2);
 
  h_truth_eta->GetYaxis()->SetRangeUser(0,300);
  h_truth_eta->GetXaxis()->SetTitle("#eta");
  
  h_truth_eta->Draw();
  h_truth_eta_1p->Draw("same");
  h_truth_eta_3p->Draw("same");
  
  h_reco_eta_1p->Draw("same");
  h_reco_eta_2p->Draw("same");
  h_reco_eta_3p->Draw("same");
  
  ATLASLabel(0.2,0.85,"Simulation");
  TLegend* legEtaPerNTrack = new TLegend(0.2,0.75,0.6,0.83);
  legEtaPerNTrack->SetFillColor(0);
  legEtaPerNTrack->SetFillStyle(0);
  legEtaPerNTrack->SetBorderSize(0);
  legEtaPerNTrack->AddEntry(h_truth_eta,   "truth eta", "l");
  legEtaPerNTrack->AddEntry(h_reco_eta_1p, "reco eta 1-prong", "l");
  legEtaPerNTrack->AddEntry(h_reco_eta_2p, "reco eta 2-prong", "l");
  legEtaPerNTrack->AddEntry(h_reco_eta_3p, "reco eta 3-prong", "l");
  legEtaPerNTrack->Draw();

  return;
}
