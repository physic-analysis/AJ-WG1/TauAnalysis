
#include <TPad.h>

void Draw()
{
  TFile* file = new TFile("/Users/takeda/workspace/public/atlas-phys/AJ/WG1/MySkimTrees.outputs.root");
  //file->ReadAll();
  TTree* tree = (TTree*)file->Get("m_InDetTrackParticles");
  //tree->Draw("d0");

  TCanvas *c1 = new TCanvas("c1","c1");
  TList* list = file->GetListOfKeys();
  //TKey *key = 0;
  TIter next(list);
  TObject* obj;
  while( (obj = next()) ){
    //key->ReadObj()->Draw();
    (TH1D*)obj;
    obj->Draw();
    c1->Clear();
  }
    
 // TObjLink *lnk = tree->GetListOfPrimitives()->FirstLink();          //
 // while (lnk) {                                                //
 //   lnk->GetObject()->Draw(lnk->GetOption());                 //
 //   lnk = lnk->Next();                                        //
 // }     
}
