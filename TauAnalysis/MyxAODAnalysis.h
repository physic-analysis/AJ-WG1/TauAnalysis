//!  MyxAODAnalysis
/*!
  A more elaborate class description.
  */


#ifndef TauAnalysis_MyxAODAnalysis_H
#define TauAnalysis_MyxAODAnalysis_H

#include <AnaAlgorithm/AnaAlgorithm.h>

// Tau
#include "xAODTau/TauJetContainer.h"
#include "xAODTau/TauJetAuxContainer.h"
#include "xAODTau/TauDefs.h" 
#include "xAODTau/TauJet.h"

#include "xAODTau/TauTrack.h"
#include "xAODTau/TauTrackContainer.h"
#include "xAODTau/TauTrackAuxContainer.h"

// TauAnalysisTools (external package)
//#include "TauAnalysisTools/TauEfficiencyCorrectionsTool.h"
#include "TauAnalysisTools/TauSelectionTool.h"
#include "TauAnalysisTools/Enums.h"
//#include "TauAnalysisTools/TauSmearingTool.h"
//#include "TauAnalysisTools/TauTruthMatchingTool.h"
//#include "TauAnalysisTools/TauTruthTrackMatchingTool.h"
#include "TauAnalysisTools/TauOverlappingElectronLLHDecorator.h"

#include "TauAnalysisTools/ITauSelectionTool.h"
#include "TauAnalysisTools/ITauSmearingTool.h"
#include "TauAnalysisTools/ITauEfficiencyCorrectionsTool.h"
#include "TauAnalysisTools/ITauTruthMatchingTool.h"

// xAOD Tracking
#include "xAODTracking/TrackParticle.h"
#include "xAODTracking/TrackParticleContainer.h"
#include "xAODTracking/TrackStateValidation.h"
#include "xAODTracking/TrackStateValidationContainer.h"
#include "xAODTracking/TrackMeasurementValidationContainer.h"
#include "xAODTracking/TrackParticlexAODHelpers.h"

//ATLAS (EDM)
#include "xAODCore/AuxStoreAccessorMacros.h"
#include "xAODEgamma/ElectronContainer.h"
#include "xAODEventInfo/EventInfo.h"
#include "xAODJet/JetContainer.h"
#include "xAODMuon/MuonContainer.h"
#include "xAODMuon/MuonSegmentContainer.h"
#include "xAODTracking/TrackParticlexAODHelpers.h"
#include "xAODTruth/TruthParticle.h"
#include "xAODTruth/TruthVertex.h"
#include "xAODBase/IParticleHelpers.h"

// xAOD Core
#include "xAODCore/ShallowAuxContainer.h"
#include "xAODCore/ShallowCopy.h"



#include <TH1.h>

class MyxAODAnalysis : public EL::AnaAlgorithm
{
  public:
    //! this is a standard algorithm constructor
    MyxAODAnalysis (const std::string& name, ISvcLocator* pSvcLocator);

    //! these are the functions inherited from Algorithm
    virtual StatusCode initialize () override;
    virtual StatusCode execute () override;
    virtual StatusCode finalize () override;

    typedef std::vector<ElementLink< xAOD::TrackStateValidationContainer > > MeasurementsOnTrack;
    typedef std::vector<ElementLink< xAOD::TrackStateValidationContainer > >::const_iterator MeasurementsOnTrackIter;

    void InitSkim();
    void FillInDetTrackParticle(const xAOD::TrackParticleContainer*);
    void Write();

  private:
    // Configuration, and any other types of variables go here.
    //float m_cutValue;
    //TTree *m_myTree;
    //TauAnalysisTools::TauSelectionTool* m_TauSelTool;
 
    /* Scan the xAOD basic variables */
    TTree* m_InDetTrackParticles;

    /* true tau tree */
    // TruthParticle container information 
    Int_t   tt_truth_TruthParticleContainerSize; //!
    Int_t   tt_truth_chargedHadron; //!
    // Basic truth information 

    TTree* m_trueTauTree; //!
    // General information 
    Int_t eventNumber;
    
    ////////////////////// Truth information 
    std::vector<int>             Truth_part_isCharged;//! /*!< The truth particle has a charge or not.*/
    std::vector<int>             Truth_part_status; //!
    std::vector<int>             Truth_part_pdgId;//!
    std::vector<int>             Truth_part_hasProdVtx;//!
    std::vector<int>             Truth_part_hasDecayVtx;//!
    std::vector<int>             Truth_part_nParents;//!
    std::vector<int>             Truth_part_nChildren;//!
    std::vector<TLorentzVector>  Truth_part_p4;//!
    int                          Truth_part_numChargedTracks;//!
    int                          Truth_part_numChargedLeptonTracks;//!
    //
    ////////////////////// Truth daughter particles information 
    std::vector<int>             Truth_dau_pdgId;//!
    //
    ////////////////////// Truth production vertex information 
    Float_t                      Truth_prodVtx_x;//!
    Float_t                      Truth_prodVtx_y;//!
    Float_t                      Truth_prodVtx_z;//!
    Float_t                      Truth_prodVtx_perp;//!
    Float_t                      Truth_prodVtx_eta;//!
    Float_t                      Truth_prodVtx_phi;//!
    Float_t                      Truth_prodVtx_t;//!
    //
    ////////////////////// Truth decay vertex information 
    Float_t                      Truth_decVtx_x;//!
    Float_t                      Truth_decVtx_y;//!
    Float_t                      Truth_decVtx_z;//!
    Float_t                      Truth_decVtx_perp;//!
    Float_t                      Truth_decVtx_eta;//!
    Float_t                      Truth_decVtx_phi;//!
    Float_t                      Truth_decVtx_t;//!
    //
    Int_t                        Truth_tau_pdgId; //!
    Int_t                        Truth_tau_numTracks; //!
    Bool_t                       Truth_tau_isMatchedWithReco; //!
    Double_t                     tt_truth_neutrino_pt; //!      
    Double_t                     tt_truth_neutrino_eta; //!  
    Double_t                     tt_truth_neutrino_phi; //!    
    Double_t                     tt_truth_neutrino_e; //!
    //
    // Truth tau info (After the isTau selection)
    Double_t                     Truth_tau_decayRad; //!
    Double_t                     Truth_tau_pt; //!
    Double_t                     Truth_tau_eta; //!
    Double_t                     Truth_tau_phi; //!
    Double_t                     Truth_tau_e; //!
    Double_t                     Truth_tau_m; //!
    Bool_t                       Truth_tau_isLeptonic;//!
    Bool_t                       Truth_tau_isHadronic;//!
    Bool_t                       tt_truth_leptonic_electron;//!
    Bool_t                       tt_truth_leptonic_muon;//!  
    Bool_t                       tt_truth_leptonic_tau;//!  
    //
    ////////////////////// Truth neutrino information
    std::vector<int>             Truth_neutrino_pdgId;//!
    std::vector<double>          Truth_neutrino_pt;//!
    std::vector<double>          Truth_neutrino_eta;//!
    std::vector<double>          Truth_neutrino_phi;//!
    // 
    // Truth track associated with tau info
    std::vector<double>          Truth_track_pt;          //! Truth track momentum from the tau decay
    std::vector<double>          Truth_track_eta;         //! Truth track eta from the tau decay
    std::vector<double>          Truth_track_phi;         //! Truth track phi from the tau decay
    std::vector<double>          Truth_track_m;           //! Truth track TLorentzVector from the tau decay
    std::vector<double>          Truth_track_vtx;         //! Truth track production position from the tau decay
    std::vector<double>          Truth_track_charge;      //! Truth track production position from the tau decay
    std::vector<int>             Truth_track_pdgId;      //! Truth track production position from the tau decay
    //
    // Pixel cluster information (not considered the tau)
    Int_t                        Pix_numTracks;        //! the number of tracks passed pixel layer per events
    Int_t                        Pix_numCluster_IBL;   //! the number of clusters on the IBL
    Int_t                        Pix_numCluster_BLayer;//! the number of clusters on the B-Layer
    Int_t                        Pix_numCluster_L1;    //! the number of clusters on the pixel 2nd layer 
    Int_t                        Pix_numCluster_L2;    //! the number of clusters on the pixel 3rd layer
    Double_t                     Pix_charge_IBL; //!
    Double_t                     Pix_charge_BLayer; //!
    Double_t                     Pix_charge_L1; //!
    Double_t                     Pix_charge_L2; //!
    //////////////////////////////////////////////////////////////
    // InDetTrack informaiton 
    //  - This is the reconstructed tracks
    Int_t                        InDet_numTracks; //!
    std::vector<double>          InDet_track_pt; //!
    std::vector<double>          InDet_track_eta; //!
    std::vector<double>          InDet_track_phi; //!
    std::vector<double>          InDet_track_m; //!
    std::vector<float>           InDet_track_charge; //!
    std::vector<double>          InDet_track_d0; //!
    std::vector<double>          InDet_track_z0; //!
    std::vector<bool>            InDet_track_pixZero; //!
    std::vector<bool>            InDet_track_sctZero; //!
    std::vector<int>             InDet_track_numberOfIBLHits; //!
    std::vector<int>             InDet_track_numberOfBLayerHits; //!
    std::vector<int>             InDet_track_numberOfPixelL1Hits; //!
    std::vector<int>             InDet_track_numberOfPixelL2Hits; //!
    std::vector<int>             InDet_track_sctHits; //!
    std::vector<double>          InDet_track_pixzero_pt; //!
    std::vector<double>          InDet_track_pixzero_eta; //!
    std::vector<double>          InDet_track_pixzero_phi; //!
    std::vector<double>          InDet_track_pixzero_m; //!
    std::vector<int>             InDet_track_barcode; //!
    std::vector<int>             InDet_track_type; //!
    std::vector<int>             InDet_track_numberOfPixelHits; //!
    std::vector<int>             InDet_track_numberOfSCTHits; //!
    std::vector<double>          InDet_track_vx; //!
    std::vector<double>          InDet_track_vy; //!
    std::vector<double>          InDet_track_vz; //!
    //  - This is the raw pixel cluster information 
    std::vector<std::vector<double>> InDet_pixel_raw_ClustersEta;//!
    std::vector<std::vector<double>> InDet_pixel_raw_ClustersPhi;//!
    std::vector<std::vector<double>> InDet_pixel_raw_ClustersCharge;//!
    std::vector<std::vector<double>> InDet_pixel_raw_globalX;//!
    std::vector<std::vector<double>> InDet_pixel_raw_globalY;//!
    std::vector<std::vector<double>> InDet_pixel_raw_globalZ;//!
    std::vector<std::vector<int>>    InDet_pixel_raw_IB_energyDeposit;//!
    std::vector<std::vector<int>>    InDet_pixel_raw_BL_energyDeposit;//!
    std::vector<std::vector<int>>    InDet_pixel_raw_L1_energyDeposit;//!
    std::vector<std::vector<int>>    InDet_pixel_raw_L2_energyDeposit;//!
    std::vector<std::vector<int>>    InDet_pixel_raw_IB_barcode;//!
    std::vector<std::vector<int>>    InDet_pixel_raw_BL_barcode;//!
    std::vector<std::vector<int>>    InDet_pixel_raw_L1_barcode;//!
    std::vector<std::vector<int>>    InDet_pixel_raw_L2_barcode;//!
    std::vector<std::vector<int>>    InDet_pixel_raw_IB_pdgId;//!
    std::vector<std::vector<int>>    InDet_pixel_raw_BL_pdgId;//!
    std::vector<std::vector<int>>    InDet_pixel_raw_L1_pdgId;//!
    std::vector<std::vector<int>>    InDet_pixel_raw_L2_pdgId;//!
    std::vector<std::vector<double>> InDet_sct_raw_globalX;//!
    std::vector<std::vector<double>> InDet_sct_raw_globalY;//!
    std::vector<std::vector<double>> InDet_sct_raw_globalZ;//!
    //
    //  - This is the pixel cluster information 
    Int_t                            InDet_pixel_numAllOfClusters; //!
    std::vector<std::vector<double>> InDet_pixel_IBLClustersEta;//!
    std::vector<std::vector<double>> InDet_pixel_BLayerClustersEta;//!
    std::vector<std::vector<double>> InDet_pixel_L1ClustersEta;//!
    std::vector<std::vector<double>> InDet_pixel_L2ClustersEta;//!
    std::vector<std::vector<double>> InDet_pixel_IBLClustersPhi;//!
    std::vector<std::vector<double>> InDet_pixel_BLayerClustersPhi;//!
    std::vector<std::vector<double>> InDet_pixel_L1ClustersPhi;//!
    std::vector<std::vector<double>> InDet_pixel_L2ClustersPhi;//!
    std::vector<std::vector<double>> InDet_pixel_IBLClustersCharge;//!
    std::vector<std::vector<double>> InDet_pixel_BLayerClustersCharge;//!
    std::vector<std::vector<double>> InDet_pixel_L1ClustersCharge;//!
    std::vector<std::vector<double>> InDet_pixel_L2ClustersCharge;//!
    // 
    //////////////////////////////////////////////////////////////
    // Reco tau info (After passing the truth matching)
    Double_t                     Reco_tau_pt; //!
    Double_t                     Reco_tau_eta; //!
    Double_t                     Reco_tau_phi; //!
    Double_t                     Reco_tau_e; //!
    Int_t                        Reco_tau_numTracks; //!
    Bool_t                       Reco_tau_isSelected; //!
    //
    // Reco tracks assciated with tau info 
    std::vector<double>          Reco_track_pt;  //! Reco-tau tracks momentum 
    std::vector<double>          Reco_track_eta; //! Reco-tau tracks eta 
    std::vector<double>          Reco_track_phi; //! Reco-tau tracks phi 
    std::vector<double>          Reco_track_e;   //! Reco-tau tracks e 
    std::vector<double>          Reco_track_m;   //! Reco-tau tracks e 
    std::vector<double>          Reco_track_d0;  //! A track d0 parameter from reco-tau 
    std::vector<double>          Reco_track_z0;  //! A track z0 parameter from reco-tau
    std::vector<int>             Reco_track_numberOfPixelHits;//!
    // 
    // Pixel 
    std::vector<unsigned int>    tt_numberOfContribPixelLayers            ; //!
    std::vector<unsigned int>    tt_numberOfPixelHits                     ; //!
    std::vector<unsigned int>    tt_numberOfPixelHoles                    ; //!
    std::vector<unsigned int>    tt_expectInnermostPixelLayerHit          ; //!
    std::vector<unsigned int>    tt_numberOfInnermostPixelLayerHits       ; //!
    std::vector<unsigned int>    tt_numberOfNextToInnermostPixelLayerHits ; //!
    std::vector<unsigned int>    tt_numberOfGangedPixels                  ; //!
    std::vector<unsigned int>    tt_numberOfPixelDeadSensors              ; //!
    std::vector<unsigned int>    tt_numberOfPixelSharedHits               ; //!
    std::vector<unsigned int>    tt_numberOfPixelSplitHits               ; //!
    std::vector<unsigned int>    tt_numberOfPixelOutliers                 ; //!
    std::vector<unsigned int>    tt_numberOfPixelSpoiltHits               ; //!
    // Pixel B-Layer
    std::vector<unsigned int>    tt_numberOfBLayerHits             ; //!
    std::vector<unsigned int>    tt_numberOfBLayerSharedHits       ; //!
    std::vector<unsigned int>    tt_numberOfBLayerOutliers         ; //!
    std::vector<unsigned int>    tt_numberOfBLayerSplitHits        ; //!
    std::vector<unsigned int>    tt_expectBLayerHit                ; //!
    // Precision information
    std::vector<unsigned int>    tt_numberOfPrecisionLayers        ; //!   layers with at least 3 hits [unit8_t].
    std::vector<unsigned int>    tt_numberOfPrecisionHoleLayers    ; //!   layers with holes AND no hits [unit8_t].
    std::vector<unsigned int>    tt_numberOfPhiLayers              ; //!   layers with a trigger phi hit [unit8_t].
    std::vector<unsigned int>    tt_numberOfPhiHoleLayers          ; //!   layers with trigger phi holes but no hits [unit8_t].
    std::vector<unsigned int>    tt_numberOfTriggerEtaLayers       ; //!   layers with trigger eta hits [unit8_t].
    std::vector<unsigned int>    tt_numberOfTriggerEtaHoleLayers   ; //!   layers with trigger eta holes but no hits [unit8_t].
    std::vector<unsigned int>    tt_numberOfGoodPrecisionLayers    ; //!   layers with at least 3 hits that are not deweighted [uint8_t]
    std::vector<unsigned int>    tt_numberOfOutliersOnTrack        ; //!   number of measurements flaged as outliers in TSOS [unit8_t].
    //
    std::vector<unsigned int>    tt_numberOfSCTHits                ; //!
    std::vector<unsigned int>    tt_numberOfTRTHits                ; //!
    std::vector<unsigned int>    tt_numberOfSCTSharedHits          ; //!
    std::vector<unsigned int>    tt_numberOfTRTSharedHits          ; //!
    std::vector<unsigned int>    tt_numberOfSCTHoles               ; //!
    std::vector<unsigned int>    tt_numberOfSCTDoubleHoles         ; //!
    std::vector<unsigned int>    tt_numberOfTRTHoles               ; //!
    std::vector<unsigned int>    tt_numberOfSCTOutliers            ; //!
    std::vector<unsigned int>    tt_numberOfTRTOutliers            ; //!
    std::vector<unsigned int>    tt_numberOfSCTSpoiltHits          ; //!
    std::vector<unsigned int>    tt_numberOfGangedFlaggedFakes     ; //!
    std::vector<unsigned int>    tt_numberOfSCTDeadSensors         ; //!
    std::vector<unsigned int>    tt_numberOfTRTDeadStraws          ; //!
    std::vector<float>           tt_pixeldEdx; //!                     

    // Pixel raw cluster
    std::vector<unsigned int>    Reco_track_numberOfInnermostPixelLayerSensors;//!
    std::vector<unsigned int>    Reco_track_numberOfNextToInnermostPixelLayerSensors;//!
    std::vector<unsigned int>    Reco_track_numberOfPixelL1Sensors;//!
    std::vector<unsigned int>    Reco_track_numberOfPixelL2Sensors;//!
    
    std::vector<unsigned int>    Reco_track_numberOfInnermostPixelHits;        //! The number of hits associated with reco-tau
    std::vector<unsigned int>    Reco_track_numberOfNextToInnermostPixelHits;  //! The number of hits associated with reco-tau
    std::vector<unsigned int>    Reco_track_numberOfPixelL1Hits;               //! The number of hits associated with reco-tau
    std::vector<unsigned int>    Reco_track_numberOfPixelL2Hits;               //! The number of hits associated with reco-tau
   
    // Scan
    std::vector<float>*         InDetTrackParticles_d0                                           ;
    std::vector<float>*         InDetTrackParticles_z0                                           ;
    std::vector<float>*         InDetTrackParticles_phi                                          ;
    std::vector<float>*         InDetTrackParticles_theta                                        ;
    std::vector<float>*         InDetTrackParticles_qOverP                                       ;
    //std::vector<>*            InDetTrackParticles_definingParametersCovMatrix                ;
    std::vector<float>*         InDetTrackParticles_vx                                           ;
    std::vector<float>*         InDetTrackParticles_vy                                           ;
    std::vector<float>*         InDetTrackParticles_vz                                           ;
    std::vector<float>*         InDetTrackParticles_radiusOfFirstHit                             ;
    std::vector<unsigned long>* InDetTrackParticles_identifierOfFirstHit                         ;
    std::vector<float>*         InDetTrackParticles_beamlineTiltX                                ;
    std::vector<float>*         InDetTrackParticles_beamlineTiltY                                ;
    std::vector<unsigned int>*  InDetTrackParticles_hitPattern                                   ;
    std::vector<float>*         InDetTrackParticles_chiSquared                                   ;
    std::vector<float>*         InDetTrackParticles_numberDoF                                    ;
    std::vector<unsigned char>* InDetTrackParticles_trackFitter                                  ;
    std::vector<unsigned long>* InDetTrackParticles_particleHypothesis                           ;
    std::vector<unsigned char>* InDetTrackParticles_trackProperties                              ;
    std::vector<unsigned char>* InDetTrackParticles_patternRecoInfo                              ;
    std::vector<unsigned char>* InDetTrackParticles_numberOfContribPixelLayers                   ;
    std::vector<unsigned char>* InDetTrackParticles_numberOfInnermostPixelLayerHits              ;
    std::vector<unsigned char>* InDetTrackParticles_numberOfInnermostPixelLayerOutliers          ;
    std::vector<unsigned char>* InDetTrackParticles_numberOfInnermostPixelLayerSharedHits        ;
    std::vector<unsigned char>* InDetTrackParticles_numberOfInnermostPixelLayerSplitHits         ;
    std::vector<unsigned char>* InDetTrackParticles_expectInnermostPixelLayerHit                 ;
    std::vector<unsigned char>* InDetTrackParticles_numberOfNextToInnermostPixelLayerHits        ;
    std::vector<unsigned char>* InDetTrackParticles_numberOfNextToInnermostPixelLayerOutliers    ;
    std::vector<unsigned char>* InDetTrackParticles_numberOfNextToInnermostPixelLayerSharedHits  ;
    std::vector<unsigned char>* InDetTrackParticles_numberOfNextToInnermostPixelLayerSplitHits   ;
    std::vector<unsigned char>* InDetTrackParticles_expectNextToInnermostPixelLayerHit           ;
    std::vector<unsigned char>* InDetTrackParticles_numberOfPixelHits                            ;
    std::vector<unsigned char>* InDetTrackParticles_numberOfPixelOutliers                        ;
    std::vector<unsigned char>* InDetTrackParticles_numberOfPixelHoles                           ;
    std::vector<unsigned char>* InDetTrackParticles_numberOfPixelSharedHits                      ;
    std::vector<unsigned char>* InDetTrackParticles_numberOfPixelSplitHits                       ;
    std::vector<unsigned char>* InDetTrackParticles_numberOfGangedPixels                         ;
    std::vector<unsigned char>* InDetTrackParticles_numberOfGangedFlaggedFakes                   ;
    std::vector<unsigned char>* InDetTrackParticles_numberOfPixelDeadSensors                     ;
    std::vector<unsigned char>* InDetTrackParticles_numberOfPixelSpoiltHits                      ;
    std::vector<unsigned char>* InDetTrackParticles_numberOfDBMHits                              ;
    std::vector<unsigned char>* InDetTrackParticles_numberOfSCTHits                              ;
    std::vector<unsigned char>* InDetTrackParticles_numberOfSCTOutliers                          ;
    std::vector<unsigned char>* InDetTrackParticles_numberOfSCTHoles                             ;
    std::vector<unsigned char>* InDetTrackParticles_numberOfSCTDoubleHoles                       ;
    std::vector<unsigned char>* InDetTrackParticles_numberOfSCTSharedHits                        ;
    std::vector<unsigned char>* InDetTrackParticles_numberOfSCTDeadSensors                       ;
    std::vector<unsigned char>* InDetTrackParticles_numberOfSCTSpoiltHits                        ;
    std::vector<unsigned char>* InDetTrackParticles_numberOfTRTHits                              ;
    std::vector<unsigned char>* InDetTrackParticles_numberOfTRTOutliers                          ;
    std::vector<unsigned char>* InDetTrackParticles_numberOfTRTHoles                     ;
    std::vector<unsigned char>* InDetTrackParticles_numberOfTRTHighThresholdHits         ;
    std::vector<unsigned char>* InDetTrackParticles_numberOfTRTHighThresholdHitsTotal    ;
    std::vector<unsigned char>* InDetTrackParticles_numberOfTRTHighThresholdOutliers     ;
    std::vector<unsigned char>* InDetTrackParticles_numberOfTRTDeadStraws                ;
    std::vector<unsigned char>* InDetTrackParticles_numberOfTRTTubeHits                  ;
    std::vector<unsigned char>* InDetTrackParticles_numberOfTRTXenonHits                 ;
    std::vector<unsigned char>* InDetTrackParticles_numberOfTRTSharedHits                ;
    std::vector<unsigned char>* InDetTrackParticles_numberOfPrecisionLayers              ;
    std::vector<unsigned char>* InDetTrackParticles_numberOfPrecisionHoleLayers          ;
    std::vector<unsigned char>* InDetTrackParticles_numberOfPhiLayers                    ;
    std::vector<unsigned char>* InDetTrackParticles_numberOfPhiHoleLayers                ;
    std::vector<unsigned char>* InDetTrackParticles_numberOfTriggerEtaLayers             ;
    std::vector<unsigned char>* InDetTrackParticles_numberOfTriggerEtaHoleLayers         ;
    std::vector<unsigned char>* InDetTrackParticles_numberOfOutliersOnTrack              ;
    std::vector<unsigned char>* InDetTrackParticles_standardDeviationOfChi2OS            ;
    std::vector<float>*         InDetTrackParticles_eProbabilityComb                     ;
    std::vector<float>*         InDetTrackParticles_eProbabilityHT                       ;
    std::vector<float>*         InDetTrackParticles_pixeldEdx                            ;
    std::vector<unsigned char>* InDetTrackParticles_numberOfUsedHitsdEdx                 ;
    std::vector<unsigned char>* InDetTrackParticles_numberOfIBLOverflowsdEdx             ;
    //std::vector<>*            InDetTrackParticles_vertexLink                          ;
    std::vector<float>*         InDetTrackParticles_TRTTrackOccupancy                    ;
    //std::vector<>*            InDetTrackParticles_TrkIBLX                             ;
    std::vector<unsigned char>* InDetTrackParticles_TRTdEdxUsedHits                      ;
    std::vector<float>*         InDetTrackParticles_TrkIBLY                              ;
    std::vector<float>*         InDetTrackParticles_TrkIBLZ                              ;
    std::vector<float>*         InDetTrackParticles_TRTdEdx                              ;
    std::vector<float>*         InDetTrackParticles_TrkBLX                               ;
    std::vector<float>*         InDetTrackParticles_TrkBLY                               ;
    std::vector<float>*         InDetTrackParticles_TrkBLZ                               ;
    std::vector<int>*           InDetTrackParticles_nBC_meas                             ;
    std::vector<float>*         InDetTrackParticles_TrkL1X                               ;
    std::vector<float>*         InDetTrackParticles_TrkL1Y                               ;
    std::vector<float>*         InDetTrackParticles_TrkL2X                               ;
    std::vector<float>*         InDetTrackParticles_truthMatchProbability                ;
    std::vector<float>*         InDetTrackParticles_TrkL2Y                               ;
    //std::vector<>*            InDetTrackParticles_truthType                           ;
    //std::vector<>*            InDetTrackParticles_truthOrigin                         ;
    std::vector<float>*         InDetTrackParticles_TrkL2Z                               ;
    //std::vector<>*            InDetTrackParticles_measurement_region                  ;
    //std::vector<>*            InDetTrackParticles_measurement_det                     ;
    //std::vector<>*            InDetTrackParticles_measurement_iLayer                  ;
    //std::vector<>*            InDetTrackParticles_hitResiduals_residualLocX           ;
    //std::vector<>*            InDetTrackParticles_hitResiduals_pullLocX               ;
    //std::vector<>*            InDetTrackParticles_hitResiduals_residualLocY           ;
    //std::vector<>*            InDetTrackParticles_hitResiduals_pullLocY               ;
    //std::vector<>*            InDetTrackParticles_hitResiduals_phiWidth               ;
    //std::vector<>*            InDetTrackParticles_hitResiduals_etaWidth               ;
    //std::vector<>*            InDetTrackParticles_measurement_type                    ;
    std::vector<float>*         InDetTrackParticles_d0err                       ;
    std::vector<float>*         InDetTrackParticles_z0err                       ;
    std::vector<float>*         InDetTrackParticles_phierr                      ;
    std::vector<float>*         InDetTrackParticles_thetaerr                    ;
    std::vector<float>*         InDetTrackParticles_qopterr                     ;
    //std::vector<>* InDetTrackParticles_msosLink                   ;
    //std::vector<>* InDetTrackParticles_caloExt_Decorated          ;
    //std::vector<>* InDetTrackParticles_caloExt_eta                ;
    //std::vector<>* InDetTrackParticles_caloExt_phi                ;




















    /// StoreGate key for the muon container to investigate
    std::string m_sgKey;
    /// Connection to the selection tool
    ToolHandle< TauAnalysisTools::ITauSelectionTool > m_selTool;
    /// Connection to the smearing tool
    ToolHandle< TauAnalysisTools::ITauSmearingTool > m_smearTool;
    /// Connection to the efficiency correction tool
    ToolHandle< TauAnalysisTools::ITauEfficiencyCorrectionsTool > m_effTool;
    
    ToolHandle< TauAnalysisTools::ITauTruthMatchingTool > m_truthTau;
    ToolHandle< TauAnalysisTools::ITauSmearingTool > m_smeTool;

    // Selection tool
    TauAnalysisTools::TauSelectionTool* TauSelTool;

    TauAnalysisTools::TauOverlappingElectronLLHDecorator* TOELLHDecorator;
    AsgElectronLikelihoodTool* m_tEMLHTool;
};

#endif
