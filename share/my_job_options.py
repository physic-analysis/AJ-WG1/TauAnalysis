import AthenaRootComps.ReadAthenaxAODHybrid

simulator='MC12G4'
#simulator='FullG4_LongLived'
test_name="_test" # if you do some tests, you should change the suffix
#Cut="_decayRad122.5"
Cut="_noCut"
filesInput = "/afs/cern.ch/user/k/ktakeda/workspace/public/Data/AJ/SingleTau/xAOD/user.ktakeda.singletau." + simulator + ".pT1000GeV.100000events_v3_EXT1/*";
#filesInput = "/afs/cern.ch/user/k/ktakeda/workspace/public/Data/AJ/SingleTau/xAOD/user.ktakeda.singletau." + simulator + ".pT1000GeV.100000events_v3_EXT1/user.ktakeda.15239416.EXT1._000001.xAOD.pool.root";

theApp.EvtMax=-1                                         #says how many events to run over. Set to -1 for all events
from glob import glob
jps.AthenaCommonFlags.FilesInput = glob(vars().get("filesInput","*.root"))  #list of input files

algSeq = CfgMgr.AthSequencer("AthAlgSeq")

# create our algorithm with teh given name
alg = CfgMgr.MyxAODAnalysis()

# later on we'll add some configuration options for our algorithm that go here

algSeq += alg

# optional include for reducing printout from athena
include("AthAnalysisBaseComps/SuppressLogging.py")

ServiceMgr += CfgMgr.THistSvc()
#ServiceMgr.THistSvc.Output += [ "MYSTREAM DATAFILE='MyxAODAnalysis.10000events.outputs.root' OPT='RECREATE'" ]

another_stream = "ANOTHERSTREAM DATAFILE='MyTauNtuple." + simulator + test_name + ".outputs.root' OPT='RECREATE'"
output_name = "SKIMTREESTREAM DATAFILE='MySkimTrees." + simulator + Cut +  ".outputs.root' OPT='RECREATE'"
ServiceMgr.THistSvc.Output += [ another_stream ]
ServiceMgr.THistSvc.Output += [ output_name ]

#jps.AthenaCommonFlags.HistOutputs = ["MYSTREAM:myfile.root"]
