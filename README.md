# TauAnalysis
This package can be used to analyze the tau.
Main logic will output two TTrees. One is for the reco-tau based analysis, and the another one is for the truth-tau based analysis.
* Reco-tau based analysis  (m\_recoTauTree)
  * At first this algorithm select a reco-tau, and then we'll find a truth-tau.
* Truth-tau based analysis (m\_trueTauTree)
  * At first this algorithm select a truth-tau, and then we'll find a reco-tau. Of cource, the reconstruction efficiency exits, so the algorithm may not find it.
    Therefore, we can calculate the reconstruction efficiency using this TTree tau.

## How to use
### How to compile this package 
- mkdir source run build
- cd source/
- git clone https://:@gitlab.cern.ch:8443/physic-analysis/AJ-WG1/TauAnalysis.git
- cd ../build
- setupATLAS
- acmSetup AthAnalysis,21.2.36

### Whenever you return, you just need to do:
- cd build/
- acmSetup

### how to compile 
- cd build/
- acm compile

### how to run the package
- cd run/
- athena.py TauAnalysis/my\_job\_options.py

## Descriptions for each directory
* Root 
  * This directory includes the main logic source code (MyxAODAnalysis.cxx).
* TauAnalysis
  * This directory includes header files. The main header file is MyxAODAnalysis.h.
* macro
  * This directory includes ROOT macro files.
* share  
* src
